CREATE OR REPLACE PROCEDURE ADWU_OPTIMA_LAEX.OPT_AX_TBLSPACE (
                                  I_V_REGN IN VARCHAR2,
                                  O_TBL_TS   OUT VARCHAR2,
                                  O_IDX_TS   OUT VARCHAR2
                                 )
------------------------------------------------------------------------------------
  -- Procedure    : OPT_AX_TBLSPACE                                               --
  -- Originator   : Senthil (senthilkumar.js@hp.com)                              --
  -- Author       : Senthil (senthilkumar.js@hp.com)                              --
  -- Date         : 06-Oct-2014                                                   --
  -- Purpose      : Set the Corresponding Region TS for Table and Index           --
  ----------------------------------------------------------------------------------
  -- Parameters
  ----------------------------------------------------------------------------------
  -- No  Name              Desctiption          /   Example                       --
  ----------------------------------------------------------------------------------
  -- 1  O_TBL_TS           TableSpace (Table)   /   'DATA01_COMP_LOW_AP'          --
  -- 1  O_TBL_TS           TableSpace (Index)   /   'INDEX01_AP'                  --
  ----------------------------------------------------------------------------------
  -- Change History                                                               --
  -- Date        Programmer        Description                                    --
  -- ----------- ----------------- -------------------------------------------------
  -- 06-Oct-2014 Senthil Kumar     Initial Version                                --
------------------------------------------------------------------------------------
AS
   V_REGN    VARCHAR2(4);

BEGIN
  IF I_V_REGN IS NOT NULL THEN V_REGN := I_V_REGN;
  ELSE
  --Find which region and assign the corresponding TS
  SELECT SUBSTR(USER,INSTR(USER,'_',2,2)+1,2) INTO V_REGN FROM DUAL;
  END IF;

  IF V_REGN IS NULL THEN
     O_TBL_TS     := 'TABLESPACE DEFAULT';
     O_IDX_TS     := 'DEFAULT';
  ELSE
     O_TBL_TS     := 'TABLESPACE DATA01_COMP_LOW_'||V_REGN;
     O_IDX_TS     := 'INDEX01_'||V_REGN;
  END IF;
DBMS_OUTPUT.PUT_LINE ('TBLSPACE :' ||O_TBL_TS);
DBMS_OUTPUT.PUT_LINE ('INDEX NAME:'||O_IDX_TS);
  
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('Sql Errors Occured while '||SQLCODE ||'The Error code is : ' || SQLCODE || ': ' || SUBSTR(SQLERRM, 1, 64));
    RETURN;
END;
--sox implementation end
/
