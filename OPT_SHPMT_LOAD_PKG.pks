CREATE OR REPLACE PACKAGE ADWU_OPTIMA_LAEX.OPT_SHPMT_LOAD_PKG IS

  /**********************************************************************/
  /* name: OPT_SHPMT_LOAD_PKG                                              */
  /* purpose:build composite partitions for shipment and its restatement  */
  /*     exchange partitions for shipment tables           */
  /* author: Bodhi Wang                                                   */
  /* version: 1.00 - initial version                                    */
  /**********************************************************************/

  C_FINAL_TBL_STORAGE       VARCHAR2(100) := 'PCTFREE 0 PCTUSED 0 INITRANS 1 MAXTRANS 255';
  C_STATS_FINAL_EST_PERCENT NUMBER := 5;
  C_STATS_TEMP_EST_PERCENT  NUMBER := 5;
  C_DEFAULT_PAST_MTH_LIMIT  NUMBER := 24;
  C_NEXT_MTH_LIMIT          NUMBER := 12;

  PROCEDURE PRO_PUT_LINE(P_STRING IN VARCHAR2);
  FUNCTION FN_FETCH_PAST_MTH_LIMIT(P_TBL_NAME IN VARCHAR2) return number;

  PROCEDURE PRO_CHECK_TBL(P_TBL_NAME IN USER_TABLES.TABLE_NAME%TYPE,
                          P_FLAG     OUT NUMBER);

  PROCEDURE PRO_DROP_PRTTN(P_TBL_NAME IN USER_TABLES.TABLE_NAME%TYPE,
                           P_DROP_MAX_IND IN VARCHAR2 DEFAULT 'Y');
  PROCEDURE PRO_TRUNCATE_TABLE(TB_NAME VARCHAR2);
  PROCEDURE PRO_TRNCT_PTTN (P_TBL_NAME VARCHAR2,
                            P_RSTMT_IND IN VARCHAR2 DEFAULT 'N',
                            P_PAST_MTH_NUM IN NUMBER DEFAULT 6);

  PROCEDURE PRO_INIT_PRTTN(P_VC2_TBL_NAME IN USER_TABLES.TABLE_NAME%TYPE,
                                 P_RSTMT_IND IN VARCHAR2 DEFAULT 'N',
                                 P_PAST_MTH_NUM IN NUMBER DEFAULT 6,
                                 P_NEXT_MTH_NUM IN NUMBER DEFAULT 1
                                 );
  PROCEDURE PRO_CREAT_MSNG_PRTTN(P_VC2_TBL_NAME IN USER_TABLES.TABLE_NAME%TYPE,
                                 P_PAST_MTH_NUM IN NUMBER DEFAULT 6,
                                 P_NEXT_MTH_NUM IN NUMBER DEFAULT 1
                                 );

  PROCEDURE PRO_EXCHG_PRTTN(P_VC2_SRCE_TBL IN USER_TABLES.TABLE_NAME%TYPE,
                            P_VC2_DEST_TBL IN USER_TABLES.TABLE_NAME%TYPE,
                            P_RSTMT_IND IN VARCHAR2 DEFAULT 'N',
                            P_PAST_MTH_NUM IN NUMBER DEFAULT 6,
							V_REGN VARCHAR2 DEFAULT NULL -- SOX IMPLEMENTATION PROJECT
                           );
  PROCEDURE PRO_LOAD(P_VC2_SRCE_TBL IN USER_TABLES.TABLE_NAME%TYPE,
                     P_VC2_DEST_TBL IN USER_TABLES.TABLE_NAME%TYPE,
                     P_VC2_MTH_QRY_NAME IN VARCHAR2,
                     P_CTRLM_ORD_ID IN VARCHAR2,
                     P_RSTMT_IND IN VARCHAR2 DEFAULT 'N'
                     );
  PROCEDURE PRO_FINAL(P_ETL_RUN_DATE IN DATE);
  PROCEDURE PRO_CHK_SHPMT_ACTL_DATE(P_CURR_DATE IN VARCHAR2 DEFAULT TO_CHAR(SYSDATE));
END OPT_SHPMT_LOAD_PKG;
/

