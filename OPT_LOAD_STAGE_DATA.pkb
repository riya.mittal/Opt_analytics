CREATE OR REPLACE PACKAGE BODY ADWU_OPTIMA_LAEX.OPT_LOAD_STAGE_DATA IS

    /**********************************************************************/
    /* name: pro_put_line                                                 */
    /* purpose: print line of text divided into 255-character chunks      */
    /* parameters:                                                        */
    /*   p_string - string to be printed                                  */
    /* author: bazyli blicharski                                          */
    /* originator: brian beckman                                          */
    /* version: 1.00 - initial version                                    */
    /*          1.01 - string cut according to new line marker            */
    /*          2.00 - Updated by barbara in R10 on 2010/03/12            */
    /*                 Modify pro_put_line in to handle                   */
    /*                 dbms buffer overflow                               */
    /*                 Add a new procedure load_shpmt_data to insert      */
    /*                 shipment data for shipment re-arch                 */
    /*                 Modify procedure load_data and insert_data         */
    /*                 for shipment re-arch                               */
    /**********************************************************************/
    PROCEDURE PRO_PUT_LINE(PI_STR           IN VARCHAR2,
                           PI_LINE_MAX_SIZE IN BINARY_INTEGER DEFAULT 255) IS
      V_INPUT_STR  VARCHAR2(32767);
      V_SUB_STR    VARCHAR2(32767);
      V_OUTPUT_STR VARCHAR2(32767);
      V_LF_POS     NUMBER(5);
      V_SP_POS     NUMBER(5);
      V_COMMA_POS  NUMBER(5);
    BEGIN
      IF PI_LINE_MAX_SIZE < 1 THEN
        RAISE_APPLICATION_ERROR(-20001, 'Line size must be positive.');
      END IF;
      DBMS_OUTPUT.ENABLE(NULL);
      -- replace carriage return with linefeed
      V_INPUT_STR := REPLACE(PI_STR, CHR(13), CHR(10));
      IF V_INPUT_STR IS NOT NULL THEN
        WHILE LENGTH(V_INPUT_STR) > PI_LINE_MAX_SIZE LOOP
          -- get the first position of linefeed
          V_LF_POS := INSTR(V_INPUT_STR, CHR(10), 1, 1);
          IF V_LF_POS > 0 THEN
            V_SUB_STR   := SUBSTR(V_INPUT_STR, 1, V_LF_POS - 1);
            V_INPUT_STR := SUBSTR(V_INPUT_STR, V_LF_POS + 1);
          ELSE
            V_SUB_STR   := SUBSTR(V_INPUT_STR, 1);
            V_INPUT_STR := '';
          END IF;
          -- divide the string to multi lines
          WHILE LENGTH(V_SUB_STR) > PI_LINE_MAX_SIZE LOOP
            V_OUTPUT_STR := SUBSTR(V_SUB_STR, 1, PI_LINE_MAX_SIZE);
            -- get the last space position
            V_SP_POS := INSTR(V_OUTPUT_STR, ' ', -1);
            -- new a line after the last space to linefeed
            IF V_SP_POS = 0 THEN
              -- get the last comma position
              V_COMMA_POS := INSTR(V_OUTPUT_STR, ',', -1);
              -- new a line after the last comma to linefeed
              IF V_COMMA_POS = 0 THEN
                DBMS_OUTPUT.PUT_LINE(V_OUTPUT_STR);
                V_SUB_STR := SUBSTR(V_SUB_STR, PI_LINE_MAX_SIZE + 1);
              ELSE
                DBMS_OUTPUT.PUT_LINE(SUBSTR(V_OUTPUT_STR, 1, V_COMMA_POS));
                V_SUB_STR := SUBSTR(V_SUB_STR, V_COMMA_POS + 1);
              END IF;
            ELSE
              DBMS_OUTPUT.PUT_LINE(SUBSTR(V_OUTPUT_STR, 1, V_SP_POS));
              V_SUB_STR := SUBSTR(V_SUB_STR, V_SP_POS + 1);
            END IF;
          END LOOP;
          DBMS_OUTPUT.PUT_LINE(V_SUB_STR);
        END LOOP;
        DBMS_OUTPUT.PUT_LINE(V_INPUT_STR);
      END IF;
    END;

    /************************************************************************************/
    /* name: drop_table (internal procedure                                             */
    /* purpose: drop table passed in the parameter (if exists)                          */
    /* parameters:                                                                      */
    /*   p_tbl_name - table to be removed                                               */
    /* author: bazyli blicharski                                                        */
    /* version: 1.00 - initial version                                                  */
    /************************************************************************************/
    PROCEDURE DROP_TABLE(P_TBL_NAME IN VARCHAR2) IS
      V_CNT NUMBER;
      V_SQL VARCHAR2(32767);
    BEGIN
      SELECT COUNT(1)
        INTO V_CNT
        FROM USER_TABLES
       WHERE TABLE_NAME = P_TBL_NAME;
      IF V_CNT > 0 THEN
        V_SQL := 'DROP TABLE ' || P_TBL_NAME || ' purge';
        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        EXECUTE IMMEDIATE V_SQL;
        PRO_PUT_LINE('Table dropped.');
      END IF;
    END;

    /**********************************************************************/
    /* name: creat_stage_tbl                                              */
    /* purpose: create temporary table for module                         */
    /* parameters:                                                        */
    /*        p_tbl_name - Table to be processed                          */
    /* author: Martin Que                                                 */
    /* version: 1.00 - initial version                                    */
    /**********************************************************************/
    PROCEDURE CREATE_STAGE_TABLE(P_TBL_NAME     IN VARCHAR2,
                                 P_DOP          IN NUMBER DEFAULT NULL,
                                 P_EXCT_SEQ_NUM IN NUMBER,
                                 V_REGN IN VARCHAR2 DEFAULT NULL) IS
      COUNT_TOTAL    NUMBER(15); -- Statistic total rows
      V_CNT          NUMBER(2);
      V_EXCT_SEQ_NUM NUMBER(3) := P_EXCT_SEQ_NUM;
      L_STEP_INFO    VARCHAR2(32767);
       V_DBLINK        VARCHAR2(40); 
      V_SCHMA         VARCHAR2(40);
      V_TBL_NAME     VARCHAR2(100) := '';
      V_SQL_STMT     VARCHAR2(32767);
      V_SQL          VARCHAR2(32767);
      V_SQL_INDEX    VARCHAR2(3000);
    BEGIN
      IF P_TBL_NAME IS NULL THEN
        PRO_PUT_LINE('Error: wrong input parameters!');
        RAISE_APPLICATION_ERROR(C_WRONG_PARMS_ERROR,
                                'Error: wrong input parameters!');
      END IF;

      --check if temporary tables exist
      SELECT COUNT(1)
        INTO V_CNT
        FROM USER_TABLES
       WHERE TABLE_NAME = P_TBL_NAME;

      --drop temporary tables if exist
      IF V_CNT > 0 THEN
        L_STEP_INFO := 'table exist, drop it immediately....';
        PRO_PUT_LINE(L_STEP_INFO);
        V_SQL := 'drop table ' || P_TBL_NAME || ' purge';
        EXECUTE IMMEDIATE V_SQL;
        PRO_PUT_LINE('drop table ' || P_TBL_NAME || ' successfully.');
      END IF;

      --create temporary tables, the sql statement will extract from --
      --opt_creat_tdads_prc                                          --
      SELECT REPLACE(TBL_NAME, CHR(10), ''), SQL_STMT_TXT
        INTO V_TBL_NAME, V_SQL_STMT
        FROM OPT_LOAD_DATA_PRC
       WHERE REPLACE(TBL_NAME, CHR(10), '') = P_TBL_NAME
         AND EXCT_SEQ_NUM = V_EXCT_SEQ_NUM;

      OPT_AX_TBLSPACE(V_REGN,V_TBLSPACE_NAME,V_INDEX); -- SOX IMPLEMENTATION PROJECT

      -- Add COMPRESS parameter for better performance
      -- Simon 14:36 2009-9-24
      -- Modified by Myra 20090929 in R8. add table storage and tablespace feature
      IF V_TBL_NAME IN ('OPT_PRMTN_PROD_SFCT_2') THEN
        V_SQL := 'create table ' || V_TBL_NAME || CHR(10) || V_TABLE_DOP ||
                 CHR(10) || C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME ||
                 CHR(10) || ' nologging compress as ' || V_SQL_STMT;
        PRO_PUT_LINE(V_SQL);
      ELSE
        /*v_sql:='create table '||v_tbl_name|| CHR(10) ||
        ' '|| V_TBLSPACE_NAME || CHR(10) ||
        ' nologging compress as '||v_sql_stmt;*/
        V_SQL := 'create table ' || V_TBL_NAME || CHR(10) || ' parallel ' ||
                 P_DOP || ' ' || C_FINAL_TBL_STORAGE || ' ' ||
                 V_TBLSPACE_NAME || CHR(10) || ' nologging as ' || V_SQL_STMT;
        PRO_PUT_LINE(V_SQL);
      END IF;
         --Added by Jothi for Argentina DD Creation
       IF P_TBL_NAME = 'OPT_TX_HOLD_AR_BRB_TEMP' THEN
        SELECT DISTINCT PARM_VAL
          INTO V_DBLINK
          FROM OPT_QRY_EXTRN_PARM_PRC
         WHERE QRY_NAME_CODE = 'SIEBEL_DBLINK'
           AND PARM_NAME_CODE = 'SIEBEL_DBLINK';

        SELECT DISTINCT PARM_VAL
          INTO V_SCHMA
          FROM OPT_QRY_EXTRN_PARM_PRC
         WHERE QRY_NAME_CODE = 'SIEBEL_SCHEMA'
           AND PARM_NAME_CODE = 'SIEBEL_SCHEMA';
        
        V_SQL := REPLACE(V_SQL, '$DBLINK$', V_DBLINK);
        V_SQL := REPLACE(V_SQL, '$DBSCHMA$', V_SCHMA);
        
   END IF;
      EXECUTE IMMEDIATE V_SQL;
      COUNT_TOTAL := SQL%ROWCOUNT;
      PRO_PUT_LINE('create table ' || P_TBL_NAME || ' with ' || COUNT_TOTAL ||
                   ' rows successfully');

      --V_SQL := 'grant select on ' || V_TBL_NAME || ' to ' || USER;
      V_SQL := 'grant select on ' || V_TBL_NAME || ' to ' || USER ||','||C_TBL_ROLES;
      PRO_PUT_LINE(V_SQL);
      EXECUTE IMMEDIATE V_SQL;
      PRO_PUT_LINE('Grant ' || V_TBL_NAME ||
                   ' select privilege to ETL user.');
                   
     -- added by Bean, for table statistics accumulation.
      -- skip rebuild bitmap indexes as bitmap indexes will be rebuilt on FCT table.
      IF P_TBL_NAME NOT IN
         ('OPT_PRMTN_PROD_SFCT', 'OPT_PROD_BASLN_SFCT', 'OPT_BRAND_BASLN_SFCT') THEN
        --l_step_info:='Target table '|| p_tbl_name||' is SFCT table, skip rebuild bitmap indexes. as they will be rebuilt on FCT table.';
        --pro_put_line(l_step_info);
        --opt_gather_stats.opt_rebuild_ind(p_tbl_name,null,'Y');
        L_STEP_INFO := 'Rebuild all indexes on table ' || P_TBL_NAME ||
                       ' ...';
        -- Rebuild all indexes on stage table
        -- Simon 13:48 2009-9-24
        PRO_PUT_LINE(L_STEP_INFO);
        OPT_GATHER_STATS.OPT_REBUILD_IND(P_TBL_NAME);
      END IF;

      -- added by Myra in R9 20091113 . create indexes

      CASE
        WHEN P_TBL_NAME = 'OPT_AA_PRMTN_PROD_TDADS' THEN
          -- Index
          V_SQL_INDEX := 'CREATE INDEX OPT_AA_PRMTN_PROD_TDADS_IDX1 ON OPT_AA_PRMTN_PROD_TDADS(PROD_ID, FY_DATE_SKID, BUS_UNIT_ID) NOLOGGING';
          EXECUTE IMMEDIATE V_SQL_INDEX;

          V_SQL_INDEX := 'CREATE INDEX OPT_AA_PRMTN_PROD_TDADS_IDX2 ON OPT_AA_PRMTN_PROD_TDADS(ACCT_ID, BUS_UNIT_ID, PROD_ID, PGM_START_DATE, PGM_END_DATE) NOLOGGING';

        WHEN P_TBL_NAME = 'OPT_AA_ACTVY_PROD_TDADS' THEN
          -- Index
          V_SQL_INDEX := 'CREATE INDEX OPT_AA_ACTVY_PROD_TDADS_IDX1 ON OPT_AA_ACTVY_PROD_TDADS(ACCT_ID,PROD_ID, BUS_UNIT_ID, ACTVY_ID, PRMTN_ID) NOLOGGING';

        WHEN P_TBL_NAME = 'OPT_AA_ACTVY_PROD_PREV_SDIM' THEN
          -- Index
          V_SQL_INDEX := 'CREATE INDEX AA_ACTVY_PROD_PREV_SDIM_IDX1 ON OPT_AA_ACTVY_PROD_PREV_SDIM(PROD_SKID, PROD_ID, FY_DATE_SKID,BUS_UNIT_ID) NOLOGGING';

        WHEN P_TBL_NAME = 'OPT_AA_PROD_HIER_TDADS' THEN
          -- Index
          V_SQL_INDEX := 'CREATE INDEX OPT_AA_PROD_HIER_TDADS_IDX1 ON OPT_AA_PROD_HIER_TDADS(BUS_UNIT_ID,PROD_ID, FY_DATE_SKID) NOLOGGING';

        WHEN P_TBL_NAME = 'OPT_SVP_BASE_TFADS' THEN
          -- Index
          V_SQL_INDEX := 'CREATE INDEX OPT_SVP_BASE_TFADS_IDX1 ON  OPT_SVP_BASE_TFADS(ACCT_ID, PROD_ID, PERD_START_DATE) NOLOGGING';

        WHEN P_TBL_NAME = 'OPT_PRMTN_ITEM_GTIN_TDADS' THEN
          -- Index
          V_SQL_INDEX := 'CREATE INDEX PRMTN_ITEM_GTIN_TDADS_IDX1 ON OPT_PRMTN_ITEM_GTIN_TDADS(PRMTN_ID,PROD_ITEM_GTIN)';

        WHEN P_TBL_NAME = 'OPT_SVP_INCRM_TFADS' THEN
          -- Index
          V_SQL_INDEX := 'CREATE INDEX OPT_SVP_INCRM_TFADS_IDX1 ON OPT_SVP_INCRM_TFADS(ACCT_ID, PROD_ID, PERD_START_DATE) NOLOGGING';

      /*WHEN v_tbl_name = 'OPT_AA_PRMTN_GTIN_TFADS' THEN
              v_sql_index := 'SELECT SYSDATE FROM DUAL';*/

        ELSE
          V_SQL_INDEX := 'SELECT SYSDATE FROM DUAL';
      END CASE;

      PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                   REPLACE(V_SQL_INDEX, CHR(10), CHR(10) || '> '));
      PRO_PUT_LINE('Create Index (BEGIN)');
      EXECUTE IMMEDIATE V_SQL_INDEX;
      PRO_PUT_LINE('Create Index (END)');
      -- end by Myra

      -- Changed to use Optima Package for statistics gathering
      -- Skip OPT_PRMTN_PROD_SFCT as it's just used for partition exchange with OPT_PRMTN_PROD_FCT
      -- Simon 13:20 2009-9-28
      IF P_TBL_NAME NOT IN
         ('OPT_PRMTN_PROD_SFCT', 'OPT_PROD_BASLN_SFCT', 'OPT_BRAND_BASLN_SFCT',
          'OPT_ACTVY_SPNDG_MTH_SFCT', 'OPT_FUND_GEN_SPNDG_SFCT') THEN
        -- gather statistics for all involved partitions
        PRO_PUT_LINE('Starting gathering statistics on ' || P_TBL_NAME ||
                     ' table...');
        OPT_GATHER_STATS.OPTIMA_STATS_TBL(P_TBL_NAME);
      END IF;

    EXCEPTION
      WHEN OTHERS THEN
        PRO_PUT_LINE('Error: Creation of stage table ' || P_TBL_NAME ||
                     ' failed with error - ' || SQLERRM);
        RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                'Error: Creation of stage table ' ||
                                P_TBL_NAME || ' failed with error - ' ||
                                SQLERRM);

    END;

    /**********************************************************************/
    /* name: insert_data                                                  */
    /* purpose: insert data to table                                      */
    /* parameters:                                                        */
    /*        p_tbl_name - Table to be processed                          */
    /* author: Martin Que                                                 */
    /* version: 1.00 - initial version                                    */
    /**********************************************************************/
    PROCEDURE INSERT_DATA(P_TBL_NAME     IN VARCHAR2,
                          V_DOP          IN NUMBER DEFAULT NULL,
                          P_EXCT_SEQ_NUM IN NUMBER,
                          P_TRNCT_IND    IN VARCHAR2 DEFAULT 'Y',
                          V_PRTTN_ARY    IN PARRAY DEFAULT NULL,
						  V_REGN VARCHAR2 DEFAULT NULL) IS -- SOX IMPLEMENTATION PROJECT
      COUNT_TOTAL     NUMBER(15);
      V_EXCT_SEQ_NUM  NUMBER(3) := P_EXCT_SEQ_NUM;
      V_COLS          VARCHAR2(32767) := '';
      V_SQL_STMT      VARCHAR2(32767);
      V_SQL           VARCHAR2(32767);
      L_STEP_INFO     VARCHAR2(32767);
      V_DBLINK        VARCHAR2(40);
      V_SCHMA         VARCHAR2(40);
      V_PARM_MTH      VARCHAR2(10);
      p_user         varchar2(30); --Added by Jothi for PR# PRB0043054 CR# CHG0045328 to avoid the long running issue in KH5-A2 job on 14th July 2017
      V_DML_PARAL_IND OPT_LOAD_DATA_PRC.DML_PARAL_IND%TYPE;
    --add a variable to receive the value of PRCSS_NAME column by Daniel, 2011-July-29,Begin
      V_PRCSS_NAME    OPT_LOAD_DATA_PRC.PRCSS_NAME%TYPE;
    --add a variable to receive the value of PRCSS_NAME column by Daniel, 2011-July-29,End
    BEGIN
      IF P_TBL_NAME IS NULL THEN
        PRO_PUT_LINE('Error: wrong input parameters!');
        RAISE_APPLICATION_ERROR(C_WRONG_PARMS_ERROR,
                                'Error: wrong input parameters!');
      END IF;
      --retrive columns of the source table
      FOR C IN (SELECT COLUMN_NAME
                  FROM USER_TAB_COLUMNS
                 WHERE TABLE_NAME = P_TBL_NAME
                 ORDER BY COLUMN_ID) LOOP
        IF V_COLS IS NOT NULL THEN
          V_COLS := V_COLS || ',';
        END IF;
        V_COLS := V_COLS || C.COLUMN_NAME;
      END LOOP;

      --truncate table
      -- Modified by Myra 20090928, add 'reuse storage' for improve performance
      -- Modified by Bean 20091124. Fixing bug for follow two tables were truncate twice.
      -- Modified by David 2010-02-02, it is for Fund re-arch in Optima10
      -- Optima11,CR923,22-Nov-2010,Yves begin
      -- Optima11.1, BIP-Wv3-1, 07-Jul-2011, by Kingham to add parameter P_TRNCT_IND to indicate whether it needs to truncate table before insert data

      DBMS_OUTPUT.PUT_LINE('P_TRNCT_IND IS '||P_TRNCT_IND);
      IF P_TRNCT_IND = 'Y' THEN

        IF ((P_TBL_NAME = 'OPT_FUND_HIST_IFCT' AND P_EXCT_SEQ_NUM = 1)) THEN
          -- Optima11,CR923,22-Nov-2010,Yves end
          PRO_PUT_LINE('The Table ' || P_TBL_NAME ||
                       ' Does not need truncate at this step');
        ELSE
          L_STEP_INFO := 'truncate table ' || P_TBL_NAME || '...';
          PRO_PUT_LINE(L_STEP_INFO);
          --add reuse storage option after truncate statement by Leo 12/23/2010 -  begin
          V_SQL := 'truncate table ' || P_TBL_NAME || ' reuse storage';
          --add reuse storage option after truncate statement by Leo 12/23/2010 -  end
          EXECUTE IMMEDIATE V_SQL;
          PRO_PUT_LINE(V_SQL || ' successfully.');
        END IF;
      END IF;

      -- Alter all indexes on stage table unusable
      -- Simon 13:48 2009-9-24
      L_STEP_INFO := 'Alter all indexes on table ' || P_TBL_NAME ||
                     ' unusable...';
      PRO_PUT_LINE(L_STEP_INFO);
      OPT_GATHER_STATS.OPT_UNUSABLE_IND(P_TBL_NAME, 'Y');

      --insert data into table
      --add PRCSS_NAME here,selest PRCSS_NAME's value into V_PRCSS_NAME,Dniel,2011-July-29,Begin
      SELECT SQL_STMT_TXT, DML_PARAL_IND,PRCSS_NAME
        INTO V_SQL_STMT, V_DML_PARAL_IND,V_PRCSS_NAME
      --add PRCSS_NAME here,selest PRCSS_NAME's value into V_PRCSS_NAME,Dniel,2011-July-29,End
        FROM OPT_LOAD_DATA_PRC
       WHERE REPLACE(TBL_NAME, CHR(10), '') = P_TBL_NAME
         AND EXCT_SEQ_NUM = V_EXCT_SEQ_NUM;

      --Added by Barbara 20100211 for shipment  processing
      IF P_TBL_NAME = 'OPT_SHPMT_FULL_SFCT' OR P_TBL_NAME = 'OPT_DISP_PLAN_CMPT_SDIM' THEN
        SELECT DISTINCT PARM_VAL
          INTO V_DBLINK
          FROM OPT_QRY_EXTRN_PARM_PRC
         WHERE QRY_NAME_CODE = 'SIEBEL_DBLINK'
           AND PARM_NAME_CODE = 'SIEBEL_DBLINK';

        SELECT DISTINCT PARM_VAL
          INTO V_SCHMA
          FROM OPT_QRY_EXTRN_PARM_PRC
         WHERE QRY_NAME_CODE = 'SIEBEL_SCHEMA'
           AND PARM_NAME_CODE = 'SIEBEL_SCHEMA';

        IF V_EXCT_SEQ_NUM = 1 THEN
          --Monthly Processing
          SELECT PARM_VAL
            INTO V_PARM_MTH
            FROM OPT_QRY_EXTRN_PARM_PRC
           WHERE QRY_NAME_CODE = 'OPT_SHPMT_FCT_MLY'
             AND PARM_NAME_CODE = 'PRCSS_MTH';
        ELSIF V_EXCT_SEQ_NUM = 2 THEN
          --Weekly Processing
          SELECT PARM_VAL
            INTO V_PARM_MTH
            FROM OPT_QRY_EXTRN_PARM_PRC
           WHERE QRY_NAME_CODE = 'OPT_SHPMT_FCT_WLY'
             AND PARM_NAME_CODE = 'PRCSS_MTH';
        END IF;
        V_SQL_STMT := REPLACE(V_SQL_STMT, '$MONTH$', V_PARM_MTH);
        V_SQL_STMT := REPLACE(V_SQL_STMT, '$DBLINK$', V_DBLINK);
        V_SQL_STMT := REPLACE(V_SQL_STMT, '$DBSCHMA$', V_SCHMA);

      END IF;

      --Optima11, BIT F02, 05-May-2011, Daniel, enable parallel DML if job pdml_ind is Y,Begin
      --IF P_TBL_NAME IN ('OPT_PROD_BASLN_SFCT','OPT_PRMTN_GTIN_MTH_SFCT','OPT_PRMTN_PROD_ESTMT_SFCT') THEN
      IF V_DML_PARAL_IND = 'Y' THEN
        EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DML';
      END IF;
      --Optima11, BIT F02, 05-May-2011, Daniel, enable parallel DML if job pdml_ind is Y,End

      /* Start Added by HP for PR#PM00019779 - SVP AA Elimination*/
      IF P_TBL_NAME = 'OPT_ACTVY_GTIN_FCT'
      THEN
        PRO_PUT_LINE('Starting Gathering statistics on tables whose Jobs are disabled');
        OPT_GATHER_STATS.OPTIMA_STATS_TBL('OPT_AA_PROD_SDIM');
        OPT_GATHER_STATS.OPTIMA_STATS_TBL('OPT_AA_PRMTN_MTH_SFCT');
        OPT_GATHER_STATS.OPTIMA_STATS_TBL('OPT_AA_PROD_MTH_SFCT');
        OPT_GATHER_STATS.OPTIMA_STATS_TBL('OPT_DCMPD_PROD_DIM');
        OPT_GATHER_STATS.OPTIMA_STATS_TBL('OPT_SVP_AA_ACTVY_GTIN_FCT');
      END IF;
      /* End Added by HP for PR#PM00019779 - SVP AA Elimination*/

      --manually extend workarea size by leo for OPT_ACTVY_VAR_COST_TFADS md on Jan 19, 2011 begin
      --remove current workaround for dpwt issue after oracle patch applied by Leo on Feb 14, 2011 - begin
      /* IF P_TBL_NAME = 'OPT_ACTVY_PROD_GTIN_SFCT' OR
         P_TBL_NAME = 'OPT_ACTVY_GTIN_ESTMT_SFCT' OR
         P_TBL_NAME = 'OPT_ACTVY_GTIN_REVSD_SFCT' OR
         P_TBL_NAME = 'OPT_ACTVY_GTIN_ACTL_SFCT' THEN
        EXECUTE IMMEDIATE 'ALTER SESSION SET workarea_size_policy = manual';
        EXECUTE IMMEDIATE 'ALTER SESSION SET sort_area_size = 2147483647';
        EXECUTE IMMEDIATE 'ALTER SESSION SET hash_area_size = 2147483647';
      END IF; */
      --remove current workaround for dpwt issue after oracle patch applied by Leo on Feb 14, 2011 - end
      --manually extend workarea size by leo for OPT_ACTVY_VAR_COST_TFADS md on Jan 19, 2011 end

      -- Add hint APPEND to enable batch insert
      -- Simon 15:56 2009-9-24
      -- Modified by Myra 20090929 in R9. add 'parallel dop' for tuning
      -- modified by Leo 20110211 in R11. move the parallel hint to select subsection - begin

      --Optima11, BIT Performance Improvement, 14-Mar-2011, Daniel, add insert dop and remove the select dop,Begin
       
      --Added by Jothi for PR# PRB0043054 CR# CHG0045328 to avoid the long running issue in KH5-A2 job on 14th July 2017(begin) 

      select username into p_user from user_users where rownum = 1;
      IF upper(P_TBL_NAME) ='OPT_PRMTN_BRAND_MTH_SFCT' THEN
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') || ' Gather stats on OPT_PRMTN_DIM table (Begin)');
        DBMS_STATS.GATHER_TABLE_STATS (p_user,'OPT_PRMTN_DIM', estimate_percent=>DBMS_STATS.AUTO_SAMPLE_SIZE,METHOD_OPT =>'FOR ALL COLUMNS SIZE AUTO',degree=>4,granularity=>'ALL',cascade=>TRUE);
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') || ' Gather stats on OPT_PRMTN_DIM table (End)');
      END IF;
      
      --Added by Jothi for PR# PRB0043054 CR# CHG0045328 to avoid the long running issue in KH5-A2 job on 14th July 2017(End) 
      
      V_SQL := 'insert /*+ APPEND parallel(trgt, ' || V_DOP || ') */  into ' ||
               P_TBL_NAME || ' trgt select ' || V_COLS || CHR(10) || 'from (' ||
               V_SQL_STMT || ')';
      --Optima11, BIT Performance Improvement, 14-Mar-2011, Daniel, add insert dop and remove the select dop,End

      -- modified by Leo 20110211 in R11. move the parallel hint to select subsection - end
      PRO_PUT_LINE(V_SQL);
      EXECUTE IMMEDIATE V_SQL;
      COUNT_TOTAL := SQL%ROWCOUNT;
      COMMIT;
      PRO_PUT_LINE('insert data into table ' || P_TBL_NAME || ' with ' ||
                   COUNT_TOTAL || ' rows successfully.');

      --Optima11, BIT F02, 05-May-2011, Daniel, enable parallel DML if job pdml_ind is Y,Begin
      IF V_DML_PARAL_IND = 'Y' THEN
        EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DML';
      END IF;
      --Optima11, BIT F02, 05-May-2011, Daniel, enable parallel DML if job pdml_ind is Y,End

      -- Modified by Myra 20091015. if table is stage table like SFCT,
      -- skip rebuild bitmap indexes as bitmap indexes will be rebuilt on FCT table.
      IF P_TBL_NAME NOT IN
         ('OPT_PRMTN_PROD_SFCT', 'OPT_PROD_BASLN_SFCT', 'OPT_BRAND_BASLN_SFCT',
          'OPT_SHPMT_FULL_SFCT', 'OPT_ACTVY_SFCT') THEN
        --l_step_info:='Target table '|| p_tbl_name||' is SFCT table, skip rebuild bitmap indexes. as they will be rebuilt on FCT table.';
        --pro_put_line(l_step_info);
        --opt_gather_stats.opt_rebuild_ind(p_tbl_name,null,'Y');
        L_STEP_INFO := 'Rebuild all indexes on table ' || P_TBL_NAME ||
                       ' ...';
        -- Rebuild all indexes on stage table
        -- Simon 13:48 2009-9-24
        PRO_PUT_LINE(L_STEP_INFO);
        --Optima QMR 11.1,29-Jul-2011, BIP-Wv3-1, Daniel.Qin, use opt_tool.RECREATE_TABLE_INDEX for all P type prcss fct table, Begin
        IF V_PRCSS_NAME = 'P' THEN
          OPT_TOOL.RECREATE_TABLE_INDEX(P_TBL_NAME,V_REGN);--SOX IMPLEMENTATION PROJECT
        ELSE
          OPT_GATHER_STATS.OPT_REBUILD_IND(P_TBL_NAME);
        END IF;
        --Optima QMR 11.1,29-Jul-2011, BIP-Wv3-1, Daniel.Qin, use opt_tool.RECREATE_TABLE_INDEX for all P type prcss fct table, End
      END IF;

      -- Changed to use Optima Package for statistics gathering
      -- Skip OPT_ACTVY_SFCT,
      --      OPT_PRMTN_PROD_SFCT,
      --      OPT_PROD_BASLN_SFCT,
      --      OPT_BRAND_BASLN_SFCT,
      --      OPT_FUND_GEN_SPNDG_FY_SFCT 5 tables' gather as it's just used for partition exchange with FCT table
      -- Daniel.Qin,OPTIMA11, BIP-wv3-1, 24-Aug-2011
      IF P_TBL_NAME NOT IN ('OPT_ACTVY_SFCT','OPT_PRMTN_PROD_SFCT',
                            'OPT_PROD_BASLN_SFCT','OPT_BRAND_BASLN_SFCT',
                            'OPT_FUND_GEN_SPNDG_FY_SFCT'
      ) THEN
        -- gather statistics for all involved partitions
        PRO_PUT_LINE('Starting gathering statistics on ' || P_TBL_NAME ||
                     ' table...');
        --Optima QMR 11.1,24-Aug-2011, BIP-Wv3-1, Daniel.Qin,Only gather partitions which have been truncated for 8 tables below,Begin
        IF P_TBL_NAME in ('OPT_PRMTN_PYMT_FCT','OPT_FUND_ACCRL_FCT','OPT_PRMTN_PROD_BASLN_SFCT',
                          'OPT_PRMTN_BRAND_MTH_SFCT','OPT_PRMTN_GTIN_MTH_SFCT','OPT_PRMTN_PYMT_LINE_FCT',
                          'OPT_PRMTN_PROD_ESTMT_SFCT','OPT_PRMTN_PROD_ACTL_SFCT')
           AND V_PRTTN_ARY IS NOT NULL
           AND V_PRTTN_ARY.COUNT > 0
        THEN
          FOR pi IN V_PRTTN_ARY.FIRST..V_PRTTN_ARY.LAST
          LOOP
            OPT_GATHER_STATS.OPTIMA_STATS_PTBL(P_TBL_NAME,V_PRTTN_ARY(pi),4);
          END LOOP;
        --Optima QMR 11.1,24-Aug-2011, BIP-Wv3-1, Daniel.Qin,Only gather partitions which have been truncated for 8 tables above,End
        ELSE
          OPT_GATHER_STATS.OPTIMA_STATS_TBL(P_TBL_NAME);
        END IF;
      END IF;

    EXCEPTION
      WHEN OTHERS THEN
        PRO_PUT_LINE('Error: insert into table ' || P_TBL_NAME ||
                     ' failed with error - ' || SQLERRM);
        RAISE_APPLICATION_ERROR(C_TBL_INSRT_ERROR,
                                'Error: insert into table ' || P_TBL_NAME ||
                                ' failed with error - ' || SQLERRM);
    END;

    /************************************************************************************/
    /* name: insert_dummy_product_data                                                   */
    /* purpose: Populate dummy product to related product tables                        */
    /* author:  Yves Chen                                                               */
    /* version: 1.00 - initial version                                                  */
    /***********************************************************************************/
    PROCEDURE INSERT_DUMMY_PRODUCT_DATA IS
      V_SQL_STMT  VARCHAR2(32767);
      COUNT_TOTAL NUMBER(15);
      V_TBL_NAME  VARCHAR2(40);
    BEGIN
      -----------------------------------------------------------------
      ---STEP 010: insert data to S_CTLG_PLC
      -----------------------------------------------------------------
      V_TBL_NAME := 'S_CTLG_PLC';
      V_SQL_STMT := 'INSERT INTO S_CTLG_PLC
                                   (ROW_ID,
                                    ACTIVE_FLG,
                                    CTLG_TYPE_CD,
                                    NAME,
                                    EFF_END_DT,
                                    EFF_START_DT,
                                    PERIOD_ID)
                                 VALUES
                                   (''DUMMY_CTLG'',
                                    ''Y'',
                                    ''Product Hierarchy'',
                                    ''Unknown Product Catalog'',
                                    TO_DATE(''31-12-9999'', ''dd-mm-yyyy''),
                                    TO_DATE(''01-01-1900'', ''dd-mm-yyyy''),
                                    NULL)';

      EXECUTE IMMEDIATE V_SQL_STMT;
      COUNT_TOTAL := SQL%ROWCOUNT;
      DBMS_OUTPUT.PUT_LINE(CHR(10) ||
                           TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS  ') ||
                           'S_CTLG_PLC ' || COUNT_TOTAL || ' rows inserted.' ||
                           CHR(10));
      COMMIT;

      -----------------------------------------------------------------
      ---STEP 020: insert data to OPT_PROD_FDIM
      -----------------------------------------------------------------
      V_TBL_NAME := 'OPT_PROD_FDIM';
      V_SQL_STMT := 'insert into OPT_PROD_FDIM
                                   select PROD_SKID,
                                          PROD_EFF_DATE,
                                          PROD_END_DATE,
                                          PROD_ID,
                                          PROD_ACTIV_IND,
                                          PROD_DESC,
                                          PROD_NAME,
                                          GIV_CONV_FACTR,
                                          SU_FACTR,
                                          PROD_LVL_DESC,
                                          STTUS_CODE,
                                          CURR_IND,
                                          EFF_END_DATE,
                                          EFF_START_DATE,
                                          GTIN,
                                          BUS_UNIT_SKID,
                                          ETL_RUN_ID,
                                          PROD_CODE,
                                          ACTIV_IND,
                                          BRAND_SPLIT_NUM,
                                          CREAT_DATE,
                                          BUOM_IND,
                                          BRAND_ID,
                                          BUOM_CU_CONV_FACTR,
                                          CU_GTIN,
                                          CU_GTIN_SKID,
                                          CU_GTIN_IND,
                                          CMPLX_GTIN_IND,
                                          VENDR_FPC_CODE
                                     from OPT_PROD_DUMMY_FDIM';
      EXECUTE IMMEDIATE V_SQL_STMT;

      COUNT_TOTAL := SQL%ROWCOUNT;
      DBMS_OUTPUT.PUT_LINE(CHR(10) ||
                           TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS  ') ||
                           'OPT_PROD_FDIM ' || COUNT_TOTAL ||
                           ' rows inserted.' || CHR(10));
      COMMIT;

      -------------------------------------------------------------------
      ---STEP 030: insert data to OPT_PROD_DIM
      -------------------------------------------------------------------
      V_TBL_NAME := 'OPT_PROD_DIM';
      V_SQL_STMT := 'insert into OPT_PROD_DIM
                                     select PROD_SKID,
                                 PROD_EFF_DATE,
                                 ACCT_SKID,
                                 ACTIV_IND,
                                 BUOM_IND,
                                 BRAND_ID,
                                 BRAND_NAME,
                                 BRAND_SPLIT_NUM,
                                 BUS_UNIT_SKID,
                                 CREAT_BY_NAME,
                                 CREAT_DATE,
                                 CURR_IND,
                                 ETL_RUN_ID,
                                 EFF_END_DATE,
                                 EFF_START_DATE,
                                 FLD_RPLCBL_UNIT_IND,
                                 GIV_CONV_FACTR,
                                 GTIN,
                                 GLOBL_TRADE_ITEM_NUM,
                                 ITEM_SIZE_NUM,
                                 LEAD_TIME_NUM,
                                 MAKE_CODE,
                                 MEAN_TIME_BTWN_FAIL_NUM,
                                 MEAN_TIME_TO_REPR_NUM,
                                 NAME_1,
                                 NAME_2,
                                 NAME_3,
                                 ONE_TO_ONE_PROD_NAME,
                                 PARNT_PROD_ID,
                                 PART_NUM,
                                 PRED_GTIN,
                                 PRICE_LIST_DESC,
                                 PROD_ACTIV_IND,
                                 PROD_CATEG_DESC,
                                 PROD_CATEG_NAME,
                                 PROD_CODE,
                                 PROD_DESC,
                                 PROD_END_DATE,
                                 PROD_GLOBL_UID,
                                 PROD_ID,
                                 PROD_LVL_DESC,
                                 PROD_NAME,
                                 PROD_TYPE_CODE,
                                 REGN_CODE,
                                 RET_DFCTV_IND,
                                 SDC_ACTIV_IND,
                                 SALES_PROD_IND,
                                 SALES_SERV_IND,
                                 SERL_IND,
                                 SHORT_BRAND_NAME,
                                 STTUS_CODE,
                                 SU_FACTR,
                                 UOM_ID,
                                 VENDR_PART_NUM,
                                 ACTL_PARNT_PROD_ID,
                                 BUOM_CU_CONV_FACTR,
                                 CU_GTIN,
                                 CU_GTIN_SKID,
                                 CU_GTIN_IND,
                                 CMPLX_GTIN_IND,
                                 VENDR_FPC_CODE
                            from OPT_PROD_DUMMY_DIM_VW';
      EXECUTE IMMEDIATE V_SQL_STMT;

      COUNT_TOTAL := SQL%ROWCOUNT;
      DBMS_OUTPUT.PUT_LINE(CHR(10) ||
                           TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS  ') ||
                           'OPT_PROD_DIM ' || COUNT_TOTAL ||
                           ' rows inserted.' || CHR(10));
      COMMIT;

      -------------------------------------------------------------------
      ---STEP 040: insert data to OPT_PROD_ASSOC_ESI
      -------------------------------------------------------------------
      V_TBL_NAME := 'OPT_PROD_ASSOC_ESI';
      V_SQL_STMT := 'insert into OPT_PROD_ASSOC_ESI
                                   select PROD_SKID,
                                          CTLG_ID,
                                          BUS_UNIT_SKID,
                                          PROD_ID,
                                          PARNT_PROD_SKID,
                                          PARNT_PROD_ID,
                                          ACTVY_CTLG_IND,
                                          PROD_LVL_NUM,
                                          PROD_LVL_DESC,
                                          HDN_IND
                                     from OPT_PROD_DUMMY_ASSOC_ESI';

      EXECUTE IMMEDIATE V_SQL_STMT;

      COUNT_TOTAL := SQL%ROWCOUNT;
      DBMS_OUTPUT.PUT_LINE(CHR(10) ||
                           TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS  ') ||
                           'OPT_PROD_ASSOC_ESI ' || COUNT_TOTAL ||
                           ' rows inserted.' || CHR(10));
      COMMIT;

      -------------------------------------------------------------------
      ---STEP 050: insert data to OPT_FUND_PROD_FDIM
      -------------------------------------------------------------------
      V_TBL_NAME := 'OPT_FUND_PROD_FDIM';
      V_SQL_STMT := 'INSERT INTO OPT_FUND_PROD_FDIM
                                    SELECT FUND_PROD_SKID,
                                           FUND_PROD_EFF_DATE,
                                           FUND_PROD_END_DATE,
                                           FUND_TRGT_FLG,
                                           BDF_FUND_RATE_NUM,
                                           FUND_GRP_ID,
                                           FUND_GRP_SKID,
                                           PROD_ID,
                                           PROD_SKID,
                                           PROD_NAME,
                                           PROD_DESC,
                                           PROD_LVL_DESC,
                                           PROD_START_DATE,
                                           PROD_END_DATE,
                                           CURR_IND,
                                           ETL_RUN_ID,
                                           REGN_CODE,
                                           BUS_UNIT_SKID,
                                           ROOF_RATE_NUM,
                                           OVLAP_IND,
                                           FY_DATE_SKID,
                                           CMMNT_DESC,
                                           FUND_PROD_ID
                                      FROM OPT_FUND_PROD_DUMMY_FDIM';
      EXECUTE IMMEDIATE V_SQL_STMT;

      COUNT_TOTAL := SQL%ROWCOUNT;
      DBMS_OUTPUT.PUT_LINE(CHR(10) ||
                           TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS  ') ||
                           'OPT_FUND_PROD_FDIM ' || COUNT_TOTAL ||
                           ' rows inserted.' || CHR(10));
      COMMIT;

      -------------------------------------------------------------------
      ---STEP 060: insert data to OPT_FUND_PROD_DIM
      -------------------------------------------------------------------
      V_TBL_NAME := 'OPT_FUND_PROD_DIM';
      V_SQL_STMT := 'insert into OPT_FUND_PROD_DIM
                                      SELECT FUND_PROD_SKID,
                                   FUND_PROD_EFF_DATE,
                                   PROD_SKID,
                                   BDF_FUND_RATE_NUM,
                                   BUS_UNIT_SKID,
                                   CURR_IND,
                                   ETL_RUN_ID,
                                   FUND_GRP_ID,
                                   FUND_GRP_SKID,
                                   FUND_PROD_END_DATE,
                                   FUND_PROD_ID,
                                   FUND_TRGT_FLG,
                                   PROD_DESC,
                                   PROD_END_DATE,
                                   PROD_ID,
                                   PROD_LVL_DESC,
                                   PROD_NAME,
                                   PROD_START_DATE,
                                   REGN_CODE,
                                   ROOF_RATE_NUM,
                                   OVLAP_IND,
                                   FY_DATE_SKID,
                                   CMMNT_DESC
                                     from OPT_FUND_PROD_DUMMY_FDIM';

      EXECUTE IMMEDIATE V_SQL_STMT;

      COUNT_TOTAL := SQL%ROWCOUNT;
      DBMS_OUTPUT.PUT_LINE(CHR(10) ||
                           TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS  ') ||
                           'OPT_FUND_PROD_DIM ' || COUNT_TOTAL ||
                           ' rows inserted.' || CHR(10));
      COMMIT;

    EXCEPTION
      WHEN OTHERS THEN
        PRO_PUT_LINE('Error: insert dummy products to table ' || V_TBL_NAME ||
                     ' failed with error - ' || SQLERRM);
        RAISE_APPLICATION_ERROR(C_TBL_DELETE_ERROR,
                                'Error: insert dummy products to table ' ||
                                V_TBL_NAME || ' failed with error - ' ||
                                SQLERRM);

    END;

    /**********************************************************************/
    /* name: delete_data                                                  */
    /* purpose: insert data to table                                      */
    /* parameters:                                                        */
    /*        p_tbl_name - Table to be processed                          */
    /* author:  Bean,Zhu                                                  */
    /* version: 1.00 - initial version                                    */
    /**********************************************************************/
    PROCEDURE DELETE_DATA(P_TBL_NAME     IN VARCHAR2,
                          V_DOP          IN NUMBER DEFAULT NULL,
                          P_EXCT_SEQ_NUM IN NUMBER) IS
      COUNT_TOTAL    NUMBER(15);
      V_EXCT_SEQ_NUM NUMBER(3) := P_EXCT_SEQ_NUM;
      --v_cols             VARCHAR2(32767):='';
      V_SQL_STMT  VARCHAR2(32767);
      V_SQL       VARCHAR2(32767);
      L_STEP_INFO VARCHAR2(32767);
    BEGIN
      IF P_TBL_NAME IS NULL THEN
        PRO_PUT_LINE('Error: wrong input parameters!');
        RAISE_APPLICATION_ERROR(C_WRONG_PARMS_ERROR,
                                'Error: wrong input parameters!');
      END IF;

      SELECT SQL_STMT_TXT
        INTO V_SQL_STMT
        FROM OPT_LOAD_DATA_PRC
       WHERE REPLACE(TBL_NAME, CHR(10), '') = P_TBL_NAME
         AND EXCT_SEQ_NUM = V_EXCT_SEQ_NUM;

      L_STEP_INFO := 'Delete table ' || P_TBL_NAME || ' by where condition ' ||
                     CHR(10) || V_SQL_STMT;
      PRO_PUT_LINE(L_STEP_INFO);
      V_SQL := 'Delete /*+ PARALLEL(' || V_DOP || ') */ from ' || P_TBL_NAME ||
               ' where ' || V_SQL_STMT;
      PRO_PUT_LINE(V_SQL);
      EXECUTE IMMEDIATE V_SQL;
      COUNT_TOTAL := SQL%ROWCOUNT;
      COMMIT;

      /*
      -- Alter all indexes on stage table unusable
      -- Simon 13:48 2009-9-24
      l_step_info:='Alter all indexes on table '||p_tbl_name||' unusable...';
      pro_put_line(l_step_info);
      opt_gather_stats.opt_unusable_ind(p_tbl_name, 'Y');*/

      PRO_PUT_LINE('Delete data from table ' || P_TBL_NAME || ' with ' ||
                   COUNT_TOTAL || ' rows successfully.');

      -- Modified by Myra 20091015. if table is stage table like SFCT,
      -- skip rebuild bitmap indexes as bitmap indexes will be rebuilt on FCT table.
      IF P_TBL_NAME NOT IN
         ('OPT_PRMTN_PROD_SFCT', 'OPT_PROD_BASLN_SFCT', 'OPT_BRAND_BASLN_SFCT') THEN
        --l_step_info:='Target table '|| p_tbl_name||' is SFCT table, skip rebuild bitmap indexes. as they will be rebuilt on FCT table.';
        --pro_put_line(l_step_info);
        --opt_gather_stats.opt_rebuild_ind(p_tbl_name,null,'Y');
        L_STEP_INFO := 'Rebuild all indexes on table ' || P_TBL_NAME ||
                       ' ...';
        -- Rebuild all indexes on stage table
        -- Simon 13:48 2009-9-24
        PRO_PUT_LINE(L_STEP_INFO);
        OPT_GATHER_STATS.OPT_REBUILD_IND(P_TBL_NAME);
      END IF;

      -- Changed to use Optima Package for statistics gathering
      -- Skip OPT_PRMTN_PROD_SFCT as it's just used for partition exchange with OPT_PRMTN_PROD_FCT
      -- Simon 13:20 2009-9-28
      IF P_TBL_NAME NOT IN
         ('OPT_PRMTN_PROD_SFCT', 'OPT_PROD_BASLN_SFCT', 'OPT_BRAND_BASLN_SFCT') THEN
        -- gather statistics for all involved partitions
        PRO_PUT_LINE('Starting gathering statistics on ' || P_TBL_NAME ||
                     ' table...');
        OPT_GATHER_STATS.OPTIMA_STATS_TBL(P_TBL_NAME);
      END IF;

    EXCEPTION
      WHEN OTHERS THEN
        PRO_PUT_LINE('Error: Delete from table ' || P_TBL_NAME ||
                     ' failed with error - ' || SQLERRM);
        RAISE_APPLICATION_ERROR(C_TBL_DELETE_ERROR,
                                'Error: Delete from table ' || P_TBL_NAME ||
                                ' failed with error - ' || SQLERRM);
    END;

    /************************************************************************************/
    /* name: delete_dummy_product_data                                                  */
    /* purpose: Remove dummy product from related product tables                        */
    /* author:  Yves Chen                                                               */
    /* version: 1.00 - initial version                                                  */
    /***********************************************************************************/
    PROCEDURE DELETE_DUMMY_PRODUCT_DATA IS
      V_SQL_STMT  VARCHAR2(32767);
      COUNT_TOTAL NUMBER(15);
      V_TBL_NAME  VARCHAR2(40);
    BEGIN
      -----------------------------------------------------------------
      ---STEP 010: remove data from S_CTLG_PLC
      -----------------------------------------------------------------
      V_TBL_NAME := 'S_CTLG_PLC';
      V_SQL_STMT := 'DELETE FROM S_CTLG_PLC WHERE ROW_ID = ''DUMMY_CTLG''';

      EXECUTE IMMEDIATE V_SQL_STMT;
      COUNT_TOTAL := SQL%ROWCOUNT;
      DBMS_OUTPUT.PUT_LINE(CHR(10) ||
                           TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS  ') ||
                           'S_CTLG_PLC ' || COUNT_TOTAL || ' rows removed.' ||
                           CHR(10));
      COMMIT;

      -----------------------------------------------------------------
      ---STEP 020: remove data from OPT_PROD_FDIM
      -----------------------------------------------------------------
      V_TBL_NAME := 'OPT_PROD_FDIM';
      V_SQL_STMT := 'DELETE FROM OPT_PROD_FDIM WHERE prod_skid > 999999999000000';

      EXECUTE IMMEDIATE V_SQL_STMT;
      COUNT_TOTAL := SQL%ROWCOUNT;
      DBMS_OUTPUT.PUT_LINE(CHR(10) ||
                           TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS  ') ||
                           'OPT_PROD_FDIM ' || COUNT_TOTAL ||
                           ' rows removed.' || CHR(10));
      COMMIT;

      -----------------------------------------------------------------
      ---STEP 030: remove data from OPT_PROD_FDIM
      -----------------------------------------------------------------
      V_TBL_NAME := 'OPT_PROD_DIM';
      V_SQL_STMT := 'DELETE FROM OPT_PROD_DIM WHERE prod_skid > 999999999000000';

      EXECUTE IMMEDIATE V_SQL_STMT;
      COUNT_TOTAL := SQL%ROWCOUNT;
      DBMS_OUTPUT.PUT_LINE(CHR(10) ||
                           TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS  ') ||
                           'OPT_PROD_DIM ' || COUNT_TOTAL ||
                           ' rows removed.' || CHR(10));
      COMMIT;

      -----------------------------------------------------------------
      ---STEP 040: remove data from OPT_PROD_ASSOC_ESI
      -----------------------------------------------------------------
      V_TBL_NAME := 'OPT_PROD_ASSOC_ESI';
      V_SQL_STMT := 'DELETE FROM OPT_PROD_ASSOC_ESI WHERE prod_skid > 999999999000000';

      EXECUTE IMMEDIATE V_SQL_STMT;
      COUNT_TOTAL := SQL%ROWCOUNT;
      DBMS_OUTPUT.PUT_LINE(CHR(10) ||
                           TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS  ') ||
                           'OPT_PROD_ASSOC_ESI ' || COUNT_TOTAL ||
                           ' rows removed.' || CHR(10));
      COMMIT;

      -----------------------------------------------------------------
      ---STEP 050: remove data from OPT_FUND_PROD_FDIM
      -----------------------------------------------------------------
      V_TBL_NAME := 'OPT_FUND_PROD_FDIM';
      V_SQL_STMT := 'DELETE FROM OPT_FUND_PROD_FDIM WHERE prod_skid > 999999999000000';

      EXECUTE IMMEDIATE V_SQL_STMT;
      COUNT_TOTAL := SQL%ROWCOUNT;
      DBMS_OUTPUT.PUT_LINE(CHR(10) ||
                           TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS  ') ||
                           'OPT_FUND_PROD_FDIM ' || COUNT_TOTAL ||
                           ' rows removed.' || CHR(10));
      COMMIT;

      -----------------------------------------------------------------
      ---STEP 060: remove data from OPT_FUND_PROD_DIM
      -----------------------------------------------------------------
      V_TBL_NAME := 'OPT_FUND_PROD_DIM';
      V_SQL_STMT := 'DELETE FROM OPT_FUND_PROD_DIM WHERE prod_skid > 999999999000000';

      EXECUTE IMMEDIATE V_SQL_STMT;
      COUNT_TOTAL := SQL%ROWCOUNT;
      DBMS_OUTPUT.PUT_LINE(CHR(10) ||
                           TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS  ') ||
                           'OPT_FUND_PROD_DIM ' || COUNT_TOTAL ||
                           ' rows removed.' || CHR(10));
      COMMIT;

    EXCEPTION
      WHEN OTHERS THEN
        PRO_PUT_LINE('Error: insert dummy products to table ' || V_TBL_NAME ||
                     ' failed with error - ' || SQLERRM);
        RAISE_APPLICATION_ERROR(C_TBL_DELETE_ERROR,
                                'Error: insert dummy products to table ' ||
                                V_TBL_NAME || ' failed with error - ' ||
                                SQLERRM);

    END;

    /**********************************************************************/
    /* name: merge_data                                                  */
    /* purpose: merge data to table                                      */
    /* parameters:                                                        */
    /*        p_tbl_name - Table to be processed                          */
    /* author:  David, Lang                                                  */
    /* version: 1.00 - initial version                                    */
    /**********************************************************************/
    PROCEDURE MERGE_DATA(P_TBL_NAME IN VARCHAR2, P_EXCT_SEQ_NUM IN NUMBER) IS
     COUNT_TTL_MERGED NUMBER(30);
      V_EXCT_SEQ_NUM NUMBER(3) := P_EXCT_SEQ_NUM;
      V_SQL_STMT     VARCHAR2(32767);
      L_STEP_INFO    VARCHAR2(32767);
    BEGIN
      IF P_TBL_NAME IS NULL THEN
        PRO_PUT_LINE('Error: wrong input parameters!');
        RAISE_APPLICATION_ERROR(C_WRONG_PARMS_ERROR,
                                'Error: wrong input parameters!');
      END IF;

      SELECT SQL_STMT_TXT
        INTO V_SQL_STMT
        FROM OPT_LOAD_DATA_PRC
       WHERE REPLACE(TBL_NAME, CHR(10), '') = P_TBL_NAME
         AND EXCT_SEQ_NUM = V_EXCT_SEQ_NUM;

      L_STEP_INFO := 'merge table ' || P_TBL_NAME || '...';
      PRO_PUT_LINE(L_STEP_INFO);
      
       EXECUTE IMMEDIATE V_SQL_STMT;
             PRO_PUT_LINE(V_SQL_STMT);
      COUNT_TTL_MERGED := SQL%ROWCOUNT;
      COMMIT;
      PRO_PUT_LINE(COUNT_TTL_MERGED||' Rows Merged into  ' || P_TBL_NAME || ' successfully.');



      -- Changed to use Optima Package for statistics gathering
      -- Skip OPT_PRMTN_PROD_SFCT as it's just used for partition exchange with OPT_PRMTN_PROD_FCT
      -- Simon 13:20 2009-9-28
      IF P_TBL_NAME NOT IN
         ('OPT_PRMTN_PROD_SFCT', 'OPT_PROD_BASLN_SFCT', 'OPT_BRAND_BASLN_SFCT') THEN
        -- gather statistics for all involved partitions
        PRO_PUT_LINE('Starting gathering statistics on ' || P_TBL_NAME ||
                     ' table...');
        OPT_GATHER_STATS.OPTIMA_STATS_TBL(P_TBL_NAME);
      END IF;

    EXCEPTION
      WHEN OTHERS THEN
        PRO_PUT_LINE('Error: Merge table ' || P_TBL_NAME ||
                     ' failed with error - ' || SQLERRM);
        RAISE_APPLICATION_ERROR(C_TBL_MERGO_ERROR,
                                'Error: Merge table ' || P_TBL_NAME ||
                                ' failed with error - ' || SQLERRM);
    END;

  PROCEDURE FY_INCRM_PRCSS (p_md_name IN varchar2, p_table_name IN VARCHAR2, v_dop IN NUMBER DEFAULT NULL, P_EXCT_SEQ_NUM IN NUMBER,V_REGN VARCHAR2 DEFAULT NULL)-- SOX IMPLEMENTATION PROJECT
  /*
  ***********************************************************************
  * -- Procedure Name: FY_INCRM_PRCSS
  * -- Purpose: Make the table processed incrementally by fiscal year
  * -- Author: Zhong, Jinhan(Kingham)
  * -- Change History
  * -- Date         Programmer         Description
  * ---------------------------------------------------------------------
  * -- 14-Jul-2011  Kingham            Initial Version
  * -- 24-Aug-2011  Daniel.Qin         collect these truncated partitions
  ***********************************************************************
  */
  IS
    v_table_name VARCHAR2(30);
    v_key_name VARCHAR2(200);
    v_sql VARCHAR2(32767);
    v_cnt NUMBER;
    v_n NUMBER;
    --Optima QMR 11.1,24-Aug-2011, BIP-Wv3-1, Daniel.Qin, Declare a array for storage partition names
    v_prttn_ary PARRAY := PARRAY();

  BEGIN
    -- Checking parameter
    IF p_table_name IS NULL THEN
      raise_application_error('-20100', 'ERROR: Parameter p_table_name is null.');
    ELSE
      v_table_name := p_table_name;
      dbms_output.put_line(to_char(SYSDATE, 'YYYY-MM-DD HH24:MI:SS') || chr(10) || 'Table name is '||v_table_name);
    END IF;


    --Commented by Kingham on 26-Jul-2011 for Optima 11.1 BIP-Wv3-1
    /*
    -- Checking fiscal year partition for target table
    SELECT column_name INTO v_key_name  FROM USER_PART_KEY_COLUMNS WHERE NAME = v_table_name AND  object_type = 'TABLE';
    IF v_key_name <> 'FY_DATE_SKID' THEN
      raise_application_error('-20200', 'ERROR: Partition Key of table '||v_table_name||' is not FY_DATE_SKID, table can''t be processed incrementally by FY');
    END IF;*/

    SELECT COUNT(partition_name) INTO v_cnt FROM USER_TAB_PARTITIONS WHERE table_name = v_table_name;
    IF v_cnt = 1 THEN
      RAISE_application_error('-20250', 'ERROR: Only one partition exists for table '||v_table_name);
    END IF;

    -- Fiscal year partition window moving

    opt_pkg_prttn_util.pro_verify_fy_window_prttn(v_table_name);

    -- Context setting
    OPT_CNTXT_STTNG_PRC(p_md_name, 'opt_load_stage_data.ksh');

    -- Check whether context has been set
    v_sql := 'SELECT COUNT(1) FROM v$context WHERE attribute = ''OPT_MIN_FY_DATE_SKID''';
    EXECUTE IMMEDIATE v_sql INTO v_cnt;
    IF v_cnt = 0 THEN
      raise_application_error('-20270', 'ERROR: Context was not set correctly');
    END IF;

    -- Truncate table or partition according to the context setting
    IF sys_context('OPTIMA', 'OPT_MIN_FY_DATE_SKID') = 0 THEN
      v_sql := 'TRUNCATE TABLE '||v_table_name;
      DBMS_OUTPUT.PUt_line(to_char(SYSDATE, 'YYYY-MM-DD HH24:MI:SS') || chr(10) ||v_sql);
      EXECUTE IMMEDIATE v_sql;
      dbms_output.put_line(to_char(SYSDATE, 'YYYY-MM-DD HH24:MI:SS') || chr(10) || 'Table '||v_table_name|| ' has been truncated');
    ELSE
       FOR I IN (SELECT HIGH_VALUE, PARTITION_NAME
                    FROM USER_TAB_PARTITIONS
                   WHERE TABLE_NAME = v_table_name)
       LOOP
          BEGIN
            v_n := TO_NUMBER(I.HIGH_VALUE);
          EXCEPTION
            WHEN OTHERS THEN
              IF I.PARTITION_NAME = 'PMAX' THEN
                NULL;
              ELSE
                RAISE_APPLICATION_ERROR('-20300', 'Error:  High value '||i.high_value||' of partition '||I.PARTITION_NAME||' can not be converted to number type');
              END IF;
          END;
          IF V_N > SYS_CONTEXT('OPTIMA', 'OPT_MIN_FY_DATE_SKID')
           THEN
            V_SQL := 'ALTER TABLE '||v_table_name||' TRUNCATE PARTITION ' ||I.PARTITION_NAME;
            DBMS_OUTPUT.PUt_line(to_char(SYSDATE, 'YYYY-MM-DD HH24:MI:SS') || chr(10) ||v_sql);
            EXECUTE IMMEDIATE V_SQL;
            DBMS_OUTPUT.PUt_line(to_char(SYSDATE, 'YYYY-MM-DD HH24:MI:SS') || chr(10) ||'Partition '||i.partition_name||' has been truncated');
            --Optima QMR 11.1,24-Aug-2011, BIP-Wv3-1, Daniel.Qin, Put the trucated partition name to the array
            v_prttn_ary.extend;
            v_prttn_ary(v_prttn_ary.last) := I.PARTITION_NAME;
          END IF;
       END LOOP;
    END IF;

    -- insert data to table based on the view
    --Optima QMR 11.1,24-Aug-2011, BIP-Wv3-1, Daniel.Qin,Add partition array parameter v_prttn_ary
    --insert_data(v_table_name, v_dop, P_EXCT_SEQ_NUM,'N');
    insert_data(v_table_name, v_dop, P_EXCT_SEQ_NUM,'N',v_prttn_ary,V_REGN);--SOX IMPLEMENTATION PROJECT
  END FY_INCRM_PRCSS;

    /**********************************************************************/
    /* name: post_update                                                  */
    /* purpose: post update the target tables                             */
    /* parameters:                                                        */
    /*        p_md_name - Module to be processed                          */
    /* author: Luke, Lu                                                   */
    /* version: 1.00 - initial version                                    */
    /* version: 2.00 - Eric update it for B007 in R10                     */
    /* version: 3.00 - Martin, Add a post process for OPT_PROD_FY_FDIM    */
    /* version: 4.00 - Leo,  add one post step for user dim table         */
    /* version: 5.00 - Daniel, update 3 fields for opt_fund_gen_spndg_sfct*/
    /* OPTIMA11,CR894, 16-Dec-2010,Martin, Use 'MERGE' SQL statement to   */
    /*                 replace all 'UPDATE/+ BYPASS_UJVC /' SQL statement.*/
    /* OPTIMA11,B050, 23-Dec-2010,Daniel, modified the la_tot_book_amt    */
    /*                   value in table opt_fund_gen_spndg_sfct           */
    /**********************************************************************/
    PROCEDURE POST_UPDATE(P_MD_NAME IN VARCHAR2) IS
      COUNT_TOTAL NUMBER(15);
      V_CODE      NUMBER := SQLCODE;
      V_ERRM      VARCHAR2(128) := SUBSTR(SQLERRM, 1, 128);
      EXECSQL     VARCHAR2(32767);
      --Define parameters for exception handling
      DEADLOCK_DETECTED EXCEPTION;
      PRAGMA EXCEPTION_INIT(DEADLOCK_DETECTED, -60);
      RESOURCE_BUSY EXCEPTION;
      PRAGMA EXCEPTION_INIT(RESOURCE_BUSY, -54);
    BEGIN
      CASE
      -- Remove the Post update process for OPT_BRAND_BASLN_SFCT as now calculate this in OPT_BRAND_BASLN_IFCT
      -- Change by Luke in R9 Product

      --WHEN UPPER(p_md_name) = 'OPT_BRAND_BASLN_SFCT' THEN
      --  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
      --                       ' Aggregate TRADE_TERM_PCT from OPT_PROD_BASLN_FCT into OPT_BRAND_BASLN_SFCT(BEGIN)');
      --  EXECSQL := 'UPDATE /*+ BYPASS_UJVC */
      /*(SELECT BRAND_BASLN.PROD_SKID,
             BRAND_BASLN.DATE_SKID,
             BRAND_BASLN.PRMTN_ACCT_SKID,
             PROD_BASLN.TRADE_TERM_PCT AS SLOG_TRADE_TERM_PCT,
             BRAND_BASLN.TRADE_TERM_PCT
        FROM OPT_BRAND_BASLN_SFCT BRAND_BASLN,
             (SELECT PROD_HIER.BRAND_SKID,
                     PROD_BASLN.PRMTN_ACCT_SKID,
                     PROD_BASLN.DATE_SKID,
                     AVG(PROD_BASLN.TRADE_TERM_PCT) TRADE_TERM_PCT
                FROM OPT_BRAND_HIER_TDADS PROD_HIER,
                     OPT_PROD_BASLN_FCT PROD_BASLN
               WHERE PROD_HIER.PROD_SKID = PROD_BASLN.PROD_SKID
                 AND PROD_HIER.FY_DATE_SKID = PROD_BASLN.FY_DATE_SKID
                 AND PROD_HIER.PROD_LVL_DESC = ''GTIN''
                 AND PROD_BASLN.TRADE_TERM_PCT IS NOT NULL
               GROUP BY PROD_HIER.BRAND_SKID,
                        PROD_BASLN.PRMTN_ACCT_SKID,
                        PROD_BASLN.DATE_SKID) PROD_BASLN
       WHERE BRAND_BASLN.PROD_SKID = PROD_BASLN.BRAND_SKID
         AND BRAND_BASLN.PRMTN_ACCT_SKID = PROD_BASLN.PRMTN_ACCT_SKID
         AND BRAND_BASLN.DATE_SKID = PROD_BASLN.DATE_SKID
         AND BRAND_BASLN.TRADE_TERM_PCT IS NULL
         AND BRAND_BASLN.PRMTN_ACCT_SKID <> 0
         AND BRAND_BASLN.PROD_SKID <> 0) O
         SET O.TRADE_TERM_PCT = O.SLOG_TRADE_TERM_PCT';
              pro_put_line(EXECSQL);
              EXECUTE IMMEDIATE EXECSQL;
              COUNT_TOTAL := SQL%ROWCOUNT;
              DBMS_OUTPUT.PUT_LINE(CHR(10) ||
                                   TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') || ' ' ||
                                   COUNT_TOTAL || ' rows updated.' ||
                                   CHR(10));
              --Commit changes
              COMMIT;
              DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                                   ' Aggregate TRADE_TERM_PCT from OPT_PROD_BASLN_FCT into OPT_BRAND_BASLN_SFCT(END)');*/

        WHEN UPPER(P_MD_NAME) = 'OPT_FUND_BRAND_SFCT' THEN
          DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                               ' Update BRAND_SPLIT_PCT from OPT_FUND_BRAND_SPLIT_VW into OPT_FUND_BRAND_SFCT(BEGIN)');
          -- First Set the Brand split% to 0 when have user define Brand Split%
          -- Modified by Eric 20100115, use the temp table OPT_BRAND_SPLIT_SFCT replace the view OPT_FUND_BRAND_SPLIT_VW
          --               FROM OPT_FUND_BRAND_SPLIT_VW
          --              WHERE FUND_TYPE_CODE = ''Fixed'')';

          EXECSQL := 'UPDATE OPT_FUND_BRAND_SFCT
     SET BRAND_SPLIT_PCT = 0
   WHERE FUND_SKID IN (SELECT FUND_SKID
                   FROM OPT_BRAND_SPLIT_SFCT)';
          PRO_PUT_LINE(EXECSQL);
          EXECUTE IMMEDIATE EXECSQL;
          COUNT_TOTAL := SQL%ROWCOUNT;
          DBMS_OUTPUT.PUT_LINE(CHR(10) ||
                               TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') || ' ' ||
                               COUNT_TOTAL || ' rows updated.' || CHR(10));
          --Commit changes
          COMMIT;
          -- Insert the data which miss Brand in Product Hierarchy
          -- Mondified by Myra 20090929 in R9, add 'parallel dop' for tuning.
          -- Modified by Myra 20090929, use 'not exists..' to replace 'not in'
          -- Modified by Eric 20100115, use the temp table OPT_BRAND_SPLIT_SFCT replace the view OPT_FUND_BRAND_SPLIT_VW
          -- FROM OPT_FUND_BRAND_SPLIT_VW
          -- WHERE FUND_TYPE_CODE = ''Fixed''
          -- AND NOT EXISTS (SELECT ''X'' FROM OPT_FUND_BRAND_SPLIT_VW V,

          EXECSQL := 'INSERT /*+ APPEND PARALLEL(' || P_DOP ||
                     ') */  INTO OPT_FUND_BRAND_SFCT
    SELECT FUND_SKID,
           FUND_ID,
           FUND_TYPE_CODE,
           FUND_GRP_SKID,
           FUND_GRP_ID,
           PROD_SKID,
           PROD_ID,
           BUS_UNIT_SKID,
           BRAND_SPLIT_PCT
        FROM OPT_BRAND_SPLIT_SFCT
        WHERE NOT EXISTS (SELECT ''X'' FROM OPT_BRAND_SPLIT_SFCT V,
       OPT_FUND_BRAND_SFCT F
       WHERE V.FUND_SKID = F.FUND_SKID
       AND V.PROD_SKID = F.PROD_SKID)';
          PRO_PUT_LINE(EXECSQL);
          EXECUTE IMMEDIATE EXECSQL;
          COUNT_TOTAL := SQL%ROWCOUNT;
          DBMS_OUTPUT.PUT_LINE(CHR(10) ||
                               TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') || ' ' ||
                               COUNT_TOTAL || ' rows updated.' || CHR(10));
          --Commit changes
          COMMIT;
          -- Update the Brand Split when have user define Brand Split%
          -- Modified by Eric 20100115, use the temp table OPT_BRAND_SPLIT_SFCT replace the view OPT_FUND_BRAND_SPLIT_VW
          -- OPT_FUND_BRAND_SPLIT_VW U
          -- AND U.FUND_TYPE_CODE = ''Fixed'') O
          EXECSQL := 'MERGE INTO opt_fund_brand_sfct trgt
                           USING opt_brand_split_sfct srce
                              ON (srce.fund_skid = trgt.fund_skid
                              AND srce.prod_skid = trgt.prod_skid)
                      WHEN MATCHED
                      THEN
                         UPDATE SET trgt.brand_split_pct = srce.brand_split_pct';

          /*  Old SQL        EXECSQL := 'UPDATE /*+ BYPASS_UJVC /
          (SELECT F.FUND_SKID,
                  F.PROD_SKID,
                  F.BRAND_SPLIT_PCT AS BRAND_SPLIT_PCT,
                  U.BRAND_SPLIT_PCT AS T_BRAND_SPLIT_PCT
             FROM OPT_FUND_BRAND_SFCT     F,
                  OPT_BRAND_SPLIT_SFCT U
            WHERE U.FUND_SKID = F.FUND_SKID
              AND U.PROD_SKID = F.PROD_SKID) O
             SET O.BRAND_SPLIT_PCT = O.T_BRAND_SPLIT_PCT';
          */
          PRO_PUT_LINE(EXECSQL);
          EXECUTE IMMEDIATE EXECSQL;
          COUNT_TOTAL := SQL%ROWCOUNT;
          DBMS_OUTPUT.PUT_LINE(CHR(10) ||
                               TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') || ' ' ||
                               COUNT_TOTAL || ' rows updated.' || CHR(10));
          --Commit changes
          COMMIT;

          DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                               ' Update BRAND_SPLIT_PCT from OPT_FUND_BRAND_SPLIT_VW into OPT_FUND_BRAND_SFCT(END)');

        WHEN UPPER(P_MD_NAME) = 'OPT_PROD_FY_FDIM' THEN
          DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                               ' Update those products'' information from GTIN to above Brand levels directly into' ||
                               ' OPT_PROD_FY_FDIM table (BEGIN)');
          EXECSQL := 'MERGE INTO OPT_PROD_FY_FDIM TRGT
              USING ( WITH SRCE_QRY AS
                          (
                          SELECT /*+ MATERIALIZE */  SDIM.PROD_SKID,
                            ESI.PROD_ID,
                            SDIM.CTLG_ID,
                            SDIM.FY_DATE_SKID,
                            SDIM.BUS_UNIT_SKID,
                            SDIM.CATEG_DESC,
                            SDIM.SUB_SECTR_DESC,
                            SDIM.SECTR_DESC
                     FROM  OPT_BRAND_SECTR_SDIM SDIM,
                           OPT_PROD_ASSOC_ESI ESI
                     WHERE  SDIM.PROD_SKID=ESI.PROD_SKID
                       AND  SDIM.CTLG_ID=ESI.CTLG_ID
                       AND  SDIM.BUS_UNIT_SKID=ESI.BUS_UNIT_SKID
                       )
                       SELECT PROD_SKID,
                                   PROD_ID,
                                   CTLG_ID,
                                   FY_DATE_SKID,
                                   BUS_UNIT_SKID,
                                   CATEG_DESC,
                                   SUB_SECTR_DESC,
                                   SECTR_DESC
                       FROM SRCE_QRY
                       ) SRCE
           ON (TRGT.PARNT_PROD_ID = SRCE.PROD_ID
              AND TRGT.CTLG_ID = SRCE.CTLG_ID
              AND TRGT.FY_DATE_SKID = SRCE.FY_DATE_SKID
              AND TRGT.BUS_UNIT_SKID = SRCE.BUS_UNIT_SKID)
          WHEN MATCHED THEN
            UPDATE
               SET TRGT.PROD_CATEG_DESC     = SRCE.CATEG_DESC,
                   TRGT.SUB_SECTR_DESC = SRCE.SUB_SECTR_DESC,
                   TRGT.SECTR_DESC     = SRCE.SECTR_DESC';

          /*  Old SQL   EXECSQL:='UPDATE /*+ BYPASS_UJVC /
           (SELECT FY.PROD_SKID,
                   FY.CTLG_ID,
                   FY.FY_DATE_SKID,
                   FY.BUS_UNIT_SKID,
                  FY.PROD_CATEG_DESC CATEG_DESC,
                  FY.SUB_SECTR_DESC SUB_SECTR_DESC,
                  FY.SECTR_DESC SECTR_DESC,
                  TG.CATEG_DESC T_CATEG_DESC,
                  TG.SUB_SECTR_DESC T_SUB_SECTR_DESC,
                  TG.SECTR_DESC T_SECTR_DESC
              FROM (SELECT FY_FDIM.PROD_SKID PROD_SKID,
                           PROD.PROD_SKID PARNT_PROD_SKID,
                           FY_FDIM.CTLG_ID,
                           FY_FDIM.FY_DATE_SKID,
                           FY_FDIM.BUS_UNIT_SKID,
                           FY_FDIM.PROD_CATEG_DESC,
                           FY_FDIM.SUB_SECTR_DESC,
                           FY_FDIM.SECTR_DESC
                      FROM OPT_PROD_FY_FDIM FY_FDIM,
                           OPT_PROD_ASSOC_ESI PROD
                     WHERE FY_FDIM.SECTR_DESC IS NULL
                       AND FY_FDIM.PARNT_PROD_ID=PROD.PROD_ID
                       AND FY_FDIM.CTLG_ID=PROD.CTLG_ID
                       AND FY_FDIM.BUS_UNIT_SKID=PROD.BUS_UNIT_SKID) FY,
                   OPT_BRAND_SECTR_SDIM TG
             WHERE FY.PARNT_PROD_SKID=TG.PROD_SKID
               AND FY.CTLG_ID=TG.CTLG_ID
               AND FY.FY_DATE_SKID=TG.FY_DATE_SKID
               AND FY.BUS_UNIT_SKID=TG.BUS_UNIT_SKID) O
             SET   O.CATEG_DESC=O.T_CATEG_DESC,
                   O.SUB_SECTR_DESC=O.T_SUB_SECTR_DESC,
                   O.SECTR_DESC=O.T_SECTR_DESC';
          */
          PRO_PUT_LINE(EXECSQL);
          EXECUTE IMMEDIATE EXECSQL;
          COUNT_TOTAL := SQL%ROWCOUNT;
          DBMS_OUTPUT.PUT_LINE(CHR(10) ||
                               TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') || ' ' ||
                               COUNT_TOTAL || ' rows updated.' || CHR(10));
          COMMIT;

          EXECSQL := 'UPDATE OPT_PROD_FY_FDIM SET PROD_CATEG_DESC=PROD_DESC WHERE PROD_CATEG_DESC IS NULL AND PROD_LVL_DESC=''Category''';
          PRO_PUT_LINE(EXECSQL);
          COUNT_TOTAL := SQL%ROWCOUNT;
          PRO_PUT_LINE(COUNT_TOTAL || ' rows updated.' || CHR(10));
          EXECUTE IMMEDIATE EXECSQL;
          EXECSQL := 'UPDATE OPT_PROD_FY_FDIM SET SUB_SECTR_DESC=PROD_DESC WHERE SUB_SECTR_DESC IS NULL AND SUBSTR(PROD_LVL_DESC,1,3)=''Sub''';
          PRO_PUT_LINE(EXECSQL);
          COUNT_TOTAL := SQL%ROWCOUNT;
          PRO_PUT_LINE(COUNT_TOTAL || ' rows updated.' || CHR(10));
          EXECUTE IMMEDIATE EXECSQL;
          EXECSQL := 'UPDATE OPT_PROD_FY_FDIM SET SECTR_DESC=PROD_DESC WHERE SECTR_DESC IS NULL AND PROD_LVL_DESC=''Sector''';
          PRO_PUT_LINE(EXECSQL);
          COUNT_TOTAL := SQL%ROWCOUNT;
          PRO_PUT_LINE(COUNT_TOTAL || ' rows updated.' || CHR(10));
          COMMIT;
          DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                               ' Update those products'' information into OPT_PROD_FY_FDIM table (END).');

      --updated by Leo on 20100427 for CR#408 in R10, begin
      --added post step for user dim load to populate the user table for PLI not enabled countries
      --removed by Leo on 20100510 per the requirement of CR#408
      --WHEN UPPER(P_MD_NAME) = 'OPT_USER_DIM' THEN
      -- DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
      --                      ' To have the First Name and Last Name details populated on the User Table for PLI Disabled businesses');
      -- EXECSQL := 'INSERT /*+APPEND PARALLEL(' || P_DOP ||
      --            ')*/ INTO OPT_USER_DIM
      --             (USER_ID, FIRST_NAME, LAST_NAME, T_NUM)
      --             SELECT APPL_ID  AS USER_ID,
      --                    PARM_VAL AS FIRST_NAME,
      --                    PARM_VAL AS LAST_NAME,
      --                    APPL_ID  AS T_NUM
      --               FROM OPT_QRY_EXTRN_PARM_PRC
      --              WHERE PARM_NAME_CODE = ''PLI_ENABLE''
      --               AND STEP_IND_NUM = 1';
      -- PRO_PUT_LINE(EXECSQL);
      -- EXECUTE IMMEDIATE EXECSQL;
      -- COUNT_TOTAL := SQL%ROWCOUNT;
      -- DBMS_OUTPUT.PUT_LINE(CHR(10) ||
      --                      TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') || ' ' ||
      --                      COUNT_TOTAL || ' rows inserted.' || CHR(10));
      -- COMMIT;
      --updated by Leo on 20100427 for CR#408 in R10, end

      --Optima11, B050, 28-Oct-2010, Daniel, Update 3 fields data of table opt_fund_gen_spndg_sfct, Begin
      --Optima11, B050, 28-Apr-2011, Daniel, Move the merge logic into the insertion part of this table
      --WHEN UPPER(p_md_name)='OPT_FUND_GEN_SPNDG_SFCT'
      --THEN
      ----initialize on_invc_book_amt,non_on_invc_book_amt data
      --DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS')||
      --                  ' Update book_amt,on_invc_book_amt,non_on_invc_book_amt information from OPT_PRMTN_PYMT_FCT into'||
      --                  ' OPT_FUND_GEN_SPNDG_SFCT table (BEGIN)');
      --
      ----Optima11, B050, 29-Dec-2010, Daniel, modified the la_tot_book_amt value in table opt_fund_gen_spndg_sfct, Begin
      --    --merge opt_fund_gen_spndg_sfct table from opt_prmtn_pymt_fct table
      --    EXECSQL := 'MERGE INTO opt_fund_gen_spndg_sfct fg_sfct
      --                    USING
      --                    (
      --                    SELECT sfct.fund_skid    ,
      --                       sfct.acct_skid    ,
      --                       sfct.prod_skid    ,
      --                       sfct.fy_date_skid ,
      --                       sfct.date_skid    ,
      --                       sfct.bus_unit_skid,
      --                       pymt_fct.book_amt             / COUNT(1) OVER(PARTITION BY sfct.fund_skid,sfct.bus_unit_skid,sfct.date_skid) AS book_amt,
      --                       pymt_fct.on_invc_book_amt     / COUNT(1) OVER(PARTITION BY sfct.fund_skid,sfct.bus_unit_skid,sfct.date_skid) AS on_invc_book_amt,
      --                       pymt_fct.non_on_invc_book_amt / COUNT(1) OVER(PARTITION BY sfct.fund_skid,sfct.bus_unit_skid,sfct.date_skid) AS non_on_invc_book_amt,
      --                       case when pymt_fct.mth_start_date <= trunc(sysdate,''MM'') then pymt_fct.book_amt / COUNT(1) OVER(PARTITION BY sfct.fund_skid,sfct.bus_unit_skid,sfct.date_skid)
      --                            else sfct.la_tot_book_amt end as la_tot_book_amt
      --                  FROM opt_fund_gen_spndg_sfct sfct,
      --                  (
      --                       SELECT t.fund_skid,
      --                              t.bus_unit_skid,
      --                              cal.mth_skid as date_skid,
      --                              cal.mth_start_date,
      --                              SUM(NVL(book_amt            ,0)) AS book_amt,
      --                              SUM(NVL(on_invc_book_amt    ,0)) AS on_invc_book_amt,
      --                              SUM(NVL(non_on_invc_book_amt,0)) AS non_on_invc_book_amt
      --                       from (
      --                                   SELECT hdr.fund_skid,
      --                                          CASE WHEN line.parnt_pymt_id IS NULL THEN hdr.bus_unit_skid   ELSE line.bus_unit_skid   END AS bus_unit_skid,
      --                                          CASE WHEN line.parnt_pymt_id IS NULL THEN hdr.postg_date_skid ELSE line.postg_date_skid END AS date_skid    ,
      --                                          CASE WHEN line.parnt_pymt_id IS NULL THEN hdr.local_pymt_amt       ELSE line.local_pymt_amt        END AS book_amt   ,
      --                                          CASE WHEN line.parnt_pymt_id IS NULL THEN hdr.on_invc_book_amt     ELSE line.on_invc_book_amt      END AS on_invc_book_amt,
      --                                          CASE WHEN line.parnt_pymt_id IS NULL THEN hdr.non_on_invc_book_amt ELSE line.non_on_invc_book_amt  END AS non_on_invc_book_amt
      --                                     from opt_prmtn_pymt_fct hdr,
      --                                          opt_prmtn_pymt_fct line
      --                                    where hdr.prmtn_pymt_id = line.parnt_pymt_id(+)
      --                                      AND line.prmtn_pymt_id(+) <> line.parnt_pymt_id(+)
      --                                      AND hdr.pymt_sttus_code = ''Booked''
      --                                      AND line.pymt_sttus_code(+) = ''Booked''
      --                                   ) t,opt_cal_mastr_dim cal
      --                       where t.date_skid = cal.cal_mastr_skid
      --                       group by t.fund_skid,
      --                                t.bus_unit_skid,
      --                                cal.mth_skid,
      --                                cal.mth_start_date
      --                       ) pymt_fct
      --                 WHERE sfct.fund_skid = pymt_fct.fund_skid
      --                   AND sfct.bus_unit_skid = pymt_fct.bus_unit_skid
      --                   AND sfct.date_skid = pymt_fct.date_skid
      --                    ) src
      --                    ON
      --                    (
      --                            fg_sfct.fund_skid     = src.fund_skid
      --                        AND fg_sfct.acct_skid     = src.acct_skid
      --                        AND fg_sfct.prod_skid     = src.prod_skid
      --                        AND fg_sfct.fy_date_skid  = src.fy_date_skid
      --                        AND fg_sfct.date_skid     = src.date_skid
      --                        AND fg_sfct.bus_unit_skid = src.bus_unit_skid
      --                    )
      --                    WHEN MATCHED THEN
      --                    UPDATE
      --                    SET
      --                        fg_sfct.book_amt             = src.book_amt ,
      --                        fg_sfct.on_invc_book_amt     = src.on_invc_book_amt,
      --                        fg_sfct.non_on_invc_book_amt = src.non_on_invc_book_amt,
      --                        fg_sfct.la_tot_book_amt      = src.la_tot_book_amt';
      ----Optima11, B050, 29-Dec-2010, Daniel, modified the la_tot_book_amt value in table opt_fund_gen_spndg_sfct, End
      --pro_put_line(EXECSQL);
      --  EXECUTE IMMEDIATE EXECSQL;
      --  COUNT_TOTAL:=SQL%ROWCOUNT;
      --  pro_put_line(COUNT_TOTAL||' rows merged.'||chr(10));
      --COMMIT;
      --
      --DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS')||
      --                  ' Update book_amt,on_invc_book_amt,non_on_invc_book_amt information from OPT_PRMTN_PYMT_FCT into'||
      --                  ' OPT_FUND_GEN_SPNDG_SFCT table (END)');
      --Optima11, B050, 28-Oct-2010, Daniel, Update 3 fields data of table opt_fund_gen_spndg_sfct, End

        ELSE
          DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                               ' No post update process for ' || P_MD_NAME);
      END CASE;

    EXCEPTION
      WHEN DEADLOCK_DETECTED THEN
        ROLLBACK;
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS'));
        RAISE_APPLICATION_ERROR(-20002,
                                ' DeadLock Detected Cannot Continue Retry after Sometime!');
      WHEN RESOURCE_BUSY THEN
        ROLLBACK;
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS'));
        RAISE_APPLICATION_ERROR(-20003,
                                'ORA-00054: resource busy and acquire with NOWAIT specified!');
      WHEN OTHERS THEN
        ROLLBACK;
        V_CODE := SQLCODE;
        V_ERRM := SUBSTR(SQLERRM, 1, 128);
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS'));
        RAISE_APPLICATION_ERROR(-20004,
                                ' Sql Errors Occured while post update running. The Error code is : ' ||
                                V_CODE || ': ' || V_ERRM);

    END;

    /**********************************************************************/
    /* name: load_data                                                    */
    /* purpose: load data into table                                      */
    /* parameters:                                                        */
    /*        v_tbl_name - Table to be processed                          */
    /*        v_dop - degree of parallel                                  */
    /* author: Martin Que                                                 */
    /* version: 1.00 - initial version                                    */
    /**********************************************************************/
    PROCEDURE LOAD_DATA(P_MD_NAME IN VARCHAR2, V_REGN IN VARCHAR2 DEFAULT NULL) IS--SOX IMPLEMENTATION PROJECT
      V_TBL_NAME     VARCHAR2(100);
      V_DOP          NUMBER(2);
      V_EXCT_SEQ_NUM NUMBER(3);
    BEGIN
      --R11 optima BIT Long term, remove 'alter session set optimizer_dynamic_sampling=0' by Jimmy on 20110516 begin
      --R11 optima BIT Long term, Add 'alter session set optimizer_dynamic_sampling=0' by Jimmy on 20110510 begin
      --execute immediate  'alter session set optimizer_dynamic_sampling=0';
      --R11 optima BIT Long term, Add 'alter session set optimizer_dynamic_sampling=0' by Jimmy on 20110510 end
      --R11 optima BIT Long term, remove 'alter session set optimizer_dynamic_sampling=0' by Jimmy on 20110516 end

      -- If the environment is WE PROD, then the DOP should be set to 4
      -- Added by Kingham on 27-Nov-2009
      IF REGEXP_LIKE(UPPER(USER), 'ADWG_.+WE.+') THEN
        P_DOP := 4;
      END IF;

      IF P_MD_NAME IS NULL THEN
        PRO_PUT_LINE('Error: wrong input parameters!');
        RAISE_APPLICATION_ERROR(C_WRONG_PARMS_ERROR,
                                'Error: wrong input parameters!');
      END IF;

      --Optima11,CR1159,25-Apr-2011,Yves Begin
      IF P_MD_NAME = 'DUMMY_PROD' THEN
        PRO_PUT_LINE('begin to delete previous dummy product...');
        DELETE_DUMMY_PRODUCT_DATA;
        PRO_PUT_LINE('delete previous dummy product successfully...');
        PRO_PUT_LINE('begin to load dummy product...');
        INSERT_DUMMY_PRODUCT_DATA;
        PRO_PUT_LINE('load dummy product successfully...');
        RETURN;
      END IF;
      --Optima11,CR1159,25-Apr-2011,Yves End

      --manually extend workarea size for long running jobs - Begin
      --By Luke 2011-1-20
      --apply same setting for 'SHIP_BUOM_WLY_DD' and 'SHIP_CU_WLY_DD' modules by Leo on Jun 24, 2010 - begin
      --remove current workaround for dpwt issue after oracle patch applied by Leo on Feb 14, 2011 - begin
      /* IF P_MD_NAME IN('OPT_FUND_GEN_SPNDG_SFCT',
                       'OPT_ACTVY_SPNDG_GTIN_SFCT',
                       'OPT_PRMTN_GTIN_MTH_SFCT',
                       'OPT_PRMTN_PROD_ESTMT_SFCT',
                       'OPT_PROD_BASLN_SFCT',
                       'OPT_PRMTN_PROD_BASLN_SFCT',
                       'SHIP_BUOM_WLY_DD',
                       'SHIP_CU_WLY_DD') THEN
        EXECUTE IMMEDIATE 'ALTER SESSION SET workarea_size_policy = manual';
        EXECUTE IMMEDIATE 'ALTER SESSION SET sort_area_size = 2147483647';
        EXECUTE IMMEDIATE 'ALTER SESSION SET hash_area_size = 2147483647';
      END IF; */
      --remove current workaround for dpwt issue after oracle patch applied by Leo on Feb 14, 2011 - end
      --apply same setting for 'SHIP_BUOM_WLY_DD' and 'SHIP_CU_WLY_DD' modules by Leo on Jun 24, 2010 - end
      --manually extend workarea size for long running jobs - End
      --By Luke 2011-1-20

      --create a circulation to deal with the table
      PRO_PUT_LINE('begin to process the table of module...');
      FOR C IN (SELECT TBL_NAME, PRCSS_NAME, PRCSS_DOP_NUM, EXCT_SEQ_NUM
                  FROM OPT_LOAD_DATA_PRC
                 WHERE MD_NAME = P_MD_NAME
                 AND (ACTIV_IND IS NULL OR ACTIV_IND = 'Y') -- Added by HP for PR#PM00019779 - SVP AA Elimination*/
                 ORDER BY EXCT_SEQ_NUM ASC) LOOP
        V_TBL_NAME := UPPER(REPLACE(C.TBL_NAME, CHR(10), ''));
        V_DOP      := C.PRCSS_DOP_NUM;
        -- Modified by Myra 20090929 in R9. give default vaule to DOP
        IF V_DOP IS NULL THEN
          V_DOP := P_DOP;
        END IF;
        V_EXCT_SEQ_NUM := C.EXCT_SEQ_NUM;
        CASE
        --if the process name is 'C', it will create the temporary table automatic
          WHEN UPPER(C.PRCSS_NAME) = 'C' THEN
            PRO_PUT_LINE('create table ' || V_TBL_NAME || ' begin...');
            CREATE_STAGE_TABLE(V_TBL_NAME, V_DOP, V_EXCT_SEQ_NUM,V_REGN);--SOX IMPLEMENTATION PROJECT
            --if the process name is 'I', it will insert data into exist table
          WHEN UPPER(C.PRCSS_NAME) = 'I' THEN
            PRO_PUT_LINE('insert data into table ' || V_TBL_NAME ||
                         ' begin...');
           -- INSERT_DATA(V_TBL_NAME, V_DOP, V_EXCT_SEQ_NUM);-- OLD CODE
		   INSERT_DATA(V_TBL_NAME, V_DOP, V_EXCT_SEQ_NUM,'Y',NULL,V_REGN);-- SOX IMPLEMENTATION PROJECT
          WHEN UPPER(C.PRCSS_NAME) = 'D' THEN
            PRO_PUT_LINE('Delete data from table ' || V_TBL_NAME ||
                         ' begin...');
            DELETE_DATA(V_TBL_NAME, V_DOP, V_EXCT_SEQ_NUM);
          WHEN UPPER(C.PRCSS_NAME) = 'M' THEN
            PRO_PUT_LINE('Merge data for table ' || V_TBL_NAME ||
                         ' begin...');
            MERGE_DATA(V_TBL_NAME, V_EXCT_SEQ_NUM);
           -- Optima QMR 11.1, BIP-Wv3-1, modified by Kingham to add a new process to perform FY incremental loading
          WHEN UPPER(C.PRCSS_NAME) = 'P' THEN
            PRO_PUT_LINE('FY Incremental loading for table '|| V_TBL_NAME ||
                         ' begin...');
            FY_INCRM_PRCSS(P_MD_NAME,V_TBL_NAME, V_DOP, V_EXCT_SEQ_NUM,V_REGN);--SOX IMPLEMENTATION PROJECT
          WHEN UPPER(C.PRCSS_NAME) IS NULL THEN
            RAISE_APPLICATION_ERROR(C_TBL_NULL_ERROR,
                                    'Error: Can not find the table!');
        END CASE; END LOOP;

      -- Change by Luke
      -- 8/24/2009
      -- Add the post update process
      POST_UPDATE(P_MD_NAME);

      PRO_PUT_LINE('The module ' || P_MD_NAME || ' is processed completely.');
    END;

    PROCEDURE LOAD_SHPMT_DATA(P_TYPE_NAME IN VARCHAR2)
    /************************************************************************************/
      /* name: load_shpmt_data                                                                                                             */
      /* purpose: Populate opt_shpmt_sfct and opt_shpmt_excpt_rpt_sfct one time by using mulititable insert */
      /* author:  Barbara Li                                                                                                                     */
      /* version: 1.00 - initial version
      /* -- Optima11,B026,18-Aug-2010,Eric, load shipment data by partition, and remove the duplicate data.                                                                                                      */
      /* -- optima11,B026,16-Oct-2010,Eric, fix the issue, if there is no partition of OPT_SHPMT_FULL_SFCT*/
      /* -- optima11,CR1111,6-Jan-2011,Eric, rollback the changes of OPT_SHPMT_FULL_SFCT duplicate data*/
      /* -- Optima11,21-Mar-2011, Eric, Track the number of rows in partition load from TX */
      /***********************************************************************************/
     IS
      V_SQL_STMT VARCHAR2(32767);
      V_CODE     VARCHAR2(32767);
      V_ERRM     VARCHAR2(32767);
      V_SQL_QRY  VARCHAR2(32767);
      -- Optima11,B026,18-Aug-2010,Eric  -- Start
      PTN_NUM        NUMBER;
      V_NUM_PAST_MTH NUMBER;
      PRTTN_ROW_NUM  NUMBER;

    BEGIN

      SELECT COUNT(PARTITION_NAME)
        INTO PTN_NUM
        FROM USER_TAB_PARTITIONS
       WHERE TABLE_NAME = 'OPT_SHPMT_FULL_SFCT';
      DBMS_OUTPUT.PUT_LINE('There are ' || PTN_NUM ||
                           ' partitions of OPT_SHPMT_FULL_SFCT');

      -- optima11,B026,16-Oct-2010,Eric, start
      -- optima11,CR1111,6-Jan-2011,Eric, Strat
      IF PTN_NUM = 1 THEN

        PRO_PUT_LINE('There is no partition of table OPT_SHPMT_FULL_SFCT!');
        RETURN;

      ELSE
        -- optima11,B026,16-Oct-2010,Eric, end

        IF UPPER(P_TYPE_NAME) = 'WLY' THEN

          SELECT NVL(PARM_VAL, 0)
            INTO V_NUM_PAST_MTH
            FROM OPT_QRY_EXTRN_PARM_PRC
           WHERE QRY_NAME_CODE = 'OPT_SHPMT_FCT_WLY'
             AND PARM_NAME_CODE = 'PRCSS_MTH';

          DBMS_OUTPUT.PUT_LINE('This is a weekly data loading! And there are ' ||
                               V_NUM_PAST_MTH ||
                               ' partitions data should be load');

        ELSIF UPPER(P_TYPE_NAME) = 'MLY' THEN

          SELECT NVL(PARM_VAL, 0)
            INTO V_NUM_PAST_MTH
            FROM OPT_QRY_EXTRN_PARM_PRC
           WHERE QRY_NAME_CODE = 'OPT_SHPMT_FCT_MLY'
             AND PARM_NAME_CODE = 'PRCSS_MTH';
          -- Optima11,21-Mar-2011, Eric, Track the number of rows in partition load from TX  Start
          IF V_NUM_PAST_MTH > 36 THEN
            V_NUM_PAST_MTH := 36;
          END IF;
          -- Optima11,21-Mar-2011, Eric, Track the number of rows in partition load from TX  end
          DBMS_OUTPUT.PUT_LINE('This is a monthly data loading! And there are ' ||
                               V_NUM_PAST_MTH ||
                               ' partitions data should be load');

        ELSIF UPPER(P_TYPE_NAME) = 'RSTMT' THEN

          V_NUM_PAST_MTH := PTN_NUM;
          DBMS_OUTPUT.PUT_LINE('This is restatment data loading! And there are ' ||
                               V_NUM_PAST_MTH ||
                               ' partitions data should be load');

        ELSE

          DBMS_OUTPUT.PUT_LINE('This is neither weekly, monthly or restatment data loading!');
          RETURN;

        END IF;

      END IF; --end of 0 partition judgement

      FOR I IN (SELECT PARTITION_NAME
                  FROM (SELECT PARTITION_NAME,
                               RANK() OVER(ORDER BY PARTITION_NAME DESC) RK
                          FROM USER_TAB_PARTITIONS
                         WHERE TABLE_NAME = 'OPT_SHPMT_FULL_SFCT'
                        --AND partition_name <> 'PMAX'
                        )
                 WHERE RK <= V_NUM_PAST_MTH + 3)
      -- Optima11,21-Mar-2011, Eric, Track the number of rows in partition load from TX  Start
       LOOP
        V_SQL_QRY := 'select count(1) from OPT_SHPMT_FULL_SFCT partition(' ||
                     I.PARTITION_NAME || ')';

        EXECUTE IMMEDIATE V_SQL_QRY
          INTO PRTTN_ROW_NUM;
        PRO_PUT_LINE(PRTTN_ROW_NUM || ' rows in paratition ' ||
                     I.PARTITION_NAME || ' load from Tx');

      END LOOP;
      -- Optima11,21-Mar-2011, Eric, Track the number of rows in partition load from TX  end

      -- optima11,CR1111,6-Jan-2011,Eric, end. Now loop the partition name in the cursor for the performance.
      PRO_PUT_LINE('This is beginning of multiple insert');

/*
      V_SQL_STMT := '
                      INSERT FIRST
                      WHEN (bus_unit_skid <> 0 AND acct_skid <> 0 AND prod_skid <> 0
                          AND (( iso_crncy_code = cmprn_iso_crncy_code)
                                  OR ((vol_buom_amt <> 0 or vol_su_amt <> 0) and (giv_amt = 0 and niv_amt = 0 and niv2_amt = 0) and iso_crncy_code is null)))
                      THEN INTO opt_shpmt_sfct (opt_shpmt_skid,                  shpmt_date,
                                                              cal_skid,                              shpmt_date_skid,
                                                              data_type_code,                  prod_skid,
                                                              prod_id,                              gtin_code,
                                                              shpmt_to_code,                   bus_unit_id,
                                                              bus_unit_skid,                      niv_amt,
                                                              giv_amt,                              vol_su_amt,
                                                              prod_eff_date,                     bus_unit_eff_date,
                                                              acct_skid,                            date_skid,
                                                              acct_id,                               etl_run_id,
                                                              etl_mgrt_code,                     regn_code,
                                                              geo_code,                           vol_buom_amt,
                                                              bus_categ,                           iso_crncy_code,
                                                              shpmt_from_code,                shpmt_to_from_code,
                                                              niv2_amt,                            comp_prod_gtin_code,
                                                              comp_prod_skid,                   eff_date,
                                                              shpmt_geo_code)
                                                  VALUES(1,                                       shpmt_date,
                                                              cal_skid,                               shpmt_date_skid,
                                                              data_type_code,                   prod_skid,
                                                              prod_id,                               gtin_code,
                                                              shpmt_to_code,                    bus_unit_id,
                                                              bus_unit_skid,                       niv_amt,
                                                              giv_amt,                               vol_su_amt,
                                                              prod_eff_date,                      bus_unit_eff_date,
                                                              acct_skid,                             date_skid,
                                                              acct_id,                                etl_run_id,
                                                              etl_mgrt_code,                     regn_code,
                                                              geo_code,                           vol_buom_amt
                                                              ,bus_categ,                          nvl(iso_crncy_code,cmprn_iso_crncy_code),
                                                              shpmt_from_code,                shpmt_to_from_code,
                                                              niv2_amt,                             comp_prod_gtin_code,
                                                              comp_prod_skid,                   eff_date,
                                                              shpmt_geo_code)
                      ELSE
                      INTO opt_shpmt_excpt_rpt_sfct
                                                            (data_type_code,                     iso_crncy_code,
                                                            bus_unit_skid,                          shpmt_date,
                                                            gtin_code,                               sap_sales_org_code,
                                                            shipt_code,                              vol_su_amt,
                                                            giv_amt,                                  niv_amt,
                                                            regn_code,                              cntry_desc,
                                                            etl_run_id,                               shpmt_geo_code)
                                               VALUES (data_type_code,                      iso_crncy_code,
                                                            0,                                           shpmt_date,
                                                            gtin_name_expt,                      sap_sales_org_code,
                                                            shpto_name_expt,                    vol_su_amt,
                                                            giv_amt,                                  niv_amt,
                                                            excpt_regn_code,                     cntry_desc,
                                                            etl_run_id,                               shpmt_geo_code)
                      SELECT
                      s.shpmt_date                         AS shpmt_date,
                      s.mth_skid                           AS cal_skid,
                      s.cal_skid                           AS shpmt_date_skid,
                      s.data_type_code                     AS data_type_code ,
                      nvl(prod.prod_skid,0)                      AS prod_skid,
                      prod.prod_id                                  AS prod_id,
                      s.gtin_code                                  AS gtin_code,
                      s.regn_code                                  AS regn_code,
                      nvl(acct.acct_skid,0)                      AS acct_skid,
                      acct.acct_id                                  AS acct_id,
                      s.shpmt_to_code                              AS shpmt_to_code,
                      s.bus_unit_id                                AS bus_unit_id,
                      s.bus_unit_skid                            AS bus_unit_skid,
                      nvl(round(s.niv_amt, 7),0)                   AS niv_amt,
                      nvl(round(s.giv_amt, 7),0)                   AS giv_amt,
                      nvl(round(s.vol_su_amt,7),0)                 AS vol_su_amt ,
                      1                                              AS etl_run_id,
                      1                                              AS etl_mgrt_code,
                      prod.prod_eff_date                          AS prod_eff_date,
                      s.bus_unit_eff_date                        AS bus_unit_eff_date,
                      s.cal_skid                                  AS date_skid,
                      s.intrn_geo_code                            AS geo_code,
                      nvl(s.vol_buom_amt,0)                                AS vol_buom_amt,
                      s.bus_categ_code                          AS bus_categ,
                      s.iso_crncy_code                            AS iso_crncy_code,
                      s.shpmt_from_code                             AS shpmt_from_code,
                      CASE WHEN lower(p_acct.non_call_ind)=''y'' AND s.shpmt_date>=p_acct.non_call_eff_date AND s.shpmt_date<=p_acct.non_call_end_date
                      THEN s.shpmt_from_code ELSE s.shpmt_to_code END AS shpmt_to_from_code,
                      round(s.comp_niv_amt, 7)                    AS niv2_amt,
                      s.comp_prod_gtin_code                AS comp_prod_gtin_code,
                      nvl(comp_prod.prod_skid,0)           AS comp_prod_skid,
                      s.eff_date                           AS eff_date,
                      s.geo_code                           AS shpmt_geo_code,
                      s.cmprn_iso_crncy_code               AS cmprn_iso_crncy_code,
                      s.sap_sales_org_code                 AS sap_sales_org_code,
                      s.cntry_desc                         AS cntry_desc,
                      s.excpt_regn_code                    AS excpt_regn_code ,
                      CASE WHEN prod.prod_skid IS NOT NULL THEN NULL ELSE s.gtin_code END     AS gtin_name_expt,
                      CASE WHEN acct.acct_skid IS NOT NULL THEN NULL ELSE s.shpmt_to_code END AS shpto_name_expt
                      FROM   opt_shpmt_full_sfct s,
                             opt_acct_fdim acct,
                             opt_acct_fdim p_acct,
                             opt_prod_fdim prod,
                             opt_prod_fdim comp_prod
                       WHERE  s.shpmt_to_code= acct.name(+)
                       AND s.bus_unit_skid = acct.bus_unit_skid(+)
                       AND acct.parnt_acct_id = p_acct.acct_id(+)
                       AND s.gtin_code = prod.prod_name(+)
                       AND s.bus_unit_skid = prod.bus_unit_skid(+)
                       AND s.comp_prod_gtin_code = comp_prod.prod_name(+)
                       AND s.bus_unit_skid = comp_prod.bus_unit_skid(+)';
*/

      V_SQL_STMT := '
                      INSERT FIRST
                      WHEN ( bus_unit_skid <> 0 AND acct_skid <> 0 AND prod_skid <> 0
                       AND   ( (iso_crncy_code = cmprn_iso_crncy_code) OR ((vol_buom_amt <> 0 or vol_su_amt <> 0) and (giv_amt = 0 and niv_amt = 0 and niv2_amt = 0) and iso_crncy_code is null)
                             )
                           )
                      THEN INTO opt_shpmt_sfct (opt_shpmt_skid,                 shpmt_date,
                                                cal_skid,                       shpmt_date_skid,
                                                data_type_code,                 prod_skid,
                                                prod_id,                        gtin_code,
                                                shpmt_to_code,                  bus_unit_id,
                                                bus_unit_skid,                  niv_amt,
                                                giv_amt,                        vol_su_amt,
                                                prod_eff_date,                  bus_unit_eff_date,
                                                acct_skid,                      date_skid,
                                                acct_id,                        etl_run_id,
                                                etl_mgrt_code,                  regn_code,
                                                geo_code,                       vol_buom_amt,
                                                bus_categ,                      iso_crncy_code,
                                                shpmt_from_code,                shpmt_to_from_code,
                                                niv2_amt,                       comp_prod_gtin_code,
                                                comp_prod_skid,                 eff_date,
                                                shpmt_geo_code
                                               )
                                         VALUES(1,                              shpmt_date,
                                                cal_skid,                       shpmt_date_skid,
                                                data_type_code,                 prod_skid,
                                                prod_id,                        gtin_code,
                                                shpmt_to_code,                  bus_unit_id,
                                                bus_unit_skid,                  niv_amt,
                                                giv_amt,                        vol_su_amt,
                                                prod_eff_date,                  bus_unit_eff_date,
                                                acct_skid,                      date_skid,
                                                acct_id,                        etl_run_id,
                                                etl_mgrt_code,                  regn_code,
                                                geo_code,                       vol_buom_amt,
                                                bus_categ,                      nvl(iso_crncy_code,cmprn_iso_crncy_code),
                                                shpmt_from_code,                shpmt_to_from_code,
                                                niv2_amt,                       comp_prod_gtin_code,
                                                comp_prod_skid,                 eff_date,
                                                shpmt_geo_code
                                               )
                      ELSE
                      INTO opt_shpmt_excpt_rpt_sfct
                                               (data_type_code,                 iso_crncy_code,
                                                bus_unit_skid,                  shpmt_date,
                                                gtin_code,                      sap_sales_org_code,
                                                shipt_code,                     vol_su_amt,
                                                giv_amt,                        niv_amt,
                                                regn_code,                      cntry_desc,
                                                etl_run_id,                     shpmt_geo_code
                                               )
                                         VALUES(data_type_code,                 iso_crncy_code,
                                                0,                              shpmt_date,
                                                gtin_name_expt,                 sap_sales_org_code,
                                                shpto_name_expt,                vol_su_amt,
                                                giv_amt,                        niv_amt,
                                                excpt_regn_code,                cntry_desc,
                                                etl_run_id,                     shpmt_geo_code
                                               )
                      SELECT s.shpmt_date                         AS shpmt_date,
                             s.mth_skid                           AS cal_skid,
                             s.cal_skid                           AS shpmt_date_skid,
                             s.data_type_code                     AS data_type_code,
                             -- Start Modified by DXC for Decomposed Volume Change R5-CHG1274-US16668
                             case when bu.disp_plan_cmpt = ''Y'' and nvl(acct.anaplan_acct_flg,p_acct.anaplan_acct_flg) = ''Y'' then
                                       case when dp.gtin is not null and dp.attribute_number = 142 and upper(dp.attribute_value) = ''DISPLAY''
                                              then nvl(comp_prod.prod_skid,0)
                                              else nvl(prod.prod_skid,0)
                                       end
                                  else nvl(prod.prod_skid,0)
                             end AS prod_skid,
                             case when bu.disp_plan_cmpt = ''Y'' and nvl(acct.anaplan_acct_flg,p_acct.anaplan_acct_flg) = ''Y'' then
                                       case when dp.gtin is not null and dp.attribute_number = 142 and upper(dp.attribute_value) = ''DISPLAY''
                                              then comp_prod.prod_id
                                              else prod.prod_id
                                       end
                                  else prod.prod_id
                             end AS prod_id,
                             case when bu.disp_plan_cmpt = ''Y'' and nvl(acct.anaplan_acct_flg,p_acct.anaplan_acct_flg) = ''Y'' then
                                       case when dp.gtin is not null and dp.attribute_number = 142 and upper(dp.attribute_value) = ''DISPLAY''
                                              then s.comp_prod_gtin_code
                                              else s.gtin_code
                                       end
                                  else s.gtin_code
                             end AS gtin_code,
                             --nvl(prod.prod_skid,0)                AS prod_skid,
                             --prod.prod_id                         AS prod_id,
                             --s.gtin_code                          AS gtin_code,
                             -- End Modified by DXC for Decomposed Volume Change R5-CHG1274-US16668
                             s.regn_code                          AS regn_code,
                             nvl(acct.acct_skid,0)                AS acct_skid,
                             acct.acct_id                         AS acct_id,
                             s.shpmt_to_code                      AS shpmt_to_code,
                             s.bus_unit_id                        AS bus_unit_id,
                             s.bus_unit_skid                      AS bus_unit_skid,
                             nvl(round(s.niv_amt, 7),0)           AS niv_amt,
                             nvl(round(s.giv_amt, 7),0)           AS giv_amt,
                             nvl(round(s.vol_su_amt,7),0)         AS vol_su_amt ,
                             1                                    AS etl_run_id,
                             1                                    AS etl_mgrt_code,
                             prod.prod_eff_date                   AS prod_eff_date,
                             s.bus_unit_eff_date                  AS bus_unit_eff_date,
                             s.cal_skid                           AS date_skid,
                             s.intrn_geo_code                     AS geo_code,
                             case when bu.disp_plan_cmpt = ''Y'' and nvl(acct.anaplan_acct_flg,p_acct.anaplan_acct_flg) = ''Y'' then
                                       case when dp.gtin is not null and dp.attribute_number = 142 and upper(dp.attribute_value) = ''DISPLAY''
                                            then nvl(s.comp_uom_qty,0)
                                            else nvl(s.vol_buom_amt,0)
                                       end
                                  else nvl(s.vol_buom_amt,0)
                             end                                  AS vol_buom_amt,
                             s.bus_categ_code                     AS bus_categ,
                             s.iso_crncy_code                     AS iso_crncy_code,
                             s.shpmt_from_code                    AS shpmt_from_code,
                             CASE WHEN lower(p_acct.non_call_ind) = ''y'' AND s.shpmt_date >= p_acct.non_call_eff_date AND s.shpmt_date <= p_acct.non_call_end_date
                                  THEN s.shpmt_from_code
                                  ELSE s.shpmt_to_code
                             END                                  AS shpmt_to_from_code,
                             round(s.comp_niv_amt, 7)             AS niv2_amt,
                             s.comp_prod_gtin_code                AS comp_prod_gtin_code,
                             nvl(comp_prod.prod_skid,0)           AS comp_prod_skid,
                             s.eff_date                           AS eff_date,
                             s.geo_code                           AS shpmt_geo_code,
                             s.cmprn_iso_crncy_code               AS cmprn_iso_crncy_code,
                             s.sap_sales_org_code                 AS sap_sales_org_code,
                             s.cntry_desc                         AS cntry_desc,
                             s.excpt_regn_code                    AS excpt_regn_code ,
                             CASE WHEN prod.prod_skid IS NOT NULL
                                  THEN NULL
                                  ELSE s.gtin_code
                             END                                  AS gtin_name_expt,
                             CASE WHEN acct.acct_skid IS NOT NULL
                                  THEN NULL
                                  ELSE s.shpmt_to_code
                             END                                  AS shpto_name_expt
                        FROM opt_shpmt_full_sfct s,
                             opt_acct_fdim acct,
                             opt_acct_fdim p_acct,
                             opt_prod_fdim prod,
                             opt_prod_fdim comp_prod,
                             opt_bus_unit_fdim bu,
                             opt_disp_plan_cmpt_dim dp
                       WHERE s.shpmt_to_code= acct.name(+)
                         AND s.bus_unit_skid = acct.bus_unit_skid(+)
                         AND acct.parnt_acct_id = p_acct.acct_id(+)
                         AND s.gtin_code = prod.prod_name(+)
                         AND s.bus_unit_skid = prod.bus_unit_skid(+)
                         AND s.comp_prod_gtin_code = comp_prod.prod_name(+)
                         AND s.bus_unit_skid = comp_prod.bus_unit_skid(+)
                         AND s.bus_unit_skid = bu.bus_unit_skid
                         AND s.bus_unit_skid = dp.bus_unit_skid(+)
                         AND s.gtin_code = dp.gtin(+)';

      EXECUTE IMMEDIATE V_SQL_STMT;

      PRO_PUT_LINE('This is end of multiple insert.');

      COMMIT;

      -- Optima11, B022, 01-Dec-2010, Daniel, correct the two comparative feilds' upper/lower case for shpmt_to_from_code calculation logic, End
      -- OPTIMA11, B022, By Linyan, 8/16/2010, Changed shpmt_to_from_code calculation logic,end
      DBMS_OUTPUT.PUT_LINE('Multitable insert finished at ' ||
                           TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

      -- Optima11,B026,18-Aug-2010,Eric  -- Start
      -- gather statistics for all involved partitions
      PRO_PUT_LINE('Starting gathering statistics on OPT_SHPMT_SFCT table...');
      OPT_GATHER_STATS.OPTIMA_STATS_TBL('OPT_SHPMT_SFCT');
      -- Optima11,B026,18-Aug-2010,Eric  -- End

    EXCEPTION

      WHEN OTHERS THEN
        ROLLBACK;
        V_CODE := SQLCODE;
        V_ERRM := SUBSTR(SQLERRM, 1, 128);
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS'));
        RAISE_APPLICATION_ERROR(-20004,
                                ' Sql Errors Occured while Mulitable Inserting. The Error code is : ' ||
                                V_CODE || ': ' || V_ERRM);

    END;


  END OPT_LOAD_STAGE_DATA;
/

