CREATE OR REPLACE PACKAGE ADWU_OPTIMA_LAEX.OPT_LOAD_STAGE_DATA IS
  ------------------------------------------------------------------------------------
  -- Package      : OPT_LOAD_STAGE_DATA                                             --
  -- Author       : Martin Que(weijie.que@hp.com)                                   --
  -- Date         : 04-Aug-2009                                                     --
  -- Purpose      : create temporary  dimension atomic data store                   --
  ------------------------------------------------------------------------------------
  -- Change History                                                                 --
  -- Date        Programmer        Description                                      --
  -- ----------- ----------------- ---------------------------------------------------
  -- 04-Aug-2009 Martin Que        Initial Version                                  --
  ------------------------------------------------------------------------------------
     -- constants
     -- Added by Myra 20090928 in R9. for tuning
  -- ----------- ----------------- ---------------------------------------------------
  -- 28-Oct-2009  Bean  Alter source table change the primary key                   --
  --                    add a exct_seq_num + tbl_name as the primary                --
  --                    add delete procedure for data deletion                      --
  -- ----------- ----------------- ---------------------------------------------------
  -- 28-Oct-2010 Daniel Add post update process on table opt_fund_gen_spndg_sfct    --
  --                    add 3 fields and meger them from opt_prmtn_pymt_fct to it   --
  --                    for Optima11, B050                                          --
  ------------------------------------------------------------------------------------


    C_FINAL_TBL_STORAGE VARCHAR2(100) := ' ';--PCTFREE 0 PCTUSED 0 INITRANS 1 MAXTRANS 255';
    --V_TBLSPACE_NAME VARCHAR2(20) := 'TABLESPACE OPTIMA01';
    V_TBLSPACE_NAME     VARCHAR2(50);
    V_INDEX             VARCHAR2(50);

    C_TBL_ROLES VARCHAR2(100) := 'ADWU_OPTIMA_ETL_UPDATE,ADWU_OPTIMA_SELECT,SSE_ROLE,SSE_RO_ROLE';
    V_TABLE_DOP VARCHAR2(20) := ' PARALLEL 4 ';
    P_DOP number := 2;
    TYPE PARRAY IS TABLE OF VARCHAR2(10);
    c_wrong_parms_error       number := -20000;
    c_tbl_null_error          number := -20001;
    c_tbl_insrt_error         number := -20002;
    c_tbl_creat_error         number := -20003;
    c_md_name_error           number := -20004;
    c_tbl_delete_error        number := -20005;
    c_tbl_mergo_error        number := -20006;
    C_STATS_FINAL_EST_PERCENT NUMBER := 5;
    procedure load_data(p_md_name in varchar2,V_REGN IN VARCHAR2 DEFAULT NULL); -- SOX implementation project
/*    procedure create_stage_table(p_tbl_name in varchar2,
                             p_dop in number default null,
                             p_exct_seq_num in number);*/
   PROCEDURE load_shpmt_data(p_type_name IN VARCHAR2);
END OPT_LOAD_STAGE_DATA;
/

