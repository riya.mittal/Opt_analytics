CREATE OR REPLACE PACKAGE BODY ADWU_OPTIMA_LAEX.OPT_IZOOM_REGNL_LOAD_PKG IS

  PROCEDURE PRO_PUT_LINE(P_STRING IN VARCHAR2) IS
    /*
    **********************************************************************
    * name: PRO_PUT_LINE
    * purpose: Record the log
    * parameters:
    *       P_STRING : the content of log
    * author: David Lang
    * version: 1.00 - initial version
    ********************************************************************
    */
    V_STRING_LENGTH NUMBER;
    V_STRING_OFFSET NUMBER := 1;
    V_CUT_POS       NUMBER;
    V_ADD           NUMBER;
  BEGIN
    DBMS_OUTPUT.NEW_LINE;
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
    V_STRING_LENGTH := LENGTH(P_STRING);

    -- loop thru the string by 255 characters and output each chunk
    WHILE V_STRING_OFFSET < V_STRING_LENGTH LOOP
      V_CUT_POS := NVL(INSTR(P_STRING, CHR(10), V_STRING_OFFSET), 0) -
                   V_STRING_OFFSET;
      IF V_CUT_POS < 0 OR V_CUT_POS >= 255 THEN
        V_CUT_POS := 255;
        V_ADD     := 0;
      ELSE
        V_ADD := 1;
      END IF;
      DBMS_OUTPUT.PUT_LINE(SUBSTR(P_STRING, V_STRING_OFFSET, V_CUT_POS));
      V_STRING_OFFSET := V_STRING_OFFSET + V_CUT_POS + V_ADD;
    END LOOP;
  END;

  PROCEDURE PRO_CHECK_TBL_PRTTN(P_TBL_NAME IN USER_TABLES.TABLE_NAME%TYPE,
                                P_FLAG     OUT NUMBER) IS
    /*
    **********************************************************************
    * name: PRO_CHECK_TBL_PRTTN
    * purpose: checking table if partitons or subpartitions existed
    * parameters:
    *       P_TBL_NAME : table_name
    *       P_FLAG : 3 -- Table is not partitioned
    *                   2 -- Table does not exist
    *                   1 -- Table is subpartitioned
    *                   0 -- Table is partitioned by srce_sys_id
    *                   4 -- Synonym is used for WE,CE,LA
    * author: David Lang
    * version: 1.00 - initial version
    ********************************************************************
    */
    V_TBL_NAME           USER_TABLES.TABLE_NAME%TYPE := UPPER(P_TBL_NAME);
    V_NUM_TBL_COUNT      NUMBER;
    V_NUM_PRTTN_COUNT    NUMBER;
    V_NUM_SUBPRTTN_COUNT NUMBER;
    V_NUM_SYNONYMS_COUNT NUMBER;

  BEGIN
    SELECT COUNT(1)
      INTO V_NUM_TBL_COUNT
      FROM USER_TABLES
     WHERE TABLE_NAME = V_TBL_NAME;

    SELECT COUNT(1)
      INTO V_NUM_SYNONYMS_COUNT
      FROM USER_SYNONYMS
     WHERE SYNONYM_NAME = V_TBL_NAME;

    IF (V_NUM_TBL_COUNT = 0) THEN
    -- Check if the synonyms is there
      IF (V_NUM_SYNONYMS_COUNT > 0) THEN
        P_FLAG := 4;
      ELSE
        RAISE_APPLICATION_ERROR(-20010,
                                'ERROR! ' || V_TBL_NAME ||
                                ' does not exist');
        P_FLAG := 2;

      END IF;
    ELSE

      SELECT COUNT(1)
        INTO V_NUM_SUBPRTTN_COUNT
        FROM USER_TAB_SUBPARTITIONS
       WHERE TABLE_NAME = V_TBL_NAME;

      IF (V_NUM_SUBPRTTN_COUNT > 0) THEN
        P_FLAG := 1;
      ELSE
        PRO_PUT_LINE('Table ' || V_TBL_NAME || ' is not subpartitioned');

        SELECT COUNT(1)
          INTO V_NUM_PRTTN_COUNT
          FROM USER_TAB_PARTITIONS P, USER_PART_KEY_COLUMNS K
         WHERE P.TABLE_NAME = K.NAME
           AND P.TABLE_NAME = V_TBL_NAME
           AND K.COLUMN_NAME = 'SRCE_SYS_ID'
           AND P.PARTITION_NAME = 'P_DEFAULT';

        IF (V_NUM_PRTTN_COUNT = 0) THEN
          RAISE_APPLICATION_ERROR(-20020,
                                  'ERROR! ' || V_TBL_NAME ||
                                  ' is not partitioned by SRCE_SYS_ID or without P_DEFAULT partition ');
          P_FLAG := 3;
        ELSE
          PRO_PUT_LINE('Table ' || V_TBL_NAME ||
                       ' is  partitioned by SRCE_SYS_ID and with P_DEFAULT partition');
          P_FLAG := 0;
        END IF;
      END IF;
    END IF;

  END PRO_CHECK_TBL_PRTTN;

  PROCEDURE PRO_TRUNCATE_TABLE(TB_NAME VARCHAR2) IS
    /*
    **********************************************************************
    * name: PRO_TRUNCATE_TABLE
    * purpose: truncate table
    * parameters:
    *       P_TBL_NAME : table_name
    * author: David Lang
    * version: 1.00 - initial version
    ********************************************************************
    */
    V_COUNT NUMBER(1);
  BEGIN

    SELECT COUNT(1)
      INTO V_COUNT
      FROM USER_TABLES
     WHERE TABLE_NAME = TB_NAME;
    IF (V_COUNT > 0) THEN
      EXECUTE IMMEDIATE 'TRUNCATE TABLE ' || TB_NAME;
    END IF;

  END PRO_TRUNCATE_TABLE;

  PROCEDURE PRO_DROP_TABLE(TB_NAME VARCHAR2) IS
    /**********************************************************************
    * name: PRO_DROP_TABLE
    * purpose: Drop table
    * parameters: P_TBL_NAME -- Target table
    * author: David Lang
    * version: 1.00 - initial version
    **********************************************************************/
    V_COUNT NUMBER(1);
  BEGIN

    SELECT COUNT(1)
      INTO V_COUNT
      FROM USER_TABLES
     WHERE TABLE_NAME = TB_NAME;
    IF (V_COUNT > 0) THEN
      EXECUTE IMMEDIATE 'DROP TABLE ' || TB_NAME;
    END IF;

  END PRO_DROP_TABLE;

  PROCEDURE PRO_DROP_PRTTN(P_TBL_NAME IN USER_TABLES.TABLE_NAME%TYPE,
                           P_FLG      IN VARCHAR2 DEFAULT 'Y') IS
    /**********************************************************************
    * name: PRO_DROP_PRTTN
    * purpose: Drop all partitions and subpartitions for target table
    * parameters: P_TBL_NAME -- Target table
    *                   P_FLG -- 'Y' means drop all the partitions and subpartitons
    *                               'N' measn drop all the partitions except PMAX or P_DEAULT partition
    * author: David Lang
    * version: 1.00 - initial version
    *             1.01 - Add a new input parameter 2009-09-09
    **********************************************************************/
    V_SQLSTMT  VARCHAR2(500);
    V_TBL_NAME USER_TABLES.TABLE_NAME%TYPE := UPPER(P_TBL_NAME);
    V_FLAG     NUMBER(1);

    -- Get all the partitions except PMAX partition
    CURSOR PRTTN_CUR_TYP IS
      SELECT PARTITION_NAME
        FROM USER_TAB_PARTITIONS
       WHERE TABLE_NAME = V_TBL_NAME
         AND PARTITION_POSITION !=
             (SELECT MAX(PARTITION_POSITION)
                FROM USER_TAB_PARTITIONS
               WHERE TABLE_NAME = V_TBL_NAME);

    -- Get all subpartitions except subpartitions within PMAX
    CURSOR SUBPRTTN_CUR_TYP IS
      SELECT SUBPARTITION_NAME
        FROM USER_TAB_SUBPARTITIONS
       WHERE TABLE_NAME = V_TBL_NAME
         AND PARTITION_NAME = 'PMAX'
         AND SUBPARTITION_NAME != 'PMAX_DEFAULT';

  BEGIN
    PRO_CHECK_TBL_PRTTN(P_TBL_NAME, V_FLAG);
    -- Table is partitioned by srce_sys_id
    IF (V_FLAG = 0) THEN
      --First drop all old partitions if they exist
      PRO_PUT_LINE('Preparing to drop partitions from ' || V_TBL_NAME);
      FOR PRTTN_REC_TYP IN PRTTN_CUR_TYP LOOP
        V_SQLSTMT := 'ALTER TABLE ' || V_TBL_NAME || ' ' || CHR(10) ||
                     'DROP PARTITION ' || PRTTN_REC_TYP.PARTITION_NAME;

        PRO_PUT_LINE('Executing: ' || V_SQLSTMT);
        EXECUTE IMMEDIATE V_SQLSTMT;
      END LOOP;
      -- Table is subpartitioned by srce_sys_id
    ELSIF (V_FLAG = 1) THEN
      --First drop all old partitions if they exist
      PRO_PUT_LINE('Preparing to drop partitions from ' || V_TBL_NAME);
      FOR PRTTN_REC_TYP IN PRTTN_CUR_TYP LOOP
        V_SQLSTMT := 'ALTER TABLE ' || V_TBL_NAME || ' ' || CHR(10) ||
                     'DROP PARTITION ' || PRTTN_REC_TYP.PARTITION_NAME;

        PRO_PUT_LINE('Executing: ' || V_SQLSTMT);
        EXECUTE IMMEDIATE V_SQLSTMT;
      END LOOP;

      IF (P_FLG = 'Y') THEN
        --Second drop all old subpartitions from PMAX if they exist
        PRO_PUT_LINE('Preparing to drop subpartitions from ' || V_TBL_NAME);
        FOR SUBPRTTN_REC_TYP IN SUBPRTTN_CUR_TYP LOOP
          V_SQLSTMT := 'ALTER TABLE ' || V_TBL_NAME || ' ' || CHR(10) ||
                       'DROP SUBPARTITION ' ||
                       SUBPRTTN_REC_TYP.SUBPARTITION_NAME;

          PRO_PUT_LINE('Executing: ' || V_SQLSTMT);
          EXECUTE IMMEDIATE V_SQLSTMT;
        END LOOP;
      END IF;
    END IF;
  END PRO_DROP_PRTTN;

  PROCEDURE PRO_INITIALIZE_MTH_PRTTN(P_VC2_TBL_NAME       IN USER_TABLES.TABLE_NAME%TYPE,
                                     P_NUM_PAST_MTH_NUM   IN NUMBER,
                                     P_NUM_FUTURE_MTH_NUM IN NUMBER,
									 V_REGN VARCHAR2 DEFAULT NULL)--SOX IMPLEMENTATION PROJECT
  /*
    ***************************************************************************
    * Program : PRO_INITIALIZE_MTH_PRTTN
    * Version : 1.0
    * Author  : David lang
    * Date    : 03-AUG-2009
    * Purpose : Fully rebuilds the monthly partitioning of a table
    *           Should only be done in preparation for a full load of data
    *
    * Note:  This procedure does not provide error handling --- calling code must
    *          handle errors returned from this procedure
    *
    * Change History
    * Date         Programmer         Description
    * -------------------------------------------------------------------------
    * 03-AUG-2009  David Lang      Initial Version
     * 09-SEP-2009  David Lang     Update function of loading older month data
    ****************************************************************************
    */
   IS
    V_VC2_MAX_PRTTN    USER_TAB_PARTITIONS.PARTITION_NAME%TYPE;
    V_VC2_SQLSTMT      VARCHAR2(500);
    V_CURR_COUNT       NUMBER;
    V_FLAG             NUMBER;
    V_VC2_TBL_NAME     USER_TABLES.TABLE_NAME%TYPE := UPPER(P_VC2_TBL_NAME);
    V_TMP_BKP_TBL_NAME USER_TABLES.TABLE_NAME%TYPE := '';
    V_TMP_TBL_NAME     USER_TABLES.TABLE_NAME%TYPE := '';

  BEGIN

    PRO_CHECK_TBL_PRTTN(V_VC2_TBL_NAME, V_FLAG);
    IF (V_FLAG = 2) THEN
      RAISE_APPLICATION_ERROR(-20030,
                              'ERROR!!! ' || V_VC2_TBL_NAME ||
                              ' does not exist');
    ELSE
      OPT_PKG_UTIL.PRO_PUT_LINE('Table ' || V_VC2_TBL_NAME || ' exists');
    END IF;

    SELECT COUNT(DISTINCT PARTITION_NAME) - 2
      INTO V_CURR_COUNT
      FROM USER_TAB_PARTITIONS
     WHERE TABLE_NAME = V_VC2_TBL_NAME;

    PRO_PUT_LINE('Table ' || V_VC2_TBL_NAME ||
                 ' need to load older month data begin');
    -- Need to load older month data than current month
    IF (P_NUM_PAST_MTH_NUM > V_CURR_COUNT) THEN
      PRO_PUT_LINE('Create Backup table for ' || V_VC2_TBL_NAME ||
                   ' begin');
      V_TMP_BKP_TBL_NAME := V_VC2_TBL_NAME || '_BKP';
      -- Drop temporary backup table
      PRO_DROP_TABLE(V_TMP_BKP_TBL_NAME);
      -- Copy DDL to backup table
      OPT_PRTTN_LOAD_PKG.DUPLICATE_PRTTN_TABLE(V_VC2_TBL_NAME,
                                               V_TMP_BKP_TBL_NAME,V_REGN);-- SOX IMPLEMENTATION PROJECT
      -- Create temporary table
      OPT_PKG_PRTTN_UTIL.PRO_CREATE_TEMP_TBL(V_VC2_TBL_NAME,
                                             V_TMP_TBL_NAME,V_REGN); -- SOX IMPLEMENTATION PROJECT
      -- Backup table to backup table
      PRO_EXCHANGE_PRTTN(V_VC2_TBL_NAME, V_TMP_BKP_TBL_NAME);
      PRO_PUT_LINE('Create Backup table for ' || V_VC2_TBL_NAME || ' end');
      -- Clear the partitions
      PRO_DROP_PRTTN(V_VC2_TBL_NAME, 'N');

      -- Create new range paritions
      -- Get maxvalue partition name
      SELECT PARTITION_NAME
        INTO V_VC2_MAX_PRTTN
        FROM USER_TAB_PARTITIONS
       WHERE TABLE_NAME = V_VC2_TBL_NAME
         AND PARTITION_POSITION =
             (SELECT MAX(PARTITION_POSITION)
                FROM USER_TAB_PARTITIONS
               WHERE TABLE_NAME = V_VC2_TBL_NAME);

      --Now loop thru all monthly SKIDs and create the partitions
      FOR MTH_REC_TYP IN (SELECT DISTINCT MTH_SKID + MTH_DAYS_IN_MTH_QTY AS MTH_SKID,
                                          TO_CHAR(TRUNC(MTH_START_DATE, 'MM'),
                                                  'YYYYMM') AS MTH_NAME
                            FROM CAL_MASTR_DIM
                           WHERE MTH_START_DATE BETWEEN
                                 TRUNC(ADD_MONTHS(SYSDATE,
                                                  -1 * P_NUM_PAST_MTH_NUM),
                                       'MM') AND TRUNC(ADD_MONTHS(SYSDATE,
                                                                  P_NUM_FUTURE_MTH_NUM),
                                                       'MM')
                             AND NOT EXISTS
                           (SELECT 1
                                    FROM USER_TAB_PARTITIONS
                                   WHERE PARTITION_NAME =
                                         'P' ||
                                         TO_CHAR(TRUNC(MTH_START_DATE, 'MM'),
                                                 'YYYYMM')
                                     AND TABLE_NAME = V_VC2_TBL_NAME)
                           ORDER BY MTH_SKID ASC) LOOP
        V_VC2_SQLSTMT := 'ALTER TABLE ' || V_VC2_TBL_NAME || ' ' || CHR(10) ||
                         'SPLIT PARTITION ' || V_VC2_MAX_PRTTN || ' ' ||
                         'AT (' || TO_CHAR(MTH_REC_TYP.MTH_SKID) || ') ' ||
                         CHR(10) || 'INTO (PARTITION P' ||
                         MTH_REC_TYP.MTH_NAME || ', ' || CHR(10) ||
                         '      PARTITION ' || V_VC2_MAX_PRTTN || CHR(10) ||
                         '     )';

        OPT_PKG_UTIL.PRO_PUT_LINE('Creating partition P' ||
                                  MTH_REC_TYP.MTH_NAME);
        OPT_PKG_UTIL.PRO_PUT_LINE(V_VC2_SQLSTMT);

        EXECUTE IMMEDIATE V_VC2_SQLSTMT;

      END LOOP;

      -- Create temporary table
      OPT_PKG_PRTTN_UTIL.PRO_CREATE_TEMP_TBL(V_TMP_BKP_TBL_NAME,
                                             V_TMP_TBL_NAME,V_REGN);--SOX IMPLEMENTATION PROJECT
      -- Restore the data from backup table to target table with new partitions
      PRO_EXCHANGE_PRTTN(V_TMP_BKP_TBL_NAME, V_VC2_TBL_NAME);
      PRO_PUT_LINE('Table ' || V_VC2_TBL_NAME ||
                   ' load older month data end');
      -- Add the latest current month partition
    ELSE
      --Get maxvalue partition name
      SELECT PARTITION_NAME
        INTO V_VC2_MAX_PRTTN
        FROM USER_TAB_PARTITIONS
       WHERE TABLE_NAME = V_VC2_TBL_NAME
         AND PARTITION_POSITION =
             (SELECT MAX(PARTITION_POSITION)
                FROM USER_TAB_PARTITIONS
               WHERE TABLE_NAME = V_VC2_TBL_NAME);

      --Now loop thru all monthly SKIDs and create the partitions
      FOR MTH_REC_TYP IN (SELECT DISTINCT MTH_SKID + MTH_DAYS_IN_MTH_QTY AS MTH_SKID,
                                          TO_CHAR(TRUNC(MTH_START_DATE, 'MM'),
                                                  'YYYYMM') AS MTH_NAME
                            FROM CAL_MASTR_DIM
                           WHERE MTH_START_DATE BETWEEN
                                 TRUNC(ADD_MONTHS(SYSDATE,
                                                  -1 * P_NUM_PAST_MTH_NUM),
                                       'MM') AND TRUNC(ADD_MONTHS(SYSDATE,
                                                                  P_NUM_FUTURE_MTH_NUM),
                                                       'MM')
                             AND NOT EXISTS
                           (SELECT 1
                                    FROM USER_TAB_PARTITIONS
                                   WHERE PARTITION_NAME =
                                         'P' ||
                                         TO_CHAR(TRUNC(MTH_START_DATE, 'MM'),
                                                 'YYYYMM')
                                     AND TABLE_NAME = V_VC2_TBL_NAME)
                           ORDER BY MTH_SKID ASC) LOOP
        V_VC2_SQLSTMT := 'ALTER TABLE ' || V_VC2_TBL_NAME || ' ' || CHR(10) ||
                         'SPLIT PARTITION ' || V_VC2_MAX_PRTTN || ' ' ||
                         'AT (' || TO_CHAR(MTH_REC_TYP.MTH_SKID) || ') ' ||
                         CHR(10) || 'INTO (PARTITION P' ||
                         MTH_REC_TYP.MTH_NAME || ', ' || CHR(10) ||
                         '      PARTITION ' || V_VC2_MAX_PRTTN || CHR(10) ||
                         '     )';

        PRO_PUT_LINE('Creating partition P' || MTH_REC_TYP.MTH_NAME);
        PRO_PUT_LINE(V_VC2_SQLSTMT);

        EXECUTE IMMEDIATE V_VC2_SQLSTMT;

      END LOOP;
    END IF;

  END PRO_INITIALIZE_MTH_PRTTN;

  PROCEDURE PRO_INITIALIZE_PRTTN(P_VC2_TBL_NAME IN USER_TABLES.TABLE_NAME%TYPE) IS
    /*
    *********************************************************************
    * name: PRO_INITIALIZE_PRTTN
    * purpose: Initialize and rebuild partitions for target table
    * parameters: P_VC2_TBL_NAME -- Target table
    * Change History
    * Date         Programmer         Description
    * -------------------------------------------------------------------------
    * 2009-07-24  David Lang        Initial Version
    ****************************************************************************
    */

    V_TBL_NAME     USER_TABLES.TABLE_NAME%TYPE := UPPER(P_VC2_TBL_NAME);
    V_PAST_MTH_NUM NUMBER;
    V_SQL          VARCHAR2(2024);
    V_TBL_FLG      NUMBER(3);

    -- Get all the partitions
    CURSOR PRTTN_CUR_TYP IS
      SELECT PARTITION_NAME
        FROM USER_TAB_PARTITIONS
       WHERE TABLE_NAME = V_TBL_NAME;
    /*         AND PARTITION_POSITION !=
    (SELECT MAX(PARTITION_POSITION)
       FROM USER_TAB_PARTITIONS
      WHERE TABLE_NAME = V_TBL_NAME);*/

  BEGIN
    -- Check if subpartitioned
    PRO_CHECK_TBL_PRTTN(V_TBL_NAME, V_TBL_FLG);
    PRO_PUT_LINE('Initialize the partitions according to metadata table');

    -- Table is partitioned by srce_sys_id
    IF (V_TBL_FLG = 0) THEN
      PRO_PUT_LINE('Table is no subpartitions');
      PRO_PUT_LINE('Create all partitions for table ' || V_TBL_NAME ||
                   ' begin...');
      -- Add missed list partitions by srce_sys_id
      FOR J IN (SELECT TO_CHAR(SRCE_SYS_ID) AS SRCE_SYS_ID
                  FROM OPT_IZOOM_MTDTA_PRC MT, OPT_CAL_MASTR_DIM CAL
                 WHERE UPPER(NVL(MT.DAY_OF_WK_LOAD_DATE,
                                 DEFLT_DAY_OF_WK_LOAD_DATE)) =
                       CAL.DAY_OF_WK_NAME
                   AND CAL.DAY_DATE = TRUNC(SYSDATE)
                MINUS
                SELECT SUBSTR(PARTITION_NAME, 3)
                  FROM USER_TAB_PARTITIONS
                 WHERE TABLE_NAME = V_TBL_NAME
                   AND PARTITION_NAME <> 'P_DEFAULT') LOOP
        V_SQL := 'Alter table ' || V_TBL_NAME ||
                 ' SPLIT PARTITION P_DEFAULT VALUES(''' || J.SRCE_SYS_ID ||
                 ''') into ' || '(partition P_' || J.SRCE_SYS_ID ||
                 ', partition P_DEFAULT) update indexes';
        PRO_PUT_LINE('SQL:' || V_SQL);
        EXECUTE IMMEDIATE V_SQL;
        PRO_PUT_LINE('Partition P_' || J.SRCE_SYS_ID ||
                     ' was splited from P_DEFAULT partition');
      END LOOP;
      PRO_PUT_LINE('Create all partitions for table ' || V_TBL_NAME ||
                   ' end');

      --Table is partitioned by range mth_skid and subpartitioned by srce_sys_id
    ELSIF (V_TBL_FLG = 1) THEN
      PRO_PUT_LINE('Create partitions and subpartitions for table ' ||
                   V_TBL_NAME || ' begin...');
      -- Add missed subpartitions by srce_sys_id
      FOR I IN PRTTN_CUR_TYP LOOP
        FOR J IN (SELECT TO_CHAR(SRCE_SYS_ID) AS SRCE_SYS_ID
                    FROM OPT_IZOOM_MTDTA_PRC MT, OPT_CAL_MASTR_DIM CAL
                   WHERE UPPER(NVL(MT.DAY_OF_WK_LOAD_DATE,
                                   DEFLT_DAY_OF_WK_LOAD_DATE)) =
                         CAL.DAY_OF_WK_NAME
                     AND CAL.DAY_DATE = TRUNC(SYSDATE)
                  MINUS
                  SELECT SUBSTR(SUBPARTITION_NAME, 6)
                    FROM USER_TAB_SUBPARTITIONS
                   WHERE TABLE_NAME = V_TBL_NAME
                     AND PARTITION_NAME = 'PMAX'
                     AND SUBPARTITION_NAME <> 'PMAX_DEFAULT') LOOP
          V_SQL := 'Alter table ' || V_TBL_NAME || ' SPLIT SUBPARTITION ' ||
                   I.PARTITION_NAME || '_DEFAULT VALUES(''' ||
                   J.SRCE_SYS_ID || ''') into (subpartition ' ||
                   I.PARTITION_NAME || '_' || J.SRCE_SYS_ID ||
                   ', subpartition ' || I.PARTITION_NAME ||
                   '_DEFAULT) update indexes';
          PRO_PUT_LINE('SQL:' || V_SQL);
          EXECUTE IMMEDIATE V_SQL;
          PRO_PUT_LINE('Partition PMAX_' || J.SRCE_SYS_ID ||
                       ' was splited from PMAX_DEFAULT subpartition');
        END LOOP;
      END LOOP;

      -- Get the past month number from metadata table
      SELECT NVL(MAX(MTH_LOAD_NUM), 0)
        INTO V_PAST_MTH_NUM
        FROM OPT_IZOOM_MTDTA_PRC MT, OPT_CAL_MASTR_DIM CAL
       WHERE MT.DAY_OF_WK_LOAD_DATE = CAL.DAY_OF_WK_NAME
         AND CAL.DAY_DATE = TRUNC(SYSDATE);

      -- Create range partitions by mth_skid
      PRO_INITIALIZE_MTH_PRTTN(V_TBL_NAME, V_PAST_MTH_NUM, 0);
      PRO_PUT_LINE('Create partitions for table ' || V_TBL_NAME || ' end');

    ELSE
      RAISE_APPLICATION_ERROR(-20040,
                              'ERROR!!! ' || V_TBL_NAME ||
                              ' does not exist or is not partitioned');

    END IF;

  END PRO_INITIALIZE_PRTTN;

  PROCEDURE PRO_EXCHANGE_PRTTN(P_VC2_SRCE_TBL IN USER_TABLES.TABLE_NAME%TYPE,
                               P_VC2_DEST_TBL IN USER_TABLES.TABLE_NAME%TYPE,
							   V_REGN VARCHAR2 DEFAULT NULL)--SOX IMPLEMENTATION PROJECT
  /*
    ***************************************************************************
    * Program : PRO_EXCHANGE_PRTTN
    * Version : 1.0
    * Author  : David Lang
    * Purpose : Exchanges partitions between 2 partitioned tables for iZoom data
    * Parameters :  p_vc2_srce_tbl -- source table
    *               p_vc2_dest_tbl -- destination table
    * Note:  This procedure does not provide error handling --- calling code must
    *          handle errors returned from this procedure
    *
    * Change History
    * Date         Programmer         Description
    * -------------------------------------------------------------------------
    * 2009-07-24  David Lang        Initial Version
    ****************************************************************************
    */
   IS
    V_TBL_FLG       NUMBER(3);
    V_VC2_DATE_STR  VARCHAR2(8);
    V_NUM_PAST_MTH  NUMBER(2);
    V_VC2_MAX_PRTTN USER_TAB_PARTITIONS.PARTITION_NAME%TYPE;
    V_TMP_TBL_NAME  USER_TABLES.TABLE_NAME%TYPE := '';

  BEGIN
    -- Check if subpartitioned
    PRO_CHECK_TBL_PRTTN(P_VC2_SRCE_TBL, V_TBL_FLG);

    SELECT NVL(MAX(MTH_LOAD_NUM), 0)
      INTO V_NUM_PAST_MTH
      FROM OPT_IZOOM_MTDTA_PRC MT, CAL_MASTR_DIM CAL
     WHERE UPPER(NVL(MT.DAY_OF_WK_LOAD_DATE, DEFLT_DAY_OF_WK_LOAD_DATE)) =
           CAL.DAY_OF_WK_NAME
       AND CAL.DAY_DATE = TRUNC(SYSDATE);

    -- Get the date string that composes the partition name for the oldest partition we'll exchange
    SELECT MTH_NUM
      INTO V_VC2_DATE_STR
      FROM CAL_MASTR_DIM CAL
     WHERE DAY_DATE = ADD_MONTHS(TRUNC(SYSDATE), -V_NUM_PAST_MTH);

    -- Get maxvalue partition name as this should never be exchanged
    SELECT PARTITION_NAME
      INTO V_VC2_MAX_PRTTN
      FROM USER_TAB_PARTITIONS
     WHERE TABLE_NAME = P_VC2_SRCE_TBL
       AND PARTITION_POSITION =
           (SELECT MAX(PARTITION_POSITION)
              FROM USER_TAB_PARTITIONS
             WHERE TABLE_NAME = P_VC2_SRCE_TBL);

    -- Create temporary table
    OPT_PKG_PRTTN_UTIL.PRO_CREATE_TEMP_TBL(P_VC2_SRCE_TBL, V_TMP_TBL_NAME,V_REGN);--SOX IMPLEMENTATION PROJECT
    OPT_PKG_UTIL.PRO_PUT_LINE(V_TBL_FLG);
    IF (V_TBL_FLG = 0) THEN
      -- Table is partitioned
      -- Loop thru all partitions in the source table and exchange each one
      FOR REC IN (SELECT PARTITION_NAME
                    FROM USER_TAB_PARTITIONS
                   WHERE TABLE_NAME = P_VC2_SRCE_TBL
                        --AND PARTITION_NAME >= 'P' || V_VC2_DATE_STR
                     AND PARTITION_NAME != V_VC2_MAX_PRTTN) LOOP
        OPT_PKG_UTIL.PRO_PUT_LINE('Exchanging partition ' ||
                                  REC.PARTITION_NAME);
        OPT_PKG_PRTTN_UTIL.PRO_EXCHANGE_TBL_WITH_PRTTN(P_VC2_SRCE_TBL,
                                                       REC.PARTITION_NAME,
                                                       V_TMP_TBL_NAME,
                                                       'N');
        BEGIN
          OPT_PKG_PRTTN_UTIL.PRO_EXCHANGE_TBL_WITH_PRTTN(P_VC2_DEST_TBL,
                                                         REC.PARTITION_NAME,
                                                         V_TMP_TBL_NAME,
                                                         'N');
        EXCEPTION
          WHEN OTHERS THEN
            -- If exchange between temp table and dest table fails, need to exchange back to source
            OPT_PKG_UTIL.PRO_PUT_LINE(SQLERRM);
            OPT_PKG_UTIL.PRO_PUT_LINE('ERROR!!!  Need to preserve data');
            OPT_PKG_UTIL.PRO_PUT_LINE('Exchanging from ' || V_TMP_TBL_NAME ||
                                      ' back to ' || P_VC2_SRCE_TBL || '.' ||
                                      REC.PARTITION_NAME);
            OPT_PKG_PRTTN_UTIL.PRO_EXCHANGE_TBL_WITH_PRTTN(P_VC2_SRCE_TBL,
                                                           REC.PARTITION_NAME,
                                                           V_TMP_TBL_NAME,
                                                           'N');
            RAISE_APPLICATION_ERROR(-20050,
                                    'ERROR!!! Exchanging between ' ||
                                    V_TMP_TBL_NAME || ' and ' ||
                                    P_VC2_DEST_TBL);
        END;
        -- Need to truncate temp table after exchange
        OPT_PKG_UTIL.PRO_TRUNCATE_TBL(V_TMP_TBL_NAME);
      END LOOP;
    ELSIF (V_TBL_FLG = 1) THEN
      -- Table is subpartitioned
      -- This means exchange all subpartitions regardless of region
      FOR REC IN (SELECT SUBPARTITION_NAME
                    FROM USER_TAB_SUBPARTITIONS
                   WHERE TABLE_NAME = P_VC2_SRCE_TBL
                     AND PARTITION_NAME >= 'P' || V_VC2_DATE_STR
                     AND PARTITION_NAME != V_VC2_MAX_PRTTN) LOOP
        OPT_PKG_UTIL.PRO_PUT_LINE('Exchanging subpartition ' ||
                                  REC.SUBPARTITION_NAME);
        OPT_PKG_PRTTN_UTIL.PRO_EXCHANGE_TBL_WITH_PRTTN(P_VC2_SRCE_TBL,
                                                       REC.SUBPARTITION_NAME,
                                                       V_TMP_TBL_NAME,
                                                       'Y');
        BEGIN
          OPT_PKG_PRTTN_UTIL.PRO_EXCHANGE_TBL_WITH_PRTTN(P_VC2_DEST_TBL,
                                                         REC.SUBPARTITION_NAME,
                                                         V_TMP_TBL_NAME,
                                                         'Y');
        EXCEPTION
          WHEN OTHERS THEN
            -- If exchange between temp table and dest table fails, need to exchange back to source
            OPT_PKG_UTIL.PRO_PUT_LINE(SQLERRM);
            OPT_PKG_UTIL.PRO_PUT_LINE('ERROR!!!  Need to preserve data');
            OPT_PKG_UTIL.PRO_PUT_LINE('Exchanging from ' || V_TMP_TBL_NAME ||
                                      ' back to ' || P_VC2_SRCE_TBL);
            OPT_PKG_PRTTN_UTIL.PRO_EXCHANGE_TBL_WITH_PRTTN(P_VC2_SRCE_TBL,
                                                           REC.SUBPARTITION_NAME,
                                                           V_TMP_TBL_NAME,
                                                           'Y');
            RAISE_APPLICATION_ERROR(-20060,
                                    'ERROR!!! Exchanging between ' ||
                                    P_VC2_SRCE_TBL || ' and ' ||
                                    P_VC2_DEST_TBL);
        END;
        -- Need to truncate temp table after exchange
        OPT_PKG_UTIL.PRO_TRUNCATE_TBL(V_TMP_TBL_NAME);
      END LOOP;
    ELSE
      RAISE_APPLICATION_ERROR(-20040,
                              'ERROR!!! ' || P_VC2_SRCE_TBL ||
                              ' does not exist or is not partitioned');
    END IF;

  END PRO_EXCHANGE_PRTTN;

  PROCEDURE PRO_REGNL_LOAD(P_VC2_TBL_NAME IN USER_TABLES.TABLE_NAME%TYPE,
                           P_DOP          IN NUMBER DEFAULT 2) IS
    /**********************************************************************/
    /* name: PRO_REGNL_LOAD                                             */
    /* purpose: Regional loading data accroding to meta table by exchange partition     */
    /* parameters: P_TBL_NAME -- Target table
    /*                     P_DOP -- Parllel level */
    /* author: David Lang                                                  */
    /* version: 1.00 - initial version                                    */
    /**********************************************************************/

    V_TBL_NAME      USER_TABLES.TABLE_NAME%TYPE := UPPER(P_VC2_TBL_NAME);
    V_SRC_TBL_NAME  USER_TABLES.TABLE_NAME%TYPE := '';
    V_DEST_TBL_NAME USER_TABLES.TABLE_NAME%TYPE := '';
    V_SQL           VARCHAR2(500) := '';
    V_TBLSPACE_NAME VARCHAR2(100) := '';
    V_FLAG1         NUMBER;
    V_FLAG2         NUMBER;
    V_FLAG3         NUMBER;
    V_COUNT         NUMBER;
    V_NUM_TBL_COUNT NUMBER;

  BEGIN
    PRO_PUT_LINE('Regional load begin');

    -- Get the source globle table name
    V_SRC_TBL_NAME := REPLACE(V_TBL_NAME, 'REGN', 'GLOBL');

    -- Transform tabel name
    -- Use different temp table name to avoid paraller conflict
    IF (V_TBL_NAME = 'OPT_IZOOM_REGN_PROD_TDADS') THEN
      V_DEST_TBL_NAME := 'OPT_IZOOM_REGN_PROD_DADS';
      C_TMP_MTDTA_TBL_NAME := C_TMP_MTDTA_TBL_NAME||'_PROD';
    ELSIF (V_TBL_NAME = 'OPT_IZOOM_REGN_ACCT_TDADS') THEN
      V_DEST_TBL_NAME := 'OPT_IZOOM_REGN_ACCT_DADS';
      C_TMP_MTDTA_TBL_NAME := C_TMP_MTDTA_TBL_NAME||'_ACCT';
    ELSIF (V_TBL_NAME = 'OPT_IZOOM_REGN_EPOS_TFADS') THEN
      V_DEST_TBL_NAME := 'OPT_IZOOM_REGN_EPOS_TFADS';
      C_TMP_MTDTA_TBL_NAME := C_TMP_MTDTA_TBL_NAME||'_EPOS';
    ELSE
      RAISE_APPLICATION_ERROR(-20070,
                              'ERROR!!! Wrong input table name ' ||
                              P_VC2_TBL_NAME);
      PRO_PUT_LINE('This is not table related iZoom');
    END IF;

    PRO_CHECK_TBL_PRTTN(V_SRC_TBL_NAME, V_FLAG1);
    PRO_CHECK_TBL_PRTTN(V_TBL_NAME, V_FLAG2);
    PRO_CHECK_TBL_PRTTN(V_DEST_TBL_NAME, V_FLAG3);

    IF (V_FLAG1 = 2) THEN
      RAISE_APPLICATION_ERROR(-20070,
                              'ERROR!!! The table ' || V_SRC_TBL_NAME ||
                              ' does not exist');
    ELSIF (V_FLAG2 = 2) THEN
      RAISE_APPLICATION_ERROR(-20070,
                              'ERROR!!! The table ' || V_TBL_NAME ||
                              ' does not exist');
    ELSIF (V_FLAG3 = 2) THEN
      RAISE_APPLICATION_ERROR(-20070,
                              'ERROR!!! The table ' || V_DEST_TBL_NAME ||
                              ' does not exist');
    END IF;

    -- check if any data in source data or not
    V_SQL := 'SELECT COUNT(*) FROM ' || V_SRC_TBL_NAME;
    PRO_PUT_LINE('Executing: ' || V_SQL);
    EXECUTE IMMEDIATE V_SQL
      INTO V_COUNT;

    IF (V_COUNT <> 0) THEN

      -- Drop partitions for regional table
      PRO_DROP_PRTTN(V_TBL_NAME);
      -- Truncate data
      PRO_TRUNCATE_TABLE(V_TBL_NAME);
      -- Initialize or add new partitions for regional table
      PRO_INITIALIZE_PRTTN(V_TBL_NAME);
      -- Initialize or add new partitions for regional dimension table
      PRO_INITIALIZE_PRTTN(V_DEST_TBL_NAME);

      -- Create temp izoom metadata table for bu and srce_sys_id
      BEGIN
        PRO_PUT_LINE('Drop temp izoom metadata table begin');
        PRO_DROP_TABLE(C_TMP_MTDTA_TBL_NAME);
        PRO_PUT_LINE('Drop temp izoom metadata table end ok');
        -- find a tablespace
        SELECT TABLESPACE_NAME
          INTO V_TBLSPACE_NAME
          FROM USER_TAB_PARTITIONS
         WHERE TABLE_NAME = V_DEST_TBL_NAME
           AND ROWNUM = 1;

        SELECT COUNT(1)
          INTO V_NUM_TBL_COUNT
          FROM USER_TABLES
         WHERE TABLE_NAME = C_TMP_MTDTA_TBL_NAME;
        -- Check if the temp table is existed
        IF (V_NUM_TBL_COUNT = 0) THEN
          BEGIN
            -- Find srce_sys_id and bu relationship
            V_SQL := 'CREATE TABLE ' || C_TMP_MTDTA_TBL_NAME || CHR(10) ||
                     C_FINAL_TBL_STORAGE || ' TABLESPACE ' ||
                     V_TBLSPACE_NAME || ' PARALLEL ' || P_DOP || CHR(10) || 'AS' ||
                     CHR(10) || 'SELECT MT.*,BUS_UNIT_SKID' || CHR(10) ||
                     'FROM OPT_IZOOM_MTDTA_PRC MT, OPT_BUS_UNIT_PLC BU, OPT_CAL_MASTR_DIM CAL' ||
                     CHR(10) ||
                     'WHERE UPPER(NVL(MT.DAY_OF_WK_LOAD_DATE,DEFLT_DAY_OF_WK_LOAD_DATE)) = CAL.DAY_OF_WK_NAME' ||
                     CHR(10) || '   AND CAL.DAY_DATE = TRUNC(SYSDATE) ' ||
                     CHR(10) ||
                     '   AND (MT.ISO_CNTRY_CODE = BU.ISO_CNTRY_CODE' ||
                     CHR(10) ||
                     '    OR NVL(MT.ISO_CNTRY_CODE,ISO_CNTRY_NUM) = BU.ISO_CNTRY_CODE)';
            PRO_PUT_LINE('Executing: ' || V_SQL);
            EXECUTE IMMEDIATE V_SQL;
          EXCEPTION
            WHEN OTHERS THEN
              RAISE_APPLICATION_ERROR(-20080,
                                      'Failed when create table ' ||
                                      C_TMP_MTDTA_TBL_NAME || ' at Error ' ||
                                      SUBSTR(SQLERRM, 1, 230));

          END;
        END IF;
      END;

      -- Gather statistics for globle table
      IF (V_FLAG1 <> 4) THEN
        PRO_PUT_LINE('Starting gathering statistics process for ' ||
                     V_SRC_TBL_NAME || ' table (estimate percent = ' ||
                     C_STATS_TEMP_EST_PERCENT || ')...');
        DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV',
                                                                      'CURRENT_SCHEMA'),
                                      TABNAME          => V_SRC_TBL_NAME,
                                      ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                      METHOD_OPT       => 'FOR COLUMNS SIZE 1',
                                      CASCADE          => FALSE);
        PRO_PUT_LINE('Statistics gathered.');
      END IF;

      -- Load data into regional tables according to different srce_sys_id
      BEGIN
        IF (V_TBL_NAME = 'OPT_IZOOM_REGN_PROD_TDADS') THEN

          V_SQL := 'INSERT INTO ' || V_TBL_NAME || CHR(10) ||
                   'SELECT SRC.SRCE_SYS_ID,' || CHR(10) || 'SRC.GEO_ID,' ||
                   CHR(10) || 'SRC.ISO_CNTRY_NUM ,' || CHR(10) ||
                   'SRC.ISO_CNTRY_CODE ,' || CHR(10) ||
                   'SRC.PROD_EXTRN_ID ,' || CHR(10) || 'SRC.UPC ,' ||
                   CHR(10) || 'LPAD(TRIM(SRC.GTIN),14,0),' || CHR(10) ||
                   'SRC.PROD_PG_ID ,' || CHR(10) || 'SRC.PROD_CUST_ID , ' ||
                   CHR(10) || 'SRC.BRAND_ID, ' || CHR(10) ||
                   'SRC.PROD_ITEM_NAME,' || CHR(10) ||
                   'SRC.CUST_DEPT_NAME  ,' || CHR(10) ||
                   'SRC.CUST_SUB_DEPT_NAME,' || CHR(10) ||
                   'SRC.CUST_CATEG_NAME, ' || CHR(10) ||
                   'SRC.CUST_SUB_CATEG_NAME ,' || CHR(10) ||
                   'SRC.TEAM_BRAND_DESC ,' || CHR(10) ||
                   'BU.BUS_UNIT_SKID FROM ' || V_SRC_TBL_NAME || ' SRC,' ||
                   C_TMP_MTDTA_TBL_NAME || ' BU' || CHR(10) ||
                   'WHERE SRC.SRCE_SYS_ID = BU.SRCE_SYS_ID';

        ELSIF (V_TBL_NAME = 'OPT_IZOOM_REGN_ACCT_TDADS') THEN

          V_SQL := 'INSERT INTO ' || V_TBL_NAME || CHR(10) ||
                   'SELECT SRC.SRCE_SYS_ID, ' || CHR(10) ||
                   'SRC.ACCT_EXTRN_ID,   ' || CHR(10) ||
                   'SRC.ISO_CNTRY_CODE,  ' || CHR(10) ||
                   'SRC.GEO_ID,        ' || CHR(10) ||
                   'SRC.GLN ,             ' || CHR(10) ||
                   'SRC.OPTMA_CUST_XXHC_NUM, ' || CHR(10) ||
                   'SRC.OPTMA_CUST_XXFI_NUM,  ' || CHR(10) ||
                   'BU.BUS_UNIT_SKID ,  ' || CHR(10) ||
                   'SRC.ACCT_EXTRN_NAME , ' || CHR(10) ||
                   'SRC.CUST_STORE_NAME, ' || CHR(10) ||
                   'SRC.CUST_STORE_RETLR_NAME ' || CHR(10) || 'FROM ' ||
                   V_SRC_TBL_NAME || ' SRC,' || C_TMP_MTDTA_TBL_NAME ||
                   ' BU' || CHR(10) ||
                   'WHERE SRC.SRCE_SYS_ID = BU.SRCE_SYS_ID';

        ELSE
          V_SQL := 'INSERT INTO ' || V_TBL_NAME || CHR(10) ||
                   'SELECT SRC.*,BU.BUS_UNIT_SKID FROM ' || CHR(10) ||
                   V_SRC_TBL_NAME || ' SRC, ' || C_TMP_MTDTA_TBL_NAME ||
                   ' BU ' || CHR(10) ||
                   'WHERE SRC.SRCE_SYS_ID = BU.SRCE_SYS_ID';

        END IF;

        PRO_PUT_LINE('Executing: ' || V_SQL);
        EXECUTE IMMEDIATE V_SQL;
        COMMIT;
      EXCEPTION
        WHEN OTHERS THEN
          ROLLBACK;
          RAISE_APPLICATION_ERROR(-20090,
                                  'Failed when insert data into table ' ||
                                  V_TBL_NAME || ' at Error' ||
                                  SUBSTR(SQLERRM, 1, 230));
      END;

      -- Gather statistics for staging regional table
      PRO_PUT_LINE('Starting gathering statistics process for ' ||
                   V_TBL_NAME || ' table (estimate percent = ' ||
                   C_STATS_TEMP_EST_PERCENT || ')...');
      DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV',
                                                                    'CURRENT_SCHEMA'),
                                    TABNAME          => V_TBL_NAME,
                                    ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                    METHOD_OPT       => 'FOR COLUMNS SIZE 1',
                                    CASCADE          => FALSE);
      PRO_PUT_LINE('Statistics gathered.');

      -- ePOS will not be exchange partitions to ePOS SFCT table
      -- It will be load the data via Informatica mapping
      IF (V_TBL_NAME != 'OPT_IZOOM_REGN_EPOS_TFADS') THEN

        -- Performe partition exchange
        PRO_EXCHANGE_PRTTN(V_TBL_NAME, V_DEST_TBL_NAME);

      END IF;
      -- Gather statistics for target regional table
      PRO_PUT_LINE('Starting gathering statistics process for ' ||
                   V_DEST_TBL_NAME || ' table (estimate percent = ' ||
                   C_STATS_TEMP_EST_PERCENT || ')...');
      DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV',
                                                                    'CURRENT_SCHEMA'),
                                    TABNAME          => V_DEST_TBL_NAME,
                                    ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                    METHOD_OPT       => 'FOR COLUMNS SIZE 1',
                                    CASCADE          => FALSE);
      PRO_PUT_LINE('Statistics gathered.');

      PRO_PUT_LINE('Regional load end ok!');

      -- Drop invalid list partitions by srce_sys_id
      PRO_CLEANUP_PRTTN(V_DEST_TBL_NAME);

    END IF;

  END PRO_REGNL_LOAD;

  PROCEDURE PRO_CLEANUP_PRTTN(P_VC2_TBL_NAME        IN USER_TABLES.TABLE_NAME%TYPE,
                              P_VC2_PRTTN_TYPE_CODE IN VARCHAR2 DEFAULT 'L') IS
    /**********************************************************************/
    /* name: PRO_CLEANUP_PRTTN                                             */
    /* purpose: Drop all partitions or subpartitions by invalid srce_sys_id            */
    /* parameters: P_TBL_NAME -- Target table
    /*                     P_PRTTN_TYPE_CODE -- flag to indicate what kind of partition */
    /*                          will be created on target table. */
    /*                          'L' -- List partition;  */
    /*                          'C' -- Composite partition (Range partition and List sub-partition) */
    /*                          'R' -- Range partition;  */
    /* author: David Lang                                                  */
    /* version: 1.00 - initial version                                    */
    /**********************************************************************/

    V_TBL_NAME USER_TABLES.TABLE_NAME%TYPE := UPPER(P_VC2_TBL_NAME);
    V_SQLSTMT  VARCHAR2(500);

    -- Get all the partitions
    CURSOR PRTTN_CUR_TYP IS
      SELECT PARTITION_NAME
        FROM USER_TAB_PARTITIONS
       WHERE TABLE_NAME = V_TBL_NAME;
    -- Get all the invalid partitons
    CURSOR INVALID_PRTTN_CUR_TYP IS
      SELECT PARTITION_NAME
        FROM USER_TAB_PARTITIONS
       WHERE TABLE_NAME = V_TBL_NAME
         AND PARTITION_POSITION !=
             (SELECT MAX(PARTITION_POSITION)
                FROM USER_TAB_PARTITIONS
               WHERE TABLE_NAME = V_TBL_NAME)
      MINUS
      SELECT 'P_' || SRCE_SYS_ID FROM OPT_IZOOM_MTDTA_PRC;
    -- Get all the invalid subpartitons
    CURSOR INVALID_SUBPRTTN_CUR_TYP IS
      SELECT SUBSTR(SUBPARTITION_NAME, 6) AS SRCE_SYS_ID
        FROM USER_TAB_SUBPARTITIONS
       WHERE TABLE_NAME = V_TBL_NAME
         AND PARTITION_NAME = 'PMAX'
         AND SUBPARTITION_POSITION !=
             (SELECT MAX(PARTITION_POSITION)
                FROM USER_TAB_PARTITIONS
               WHERE TABLE_NAME = V_TBL_NAME)
      MINUS
      SELECT TO_CHAR(SRCE_SYS_ID) AS SRCE_SYS_ID FROM OPT_IZOOM_MTDTA_PRC;

  BEGIN
    IF (P_VC2_PRTTN_TYPE_CODE = 'L') THEN
      -- Drop all invalid partitions
      FOR PRTTN_REC_TYP IN INVALID_PRTTN_CUR_TYP LOOP
        V_SQLSTMT := 'ALTER TABLE ' || V_TBL_NAME || ' ' || CHR(10) ||
                     'DROP PARTITION ' || PRTTN_REC_TYP.PARTITION_NAME;

        PRO_PUT_LINE('Executing: ' || V_SQLSTMT);
        EXECUTE IMMEDIATE V_SQLSTMT;
      END LOOP;
    ELSIF (P_VC2_PRTTN_TYPE_CODE = 'C') THEN
      -- Drop all invalid subpartitions
      FOR I IN PRTTN_CUR_TYP LOOP
        FOR J IN INVALID_SUBPRTTN_CUR_TYP LOOP
          V_SQLSTMT := 'Alter table ' || V_TBL_NAME ||
                       ' DROP SUBPARTITION ' || I.PARTITION_NAME || '_' ||
                       J.SRCE_SYS_ID;
          PRO_PUT_LINE('SQL:' || V_SQLSTMT);
          EXECUTE IMMEDIATE V_SQLSTMT;
          PRO_PUT_LINE('The source system id ' || J.SRCE_SYS_ID ||
                       ' of iZoom is invalid');
        END LOOP;
      END LOOP;
    END IF;

  END PRO_CLEANUP_PRTTN;

END OPT_IZOOM_REGNL_LOAD_PKG; 
/

