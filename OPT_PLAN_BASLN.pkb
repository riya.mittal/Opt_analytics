CREATE OR REPLACE PACKAGE BODY ADWU_OPTIMA_LAEX.OPT_PLAN_BASLN IS

    /**********************************************************************/
    /* name: pro_put_line                                                 */
    /* purpose: print line of text divided into 255-character chunks      */
    /* parameters:                                                        */
    /*   p_string - string to be printed                                  */
    /* author: bazyli blicharski                                          */
    /* originator: brian beckman                                          */
    /* version: 1.00 - initial version                                    */
    /*          1.01 - string cut according to new line marker            */
    /**********************************************************************/
    PROCEDURE PRO_PUT_LINE(P_STRING IN VARCHAR2) IS
        V_STRING_LENGTH NUMBER;
        V_STRING_OFFSET NUMBER := 1;
        V_CUT_POS NUMBER;
        V_ADD NUMBER;
    BEGIN
        DBMS_OUTPUT.NEW_LINE;
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
        V_STRING_LENGTH := LENGTH(P_STRING);

        -- loop thru the string by 255 characters and output each chunk
        WHILE V_STRING_OFFSET < V_STRING_LENGTH
        LOOP
            V_CUT_POS := NVL(INSTR(P_STRING, CHR(10), V_STRING_OFFSET), 0) -
                         V_STRING_OFFSET;
            IF V_CUT_POS < 0 OR V_CUT_POS >= 255 THEN
                V_CUT_POS := 255;
                V_ADD := 0;
            ELSE
                V_ADD := 1;
            END IF;
            DBMS_OUTPUT.PUT_LINE(SUBSTR(P_STRING, V_STRING_OFFSET, V_CUT_POS));
            V_STRING_OFFSET := V_STRING_OFFSET + V_CUT_POS + V_ADD;
        END LOOP;
    END;

    /**********************************************************************/
    /* name: get_bus_unit_filtr                                           */
    /* purpose: prepares a list of all business units to be processed     */
    /* author: bazyli blicharski                                          */
    /* version: 1.00 - initial version                                    */
    /**********************************************************************/
    FUNCTION GET_BUS_UNIT_FILTR RETURN VARCHAR2 IS
        V_RET VARCHAR2(32767) := '';
    BEGIN
        -- scan for business units
        FOR BU IN (SELECT BUS_UNIT_SKID
                   FROM (SELECT BU.BUS_UNIT_SKID,
                                NVL(D.STRTGY_CODE, G.STRTGY_CODE) AS STRTGY_CODE
                         FROM OPT_BUS_UNIT_DIM BU,
                              OPT_BUS_UNIT_PLC G,
                              OPT_BUS_UNIT_PLC D
                         WHERE G.BUS_UNIT_SKID = 0
                         AND D.DEFLT_LVL_NAME IS NOT NULL
                         AND D.BUS_UNIT_SKID(+) <> 0
                         AND BU.BUS_UNIT_SKID = D.BUS_UNIT_SKID(+))
                   WHERE STRTGY_CODE <> C_STRTGY_CODE_DISABLED)
        LOOP
            IF V_RET IS NOT NULL THEN
                V_RET := V_RET || ', ';
            END IF;
            V_RET := V_RET || TO_CHAR(BU.BUS_UNIT_SKID);
        END LOOP;
        IF V_RET IS NOT NULL THEN
            RETURN '[A].bus_unit_skid in (' || V_RET || ')';
        ELSE
            -- no business units to generate
            RETURN '1=2';
        END IF;
    END;

    /************************************************************************************/
    /* name: drop_table (internal procedure                                             */
    /* purpose: drop table passed in the parameter (if exists)                          */
    /* parameters:                                                                      */
    /*   p_tbl_name - table to be removed                                               */
    /* author: bazyli blicharski                                                        */
    /* version: 1.00 - initial version                                                  */
    /************************************************************************************/
    PROCEDURE DROP_TABLE(P_TBL_NAME IN VARCHAR2) IS
        V_CNT NUMBER;
        V_SQL VARCHAR2(100);
    BEGIN
        SELECT COUNT(1)
        INTO V_CNT
        FROM USER_TABLES
        WHERE TABLE_NAME = P_TBL_NAME;
        IF V_CNT > 0 THEN
            V_SQL := 'DROP TABLE ' || P_TBL_NAME;
            PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                         REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Table dropped.');
        END IF;
    END;
    /************************************************************************************/
    /* name: Truncate_table (internal procedure                                             */
    /* purpose: truncate table passed in the parameter (if exists)                          */
    /* parameters:                                                                      */
    /*   p_tbl_name - table to be removed                                               */
    /* author: Mervyn Lin                                                        */
    /* version: 1.00 - initial version                                                  */
    /************************************************************************************/
    PROCEDURE TRUNCATE_TABLE(P_TBL_NAME IN VARCHAR2) IS
        V_CNT NUMBER;
        V_SQL VARCHAR2(100);
    BEGIN
        SELECT COUNT(1)
        INTO V_CNT
        FROM USER_TABLES
        WHERE TABLE_NAME = P_TBL_NAME;
        IF V_CNT > 0 THEN
            V_SQL := 'TRUNCATE TABLE ' || P_TBL_NAME;
            PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                         REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Table truncated.');
        END IF;
    END;

    /************************************************************************************/
    /* name: ship_agg_to_wk                                                             */
    /* purpose: aggregates shipment data for the aim of planning baseline calculation   */
    /* parameters:                                                                      */
    /*   p_order_id - control m order id                                                */
    /*   p_mth_cnt - number of months to be aggregated                                  */
    /*   p_dop - degree of parallelism to be used, default is 4                         */
    /* author: bazyli blicharski                                                        */
    /* version: 1.00 - initial version                                                  */
    /* version: 1.01 - Updated by Myra for new logic of R8 on 2008-12-11             */
    /* version: 1.02 - convert parallel hints to degree 2 on 2009-06-09            */
    /* version: 2.00 -  Updated by David, Performance tuing for this part, move table opt_shpmt_wk_fct to POST BASLN  */
    /* version: 2.01 -  Updated by David in R10 on 2010-4-10 for issue fixing  */
    /************************************************************************************/
    PROCEDURE SHIP_AGG_TO_WK(P_ORDER_ID IN VARCHAR2,
                             P_MTH_CNT IN NUMBER,
                             P_DOP IN NUMBER DEFAULT 2,
							 V_REGN VARCHAR2 DEFAULT NULL) IS--SOX IMPLEMENTATION PROJECT
        V_MIN_DAY_SKID CAL_MASTR_DIM.CAL_MASTR_SKID%TYPE;
        V_MAX_DAY_SKID CAL_MASTR_DIM.CAL_MASTR_SKID%TYPE;
        V_SQL VARCHAR2(32767);
        V_SQL2 VARCHAR2(32767);
        V_TBLSPACE_NAME VARCHAR2(100) := '';
        V_START_DATE DATE := SYSDATE;
        --V_BUS_UNIT_FLTR VARCHAR2(32767);
        V_P_DOP NUMBER := 2;
    BEGIN
        -- parameter control
        PRO_PUT_LINE('Checking input parameters:' || CHR(10) ||
                     '  p_order_id = ' || P_ORDER_ID || CHR(10) ||
                     '  p_mth_cnt = ' || P_MTH_CNT || CHR(10) || '  p_dop = ' ||
                     P_DOP);
        IF P_ORDER_ID IS NULL OR P_MTH_CNT IS NULL OR
           P_DOP IS NULL OR P_DOP < 1 THEN -- PR 72181 [Kalyan added for Shipment Historical]
            PRO_PUT_LINE('Error: wrong input parameters!');
            RAISE_APPLICATION_ERROR(C_WRONG_PARMS_ERROR,
                                    'Error: wrong input parameters!');
        END IF;

        -- find a tablespace
        BEGIN
            SELECT 'TABLESPACE ' || TABLESPACE_NAME
            INTO V_TBLSPACE_NAME
            FROM USER_TAB_PARTITIONS
            WHERE TABLE_NAME = 'OPT_SHPMT_FCT'
            AND ROWNUM = 1;
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Warning: Unable to recognize OPT_SHPMT_FCT tablespace, choosing default tablespace!');
        END;

        -- minimum and maximum skids
        SELECT MIN(CAL_MASTR_SKID),
               MAX(CAL_MASTR_SKID)
        INTO V_MIN_DAY_SKID,
             V_MAX_DAY_SKID
        FROM CAL_MASTR_DIM
        WHERE DAY_DATE >= ADD_MONTHS(TRUNC(SYSDATE + 6, 'MM'), -P_MTH_CNT + 1)
        AND DAY_DATE < ADD_MONTHS(TRUNC(SYSDATE + 6, 'MM'), 1);

        -- business unit filter
        -- Removed by David for B016 in R10
        --V_BUS_UNIT_FLTR := GET_BUS_UNIT_FILTR();

        -- drop temporary table
        TRUNCATE_TABLE(C_SHPMT_ACCT_MTH_BFCT);
        DROP_TABLE(C_SHIP_BRAND_AGG_TEMP_TBL_NAME);

        -- Commented by David for B016 in R10
        -- It is removed to POST BASLN
        -- prepare temporary table create statement
        /*V_SQL := 'CREATE TABLE ' || C_SHPMT_ACCT_MTH_BFCT || CHR(10) ||
                 C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ' PARALLEL ' ||
                 P_DOP || CHR(10) || 'PARTITION BY RANGE (WK_SKID)' || CHR(10) ||
                 '(PARTITION "PMAX" VALUES LESS THAN (MAXVALUE) ' ||
                 C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ')' ||
                 CHR(10) || 'AS' || CHR(10) ||
                 '  SELECT WK_SKID, PROD_SKID, BRAND_SKID, GTIN_STTUS_CODE, ACCT_SKID, VOL_SU_AMT' ||
                 CHR(10) || '  FROM OPT_SHPMT_WK_FCT' || CHR(10) ||
                 '  WHERE rownum < 1';
        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));*/
        -- prepare temporary table create statement
        -- added by myra on 2008-12-11
		
		-- prepare temporary table create statement
				-- Optima11,Oracle11g ORA-600 Bug-fix,4-Oct-2010,JimmyChen  -- Start
        -- removed the parallel parameter when create partition table to avoid ORA-600 error ' PARALLEL ' || P_DOP || CHR(10) || 
				-- Optima11,Oracle11g ORA-600 Bug-fix,4-Oct-2010,JimmyChen  -- End
        V_SQL2 := 'CREATE TABLE ' || C_SHIP_BRAND_AGG_TEMP_TBL_NAME || CHR(10) ||
                  C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ' ' || 
				  ' PARTITION BY RANGE (WK_SKID)' || CHR(10) ||
                  '(PARTITION "PMAX" VALUES LESS THAN (MAXVALUE) ' ||
                  C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ')' ||
                  CHR(10) || 'AS' || CHR(10) ||
                  '  SELECT WK_SKID, PROD_SKID, ACCT_SKID, VOL_SU_AMT' ||
                  CHR(10) || '  FROM OPT_SHPMT_BRAND_WK_FCT' || CHR(10) ||
                  '  WHERE rownum < 1';
        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL2, CHR(10), CHR(10) || '> '));
        BEGIN

           --Removed by David for B016 in R10
           /* EXECUTE IMMEDIATE V_SQL;
            -- initialize partitions
            OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN(P_VC2_TBL_NAME => C_SHIP_AGG_TEMP_TBL_NAME,
                                                        P_NUM_PAST_MTH_NUM => 50,  -- change partition num from 24 to 50, by Kingham on 20-Nov-2009 PR 72181 [Kalyan added for Shipment Historical]
                                                        P_NUM_FUTURE_MTH_NUM => 0,
                                                        P_VC2_DROP_PRTTN_FLAG => 'N');*/
            EXECUTE IMMEDIATE V_SQL2;
            -- initialize partitions
            OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN(P_VC2_TBL_NAME => C_SHIP_BRAND_AGG_TEMP_TBL_NAME,
                                                        P_NUM_PAST_MTH_NUM => 24,  -- Hard code 24 for temp table OPT_SHPMT_BRAND_WK_FCT_TMP, by Kingham on 07-Dec-2009
                                                        P_NUM_FUTURE_MTH_NUM => 0,
                                                        P_VC2_DROP_PRTTN_FLAG => 'N');
            PRO_PUT_LINE('Table created successfuly');
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table failed with error - ' ||
                             SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table failed with error - ' ||
                                         SQLERRM);
        END;

				--Added by David for performance issue on 2009-03-02
        --This is for extract wk from cal_mastr
        --Updated by David in R10 on 2010-4-10 for issue fixing
        DROP_TABLE(C_CAL_MASTR_WK_DIM);
        V_SQL := 'Create table ' || C_CAL_MASTR_WK_DIM || CHR(10) ||
                 C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ' PARALLEL ' ||
                 P_DOP || CHR(10) ||
                 ' AS' || CHR(10) ||
                 'SELECT DISTINCT               ' || CHR(10) ||
                 '  WK_SKID,  ' || CHR(10) ||
                 '  MTH_SKID,  ' || CHR(10) ||
                 '  case ' || CHR(10) ||
                 '     when to_char(wk_start_date, ''mm'') = to_char(wk_end_date, ''mm'') then 7 ' || CHR(10) ||
                 '     when wk_start_date > mth_start_date then mth_end_date - wk_start_date + 1 ' || CHR(10) ||
                 '   else wk_end_date - mth_start_date + 1 ' || CHR(10) ||
                 '   end as day_cnt ' || CHR(10) ||
                 'FROM  opt_cal_mastr_dim ' || CHR(10) ||
                 'where  DAY_DATE >= ADD_MONTHS(TRUNC(SYSDATE + 6, ''MM''), -25)  ' || CHR(10) ||
                 'AND DAY_DATE < ADD_MONTHS(TRUNC(SYSDATE + 6, ''MM''), 1) ' ;

        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Table created successfuly.');
            -- statistics
            PRO_PUT_LINE('Starting gathering statistics process for ' ||
                         C_CAL_MASTR_WK_DIM || ' table (estimate percent = ' ||
                         C_STATS_TEMP_EST_PERCENT || ')...');
            DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                                 'CURRENT_SCHEMA'),
                                          TABNAME => C_CAL_MASTR_WK_DIM,
                                          ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                          METHOD_OPT => 'FOR COLUMNS SIZE 1',
                                          CASCADE => FALSE);
            PRO_PUT_LINE('Statistics gathered.');

        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table ' ||
                             C_CAL_MASTR_WK_DIM || ' failed with error - ' ||
                             SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table ' ||
                                         C_CAL_MASTR_WK_DIM ||
                                         ' failed with error - ' || SQLERRM);
        END;

         --Added by David for performance issue on 2009-03-02
         --This is for extract month from cal_mastr
          DROP_TABLE(C_CAL_MASTR_MTH_DIM);

          V_SQL :='Create table '|| C_CAL_MASTR_MTH_DIM||CHR(10)||
          C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ' PARALLEL ' ||
          V_P_DOP || CHR(10) ||
          ' AS ' || CHR(10) ||
          'SELECT DISTINCT  MTH_SKID,   ' || CHR(10) ||
          '                 DAY_DATE ' || CHR(10) ||
          ' FROM  opt_cal_mastr_dim ' || CHR(10) ||
          ' where  DAY_DATE >= ADD_MONTHS(TRUNC(SYSDATE + 6, ''MM''), -25)  ' || CHR(10) ||
          '    AND DAY_DATE < ADD_MONTHS(TRUNC(SYSDATE + 6, ''MM''), 1)   ' || CHR(10) ||
          '    AND  CAL_MASTR_SKID = MTH_SKID';

          PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Table created successfuly.');
            -- statistics
            PRO_PUT_LINE('Starting gathering statistics process for ' ||
                         C_CAL_MASTR_MTH_DIM || ' table (estimate percent = ' ||
                         C_STATS_TEMP_EST_PERCENT || ')...');
            DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                                 'CURRENT_SCHEMA'),
                                          TABNAME => C_CAL_MASTR_MTH_DIM,
                                          ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                          METHOD_OPT => 'FOR COLUMNS SIZE 1',
                                          CASCADE => FALSE);
            PRO_PUT_LINE('Statistics gathered.');

        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table ' ||
                             C_CAL_MASTR_MTH_DIM || ' failed with error - ' ||
                             SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table ' ||
                                         C_CAL_MASTR_MTH_DIM ||
                                         ' failed with error - ' || SQLERRM);
        END;
        -- inserting data
        V_SQL := 'INSERT /*+ APPEND PARALLEL(' || P_DOP || ') */ INTO ' ||
                 C_SHPMT_ACCT_MTH_BFCT || CHR(10) ||
                 '(MTH_SKID,BUS_UNIT_SKID,ACCT_SKID,PROD_SKID,BRAND_SKID,GTIN_STTUS_CODE,VOL_SU_AMT) '|| CHR(10) ||
                 'SELECT C.MTH_SKID,'|| CHR(10) ||
                 '       S.BUS_UNIT_SKID,'|| CHR(10) ||
                 '       S.ACCT_SKID,'|| CHR(10) ||
                 '       S.PROD_SKID,'|| CHR(10) ||
                 '       S.BRAND_SKID,'|| CHR(10) ||
                 '       S.GTIN_STTUS_CODE,'|| CHR(10) ||
                 '       SUM(S.VOL_SU_AMT * C.DAY_CNT / 7) AS VOL_SU_AMT'|| CHR(10) ||
                 'FROM '|| CHR(10) ||
                 '(SELECT ACCT_HIER.PARNT_ACCT_SKID AS ACCT_SKID,'|| CHR(10) ||
                 '      FCT.BUS_UNIT_SKID,'|| CHR(10) ||
                 '      FCT.PROD_SKID,'|| CHR(10) ||
                 '      FCT.BRAND_SKID,'|| CHR(10) ||
                 '      FCT.GTIN_STTUS_CODE,'|| CHR(10) ||
                 '      FCT.WK_SKID,'|| CHR(10) ||
                 '      FCT.VOL_SU_AMT'|| CHR(10) ||
                 ' FROM OPT_SHPMT_WK_FCT FCT, OPT_ACCT_SHIPT_ASSOC_SDIM ACCT_HIER'|| CHR(10) ||
                 ' WHERE FCT.ACCT_SKID = ACCT_HIER.ACCT_SKID) S,'|| CHR(10) ||
                 'OPT_CAL_MASTR_WK_DIM C'|| CHR(10) ||
                 'WHERE S.WK_SKID = C.WK_SKID'|| CHR(10) ||
                 'GROUP BY C.MTH_SKID,'|| CHR(10) ||
                 '           S.BUS_UNIT_SKID,'|| CHR(10) ||
                 '           S.ACCT_SKID,'|| CHR(10) ||
                 '           S.PROD_SKID,'|| CHR(10) ||
                 '           S.BRAND_SKID,'|| CHR(10) ||
                 '           S.GTIN_STTUS_CODE';

        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Data created successfuly (' || SQL%ROWCOUNT ||
                         ' rows processed)');
            COMMIT;
            -- statistics
            PRO_PUT_LINE('Starting gathering statistics process for ' ||
                         C_SHPMT_ACCT_MTH_BFCT || ' table (estimate percent = ' ||
                         C_STATS_TEMP_EST_PERCENT || ')...');
            DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                                 'CURRENT_SCHEMA'),
                                          TABNAME => C_SHPMT_ACCT_MTH_BFCT,
                                          ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                          METHOD_OPT => 'FOR COLUMNS SIZE 1',
                                          CASCADE => FALSE);
            PRO_PUT_LINE('Statistics gathered.');

        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error:  Data creation on ' ||
                             C_SHPMT_ACCT_MTH_BFCT ||
                             ' failed with error - ' || SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_INS_ERROR,
                                        'Error:  Data creation on ' ||
                                         C_SHPMT_ACCT_MTH_BFCT ||
                                         ' failed with error - ' || SQLERRM);
        END;

        -- populate temporary table OPT_SHPMT_BRAND_WK_FCT_TMP
        V_SQL2 := 'INSERT /*+ APPEND PARALLEL(' || P_DOP || ') */ INTO ' ||
                  C_SHIP_BRAND_AGG_TEMP_TBL_NAME ||
                  ' (WK_SKID, PROD_SKID, ACCT_SKID, VOL_SU_AMT)' || CHR(10) ||
                  '  SELECT WK_SKID,' || CHR(10) ||
                  '         PROD_SKID,' || CHR(10) ||
                  '         ACCT_SKID,' || CHR(10) ||
                  '         sum(SU_AMT)' || CHR(10) || ' FROM ' ||
                  C_SHPMT_AGG_BFCT || CHR(10) ||
                  ' GROUP BY WK_SKID,PROD_SKID,ACCT_SKID';
        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL2, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SQL2;
            PRO_PUT_LINE('Data created successfuly (' || SQL%ROWCOUNT ||
                         ' rows processed)');
            COMMIT;

        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error:  Data creation on ' ||
                             C_SHIP_BRAND_AGG_TEMP_TBL_NAME ||
                             ' failed with error - ' || SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_INS_ERROR,
                                        'Error:  Data creation on ' ||
                                         C_SHIP_BRAND_AGG_TEMP_TBL_NAME ||
                                         ' failed with error - ' || SQLERRM);
        END;

        -- promote data to final OPT_SHPMT_WK_FCT table
        -- Removed by David for B016 in R10
        /*BEGIN
            PRO_PUT_LINE('Starting data promotion from ' ||
                         C_SHIP_AGG_TEMP_TBL_NAME || ' to OPT_SHPMT_WK_FCT...');
            -- create missing partitions
            OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN(P_VC2_TBL_NAME => 'OPT_SHPMT_WK_FCT',
                                                        P_NUM_PAST_MTH_NUM => 50, -- change the partition mth num from 25 to 50
                                                        P_NUM_FUTURE_MTH_NUM => 0,  -- By Kingham on 20-Nov-2009 [PR 72181 [Kalyan added for shipment Historical]
                                                        P_VC2_DROP_PRTTN_FLAG => 'N');
            -- promote data
            OPT_PKG_PRTTN_UTIL.PRO_PROMOTE_DATA(P_VC2_SRCE_TBL => C_SHIP_AGG_TEMP_TBL_NAME,
                                                P_VC2_DEST_TBL => 'OPT_SHPMT_WK_FCT',
                                                P_VC2_MIN_CAL_SKID => NULL,
                                                P_VC2_REGION => '',
                                                P_VC2_CTRLM_ORDR_ID => P_ORDER_ID,
                                                P_VC2_PRTTN_TYPE_CODE => 'M');
            PRO_PUT_LINE('Data promoted succesfully.');
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Promotion of data from ' ||
                             C_SHIP_AGG_TEMP_TBL_NAME ||
                             ' to OPT_SHPMT_WK_FCT failed with error - ' ||
                             SQLERRM);
                RAISE_APPLICATION_ERROR(C_DATA_PROMO_ERROR,
                                        'Error: Promotion of data from ' ||
                                         C_SHIP_AGG_TEMP_TBL_NAME ||
                                         ' to OPT_SHPMT_WK_FCT failed with error - ' ||
                                         SQLERRM);
        END;*/

        -- promote data to final OPT_SHPMT_BRAND_WK_FCT table
        BEGIN
            PRO_PUT_LINE('Starting data promotion from ' ||
                         C_SHIP_BRAND_AGG_TEMP_TBL_NAME ||
                         ' to OPT_SHPMT_WK_FCT...');
            -- create missing partitions
            OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN(P_VC2_TBL_NAME => 'OPT_SHPMT_BRAND_WK_FCT',
                                                        P_NUM_PAST_MTH_NUM => 24,
                                                        P_NUM_FUTURE_MTH_NUM => 0,
                                                        P_VC2_DROP_PRTTN_FLAG => 'N');
            -- promote data
            OPT_PKG_PRTTN_UTIL.PRO_PROMOTE_DATA(P_VC2_SRCE_TBL => C_SHIP_BRAND_AGG_TEMP_TBL_NAME,
                                                P_VC2_DEST_TBL => 'OPT_SHPMT_BRAND_WK_FCT',
                                                P_VC2_MIN_CAL_SKID => NULL,
                                                P_VC2_REGION => '',
                                                P_VC2_CTRLM_ORDR_ID => P_ORDER_ID,
                                                P_VC2_PRTTN_TYPE_CODE => 'M',
												V_REGN => V_REGN);--SOX IMPLEMENTATION PROJECT
            PRO_PUT_LINE('Data promoted succesfully.');
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Promotion of data from ' ||
                             C_SHIP_BRAND_AGG_TEMP_TBL_NAME ||
                             ' to OPT_SHPMT_BRAND_WK_FCT failed with error - ' ||
                             SQLERRM);
                RAISE_APPLICATION_ERROR(C_DATA_PROMO_ERROR,
                                        'Error: Promotion of data from ' ||
                                         C_SHIP_BRAND_AGG_TEMP_TBL_NAME ||
                                         ' to OPT_SHPMT_BRAND_WK_FCT failed with error - ' ||
                                         SQLERRM);
        END;

        -- drop temporary table
        --drop_table(c_ship_agg_temp_tbl_name);
        --drop_table(c_ship_brand_agg_temp_tbl_name);

        -- gather statistics for all involved partitions
        PRO_PUT_LINE('Starting gathering statistics on OPT_SHPMT_BRAND_WK_FCT table...');
        DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                             'CURRENT_SCHEMA'),
                                      TABNAME => 'OPT_SHPMT_BRAND_WK_FCT',
                                      GRANULARITY => 'GLOBAL',
                                      ESTIMATE_PERCENT => C_STATS_FINAL_EST_PERCENT,
                                      BLOCK_SAMPLE => FALSE, DEGREE => 1,
                                      METHOD_OPT => 'FOR ALL COLUMNS SIZE AUTO');
        PRO_PUT_LINE('Statistics on table OPT_SHPMT_BRAND_WK_FCT table gathered');
        PRO_PUT_LINE('Starting gathering statistics on refreshed partitions of OPT_BRAND_SHPMT_WK_FCT table...');
        FOR P IN (SELECT OBJECT_NAME,
                         SUBOBJECT_NAME
                  FROM USER_OBJECTS
                  WHERE OBJECT_NAME = 'OPT_SHPMT_BRAND_WK_FCT'
                  AND OBJECT_TYPE = 'TABLE PARTITION'
                  AND LAST_DDL_TIME >= V_START_DATE)
        LOOP
            PRO_PUT_LINE('Gathering statistics for ' || P.OBJECT_NAME || '.' ||
                         P.SUBOBJECT_NAME);
            DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                                 'CURRENT_SCHEMA'),
                                          TABNAME => P.OBJECT_NAME,
                                          PARTNAME => P.SUBOBJECT_NAME,
                                          GRANULARITY => 'PARTITION',
                                          ESTIMATE_PERCENT => C_STATS_FINAL_EST_PERCENT,
                                          BLOCK_SAMPLE => FALSE, DEGREE => 1,
                                          METHOD_OPT => 'FOR ALL COLUMNS SIZE AUTO');
        END LOOP;
    EXCEPTION
        WHEN OTHERS THEN
            PRO_PUT_LINE('Error: Unexpected system error - ' || SQLERRM);
            RAISE;
    END;

    /************************************************************************************/
    /* name: post_basln_calc                                                            */
    /* purpose: calculates post-event baseline for the aims of planning baseline module */
    /* parameters:                                                                      */
    /*   p_order_id - control m order id                                                */
    /*   p_mth_cnt - number of months to be calculated                                  */
    /*   p_dop - degree of parallelism to be used, default is 4                         */
    /*   p_use_stat_tbl - if to use incremental mode at all                             */
    /* author: bazyli blicharski                                                        */
    /* version: 1.00 - initial version                                                  */
    /* version: 1.01 - Updated by Myra for new logic of R8 on 2009-02-02                */
    /* version: 1.02 - Updated by David for Oracle bug issue ORA-00600 on 2009-04-02    */
    /* version: 1.03 - convert parallel hints to degree 2 on 2009-06-09                 */
    /************************************************************************************/
    PROCEDURE POST_BASLN_CALC(P_ORDER_ID IN VARCHAR2,
                              P_MTH_CNT IN NUMBER,
                              P_DOP IN NUMBER DEFAULT 2,
                              P_USE_STAT_TBL IN VARCHAR2 DEFAULT 'Y',
							  V_REGN VARCHAR2 DEFAULT NULL) IS-- SOX IMPLEMENTATION PROJECT
        V_SQL VARCHAR2(32767);
        V_TBLSPACE_NAME VARCHAR2(100) := '';
        V_START_DATE DATE := SYSDATE;
        V_PRTTN_NAME VARCHAR2(30);
        V_MIN_WK_SKID CAL_MASTR_DIM.WK_SKID%TYPE;
        V_SIX_MTH_AGO_SKID CAL_MASTR_DIM.WK_SKID%TYPE;
        V_BUS_UNIT_FLTR VARCHAR2(32767);
    BEGIN
        -- parameter control
        PRO_PUT_LINE('Checking input parameters:' || CHR(10) ||
                     '  p_order_id = ' || P_ORDER_ID || CHR(10) ||
                     '  p_mth_cnt = ' || P_MTH_CNT || CHR(10) || '  p_dop = ' ||
                     P_DOP || CHR(10) || '  p_use_stat_tbl = ' ||
                     P_USE_STAT_TBL);
        IF P_ORDER_ID IS NULL OR P_MTH_CNT IS NULL OR
           P_MTH_CNT NOT BETWEEN 1 AND 24 OR P_DOP IS NULL OR P_DOP < 1 THEN
            PRO_PUT_LINE('Error: wrong input parameters!');
            RAISE_APPLICATION_ERROR(C_WRONG_PARMS_ERROR,
                                    'Error: wrong input parameters!');
        END IF;

        -- find a tablespace
        BEGIN
            SELECT 'TABLESPACE ' || TABLESPACE_NAME
            INTO V_TBLSPACE_NAME
            FROM USER_TAB_PARTITIONS
            WHERE TABLE_NAME = 'OPT_POST_BASLN_FCT'
            AND ROWNUM = 1;
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Warning: Unable to recognize OPT_POST_BASLN_FCT tablespace, choosing default tablespace!');
        END;

        -- business unit filter
        V_BUS_UNIT_FLTR := GET_BUS_UNIT_FILTR();

        -- truncate OPT_BASLN_SHPMT_STAT_PRC table
        IF P_MTH_CNT >= 24 AND P_USE_STAT_TBL = 'Y' THEN
            PRO_PUT_LINE('Truncating OPT_BASLN_SHPMT_STAT_PRC table...');
            V_SQL := 'TRUNCATE TABLE OPT_BASLN_SHPMT_STAT_PRC';
            PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                         REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
            EXECUTE IMMEDIATE V_SQL;
        END IF;

        --***********************************Added by David*******************************************
        -- This is the workaround given which will solve the bug issue ORA-00600, added by David on 2009-04-02
        V_SQL := 'alter session set "_slave_mapping_enabled"=false';

        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Alter session sucessfully!');
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Alter session parameter failed');
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Alter session parameter _slave_mapping_enabled  ' ||
                                         ' failed with error - ' || SQLERRM);
        END;
        --**************************************************************************************************

        -- prepare temporary table with promoted products (data selection with filtering and account location)
        -- Updated by Myra on 2008-12-11
        -- modify product from 'GTIN' level to 'Brand' level

        DROP_TABLE(C_POST_BASLN_TEMP1_TBL_NAME);
        V_SQL := 'CREATE TABLE ' || C_POST_BASLN_TEMP1_TBL_NAME || CHR(10) ||
                 C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ' PARALLEL ' ||
                 P_DOP || CHR(10) || 'AS' || CHR(10) ||
                 'SELECT /*+ USE_HASH(h prom s) */ distinct' || CHR(10) ||
                 '  h.acct_skid,' || CHR(10) || '  prom.prod_skid,' || CHR(10) ||
                 '  c.wk_skid' || CHR(10) ||
                 'FROM (SELECT /*+ ORDERED USE_HASH(o s) */' || CHR(10) ||
                 '        o.acct_skid, o.root_acct_skid, o.acct_type_desc' ||
                 CHR(10) || '      FROM' || CHR(10) ||
                 '        -- account filtering: only take shipped accounts' ||
                 CHR(10) ||
                 '        (SELECT /*+ NO_MERGE */ distinct acct_skid FROM OPT_SHPMT_BRAND_WK_FCT) s,' ||
                 CHR(10) || '        (SELECT /*+ USE_HASH(o bu) */' || CHR(10) ||
                 '           acct_skid,' || CHR(10) ||
                 '           CONNECT_BY_ROOT acct_skid AS root_acct_skid,' ||
                 CHR(10) || '           acct_type_desc' || CHR(10) ||
                 '         FROM OPT_ACCT_DIM' || CHR(10) || '         WHERE ' ||
                 REPLACE(V_BUS_UNIT_FLTR, '[A].', '') || CHR(10) ||
                 '         CONNECT BY PRIOR acct_id = parnt_acct_id' || CHR(10) ||
                 '        ) o' || CHR(10) ||
                 '        WHERE o.acct_skid = s.acct_skid' || CHR(10) ||
                 '     ) h,' || CHR(10) || '     (SELECT /*+ NO_MERGE */' ||
                 CHR(10) ||
                 '        prod.prod_skid, prom.acct_skid, prom.prmtn_skid, prom.start_date, prom.end_date' ||
                 CHR(10) || '      FROM' || CHR(10) ||
                 '        (SELECT bus_unit_skid, acct_skid, prmtn_skid,' ||
                 CHR(10) ||
                 '                DECODE((CASE WHEN prmtn_stop_date IS NOT NULL AND prmtn_stop_date > pgm_start_date' ||
                 CHR(10) ||
                 '                        AND prmtn_stop_date < pgm_end_date THEN prmtn_stop_date ELSE shpmt_end_date END),' ||
                 CHR(10) || '                        NULL,' || CHR(10) ||
                 '                        pgm_start_date,' || CHR(10) ||
                 '                        shpmt_start_date)' || CHR(10) ||
                 '                as start_date,' || CHR(10) ||
                 '                DECODE((CASE WHEN prmtn_stop_date IS NOT NULL AND prmtn_stop_date > pgm_start_date' ||
                 CHR(10) ||
                 '                        AND prmtn_stop_date < pgm_end_date THEN prmtn_stop_date ELSE shpmt_end_date END),' ||
                 CHR(10) || '                        NULL,' || CHR(10) ||
                 '                        pgm_end_date,' || CHR(10) ||
                 '                        (CASE WHEN prmtn_stop_date IS NOT NULL AND prmtn_stop_date > pgm_start_date' ||
                 CHR(10) ||
                 '                         AND prmtn_stop_date < pgm_end_date THEN prmtn_stop_date ELSE shpmt_end_date END))' ||
                 CHR(10) || '                as end_date' || CHR(10) ||
                 '         FROM OPT_PRMTN_DIM' || CHR(10) ||
                 '         WHERE prmtn_sttus_code in (''Approved'', ''Confirmed'', ''Completed'')' ||
                 CHR(10) || '         AND ' ||
                 REPLACE(V_BUS_UNIT_FLTR, '[A].', '') || CHR(10) ||
                 '         AND nvl(prmtn_stop_date, pgm_end_date) >= pgm_start_date' ||
                 CHR(10) || '        ) prom,' || CHR(10) ||
                 '        -- products with max promotion length derived' ||
                 CHR(10) ||
                 '        (SELECT /*+ NO_MERGE ORDERED USE_HASH(cat prod) */' ||
                 CHR(10) || '           prod.prmtn_skid,' || CHR(10) ||
                 '           prod.prod_skid,' || CHR(10) ||
                 '           cat.max_prmtn_lngth' || CHR(10) || '         FROM' ||
                 CHR(10) ||
                 '           -- category level max promotion length defaults' ||
                 CHR(10) ||
                 '           (SELECT /*+ NO_MERGE ORDERED USE_HASH(bu cat) */' ||
                 CHR(10) || '              cat.prod_skid,' || CHR(10) ||
                 '              nvl(cat.max_prmtn_lngth, bu.max_prmtn_lngth) as max_prmtn_lngth' ||
                 CHR(10) || '            FROM' || CHR(10) ||
                 '              -- business unit max promotion length defaults' ||
                 CHR(10) || '              (SELECT' || CHR(10) ||
                 '                 bu.bus_unit_skid,' || CHR(10) ||
                 '                 nvl(d.max_prmtn_lngth, g.max_prmtn_lngth) as max_prmtn_lngth' ||
                 CHR(10) ||
                 '               FROM OPT_BUS_UNIT_DIM bu, OPT_BUS_UNIT_PLC g, OPT_BUS_UNIT_PLC d' ||
                 CHR(10) || '               WHERE g.bus_unit_skid = 0' ||
                 CHR(10) || '               AND d.deflt_lvl_name is not null' ||
                 CHR(10) || '               AND d.bus_unit_skid (+) <> 0' ||
                 CHR(10) ||
                 '               AND bu.bus_unit_skid = d.bus_unit_skid (+)' ||
                 CHR(10) || '              ) bu,' || CHR(10) ||
                 '              (SELECT /*+ NO_MERGE */' || CHR(10) ||
                 '                 h.prod_skid, h.bus_unit_skid, max(cat.max_prmtn_lngth_qty) keep (dense_rank first order by h.fy_date_skid desc) as max_prmtn_lngth' ||
                 CHR(10) ||
                 '               FROM OPT_PROD_HIER_FDIM h, OPT_BASLN_CATEG_PARM_PLC cat' ||
                 CHR(10) || '               WHERE ' ||
                 REPLACE(V_BUS_UNIT_FLTR, '[A]', 'h') || CHR(10) ||
                 '               AND h.prod_lvl_desc = ''Brand''' || CHR(10) ||
                 '               AND h.prod_4_bus_unit_skid = cat.bus_unit_skid (+)' ||
                 CHR(10) ||
                 '               AND h.prod_4_name = cat.prod_name (+)' ||
                 CHR(10) ||
                 '               group by h.prod_skid, h.bus_unit_skid' ||
                 CHR(10) || '              ) cat' || CHR(10) ||
                 '            WHERE cat.bus_unit_skid = bu.bus_unit_skid' ||
                 CHR(10) || '           ) cat, ' || CHR(10) ||
                 '           (SELECT /*+ NO_MERGE ORDERED USE_HASH(p pp) */' ||
                 CHR(10) ||
                 '              distinct pp.prmtn_skid, p.prod_5_skid as prod_skid, pp.bus_unit_skid' ||
                 CHR(10) ||
                 '            FROM OPT_PROD_HIER_FDIM p, OPT_PRMTN_PROD_FCT pp' ||
                 CHR(10) || '            WHERE pp.prod_skid = p.prod_skid' ||
                 CHR(10) || '            AND pp.fy_date_skid = p.fy_date_skid' ||
                 CHR(10) ||
                 '            AND pp.bus_unit_skid = p.bus_unit_skid' ||
                 CHR(10) || '           ) prod' || CHR(10) || '         WHERE ' ||
                 REPLACE(V_BUS_UNIT_FLTR, '[A]', 'prod') || CHR(10) ||
                 '         AND prod.prod_skid = cat.prod_skid' || CHR(10) ||
                 '        ) prod' || CHR(10) ||
                 '      WHERE prom.prmtn_skid = prod.prmtn_skid' || CHR(10) ||
                 '      AND prom.end_date - prom.start_date < prod.max_prmtn_lngth' ||
                 CHR(10) || '     ) prom,' || CHR(10) ||
                 '    -- account/product filtering: only take shipped records' ||
                 CHR(10) ||
                 '    (SELECT /*+ NO_MERGE */ distinct acct_skid, prod_skid FROM OPT_SHPMT_BRAND_WK_FCT) s,' ||
                 CHR(10) || '    -- calendar for weeks derivation' || CHR(10) ||
                 '    (SELECT /*+ NO_MERGE */ distinct wk_skid, wk_start_date, wk_end_date' ||
                 CHR(10) || '     FROM CAL_MASTR_DIM' || CHR(10) ||
                 '     WHERE day_date BETWEEN add_months(trunc(sysdate+6, ''mm''), -24) AND last_day(add_months(sysdate+6, -1))) c' ||
                 CHR(10) || 'WHERE h.acct_type_desc = ''Ship-to location''' ||
                 CHR(10) || 'AND h.root_acct_skid = prom.acct_skid' || CHR(10) ||
                 'AND prom.start_date <= c.wk_end_date' || CHR(10) ||
                 'AND prom.end_date >= c.wk_start_date' || CHR(10) ||
                 'AND s.acct_skid = h.acct_skid' || CHR(10) ||
                 'AND s.prod_skid = prom.prod_skid';
        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Table created successfuly (' || SQL%ROWCOUNT ||
                         ' rows processed).');
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table ' ||
                             C_POST_BASLN_TEMP1_TBL_NAME ||
                             ' failed with error - ' || SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table ' ||
                                         C_POST_BASLN_TEMP1_TBL_NAME ||
                                         ' failed with error - ' || SQLERRM);
        END;

        -- statistics
        PRO_PUT_LINE('Starting gathering statistics process for ' ||
                     C_POST_BASLN_TEMP1_TBL_NAME ||
                     ' table (estimate percent = ' || C_STATS_TEMP_EST_PERCENT ||
                     ')...');
        DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                             'CURRENT_SCHEMA'),
                                      TABNAME => C_POST_BASLN_TEMP1_TBL_NAME,
                                      ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                      METHOD_OPT => 'FOR COLUMNS SIZE 1',
                                      CASCADE => FALSE);
        PRO_PUT_LINE('Statistics gathered.');

        -- prepare temporary table create statement (final table - partitionned)
				-- Optima11,Oracle11g ORA-600 Bug-fix,4-Oct-2010,JimmyChen  -- Start
        -- removed the parallel parameter when create partition table to avoid ORA-600 error ' PARALLEL ' || P_DOP || CHR(10) || 
				-- Optima11,Oracle11g ORA-600 Bug-fix,4-Oct-2010,JimmyChen  -- End
        DROP_TABLE(C_POST_BASLN_TEMP2_TBL_NAME);
        V_SQL := 'CREATE TABLE ' || C_POST_BASLN_TEMP2_TBL_NAME || CHR(10) ||
                 C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ' ' ||
				 ' PARTITION BY RANGE (WK_SKID)' || CHR(10) ||
                 '(PARTITION "PMAX" VALUES LESS THAN (MAXVALUE) ' ||
                 C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ')' ||
                 CHR(10) || 'AS' || CHR(10) ||
                 '  SELECT WK_SKID, PROD_SKID, ACCT_SKID, BASLN_SU_AMT' ||
                 CHR(10) || '  FROM OPT_POST_BASLN_FCT' || CHR(10) ||
                 '  WHERE rownum < 1';
        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            -- initialize partitions
            OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN(P_VC2_TBL_NAME => C_POST_BASLN_TEMP2_TBL_NAME,
                                                        P_NUM_PAST_MTH_NUM => P_MTH_CNT + 1,
                                                         /* additional partition created for the aims of storing old data that must be added */
                                                        P_NUM_FUTURE_MTH_NUM => 0,
                                                        P_VC2_DROP_PRTTN_FLAG => 'N');
            PRO_PUT_LINE('Table created successfuly.');
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table ' ||
                             C_POST_BASLN_TEMP2_TBL_NAME ||
                             ' failed with error - ' || SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table ' ||
                                         C_POST_BASLN_TEMP2_TBL_NAME ||
                                         ' failed with error - ' || SQLERRM);
        END;
        -- renaming oldest partition
        SELECT MIN(PARTITION_NAME)
        INTO V_PRTTN_NAME
        FROM USER_TAB_PARTITIONS
        WHERE TABLE_NAME = C_POST_BASLN_TEMP2_TBL_NAME;
        V_SQL := 'ALTER TABLE ' || C_POST_BASLN_TEMP2_TBL_NAME ||
                 ' RENAME PARTITION ' || V_PRTTN_NAME || ' to ' ||
                 C_OLDEST_PRTTN_NAME;
        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        EXECUTE IMMEDIATE V_SQL;

        -- find smallest week skid
        SELECT MIN(WK_SKID)
        INTO V_MIN_WK_SKID
        FROM CAL_MASTR_DIM
        WHERE DAY_DATE BETWEEN
              ADD_MONTHS(TRUNC(SYSDATE + 6, 'mm'), -P_MTH_CNT + 1) AND
              ADD_MONTHS(LAST_DAY(SYSDATE + 6), -1);

        -- find wk_skid from two months ago
        SELECT MIN(WK_SKID)
        INTO V_SIX_MTH_AGO_SKID
        FROM CAL_MASTR_DIM
        WHERE DAY_DATE BETWEEN ADD_MONTHS(TRUNC(SYSDATE + 6, 'mm'), -7) AND
              ADD_MONTHS(TRUNC(SYSDATE + 6, 'mm'), -6) - 6;

        -- planning baseline creation
        V_SQL := 'INSERT /*+ APPEND PARALLEL(' || P_DOP || ') */ INTO ' ||
                 C_POST_BASLN_TEMP2_TBL_NAME ||
                 ' (WK_SKID, PROD_SKID, ACCT_SKID, BASLN_SU_AMT)' || CHR(10);
        IF P_USE_STAT_TBL = 'Y' THEN
            V_SQL := V_SQL || CHR(10) || '-- final filtering' || CHR(10) ||
                     'select' || CHR(10) || '  wk_skid,' || CHR(10) ||
                     '  prod_skid,' || CHR(10) || '  acct_skid,' || CHR(10) ||
                     '  basln_su_amt' || CHR(10) || 'from';
        END IF;
        V_SQL := V_SQL || CHR(10) || '  -- copy backward' || CHR(10) ||
                 '  (select' || CHR(10) || '     cf.wk_skid,' || CHR(10) ||
                 '     cf.prod_skid,' || CHR(10) || '     cf.acct_skid,' ||
                 CHR(10) || '     case' || CHR(10) ||
                 '       when cf.first_wk_skid is not null then' || CHR(10) ||
                 '         last_value(case when cf.wk_skid >= cf.first_wk_skid then cf.basln_su_amt else null end ignore nulls) over (partition by cf.prod_skid, cf.acct_skid order by cf.wk_skid desc)' ||
                 CHR(10) || '       when cf.wk_skid >= cf.first_wk_skid then' ||
                 CHR(10) || '         cf.basln_su_amt' || CHR(10) ||
                 '       else' || CHR(10) ||
                 '         max(cf.ship_su_avg) over (partition by cf.prod_skid, cf.acct_skid)' ||
                 CHR(10) || '       end' || CHR(10) || '     as basln_su_amt';
        IF P_USE_STAT_TBL = 'Y' THEN
            V_SQL := V_SQL || ',' || CHR(10) || '     s.max_wk_skid';
        END IF;
        V_SQL := V_SQL || CHR(10) || '   from';
        IF P_USE_STAT_TBL = 'Y' THEN
            V_SQL := V_SQL || CHR(10) ||
                     '     -- statistics for pairs gtin/ship-to to be joined' ||
                     CHR(10) || '     OPT_BASLN_SHPMT_STAT_PRC s,';
        END IF;
        V_SQL := V_SQL || CHR(10) || '     -- copy forward' || CHR(10) ||
                 '     (select /*+ NO_MERGE ORDERED */' || CHR(10) ||
                 '        prod_skid,' || CHR(10) || '        acct_skid,' ||
                 CHR(10) || '        wk_skid,' || CHR(10) || '        case' ||
                 CHR(10) || '          when cnt <= num_of_shpmt_wks then' ||
                 CHR(10) || '            null' || CHR(10) || '          else' ||
                 CHR(10) ||
                 '            last_value(basln_su_amt ignore nulls) over (partition by prod_skid, acct_skid order by wk_skid asc)' ||
                 CHR(10) || '          end' || CHR(10) ||
                 '        as basln_su_amt,' || CHR(10) ||
                 '        ship_su_avg,' || CHR(10) || '        cnt,' || CHR(10) ||
                 '        min(case when cnt > num_of_shpmt_wks and cnt < 9999 then wk_skid else null end) over (partition by prod_skid, acct_skid) as first_wk_skid,' ||
                 CHR(10) ||
                 '        max(last_wk_skid) over (partition by prod_skid, acct_skid) as last_wk_skid' ||
                 CHR(10) || '     from' || CHR(10) ||
                 '       (select /*+ NO_MERGE */' || CHR(10) ||
                 '          prod_skid,' || CHR(10) || '          acct_skid,' ||
                 CHR(10) || '          wk_skid,' || CHR(10) ||
                 '          basln_su_amt,' || CHR(10) ||
                 '          ship_su_avg,' || CHR(10) ||
                 '          num_of_shpmt_wks,' || CHR(10) ||
                 '          last_wk_skid,' || CHR(10) ||
                 '          count(1) over (partition by prod_skid, acct_skid order by wk_skid asc) as cnt' ||
                 CHR(10) || '        from' || CHR(10) ||
                 '           -- post-event baseline calculation' || CHR(10) ||
                 '          (select /*+ NO_MERGE ORDERED */' || CHR(10) ||
                 '             s.prod_skid,' || CHR(10) ||
                 '             s.acct_skid,' || CHR(10) ||
                 '             s.wk_skid,' || CHR(10) ||
                 '             avg(s.vol_su_amt) over (partition by s.prod_skid, s.acct_skid order by s.wk_skid asc rows between cat.num_of_shpmt_wks preceding and current row) as basln_su_amt,' ||
                 CHR(10) ||
                 '             avg(s.vol_su_amt) over (partition by s.prod_skid, s.acct_skid) as ship_su_avg,' ||
                 CHR(10) || '             cat.num_of_shpmt_wks,' || CHR(10) ||
                 '             s.last_wk_skid' || CHR(10) || '           from' ||
                 CHR(10) ||
                 '             -- obtain number of shipment weeks to use for each product' ||
                 CHR(10) ||
                 '             (SELECT /*+ NO_MERGE ORDERED USE_HASH(bu cat) */' ||
                 CHR(10) || '                cat.prod_skid,' || CHR(10) ||
                 '                nvl(cat.num_of_shpmt_wks, bu.num_of_shpmt_wks) - 1 as num_of_shpmt_wks' ||
                 CHR(10) || '              FROM' || CHR(10) ||
                 '                -- business unit max promotion length defaults' ||
                 CHR(10) || '                (SELECT' || CHR(10) ||
                 '                   bu.bus_unit_skid,' || CHR(10) ||
                 '                   nvl(d.num_of_shpmt_wks, g.num_of_shpmt_wks) as num_of_shpmt_wks' ||
                 CHR(10) ||
                 '                 FROM OPT_BUS_UNIT_DIM bu, OPT_BUS_UNIT_PLC g, OPT_BUS_UNIT_PLC d' ||
                 CHR(10) || '                 WHERE g.bus_unit_skid = 0' ||
                 CHR(10) || '                 AND d.deflt_lvl_name is not null' ||
                 CHR(10) || '                 AND d.bus_unit_skid (+) <> 0' ||
                 CHR(10) ||
                 '                 AND bu.bus_unit_skid = d.bus_unit_skid (+)' ||
                 CHR(10) || '                ) bu,' || CHR(10) ||
                 '                (SELECT /*+ NO_MERGE */' || CHR(10) ||
                 '                   h.prod_skid, h.bus_unit_skid, max(cat.num_of_shpmt_wks_qty) keep (dense_rank first order by h.fy_date_skid desc) as num_of_shpmt_wks' ||
                 CHR(10) ||
                 '                 FROM OPT_PROD_HIER_FDIM h, OPT_BASLN_CATEG_PARM_PLC cat' ||
                 CHR(10) || '                 WHERE ' ||
                 REPLACE(V_BUS_UNIT_FLTR, '[A]', 'h') || CHR(10) ||
                 '                 AND h.prod_lvl_desc = ''Brand''' || CHR(10) ||
                 '                 AND h.prod_4_bus_unit_skid = cat.bus_unit_skid (+)' ||
                 CHR(10) ||
                 '                 AND h.prod_4_name = cat.prod_name (+)' ||
                 CHR(10) ||
                 '                 group by h.prod_skid, h.bus_unit_skid' ||
                 CHR(10) || '                ) cat' || CHR(10) ||
                 '              WHERE cat.bus_unit_skid = bu.bus_unit_skid' ||
                 CHR(10) || '             ) cat,' || CHR(10) ||
                 '             -- zero-shipments derived with promo-weeks substracted' ||
                 CHR(10) ||
                 '             (select /*+ NO_MERGE USE_MERGE(s1 s2) */' ||
                 CHR(10) || '                s1.prod_skid,' || CHR(10) ||
                 '                s1.acct_skid,' || CHR(10) ||
                 '                s1.wk_skid,' || CHR(10) ||
                 '                nvl(s2.vol_su_amt,0) as vol_su_amt,' ||
                 CHR(10) || '                s1.last_wk_skid' || CHR(10) ||
                 '              from' || CHR(10) ||
                 '                -- time gaps filling' || CHR(10) ||
                 '                (select /*+ USE_MERGE(s c) */' || CHR(10) ||
                 '                   s.prod_skid,' || CHR(10) ||
                 '                   s.acct_skid,' || CHR(10) ||
                 '                   c.wk_skid,' || CHR(10) ||
                 '                   s.first_wk_skid,' || CHR(10) ||
                 '                   s.last_wk_skid' || CHR(10) ||
                 '                 from' || CHR(10) ||
                 '                   -- calendar' || CHR(10) ||
                 '                   (select' || CHR(10) ||
                 '                      distinct wk_skid' || CHR(10) ||
                 '                    from CAL_MASTR_DIM' || CHR(10) ||
                 '                    where day_date between add_months(trunc(sysdate+6, ''mm''), -24) and add_months(last_day(sysdate+6), -1)' ||
                 CHR(10) || '                   ) c,' || CHR(10) ||
                 '                      (select' || CHR(10) ||
                 '                         prod_skid, acct_skid,' || CHR(10) ||
                 '                         min(decode(greatest(vol_su_amt,0), 0, 9999999, wk_skid)) as first_wk_skid,' ||
                 CHR(10) ||
                 '                         max(decode(greatest(vol_su_amt,0), 0, 0, wk_skid)) as last_wk_skid' ||
                 CHR(10) ||
                 '                       from OPT_SHPMT_BRAND_WK_FCT' ||
                 CHR(10) ||
                 '                       group by prod_skid, acct_skid' ||
                 CHR(10) || '                      ) s' || CHR(10) ||
                 '                 -- ignore leading and ending zeros' ||
                 CHR(10) ||
                 '                 where c.wk_skid between s.first_wk_skid and s.last_wk_skid' ||
                 CHR(10) || '                ) s1,' || CHR(10) ||
                 '             ( select prod_skid, acct_skid, wk_skid, nvl(vol_su_amt,0) as vol_su_amt ' ||
                 CHR(10) || '                  from OPT_SHPMT_BRAND_WK_FCT) s2' ||
                 CHR(10) ||
                 '              where s1.prod_skid = s2.prod_skid(+) ' ||
                 CHR(10) ||
                 '                and s1.acct_skid = s2.acct_skid(+) ' ||
                 CHR(10) || '                and s1.wk_skid = s2.wk_skid(+) ' ||
                 CHR(10) ||
                 '                and (s1.prod_skid, s1.acct_skid, s1.wk_skid) not in (' ||
                 CHR(10) || '                -- substract promo weeks' ||
                 CHR(10) ||
                 '                select prod_skid, acct_skid, wk_skid' ||
                 CHR(10) || '                from ' ||
                 C_POST_BASLN_TEMP1_TBL_NAME || ')' || CHR(10) ||
                 '             ) s' || CHR(10) ||
                 '             where s.prod_skid = cat.prod_skid)' || CHR(10) ||
                 '        union all' || CHR(10) ||
                 '        -- empty promo weeks with GTIN history maintanance' ||
                 CHR(10) || '        select' || CHR(10) ||
                 '          prod_skid,' || CHR(10) || '          acct_skid,' ||
                 CHR(10) || '          wk_skid,' || CHR(10) ||
                 '          null as basln_su_amt,' || CHR(10) ||
                 '          null as ship_su_avg,' || CHR(10) ||
                 '          9998 as num_of_shpmt_wks,' || CHR(10) ||
                 '          null as last_wk_skid,' || CHR(10) ||
                 '          9999 as cnt' || CHR(10) || '        from ' ||
                 C_POST_BASLN_TEMP1_TBL_NAME || ' e' || CHR(10) || '      )' ||
                 CHR(10) || '    ) cf' || CHR(10) ||
                 '   /* this line ignores leading promotions (when there was no shipments) */' ||
                 CHR(10) ||
                 '   where not (cf.basln_su_amt is null and cf.cnt = 9999)' ||
                 CHR(10) ||
                 '   /* this line ignores ending promotions (when there was not shipments) */' ||
                 CHR(10) || '   and wk_skid <= last_wk_skid';
        IF P_USE_STAT_TBL = 'Y' THEN
            V_SQL := V_SQL || CHR(10) ||
                     '   and cf.acct_skid = s.acct_skid (+)' || CHR(10) ||
                     '   and cf.prod_skid = s.prod_skid (+)';
        END IF;
        V_SQL := V_SQL || CHR(10) || '  )';
        IF P_USE_STAT_TBL = 'Y' THEN
            V_SQL := V_SQL || CHR(10) ||
                     'where max_wk_skid is null or wk_skid >= least(max_wk_skid, ' ||
                     V_MIN_WK_SKID || ')';
        END IF;

        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Data created successfuly (' || SQL%ROWCOUNT ||
                         ' rows processed).');
            COMMIT;
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table ' ||
                             C_POST_BASLN_TEMP2_TBL_NAME ||
                             ' failed with error - ' || SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table ' ||
                                         C_POST_BASLN_TEMP2_TBL_NAME ||
                                         ' failed with error - ' || SQLERRM);
        END;

        -- Recreating OPT_BASLN_SHPMT_STAT_PRC table
        IF P_USE_STAT_TBL = 'Y' THEN
            IF P_MTH_CNT >= 24 THEN
                -- full refresh
                V_SQL := 'INSERT /*+ APPEND PARALLEL(' || P_DOP ||
                         ') */ INTO OPT_BASLN_SHPMT_STAT_PRC (prod_skid, acct_skid, min_wk_skid, max_wk_skid)' ||
                         CHR(10) ||
                         'SELECT prod_skid, acct_skid, min(wk_skid), max(wk_skid) FROM ' ||
                         C_POST_BASLN_TEMP2_TBL_NAME ||
                         ' GROUP BY prod_skid, acct_skid';
            ELSE
                -- incremental refresh
                V_SQL := 'INSERT /*+ APPEND PARALLEL(' || P_DOP ||
                         ') */ INTO OPT_BASLN_SHPMT_STAT_PRC (PROD_SKID, ACCT_SKID, MIN_WK_SKID, MAX_WK_SKID)' ||
                         CHR(10) || 'WITH' || CHR(10) || '  inc_stat AS (' ||
                         CHR(10) || '    SELECT' || CHR(10) ||
                         '      prod_skid, acct_skid,' || CHR(10) ||
                         '      MIN(wk_skid) AS min_wk_skid, MAX(wk_skid) AS max_wk_skid' ||
                         CHR(10) || '    FROM ' || C_POST_BASLN_TEMP2_TBL_NAME ||
                         CHR(10) || '    GROUP BY prod_skid, acct_skid)' ||
                         CHR(10) || 'SELECT' || CHR(10) ||
                         '  NVL(curr_stat.prod_skid, inc_stat.prod_skid) AS prod_skid,' ||
                         CHR(10) ||
                         '  NVL(curr_stat.acct_skid, inc_stat.acct_skid) AS acct_skid,' ||
                         CHR(10) ||
                         '  LEAST(NVL(curr_stat.min_wk_skid, inc_stat.min_wk_skid),' ||
                         CHR(10) ||
                         '        NVL(inc_stat.min_wk_skid, curr_stat.min_wk_skid)) AS min_wk_skid,' ||
                         CHR(10) ||
                         '  GREATEST(NVL(curr_stat.max_wk_skid, inc_stat.max_wk_skid),' ||
                         CHR(10) ||
                         '           NVL(inc_stat.max_wk_skid, curr_stat.max_wk_skid)) AS max_wk_skid' ||
                         CHR(10) ||
                         'FROM OPT_BASLN_SHPMT_STAT_PRC curr_stat FULL OUTER JOIN inc_stat' ||
                         CHR(10) ||
                         'ON curr_stat.prod_skid = inc_stat.prod_skid' ||
                         CHR(10) ||
                         'AND curr_stat.acct_skid = inc_stat.acct_skid';
            END IF;
            PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                         REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
            BEGIN
                EXECUTE IMMEDIATE V_SQL;
                PRO_PUT_LINE('Data created successfuly (' || SQL%ROWCOUNT ||
                             ' rows processed).');
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    PRO_PUT_LINE('Error: Preparing data for table OPT_BASLN_SHPMT_STAT_PRC failed with error - ' ||
                                 SQLERRM);
                    RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                            'Error: Preparing data for table OPT_BASLN_SHPMT_STAT_PRC failed with error - ' ||
                                             SQLERRM);
            END;

            -- statistics
            PRO_PUT_LINE('Starting gathering statistics process for OPT_BASLN_SHPMT_STAT_PRC table (estimate percent = ' ||
                         C_STATS_TEMP_EST_PERCENT || ')...');
            DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                                 'CURRENT_SCHEMA'),
                                          TABNAME => 'OPT_BASLN_SHPMT_STAT_PRC',
                                          ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                          METHOD_OPT => 'FOR COLUMNS SIZE 1',
                                          CASCADE => FALSE);
            PRO_PUT_LINE('Statistics gathered.');
        END IF;

        -- promote and drop P000000 partition
        V_SQL := 'INSERT /*+ APPEND PARALLEL(' || P_DOP ||
                 ') */ INTO OPT_POST_BASLN_FCT (wk_skid, prod_skid, acct_skid, basln_su_amt)' ||
                 CHR(10) ||
                 'SELECT wk_skid, prod_skid, acct_skid, basln_su_amt FROM ' ||
                 C_POST_BASLN_TEMP2_TBL_NAME || ' PARTITION (' ||
                 C_OLDEST_PRTTN_NAME || ')';
        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Data created successfuly (' || SQL%ROWCOUNT ||
                         ' rows processed).');
            COMMIT;
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Preparing data for table OPT_POST_BASLN_FCT failed with error - ' ||
                             SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Preparing data for table OPT_POST_BASLN_FCT failed with error - ' ||
                                         SQLERRM);
        END;
        V_SQL := 'ALTER TABLE ' || C_POST_BASLN_TEMP2_TBL_NAME ||
                 ' DROP PARTITION ' || C_OLDEST_PRTTN_NAME;
        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Partition dropped successfuly.');
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Droping partition ' || C_OLDEST_PRTTN_NAME ||
                             ' of table ' || C_POST_BASLN_TEMP2_TBL_NAME ||
                             ' failed with error - ' || SQLERRM);
                RAISE_APPLICATION_ERROR(C_DROP_PRTTN_ERROR,
                                        'Droping partition ' ||
                                         C_OLDEST_PRTTN_NAME || ' of table ' ||
                                         C_POST_BASLN_TEMP2_TBL_NAME ||
                                         ' failed with error - ' || SQLERRM);
        END;

        -- promote rest of data to final OPT_POST_BASLN_FCT table
        BEGIN
            PRO_PUT_LINE('Starting data promotion from ' ||
                         C_POST_BASLN_TEMP2_TBL_NAME ||
                         ' to OPT_POST_BASLN_FCT...');
            -- create missing partitions
            OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN(P_VC2_TBL_NAME => 'OPT_POST_BASLN_FCT',
                                                        P_NUM_PAST_MTH_NUM => 24,
                                                        P_NUM_FUTURE_MTH_NUM => 0,
                                                        P_VC2_DROP_PRTTN_FLAG => 'N');
            -- promote data
            OPT_PKG_PRTTN_UTIL.PRO_PROMOTE_DATA(P_VC2_SRCE_TBL => C_POST_BASLN_TEMP2_TBL_NAME,
                                                P_VC2_DEST_TBL => 'OPT_POST_BASLN_FCT',
                                                P_VC2_MIN_CAL_SKID => NULL,
                                                P_VC2_REGION => '',
                                                P_VC2_CTRLM_ORDR_ID => P_ORDER_ID,
                                                P_VC2_PRTTN_TYPE_CODE => 'M',
												V_REGN => V_REGN);--SOX IMPLEMENTATION PROJECT
            PRO_PUT_LINE('Data promoted succesfully.');
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Promotion of data from ' ||
                             C_POST_BASLN_TEMP2_TBL_NAME ||
                             ' to OPT_POST_BASLN_FCT failed with error - ' ||
                             SQLERRM);
                RAISE_APPLICATION_ERROR(C_DATA_PROMO_ERROR,
                                        'Error: Promotion of data from ' ||
                                         C_POST_BASLN_TEMP2_TBL_NAME ||
                                         ' to OPT_POST_BASLN_FCT failed with error - ' ||
                                         SQLERRM);
        END;

        -- drop temporary tables
        --drop_table(c_post_basln_temp1_tbl_name);
        --drop_table(c_post_basln_temp2_tbl_name);
        --drop_table(c_ship_brand_agg_temp_tbl_name);

        -- gather statistics for all involved partitions
        PRO_PUT_LINE('Starting gathering statistics on OPT_POST_BASLN_FCT table...');
        DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                             'CURRENT_SCHEMA'),
                                      TABNAME => 'OPT_POST_BASLN_FCT',
                                      GRANULARITY => 'GLOBAL',
                                      ESTIMATE_PERCENT => C_STATS_FINAL_EST_PERCENT,
                                      BLOCK_SAMPLE => FALSE, DEGREE => 1,
                                      METHOD_OPT => 'FOR ALL COLUMNS SIZE AUTO');
        PRO_PUT_LINE('Statistics on table OPT_POST_BASLN_FCT table gathered.');
        PRO_PUT_LINE('Starting gathering statistics on refreshed partitions of OPT_POST_BASLN_FCT table...');
        FOR P IN (SELECT OBJECT_NAME,
                         SUBOBJECT_NAME
                  FROM USER_OBJECTS
                  WHERE OBJECT_NAME = 'OPT_POST_BASLN_FCT'
                  AND OBJECT_TYPE = 'TABLE PARTITION'
                  AND LAST_DDL_TIME >= V_START_DATE)
        LOOP
            PRO_PUT_LINE('Gathering statistics for ' || P.OBJECT_NAME || '.' ||
                         P.SUBOBJECT_NAME);
            DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                                 'CURRENT_SCHEMA'),
                                          TABNAME => P.OBJECT_NAME,
                                          PARTNAME => P.SUBOBJECT_NAME,
                                          GRANULARITY => 'PARTITION',
                                          ESTIMATE_PERCENT => C_STATS_FINAL_EST_PERCENT,
                                          BLOCK_SAMPLE => FALSE, DEGREE => 1,
                                          METHOD_OPT => 'FOR ALL COLUMNS SIZE AUTO');
        END LOOP;
    EXCEPTION
        WHEN OTHERS THEN
            PRO_PUT_LINE('Error: Unexpected system error - ' || SQLERRM);
            RAISE;
    END;

    /************************************************************************************/
    /* name: plan_basln_calc                                                            */
    /* purpose: calculates final planning baseline                                      */
    /* parameters:                                                                      */
    /*   p_order_id - control m order id                                                */
    /*   p_gen_mth - number of months to be calculated                                  */
    /*   p_dop - degree of parallelism to be used, default is 4                         */
    /*   p_copy_mth - number of months to be copied forward                             */
    /* author: bazyli blicharski                                                        */
    /* version: 1.00 - initial version                                                  */
    /* version: 1.01 - Updated by Myra for new logic in Optima8 on DEC 11th 2008        */
    /* version: 1.02 - Updated by Myra for performance tunning on 2009-02-02            */
    /* version: 1.03 - Updated by David for performance tunning on 2009-03-02           */
    /* version: 1.04 - convert parallel hints to degree 2 on 2009-06-09                 */
    /* version: 2.00 - Updated by Daivd for defect fix in B016 of R10 on 2010-3-12  */
    /* version: 2.01 - Updated by Daivd for performance tuning in B016 of R10 on 2010-3-16  */
    /* version: 2.02 - Updated by David for issue fixing in R10 on 2010-04-18 */
    /* version: 2.1 - Updated by Dawid Glowacki for R13 - copy forward 2012-05-28 */
    /************************************************************************************/
    PROCEDURE PLAN_BASLN_CALC(P_ORDER_ID IN VARCHAR2,
                              P_GEN_MTH IN NUMBER,
                              P_DOP IN NUMBER DEFAULT 2,
                              P_COPY_MTH IN NUMBER DEFAULT 0,
							  V_REGN VARCHAR2 DEFAULT NULL -- SOX IMPLEMENTATION PROJECT
                              ) IS
        -- types
        TYPE T_HASH_VARCHAR2_TABLE IS TABLE OF INTEGER INDEX BY VARCHAR2(10);
        -- variables
        V_START_DATE DATE := SYSDATE;
        V_TBLSPACE_NAME VARCHAR2(100) := '';
        V_STRATEGIES T_HASH_VARCHAR2_TABLE;
        V_CYTD_MIN_MTH_SKID CAL_MASTR_DIM.MTH_SKID%TYPE;
        V_CYTD_MAX_MTH_SKID CAL_MASTR_DIM.MTH_SKID%TYPE;
        V_CYTDYA_MIN_MTH_SKID CAL_MASTR_DIM.MTH_SKID%TYPE;
        V_CYTDYA_MAX_MTH_SKID CAL_MASTR_DIM.MTH_SKID%TYPE;
        V_SIX_MTH_AGO_SKID CAL_MASTR_DIM.WK_SKID%TYPE;
        V_SQL VARCHAR2(32767);
        --V_AGG1_SQL VARCHAR2(32767);
        V_AGG2_SQL VARCHAR2(32767);
        V_GROWTH_SQL_TMPLT VARCHAR2(32767);
        V_NOGROWTH_SQL_TMPLT VARCHAR2(32767);
        V_AVG_SQL_TMPLT VARCHAR2(32767);
        V_CURR_STRTGY VARCHAR2(10);
        V_BUS_UNIT_FLTR VARCHAR2(32767);
        --Added by David for performance tunning
        V_PROD_HIST_SQL VARCHAR2(32767);
        V_SHPMT_PAST_MTHS_SQL VARCHAR2(32767);
        V_GTIN_ACCT_AGG_SQL VARCHAR2(32767);
        V_P_DOP NUMBER := 2;

    BEGIN
        -- parameter control
        PRO_PUT_LINE('Checking input parameters:' || CHR(10) ||
                     '  p_order_id = ' || P_ORDER_ID || CHR(10) ||
                     '  p_gen_mth = ' || P_GEN_MTH || CHR(10) || '  p_dop = ' ||
                     V_P_DOP);
        IF P_ORDER_ID IS NULL OR P_GEN_MTH IS NULL OR
           P_GEN_MTH NOT BETWEEN 1 AND 12 OR P_DOP IS NULL OR P_DOP < 1 THEN
            PRO_PUT_LINE('Error: wrong input parameters!');
            RAISE_APPLICATION_ERROR(C_WRONG_PARMS_ERROR,
                                    'Error: wrong input parameters!');
        END IF;

        -- find a tablespace
        BEGIN
            SELECT 'TABLESPACE ' || TABLESPACE_NAME
            INTO V_TBLSPACE_NAME
            FROM USER_TAB_PARTITIONS
            WHERE TABLE_NAME = 'OPT_POST_BASLN_FCT'
            AND ROWNUM = 1;
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Warning: unable to recognize OPT_POST_BASLN_FCT tablespace, choosing default tablespace!');
        END;

        -- business unit filter
        V_BUS_UNIT_FLTR := GET_BUS_UNIT_FILTR();

        -- find wk_skid from two months ago
        SELECT MIN(WK_SKID)
        INTO V_SIX_MTH_AGO_SKID
        FROM CAL_MASTR_DIM
        WHERE DAY_DATE BETWEEN ADD_MONTHS(TRUNC(SYSDATE + 6, 'mm'), -7) AND
              ADD_MONTHS(TRUNC(SYSDATE + 6, 'mm'), -6) - 6;

        -- drop temporary tables
        DROP_TABLE(C_PLAN_BASLN_TEMP1_TBL_NAME);
        DROP_TABLE(C_PLAN_BASLN_TEMP2_TBL_NAME);
        DROP_TABLE(C_GTIN_PLAN_BASLN_TEMP); --
        DROP_TABLE(C_GTIN_WEIGHT_TEMP_TBL_NAME);

        -- trick to avoid Oracle Bug 4639977 (fixed in 10.2.0.3)
        V_SQL := 'alter session set "_optimizer_connect_by_cost_based" = false';
        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' || V_SQL);
        EXECUTE IMMEDIATE V_SQL;

        -- prepare temporary table c_plan_basln_temp2_tbl_name create statement (final table - partitionned)
				-- Optima11,Oracle11g ORA-600 Bug-fix,4-Oct-2010,JimmyChen  -- Start
        -- removed the parallel parameter when create partition table to avoid ORA-600 error ' PARALLEL ' || P_DOP || CHR(10) || 
				-- Optima11,Oracle11g ORA-600 Bug-fix,4-Oct-2010,JimmyChen  -- End
        DROP_TABLE(C_PLAN_BASLN_TEMP2_TBL_NAME);
        V_SQL := 'CREATE TABLE ' || C_PLAN_BASLN_TEMP2_TBL_NAME || CHR(10) ||
                 C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ' ' ||
				 ' PARTITION BY RANGE (MTH_SKID)' || CHR(10) ||
                 '(PARTITION "PMAX" VALUES LESS THAN (MAXVALUE) ' ||
                 C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ')' ||
                 CHR(10) || 'AS' || CHR(10) ||
                 '  SELECT MTH_SKID, PROD_SKID, ACCT_SKID, BASLN_SU_AMT' ||
                 CHR(10) || '  FROM OPT_PLAN_BASLN_FCT' || CHR(10) ||
                 '  WHERE rownum < 1';
        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            -- initialize partitions
            OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN(P_VC2_TBL_NAME => C_PLAN_BASLN_TEMP2_TBL_NAME,
                                                        P_NUM_PAST_MTH_NUM => 0,
                                                        P_NUM_FUTURE_MTH_NUM => P_GEN_MTH,
                                                         /* additional partition created for the aims of storing old data that must be added */
                                                        P_VC2_DROP_PRTTN_FLAG => 'N');
            PRO_PUT_LINE('Table created successfuly.');
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table ' ||
                             C_PLAN_BASLN_TEMP2_TBL_NAME ||
                             ' failed with error - ' || SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table ' ||
                                         C_PLAN_BASLN_TEMP2_TBL_NAME ||
                                         ' failed with error - ' || SQLERRM);
        END;

        -- prepare temporary table c_gtin_plan_basln_temp create statement (final table - partitionned)
				-- Optima11,Oracle11g ORA-600 Bug-fix,4-Oct-2010,JimmyChen  -- Start
        -- removed the parallel parameter when create partition table to avoid ORA-600 error ' PARALLEL ' || P_DOP || CHR(10) || 
				-- Optima11,Oracle11g ORA-600 Bug-fix,4-Oct-2010,JimmyChen  -- End
        DROP_TABLE(C_GTIN_PLAN_BASLN_TEMP);
        V_SQL := 'CREATE TABLE ' || C_GTIN_PLAN_BASLN_TEMP || CHR(10) ||
                 C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ' ' ||
				 ' PARTITION BY RANGE (MTH_SKID)' || CHR(10) ||
                 '(PARTITION "PMAX" VALUES LESS THAN (MAXVALUE) ' ||
                 C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ')' ||
                 CHR(10) || 'AS' || CHR(10) ||
                 '  SELECT MTH_SKID, PROD_SKID, ACCT_SKID, BASLN_SU_AMT' ||
                 CHR(10) || '  FROM OPT_PLAN_BASLN_FCT' || CHR(10) ||
                 '  WHERE rownum < 1';
        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            -- initialize partitions
            OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN(P_VC2_TBL_NAME => C_GTIN_PLAN_BASLN_TEMP,
                                                        P_NUM_PAST_MTH_NUM => 0,
                                                        P_NUM_FUTURE_MTH_NUM => P_GEN_MTH + P_COPY_MTH,
                                                         /* additional partition created for the aims of storing old data that must be added */
                                                        P_VC2_DROP_PRTTN_FLAG => 'N');
            PRO_PUT_LINE('Table created successfuly.');
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table ' ||
                             C_GTIN_PLAN_BASLN_TEMP || ' failed with error - ' ||
                             SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table ' ||
                                         C_GTIN_PLAN_BASLN_TEMP ||
                                         ' failed with error - ' || SQLERRM);
        END;
        -- prepare temporary table create statement (final table - partitionned)
        -- added by myra for R8 on 2009-02-02
        /*DROP_TABLE(C_POST_GTIN_MTH_TEMP_TBL_NAME);
        V_SQL := 'CREATE TABLE ' || C_POST_GTIN_MTH_TEMP_TBL_NAME || CHR(10) ||
                 '(mth_skid number(15), prod_skid number(15), brand_skid number(15), gtin_sttus_code varchar2(60), acct_skid number(15), past_mth_cnt number(15), vol_su_amt number(22,7), gtin_weight number(22,7) )' ||
                 CHR(10) || C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME ||
                 ' PARALLEL ' || V_P_DOP || CHR(10) ||
                 'PARTITION BY RANGE (MTH_SKID)' || CHR(10) ||
                 '(PARTITION "PMAX" VALUES LESS THAN (MAXVALUE) ' ||
                 C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ')';

        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            -- initialize partitions
            OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN(P_VC2_TBL_NAME => C_POST_GTIN_MTH_TEMP_TBL_NAME,
                                                        P_NUM_PAST_MTH_NUM => 24,
                                                        P_NUM_FUTURE_MTH_NUM => 0,
                                                         \* additional partition created for the aims of storing old data that must be added *\
                                                        P_VC2_DROP_PRTTN_FLAG => 'N');
            PRO_PUT_LINE('Table created successfuly.');
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table ' ||
                             C_POST_GTIN_MTH_TEMP_TBL_NAME ||
                             ' failed with error - ' || SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table ' ||
                                         C_POST_GTIN_MTH_TEMP_TBL_NAME ||
                                         ' failed with error - ' || SQLERRM);
        END;*/

        /*************************************************************************/
        /* iterate trough all available planning baseline calculation strategies */
        /*************************************************************************/
        PRO_PUT_LINE('Checking for Planning Baseline calculation strategies (Business Unit level)...');
        FOR S IN (SELECT BU.BUS_UNIT_SKID,
                         NVL(D.STRTGY_CODE, G.STRTGY_CODE) AS STRTGY_CODE,
                         NVL(D.AVG_MTHS_CNT, G.AVG_MTHS_CNT) AS AVG_MTHS_CNT
                  FROM OPT_BUS_UNIT_PLC D,
                       OPT_BUS_UNIT_PLC G,
                       OPT_BUS_UNIT_DIM BU
                  WHERE BU.BUS_UNIT_SKID = D.BUS_UNIT_SKID(+)
                  AND BU.BUS_UNIT_SKID <> 0
                  AND G.BUS_UNIT_SKID = 0)
        LOOP
            IF S.STRTGY_CODE <> C_STRTGY_CODE_DISABLED THEN
                V_STRATEGIES(S.STRTGY_CODE) := 1;
            END IF;

            IF S.STRTGY_CODE = C_STRTGY_CODE_GROWTH THEN
                PRO_PUT_LINE('Organisation with BUS_UNIT_SKID = ' ||
                             S.BUS_UNIT_SKID || ': GR to be calculated');
            ELSIF S.STRTGY_CODE = C_STRTGY_CODE_NOGROWTH THEN
                PRO_PUT_LINE('Organisation with BUS_UNIT_SKID = ' ||
                             S.BUS_UNIT_SKID || ': GR equal 1');
            ELSIF S.STRTGY_CODE = C_STRTGY_CODE_AVG THEN
                PRO_PUT_LINE('Organisation with BUS_UNIT_SKID = ' ||
                             S.BUS_UNIT_SKID || ': Average used (' ||
                             S.AVG_MTHS_CNT || ' months)');
            ELSE
                PRO_PUT_LINE('Organisation with BUS_UNIT_SKID = ' ||
                             S.BUS_UNIT_SKID ||
                             ': Planning Baseline is disabled!');
            END IF;
        END LOOP;

        PRO_PUT_LINE('Checking for Planning Baseline calculation strategies (Category level)...');
        FOR S IN (SELECT P.PROD_SKID,
                         P.PROD_DESC,
                         P.BUS_UNIT_SKID,
                         C.STRTGY_CODE,
                         C.AVG_MTHS_CNT
                  FROM OPT_BASLN_CATEG_PARM_PLC C,
                       OPT_PROD_DIM P
                  WHERE C.BUS_UNIT_SKID = P.BUS_UNIT_SKID
                  AND C.PROD_NAME = P.PROD_NAME
                  AND NVL(C.STRTGY_CODE, C.AVG_MTHS_CNT) IS NOT NULL
                  ORDER BY P.BUS_UNIT_SKID,
                           P.PROD_DESC)
        LOOP
            IF NVL(S.STRTGY_CODE, C_STRTGY_CODE_DISABLED) <>
               C_STRTGY_CODE_DISABLED THEN
                V_STRATEGIES(S.STRTGY_CODE) := 1;

                IF S.STRTGY_CODE = C_STRTGY_CODE_GROWTH THEN
                    PRO_PUT_LINE('Category "' || S.PROD_DESC ||
                                 '" (prod_skid = ' || S.PROD_SKID ||
                                 ', bus_unit_skid = ' || S.BUS_UNIT_SKID ||
                                 '): GR to be calculated');
                ELSIF S.STRTGY_CODE = C_STRTGY_CODE_NOGROWTH THEN
                    PRO_PUT_LINE('Category "' || S.PROD_DESC ||
                                 '" (prod_skid = ' || S.PROD_SKID ||
                                 ', bus_unit_skid = ' || S.BUS_UNIT_SKID ||
                                 '): GR equal 1');
                ELSIF S.STRTGY_CODE = C_STRTGY_CODE_AVG THEN
                    PRO_PUT_LINE('Category "' || S.PROD_DESC ||
                                 '" (prod_skid = ' || S.PROD_SKID ||
                                 ', bus_unit_skid = ' || S.BUS_UNIT_SKID ||
                                 '): Average used');
                END IF;
            END IF;
            IF S.AVG_MTHS_CNT IS NOT NULL THEN
                PRO_PUT_LINE('Category "' || S.PROD_DESC || '" (prod_skid = ' ||
                             S.PROD_SKID || ', bus_unit_skid = ' ||
                             S.BUS_UNIT_SKID || '): Average months count = ' ||
                             S.AVG_MTHS_CNT);
            END IF;
        END LOOP;

        -- find minimum and maximum month skids for CYTD and CYTDYA
        SELECT MIN(MTH_SKID),
               MAX(MTH_SKID) - 1
        INTO V_CYTD_MIN_MTH_SKID,
             V_CYTD_MAX_MTH_SKID
        FROM CAL_MASTR_DIM
        WHERE DAY_DATE >= ADD_MONTHS(TRUNC(SYSDATE + 6, 'MM'), -P_GEN_MTH)
        AND DAY_DATE <= TRUNC(SYSDATE + 6, 'MM');

        SELECT MIN(MTH_SKID),
               MAX(MTH_SKID) - 1
        INTO V_CYTDYA_MIN_MTH_SKID,
             V_CYTDYA_MAX_MTH_SKID
        FROM CAL_MASTR_DIM
        WHERE DAY_DATE >= ADD_MONTHS(TRUNC(SYSDATE + 6, 'MM'), -P_GEN_MTH * 2)
        AND DAY_DATE <= ADD_MONTHS(TRUNC(SYSDATE + 6, 'MM'), -P_GEN_MTH);

        /***********************************************************************************************************/
        /* prepare query with first two steps: data aggregation to default accounts and wise aggregation to months */
        /***********************************************************************************************************/
        -- part 1: temporary hierarchy clause
        /* V_AGG1_SQL := '  -- temporary denormalized hierarchy' || CHR(10) ||
                      '  ACCT_HIER as (' || CHR(10) || '    select' || CHR(10) ||
                      '      level - 1 as distance, acct_id, acct_skid, bus_unit_skid, acct_type_desc as lvl_name,' ||
                      CHR(10) ||
                      '      CONNECT_BY_ROOT acct_skid AS root_acct_skid,' ||
                      CHR(10) ||
                      '      CONNECT_BY_ROOT acct_id AS root_acct_id,' ||
                      CHR(10) ||
                      '      CONNECT_BY_ROOT acct_type_desc as root_lvl_name' ||
                      CHR(10) || '    from OPT_ACCT_DIM' || CHR(10) ||
                      '    where ' || REPLACE(V_BUS_UNIT_FLTR, '[A].', '') ||
                      CHR(10) ||
                      '    connect by prior acct_id = parnt_acct_id),' ||
                      CHR(10) || '  -- accounts with PARNT_ACCT_SKID derived' ||
                      CHR(10) || '  ACCT_HIER_SKID as (' || CHR(10) ||
                      '    select' || CHR(10) || '      a1.acct_skid,' ||
                      CHR(10) || '      a2.acct_skid as parnt_acct_skid' ||
                      CHR(10) || '    from' || CHR(10) ||
                      '      OPT_ACCT_DIM a1,' || CHR(10) || '      (' ||
                      CHR(10) || '      select acct_skid, acct_id' || CHR(10) ||
                      '      from OPT_ACCT_DIM' || CHR(10) || '      where ' ||
                      REPLACE(V_BUS_UNIT_FLTR, '[A].', '') || CHR(10) ||
                      '      ) a2' || CHR(10) || '    where ' ||
                      REPLACE(V_BUS_UNIT_FLTR, '[A].', 'a1.') || CHR(10) ||
                      '    and a1.parnt_acct_id = a2.acct_id (+)' || CHR(10) ||
                      '  ),' || CHR(10) ||
                      '  -- all accounts with GEN_BASLN_FLAG information derived' ||
                      CHR(10) || '  OPT_ACCT as (' || CHR(10) || '    select' ||
                      CHR(10) || '      a.acct_skid,' || CHR(10) ||
                      '      a.parnt_acct_skid,' || CHR(10) ||
                      '      nvl2(g.acct_skid, ''Y'', ''N'') as gen_basln_flag' ||
                      CHR(10) || '    from (' || CHR(10) ||
                      '      -- relations coming from organization level default' ||
                      CHR(10) || '      select' || CHR(10) ||
                      '        distinct case when bu.lvl_reltv_pos >= 0 then loc.acct_skid else loc.root_acct_skid end as acct_skid' ||
                      CHR(10) || '      from' || CHR(10) ||
                      '        ACCT_HIER loc,' || CHR(10) ||
                      '        -- temporary defaults for all business units' ||
                      CHR(10) || '        (select' || CHR(10) ||
                      '           bu.bus_unit_skid,' || CHR(10) ||
                      '           nvl(d.deflt_lvl_name, g.deflt_lvl_name) as deflt_lvl_name,' ||
                      CHR(10) ||
                      '           nvl(d.lvl_reltv_pos, g.lvl_reltv_pos) as lvl_reltv_pos' ||
                      CHR(10) || '         from OPT_BUS_UNIT_DIM bu,' ||
                      CHR(10) || '              OPT_BUS_UNIT_PLC g,' || CHR(10) ||
                      '              OPT_BUS_UNIT_PLC d' || CHR(10) ||
                      '         where g.bus_unit_skid = 0' || CHR(10) ||
                      '         and d.deflt_lvl_name is not null' || CHR(10) ||
                      '         and d.bus_unit_skid (+) <> 0' || CHR(10) ||
                      '         and bu.bus_unit_skid = d.bus_unit_skid (+)' ||
                      CHR(10) || '        ) bu' || CHR(10) ||
                      '      where loc.bus_unit_skid = bu.bus_unit_skid' ||
                      CHR(10) || '      and (' || CHR(10) ||
                      '        (bu.lvl_reltv_pos = 0 and loc.distance = 0 and loc.root_lvl_name = bu.deflt_lvl_name) or' ||
                      CHR(10) ||
                      '        (bu.lvl_reltv_pos > 0 and loc.distance = bu.lvl_reltv_pos and loc.root_lvl_name = bu.deflt_lvl_name) or' ||
                      CHR(10) ||
                      '        (bu.lvl_reltv_pos < 0 and loc.distance = -bu.lvl_reltv_pos and loc.lvl_name = bu.deflt_lvl_name)' ||
                      CHR(10) || '      )' || CHR(10) || '      union' ||
                      CHR(10) ||
                      '      -- relations coming from flagged accounts' ||
                      CHR(10) || '      select a2.acct_skid' || CHR(10) ||
                      '      from (' || CHR(10) || '        select distinct' ||
                      CHR(10) || '          parnt_acct_id' || CHR(10) ||
                      '        from OPT_ACCT_DIM' || CHR(10) ||
                      '        where gen_basln_flag = ''Y'') a1,' || CHR(10) ||
                      '        OPT_ACCT_DIM a2' || CHR(10) ||
                      '      where a1.parnt_acct_id = a2.parnt_acct_id' ||
                      CHR(10) || '      and ' ||
                      REPLACE(V_BUS_UNIT_FLTR, '[A].', 'a2.') || CHR(10) ||
                      '      union' || CHR(10) ||
                      '      -- root nodes - with no brothers' || CHR(10) ||
                      '      select acct_skid' || CHR(10) ||
                      '      from OPT_ACCT_DIM' || CHR(10) ||
                      '      where gen_basln_flag = ''Y''' || CHR(10) ||
                      '      and ' || REPLACE(V_BUS_UNIT_FLTR, '[A].', '') ||
                      CHR(10) || '    ) g, ACCT_HIER_SKID a' || CHR(10) ||
                      '    where a.acct_skid = g.acct_skid (+)' || CHR(10) ||
                      '  ),' || CHR(10) ||
                      '  -- for each node storing information about children with GEN_BASLN_FLAG marked' ||
                      CHR(10) || '  GEN_BASLN_CHILDREN as (' || CHR(10) ||
                      '    select root_acct_skid as acct_skid, max(case when root_acct_skid = acct_skid then ''N'' else gen_basln_flag end) as gen_basln_flag from (' ||
                      CHR(10) ||
                      '      select acct_skid, connect_by_root acct_skid as root_acct_skid, gen_basln_flag' ||
                      CHR(10) || '      from OPT_ACCT' || CHR(10) ||
                      '      connect by prior acct_skid = parnt_acct_skid' ||
                      CHR(10) || '    )' || CHR(10) ||
                      '    group by root_acct_skid' || CHR(10) || '  ),' ||
                      CHR(10) ||
                      '  -- find all nodes without colision with children' ||
                      CHR(10) || '  ACCT_NO_COLLISION as (' || CHR(10) ||
                      '    select a.*' || CHR(10) ||
                      '    from OPT_ACCT a, GEN_BASLN_CHILDREN c' || CHR(10) ||
                      '    where a.acct_skid = c.acct_skid' || CHR(10) ||
                      '    and a.gen_basln_flag = ''Y''' || CHR(10) ||
                      '    and c.gen_basln_flag = ''N''' || CHR(10) || '  ),' ||
                      CHR(10) ||
                      '  -- for every node to be pulled down find the children that doesn''t have marked node above and below' ||
                      CHR(10) || '  ACCT_COLLISION_SOLVE as (' || CHR(10) ||
                      '    select nc.acct_skid, np.acct_skid_path' || CHR(10) ||
                      '    from' || CHR(10) || '      GEN_BASLN_CHILDREN nc,' ||
                      CHR(10) || '      (' || CHR(10) ||
                      '      -- find all nodes that have got colision with parent' ||
                      CHR(10) || '      select *' || CHR(10) || '      from' ||
                      CHR(10) || '        (' || CHR(10) || '        select' ||
                      CHR(10) ||
                      '          level as lvl, acct_skid, parnt_acct_skid,' ||
                      CHR(10) ||
                      '          connect_by_root(acct_skid) as root_acct_skid,' ||
                      CHR(10) ||
                      '          sys_connect_by_path(gen_basln_flag, ''|'') as gen_basln_path,' ||
                      CHR(10) ||
                      '          sys_connect_by_path(acct_skid, ''|'') as acct_skid_path' ||
                      CHR(10) || '        from OPT_ACCT' || CHR(10) ||
                      '        connect by prior acct_skid = parnt_acct_skid' ||
                      CHR(10) || '        )' || CHR(10) ||
                      '      where instr(substr(gen_basln_path, 3), ''Y'') = 0' ||
                      CHR(10) || '      ) np, (' || CHR(10) ||
                      '      -- find all nodes that have got colision with children' ||
                      CHR(10) || '      select a.*' || CHR(10) ||
                      '      from OPT_ACCT a, GEN_BASLN_CHILDREN c' || CHR(10) ||
                      '      where a.acct_skid = c.acct_skid' || CHR(10) ||
                      '      and a.gen_basln_flag = ''Y''' || CHR(10) ||
                      '      and c.gen_basln_flag = ''Y''' || CHR(10) ||
                      '      ) a' || CHR(10) ||
                      '    where a.acct_skid = np.root_acct_skid' || CHR(10) ||
                      '    and np.acct_skid = nc.acct_skid' || CHR(10) ||
                      '    and nc.gen_basln_flag = ''N''' || CHR(10) || '  ),' ||
                      CHR(10) ||
                      '  -- eliminate accounts with other candidates above' ||
                      CHR(10) || '  ACCT_COLLISION_FINAL as (' || CHR(10) ||
                      '    select f1.*' || CHR(10) ||
                      '    from ACCT_COLLISION_SOLVE f1' || CHR(10) ||
                      '    where f1.acct_skid is not null' || CHR(10) ||
                      '    and f1.acct_skid not in (' || CHR(10) ||
                      '      select f3.acct_skid' || CHR(10) ||
                      '      from ACCT_COLLISION_SOLVE f2, ACCT_COLLISION_SOLVE f3' ||
                      CHR(10) || '      where f2.acct_skid is not null' ||
                      CHR(10) ||
                      '      and f3.acct_skid_path like f2.acct_skid_path || ''%''' ||
                      CHR(10) || '      and f3.acct_skid <> f2.acct_skid' ||
                      CHR(10) || '    )' || CHR(10) || '  )'; */

        -- prepare temporary table create statement for c_acct_detect_temp(final table - partitionned)
        -- added by myra on 2009-02-02 for R8 f008
        -- Modified by David on 2009-03-02 for R8 f008

    /*  DROP_TABLE(C_ACCT_DETECT_TEMP);
        V_SQL := 'CREATE TABLE ' || C_ACCT_DETECT_TEMP || CHR(10) ||
                 C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ' PARALLEL ' ||
                 V_P_DOP || CHR(10) || 'as  with ' || V_AGG1_SQL || CHR(10) ||
                 '       (' || CHR(10) ||
                 '         -- final selected accounts with their children on Ship-To level' ||
                 CHR(10) || '         select' || CHR(10) ||
                 '           ah.acct_skid, ah.root_acct_skid as parnt_acct_skid' ||
                 CHR(10) || '         from' || CHR(10) || '         (' ||
                 CHR(10) ||
                 '           select acct_skid from ACCT_COLLISION_FINAL union all' ||
                 CHR(10) ||
                 '           select acct_skid from ACCT_NO_COLLISION' ||
                 CHR(10) || '         ) a, ACCT_HIER ah' || CHR(10) ||
                 '        where ah.lvl_name = ''Ship-to location''' || CHR(10) ||
                 '        and ah.root_acct_skid = a.acct_skid' || CHR(10) ||
                 '       )  ';

        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Table created successfuly.');
            -- statistics
            PRO_PUT_LINE('Starting gathering statistics process for ' ||
                         C_ACCT_DETECT_TEMP || ' table (estimate percent = ' ||
                         C_STATS_TEMP_EST_PERCENT || ')...');
            DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                                 'CURRENT_SCHEMA'),
                                          TABNAME => C_ACCT_DETECT_TEMP,
                                          ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                          METHOD_OPT => 'FOR COLUMNS SIZE 1',
                                          CASCADE => FALSE);
            PRO_PUT_LINE('Statistics gathered.');

        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table ' ||
                             C_ACCT_DETECT_TEMP || ' failed with error - ' ||
                             SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table ' ||
                                         C_ACCT_DETECT_TEMP ||
                                         ' failed with error - ' || SQLERRM);
        END;
        */

        --Added by Mervyn for performance issue on 2010-02-04
        --The table Aggregates weekly to Monthly, and joins Account with shipment
        -- Commented by David for B016 in R10, move it to SHIP_AGG_TO_WK procedure
        /*TRUNCATE_TABLE(C_SHPMT_ACCT_MTH_BFCT);
        V_SQL := 'INSERT \*+ APPEND PARALLEL(' || V_P_DOP || ') *\ INTO ' || C_SHPMT_ACCT_MTH_BFCT || CHR(10) ||
                 '(SELECT C.MTH_SKID,                                               '||CHR(10)||
        '       S.BUS_UNIT_SKID,                                                    '||CHR(10)||
        '       S.ACCT_SKID,                                                   '||CHR(10)||
        '       S.PROD_SKID,                                                       '||CHR(10)||
        '       S.BRAND_SKID,                                                        '||CHR(10)||
        '       S.GTIN_STTUS_CODE,                                                       '||CHR(10)||
        '       SUM(S.VOL_SU_AMT * C.DAY_CNT / 7) AS VOL_SU_AMT                     '||CHR(10)||
        '  FROM (SELECT ACCT_HIER.PARNT_ACCT_SKID AS ACCT_SKID,                     '||CHR(10)||
        '               FCT.BUS_UNIT_SKID,                                          '||CHR(10)||
        '               FCT.PROD_SKID,                                              '||CHR(10)||
        '               FCT.BRAND_SKID,                                             '||CHR(10)||
        '               FCT.GTIN_STTUS_CODE,                                        '||CHR(10)||
        '               FCT.WK_SKID,                                                '||CHR(10)||
        '               FCT.VOL_SU_AMT                                              '||CHR(10)||
        '          FROM OPT_SHPMT_WK_FCT_TMP1 FCT, OPT_ACCT_SHIPT_ASSOC_SDIM ACCT_HIER     '||CHR(10)||
        '         WHERE FCT.ACCT_SKID = ACCT_HIER.ACCT_SKID) S,                     '||CHR(10)||
        '       OPT_CAL_MASTR_WK_DIM C                                              '||CHR(10)||
        ' WHERE S.WK_SKID = C.WK_SKID                                               '||CHR(10)||
        ' GROUP BY C.MTH_SKID,                                                      '||CHR(10)||
        '       S.BUS_UNIT_SKID,                                                    '||CHR(10)||
        '       S.ACCT_SKID,                                                        '||CHR(10)||
        '       S.PROD_SKID,                                                        '||CHR(10)||
        '       S.BRAND_SKID,                                                       '||CHR(10)||
        '       S.GTIN_STTUS_CODE)                                                  ' ;

        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Table is inserted successfuly.');
            -- statistics
            PRO_PUT_LINE('Starting gathering statistics process for ' ||
                         C_SHPMT_ACCT_MTH_BFCT || ' table (estimate percent = ' ||
                         C_STATS_TEMP_EST_PERCENT || ')...');
            DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                                 'CURRENT_SCHEMA'),
                                          TABNAME => C_SHPMT_ACCT_MTH_BFCT,
                                          ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                          METHOD_OPT => 'FOR COLUMNS SIZE 1',
                                          CASCADE => FALSE);
            PRO_PUT_LINE('Statistics gathered.');

        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Insert temporary table ' ||
                             C_SHPMT_ACCT_MTH_BFCT || ' failed with error - ' ||
                             SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Insert temporary table ' ||
                                         C_SHPMT_ACCT_MTH_BFCT ||
                                         ' failed with error - ' || SQLERRM);
        END;*/

        --Added by David for performance issue on 2009-03-02
        --This is for gtin account location
        DROP_TABLE(C_PROD_HIST_TEMP);
        V_PROD_HIST_SQL := 'Create table ' || C_PROD_HIST_TEMP || CHR(10) ||
                           C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ' PARALLEL ' ||
                           V_P_DOP || CHR(10) ||
                           ' AS' || CHR(10) || '  (' ||
                           CHR(10) || '   -- denormalized prod history' ||
                           CHR(10) || '   SELECT PROD_SKID AS CHILD_PROD_SKID,' ||
                           CHR(10) ||
                           '           CONNECT_BY_ROOT PROD_SKID AS PARNT_PROD_SKID,' ||
                           CHR(10) ||
                           '           CONNECT_BY_ROOT PROD_ACTIV_IND AS PARNT_ACTIV_IND,' ||
                           CHR(10) ||
                           '           CONNECT_BY_ROOT STTUS_CODE AS PARNT_GTIN_STTUS_CODE' ||
                           CHR(10) || '   FROM (SELECT P1.PROD_SKID,' ||
                           CHR(10) ||
                           '                 P2.PROD_SKID AS PARNT_PROD_SKID,' ||
                           CHR(10) || '                 DECODE(P1.STTUS_CODE,' ||
                           CHR(10) || '                        ''Inactive'',' ||
                           CHR(10) || '                        ''N'',' ||
                           CHR(10) || '                        ''Historical'',' ||
                           CHR(10) || '                        ''N'',' ||
                           CHR(10) ||
                           '                        P1.PROD_ACTIV_IND) AS PROD_ACTIV_IND,' ||
                           CHR(10) || '                  P1.STTUS_CODE ' ||
                           CHR(10) || '          FROM OPT_PROD_DIM P1,' ||
                           CHR(10) || '               OPT_PROD_DIM P2' ||
                           CHR(10) ||
                           '          WHERE P1.BUS_UNIT_SKID = P2.BUS_UNIT_SKID(+)' ||
                           CHR(10) || '          AND P1.GTIN = P2.PRED_GTIN(+)' ||
                           CHR(10) ||
                           '          AND P2.PRED_GTIN(+) IS NOT NULL' ||
                           CHR(10) || '    and ' ||
                           REPLACE(V_BUS_UNIT_FLTR, '[A]', 'P1') || ')' ||
                           CHR(10) ||
                           '   CONNECT BY NOCYCLE PRIOR PROD_SKID = PARNT_PROD_SKID)';

        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_PROD_HIST_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_PROD_HIST_SQL;
            PRO_PUT_LINE('Table created successfuly.');
            -- statistics
            PRO_PUT_LINE('Starting gathering statistics process for ' ||
                         C_PROD_HIST_TEMP || ' table (estimate percent = ' ||
                         C_STATS_TEMP_EST_PERCENT || ')...');
            DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                                 'CURRENT_SCHEMA'),
                                          TABNAME => C_PROD_HIST_TEMP,
                                          ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                          METHOD_OPT => 'FOR COLUMNS SIZE 1',
                                          CASCADE => FALSE);
            PRO_PUT_LINE('Statistics gathered.');

        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table ' ||
                             C_PROD_HIST_TEMP || ' failed with error - ' ||
                             SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table ' ||
                                         C_PROD_HIST_TEMP ||
                                         ' failed with error - ' || SQLERRM);
        END;
        --Added by David for performance issue on 2009-03-02
        --This is for filter shipment data PAST_MTH_CNT months ago
        TRUNCATE_TABLE(C_SHPMT_PAST_MTHS_TEMP);
        V_SHPMT_PAST_MTHS_SQL := 'INSERT /*+ APPEND PARALLEL(' || V_P_DOP || ') */  INTO ' ||
                                 C_SHPMT_PAST_MTHS_TEMP||'(MTH_SKID,PROD_SKID,BRAND_SKID,BUS_UNIT_SKID,GTIN_STTUS_CODE,ACCT_SKID,VOL_SU_AMT,PAST_MTH_CNT,FIRST_WK_SKID) '|| CHR(10) ||
                                -- C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ' PARALLEL ' ||
                                -- V_P_DOP || CHR(10) ||
                                -- ' AS' || CHR(10) ||
                                 '  (SELECT F.MTH_SKID,' || CHR(10) ||
                                 '          F.PROD_SKID,' || CHR(10) ||
                                 '          F.BRAND_SKID,' || CHR(10) ||
                                 '          F.BUS_UNIT_SKID,' || CHR(10) ||
                                 '          F.GTIN_STTUS_CODE,' || CHR(10) ||
                                 '          F.ACCT_SKID,' || CHR(10) ||
                                 '          F.VOL_SU_AMT,' || CHR(10) ||
                                 '          NVL(G.PAST_MTHS_CNT, 24) AS PAST_MTHS_CNT,' ||
                                 CHR(10) ||
                                 '          C.WK_SKID AS FIRST_WK_SKID' ||
                                 CHR(10) || '   FROM OPT_SHPMT_ACCT_MTH_TSFCT F,' ||
                                 CHR(10) || '        CAL_MASTR_DIM C,' ||
                                 CHR(10) ||
                                 '        (SELECT /*+ NO_MERGE ORDERED USE_HASH(bu cat) */' ||
                                 CHR(10) || '          CAT.PROD_SKID,' ||
                                 CHR(10) ||
                                 '          NVL(CAT.PAST_MTHS_CNT, BU.PAST_MTHS_CNT) AS PAST_MTHS_CNT' ||
                                 CHR(10) ||
                                 '         FROM -- business unit max promotion length defaults' ||
                                 CHR(10) ||
                                 '               (SELECT BU.BUS_UNIT_SKID,' ||
                                 CHR(10) ||
                                 '                       NVL(D.PAST_MTHS_CNT, G.PAST_MTHS_CNT) AS PAST_MTHS_CNT' ||
                                 CHR(10) ||
                                 '                FROM OPT_BUS_UNIT_DIM BU,' ||
                                 CHR(10) ||
                                 '                     OPT_BUS_UNIT_PLC G,' ||
                                 CHR(10) ||
                                 '                     OPT_BUS_UNIT_PLC D' ||
                                 CHR(10) ||
                                 '                WHERE G.BUS_UNIT_SKID = 0' ||
                                 CHR(10) ||
                                 '                AND D.DEFLT_LVL_NAME IS NOT NULL' ||
                                 CHR(10) ||
                                 '                AND D.BUS_UNIT_SKID(+) <> 0' ||
                                 CHR(10) ||
                                 '                AND BU.BUS_UNIT_SKID = D.BUS_UNIT_SKID(+)) BU,' ||
                                 CHR(10) ||
                                 '              (SELECT /*+ NO_MERGE */' ||
                                 CHR(10) || '                H.PROD_SKID,' ||
                                 CHR(10) || '                H.BUS_UNIT_SKID,' ||
                                 CHR(10) ||
                                 '                MAX(CAT.PAST_MTHS_CNT) KEEP(DENSE_RANK FIRST ORDER BY H.FY_DATE_SKID DESC) AS PAST_MTHS_CNT' ||
                                 CHR(10) ||
                                 '               FROM OPT_PROD_HIER_FDIM H,' ||
                                 CHR(10) ||
                                 '                    OPT_BASLN_CATEG_PARM_PLC CAT' ||
                                 CHR(10) || '               WHERE ' ||
                                 REPLACE(V_BUS_UNIT_FLTR, '[A]', 'H') ||
                                 CHR(10) ||
                                 '               AND H.PROD_LVL_DESC = ''GTIN'' ' ||
                                 CHR(10) ||
                                 '               AND H.PROD_4_BUS_UNIT_SKID = CAT.BUS_UNIT_SKID(+)' ||
                                 CHR(10) ||
                                 '               AND H.PROD_4_NAME = CAT.PROD_NAME(+)' ||
                                 CHR(10) ||
                                 '               GROUP BY H.PROD_SKID,' ||
                                 CHR(10) ||
                                 '                        H.BUS_UNIT_SKID) CAT' ||
                                 CHR(10) ||
                                 '         WHERE CAT.BUS_UNIT_SKID = BU.BUS_UNIT_SKID) G' ||
                                 CHR(10) ||
                                 '   WHERE F.PROD_SKID = G.PROD_SKID(+)' ||
                                 CHR(10) || '   AND F.MTH_SKID >= C.MTH_SKID' ||
                                 CHR(10) || '   AND C.DAY_DATE =' || CHR(10) ||
                                 '         ADD_MONTHS(TRUNC(SYSDATE + 6, ''MM''), - (NVL(G.PAST_MTHS_CNT, 24) + 1))' ||
                                 CHR(10) ||
                                 '   AND F.GTIN_STTUS_CODE <> ''Remnant'') ';

        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SHPMT_PAST_MTHS_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SHPMT_PAST_MTHS_SQL;
            PRO_PUT_LINE('Table created successfuly.');
            -- statistics
            PRO_PUT_LINE('Starting gathering statistics process for ' ||
                         C_SHPMT_PAST_MTHS_TEMP ||
                         ' table (estimate percent = ' ||
                         C_STATS_TEMP_EST_PERCENT || ')...');
            DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                                 'CURRENT_SCHEMA'),
                                          TABNAME => C_SHPMT_PAST_MTHS_TEMP,
                                          ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                          METHOD_OPT => 'FOR COLUMNS SIZE 1',
                                          CASCADE => FALSE);
            PRO_PUT_LINE('Statistics gathered.');

        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table ' ||
                             C_SHPMT_PAST_MTHS_TEMP || ' failed with error - ' ||
                             SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table ' ||
                                         C_SHPMT_PAST_MTHS_TEMP ||
                                         ' failed with error - ' || SQLERRM);
        END;

        --Added by David for performance issue on 2009-03-02
        --This is for aggregating the su after GTIN history maintain
        DROP_TABLE(C_GTIN_ACCT_AGG_TEMP);
        V_GTIN_ACCT_AGG_SQL := 'Create table ' || C_GTIN_ACCT_AGG_TEMP || CHR(10) ||
                               C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ' PARALLEL ' ||
                               V_P_DOP || CHR(10) ||
                               ' AS' || CHR(10) ||
                               '    -- shipments with GTIN history maintanance' ||CHR(10) ||
                               '     (SELECT /*+ NO_MERGE ORDERED USE_HASH(p s) */ ' || CHR(10) ||
                               '       P.PARNT_PROD_SKID AS PROD_SKID, ' ||CHR(10) ||
                               '       S.BRAND_SKID AS BRAND_SKID, ' || CHR(10) ||
                               '       P.PARNT_GTIN_STTUS_CODE AS GTIN_STTUS_CODE, ' || CHR(10) ||
                               '       S.ACCT_SKID, ' || CHR(10) ||
                               '       S.MTH_SKID, ' || CHR(10) ||
                               '       SUM(CASE ' || CHR(10) ||
                               '              WHEN P.PARNT_ACTIV_IND = ''Y'' THEN ' ||CHR(10) ||
                               '                   S.VOL_SU_AMT ' || CHR(10) ||
                               '              ELSE ' || CHR(10) ||
                               '                   0 ' || CHR(10) ||
                               '          END) AS VOL_SU_AMT, ' || CHR(10) ||
                               '          S.PAST_MTH_CNT ' || CHR(10) ||
                               '     FROM
                               ' || C_PROD_HIST_TEMP || ' P, ' || CHR(10) || C_SHPMT_PAST_MTHS_TEMP || ' S ' ||CHR(10) ||
                               '     WHERE P.CHILD_PROD_SKID = S.PROD_SKID ' ||CHR(10) ||  CHR(10) ||
                               '            AND S.MTH_SKID < ' || CHR(10) ||
                               '            (SELECT MIN(MTH_SKID) AS FIRST_MTH_SKID ' ||CHR(10) ||
                               '                 FROM  ' ||   C_SHPMT_PAST_MTHS_TEMP || ' S1 ' || CHR(10) ||
                               '                 WHERE S1.PROD_SKID=P.PARNT_PROD_SKID ' ||CHR(10) ||
                               '                 AND S.ACCT_SKID=S1.ACCT_SKID ' || CHR(10) ||
                               '                 AND S.BRAND_SKID=S1.BRAND_SKID)  ' || CHR(10) ||
                               '            GROUP BY P.PARNT_PROD_SKID, ' || CHR(10) ||
                               '                          S.BRAND_SKID, ' || CHR(10) ||
                               '                          P.PARNT_GTIN_STTUS_CODE, ' ||CHR(10) ||
                               '                          S.ACCT_SKID, ' ||CHR(10) ||
                               '                          S.MTH_SKID, ' || CHR(10) ||
                               '                          S.PAST_MTH_CNT ' ||CHR(10) ||
                               '     UNION ALL ' || CHR(10) ||
                               '     SELECT /*+ NO_MERGE ORDERED USE_HASH(p s) */ ' ||
                               CHR(10) || '      S.PROD_SKID AS PROD_SKID, ' ||
                               CHR(10) || '      S.BRAND_SKID AS BRAND_SKID, ' ||
                               CHR(10) || '      S.GTIN_STTUS_CODE, ' ||
                               CHR(10) ||'       S.ACCT_SKID, ' ||
                               CHR(10) || '      S.MTH_SKID, ' ||
                               CHR(10) || '      S.VOL_SU_AMT AS VOL_SU_AMT, ' ||
                               CHR(10) || '      S.PAST_MTH_CNT    ' ||
                               CHR(10) || '     FROM ' ||
                               C_SHPMT_PAST_MTHS_TEMP || ' S ' || CHR(10) ||
                               '     WHERE S.GTIN_STTUS_CODE NOT IN (''Inactive'',''Historical'') ' ||CHR(10) ||
                               '   AND S.MTH_SKID BETWEEN [MIN_MTH_SKID] AND [MAX_MTH_SKID])' ;

        V_GTIN_ACCT_AGG_SQL := REPLACE(REPLACE(V_GTIN_ACCT_AGG_SQL,
                                               '[MIN_MTH_SKID]',
                                               V_CYTDYA_MIN_MTH_SKID),
                                       '[MAX_MTH_SKID]', V_CYTD_MAX_MTH_SKID);

        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_GTIN_ACCT_AGG_SQL, CHR(10), CHR(10) || '> '));

        BEGIN
            EXECUTE IMMEDIATE V_GTIN_ACCT_AGG_SQL;
            PRO_PUT_LINE('Table created successfuly.');
            -- statistics
            PRO_PUT_LINE('Starting gathering statistics process for ' ||
                         C_GTIN_ACCT_AGG_TEMP || ' table (estimate percent = ' ||
                         C_STATS_TEMP_EST_PERCENT || ')...');
            DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                                 'CURRENT_SCHEMA'),
                                          TABNAME => C_GTIN_ACCT_AGG_TEMP,
                                          ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                          METHOD_OPT => 'FOR COLUMNS SIZE 1',
                                          CASCADE => FALSE);
            PRO_PUT_LINE('Statistics gathered.');

        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table ' ||
                             C_GTIN_ACCT_AGG_TEMP || ' failed with error - ' ||
                             SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table ' ||
                                         C_GTIN_ACCT_AGG_TEMP ||
                                         ' failed with error - ' || SQLERRM);
        END;

        -- part 3: gtin account location and time aggregation
        -- added by myra for F008 in R8 on 2009-02-02

        /********************************************************************************************/
        /* templates for the final query - planning baseline calculation with GR, noGR and averages */
        /********************************************************************************************/
        -- Updated by David for B052 in R9 on 2009-09-02
		-- Defect 2850, OPTIMA11, 2011-02-16, Martin, Start: Modify the process on getting the plan_basln_factr
        V_GROWTH_SQL_TMPLT := '-- final baseline: growth rate factor used' ||
                              CHR(10) || 'with' || CHR(10) ||
                              '  POST_BASLN12 as (' || CHR(10) ||
                              '    [AGG_VIEW12]' || CHR(10) || '  ),' ||
                              CHR(10) || '  POST_BASLN24 as (' || CHR(10) ||
                              '    [AGG_VIEW24]' || CHR(10) || '  ),' ||
                              CHR(10) || '  POST_BASLN12_STAT as (' || CHR(10) ||
                              '    select' || CHR(10) ||
                              '      prod_skid, acct_skid,' || CHR(10) ||
                              '      sum(basln_su_amt) / (round((' ||
                              TO_CHAR(V_CYTD_MAX_MTH_SKID) ||
                              ' - min(mth_skid)) / 30)) as avg_basln_su_amt,' ||
                              CHR(10) || '      min(mth_skid) as min_mth_skid,' ||
                              CHR(10) || '      max(mth_skid) as max_mth_skid' ||
                              CHR(10) || '    from POST_BASLN12' || CHR(10) ||
                              '    group by prod_skid, acct_skid),' || CHR(10) ||
                              '  MTHS_CAL as (' || CHR(10) ||
                              '    select distinct' || CHR(10) ||
                              '      mth_skid,' || CHR(10) ||
                              '      mth_skid + (add_months(mth_start_date, ' ||
                              P_GEN_MTH || ') - mth_start_date) as ny_mth_skid' ||
                              CHR(10) || '    from CAL_MASTR_DIM' || CHR(10) ||
                              '    where mth_start_date between add_months(trunc(sysdate+6, ''mm''), -' ||
                              P_GEN_MTH ||
                              ') and add_months(trunc(sysdate+6, ''mm''), -1))' ||CHR(10) ||
                              'select p.prod_skid,p.acct_skid,p.mth_skid,p.basln_su_amt*cat.plan_basln_factr'|| CHR(10) ||
                              'from' || CHR(10) ||
                              ' (SELECT PROD_SKID, PLAN_BASLN_FACTR FROM' || CHR(10)||
                              '  (SELECT /*+ NO_MERGE */' || CHR(10) ||
                              '     distinct h.prod_skid, nvl(cat.plan_basln_factr,1) as plan_basln_factr,' ||CHR(10) ||
                              '              rank() over(partition by h.prod_skid order by cat.plan_basln_factr asc) row_cnt' ||CHR(10) ||
                              '   FROM OPT_PROD_HIER_FDIM h, OPT_BASLN_CATEG_PARM_PLC cat' ||CHR(10) ||
                              '   WHERE ' || REPLACE(V_BUS_UNIT_FLTR, '[A]', 'h') || CHR(10) ||
                              '       AND h.prod_lvl_desc = ''Brand''' || CHR(10) ||
                              '       AND h.bus_unit_skid = cat.bus_unit_skid (+)' || CHR(10) ||
                              '       AND h.prod_4_name = cat.prod_name (+)' ||CHR(10) ||
                              '  ) where row_cnt=1) cat,' || CHR(10) ||
                              '(select' || CHR(10) ||
                              '  prod_skid, acct_skid, mth_skid,' || CHR(10) ||
                              '  last_value(basln_su_amt ignore nulls) over (' || CHR(10) ||
                              '    partition by prod_skid, acct_skid' || CHR(10) ||
                              '    order by mth_skid desc' || CHR(10) ||
                              '  ) as basln_su_amt' || CHR(10) ||
                              'from' || CHR(10) || '  (' || CHR(10) ||
                              '  -- missing months (needed for copy-backward)' ||
                              CHR(10) || '  select /*+ USE_MERGE(a c) */' ||
                              CHR(10) ||
                              '    a.prod_skid, a.acct_skid, c.ny_mth_skid as mth_skid, null as basln_su_amt' ||
                              CHR(10) || '  from' || CHR(10) ||
                              '    MTHS_CAL c,' || CHR(10) ||
                              '    POST_BASLN12_STAT a' || CHR(10) ||
                              '  where c.mth_skid < a.min_mth_skid' || CHR(10) ||
                              '  union all' || CHR(10) ||
                              '  -- calculated baseline' || CHR(10) ||
                              '  select /*+ ORDERED USE_HASH(g b c) */' ||
                              CHR(10) ||
                              '    b.prod_skid, b.acct_skid, c.ny_mth_skid as mth_skid, b.basln_su_amt * gr_factr as basln_su_amt' ||
                              CHR(10) || '  from' || CHR(10) ||
                              '    MTHS_CAL c,' || CHR(10) || '    (select' ||
                              CHR(10) ||
                              '       abl12.prod_skid, abl12.acct_skid,' ||
                              CHR(10) ||
                              '       decode(nvl(abl24.avg_basln_su_amt, 0), 0, 1, abl12.avg_basln_su_amt / abl24.avg_basln_su_amt) as gr_factr' ||
                              CHR(10) || '     from POST_BASLN12_STAT abl12,' ||
                              CHR(10) ||
                              '       (select prod_skid, acct_skid, avg(basln_su_amt) as avg_basln_su_amt' ||
                              CHR(10) || '       from POST_BASLN24' || CHR(10) ||
                              '       group by prod_skid, acct_skid) abl24' ||
                              CHR(10) ||
                              '     where abl12.prod_skid = abl24.prod_skid (+)' ||
                              CHR(10) ||
                              '     and abl12.acct_skid = abl24.acct_skid (+)) g,' ||
                              CHR(10) || '     POST_BASLN12 b' || CHR(10) ||
                              '  where g.prod_skid = b.prod_skid' || CHR(10) ||
                              '  and g.acct_skid = b.acct_skid' || CHR(10) ||
                              '  and b.mth_skid = c.mth_skid)' || CHR(10) ||
                              'union all' || CHR(10) ||
                              '-- zero baseline data for missing up-to-date records' ||
                              CHR(10) ||
                              'select /*+ NO_MERGE USE_MERGE(s c) */' ||
                              CHR(10) || '  s.prod_skid,' || CHR(10) ||
                              '  s.acct_skid,' || CHR(10) ||
                              '  c.ny_mth_skid as mth_skid,' || CHR(10) ||
                              '  0 as basln_su_amt' || CHR(10) ||
                              'from POST_BASLN12_STAT s, MTHS_CAL c' || CHR(10) ||
                              'where c.mth_skid > s.max_mth_skid) p' || CHR(10) ||
                              'where cat.prod_skid = p.prod_skid';
		-- Defect 2850, OPTIMA11, 2011-02-16, Martin, end;

        -- Updated by David for B052 in R9 on 2009-09-02
		-- Defect 2850, OPTIMA11, 2011-02-16, Martin, Start: Modify the process on getting the plan_basln_factr
        V_NOGROWTH_SQL_TMPLT := '-- final baseline: nogrowth rate (gr=1)' ||
                                CHR(10) || 'with' || CHR(10) ||
                                '  POST_BASLN12 as (' || CHR(10) ||
                                '    [AGG_VIEW12]' || CHR(10) || '  ),' ||
                                CHR(10) || '  POST_BASLN12_STAT as (' ||
                                CHR(10) || '    select' || CHR(10) ||
                                '      prod_skid, acct_skid,' || CHR(10) ||
                                '      min(mth_skid) as min_mth_skid,' ||
                                CHR(10) ||
                                '      max(mth_skid) as max_mth_skid' ||
                                CHR(10) || '    from POST_BASLN12' || CHR(10) ||
                                '    group by prod_skid, acct_skid),' ||
                                CHR(10) || '  MTHS_CAL as (' || CHR(10) ||
                                '    select distinct' || CHR(10) ||
                                '      mth_skid,' || CHR(10) ||
                                '      mth_skid + (add_months(mth_start_date, ' ||
                                P_GEN_MTH ||
                                ') - mth_start_date) as ny_mth_skid' || CHR(10) ||
                                '    from CAL_MASTR_DIM' || CHR(10) ||
                                '    where mth_start_date between add_months(trunc(sysdate+6, ''mm''), -' ||
                                P_GEN_MTH ||
                                ') and add_months(trunc(sysdate+6, ''mm''), -1))' ||CHR(10) ||
                                'select p.prod_skid,p.acct_skid,p.mth_skid,p.basln_su_amt*cat.plan_basln_factr'|| CHR(10) ||
                                'from' || CHR(10) ||
                                ' (SELECT PROD_SKID, PLAN_BASLN_FACTR FROM' || CHR(10)||
                                '  (SELECT /*+ NO_MERGE */' || CHR(10) ||
                                '     distinct h.prod_skid, nvl(cat.plan_basln_factr,1) as plan_basln_factr,' ||CHR(10) ||
                                '              rank() over(partition by h.prod_skid order by cat.plan_basln_factr asc) row_cnt' ||CHR(10) ||
                                '   FROM OPT_PROD_HIER_FDIM h, OPT_BASLN_CATEG_PARM_PLC cat' ||CHR(10) ||
                                '   WHERE ' || REPLACE(V_BUS_UNIT_FLTR, '[A]', 'h') || CHR(10) ||
                                '       AND h.prod_lvl_desc = ''Brand''' || CHR(10) ||
                                '       AND h.bus_unit_skid = cat.bus_unit_skid (+)' || CHR(10) ||
                                '       AND h.prod_4_name = cat.prod_name (+)' ||CHR(10) ||
                                '  ) where row_cnt=1) cat,' || CHR(10) ||
                                '(select' || CHR(10) ||
                                '  prod_skid, acct_skid, mth_skid,' || CHR(10) ||
                                '  last_value(basln_su_amt ignore nulls) over (' ||CHR(10) ||
                                '    partition by prod_skid, acct_skid' ||CHR(10) ||
                                '    order by mth_skid desc' ||CHR(10) ||
                                '  ) as basln_su_amt' || CHR(10) ||
                                'from' || CHR(10) || '  (' || CHR(10) ||
                                '  -- missing months (needed for copy-backward)' ||
                                CHR(10) || '  select /*+ USE_MERGE(a c) */' ||
                                CHR(10) ||
                                '    a.prod_skid, a.acct_skid, c.ny_mth_skid as mth_skid, null as basln_su_amt' ||
                                CHR(10) || '  from' || CHR(10) ||
                                '    MTHS_CAL c,' || CHR(10) ||
                                '    POST_BASLN12_STAT a' || CHR(10) ||
                                '  where c.mth_skid < a.min_mth_skid' ||
                                CHR(10) || '  union all' || CHR(10) ||
                                '  -- calculated baseline' || CHR(10) ||
                                '  select /*+ ORDERED USE_HASH(g b c) */' ||
                                CHR(10) ||
                                '    b.prod_skid, b.acct_skid, c.ny_mth_skid as mth_skid, b.basln_su_amt as basln_su_amt' ||
                                CHR(10) || '  from' || CHR(10) ||
                                '    MTHS_CAL c,' || CHR(10) ||
                                '    POST_BASLN12 b' || CHR(10) ||
                                '  where b.mth_skid = c.mth_skid)' || CHR(10) ||
                                'union all' || CHR(10) ||
                                '-- zero baseline data for missing up-to-date records' ||
                                CHR(10) ||
                                'select /*+ NO_MERGE USE_MERGE(s c) */' ||
                                CHR(10) || '  s.prod_skid,' || CHR(10) ||
                                '  s.acct_skid,' || CHR(10) ||
                                '  c.ny_mth_skid as mth_skid,' || CHR(10) ||
                                '  0 as basln_su_amt' || CHR(10) ||
                                'from POST_BASLN12_STAT s, MTHS_CAL c' ||
                                CHR(10) ||
                                'where c.mth_skid > s.max_mth_skid) p'|| CHR(10) ||
                                'where cat.prod_skid = p.prod_skid';
        -- Defect 2850, OPTIMA11, 2011-02-16, Martin, end;
		
        -- Updated by david for B052 in R9 on 2009-09-02
        -- Updated by David for B016 in R10 on 2010-3-16
        -- Updated by David for issue fixing in R10 on 2010-04-18
		-- Defect 2850, OPTIMA11, 2011-02-16, Martin, Start: Modify the process on getting the plan_basln_factr
        V_AVG_SQL_TMPLT := 'with' || CHR(10) || '  SETTING as (' || CHR(10) ||
                           '    SELECT /*+ NO_MERGE ORDERED USE_HASH(bu cat) */' || CHR(10) ||
                           '      cat.prod_skid,' || CHR(10) ||
                           '      nvl(cat.avg_mths_cnt, bu.avg_mths_cnt) as avg_mths_cnt' || CHR(10) ||
                           '    FROM' || CHR(10) ||
                           '      -- business unit average months count defaults' || CHR(10) ||
                           '      (SELECT' || CHR(10) ||
                           '         bu.bus_unit_skid,' || CHR(10) ||
                           '         nvl(d.avg_mths_cnt, g.avg_mths_cnt) as avg_mths_cnt,' || CHR(10) ||
                           '         nvl(d.strtgy_code, g.strtgy_code) as strtgy_code' || CHR(10) ||
                           '       FROM OPT_BUS_UNIT_DIM bu, OPT_BUS_UNIT_PLC g, OPT_BUS_UNIT_PLC d' || CHR(10) ||
                           '       WHERE g.bus_unit_skid = 0' || CHR(10) ||
                           '       AND d.deflt_lvl_name is not null' || CHR(10) ||
                           '       AND d.bus_unit_skid (+) <> 0' ||CHR(10) ||
                           '       AND bu.bus_unit_skid = d.bus_unit_skid (+)' || CHR(10) ||
                           '      ) bu,' || CHR(10) ||
                           '      (SELECT /*+ NO_MERGE */' || CHR(10) ||
                           '         h.prod_skid, h.bus_unit_skid, max(cat.avg_mths_cnt) keep (dense_rank first order by h.fy_date_skid desc) as avg_mths_cnt,' ||CHR(10) ||
                           '         max(cat.strtgy_code) keep (dense_rank first order by h.fy_date_skid desc) as strtgy_code' ||
                           CHR(10) ||
                           '       FROM OPT_PROD_HIER_FDIM h, OPT_BASLN_CATEG_PARM_PLC cat' ||
                           CHR(10) ||
                           '       WHERE ' ||
                           REPLACE(V_BUS_UNIT_FLTR, '[A]', 'h') || CHR(10) ||
                           '       AND h.prod_lvl_desc = ''Brand''' || CHR(10) ||
                           '       AND h.prod_4_bus_unit_skid = cat.bus_unit_skid (+)' ||
                           CHR(10) ||
                           '       AND h.prod_4_name = cat.prod_name (+)' ||
                           CHR(10) ||
                           '       group by h.prod_skid, h.bus_unit_skid' ||
                           CHR(10) ||
                           '      ) cat' || CHR(10) ||
                           '    WHERE cat.bus_unit_skid = bu.bus_unit_skid' ||
                           CHR(10) ||
                           '    and nvl(cat.strtgy_code, bu.strtgy_code) = ''' ||
                           C_STRTGY_CODE_AVG || '''' || CHR(10) ||
                           '  ),' || CHR(10) ||
                           '  MTHS_CAL as (' || CHR(10) ||
                           '    select distinct' || CHR(10) ||
                           '      mth_skid' || CHR(10) ||
                           '    from CAL_MASTR_DIM' || CHR(10) ||
                           '    where mth_start_date between add_months(trunc(sysdate+6, ''mm''), -18) and add_months(trunc(sysdate+6, ''mm''), -1)),' ||CHR(10) ||
                           '  POST_BASLN12 as (' || CHR(10) ||
                           '    [AGG_VIEW12]' || CHR(10) ||
                           '  ),' || CHR(10) ||
                           '  POST_BASLN12_STAT as (' || CHR(10) ||
                           '    select prod_skid, acct_skid, min(mth_skid) as min_mth_skid' ||
                           CHR(10) ||
                           '    from POST_BASLN12' || CHR(10) ||
                           '    group by prod_skid, acct_skid),' || CHR(10) ||
                           ' POST_BASLN_ZERO AS (  ' || CHR(10) ||
                           '    select /*+ NO_MERGE USE_MERGE(s c) */' || CHR(10) ||
                           '      s.prod_skid,' || CHR(10) ||
                           '      s.acct_skid,' || CHR(10) ||
                           '      c.mth_skid,'|| CHR(10) ||
                           '      0 AS basln_su_amt'|| CHR(10) ||
                           '    from POST_BASLN12_STAT s, MTHS_CAL c'|| CHR(10) ||
                           '    where c.mth_skid > s.min_mth_skid)' || CHR(10) ||
                           '-- running average calculation' || CHR(10) ||
                           'select' || CHR(10) ||
                           '  p.prod_skid,' || CHR(10) ||
                           '  p.acct_skid,' || CHR(10) ||
                           '  p.mth_skid,' || CHR(10) ||
                           '  p.basln_su_amt * cat.plan_basln_factr' || CHR(10) ||
                           'from' || CHR(10) ||
                           ' (SELECT PROD_SKID, PLAN_BASLN_FACTR FROM' || CHR(10)||
                           '  (SELECT /*+ NO_MERGE */' || CHR(10) ||
                           '     distinct h.prod_skid, nvl(cat.plan_basln_factr,1) as plan_basln_factr,' ||CHR(10) ||
                           '              rank() over(partition by h.prod_skid order by cat.plan_basln_factr asc) row_cnt' ||CHR(10) ||
                           '   FROM OPT_PROD_HIER_FDIM h, OPT_BASLN_CATEG_PARM_PLC cat' ||CHR(10) ||
                           '   WHERE ' || REPLACE(V_BUS_UNIT_FLTR, '[A]', 'h') || CHR(10) ||
                           '       AND h.prod_lvl_desc = ''Brand''' || CHR(10) ||
                           '       AND h.bus_unit_skid = cat.bus_unit_skid (+)' || CHR(10) ||
                           '       AND h.prod_4_name = cat.prod_name (+)' ||CHR(10) ||
                           '  ) where row_cnt=1) cat,' || CHR(10) ||
                           ' (' || CHR(10) ||
                           '  (select' ||  CHR(10) ||
                           '     prod_skid,' || CHR(10) ||
                           '     acct_skid,' || CHR(10) ||
                           '     mth_skid,' ||  CHR(10) ||
                           '     opt_running_avg(basln_su_amt) over (partition by prod_skid, acct_skid order by mth_skid asc rows between avg_mths_cnt preceding and current row) as basln_su_amt' || CHR(10) ||
                           '   from' || CHR(10) ||
                           '     -- post baseline data aggregated to months and proper accounts' || CHR(10) ||
                           '   ( SELECT * FROM '|| CHR(10) ||
                           '     (select /*+ ORDERED USE_HASH(a b) */' || CHR(10) ||
                           '        b.prod_skid, b.acct_skid, b.mth_skid, b.basln_su_amt, a.avg_mths_cnt - 1 as avg_mths_cnt' || CHR(10) ||
                           '      from' || CHR(10) ||
                           '        SETTING a,' || CHR(10) ||
                           '        (-- baseline from last 12 months' || CHR(10) ||
                           '         select prod_skid, acct_skid, mth_skid, basln_su_amt from POST_BASLN12 union all' || CHR(10) ||
                           '         -- zero baseline data for missing up-to-date records' || CHR(10) ||
                           '         (select prod_skid, acct_skid, mth_skid, basln_su_amt from POST_BASLN_ZERO' || CHR(10) ||
                           '           minus' || CHR(10) ||
                           '           select prod_skid, acct_skid, mth_skid, 0 as basln_su_amt from POST_BASLN12)' || CHR(10) ||
                           '        ) b' || CHR(10) ||
                           '      where b.prod_skid = a.prod_skid' || CHR(10) ||
                           '      union all' || CHR(10) ||
                           '      -- empty data for incoming year' || CHR(10) ||
                           '      select /*+ ORDERED USE_HASH(a b) */' || CHR(10) ||
                           '        b.prod_skid, b.acct_skid, c.mth_skid, -1 as basln_su_amt, a.avg_mths_cnt - 1 as avg_mths_cnt' || CHR(10) ||
                           '      from' || CHR(10) ||
                           '        SETTING a,' || CHR(10) ||
                           '        -- all gtin/ship-to pairs' || CHR(10) ||
                           '        POST_BASLN12_STAT b,' || CHR(10) ||
                           '        -- 12 future months' || CHR(10) ||
                           '        (select distinct' || CHR(10) ||
                           '           mth_skid' || CHR(10) ||
                           '         from CAL_MASTR_DIM' || CHR(10) ||
                           '         where mth_start_date between trunc(sysdate+6, ''mm'') and add_months(trunc(sysdate+6, ''mm''), 11)' ||CHR(10) ||
                           '        ) c' || CHR(10) ||
                           '      where b.prod_skid = a.prod_skid' || CHR(10) ||
                           '     )' || CHR(10) ||
                           '     )' || CHR(10) ||
                           '     WHERE avg_mths_cnt is not null' || CHR(10) ||                           
                           '   ) '|| CHR(10) ||
                           '   UNION ALL '|| CHR(10) ||           
                           '  (select' ||  CHR(10) ||
                           '     prod_skid,' || CHR(10) ||
                           '     acct_skid,' || CHR(10) ||
                           '     mth_skid,' ||  CHR(10) ||
                           '     NULL as basln_su_amt' || CHR(10) ||
                           '   from' || CHR(10) ||
                           '     -- post baseline data aggregated to months and proper accounts' || CHR(10) ||
                           '   ( SELECT * FROM '|| CHR(10) ||
                           '     (select /*+ ORDERED USE_HASH(a b) */' || CHR(10) ||
                           '        b.prod_skid, b.acct_skid, b.mth_skid, b.basln_su_amt, a.avg_mths_cnt - 1 as avg_mths_cnt' || CHR(10) ||
                           '      from' || CHR(10) ||
                           '        SETTING a,' || CHR(10) ||
                           '        (-- baseline from last 12 months' || CHR(10) ||
                           '         select prod_skid, acct_skid, mth_skid, basln_su_amt from POST_BASLN12 union all' || CHR(10) ||
                           '         -- zero baseline data for missing up-to-date records' || CHR(10) ||
                           '         (select prod_skid, acct_skid, mth_skid, basln_su_amt from POST_BASLN_ZERO' || CHR(10) ||
                           '           minus' || CHR(10) ||
                           '           select prod_skid, acct_skid, mth_skid, 0 as basln_su_amt from POST_BASLN12)' || CHR(10) ||
                           '        ) b' || CHR(10) ||
                           '      where b.prod_skid = a.prod_skid' || CHR(10) ||
                           '      union all' || CHR(10) ||
                           '      -- empty data for incoming year' || CHR(10) ||
                           '      select /*+ ORDERED USE_HASH(a b) */' || CHR(10) ||
                           '        b.prod_skid, b.acct_skid, c.mth_skid, -1 as basln_su_amt, a.avg_mths_cnt - 1 as avg_mths_cnt' || CHR(10) ||
                           '      from' || CHR(10) ||
                           '        SETTING a,' || CHR(10) ||
                           '        -- all gtin/ship-to pairs' || CHR(10) ||
                           '        POST_BASLN12_STAT b,' || CHR(10) ||
                           '        -- 12 future months' || CHR(10) ||
                           '        (select distinct' || CHR(10) ||
                           '           mth_skid' || CHR(10) ||
                           '         from CAL_MASTR_DIM' || CHR(10) ||
                           '         where mth_start_date between trunc(sysdate+6, ''mm'') and add_months(trunc(sysdate+6, ''mm''), 11)' ||CHR(10) ||
                           '        ) c' || CHR(10) ||
                           '      where b.prod_skid = a.prod_skid' || CHR(10) ||
                           '     )' || CHR(10) ||
                           '     )' || CHR(10) ||
                           '     WHERE avg_mths_cnt is null' || CHR(10) ||                           
                           '   ) '|| CHR(10) ||                                           
                           '  ) p' || CHR(10) ||
                           'where cat.prod_skid = p.prod_skid' || CHR(10) ||
                           '   AND p.mth_skid > ' || TO_CHAR(V_CYTD_MAX_MTH_SKID);
        -- Defect 2850, OPTIMA11, 2011-02-16, Martin, end;
		
        /**********************************************************/
        /* iterating through strategies, multiple query execution */
        /**********************************************************/
        PRO_PUT_LINE('Building temporary table (source data division by strategies)...');

        V_SQL := 'CREATE TABLE ' || C_PLAN_BASLN_TEMP1_TBL_NAME || CHR(10) ||
                 C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ' PARALLEL ' ||
                 V_P_DOP || CHR(10) || 'PARTITION BY RANGE (MTH_SKID)' || CHR(10) ||
                 'SUBPARTITION BY LIST (STRTGY_CODE)' || CHR(10) ||
                 'SUBPARTITION TEMPLATE (';
        -- partitioning by date, subpartitioning by strategies
        V_CURR_STRTGY := V_STRATEGIES.FIRST;
        IF V_STRATEGIES.COUNT > 0 THEN
            FOR I IN 1 .. V_STRATEGIES.COUNT
            LOOP
                IF I > 1 THEN
                    V_SQL := V_SQL || ',';
                END IF;
                V_SQL := V_SQL || CHR(10) || '  SUBPARTITION "' ||
                         V_CURR_STRTGY || '" VALUES (''' || V_CURR_STRTGY ||
                         ''') ' || V_TBLSPACE_NAME;
                IF I < V_STRATEGIES.COUNT THEN
                    V_CURR_STRTGY := V_STRATEGIES.NEXT(V_CURR_STRTGY);
                END IF;
            END LOOP;
        ELSE
            V_SQL := V_SQL || CHR(10) ||
                     '    SUBPARTITION "DEFAULT" VALUES (DEFAULT) ' ||
                     V_TBLSPACE_NAME;
        END IF;


        -- part 2: account location and time aggregation
        V_AGG2_SQL := '-- account location and wise monthly aggregation' ||
                      CHR(10) ||
                      'select /*+ NO_MERGE ORDERED USE_HASH(t basln) */' ||
                      CHR(10) ||
                      '  b.prod_skid, b.acct_skid, b.mth_skid, b.strtgy_code, sum(b.basln_su_amt) as basln_su_amt' ||
                      CHR(10) || 'from'
                      || CHR(10) || '  -- time hierarchy' ||
                      CHR(10) ||
                      '   -- post-event baseline with account location and strategy code derivation' ||
                      CHR(10) ||
                      '   (select /*+ NO_MERGE ORDERED USE_HASH(p b) */' ||
                      CHR(10) || '      b.prod_skid,' || CHR(10) ||
                      '      b.acct_skid,' || CHR(10) || '      b.mth_skid,' ||
                      CHR(10) || '      p.strtgy_code,' || CHR(10) ||
                      '      b.basln_su_amt' || CHR(10) || '    from' ||
                      CHR(10) ||
                      '      (SELECT /*+ NO_MERGE ORDERED USE_HASH(bu cat) */' ||
                      CHR(10) || '         cat.prod_skid,' || CHR(10) ||
                      '         nvl(cat.strtgy_code, bu.strtgy_code) as strtgy_code' ||
                      CHR(10) || '       FROM' || CHR(10) ||
                      '         -- business unit max promotion length defaults' ||
                      CHR(10) || '         (SELECT' || CHR(10) ||
                      '            bu.bus_unit_skid,' || CHR(10) ||
                      '           nvl(d.strtgy_code, g.strtgy_code) as strtgy_code' ||
                      CHR(10) ||
                      '          FROM OPT_BUS_UNIT_DIM bu, OPT_BUS_UNIT_PLC g, OPT_BUS_UNIT_PLC d' ||
                      CHR(10) || '          WHERE g.bus_unit_skid = 0' ||
                      CHR(10) || '          AND d.deflt_lvl_name is not null' ||
                      CHR(10) || '          AND d.bus_unit_skid (+) <> 0' ||
                      CHR(10) ||
                      '          AND bu.bus_unit_skid = d.bus_unit_skid (+)' ||
                      CHR(10) || '         ) bu,' || CHR(10) ||
                      '         (SELECT /*+ NO_MERGE */' || CHR(10) ||
                      '            h.prod_skid, h.bus_unit_skid, max(cat.strtgy_code) keep (dense_rank first order by h.fy_date_skid desc) as strtgy_code' ||
                      CHR(10) ||
                      '          FROM OPT_PROD_HIER_FDIM h, OPT_BASLN_CATEG_PARM_PLC cat' ||
                      CHR(10) || '          WHERE ' ||
                      REPLACE(V_BUS_UNIT_FLTR, '[A]', 'h') || CHR(10) ||
                      '          AND h.prod_lvl_desc = ''Brand''' || CHR(10) ||
                      '          AND h.prod_4_bus_unit_skid = cat.bus_unit_skid (+)' ||
                      CHR(10) ||
                      '          AND h.prod_4_name = cat.prod_name (+)' ||
                      CHR(10) ||
                      '          group by h.prod_skid, h.bus_unit_skid' ||
                      CHR(10) || '         ) cat' || CHR(10) ||
                      '       WHERE cat.bus_unit_skid = bu.bus_unit_skid' ||
                      CHR(10) || '      ) p,' || CHR(10) ||
                      '--b: Post Baseline with balance accounts(account location )'|| CHR(10) ||
                      '      (select /*+ NO_MERGE ORDERED USE_HASH(a b) */' ||
                      CHR(10) || '         b.prod_skid,' || CHR(10) ||
                      '         a.parnt_acct_skid as acct_skid,' || CHR(10) ||
                      '         h.mth_skid,' || CHR(10) ||
                      ' (nvl(B.BASLN_SU_AMT,0) * H.DAY_CNT / 7) AS basln_su_amt' ||
                      CHR(10) || ' from OPT_ACCT_SHIPT_ASSOC_SDIM a,' || CHR(10) ||
                      '       OPT_POST_BASLN_FCT b,OPT_CAL_MASTR_WK_DIM h' || CHR(10) ||
                      '       where b.wk_skid between [MIN_MTH_SKID] and [MAX_MTH_SKID]' ||
                      CHR(10) || '       and b.acct_skid = a.acct_skid' ||

                      CHR(10) || 'and b.wk_skid = h.wk_skid(+) ) b' || CHR(10) ||
                      '    where b.prod_skid = p.prod_skid' || CHR(10) ||
                      '    ) b' ||
                      CHR(10) ||
                      'group by b.prod_skid, b.acct_skid, b.mth_skid, b.strtgy_code';


        V_SQL := V_SQL || CHR(10) || ')' || CHR(10) || '(' || CHR(10) ||
                 '  PARTITION "CYTDYA" VALUES LESS THAN (' ||
                 V_CYTD_MIN_MTH_SKID || ') ' || C_FINAL_TBL_STORAGE || ' ' ||
                 V_TBLSPACE_NAME || ',' || CHR(10) ||
                 '  PARTITION "CYTD" VALUES LESS THAN (MAXVALUE) ' ||
                 C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || CHR(10) || ')' ||
                 CHR(10) || 'AS' || CHR(10) ||
                 REPLACE(REPLACE(V_AGG2_SQL, '[MIN_MTH_SKID]',
                                 V_CYTDYA_MIN_MTH_SKID), '[MAX_MTH_SKID]',
                         V_CYTD_MAX_MTH_SKID);
        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Table created successfuly (' || SQL%ROWCOUNT ||
                         ' rows processed).');
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table ' ||
                             C_PLAN_BASLN_TEMP1_TBL_NAME ||
                             ' failed with error - ' || SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table ' ||
                                         C_PLAN_BASLN_TEMP1_TBL_NAME ||
                                         ' failed with error - ' || SQLERRM);
        END;

        -- statistics
        PRO_PUT_LINE('Starting gathering statistics process for ' ||
                     C_PLAN_BASLN_TEMP1_TBL_NAME ||
                     ' table (estimate percent = ' || C_STATS_TEMP_EST_PERCENT ||
                     ')...');
        DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                             'CURRENT_SCHEMA'),
                                      TABNAME => C_PLAN_BASLN_TEMP1_TBL_NAME,
                                      ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                      METHOD_OPT => 'FOR COLUMNS SIZE 1',
                                      CASCADE => FALSE);
        PRO_PUT_LINE('Statistics gathered.');

        /******************************************************************************************/
        /* calculate BRAND planning baseline into  c_plan_basln_temp2_tbl_name              */
        /******************************************************************************************/

        -- final data creation (for each data separate DML statement)
        V_CURR_STRTGY := V_STRATEGIES.FIRST;
        FOR I IN 1 .. V_STRATEGIES.COUNT
        LOOP

            V_SQL := 'INSERT /*+ APPEND PARALLEL(' || V_P_DOP || ') */ INTO ' ||
                     C_PLAN_BASLN_TEMP2_TBL_NAME ||
                     ' (PROD_SKID, ACCT_SKID, MTH_SKID, BASLN_SU_AMT)' ||
                     CHR(10);
            IF V_CURR_STRTGY = C_STRTGY_CODE_GROWTH THEN
                -- growth rate calculation
                PRO_PUT_LINE('Growth Rate calculation strategy execution');

                V_SQL := V_SQL ||
                         REPLACE(REPLACE(V_GROWTH_SQL_TMPLT, '[AGG_VIEW12]',
                                         'select prod_skid, acct_skid, mth_skid, basln_su_amt' ||
                                          CHR(10) || '    from ' ||
                                          C_PLAN_BASLN_TEMP1_TBL_NAME ||
                                          ' partition (CYTD)' || CHR(10) ||
                                          '    where strtgy_code = ''' ||
                                          V_CURR_STRTGY || ''''), '[AGG_VIEW24]',
                                 'select prod_skid, acct_skid, mth_skid, basln_su_amt' ||
                                  CHR(10) || '    from ' ||
                                  C_PLAN_BASLN_TEMP1_TBL_NAME ||
                                  ' partition (CYTDYA)' || CHR(10) ||
                                  '    where strtgy_code = ''' || V_CURR_STRTGY || '''');

            ELSIF V_CURR_STRTGY = C_STRTGY_CODE_NOGROWTH THEN
                -- growth rate equal to 1
                PRO_PUT_LINE('No Growth Rate calculation strategy (GR=1) execution');

                V_SQL := V_SQL ||
                         REPLACE(V_NOGROWTH_SQL_TMPLT, '[AGG_VIEW12]',
                                 'select prod_skid, acct_skid, mth_skid, basln_su_amt' ||
                                  CHR(10) || '    from ' ||
                                  C_PLAN_BASLN_TEMP1_TBL_NAME ||
                                  ' partition (CYTD)' || CHR(10) ||
                                  '    where strtgy_code = ''' || V_CURR_STRTGY || '''');

            ELSE
                -- averages
                -- Updated by David for B016 defect fix on 2010-3-12
                PRO_PUT_LINE('Running Averages calculation strategy execution');

                V_SQL := V_SQL ||
                         REPLACE(V_AVG_SQL_TMPLT, '[AGG_VIEW12]',
                                 'select prod_skid, acct_skid, s.mth_skid, basln_su_amt' ||
                                  CHR(10) || '    from ' ||
                                  C_PLAN_BASLN_TEMP1_TBL_NAME || ' s, MTHS_CAL c' ||CHR(10) ||
                                  '    where c.mth_skid = s.mth_skid ' ||CHR(10) ||
                                  '        and strtgy_code = ''' || V_CURR_STRTGY || '''');
            END IF;

            -- DML execution
            PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                         REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
            BEGIN
                EXECUTE IMMEDIATE V_SQL;
                PRO_PUT_LINE('Data created successfuly (' || SQL%ROWCOUNT ||
                             ' rows processed)');
                COMMIT;
            EXCEPTION
                WHEN OTHERS THEN
                    PRO_PUT_LINE('Error: Data creation on ' ||
                                 C_PLAN_BASLN_TEMP2_TBL_NAME ||
                                 ' failed with error - ' || SQLERRM);
                    RAISE_APPLICATION_ERROR(C_TBL_INS_ERROR,
                                            'Error: Data creation on ' ||
                                             C_PLAN_BASLN_TEMP2_TBL_NAME ||
                                             ' failed with error - ' || SQLERRM);
            END;

            IF I < V_STRATEGIES.COUNT THEN
                V_CURR_STRTGY := V_STRATEGIES.NEXT(V_CURR_STRTGY);
            END IF;
        END LOOP;

        /******************************************************************************************/
        /* aggregate GTIN actual SU to accounts and month. and calculate GTIN weight             */
        /******************************************************************************************/

        -- prepare temporary table(gtin, month, account) create statement (final table - partitionned)
        -- Aggregate SU to month
       /* V_SQL := 'INSERT \*+ APPEND  *\ INTO ' ||
                 C_POST_GTIN_MTH_TEMP_TBL_NAME || ' nologging '||CHR(10) ||
                 ' (MTH_SKID, PROD_SKID, BRAND_SKID, GTIN_STTUS_CODE, ACCT_SKID, VOL_SU_AMT, PAST_MTHS_CNT, GTIN_WEIGHT) ' ||
                 CHR(10) || '(SELECT \*+ NO_MERGE *\' || CHR(10) ||
              '   --DISTINCT ' || CHR(10) ||
              '           C.MTH_SKID, ' || CHR(10) ||
              '           S.PROD_SKID,' || CHR(10) ||
              '           S.BRAND_SKID,' || CHR(10) ||
              '           S.GTIN_STTUS_CODE,' || CHR(10) ||
              '           S.ACCT_SKID,' || CHR(10) ||
              '           SUM(S.VOL_SU_AMT * C.DAY_CNT / 7) AS VOL_SU_AMT, ' || CHR(10) ||
              '           S.PAST_MTHS_CNT, ' || CHR(10) ||
              '           0 AS GTIN_WEIGHT ' || CHR(10) ||
              '  FROM OPT_CAL_MASTR_WK_DIM C,' || CHR(10) ||
              '       OPT_GTIN_ACCT_AGG_TMP S' || CHR(10) ||
              '  WHERE C.WK_SKID = S.WK_SKID ' || CHR(10) ||
              '  GROUP BY C.MTH_SKID,' || CHR(10) ||
              '           S.PROD_SKID,' || CHR(10) ||
              '           S.BRAND_SKID,' || CHR(10) ||
              '           S.GTIN_STTUS_CODE,' || CHR(10) ||
              '           S.ACCT_SKID, ' || CHR(10) ||
              '          S.PAST_MTHS_CNT)';

        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));

        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Table populated successfuly.');
            COMMIT;
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table ' ||
                             C_POST_GTIN_MTH_TEMP_TBL_NAME ||
                             ' failed with error - ' || SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table ' ||
                                         C_POST_GTIN_MTH_TEMP_TBL_NAME ||
                                         ' failed with error - ' || SQLERRM);
        END;

        -- statistics
        PRO_PUT_LINE('Starting gathering statistics process for ' ||
                     C_POST_GTIN_MTH_TEMP_TBL_NAME ||
                     ' table (estimate percent = ' || C_STATS_TEMP_EST_PERCENT ||
                     ')...');
        DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                             'CURRENT_SCHEMA'),
                                      TABNAME => C_POST_GTIN_MTH_TEMP_TBL_NAME,
                                      ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                      METHOD_OPT => 'FOR COLUMNS SIZE 1',
                                      CASCADE => FALSE);
        PRO_PUT_LINE('Statistics gathered.');*/

        -- Month skid filling
        /*V_SQL := 'INSERT \*+ APPEND*\ INTO ' ||
                 C_GTIN_ACCT_AGG_TEMP || ' nologging '||CHR(10) ||
                 ' (MTH_SKID, PROD_SKID, BRAND_SKID, GTIN_STTUS_CODE, ACCT_SKID, VOL_SU_AMT, PAST_MTHS_CNT, GTIN_WEIGHT) ' ||
                 CHR(10) ||
                 '-- account location and wise monthly aggregation for shipment GTIN actual SU' || CHR(10) ||
                    'SELECT DISTINCT C.MTH_SKID,' || CHR(10) ||
                    'A.PROD_SKID,' || CHR(10) ||
                    'A.BRAND_SKID,' || CHR(10) ||
                    'A.GTIN_STTUS_CODE,' || CHR(10) ||
                    'A.ACCT_SKID,' || CHR(10) ||
                    '0 AS VOL_SU_AMT,' || CHR(10) ||
                    'A.PAST_MTHS_CNT,' || CHR(10) ||
                    '0 AS GTIN_WEIGHT ' || CHR(10) ||
    'FROM OPT_POST_GTIN_MTH_FCT_TMP A,' || CHR(10) ||
    '     (SELECT DISTINCT MTH_SKID FROM OPT_CAL_MASTR_WK_DIM) C' || CHR(10) ||
    'WHERE C.MTH_SKID >= A.MTH_SKID' || CHR(10) ||
    'AND NOT EXISTS ' || CHR(10) ||
          '(SELECT ''x'' FROM OPT_POST_GTIN_MTH_FCT_TMP A2' || CHR(10) ||
           'WHERE A.PROD_SKID = A2.PROD_SKID' || CHR(10) ||
          ' AND A.BRAND_SKID = A2.BRAND_SKID' || CHR(10) ||
           'AND A.GTIN_STTUS_CODE = A2.GTIN_STTUS_CODE' || CHR(10) ||
           'AND A.ACCT_SKID = A2.ACCT_SKID' || CHR(10) ||
          ' AND C.MTH_SKID = A2.MTH_SKID)';

        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Table populated successfuly.');
            COMMIT;
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table ' ||
                             C_GTIN_ACCT_AGG_TEMP ||
                             ' failed with error - ' || SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table ' ||
                                         C_GTIN_ACCT_AGG_TEMP ||
                                         ' failed with error - ' || SQLERRM);
        END;

        -- statistics
        PRO_PUT_LINE('Starting gathering statistics process for ' ||
                     C_GTIN_ACCT_AGG_TEMP ||
                     ' table (estimate percent = ' || C_STATS_TEMP_EST_PERCENT ||
                     ')...');
        DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                             'CURRENT_SCHEMA'),
                                      TABNAME => C_GTIN_ACCT_AGG_TEMP,
                                      ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                      METHOD_OPT => 'FOR COLUMNS SIZE 1',
                                      CASCADE => FALSE);
        PRO_PUT_LINE('Statistics gathered.');*/

        -- maintain shipment history GTIN and remove status code = 'Remnant' products
        -- calculate GTIN weight
        V_SQL := 'create table ' || C_GTIN_WEIGHT_TEMP_TBL_NAME || CHR(10) ||
                 C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ' PARALLEL ' ||
                 V_P_DOP || CHR(10) ||
                 'AS ' || CHR(10) ||
                 'SELECT F.PROD_SKID,' || CHR(10) ||
                 '       F.BRAND_SKID,' || CHR(10) ||
                 '       F.GTIN_STTUS_CODE,' || CHR(10) ||
                 '       F.ACCT_SKID,' || CHR(10) ||
                 '       DECODE(F.BRAND_AMT, 0, 0, ROUND(F.PROD_AMT / F.BRAND_AMT, 7)) AS GTIN_WEIGHT' || CHR(10) ||
                 '       --F.PROD_CNT,' || CHR(10) ||
                 '       --F.PAST_MTH_CNT' || CHR(10) ||
                 'FROM (SELECT F.PROD_SKID,' || CHR(10) ||
                 '             F.BRAND_SKID,' || CHR(10) ||
                 '             GTIN_STTUS_CODE,' || CHR(10) ||
                 '             ACCT_SKID,' || CHR(10) ||
                 '             VOL_SU_AMT,' || CHR(10) ||
                 '             --F.PAST_MTH_CNT,' || CHR(10) ||
                 '             COUNT(1) OVER(PARTITION BY F.PROD_SKID, ACCT_SKID, GTIN_STTUS_CODE ORDER BY F.MTH_SKID DESC) AS PROD_CNT,' ||CHR(10) ||
                 '             SUM(VOL_SU_AMT) OVER(PARTITION BY F.PROD_SKID, ACCT_SKID ORDER BY F.MTH_SKID ASC) AS PROD_AMT,' ||CHR(10) ||
                 '             SUM(VOL_SU_AMT) OVER(PARTITION BY F.BRAND_SKID, ACCT_SKID) AS BRAND_AMT' ||CHR(10) ||
                 '      FROM -- maintain shipment history GTIN and remove Remnant products' || CHR(10) ||
                 '            OPT_GTIN_ACCT_AGG_TMP F,' ||CHR(10) ||
                 '            OPT_CAL_MASTR_MTH_DIM C,' || CHR(10) ||
                 '            OPT_PROD_FDIM P' || CHR(10) ||
                 '      WHERE F.MTH_SKID = C.MTH_SKID' || CHR(10) ||
                 '      AND F.PROD_SKID = P.PROD_SKID' || CHR(10) ||
                 '      AND P.BUOM_IND = ''Y'' ' || CHR(10) ||
                 '      AND C.DAY_DATE >=' || CHR(10) ||
                 '            ADD_MONTHS(TRUNC(SYSDATE + 6, ''MM''), -F.PAST_MTH_CNT)' || CHR(10) ||
                 '      AND C.DAY_DATE < TRUNC(SYSDATE + 6, ''MM'')) F' || CHR(10) ||
                 'WHERE F.PROD_CNT = 1';

        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Table populated successfuly.');
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table ' ||
                             C_GTIN_WEIGHT_TEMP_TBL_NAME ||
                             ' failed with error - ' || SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table ' ||
                                         C_GTIN_WEIGHT_TEMP_TBL_NAME ||
                                         ' failed with error - ' || SQLERRM);
        END;

        -- statistics
        PRO_PUT_LINE('Starting gathering statistics process for ' ||
                     C_GTIN_ACCT_AGG_TEMP ||
                     ' table (estimate percent = ' || C_STATS_TEMP_EST_PERCENT ||
                     ')...');
        DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                             'CURRENT_SCHEMA'),
                                      TABNAME => C_GTIN_WEIGHT_TEMP_TBL_NAME,
                                      ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                      METHOD_OPT => 'FOR COLUMNS SIZE 1',
                                      CASCADE => FALSE);
        PRO_PUT_LINE('Statistics gathered.');

        /******************************************************************************************/
        /* split brand planning baseline into GTIN                                                */
        /******************************************************************************************/

        V_SQL := 'INSERT /*+ APPEND */ INTO ' ||
                 C_GTIN_PLAN_BASLN_TEMP ||
                 ' (PROD_SKID, ACCT_SKID, MTH_SKID, BASLN_SU_AMT)' || CHR(10) ||
                 ' select  g.prod_skid,  b.acct_skid, b.mth_skid,  b.basln_su_amt*g.gtin_weight as basln_su_amt ' ||
                 CHR(10) || ' from  ' || C_PLAN_BASLN_TEMP2_TBL_NAME || ' b, ' ||
                 C_GTIN_WEIGHT_TEMP_TBL_NAME || ' g ' || CHR(10) ||
                 ' where b.prod_skid = g.brand_skid ' || CHR(10) ||
                 ' and b.acct_skid = g.acct_skid ' || CHR(10) ||
                 ' and g.gtin_sttus_code not in ( ''Inactive'', ''Historical'') ';

        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Table populated successfuly.');
            COMMIT;
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table ' ||
                             C_GTIN_PLAN_BASLN_TEMP || ' failed with error - ' ||
                             SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table ' ||
                                         C_GTIN_PLAN_BASLN_TEMP ||
                                         ' failed with error - ' || SQLERRM);
        END;
        
        -- copy forward functionality
        FOR v_idx IN 1..P_COPY_MTH LOOP
        
            V_SQL := '
            INSERT INTO '||C_GTIN_PLAN_BASLN_TEMP||'
            (PROD_SKID, ACCT_SKID, MTH_SKID, BASLN_SU_AMT)
            SELECT
            f.PROD_SKID, f.ACCT_SKID, cal.MTH_SKID, f.BASLN_SU_AMT
            FROM '||C_GTIN_PLAN_BASLN_TEMP||' f
            join opt_cal_mastr_dim cal
            on cal.day_date = trunc(add_months(sysdate,'||P_GEN_MTH||'-1 + '||v_idx||'),''MM'')
            WHERE f.MTH_SKID = (select cal_mastr_skid
                from opt_cal_mastr_dim
                where day_date = trunc(add_months(sysdate,'||P_GEN_MTH||'-1 ),''MM'')
                             ) 
            ' ;     
            EXECUTE IMMEDIATE V_SQL;
            
        END LOOP;
        PRO_PUT_LINE('Copy forward done for ' ||P_COPY_MTH||' months');
        COMMIT;

        -- statistics
        PRO_PUT_LINE('Starting gathering statistics process for ' ||
                     C_GTIN_PLAN_BASLN_TEMP || ' table (estimate percent = ' ||
                     C_STATS_TEMP_EST_PERCENT || ')...');
        DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                             'CURRENT_SCHEMA'),
                                      TABNAME => C_GTIN_PLAN_BASLN_TEMP,
                                      ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                      METHOD_OPT => 'FOR COLUMNS SIZE 1',
                                      CASCADE => FALSE);
        PRO_PUT_LINE('Statistics gathered.');

        /***********************************************************************************/
        /* promote data to final OPT_PLAN_BASLN_FCT table and statistics gathering process */
        /***********************************************************************************/
        BEGIN
            PRO_PUT_LINE('Starting data promotion from ' ||
                         C_GTIN_PLAN_BASLN_TEMP || ' to OPT_PLAN_BASLN_FCT...');
            -- create missing partitions
            OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN(P_VC2_TBL_NAME => 'OPT_PLAN_BASLN_FCT',
                                                        P_NUM_PAST_MTH_NUM => 0,
                                                        P_NUM_FUTURE_MTH_NUM => P_GEN_MTH + P_COPY_MTH,
                                                        P_VC2_DROP_PRTTN_FLAG => 'N');
            -- promote data
            OPT_PKG_PRTTN_UTIL.PRO_PROMOTE_DATA(P_VC2_SRCE_TBL => C_GTIN_PLAN_BASLN_TEMP,
                                                P_VC2_DEST_TBL => 'OPT_PLAN_BASLN_FCT',
                                                P_VC2_MIN_CAL_SKID => NULL,
                                                P_VC2_REGION => '',
                                                P_VC2_CTRLM_ORDR_ID => P_ORDER_ID,
                                                P_VC2_PRTTN_TYPE_CODE => 'M',
												V_REGN => V_REGN);--SOX IMPLEMENTATION PROJECT
            PRO_PUT_LINE('Data promoted succesfully.');
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Promotion of data from ' ||
                             C_GTIN_PLAN_BASLN_TEMP ||
                             ' to OPT_PLAN_BASLN_FCT failed with error - ' ||
                             SQLERRM);
                RAISE_APPLICATION_ERROR(C_DATA_PROMO_ERROR,
                                        'Error: Promotion of data from ' ||
                                         C_GTIN_PLAN_BASLN_TEMP ||
                                         ' to OPT_PLAN_BASLN_FCT failed with error - ' ||
                                         SQLERRM);
        END;

        -- drop temporary tables
        -- drop_table(c_plan_basln_temp1_tbl_name);
        -- drop_table(c_gtin_plan_basln_temp);
        -- drop_table(c_post_gtin_mth_temp_tbl_name);
        -- drop_table(c_gtin_weight_temp_tbl_name);

        -- gather statistics for all involved partitions
        PRO_PUT_LINE('Starting gathering statistics on OPT_PLAN_BASLN_FCT table...');
        DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                             'CURRENT_SCHEMA'),
                                      TABNAME => 'OPT_PLAN_BASLN_FCT',
                                      GRANULARITY => 'GLOBAL',
                                      ESTIMATE_PERCENT => C_STATS_FINAL_EST_PERCENT,
                                      BLOCK_SAMPLE => FALSE, DEGREE => 1,
                                      METHOD_OPT => 'FOR ALL COLUMNS SIZE AUTO');
        PRO_PUT_LINE('Statistics on table OPT_PLAN_BASLN_FCT table gathered.');
        PRO_PUT_LINE('Starting gathering statistics on refreshed partitions of OPT_PLAN_BASLN_FCTT table...');
        FOR P IN (SELECT OBJECT_NAME,
                         SUBOBJECT_NAME
                  FROM USER_OBJECTS
                  WHERE OBJECT_NAME = 'OPT_PLAN_BASLN_FCT'
                  AND OBJECT_TYPE = 'TABLE PARTITION'
                  AND LAST_DDL_TIME >= V_START_DATE)
        LOOP
            PRO_PUT_LINE('Gathering statistics for ' || P.OBJECT_NAME || '.' ||
                         P.SUBOBJECT_NAME);
            DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => SYS_CONTEXT('USERENV',
                                                                 'CURRENT_SCHEMA'),
                                          TABNAME => P.OBJECT_NAME,
                                          PARTNAME => P.SUBOBJECT_NAME,
                                          GRANULARITY => 'PARTITION',
                                          ESTIMATE_PERCENT => C_STATS_FINAL_EST_PERCENT,
                                          BLOCK_SAMPLE => FALSE, DEGREE => 1,
                                          METHOD_OPT => 'FOR ALL COLUMNS SIZE AUTO');
        END LOOP;
    EXCEPTION
        WHEN OTHERS THEN
            PRO_PUT_LINE('Error: Unexpected system error - ' || SQLERRM);
            RAISE;
    END;

END;
/

