CREATE OR REPLACE PROCEDURE ADWU_OPTIMA_LAEX.OPT_FILTR_LKP_RESTATE (IN_REGN       VARCHAR2,
                                                  IN_INIT_FLAG  VARCHAR2,
                                                  IN_FTBL_NAME  VARCHAR2,
                                                  IN_SFTBL_NAME VARCHAR2,
                                                  R_RTN_VALUE   OUT NUMBER,
                                                  R_RTN_MSG     OUT VARCHAR2,
												  V_REGN VARCHAR2 DEFAULT NULL)
------------------------------------------------------------------------------------
  -- Procedure    : OPT_FILTR_LKP_RESTATE                                           --
  -- Originator   : Gao Yongwei (yongwei.gao@hp.com)                                --
  -- Author       : Gao Yongwei (yongwei.gao@hp.com)                                --
  -- Date         : 04-Sep-2007                                                     --
  -- Purpose      : loading OPT_FILTR_LKP table process                             --
  ------------------------------------------------------------------------------------
  -- Parameters
  ------------------------------------------------------------------------------------
  -- No  Name              Desctiption          /   Example                         --
  ------------------------------------------------------------------------------------
  -- 1  in_Regn              Region Code        /   'APAC'                          --
  -- 2  in_Init_Flag         Incremental flag   /   'Y'                             --
  -- 3  in_Ftbl_Name         Fact table         /   'OPT_FUND_FCT'                  --
  -- 4  in_Sftbl_Name        Sfact table        /   'OPT_FUND_SFCT'                 --
  -- 5  r_Rtn_value          Region code        /   '0'                             --
  -- 6  r_Rtn_msg            Region msg         /   'Success....'                   --
  ------------------------------------------------------------------------------------
  -- Change History                                                                 --
  -- Date        Programmer        Description                                      --
  -- ----------- ----------------- ---------------------------------------------------
  -- 04-Sep-2007 Gao Yongwei      Initial Version                                   --
  -- 22-Sep-2007 Gao Yongwei      Transfer Gtin To Brand Skid                       --
  --                              Add Refres Model Tweak                            --
  --                              Add exception process for tmp table               --
  -- 25-Sep-2007 Gao Yongwei      Add ACTVY_SKID into filter                        --
  -- 11-Oct 2007 Gao Yongwei      Agg Gtin to Brand for accrl Fact table            --
  -- 03-Feb-2008 Hill Pan         Add FY_DATE_SKID for Dated PROD HIER.
  --17-Dec-2008 David Lang    Add OPT_SHPMT_ACTL_FRCST_FCT for F030--
  -- 03-Apr-2009 Mervyn Lin       Disable OPT_SHPMT_ACTL_FRCST_FCT(one number report fact)
  --                               data loading, aggregate data to fiscal year level
  --                               instead of monthly level(DATE_SKID)
  --09-Oct-2009 HongXing Zhang    Add OPT_BASLN_FCT for F004--
  -- replace OPT_INTMD_ACCRL_GTIN_FCT by OPT_FUND_ACCRL_FCT for B007 in R10 by David --
  -- Added by David for b007 in R10                                                  --
  -- Modified by Lou Yang 20100125 in R10 For B007                                   --
  -- Add some comment at header                                                      --
  --02-Dec-2010 Asca add new source OPT_TSP_LVL_BASLN_IFCT                           --
  ------------------------------------------------------------------------------------
 AS
  -- constants
  -- Added by Myra 20090928 in R9. for tuning
  C_FINAL_TBL_STORAGE VARCHAR2(100) := 'PCTFREE 0 PCTUSED 0 INITRANS 1 MAXTRANS 255';

  --V_TBLSPACE_NAME     VARCHAR2(20) := 'TABLESPACE OPTIMA01';
  V_TBLSPACE_NAME     VARCHAR2(50);
  V_INDEX             VARCHAR2(50);

  P_DOP               NUMBER := 2;
  --Define parameters for exception handling
  DEADLOCK_DETECTED EXCEPTION;
  PRAGMA EXCEPTION_INIT(DEADLOCK_DETECTED, -60);
  V_CODE              NUMBER := SQLCODE;
  V_ERRM              VARCHAR2(64) := SUBSTR(SQLERRM, 1, 64);
  SQL_MSG             VARCHAR2(200);
  V_ETL_RUN_ID        NUMBER(15);
  G_SQLSTMT           VARCHAR2(5000);
  CONST_FCT_SFCT_RATE NUMBER(4) := 10 / 2;
  COUNT_OF_FCT        NUMBER(38);
  COUNT_OF_SFCT       NUMBER(38);
  V_INIT_FLAG         VARCHAR2(4) := IN_INIT_FLAG;
  V_NUMBER_TBL_COUNT  NUMBER;
BEGIN
  --Initinal variable to achieve ETL_RUN_ID
  SELECT OPT_ETL_RUN_ID_SEQ.NEXTVAL INTO V_ETL_RUN_ID FROM DUAL;
  
  OPT_AX_TBLSPACE(V_REGN,V_TBLSPACE_NAME,V_INDEX);--SOX IMPLEMENTATION PROJECT

  --Initinal variable v_Init_Flag used to choose refresh model(Default : N)
  G_SQLSTMT := 'select count(1) from ' || IN_FTBL_NAME;
  EXECUTE IMMEDIATE G_SQLSTMT
    INTO COUNT_OF_FCT;

  G_SQLSTMT := 'select count(1) from ' || IN_SFTBL_NAME;
  EXECUTE IMMEDIATE G_SQLSTMT
    INTO COUNT_OF_SFCT;

  IF COUNT_OF_SFCT * CONST_FCT_SFCT_RATE >= COUNT_OF_FCT THEN
    V_INIT_FLAG := 'Y';
  END IF;
  DBMS_OUTPUT.PUT_LINE('Flag Set ' || V_INIT_FLAG );
  DBMS_OUTPUT.PUT_LINE('Count Of Fct Table ' || IN_FTBL_NAME || ' :' ||
                       COUNT_OF_FCT);
  DBMS_OUTPUT.PUT_LINE('Count Of Sfct Table ' || IN_SFTBL_NAME || ' :' ||
                       COUNT_OF_SFCT);
  DBMS_OUTPUT.PUT_LINE('Filter Refresh Model :' || V_INIT_FLAG);
  --------------------------------------------------------------------------------------------------------------------------
  -- STEP 010: fully refresh table OPT_FILTR_LKP.
  --------------------------------------------------------------------------------------------------------------------------
  IF V_INIT_FLAG = 'Y' THEN

    CASE
    --This is for F030
    --OPT_SHPMT_ACTL_FRCST_FCT
    --Need to disable data loading from one number report fact
      WHEN IN_FTBL_NAME = 'OPT_SHPMT_ACTL_FRCST_FCT' THEN
        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';
        /*
        WHEN IN_FTBL_NAME='OPT_SHPMT_ACTL_FRCST_FCT' THEN
            DELETE FROM OPT_FILTR_LKP WHERE TBL_NAME = 'OPT_SHPMT_ACTL_FRCST_FCT' ;
            COMMIT;
            INSERT INTO OPT_FILTR_LKP
              (TBL_NAME,
               FUND_SKID,
               ACCT_FUND_SKID,
               PROD_SKID,
               PRMTN_SKID,
               DATE_SKID,
               BUS_UNIT_SKID,
               FUND_PROD_SKID,
               FUND_GRP_SKID,
               ACCT_PRMTN_SKID,
               FY_DATE_SKID,
               ETL_RUN_ID)
               SELECT 'OPT_SHPMT_ACTL_FRCST_FCT',
                     FUND_SKID,
                     ACCT_FUND_SKID,
                     PROD_SKID,
                     PRMTN_SKID,
                     DATE_SKID,
                     BUS_UNIT_SKID,
                     FUND_PROD_SKID,
                     FUND_GRP_SKID,
                     ACCT_PRMTN_SKID,
                     FY_DATE_SKID,
                     V_ETL_RUN_ID
                 FROM (SELECT DISTINCT 0 FUND_SKID,
                                      0 ACCT_FUND_SKID,
                                      PROD_SKID,
                                      0 PRMTN_SKID,
                                      FY_DATE_SKID DATE_SKID,
                                      BUS_UNIT_SKID,
                                      0 FUND_PROD_SKID,
                                      0 FUND_GRP_SKID,
                                      ACCT_PRMTN_SKID,
                                      FY_DATE_SKID
                        FROM OPT_SHPMT_ACTL_FRCST_FCT
                        ) A;
            COMMIT;
            R_RTN_VALUE := 0;
            R_RTN_MSG   := 'Success....';
         */

    ---Optima90, B001, Bean
    ---OPT_EPOS_VALDN_FCT
      WHEN IN_FTBL_NAME = 'OPT_EPOS_VALDN_FCT' THEN

        DELETE FROM OPT_FILTR_LKP WHERE TBL_NAME = 'OPT_EPOS_VALDN_FCT';
        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        -- move parallel hint to subquery by Leo on Jan 26, 2011 - begin
        INSERT /*+ APPEND */
        INTO OPT_FILTR_LKP
          (TBL_NAME,
           FUND_SKID,
           ACCT_FUND_SKID,
           PROD_SKID,
           PRMTN_SKID,
           DATE_SKID,
           BUS_UNIT_SKID,
           FUND_PROD_SKID,
           FUND_GRP_SKID,
           ACCT_PRMTN_SKID,
           FY_DATE_SKID,
           ETL_RUN_ID)
          SELECT 'OPT_EPOS_VALDN_FCT',
                 FUND_SKID,
                 ACCT_FUND_SKID,
                 PROD_SKID,
                 PRMTN_SKID,
                 DATE_SKID,
                 BUS_UNIT_SKID,
                 FUND_PROD_SKID,
                 FUND_GRP_SKID,
                 ACCT_PRMTN_SKID,
                 FY_DATE_SKID,
                 V_ETL_RUN_ID
            FROM (SELECT /*+ parallel(a, 2) */ DISTINCT 0 FUND_SKID,
                                  0 ACCT_FUND_SKID,
                                  PROD_SKID,
                                  0 PRMTN_SKID,
                                  DATE_SKID DATE_SKID,
                                  BUS_UNIT_SKID,
                                  0 FUND_PROD_SKID,
                                  0 FUND_GRP_SKID,
                                  ACCT_SKID ACCT_SKID,
                                  ACCT_SKID ACCT_PRMTN_SKID,
                                  FY_DATE_SKID
                    FROM OPT_EPOS_VALDN_FCT) A;
        -- move parallel hint to subquery by Leo on Jan 26, 2011 - end
        COMMIT;
        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --- Added by joy on Oct 09, 2009 for B004
      WHEN IN_FTBL_NAME = 'OPT_BASLN_FCT' THEN

        DELETE FROM OPT_FILTR_LKP WHERE TBL_NAME = 'OPT_BASLN_FCT';
        COMMIT;

      -- move parallel hint into subquery by Leo on Jan 26, 2011 - begin
        INSERT /*+ APPEND */
        INTO OPT_FILTR_LKP
          (TBL_NAME,
           FUND_SKID,
           ACCT_FUND_SKID,
           PROD_SKID,
           PRMTN_SKID,
           DATE_SKID,
           BUS_UNIT_SKID,
           FUND_PROD_SKID,
           FUND_GRP_SKID,
           ACCT_PRMTN_SKID,
           FY_DATE_SKID,
           ETL_RUN_ID)
          SELECT 'OPT_BASLN_FCT',
                 FUND_SKID,
                 ACCT_FUND_SKID,
                 PROD_SKID,
                 PRMTN_SKID,
                 DATE_SKID,
                 BUS_UNIT_SKID,
                 FUND_PROD_SKID,
                 FUND_GRP_SKID,
                 ACCT_PRMTN_SKID,
                 FY_DATE_SKID,
                 V_ETL_RUN_ID
            FROM (SELECT /*+ parallel(a, 2) */ DISTINCT 0 FUND_SKID,
                                  0 ACCT_FUND_SKID,
                                  PROD_SKID,
                                  PRMTN_SKID,
                                  WK_SKID DATE_SKID,
                                  BUS_UNIT_SKID,
                                  0 FUND_PROD_SKID,
                                  0 FUND_GRP_SKID,
                                  ACCT_SKID ACCT_PRMTN_SKID,
                                  FY_DATE_SKID
                    FROM OPT_BASLN_FCT a
                   WHERE PRMTN_SKID IS NOT NULL) A;
      -- move parallel hint into subquery by Leo on Jan 26, 2011 - end
        COMMIT;
        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_ACTVY_SPNDG_MTH_FCT
      WHEN IN_FTBL_NAME = 'OPT_ACTVY_SPNDG_MTH_FCT' THEN

        DELETE /*+ PARALLEL(4) */ FROM OPT_FILTR_LKP
         WHERE TBL_NAME = 'OPT_ACTVY_SPNDG_MTH_FCT';
        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        -- move parallel hint into sub-query by Leo on Jan 26, 2010 - begin
        INSERT /*+ APPEND */
        INTO OPT_FILTR_LKP
          (TBL_NAME,
           FUND_SKID,
           ACCT_FUND_SKID,
           PROD_SKID,
           PRMTN_SKID,
           DATE_SKID,
           BUS_UNIT_SKID,
           FUND_PROD_SKID,
           FUND_GRP_SKID,
           ACCT_PRMTN_SKID,
           FY_DATE_SKID,
           ETL_RUN_ID)
          SELECT 'OPT_ACTVY_SPNDG_MTH_FCT',
                 FUND_SKID,
                 ACCT_FUND_SKID,
                 PROD_SKID,
                 PRMTN_SKID,
                 DATE_SKID,
                 BUS_UNIT_SKID,
                 FUND_PROD_SKID,
                 FUND_GRP_SKID,
                 ACCT_PRMTN_SKID,
                 FY_DATE_SKID,
                 V_ETL_RUN_ID
            FROM (SELECT /*+ parallel(a, 2) */DISTINCT FUND_SKID,
                                  0 ACCT_FUND_SKID,
                                  PROD_SKID,
                                  0 PRMTN_SKID,
                                  0 DATE_SKID,
                                  BUS_UNIT_SKID,
                                  0 FUND_PROD_SKID,
                                  0 FUND_GRP_SKID,
                                  ACCT_SKID AS ACCT_PRMTN_SKID,
                                  FY_DATE_SKID
                    FROM OPT_ACTVY_SPNDG_MTH_FCT a) A;
        -- move parallel hint into sub-query by Leo on Jan 26, 2010 - end
        COMMIT;
        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    ---OPT_FUND_GEN_SPNDG_FCT
      WHEN IN_FTBL_NAME = 'OPT_FUND_GEN_SPNDG_FCT' THEN

        DELETE /*+ PARALLEL(4) */ FROM OPT_FILTR_LKP
         WHERE TBL_NAME = 'OPT_FUND_GEN_SPNDG_FCT';
        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        -- move parallel hint into sub-query by Leo on Jan 26, 2010 - begin
        INSERT /*+ APPEND */
        INTO OPT_FILTR_LKP
          (TBL_NAME,
           FUND_SKID,
           ACCT_FUND_SKID,
           PROD_SKID,
           PRMTN_SKID,
           DATE_SKID,
           BUS_UNIT_SKID,
           FUND_PROD_SKID,
           FUND_GRP_SKID,
           ACCT_PRMTN_SKID,
           FY_DATE_SKID,
           ETL_RUN_ID)
          SELECT 'OPT_FUND_GEN_SPNDG_FCT',
                 FUND_SKID,
                 ACCT_FUND_SKID,
                 PROD_SKID,
                 PRMTN_SKID,
                 DATE_SKID,
                 BUS_UNIT_SKID,
                 FUND_PROD_SKID,
                 FUND_GRP_SKID,
                 ACCT_PRMTN_SKID,
                 FY_DATE_SKID,
                 V_ETL_RUN_ID
            FROM (SELECT /*+ parallel(a, 2) */ DISTINCT FUND_SKID,
                                  ACCT_SKID ACCT_FUND_SKID,
                                  PROD_SKID,
                                  0 PRMTN_SKID,
                                  FY_DATE_SKID DATE_SKID,
                                  BUS_UNIT_SKID,
                                  0 FUND_PROD_SKID,
                                  0 FUND_GRP_SKID,
                                  ACCT_SKID ACCT_PRMTN_SKID,
                                  FY_DATE_SKID
                    FROM OPT_FUND_GEN_SPNDG_FCT a) A;
        -- move parallel hint into sub-query by Leo on Jan 26, 2010 - end
        COMMIT;
        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_FUND_DIM
      WHEN IN_FTBL_NAME = 'OPT_FUND_DIM' THEN

        DELETE /*+ PARALLEL(4) */ FROM OPT_FILTR_LKP WHERE TBL_NAME = 'OPT_FUND_DIM';
        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        -- move parallel hint into sub-query by Leo on Jan 26, 2010 - begin
        INSERT /*+ APPEND PARALLEL(2) */
        INTO OPT_FILTR_LKP
          (TBL_NAME,
           FUND_SKID,
           ACCT_FUND_SKID,
           PROD_SKID,
           PRMTN_SKID,
           DATE_SKID,
           BUS_UNIT_SKID,
           FUND_PROD_SKID,
           FUND_GRP_SKID,
           ACCT_PRMTN_SKID,
           FY_DATE_SKID,
           ETL_RUN_ID)
          SELECT 'OPT_FUND_DIM',
                 FUND_SKID,
                 ACCT_FUND_SKID,
                 PROD_SKID,
                 PRMTN_SKID,
                 DATE_SKID,
                 BUS_UNIT_SKID,
                 FUND_PROD_SKID,
                 FUND_GRP_SKID,
                 ACCT_PRMTN_SKID,
                 FY_DATE_SKID,
                 V_ETL_RUN_ID
            FROM (SELECT /*+ parallel(FDIM, 2) */ DISTINCT FDIM.FUND_SKID     AS FUND_SKID,
                                  FDIM.ACCT_SKID     AS ACCT_FUND_SKID,
                                  0                  PROD_SKID,
                                  0                  PRMTN_SKID,
                                  0                  DATE_SKID,
                                  FDIM.BUS_UNIT_SKID AS BUS_UNIT_SKID,
                                  0                  FUND_PROD_SKID,
                                  ESI.FUND_GRP_SKID  AS FUND_GRP_SKID,
                                  0                  ACCT_PRMTN_SKID,
                                  FDIM.FY_DATE_SKID  AS FY_DATE_SKID
                  -- Join Fund Fdim with Fund ESI to get the real Fund Group SKID for bug fixing
                  -- Simon 10:41 2009-9-28
                  -- Join FDIM and ESI on (FDIM.FUND_SKID = ESI.FUND_SKID), and use ESI.FUND_GRP_SKID instead of ESI.FUND_SKID to keep consistent with Fund Fact
                  -- Simon 18:03 2009-11-30
                    FROM OPT_FUND_FDIM FDIM, OPT_FUND_ESI ESI
                   WHERE FDIM.FUND_SKID = ESI.FUND_SKID) A;
        -- move parallel hint into sub-query by Leo on Jan 26, 2010 - end
        COMMIT;
        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_PYMT_FCT
      WHEN IN_FTBL_NAME = 'OPT_PYMT_FCT' THEN
        --Judge Whether tmp table exists then drop
        SELECT COUNT(1)
          INTO V_NUMBER_TBL_COUNT
          FROM USER_TABLES
         WHERE TABLE_NAME = 'OPT_PROD_GTIN_BRAND_TMP_0';
        IF V_NUMBER_TBL_COUNT = 1 THEN
          G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_0 purge';
          EXECUTE IMMEDIATE G_SQLSTMT;
        END IF;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  ' || CHR(10) ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10) ||' nologging compress ';
        G_SQLSTMT := 'create table OPT_PROD_GTIN_BRAND_TMP_0 ' || CHR(10) ||
                     C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME ||
                     CHR(10) || ' nologging compress ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' as select gt.prod_id,gt.prod_skid,bd.brand_skid,prod_lvl_desc';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from  (select prod_id,prod_skid,brand_id,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc = ''GTIN'' ) gt,';
        G_SQLSTMT := G_SQLSTMT ||
                     ' (select prod_id,prod_skid as brand_skid from opt_prod_fdim ) bd';
        G_SQLSTMT := G_SQLSTMT || ' where gt.brand_id = bd.prod_id';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select prod_id,prod_skid,prod_skid as brand_skid ,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc <> ''GTIN''';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT || ' SELECT NULL,0,0,NULL FROM DUAL';

        EXECUTE IMMEDIATE G_SQLSTMT;

        DELETE FROM OPT_FILTR_LKP WHERE TBL_NAME = 'OPT_PYMT_FCT';
        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        G_SQLSTMT := 'insert /*+ APPEND */ into OPT_FILTR_LKP(TBL_NAME, FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID,ETL_RUN_ID)';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select ''OPT_PYMT_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID, ' ||
                     V_ETL_RUN_ID;
        G_SQLSTMT := G_SQLSTMT || ' from ';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select /*+ parallel(x, 2) */ distinct x.FUND_SKID,x.ACCT_FUND_SKID,y.BRAND_SKID as PROD_SKID, x.PRMTN_SKID,0 DATE_SKID, x.BUS_UNIT_SKID, 0 FUND_PROD_SKID, 0 FUND_GRP_SKID, 0 ACCT_PRMTN_SKID ,0 FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from OPT_PYMT_FCT x,OPT_PROD_GTIN_BRAND_TMP_0 y';
        G_SQLSTMT := G_SQLSTMT || ' where x.PROD_SKID = y.PROD_SKID ';
        G_SQLSTMT := G_SQLSTMT || ' )A';
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end

        EXECUTE IMMEDIATE G_SQLSTMT;
        COMMIT;
        --drop table OPT_PROD_GTIN_BRAND_TMP_0 purge;
        G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_0  purge';
        EXECUTE IMMEDIATE G_SQLSTMT;
        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_FUND_FCT
      WHEN IN_FTBL_NAME = 'OPT_FUND_FCT' THEN

        DELETE /*+ PARALLEL(4) */ FROM OPT_FILTR_LKP WHERE TBL_NAME = 'OPT_FUND_FCT';
        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        INSERT /*+ APPEND PARALLEL(2) */
        INTO OPT_FILTR_LKP
          (TBL_NAME,
           FUND_SKID,
           ACCT_FUND_SKID,
           PROD_SKID,
           PRMTN_SKID,
           DATE_SKID,
           BUS_UNIT_SKID,
           FUND_PROD_SKID,
           FUND_GRP_SKID,
           ACCT_PRMTN_SKID,
           FY_DATE_SKID,
           ETL_RUN_ID)
          SELECT 'OPT_FUND_FCT',
                 FUND_SKID,
                 ACCT_FUND_SKID,
                 PROD_SKID,
                 PRMTN_SKID,
                 DATE_SKID,
                 BUS_UNIT_SKID,
                 FUND_PROD_SKID,
                 FUND_GRP_SKID,
                 ACCT_PRMTN_SKID,
                 FY_DATE_SKID,
                 V_ETL_RUN_ID
            FROM (SELECT /*+ parallel(a, 2) */DISTINCT FUND_SKID,
                                  ACCT_FUND_SKID,
                                  0 PROD_SKID,
                                  0 PRMTN_SKID,
                                  FY_DATE_SKID DATE_SKID,
                                  BUS_UNIT_SKID,
                                  0 FUND_PROD_SKID,
                                  FUND_GRP_SKID,
                                  0 ACCT_PRMTN_SKID,
                                  FY_DATE_SKID
                    FROM OPT_FUND_FCT a) A;
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end
        COMMIT;
        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_TNSFR_FCT
      WHEN IN_FTBL_NAME = 'OPT_TNSFR_FCT' THEN

        SELECT COUNT(1)
          INTO V_NUMBER_TBL_COUNT
          FROM USER_TABLES
         WHERE TABLE_NAME = 'OPT_PROD_GTIN_BRAND_TMP_1';
        IF V_NUMBER_TBL_COUNT = 1 THEN
          G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_1 purge';
          EXECUTE IMMEDIATE G_SQLSTMT;
        END IF;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  ' || CHR(10) ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10) ||' nologging compress ';

        G_SQLSTMT := 'create table OPT_PROD_GTIN_BRAND_TMP_1' || CHR(10) ||
                     C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME ||
                     CHR(10) || ' nologging compress ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' as select gt.prod_id,gt.prod_skid,bd.brand_skid,prod_lvl_desc';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from  (select prod_id,prod_skid,brand_id,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc = ''GTIN'' ) gt,';
        G_SQLSTMT := G_SQLSTMT ||
                     ' (select prod_id,prod_skid as brand_skid from opt_prod_fdim ) bd';
        G_SQLSTMT := G_SQLSTMT || ' where gt.brand_id = bd.prod_id';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select prod_id,prod_skid,prod_skid as brand_skid ,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc <> ''GTIN''';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT || ' SELECT NULL,0,0,NULL FROM DUAL';
        EXECUTE IMMEDIATE G_SQLSTMT;

        DELETE FROM OPT_FILTR_LKP WHERE TBL_NAME = 'OPT_TNSFR_FCT';
        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        G_SQLSTMT := 'insert /*+ APPEND */ into OPT_FILTR_LKP(TBL_NAME, FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID,ETL_RUN_ID)';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select ''OPT_TNSFR_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID, ' ||
                     V_ETL_RUN_ID;
        G_SQLSTMT := G_SQLSTMT || ' from ';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select /*+ parallel(x, 2) */ distinct x.FUND_SKID,x.ACCT_FUND_SKID,y.BRAND_SKID as PROD_SKID,0 PRMTN_SKID,x.FY_DATE_SKID DATE_SKID, x.BUS_UNIT_SKID,0 FUND_PROD_SKID, 0 FUND_GRP_SKID, 0 ACCT_PRMTN_SKID, x.FY_DATE_SKID ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from OPT_TNSFR_FCT x,OPT_PROD_GTIN_BRAND_TMP_1 y';
        G_SQLSTMT := G_SQLSTMT || ' where x.PROD_SKID = y.PROD_SKID ';
        G_SQLSTMT := G_SQLSTMT || ' )A';

        EXECUTE IMMEDIATE G_SQLSTMT;
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end
        COMMIT;
        -- drop table OPT_PROD_GTIN_BRAND_TMP_1 purge;
        G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_1  purge';
        EXECUTE IMMEDIATE G_SQLSTMT;
        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_PRMTN_FCT
      WHEN IN_FTBL_NAME = 'OPT_PRMTN_FCT' THEN

        DELETE FROM OPT_FILTR_LKP WHERE TBL_NAME = 'OPT_PRMTN_FCT';
        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        INSERT /*+ APPEND */
        INTO OPT_FILTR_LKP
          (TBL_NAME,
           FUND_SKID,
           ACCT_FUND_SKID,
           PROD_SKID,
           PRMTN_SKID,
           DATE_SKID,
           BUS_UNIT_SKID,
           FUND_PROD_SKID,
           FUND_GRP_SKID,
           ACCT_PRMTN_SKID,
           FY_DATE_SKID,
           ETL_RUN_ID)
          SELECT 'OPT_PRMTN_FCT',
                 FUND_SKID,
                 ACCT_FUND_SKID,
                 PROD_SKID,
                 PRMTN_SKID,
                 DATE_SKID,
                 BUS_UNIT_SKID,
                 FUND_PROD_SKID,
                 FUND_GRP_SKID,
                 ACCT_PRMTN_SKID,
                 FY_DATE_SKID,
                 V_ETL_RUN_ID
            FROM (SELECT /*+ parallel(a, 2) */DISTINCT 0 FUND_SKID,
                                  0 ACCT_FUND_SKID,
                                  0 PROD_SKID,
                                  BASE_PRMTN_SKID AS PRMTN_SKID,
                                  FY_DATE_SKID DATE_SKID,
                                  BUS_UNIT_SKID,
                                  0 FUND_PROD_SKID,
                                  0 FUND_GRP_SKID,
                                  ACCT_PRMTN_SKID,
                                  FY_DATE_SKID
                    FROM OPT_PRMTN_FCT a) A;
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end
        COMMIT;
        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_PRMTN_PYMT_FCT
      WHEN IN_FTBL_NAME = 'OPT_PRMTN_PYMT_FCT' THEN

        SELECT COUNT(1)
          INTO V_NUMBER_TBL_COUNT
          FROM USER_TABLES
         WHERE TABLE_NAME = 'OPT_PROD_GTIN_BRAND_TMP_2';
        IF V_NUMBER_TBL_COUNT = 1 THEN
          G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_2 purge';
          EXECUTE IMMEDIATE G_SQLSTMT;
        END IF;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  ' || CHR(10) ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10) ||' nologging compress ';

        G_SQLSTMT := 'create table OPT_PROD_GTIN_BRAND_TMP_2' || CHR(10) ||
                     C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME ||
                     CHR(10) || ' nologging compress ';

        G_SQLSTMT := G_SQLSTMT ||
                     ' as select gt.prod_id,gt.prod_skid,bd.brand_skid,prod_lvl_desc';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from  (select prod_id,prod_skid,brand_id,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc = ''GTIN'' ) gt,';
        G_SQLSTMT := G_SQLSTMT ||
                     ' (select prod_id,prod_skid as brand_skid from opt_prod_fdim ) bd';
        G_SQLSTMT := G_SQLSTMT || ' where gt.brand_id = bd.prod_id';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select prod_id,prod_skid,prod_skid as brand_skid ,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc <> ''GTIN''';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT || ' SELECT NULL,0,0,NULL FROM DUAL';
        EXECUTE IMMEDIATE G_SQLSTMT;

        DELETE /*+ PARALLEL(4) */ FROM OPT_FILTR_LKP WHERE TBL_NAME = 'OPT_PRMTN_PYMT_FCT';
        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        G_SQLSTMT := 'insert /*+ APPEND */ into OPT_FILTR_LKP(TBL_NAME, FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID, ACTVY_SKID,FY_DATE_SKID, ETL_RUN_ID)';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select ''OPT_PRMTN_PYMT_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID, ACTVY_SKID,FY_DATE_SKID, ' ||
                     V_ETL_RUN_ID;
        G_SQLSTMT := G_SQLSTMT || ' from ';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select /*+ parallel(x, 2) */ distinct x.FUND_SKID, x.ACCT_FUND_SKID ,y.BRAND_SKID as PROD_SKID,x.PRMTN_SKID,x.FY_DATE_SKID as DATE_SKID,x.BUS_UNIT_SKID,0 FUND_PROD_SKID, 0 FUND_GRP_SKID, x.ACCT_PRMTN_SKID, x.ACTVY_SKID,x.FY_DATE_SKID ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from OPT_PRMTN_PYMT_FCT x,OPT_PROD_GTIN_BRAND_TMP_2 y';
        G_SQLSTMT := G_SQLSTMT || ' where x.PROD_SKID = y.PROD_SKID ';
        G_SQLSTMT := G_SQLSTMT || ' )A';
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end

        EXECUTE IMMEDIATE G_SQLSTMT;
        COMMIT;
        -- drop table OPT_PROD_GTIN_BRAND_TMP_APAC purge;
        G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_2 purge';
        EXECUTE IMMEDIATE G_SQLSTMT;
        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_FUND_ACCRL_FCT
    -- replace OPT_ACCRL_FCT by OPT_INTMD_ACCRL_GTIN_FCT for F063 in R8 by Myra
    -- replace OPT_INTMD_ACCRL_GTIN_FCT by OPT_FUND_ACCRL_FCT for B007 in R10 by David
      WHEN IN_FTBL_NAME = 'OPT_INTMD_ACCRL_GTIN_FCT' THEN

        SELECT COUNT(1)
          INTO V_NUMBER_TBL_COUNT
          FROM USER_TABLES
         WHERE TABLE_NAME = 'OPT_PROD_GTIN_BRAND_TMP_22';
        IF V_NUMBER_TBL_COUNT = 1 THEN
          G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_22 purge';
          EXECUTE IMMEDIATE G_SQLSTMT;
        END IF;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  ' || CHR(10) ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10) ||' nologging compress ';

        G_SQLSTMT := 'create table OPT_PROD_GTIN_BRAND_TMP_22' || CHR(10) ||
                     C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME ||
                     CHR(10) || ' nologging compress ';

        G_SQLSTMT := G_SQLSTMT ||
                     ' as select gt.prod_id,gt.prod_skid,bd.brand_skid,prod_lvl_desc';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from  (select prod_id,prod_skid,brand_id,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc = ''GTIN'' ) gt,';
        G_SQLSTMT := G_SQLSTMT ||
                     ' (select prod_id,prod_skid as brand_skid from opt_prod_fdim ) bd';
        G_SQLSTMT := G_SQLSTMT || ' where gt.brand_id = bd.prod_id';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select prod_id,prod_skid,prod_skid as brand_skid ,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc <> ''GTIN''';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT || ' SELECT NULL,0,0,NULL FROM DUAL';
        EXECUTE IMMEDIATE G_SQLSTMT;

        DELETE /*+ PARALLEL(4) */ FROM OPT_FILTR_LKP WHERE TBL_NAME = 'OPT_FUND_ACCRL_FCT';
        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        /* G_SQLSTMT := 'insert \*+ APPEND PARALLEL(2) *\ into OPT_FILTR_LKP(TBL_NAME, FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID, ETL_RUN_ID)';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select ''OPT_INTMD_ACCRL_GTIN_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID, ' ||
                     V_ETL_RUN_ID;
        G_SQLSTMT := G_SQLSTMT || ' from ';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select  distinct a.FUND_SKID,a.ACCT_FUND_SKID,b.BRAND_SKID as PROD_SKID,0 PRMTN_SKID,a.FY_DATE_SKID as DATE_SKID, a.BUS_UNIT_SKID, a.FUND_PROD_SKID, 0 FUND_GRP_SKID, 0 ACCT_PRMTN_SKID,a.FY_DATE_SKID ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from OPT_FUND_ACCRL_FCT a,OPT_PROD_GTIN_BRAND_TMP_22 b';
        G_SQLSTMT := G_SQLSTMT || ' where a.SRCE_PROD_SKID = b.PROD_SKID ';
        G_SQLSTMT := G_SQLSTMT || ' )A';*/

        -- Added by David for b007 in R10
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        G_SQLSTMT := '
       INSERT /*+ APPEND */
         INTO OPT_FILTR_LKP
           (TBL_NAME,
            FUND_SKID,
            ACCT_FUND_SKID,
            PROD_SKID,
            PRMTN_SKID,
            DATE_SKID,
            BUS_UNIT_SKID,
            FUND_PROD_SKID,
            FUND_GRP_SKID,
            ACCT_PRMTN_SKID,
            FY_DATE_SKID)
           SELECT ''OPT_FUND_ACCRL_FCT'',
                  FUND_SKID,
                  ACCT_SKID,
                  PROD_SKID,
                  PRMTN_SKID,
                  DATE_SKID,
                  BUS_UNIT_SKID,
                  FUND_PROD_SKID,
                  FUND_GRP_SKID,
                  ACCT_PRMTN_SKID,
                  FY_DATE_SKID
             FROM (SELECT /*+ parallel(A, 2) */DISTINCT A.FUND_SKID,
                                   A.ACCT_SKID,
                                   B.BRAND_SKID AS PROD_SKID,
                                   0 PRMTN_SKID,
                                   A.FISC_YR_SKID AS DATE_SKID,
                                   A.BUS_UNIT_SKID,
                                   A.FUND_PROD_SKID,
                                   0 FUND_GRP_SKID,
                                   0 ACCT_PRMTN_SKID,
                                   A.FISC_YR_SKID AS FY_DATE_SKID
                     FROM OPT_FUND_ACCRL_FCT A, OPT_PROD_GTIN_BRAND_TMP_22 B
                    WHERE A.SRCE_PROD_SKID = B.PROD_SKID)';
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end

        EXECUTE IMMEDIATE G_SQLSTMT;

        COMMIT;
        G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_22 purge';
        EXECUTE IMMEDIATE G_SQLSTMT;
        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_ACTVY_FCT
      WHEN IN_FTBL_NAME = 'OPT_ACTVY_FCT' THEN

        DELETE /*+ PARALLEL(4) */ FROM OPT_FILTR_LKP WHERE TBL_NAME = 'OPT_ACTVY_FCT';
        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        INSERT /*+ APPEND */
        INTO OPT_FILTR_LKP
          (TBL_NAME,
           FUND_SKID,
           ACCT_FUND_SKID,
           PROD_SKID,
           PRMTN_SKID,
           DATE_SKID,
           BUS_UNIT_SKID,
           FUND_PROD_SKID,
           FUND_GRP_SKID,
           ACCT_PRMTN_SKID,
           ACTVY_SKID,
           FY_DATE_SKID,
           ETL_RUN_ID)
          SELECT /*+ parallel(a, 2) */ 'OPT_ACTVY_FCT',
                 FUND_SKID,
                 ACCT_FUND_SKID,
                 0 AS PROD_SKID,
                 PRMTN_SKID,
                 FY_DATE_SKID DATE_SKID,
                 BUS_UNIT_SKID,
                 0 AS FUND_PROD_SKID,
                 0 AS FUND_GRP_SKID,
                 ACCT_PRMTN_SKID,
                 ACTVY_SKID,
                 FY_DATE_SKID,
                 V_ETL_RUN_ID
            FROM OPT_ACTVY_FCT a;
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end
        COMMIT;
        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_PRMTN_PROD_FCT
      WHEN IN_FTBL_NAME = 'OPT_PRMTN_PROD_FCT' THEN

        SELECT COUNT(1)
          INTO V_NUMBER_TBL_COUNT
          FROM USER_TABLES
         WHERE TABLE_NAME = 'OPT_PROD_GTIN_BRAND_TMP_3';
        IF V_NUMBER_TBL_COUNT = 1 THEN
          G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_3 purge';
          EXECUTE IMMEDIATE G_SQLSTMT;
        END IF;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  ' || CHR(10) ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10) ||' nologging compress ';

        G_SQLSTMT := 'create table OPT_PROD_GTIN_BRAND_TMP_3 ' || CHR(10) ||
                     C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME ||
                     CHR(10) || ' nologging compress ';

        G_SQLSTMT := G_SQLSTMT ||
                     ' as select gt.prod_id,gt.prod_skid,bd.brand_skid,prod_lvl_desc';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from  (select prod_id,prod_skid,brand_id,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc = ''GTIN'' ) gt,';
        G_SQLSTMT := G_SQLSTMT ||
                     ' (select prod_id,prod_skid as brand_skid from opt_prod_fdim ) bd';
        G_SQLSTMT := G_SQLSTMT || ' where gt.brand_id = bd.prod_id';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select prod_id,prod_skid,prod_skid as brand_skid ,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc <> ''GTIN''';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT || ' SELECT NULL,0,0,NULL FROM DUAL';
        EXECUTE IMMEDIATE G_SQLSTMT;

        --Create index on PROD_SKID from tmp table
        FOR X IN (SELECT 'CREATE  INDEX ' || SUBSTR(TABLE_NAME, 1, 24) ||
                         '_IDX' || TO_CHAR(COLUMN_ID, 'fm00') || CHR(10) ||
                         'ON ' || TABLE_NAME || '(' || COLUMN_NAME || ')' ||
                         CHR(10) || 'NOLOGGING' || CHR(10) || 'PARALLEL 1' ||
                         --CHR(10) || 'TABLESPACE optima01' || CHR(10) || CASE
                         CHR(10) || 'TABLESPACE '|| V_INDEX || CHR(10) || CASE
                           WHEN EXISTS
                            (SELECT 'x'
                                   FROM USER_TAB_PARTITIONS
                                  WHERE TABLE_NAME = DTC.TABLE_NAME) THEN
                            'LOCAL'
                         END AS CREATE_INDEX_STMT
                    FROM USER_TAB_COLUMNS DTC
                   WHERE TABLE_NAME IN ('OPT_PROD_GTIN_BRAND_TMP_3')
                     AND COLUMN_NAME = 'PROD_SKID') LOOP
          EXECUTE IMMEDIATE X.CREATE_INDEX_STMT;
        END LOOP;

        DELETE /*+ PARALLEL(4) */ FROM OPT_FILTR_LKP WHERE TBL_NAME = 'OPT_PRMTN_PROD_FCT';
        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        G_SQLSTMT := 'insert /*+ APPEND */  into OPT_FILTR_LKP(TBL_NAME, FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID,ETL_RUN_ID)';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select ''OPT_PRMTN_PROD_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID, ' ||
                     V_ETL_RUN_ID;
        G_SQLSTMT := G_SQLSTMT || ' from ';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select /*+ parallel(x, 2) */ distinct 0 FUND_SKID,0 ACCT_FUND_SKID,y.BRAND_SKID as PROD_SKID,x.PRMTN_SKID,x.FY_DATE_SKID DATE_SKID,x.BUS_UNIT_SKID,0 FUND_PROD_SKID, 0 FUND_GRP_SKID, x.ACCT_PRMTN_SKID,x.FY_DATE_SKID ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from OPT_PRMTN_PROD_FCT x,OPT_PROD_GTIN_BRAND_TMP_3 y';
        G_SQLSTMT := G_SQLSTMT || ' where x.PROD_SKID = y.PROD_SKID ';
        G_SQLSTMT := G_SQLSTMT || ' )A';
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end
        EXECUTE IMMEDIATE G_SQLSTMT;
        COMMIT;
        --  drop table OPT_PROD_GTIN_BRAND_TMP_APAC purge;
        G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_3 purge';
        EXECUTE IMMEDIATE G_SQLSTMT;
        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_ACTVY_PROD_FCT
      WHEN IN_FTBL_NAME = 'OPT_ACTVY_PROD_FCT' THEN

        SELECT COUNT(1)
          INTO V_NUMBER_TBL_COUNT
          FROM USER_TABLES
         WHERE TABLE_NAME = 'OPT_PROD_GTIN_BRAND_TMP_4';
        IF V_NUMBER_TBL_COUNT = 1 THEN
          G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_4 purge';
           DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Dropping of Table started');
           DBMS_OUTPUT.PUT_LINE(G_SQLSTMT);
           DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Dropping of Table ended');
          EXECUTE IMMEDIATE G_SQLSTMT;
        END IF;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  ' || CHR(10) ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10) ||' nologging compress ';

        G_SQLSTMT := 'create table OPT_PROD_GTIN_BRAND_TMP_4 ' || CHR(10) ||
                     C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME ||
                     CHR(10) || ' nologging compress ';

        G_SQLSTMT := G_SQLSTMT ||
                     ' as select gt.prod_id,gt.prod_skid,bd.brand_skid,prod_lvl_desc';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from  (select prod_id,prod_skid,brand_id,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc = ''GTIN'' ) gt,';
        G_SQLSTMT := G_SQLSTMT ||
                     ' (select prod_id,prod_skid as brand_skid from opt_prod_fdim ) bd';
        G_SQLSTMT := G_SQLSTMT || ' where gt.brand_id = bd.prod_id';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select prod_id,prod_skid,prod_skid as brand_skid ,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc <> ''GTIN''';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT || ' SELECT NULL,0,0,NULL FROM DUAL';
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Table creation started');
        DBMS_OUTPUT.PUT_LINE(G_SQLSTMT);
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Table creation ended');
        EXECUTE IMMEDIATE G_SQLSTMT;

        DELETE /*+ PARALLEL(4) */ FROM OPT_FILTR_LKP WHERE TBL_NAME = 'OPT_ACTVY_PROD_FCT';
        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        G_SQLSTMT := 'insert /*+ APPEND */ into OPT_FILTR_LKP(TBL_NAME, FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,ACTVY_SKID,FY_DATE_SKID,ETL_RUN_ID)';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select ''OPT_ACTVY_PROD_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID, ACTVY_SKID,FY_DATE_SKID,' ||
                     V_ETL_RUN_ID;
        G_SQLSTMT := G_SQLSTMT || ' from ';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' SELECT /*+parallel(x,2) */ distinct x.FUND_SKID,0 ACCT_FUND_SKID,y.BRAND_SKID as PROD_SKID,x.PRMTN_SKID,x.FY_DATE_SKID DATE_SKID,x.BUS_UNIT_SKID,0 FUND_PROD_SKID , 0 FUND_GRP_SKID, x.ACCT_PRMTN_SKID, x.ACTVY_SKID,x.FY_DATE_SKID ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from OPT_ACTVY_PROD_FCT x,OPT_PROD_GTIN_BRAND_TMP_4 y';
        G_SQLSTMT := G_SQLSTMT || ' where x.PROD_SKID = y.PROD_SKID ';
        G_SQLSTMT := G_SQLSTMT || ' )A';
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end
         DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Insert Table OPT_FILTR_LKP started');
         DBMS_OUTPUT.PUT_LINE(G_SQLSTMT);
         DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Insert Table OPT_FILTR_LKP ended');
         EXECUTE IMMEDIATE G_SQLSTMT;
         DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') || ' : Inserted Records :'|| SQL%ROWCOUNT||' records'); 

        COMMIT;
        -- drop table OPT_PROD_GTIN_BRAND_TMP_APAC purge;
        G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_4 purge';
        EXECUTE IMMEDIATE G_SQLSTMT;
        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_COMB_PRMTN_FCT
      WHEN IN_FTBL_NAME = 'OPT_COMB_PRMTN_FCT' THEN

        DELETE /*+ PARALLEL(4) */ FROM OPT_FILTR_LKP WHERE TBL_NAME = 'OPT_COMB_PRMTN_FCT';
        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        --remove the parallel hint by leo on Jan 26, 2011 - begin
        INSERT /*+ APPEND */
        INTO OPT_FILTR_LKP
          (TBL_NAME,
           FUND_SKID,
           ACCT_FUND_SKID,
           PROD_SKID,
           PRMTN_SKID,
           DATE_SKID,
           BUS_UNIT_SKID,
           FUND_PROD_SKID,
           FUND_GRP_SKID,
           ACCT_PRMTN_SKID,
           COMB_PRMTN_DIM_SKID,
           FY_DATE_SKID,
           ETL_RUN_ID)
          SELECT 'OPT_COMB_PRMTN_FCT',
                 FUND_SKID,
                 ACCT_FUND_SKID,
                 PROD_SKID,
                 PRMTN_SKID,
                 DATE_SKID,
                 BUS_UNIT_SKID,
                 FUND_PROD_SKID,
                 FUND_GRP_SKID,
                 ACCT_PRMTN_SKID,
                 COMB_PRMTN_DIM_SKID,
                 FY_DATE_SKID,
                 V_ETL_RUN_ID
            FROM (SELECT DISTINCT 0 FUND_SKID,
                                  0 ACCT_FUND_SKID,
                                  PROD_SKID,
                                  0 PRMTN_SKID,
                                  FY_DATE_SKID DATE_SKID,
                                  BUS_UNIT_SKID,
                                  0 FUND_PROD_SKID,
                                  0 FUND_GRP_SKID,
                                  ACCT_SKID AS ACCT_PRMTN_SKID,
                                  COMB_PRMTN_DIM_SKID,
                                  FY_DATE_SKID
                    FROM (SELECT COMB_PRMTN_FCT.COMB_PRMTN_DIM_SKID,
                                 COMB_PRMTN_FCT.BUS_UNIT_SKID,
                                 COMB_PRMTN_DIM.ACCT_SKID,
                                 COMB_PRMTN_FCT.DATE_SKID,
                                 PROD_HIER.PROD_5_SKID AS PROD_SKID,
                                 PRMTN_PROD.FY_DATE_SKID
                            FROM OPT_COMB_PRMTN_DIM COMB_PRMTN_DIM,
                                 OPT_COMB_PRMTN_FCT COMB_PRMTN_FCT,
                                 OPT_PRMTN_FDIM     PRMTN,
                                 OPT_PRMTN_PROD_FCT PRMTN_PROD,
                                 OPT_PROD_HIER_FDIM PROD_HIER
                           WHERE COMB_PRMTN_FCT.COMB_PRMTN_DIM_SKID =
                                 COMB_PRMTN_DIM.COMB_PRMTN_DIM_SKID
                             AND PRMTN.PRMTN_ID = COMB_PRMTN_FCT.PRMTN_1_ID
                             AND COMB_PRMTN_FCT.PRMTN_1_ID <> '0'
                             AND PRMTN_PROD.PRMTN_SKID = PRMTN.PRMTN_SKID
                             AND PROD_HIER.PROD_SKID = PRMTN_PROD.PROD_SKID
                          UNION
                          SELECT COMB_PRMTN_FCT.COMB_PRMTN_DIM_SKID,
                                 COMB_PRMTN_FCT.BUS_UNIT_SKID,
                                 COMB_PRMTN_DIM.ACCT_SKID,
                                 COMB_PRMTN_FCT.DATE_SKID,
                                 PROD_HIER.PROD_5_SKID AS PROD_SKID,
                                 PRMTN_PROD.FY_DATE_SKID
                            FROM OPT_COMB_PRMTN_DIM COMB_PRMTN_DIM,
                                 OPT_COMB_PRMTN_FCT COMB_PRMTN_FCT,
                                 OPT_PRMTN_FDIM     PRMTN,
                                 OPT_PRMTN_PROD_FCT PRMTN_PROD,
                                 OPT_PROD_HIER_FDIM PROD_HIER
                           WHERE COMB_PRMTN_FCT.COMB_PRMTN_DIM_SKID =
                                 COMB_PRMTN_DIM.COMB_PRMTN_DIM_SKID
                             AND PRMTN.PRMTN_ID = COMB_PRMTN_FCT.PRMTN_2_ID
                             AND COMB_PRMTN_FCT.PRMTN_2_ID <> '0'
                             AND PRMTN_PROD.PRMTN_SKID = PRMTN.PRMTN_SKID
                             AND PROD_HIER.PROD_SKID = PRMTN_PROD.PROD_SKID));
        --remove the parallel hint by leo on Jan 26, 2011 - begin
        COMMIT;
        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --sourcing data from OPT_BRAND_BASLN_IFCT
      WHEN IN_FTBL_NAME = 'OPT_BRAND_BASLN_IFCT' THEN

        SQL_MSG := 'Deleting data of OPT_FILTR_LKP sourced from OPT_BRAND_BASLN_IFCT';
        DELETE /*+ PARALLEL(4) */ FROM OPT_FILTR_LKP WHERE TBL_NAME = 'OPT_BRAND_BASLN_IFCT';
        COMMIT;

        SQL_MSG := 'Inserting data of OPT_FILTR_LKP sourced from OPT_BRAND_BASLN_IFCT';
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        INSERT /*+ APPEND PARALLEL(2) */
        INTO OPT_FILTR_LKP
          (TBL_NAME,
           FUND_SKID,
           ACCT_FUND_SKID,
           PROD_SKID,
           PRMTN_SKID,
           DATE_SKID,
           BUS_UNIT_SKID,
           FUND_PROD_SKID,
           FUND_GRP_SKID,
           ACCT_PRMTN_SKID,
           FY_DATE_SKID,
           COMB_PRMTN_DIM_SKID,
           ACTVY_SKID,
           ETL_RUN_ID)
          SELECT 'OPT_BRAND_BASLN_IFCT',
                 FUND_SKID,
                 ACCT_FUND_SKID,
                 PROD_SKID,
                 PRMTN_SKID,
                 DATE_SKID,
                 BUS_UNIT_SKID,
                 FUND_PROD_SKID,
                 FUND_GRP_SKID,
                 ACCT_PRMTN_SKID,
                 FY_DATE_SKID,
                 COMB_PRMTN_DIM_SKID,
                 ACTVY_SKID,
                 V_ETL_RUN_ID
            FROM (SELECT /*+ parallel(a, 2) */ DISTINCT BUS_UNIT_SKID,
                                  0 AS FUND_SKID,
                                  0 AS ACCT_FUND_SKID,
                                  PRMTN_ACCT_SKID AS ACCT_PRMTN_SKID,
                                  PROD_SKID,
                                  FY_DATE_SKID,
                                  DUMMY_PRMTN_SKID AS PRMTN_SKID,
                                  FY_DATE_SKID DATE_SKID,
                                  0 AS FUND_PROD_SKID,
                                  0 AS FUND_GRP_SKID,
                                  0 AS COMB_PRMTN_DIM_SKID,
                                  0 AS ACTVY_SKID,
                                  0 V_ETL_RUN_ID
                    FROM OPT_BRAND_BASLN_IFCT a) A;
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end
        COMMIT;

        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --Added by Asca on 12/2/2010 for B018 of R11 Begin
      WHEN IN_FTBL_NAME = 'OPT_TSP_LVL_BASLN_IFCT' THEN

        SQL_MSG := 'Deleting data of OPT_FILTR_LKP sourced from OPT_TSP_LVL_BASLN_IFCT';
        DELETE /*+ PARALLEL(4) */ FROM OPT_FILTR_LKP
         WHERE TBL_NAME = 'OPT_TSP_LVL_BASLN_IFCT';
        COMMIT;

        SQL_MSG := 'Inserting data of OPT_FILTR_LKP sourced from OPT_TSP_LVL_BASLN_IFCT';
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        INSERT /*+ APPEND */
        INTO OPT_FILTR_LKP
          (TBL_NAME,
           FUND_SKID,
           ACCT_FUND_SKID,
           PROD_SKID,
           PRMTN_SKID,
           DATE_SKID,
           BUS_UNIT_SKID,
           FUND_PROD_SKID,
           FUND_GRP_SKID,
           ACCT_PRMTN_SKID,
           FY_DATE_SKID,
           COMB_PRMTN_DIM_SKID,
           ACTVY_SKID,
           ETL_RUN_ID)
          SELECT 'OPT_TSP_LVL_BASLN_IFCT',
                 FUND_SKID,
                 ACCT_FUND_SKID,
                 PROD_SKID,
                 PRMTN_SKID,
                 DATE_SKID,
                 BUS_UNIT_SKID,
                 FUND_PROD_SKID,
                 FUND_GRP_SKID,
                 ACCT_PRMTN_SKID,
                 FY_DATE_SKID,
                 COMB_PRMTN_DIM_SKID,
                 ACTVY_SKID,
                 V_ETL_RUN_ID
            FROM (SELECT /*+ parallel(a,2) */ DISTINCT BUS_UNIT_SKID,
                                  0 AS FUND_SKID,
                                  0 AS ACCT_FUND_SKID,
                                  PRMTN_ACCT_SKID AS ACCT_PRMTN_SKID,
                                  PROD_SKID,
                                  FY_DATE_SKID,
                                  DUMMY_PRMTN_SKID AS PRMTN_SKID,
                                  FY_DATE_SKID AS DATE_SKID,
                                  0 AS FUND_PROD_SKID,
                                  0 AS FUND_GRP_SKID,
                                  0 AS COMB_PRMTN_DIM_SKID,
                                  0 AS ACTVY_SKID,
                                  0 V_ETL_RUN_ID
                    FROM OPT_TSP_LVL_BASLN_IFCT a) A;
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end
        COMMIT;

        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';
        --Added by Asca on 12/2/2010 for B018 of R11 End

    END CASE;

  END IF;

  --------------------------------------------------------------------------------------------------------------------------
  -- STEP 020: Daily incremental refresh table OPT_FILTR_LKP.
  --------------------------------------------------------------------------------------------------------------------------
  IF V_INIT_FLAG = 'N' THEN

    CASE
    --This is for F030
    --OPT_SHPMT_ACTL_FRCST_FCT
    --Need to disable data loading from one number report fact
      WHEN IN_FTBL_NAME = 'OPT_SHPMT_ACTL_FRCST_FCT' THEN
        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';
        /*
        WHEN IN_FTBL_NAME='OPT_SHPMT_ACTL_FRCST_FCT ' THEN
            DELETE FROM OPT_FILTR_LKP WHERE TBL_NAME = 'OPT_SHPMT_ACTL_FRCST_FCT ' ;
            COMMIT;
            INSERT INTO OPT_FILTR_LKP
              (TBL_NAME,
               FUND_SKID,
               ACCT_FUND_SKID,
               PROD_SKID,
               PRMTN_SKID,
               DATE_SKID,
               BUS_UNIT_SKID,
               FUND_PROD_SKID,
               FUND_GRP_SKID,
               ACCT_PRMTN_SKID,
               FY_DATE_SKID,
               ETL_RUN_ID)
               SELECT 'OPT_SHPMT_ACTL_FRCST_FCT',
                     FUND_SKID,
                     ACCT_FUND_SKID,
                     PROD_SKID,
                     PRMTN_SKID,
                     DATE_SKID,
                     BUS_UNIT_SKID,
                     FUND_PROD_SKID,
                     FUND_GRP_SKID,
                     ACCT_PRMTN_SKID,
                     FY_DATE_SKID,
                     V_ETL_RUN_ID
                 FROM (SELECT DISTINCT 0 FUND_SKID,
                                      0 ACCT_FUND_SKID,
                                      PROD_SKID,
                                      0 PRMTN_SKID,
                                      FY_DATE_SKID DATE_SKID,
                                      BUS_UNIT_SKID,
                                      0 FUND_PROD_SKID,
                                      0 FUND_GRP_SKID,
                                      ACCT_PRMTN_SKID,
                                      FY_DATE_SKID
                        FROM OPT_SHPMT_ACTL_FRCST_FCT) A;
            COMMIT;
            R_RTN_VALUE := 0;
            R_RTN_MSG   := 'Success....';
            */
    --OPT_FUND_DIM
      WHEN IN_FTBL_NAME = 'OPT_FUND_DIM' THEN

        DELETE /*+ PARALLEL(4) */ FROM OPT_FILTR_LKP A
         WHERE EXISTS (SELECT 'x'
                  FROM (SELECT FUND_SKID,
                               ACCT_FUND_SKID,
                               PROD_SKID,
                               PRMTN_SKID,
                               DATE_SKID,
                               BUS_UNIT_SKID,
                               FUND_PROD_SKID,
                               FUND_GRP_SKID,
                               ACCT_PRMTN_SKID,
                               FY_DATE_SKID
                          FROM (SELECT FUND_SKID,
                                       ACCT_FUND_SKID,
                                       PROD_SKID,
                                       PRMTN_SKID,
                                       0 DATE_SKID,
                                       BUS_UNIT_SKID,
                                       FUND_PROD_SKID,
                                       FUND_GRP_SKID,
                                       ACCT_PRMTN_SKID,
                                       FY_DATE_SKID
                                  FROM OPT_FILTR_LKP
                                 WHERE TBL_NAME = 'OPT_FUND_DIM'
                                MINUS
                                SELECT DISTINCT FUND_SKID,
                                                ACCT_SKID AS ACCT_FUND_SKID,
                                                0 PROD_SKID,
                                                0 PRMTN_SKID,
                                                0 DATE_SKID,
                                                BUS_UNIT_SKID,
                                                0 FUND_PROD_SKID,
                                                FUND_GRP_SKID,
                                                0 ACCT_PRMTN_SKID,
                                                FY_DATE_SKID
                                  FROM OPT_FUND_FDIM
                                /*where REGN_CODE = in_Regn*/
                                )) B
                 WHERE A.FUND_SKID = B.FUND_SKID
                   AND A.ACCT_FUND_SKID = B.ACCT_FUND_SKID
                   AND A.BUS_UNIT_SKID = B.BUS_UNIT_SKID
                   AND A.FUND_GRP_SKID = B.FUND_GRP_SKID)
           AND A.TBL_NAME = 'OPT_FUND_DIM';

        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        INSERT /*+ APPEND */
        INTO OPT_FILTR_LKP
          (TBL_NAME,
           FUND_SKID,
           ACCT_FUND_SKID,
           PROD_SKID,
           PRMTN_SKID,
           DATE_SKID,
           BUS_UNIT_SKID,
           FUND_PROD_SKID,
           FUND_GRP_SKID,
           ACCT_PRMTN_SKID,
           FY_DATE_SKID,
           ETL_RUN_ID)
          SELECT 'OPT_FUND_DIM',
                 FUND_SKID,
                 ACCT_FUND_SKID,
                 PROD_SKID,
                 PRMTN_SKID,
                 DATE_SKID,
                 BUS_UNIT_SKID,
                 FUND_PROD_SKID,
                 FUND_GRP_SKID,
                 ACCT_PRMTN_SKID,
                 FY_DATE_SKID,
                 V_ETL_RUN_ID
            FROM (SELECT /* parallel(a,2) */ DISTINCT FUND_SKID,
                                  ACCT_SKID AS ACCT_FUND_SKID,
                                  0 PROD_SKID,
                                  0 PRMTN_SKID,
                                  0 DATE_SKID,
                                  BUS_UNIT_SKID,
                                  0 FUND_PROD_SKID,
                                  FUND_GRP_SKID,
                                  0 ACCT_PRMTN_SKID,
                                  FY_DATE_SKID
                    FROM OPT_FUND_FDIM
                  --WHERE REGN_CODE = IN_REGN
                  MINUS
                  SELECT FUND_SKID,
                         ACCT_FUND_SKID,
                         PROD_SKID,
                         PRMTN_SKID,
                         DATE_SKID,
                         BUS_UNIT_SKID,
                         FUND_PROD_SKID,
                         FUND_GRP_SKID,
                         ACCT_PRMTN_SKID,
                         FY_DATE_SKID
                    FROM OPT_FILTR_LKP
                   WHERE TBL_NAME = 'OPT_FUND_DIM') A;
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end
        COMMIT;

        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_PYMT_FCT
      WHEN IN_FTBL_NAME = 'OPT_PYMT_FCT' THEN

        G_SQLSTMT := 'select count(1) from user_tables where table_name = ''OPT_PROD_GTIN_BRAND_TMP_0' ||
                     IN_REGN || '''';
        EXECUTE IMMEDIATE G_SQLSTMT
          INTO V_NUMBER_TBL_COUNT;
        IF V_NUMBER_TBL_COUNT = 1 THEN
          G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_0' || IN_REGN ||
                       ' purge';
          EXECUTE IMMEDIATE G_SQLSTMT;
        END IF;

        G_SQLSTMT := 'select count(1) from user_tables where table_name = ''FLTR_LKP_PYMT_TMP_' ||
                     IN_REGN || '''';
        EXECUTE IMMEDIATE G_SQLSTMT
          INTO V_NUMBER_TBL_COUNT;
        IF V_NUMBER_TBL_COUNT = 1 THEN
          G_SQLSTMT := 'drop table FLTR_LKP_PYMT_TMP_' || IN_REGN ||
                       ' purge';
          EXECUTE IMMEDIATE G_SQLSTMT;
        END IF;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  ' || CHR(10) ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10) ||' nologging compress ';

        G_SQLSTMT := 'create table OPT_PROD_GTIN_BRAND_TMP_0' || IN_REGN || ' ' ||
                     CHR(10) || C_FINAL_TBL_STORAGE || ' ' ||
                     V_TBLSPACE_NAME || CHR(10) || ' nologging compress ';

        G_SQLSTMT := G_SQLSTMT ||
                     ' as select gt.prod_id,gt.prod_skid,bd.brand_skid,prod_lvl_desc';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from  (select prod_id,prod_skid,brand_id,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc = ''GTIN'' /*and regn_code = ''' ||
                     IN_REGN || '''*/) gt,';
        G_SQLSTMT := G_SQLSTMT ||
                     ' (select prod_id,prod_skid as brand_skid from opt_prod_fdim /*where regn_code = ''' ||
                     IN_REGN || '''*/) bd';
        G_SQLSTMT := G_SQLSTMT || ' where gt.brand_id = bd.prod_id';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select prod_id,prod_skid,prod_skid as brand_skid ,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc <> ''GTIN'' /*and regn_code = ''' ||
                     IN_REGN || '''*/';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT || ' SELECT NULL,0,0,NULL FROM DUAL';

        EXECUTE IMMEDIATE G_SQLSTMT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  ' || CHR(10) ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10) ||' nologging compress ';

        G_SQLSTMT := 'create table FLTR_LKP_PYMT_TMP_' || IN_REGN || ' ' ||
                     CHR(10) || C_FINAL_TBL_STORAGE || ' ' ||
                     V_TBLSPACE_NAME || CHR(10) || ' nologging compress ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' as select  FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, 0 FUND_PROD_SKID, 0 FUND_GRP_SKID, 0 ACCT_PRMTN_SKID,0 FY_DATE_SKID ,';
        G_SQLSTMT := G_SQLSTMT || ' sum(ETL_MGRT_CODE) ETL_MGRT_CODE';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from OPT_PYMT_SFCT /*where REGN_CODE = ''' ||
                     IN_REGN || '''*/';
        G_SQLSTMT := G_SQLSTMT ||
                     ' group by FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID';
        G_SQLSTMT := G_SQLSTMT || ' having sum(ETL_MGRT_CODE) <=2';

        EXECUTE IMMEDIATE G_SQLSTMT;

        G_SQLSTMT := 'delete from OPT_FILTR_LKP A';
        G_SQLSTMT := G_SQLSTMT || ' where exists';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT || ' select ''x'' from ';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select ''OPT_PYMT_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' from(';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select  distinct FUND_SKID,ACCT_FUND_SKID,y.BRAND_SKID as PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, 0 FUND_PROD_SKID, 0 FUND_GRP_SKID, 0 ACCT_PRMTN_SKID ,FY_DATE_SKID,ETL_MGRT_CODE ';
        G_SQLSTMT := G_SQLSTMT || ' from FLTR_LKP_PYMT_TMP_' || IN_REGN ||
                     ' x,OPT_PROD_GTIN_BRAND_TMP_0' || IN_REGN || ' y';
        G_SQLSTMT := G_SQLSTMT || ' where x.PROD_SKID = y.PROD_SKID ';
        G_SQLSTMT := G_SQLSTMT || ' and x.ETL_MGRT_CODE = 2 ';
        G_SQLSTMT := G_SQLSTMT || ' minus';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select  distinct x.FUND_SKID,x.ACCT_FUND_SKID,z.BRAND_SKID as PROD_SKID,x.PRMTN_SKID,x.DATE_SKID, x.BUS_UNIT_SKID, 0 FUND_PROD_SKID, 0 FUND_GRP_SKID, 0 ACCT_PRMTN_SKID ,FY_DATE_SKID,2 ';
        G_SQLSTMT := G_SQLSTMT || ' from OPT_PYMT_FCT x,FLTR_LKP_PYMT_TMP_' ||
                     IN_REGN || ' y, OPT_PROD_GTIN_BRAND_TMP_0' || IN_REGN ||
                     ' z ';
        G_SQLSTMT := G_SQLSTMT || ' where x.FUND_SKID = y.FUND_SKID ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and   x.ACCT_FUND_SKID = y.ACCT_FUND_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and   x.PROD_SKID  = y.PROD_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and   x.PRMTN_SKID = y.PRMTN_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and   x.DATE_SKID = y.DATE_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and   x.BUS_UNIT_SKID = y.BUS_UNIT_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and   x.PROD_SKID = z.PROD_SKID';
        G_SQLSTMT := G_SQLSTMT || ' /*and x.REGN_CODE = ''' || IN_REGN ||
                     '''*/';
        G_SQLSTMT := G_SQLSTMT || ' and y.ETL_MGRT_CODE = 2)';
        G_SQLSTMT := G_SQLSTMT || ' )B ';
        G_SQLSTMT := G_SQLSTMT || ' where A.FUND_SKID = B.FUND_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and A.ACCT_FUND_SKID = B.ACCT_FUND_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.PROD_SKID = B.PROD_SKID ';
        G_SQLSTMT := G_SQLSTMT || ' and A.PRMTN_SKID = B.PRMTN_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.DATE_SKID = B.DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.BUS_UNIT_SKID = B.BUS_UNIT_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and A.FUND_PROD_SKID = B.FUND_PROD_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.FUND_GRP_SKID = B.FUND_GRP_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and A.ACCT_PRMTN_SKID = B.ACCT_PRMTN_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.FY_DATE_SKID = B.FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' ) ';
        G_SQLSTMT := G_SQLSTMT || ' and A.TBL_NAME = ''OPT_PYMT_FCT''';

        EXECUTE IMMEDIATE G_SQLSTMT;

        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        G_SQLSTMT := 'insert /*+ APPEND */ into OPT_FILTR_LKP(TBL_NAME, FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID,ETL_RUN_ID)';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select ''OPT_PYMT_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID,' ||
                     V_ETL_RUN_ID;
        G_SQLSTMT := G_SQLSTMT || ' from ';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select /*+ parallel(x,2) parallel(y, 2) */ distinct FUND_SKID,ACCT_FUND_SKID,y.BRAND_SKID as PROD_SKID,PRMTN_SKID, DATE_SKID, BUS_UNIT_SKID, 0 FUND_PROD_SKID, 0 FUND_GRP_SKID, 0 ACCT_PRMTN_SKID,FY_DATE_SKID ,ETL_MGRT_CODE ';
        G_SQLSTMT := G_SQLSTMT || ' from FLTR_LKP_PYMT_TMP_' || IN_REGN ||
                     ' x,OPT_PROD_GTIN_BRAND_TMP_0' || IN_REGN || ' y ';
        G_SQLSTMT := G_SQLSTMT || ' where x.PROD_SKID = y.PROD_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and x.ETL_MGRT_CODE = 1';
        G_SQLSTMT := G_SQLSTMT || ' minus';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select /*+ parallel(a, 2) */ FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID, ACCT_PRMTN_SKID, FY_DATE_SKID,1 from OPT_FILTR_LKP a where TBL_NAME = ''OPT_PYMT_FCT''';
        G_SQLSTMT := G_SQLSTMT || ' )A';
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end

        EXECUTE IMMEDIATE G_SQLSTMT;

        COMMIT;

        --    drop table FLTR_LKP_PYMT_TMP_APAC purge;
        G_SQLSTMT := 'drop table FLTR_LKP_PYMT_TMP_' || IN_REGN || ' purge';
        EXECUTE IMMEDIATE G_SQLSTMT;
        --   drop table OPT_PROD_GTIN_BRAND_TMP_APAC purge;
        G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_0' || IN_REGN ||
                     ' purge';
        EXECUTE IMMEDIATE G_SQLSTMT;

        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_FUND_FCT
      WHEN IN_FTBL_NAME = 'OPT_FUND_FCT' THEN

        G_SQLSTMT := 'select count(1) from user_tables where table_name = ''FLTR_LKP_FUND_TMP_' ||
                     IN_REGN || '''';
        EXECUTE IMMEDIATE G_SQLSTMT
          INTO V_NUMBER_TBL_COUNT;
        IF V_NUMBER_TBL_COUNT = 1 THEN
          G_SQLSTMT := 'drop table FLTR_LKP_FUND_TMP_' || IN_REGN ||
                       ' purge';
          EXECUTE IMMEDIATE G_SQLSTMT;
        END IF;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  ' || CHR(10) ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10) ||' nologging compress ';

        G_SQLSTMT := 'create table FLTR_LKP_FUND_TMP_' || IN_REGN || ' ' ||
                     CHR(10) || C_FINAL_TBL_STORAGE || ' ' ||
                     V_TBLSPACE_NAME || CHR(10) || ' nologging compress ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' as select  FUND_SKID, ACCT_FUND_SKID, 0 PROD_SKID, 0 PRMTN_SKID,  BUS_UNIT_SKID, 0 FUND_PROD_SKID,  FUND_GRP_SKID, 0 ACCT_PRMTN_SKID,FY_DATE_SKID ,sum(ETL_MGRT_CODE) ETL_MGRT_CODE';
        G_SQLSTMT := G_SQLSTMT || ' from OPT_FUND_FCT';
        G_SQLSTMT := G_SQLSTMT || ' /*where REGN_CODE = ''' || IN_REGN ||
                     '''*/';
        G_SQLSTMT := G_SQLSTMT ||
                     ' group by FUND_SKID, ACCT_FUND_SKID,  BUS_UNIT_SKID ,FUND_GRP_SKID,FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' having sum(ETL_MGRT_CODE) <= 2';

        EXECUTE IMMEDIATE G_SQLSTMT;

        G_SQLSTMT := 'delete from OPT_FILTR_LKP A';
        G_SQLSTMT := G_SQLSTMT || ' where exists';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT || ' select ''x'' from ';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select ''OPT_FUND_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' from(';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select  FUND_SKID, ACCT_FUND_SKID, 0 PROD_SKID, 0 PRMTN_SKID,  BUS_UNIT_SKID, 0 FUND_PROD_SKID,  FUND_GRP_SKID, 0 ACCT_PRMTN_SKID,FY_DATE_SKID ,ETL_MGRT_CODE';
        G_SQLSTMT := G_SQLSTMT || ' from FLTR_LKP_FUND_TMP_' || IN_REGN;
        G_SQLSTMT := G_SQLSTMT || ' where ETL_MGRT_CODE = 2 ';
        G_SQLSTMT := G_SQLSTMT || ' minus';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select distinct x.FUND_SKID, x.ACCT_FUND_SKID, 0 PROD_SKID, 0 PRMTN_SKID,  x.BUS_UNIT_SKID, 0 FUND_PROD_SKID,  x.FUND_GRP_SKID, 0 ACCT_PRMTN_SKID,x.FY_DATE_SKID ,2';
        G_SQLSTMT := G_SQLSTMT || ' from OPT_FUND_FCT x,FLTR_LKP_FUND_TMP_' ||
                     IN_REGN || ' y ';
        G_SQLSTMT := G_SQLSTMT || ' where x.FUND_SKID = y.FUND_SKID ';
        G_SQLSTMT := G_SQLSTMT || 'and x.ACCT_FUND_SKID = y.ACCT_FUND_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and x.BUS_UNIT_SKID = y.BUS_UNIT_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and x.FUND_GRP_SKID = y.FUND_GRP_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and x.FY_DATE_SKID = y.FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' /*and x.REGN_CODE = ''' || IN_REGN ||
                     '''*/';
        G_SQLSTMT := G_SQLSTMT || ' and y.ETL_MGRT_CODE = 2)';
        G_SQLSTMT := G_SQLSTMT || ' )B ';
        G_SQLSTMT := G_SQLSTMT || ' where A.FUND_SKID = B.FUND_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and A.ACCT_FUND_SKID = B.ACCT_FUND_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.PROD_SKID = B.PROD_SKID ';
        G_SQLSTMT := G_SQLSTMT || ' and A.PRMTN_SKID = B.PRMTN_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.BUS_UNIT_SKID = B.BUS_UNIT_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and A.FUND_PROD_SKID = B.FUND_PROD_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.FUND_GRP_SKID = B.FUND_GRP_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and A.ACCT_PRMTN_SKID = B.ACCT_PRMTN_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.FY_DATE_SKID = B.FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' ) ';
        G_SQLSTMT := G_SQLSTMT || ' and A.TBL_NAME = ''OPT_FUND_FCT''';

        EXECUTE IMMEDIATE G_SQLSTMT;

        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        G_SQLSTMT := 'insert /*+ APPEND */  into OPT_FILTR_LKP(TBL_NAME, FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID,ETL_RUN_ID)';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select ''OPT_FUND_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID, ' ||
                     V_ETL_RUN_ID;
        G_SQLSTMT := G_SQLSTMT || ' from ';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select /*+ parallel(a, 2) */ FUND_SKID, ACCT_FUND_SKID, 0 PROD_SKID, 0 PRMTN_SKID,FY_DATE_SKID as DATE_SKID, BUS_UNIT_SKID, 0 FUND_PROD_SKID,  FUND_GRP_SKID, 0 ACCT_PRMTN_SKID ,FY_DATE_SKID,ETL_MGRT_CODE ';
        G_SQLSTMT := G_SQLSTMT || ' from FLTR_LKP_FUND_TMP_' || IN_REGN || ' a';
        G_SQLSTMT := G_SQLSTMT || ' where ETL_MGRT_CODE = 1';
        G_SQLSTMT := G_SQLSTMT || ' minus';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select /*+ parallel(b, 2) */ FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID, ACCT_PRMTN_SKID,FY_DATE_SKID,1 from OPT_FILTR_LKP b where TBL_NAME = ''OPT_FUND_FCT''';
        G_SQLSTMT := G_SQLSTMT || ' )A';
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end
        EXECUTE IMMEDIATE G_SQLSTMT;
        COMMIT;

        -- drop table FLTR_LKP_FUND_TMP_APAC purge;
        G_SQLSTMT := 'drop table FLTR_LKP_FUND_TMP_' || IN_REGN || ' purge';
        EXECUTE IMMEDIATE G_SQLSTMT;

        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_TNSFR_FCT
      WHEN IN_FTBL_NAME = 'OPT_TNSFR_FCT' THEN
        --Judge whether the tmp exists then drop;
        G_SQLSTMT := 'select count(1) from user_tables where table_name = ''OPT_PROD_GTIN_BRAND_TMP_1' ||
                     IN_REGN || '''';
        EXECUTE IMMEDIATE G_SQLSTMT
          INTO V_NUMBER_TBL_COUNT;
        IF V_NUMBER_TBL_COUNT = 1 THEN
          G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_1' || IN_REGN ||
                       ' purge';
          EXECUTE IMMEDIATE G_SQLSTMT;
        END IF;

        G_SQLSTMT := 'select count(1) from user_tables where table_name = ''FLTR_LKP_TNSFR_TMP_' ||
                     IN_REGN || '''';
        EXECUTE IMMEDIATE G_SQLSTMT
          INTO V_NUMBER_TBL_COUNT;
        IF V_NUMBER_TBL_COUNT = 1 THEN
          G_SQLSTMT := 'drop table FLTR_LKP_TNSFR_TMP_' || IN_REGN ||
                       ' purge';
          EXECUTE IMMEDIATE G_SQLSTMT;
        END IF;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  ' || CHR(10) ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10) ||' nologging compress ';

        G_SQLSTMT := 'create table OPT_PROD_GTIN_BRAND_TMP_1' || IN_REGN || '  ' ||
                     CHR(10) || C_FINAL_TBL_STORAGE || ' ' ||
                     V_TBLSPACE_NAME || CHR(10) || ' nologging compress ';

        G_SQLSTMT := G_SQLSTMT ||
                     ' as select gt.prod_id,gt.prod_skid,bd.brand_skid,prod_lvl_desc';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from  (select prod_id,prod_skid,brand_id,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc = ''GTIN'' /*and regn_code = ''' ||
                     IN_REGN || '''*/) gt,';
        G_SQLSTMT := G_SQLSTMT ||
                     ' (select prod_id,prod_skid as brand_skid from opt_prod_fdim /*where regn_code = ''' ||
                     IN_REGN || '''*/) bd';
        G_SQLSTMT := G_SQLSTMT || ' where gt.brand_id = bd.prod_id';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select prod_id,prod_skid,prod_skid as brand_skid ,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc <> ''GTIN'' /*and regn_code = ''' ||
                     IN_REGN || '''*/';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT || ' SELECT NULL,0,0,NULL FROM DUAL';

        EXECUTE IMMEDIATE G_SQLSTMT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  ' || CHR(10) ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10) ||' nologging compress ';

        G_SQLSTMT := 'create table FLTR_LKP_TNSFR_TMP_' || IN_REGN || '  ' ||
                     CHR(10) || C_FINAL_TBL_STORAGE || ' ' ||
                     V_TBLSPACE_NAME || CHR(10) || ' nologging compress ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' as select FUND_SKID,ACCT_FUND_SKID,PROD_SKID,0 PRMTN_SKID,BUS_UNIT_SKID,0 FUND_PROD_SKID, 0 FUND_GRP_SKID, 0 ACCT_PRMTN_SKID, FY_DATE_SKID ,sum(ETL_MGRT_CODE) ETL_MGRT_CODE';
        G_SQLSTMT := G_SQLSTMT || ' from OPT_TNSFR_SFCT';
        G_SQLSTMT := G_SQLSTMT || '/* where REGN_CODE = ''' || IN_REGN ||
                     '''*/';
        G_SQLSTMT := G_SQLSTMT ||
                     ' group by FUND_SKID,ACCT_FUND_SKID,PROD_SKID,BUS_UNIT_SKID, FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' having sum(ETL_MGRT_CODE) <=2';

        EXECUTE IMMEDIATE G_SQLSTMT;

        G_SQLSTMT := 'delete from OPT_FILTR_LKP A';
        G_SQLSTMT := G_SQLSTMT || ' where exists';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT || ' select ''x'' from ';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select ''OPT_TNSFR_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' from(';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select distinct FUND_SKID,ACCT_FUND_SKID,y.BRAND_SKID as PROD_SKID,0 PRMTN_SKID,BUS_UNIT_SKID,0 FUND_PROD_SKID, 0 FUND_GRP_SKID, 0 ACCT_PRMTN_SKID,FY_DATE_SKID ,ETL_MGRT_CODE';
        G_SQLSTMT := G_SQLSTMT || ' from FLTR_LKP_TNSFR_TMP_' || IN_REGN ||
                     ' x,OPT_PROD_GTIN_BRAND_TMP_1' || IN_REGN || ' y';
        G_SQLSTMT := G_SQLSTMT || ' where x.PROD_SKID = y.PROD_SKID ';
        G_SQLSTMT := G_SQLSTMT || ' and x.ETL_MGRT_CODE = 2 ';
        G_SQLSTMT := G_SQLSTMT || ' minus';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select distinct x.FUND_SKID,x.ACCT_FUND_SKID,z.BRAND_SKID as PROD_SKID ,0 PRMTN_SKID,x.BUS_UNIT_SKID,0 FUND_PROD_SKID, 0 FUND_GRP_SKID, 0 ACCT_PRMTN_SKID, x.FY_DATE_SKID ,2';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from OPT_TNSFR_FCT x,FLTR_LKP_TNSFR_TMP_' || IN_REGN ||
                     ' y, OPT_PROD_GTIN_BRAND_TMP_1' || IN_REGN || ' z ';
        G_SQLSTMT := G_SQLSTMT || ' where x.FUND_SKID = y.FUND_SKID ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and   x.ACCT_FUND_SKID = y.ACCT_FUND_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and   x.PROD_SKID  = y.PROD_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and   x.BUS_UNIT_SKID = y.BUS_UNIT_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and   x.PROD_SKID = z.PROD_SKID';
        G_SQLSTMT := G_SQLSTMT || ' /*and x.REGN_CODE = ''' || IN_REGN ||
                     '''*/';
        G_SQLSTMT := G_SQLSTMT || ' and y.ETL_MGRT_CODE = 2)';
        G_SQLSTMT := G_SQLSTMT || ' )B ';
        G_SQLSTMT := G_SQLSTMT || ' where A.FUND_SKID = B.FUND_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and A.ACCT_FUND_SKID = B.ACCT_FUND_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.PROD_SKID = B.PROD_SKID ';
        G_SQLSTMT := G_SQLSTMT || ' and A.PRMTN_SKID = B.PRMTN_SKID';

        G_SQLSTMT := G_SQLSTMT || ' and A.BUS_UNIT_SKID = B.BUS_UNIT_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and A.FUND_PROD_SKID = B.FUND_PROD_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.FUND_GRP_SKID = B.FUND_GRP_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and A.ACCT_PRMTN_SKID = B.ACCT_PRMTN_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.FY_DATE_SKID = B.FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' ) ';
        G_SQLSTMT := G_SQLSTMT || ' and A.TBL_NAME = ''OPT_TNSFR_FCT''';

        EXECUTE IMMEDIATE G_SQLSTMT;

        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        G_SQLSTMT := 'insert /*+ APPEND */  into OPT_FILTR_LKP(TBL_NAME, FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID,ETL_RUN_ID)';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select ''OPT_TNSFR_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID,' ||
                     V_ETL_RUN_ID;
        G_SQLSTMT := G_SQLSTMT || ' from ';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select /*+ parallel(x,2) parallel(y, 2) */ distinct x.FUND_SKID,x.ACCT_FUND_SKID,y.BRAND_SKID as PROD_SKID,0 PRMTN_SKID,x.FY_DATE_SKID as DATE_SKID, x.BUS_UNIT_SKID,0 FUND_PROD_SKID, 0 FUND_GRP_SKID, 0 ACCT_PRMTN_SKID,FY_DATE_SKID ,ETL_MGRT_CODE ';
        G_SQLSTMT := G_SQLSTMT || ' from FLTR_LKP_TNSFR_TMP_' || IN_REGN ||
                     ' x,OPT_PROD_GTIN_BRAND_TMP_1' || IN_REGN || ' y ';
        G_SQLSTMT := G_SQLSTMT || ' where x.PROD_SKID = y.PROD_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and x.ETL_MGRT_CODE = 1';
        G_SQLSTMT := G_SQLSTMT || ' minus';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select /*+ parallel(a, 2) */ FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID, ACCT_PRMTN_SKID,FY_DATE_SKID,1 from OPT_FILTR_LKP a where TBL_NAME = ''OPT_TNSFR_FCT''';
        G_SQLSTMT := G_SQLSTMT || ' )A';
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end
        EXECUTE IMMEDIATE G_SQLSTMT;
        COMMIT;

        --  drop table FLTR_LKP_TNSFR_TMP_APAC purge;
        G_SQLSTMT := 'drop table FLTR_LKP_TNSFR_TMP_' || IN_REGN ||
                     ' purge';
        EXECUTE IMMEDIATE G_SQLSTMT;
        -- drop table OPT_PROD_GTIN_BRAND_TMP_APAC purge;
        G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_1' || IN_REGN ||
                     ' purge';
        EXECUTE IMMEDIATE G_SQLSTMT;

        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_PRMTN_FCT
      WHEN IN_FTBL_NAME = 'OPT_PRMTN_FCT' THEN

        G_SQLSTMT := 'select count(1) from user_tables where table_name = ''FLTR_LKP_PRMTN_TMP_' ||
                     IN_REGN || '''';
        EXECUTE IMMEDIATE G_SQLSTMT
          INTO V_NUMBER_TBL_COUNT;
        IF V_NUMBER_TBL_COUNT = 1 THEN
          G_SQLSTMT := 'drop table FLTR_LKP_PRMTN_TMP_' || IN_REGN ||
                       ' purge';
          EXECUTE IMMEDIATE G_SQLSTMT;
        END IF;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  ' || CHR(10) ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10) ||' nologging compress ';

        G_SQLSTMT := 'create table FLTR_LKP_PRMTN_TMP_' || IN_REGN || ' ' ||
                     CHR(10) || C_FINAL_TBL_STORAGE || ' ' ||
                     V_TBLSPACE_NAME || CHR(10) || ' nologging compress ';

        G_SQLSTMT := G_SQLSTMT ||
                     ' as select  0 FUND_SKID,0 ACCT_FUND_SKID,0 PROD_SKID,BASE_PRMTN_SKID as PRMTN_SKID, BUS_UNIT_SKID,0 FUND_PROD_SKID,0 FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID,sum(ETL_MGRT_CODE) ETL_MGRT_CODE';
        G_SQLSTMT := G_SQLSTMT || ' from OPT_PRMTN_SFCT';
        G_SQLSTMT := G_SQLSTMT || ' /*where REGN_CODE = ''' || IN_REGN ||
                     '''*/';
        G_SQLSTMT := G_SQLSTMT ||
                     ' group by  BASE_PRMTN_SKID, BUS_UNIT_SKID,ACCT_PRMTN_SKID,FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' having sum(ETL_MGRT_CODE) <=2';

        EXECUTE IMMEDIATE G_SQLSTMT;

        G_SQLSTMT := 'delete from OPT_FILTR_LKP A';
        G_SQLSTMT := G_SQLSTMT || ' where exists';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT || ' select ''x'' from ';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select ''OPT_PRMTN_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' from(';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select  0 FUND_SKID,0 ACCT_FUND_SKID,0 PROD_SKID, PRMTN_SKID, BUS_UNIT_SKID,0 FUND_PROD_SKID,0 FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID,ETL_MGRT_CODE';
        G_SQLSTMT := G_SQLSTMT || ' from FLTR_LKP_PRMTN_TMP_' || IN_REGN;
        G_SQLSTMT := G_SQLSTMT || ' where ETL_MGRT_CODE = 2 ';
        G_SQLSTMT := G_SQLSTMT || ' minus';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select distinct 0 FUND_SKID,0 ACCT_FUND_SKID,0 PROD_SKID,x.BASE_PRMTN_SKID as PRMTN_SKID, x.BUS_UNIT_SKID,0 FUND_PROD_SKID,0 FUND_GRP_SKID,  x.ACCT_PRMTN_SKID,x.FY_DATE_SKID,2';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from OPT_PRMTN_FCT x,FLTR_LKP_PRMTN_TMP_' || IN_REGN ||
                     ' y ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' where x.BASE_PRMTN_SKID = y.PRMTN_SKID ';

        G_SQLSTMT := G_SQLSTMT ||
                     ' and   x.BUS_UNIT_SKID = y.BUS_UNIT_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and   x.ACCT_PRMTN_SKID = y.ACCT_PRMTN_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and   x.FY_DATE_SKID = y.FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' /*and x.REGN_CODE = ''' || IN_REGN ||
                     '''*/';
        G_SQLSTMT := G_SQLSTMT || ' and y.ETL_MGRT_CODE = 2)';
        G_SQLSTMT := G_SQLSTMT || ' )B ';
        G_SQLSTMT := G_SQLSTMT || ' where A.FUND_SKID = B.FUND_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and A.ACCT_FUND_SKID = B.ACCT_FUND_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.PROD_SKID = B.PROD_SKID ';
        G_SQLSTMT := G_SQLSTMT || ' and A.PRMTN_SKID = B.PRMTN_SKID';

        G_SQLSTMT := G_SQLSTMT || ' and A.BUS_UNIT_SKID = B.BUS_UNIT_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and A.FUND_PROD_SKID = B.FUND_PROD_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.FUND_GRP_SKID = B.FUND_GRP_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and A.ACCT_PRMTN_SKID = B.ACCT_PRMTN_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.FY_DATE_SKID = B.FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' ) ';
        G_SQLSTMT := G_SQLSTMT || ' and A.TBL_NAME = ''OPT_PRMTN_FCT''';

        EXECUTE IMMEDIATE G_SQLSTMT;

        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        G_SQLSTMT := 'insert /*+ APPEND */ into OPT_FILTR_LKP(TBL_NAME, FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID,ETL_RUN_ID)';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select ''OPT_PRMTN_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID, FY_DATE_SKID,' ||
                     V_ETL_RUN_ID;
        G_SQLSTMT := G_SQLSTMT || ' from ';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select /*+ parallel(a, 2) */ 0 FUND_SKID,0 ACCT_FUND_SKID,0 PROD_SKID, PRMTN_SKID,FY_DATE_SKID as DATE_SKID, BUS_UNIT_SKID,0 FUND_PROD_SKID,0 FUND_GRP_SKID,  ACCT_PRMTN_SKID, FY_DATE_SKID,ETL_MGRT_CODE ';
        G_SQLSTMT := G_SQLSTMT || ' from FLTR_LKP_PRMTN_TMP_' || IN_REGN || ' a';
        G_SQLSTMT := G_SQLSTMT || ' where ETL_MGRT_CODE = 1';
        G_SQLSTMT := G_SQLSTMT || ' minus';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select /*+ parallel(b, 2) */ FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID, ACCT_PRMTN_SKID,FY_DATE_SKID,1 from OPT_FILTR_LKP b where TBL_NAME = ''OPT_PRMTN_FCT''';
        G_SQLSTMT := G_SQLSTMT || ' )A';
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end
        EXECUTE IMMEDIATE G_SQLSTMT;

        COMMIT;

        --  drop table FLTR_LKP_PRMTN_TMP_APAC purge;
        G_SQLSTMT := 'drop table FLTR_LKP_PRMTN_TMP_' || IN_REGN ||
                     ' purge';
        EXECUTE IMMEDIATE G_SQLSTMT;

        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_PRMTN_PYMT_FCT
      WHEN IN_FTBL_NAME = 'OPT_PRMTN_PYMT_FCT' THEN

        /* G_SQLSTMT := 'select count(1) from user_tables where table_name = ''OPT_PROD_GTIN_BRAND_TMP_2' ||
                             IN_REGN || '''';
                EXECUTE IMMEDIATE G_SQLSTMT
                  INTO V_NUMBER_TBL_COUNT;
                IF V_NUMBER_TBL_COUNT = 1 THEN
                  G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_2' || IN_REGN ||
                               ' purge';
                  EXECUTE IMMEDIATE G_SQLSTMT;
                END IF;

                G_SQLSTMT := 'select count(1) from user_tables where table_name = ''FLTR_LKP_PRMTN_PYMT_TMP_' ||
                             IN_REGN || '''';
                EXECUTE IMMEDIATE G_SQLSTMT
                  INTO V_NUMBER_TBL_COUNT;
                IF V_NUMBER_TBL_COUNT = 1 THEN
                  G_SQLSTMT := 'drop table FLTR_LKP_PRMTN_PYMT_TMP_' || IN_REGN ||
                               ' purge';
                  EXECUTE IMMEDIATE G_SQLSTMT;
                END IF;
              -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
              --  ' || CHR(10) ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10) ||' nologging compress ';

                G_SQLSTMT := 'create table OPT_PROD_GTIN_BRAND_TMP_2' || IN_REGN ||' ' || CHR(10)
                              ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10)
                              ||' nologging compress ';

                G_SQLSTMT := G_SQLSTMT ||
                             ' as select gt.prod_id,gt.prod_skid,bd.brand_skid,prod_lvl_desc';
                G_SQLSTMT := G_SQLSTMT ||
                             ' from  (select prod_id,prod_skid,brand_id,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc = ''GTIN'' \*and regn_code = ''' ||
                             IN_REGN || '''*\) gt,';
                G_SQLSTMT := G_SQLSTMT ||
                             ' (select prod_id,prod_skid as brand_skid from opt_prod_fdim \*where regn_code = ''' ||
                             IN_REGN || '''*\) bd';
                G_SQLSTMT := G_SQLSTMT || ' where gt.brand_id = bd.prod_id';
                G_SQLSTMT := G_SQLSTMT || ' UNION';
                G_SQLSTMT := G_SQLSTMT ||
                             ' select prod_id,prod_skid,prod_skid as brand_skid ,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc <> ''GTIN'' \*and regn_code = ''' ||
                             IN_REGN || '''*\';
                G_SQLSTMT := G_SQLSTMT || ' UNION';
                G_SQLSTMT := G_SQLSTMT || ' SELECT NULL,0,0,NULL FROM DUAL';

                EXECUTE IMMEDIATE G_SQLSTMT;
              -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
              --  ' || CHR(10) ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10) ||' nologging compress ';

                G_SQLSTMT := 'create table FLTR_LKP_PRMTN_PYMT_TMP_' || IN_REGN || ' ' || CHR(10)
                             ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10)
                             ||' nologging compress ';

                G_SQLSTMT := G_SQLSTMT ||
                             ' as select  FUND_SKID, ACCT_FUND_SKID ,PROD_SKID,PRMTN_SKID,DATE_SKID,BUS_UNIT_SKID,0 FUND_PROD_SKID, 0 FUND_GRP_SKID, ACCT_PRMTN_SKID ,ACTVY_SKID,FY_DATE_SKID, sum(ETL_MGRT_CODE) ETL_MGRT_CODE';
                G_SQLSTMT := G_SQLSTMT || ' from OPT_PRMTN_PYMT_SFCT';
                G_SQLSTMT := G_SQLSTMT || ' \*where REGN_CODE = ''' || IN_REGN ||
                             '''*\';
                G_SQLSTMT := G_SQLSTMT ||
                             ' group by FUND_SKID,  ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID,BUS_UNIT_SKID,ACCT_PRMTN_SKID,ACTVY_SKID,FY_DATE_SKID';
                G_SQLSTMT := G_SQLSTMT || ' having sum(ETL_MGRT_CODE) <=2';

                EXECUTE IMMEDIATE G_SQLSTMT;

                G_SQLSTMT := 'delete from OPT_FILTR_LKP A';
                G_SQLSTMT := G_SQLSTMT || ' where exists';
                G_SQLSTMT := G_SQLSTMT || ' ( ';
                G_SQLSTMT := G_SQLSTMT || ' select ''x'' from ';
                G_SQLSTMT := G_SQLSTMT || ' ( ';
                G_SQLSTMT := G_SQLSTMT ||
                             ' select ''OPT_PRMTN_PYMT_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,ACTVY_SKID,FY_DATE_SKID';
                G_SQLSTMT := G_SQLSTMT || ' from(';
                G_SQLSTMT := G_SQLSTMT ||
                             ' select  distinct x.FUND_SKID, x.ACCT_FUND_SKID ,y.BRAND_SKID as PROD_SKID,x.PRMTN_SKID,x.FY_DATE_SKID DATE_SKID,x.BUS_UNIT_SKID,0 FUND_PROD_SKID, 0 FUND_GRP_SKID, x.ACCT_PRMTN_SKID ,x.ACTVY_SKID,x.FY_DATE_SKID,ETL_MGRT_CODE ';
                G_SQLSTMT := G_SQLSTMT || ' from FLTR_LKP_PRMTN_PYMT_TMP_' ||
                             IN_REGN || ' x,OPT_PROD_GTIN_BRAND_TMP_2' || IN_REGN || ' y';
                G_SQLSTMT := G_SQLSTMT || ' where x.PROD_SKID = y.PROD_SKID ';
                G_SQLSTMT := G_SQLSTMT || ' and x.ETL_MGRT_CODE = 2 ';
                G_SQLSTMT := G_SQLSTMT || ' minus';
                G_SQLSTMT := G_SQLSTMT ||
                             ' select distinct x.FUND_SKID , x.ACCT_FUND_SKID ,z.BRAND_SKID as PROD_SKID,x.PRMTN_SKID,x.FY_DATE_SKID,x.BUS_UNIT_SKID,0 FUND_PROD_SKID, 0 FUND_GRP_SKID, x.ACCT_PRMTN_SKID ,x.ACTVY_SKID,x.FY_DATE_SKID,2 ';
                G_SQLSTMT := G_SQLSTMT ||
                             ' from OPT_PRMTN_PYMT_FCT x,FLTR_LKP_PRMTN_PYMT_TMP_' ||
                             IN_REGN || ' y, OPT_PROD_GTIN_BRAND_TMP_2' || IN_REGN ||
                             ' z ';
                G_SQLSTMT := G_SQLSTMT || ' where x.FUND_SKID = y.FUND_SKID ';
                G_SQLSTMT := G_SQLSTMT ||
                             ' and   x.ACCT_FUND_SKID = y.ACCT_FUND_SKID';
                G_SQLSTMT := G_SQLSTMT || ' and   x.PROD_SKID  = y.PROD_SKID';
                G_SQLSTMT := G_SQLSTMT || ' and   x.PRMTN_SKID = y.PRMTN_SKID';
                G_SQLSTMT := G_SQLSTMT || ' and   x.DATE_SKID = y.DATE_SKID';
                G_SQLSTMT := G_SQLSTMT ||
                             ' and   x.BUS_UNIT_SKID = y.BUS_UNIT_SKID';
                G_SQLSTMT := G_SQLSTMT ||
                             ' and   x.ACCT_PRMTN_SKID = y.ACCT_PRMTN_SKID';
                G_SQLSTMT := G_SQLSTMT || ' and   x.ACTVY_SKID = y.ACTVY_SKID';
                G_SQLSTMT := G_SQLSTMT || ' and   x.PROD_SKID = z.PROD_SKID';
                G_SQLSTMT := G_SQLSTMT || ' \*and x.REGN_CODE = ''' || IN_REGN ||
                             '''*\';
                G_SQLSTMT := G_SQLSTMT || ' and y.ETL_MGRT_CODE = 2)';
                G_SQLSTMT := G_SQLSTMT || ' )B ';
                G_SQLSTMT := G_SQLSTMT || ' where A.FUND_SKID = B.FUND_SKID';
                G_SQLSTMT := G_SQLSTMT ||
                             ' and A.ACCT_FUND_SKID = B.ACCT_FUND_SKID';
                G_SQLSTMT := G_SQLSTMT || ' and A.PROD_SKID = B.PROD_SKID ';
                G_SQLSTMT := G_SQLSTMT || ' and A.PRMTN_SKID = B.PRMTN_SKID';
                G_SQLSTMT := G_SQLSTMT || ' and A.DATE_SKID = B.DATE_SKID';
                G_SQLSTMT := G_SQLSTMT || ' and A.BUS_UNIT_SKID = B.BUS_UNIT_SKID';
                G_SQLSTMT := G_SQLSTMT ||
                             ' and A.FUND_PROD_SKID = B.FUND_PROD_SKID';
                G_SQLSTMT := G_SQLSTMT || ' and A.FUND_GRP_SKID = B.FUND_GRP_SKID';
                G_SQLSTMT := G_SQLSTMT ||
                             ' and A.ACCT_PRMTN_SKID = B.ACCT_PRMTN_SKID';
                G_SQLSTMT := G_SQLSTMT || ' and A.ACTVY_SKID = B.ACTVY_SKID';
                G_SQLSTMT := G_SQLSTMT || ' and A.FY_DATE_SKID = B.FY_DATE_SKID';
                G_SQLSTMT := G_SQLSTMT || ' ) ';
                G_SQLSTMT := G_SQLSTMT ||
                             ' and A.TBL_NAME = ''OPT_PRMTN_PYMT_FCT''';

                EXECUTE IMMEDIATE G_SQLSTMT;

                COMMIT;
              -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
             --  \*+ APPEND PARALLEL(2) *\
                G_SQLSTMT := 'insert \*+ APPEND PARALLEL(2) *\  into OPT_FILTR_LKP(TBL_NAME, FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,ACTVY_SKID,FY_DATE_SKID,ETL_RUN_ID)';
                G_SQLSTMT := G_SQLSTMT ||
                             ' select ''OPT_PRMTN_PYMT_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,ACTVY_SKID,FY_DATE_SKID,' ||
                             V_ETL_RUN_ID;
                G_SQLSTMT := G_SQLSTMT || ' from ';
                G_SQLSTMT := G_SQLSTMT || ' ( ';
                G_SQLSTMT := G_SQLSTMT ||
                             ' select  distinct x.FUND_SKID, x.ACCT_FUND_SKID ,y.BRAND_SKID as PROD_SKID,x.PRMTN_SKID,x.FY_DATE_SKID DATE_SKID,x.BUS_UNIT_SKID,0 FUND_PROD_SKID, 0 FUND_GRP_SKID, x.ACCT_PRMTN_SKID ,x.ACTVY_SKID,x.FY_DATE_SKID ,ETL_MGRT_CODE ';
                G_SQLSTMT := G_SQLSTMT || ' from FLTR_LKP_PRMTN_PYMT_TMP_' ||
                             IN_REGN || ' x,OPT_PROD_GTIN_BRAND_TMP_2' || IN_REGN ||
                             ' y ';
                G_SQLSTMT := G_SQLSTMT || ' where x.PROD_SKID = y.PROD_SKID';
                G_SQLSTMT := G_SQLSTMT || ' and x.ETL_MGRT_CODE = 1';
                G_SQLSTMT := G_SQLSTMT || ' minus';
                G_SQLSTMT := G_SQLSTMT ||
                             ' select FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID, ACCT_PRMTN_SKID, ACTVY_SKID,FY_DATE_SKID,1 from OPT_FILTR_LKP where TBL_NAME = ''OPT_PRMTN_PYMT_FCT''';
                G_SQLSTMT := G_SQLSTMT || ' )A';

                EXECUTE IMMEDIATE G_SQLSTMT;

                COMMIT;

                --  drop table FLTR_LKP_PRMTN_PYMT_TMP_APAC purge;
                G_SQLSTMT := 'drop table FLTR_LKP_PRMTN_PYMT_TMP_' || IN_REGN ||
                             ' purge';
                EXECUTE IMMEDIATE G_SQLSTMT;
                --  drop table OPT_PROD_GTIN_BRAND_TMP_APAC purge;
                G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_2' || IN_REGN ||
                             ' purge';
                EXECUTE IMMEDIATE G_SQLSTMT;
        */

        -- Modified by Lou Yang 20100125 in R10 For B007
        -- Change incremental loading to full loading since we have removed OPT_PRMTN_PYMT_SFCT table for performance tuning

        SQL_MSG := 'Deleting data of OPT_FILTR_LKP sourced from OPT_PRMTN_PYMT_FCT';
        DELETE /*+ PARALLEL(4) */ FROM OPT_FILTR_LKP WHERE TBL_NAME = 'OPT_PRMTN_PYMT_FCT';
        COMMIT;

        SQL_MSG := 'Inserting data of OPT_FILTR_LKP sourced from OPT_PRMTN_PYMT_FCT';
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        INSERT /*+ APPEND PARALLEL(2) */
        INTO OPT_FILTR_LKP
          (TBL_NAME,
           FUND_SKID,
           ACCT_FUND_SKID,
           PROD_SKID,
           PRMTN_SKID,
           DATE_SKID,
           BUS_UNIT_SKID,
           FUND_PROD_SKID,
           FUND_GRP_SKID,
           ACCT_PRMTN_SKID,
           FY_DATE_SKID,
           COMB_PRMTN_DIM_SKID,
           ACTVY_SKID,
           ETL_RUN_ID)
          SELECT 'OPT_PRMTN_PYMT_FCT',
                 FUND_SKID,
                 ACCT_FUND_SKID,
                 PROD_SKID,
                 PRMTN_SKID,
                 DATE_SKID,
                 BUS_UNIT_SKID,
                 FUND_PROD_SKID,
                 FUND_GRP_SKID,
                 ACCT_PRMTN_SKID,
                 FY_DATE_SKID,
                 COMB_PRMTN_DIM_SKID,
                 ACTVY_SKID,
                 V_ETL_RUN_ID
            FROM (SELECT /*+ parallel(PF, 2) parallel(GT, 2) parallel(BD, 2) */ DISTINCT PF.FUND_SKID,
                                  PF.ACCT_FUND_SKID,
                                  BD.PROD_SKID,
                                  PF.PRMTN_SKID,
                                  PF.FY_DATE_SKID AS DATE_SKID,
                                  PF.BUS_UNIT_SKID,
                                  0 AS FUND_PROD_SKID,
                                  0 AS FUND_GRP_SKID,
                                  PF.ACCT_PRMTN_SKID,
                                  PF.FY_DATE_SKID,
                                  0 AS COMB_PRMTN_DIM_SKID,
                                  PF.ACTVY_SKID,
                                  0 AS V_ETL_RUN_ID
                    FROM OPT_PRMTN_PYMT_FCT PF,
                         OPT_PROD_FDIM      GT,
                         OPT_PROD_FDIM      BD
                   WHERE PF.PROD_SKID = GT.PROD_SKID(+)
                     AND GT.BRAND_ID = BD.PROD_ID(+)) A;
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end
        COMMIT;

        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_ACTVY_FCT
      WHEN IN_FTBL_NAME = 'OPT_ACTVY_FCT' THEN

        G_SQLSTMT := 'select count(1) from user_tables where table_name = ''FLTR_LKP_ACTVY_TMP_' ||
                     IN_REGN || '''';
        EXECUTE IMMEDIATE G_SQLSTMT
          INTO V_NUMBER_TBL_COUNT;
        IF V_NUMBER_TBL_COUNT = 1 THEN
          G_SQLSTMT := 'drop table FLTR_LKP_ACTVY_TMP_' || IN_REGN ||
                       ' purge';
          EXECUTE IMMEDIATE G_SQLSTMT;
        END IF;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  ' || CHR(10) ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10) ||' nologging compress ';

        G_SQLSTMT := 'create table FLTR_LKP_ACTVY_TMP_' || IN_REGN || ' ' ||
                     CHR(10) || C_FINAL_TBL_STORAGE || ' ' ||
                     V_TBLSPACE_NAME || CHR(10) || ' nologging compress ';

        G_SQLSTMT := G_SQLSTMT ||
                     ' as select  FUND_SKID,ACCT_FUND_SKID ,0 PROD_SKID,PRMTN_SKID,BUS_UNIT_SKID,0 FUND_PROD_SKID,0 FUND_GRP_SKID, ACCT_PRMTN_SKID ,ACTVY_SKID,FY_DATE_SKID,sum(ETL_MGRT_CODE) ETL_MGRT_CODE';
        G_SQLSTMT := G_SQLSTMT || ' from OPT_ACTVY_SFCT';
        G_SQLSTMT := G_SQLSTMT || ' /*where REGN_CODE = ''' || IN_REGN ||
                     '''*/';
        G_SQLSTMT := G_SQLSTMT ||
                     ' group by FUND_SKID,ACCT_FUND_SKID ,PRMTN_SKID,BUS_UNIT_SKID,ACCT_PRMTN_SKID,ACTVY_SKID,FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' having sum(ETL_MGRT_CODE) <=2';

        EXECUTE IMMEDIATE G_SQLSTMT;

        G_SQLSTMT := 'delete /*+ PARALLEL(4) */ from OPT_FILTR_LKP A';
        G_SQLSTMT := G_SQLSTMT || ' where exists';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT || ' select ''x'' from ';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select  FUND_SKID,ACCT_FUND_SKID ,0 PROD_SKID,PRMTN_SKID,BUS_UNIT_SKID,0 FUND_PROD_SKID,0 FUND_GRP_SKID, ACCT_PRMTN_SKID ,ACTVY_SKID,FY_DATE_SKID ,ETL_MGRT_CODE ';
        G_SQLSTMT := G_SQLSTMT || ' from(';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select  FUND_SKID, ACCT_FUND_SKID, 0 PROD_SKID, 0 PRMTN_SKID,  BUS_UNIT_SKID, 0 FUND_PROD_SKID,  FUND_GRP_SKID, 0 ACCT_PRMTN_SKID ,ACTVY_SKID,FY_DATE_SKID ,ETL_MGRT_CODE';
        G_SQLSTMT := G_SQLSTMT || ' from FLTR_LKP_ACTVY_TMP_' || IN_REGN;
        G_SQLSTMT := G_SQLSTMT || ' where ETL_MGRT_CODE = 2 ';
        G_SQLSTMT := G_SQLSTMT || ' minus';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select distinct x.FUND_SKID,x.ACCT_FUND_SKID ,0 PROD_SKID,x.PRMTN_SKID,x.BUS_UNIT_SKID,0 FUND_PROD_SKID,0 FUND_GRP_SKID, x.ACCT_PRMTN_SKID ,x.ACTVY_SKID,x.FY_DATE_SKID ,2 ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from OPT_ACTVY_FCT x,FLTR_LKP_ACTVY_TMP_' || IN_REGN ||
                     ' y ';
        G_SQLSTMT := G_SQLSTMT || ' where x.FUND_SKID = y.FUND_SKID ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and x.ACCT_FUND_SKID = y.ACCT_FUND_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and x.PRMTN_SKID = y.PRMTN_SKID';
        --    G_SQLSTMT := G_SQLSTMT || ' and x.DATE_SKID = y.DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and x.BUS_UNIT_SKID = y.BUS_UNIT_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and x.ACCT_PRMTN_SKID = y.ACCT_PRMTN_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and x.ACTVY_SKID = y.ACTVY_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and x.FY_DATE_SKID = y.FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' /*and x.REGN_CODE = ''' || IN_REGN ||
                     '''*/';
        G_SQLSTMT := G_SQLSTMT || ' and y.ETL_MGRT_CODE = 2)';
        G_SQLSTMT := G_SQLSTMT || ' )B ';
        G_SQLSTMT := G_SQLSTMT || ' where A.FUND_SKID = B.FUND_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and A.ACCT_FUND_SKID = B.ACCT_FUND_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.PROD_SKID = B.PROD_SKID ';
        G_SQLSTMT := G_SQLSTMT || ' and A.PRMTN_SKID = B.PRMTN_SKID';
        --     G_SQLSTMT := G_SQLSTMT || ' and A.DATE_SKID = B.DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.BUS_UNIT_SKID = B.BUS_UNIT_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and A.FUND_PROD_SKID = B.FUND_PROD_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.FUND_GRP_SKID = B.FUND_GRP_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and A.ACCT_PRMTN_SKID = B.ACCT_PRMTN_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.ACTVY_SKID = B.ACTVY_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.FY_DATE_SKID = B.FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' ) ';
        G_SQLSTMT := G_SQLSTMT || ' and A.TBL_NAME = ''OPT_ACTVY_FCT''';

        EXECUTE IMMEDIATE G_SQLSTMT;

        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        G_SQLSTMT := 'insert /*+ APPEND */  into OPT_FILTR_LKP(TBL_NAME, FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,ACTVY_SKID,FY_DATE_SKID,ETL_RUN_ID)';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select ''OPT_ACTVY_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID, ACTVY_SKID,FY_DATE_SKID,' ||
                     V_ETL_RUN_ID;
        G_SQLSTMT := G_SQLSTMT || ' from ';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select /*+ parallel(a, 2) */ FUND_SKID,ACCT_FUND_SKID ,0 PROD_SKID,PRMTN_SKID,FY_DATE_SKID as DATE_SKID,BUS_UNIT_SKID,0 FUND_PROD_SKID,0 FUND_GRP_SKID, ACCT_PRMTN_SKID ,ACTVY_SKID ,FY_DATE_SKID,ETL_MGRT_CODE ';
        G_SQLSTMT := G_SQLSTMT || ' from FLTR_LKP_ACTVY_TMP_' || IN_REGN || ' a';
        G_SQLSTMT := G_SQLSTMT || ' where ETL_MGRT_CODE = 1';
        G_SQLSTMT := G_SQLSTMT || ' minus';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select /*+ parallel(b, 2) */ FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID, ACCT_PRMTN_SKID,ACTVY_SKID,FY_DATE_SKID, 1 from OPT_FILTR_LKP b where TBL_NAME = ''OPT_ACTVY_FCT''';
        G_SQLSTMT := G_SQLSTMT || ' )A';
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end
        EXECUTE IMMEDIATE G_SQLSTMT;

        COMMIT;

        --  drop table FLTR_LKP_ACTVY_TMP_APAC purge;
        G_SQLSTMT := 'drop table FLTR_LKP_ACTVY_TMP_' || IN_REGN ||
                     ' purge';
        EXECUTE IMMEDIATE G_SQLSTMT;

        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_PRMTN_PROD_FCT
      WHEN IN_FTBL_NAME = 'OPT_PRMTN_PROD_FCT' THEN

        SELECT COUNT(1)
          INTO V_NUMBER_TBL_COUNT
          FROM USER_TABLES
         WHERE TABLE_NAME = 'OPT_PROD_GTIN_BRAND_TMP_3';
        IF V_NUMBER_TBL_COUNT = 1 THEN
          G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_3 purge';
          EXECUTE IMMEDIATE G_SQLSTMT;
        END IF;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  ' || CHR(10) ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10) ||' nologging compress ';

        G_SQLSTMT := 'create table OPT_PROD_GTIN_BRAND_TMP_3 ' || CHR(10) ||
                     C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME ||
                     CHR(10) || ' nologging compress ';

        G_SQLSTMT := G_SQLSTMT ||
                     ' as select gt.prod_id,gt.prod_skid,bd.brand_skid,prod_lvl_desc';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from  (select prod_id,prod_skid,brand_id,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc = ''GTIN'' ) gt,';
        G_SQLSTMT := G_SQLSTMT ||
                     ' (select prod_id,prod_skid as brand_skid from opt_prod_fdim ) bd';
        G_SQLSTMT := G_SQLSTMT || ' where gt.brand_id = bd.prod_id';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select prod_id,prod_skid,prod_skid as brand_skid ,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc <> ''GTIN''';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT || ' SELECT NULL,0,0,NULL FROM DUAL';
        EXECUTE IMMEDIATE G_SQLSTMT;

        --Create index on PROD_SKID from tmp table
        FOR X IN (SELECT 'CREATE  INDEX ' || SUBSTR(TABLE_NAME, 1, 24) ||
                         '_IDX' || TO_CHAR(COLUMN_ID, 'fm00') || CHR(10) ||
                         'ON ' || TABLE_NAME || '(' || COLUMN_NAME || ')' ||
                         CHR(10) || 'NOLOGGING' || CHR(10) || 'PARALLEL 1' ||
                         --CHR(10) || 'TABLESPACE optima01' || CHR(10) || CASE
                         CHR(10) || 'TABLESPACE '|| V_INDEX || CHR(10) || CASE
                           WHEN EXISTS
                            (SELECT 'x'
                                   FROM USER_TAB_PARTITIONS
                                  WHERE TABLE_NAME = DTC.TABLE_NAME) THEN
                            'LOCAL'
                         END AS CREATE_INDEX_STMT
                    FROM USER_TAB_COLUMNS DTC
                   WHERE TABLE_NAME IN ('OPT_PROD_GTIN_BRAND_TMP_3')
                     AND COLUMN_NAME = 'PROD_SKID') LOOP
          EXECUTE IMMEDIATE X.CREATE_INDEX_STMT;
        END LOOP;

        DELETE /*+ PARALLEL(4) */ FROM OPT_FILTR_LKP WHERE TBL_NAME = 'OPT_PRMTN_PROD_FCT';
        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        G_SQLSTMT := 'insert  /*+ APPEND */  into OPT_FILTR_LKP(TBL_NAME, FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID,ETL_RUN_ID)';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select ''OPT_PRMTN_PROD_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,FY_DATE_SKID, ' ||
                     V_ETL_RUN_ID;
        G_SQLSTMT := G_SQLSTMT || ' from ';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select /*+ parallel(x,2) parallel(y,2) */ distinct 0 FUND_SKID,0 ACCT_FUND_SKID,y.BRAND_SKID as PROD_SKID,x.PRMTN_SKID,x.FY_DATE_SKID DATE_SKID,x.BUS_UNIT_SKID,0 FUND_PROD_SKID, 0 FUND_GRP_SKID, x.ACCT_PRMTN_SKID,x.FY_DATE_SKID ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from OPT_PRMTN_PROD_FCT x,OPT_PROD_GTIN_BRAND_TMP_3 y';
        G_SQLSTMT := G_SQLSTMT || ' where x.PROD_SKID = y.PROD_SKID ';
        G_SQLSTMT := G_SQLSTMT || ' )A';
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end
        EXECUTE IMMEDIATE G_SQLSTMT;
        COMMIT;
        --  drop table OPT_PROD_GTIN_BRAND_TMP_APAC purge;
        G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_3 purge';
        EXECUTE IMMEDIATE G_SQLSTMT;
        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --OPT_ACTVY_PROD_FCT
      WHEN IN_FTBL_NAME = 'OPT_ACTVY_PROD_FCT' THEN

        G_SQLSTMT := 'select count(1) from user_tables where table_name = ''OPT_PROD_GTIN_BRAND_TMP_4' ||
                     IN_REGN || '''';
            EXECUTE IMMEDIATE G_SQLSTMT
          INTO V_NUMBER_TBL_COUNT;
        IF V_NUMBER_TBL_COUNT = 1 THEN
          G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_4' || IN_REGN ||
                       ' purge';
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||'Dropping TMP table started');
         DBMS_OUTPUT.PUT_LINE(G_SQLSTMT);
         DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||'Dropping TMP table ended');
          EXECUTE IMMEDIATE G_SQLSTMT;
        END IF;

        G_SQLSTMT := 'select count(1) from user_tables where table_name = ''FLTR_LKP_ACTVY_PROD_TMP_' ||
                     IN_REGN || '''';
         EXECUTE IMMEDIATE G_SQLSTMT
          INTO V_NUMBER_TBL_COUNT;
        IF V_NUMBER_TBL_COUNT = 1 THEN
          G_SQLSTMT := 'drop table FLTR_LKP_ACTVY_PROD_TMP_' || IN_REGN ||
                       ' purge';
         DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Dropping TMP table started');
         DBMS_OUTPUT.PUT_LINE(G_SQLSTMT);
         DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Dropping TMP table ended');
          EXECUTE IMMEDIATE G_SQLSTMT;
        END IF;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  ' || CHR(10) ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10) ||' nologging compress ';

        G_SQLSTMT := 'create table OPT_PROD_GTIN_BRAND_TMP_4' || IN_REGN || ' ' ||
                     CHR(10) || C_FINAL_TBL_STORAGE || ' ' ||
                     V_TBLSPACE_NAME || CHR(10) || ' nologging compress ';

        G_SQLSTMT := G_SQLSTMT ||
                     ' as select gt.prod_id,gt.prod_skid,bd.brand_skid,prod_lvl_desc';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from  (select prod_id,prod_skid,brand_id,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc = ''GTIN'' /*and regn_code = ''' ||
                     IN_REGN || '''*/) gt,';
        G_SQLSTMT := G_SQLSTMT ||
                     ' (select prod_id,prod_skid as brand_skid from opt_prod_fdim /*where regn_code = ''' ||
                     IN_REGN || '''*/) bd';
        G_SQLSTMT := G_SQLSTMT || ' where gt.brand_id = bd.prod_id';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select prod_id,prod_skid,prod_skid as brand_skid ,prod_lvl_desc from opt_prod_fdim where prod_lvl_desc <> ''GTIN'' /*and regn_code = ''' ||
                     IN_REGN || '''*/';
        G_SQLSTMT := G_SQLSTMT || ' UNION';
        G_SQLSTMT := G_SQLSTMT || ' SELECT NULL,0,0,NULL FROM DUAL';
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Creation of TMP table started');
         DBMS_OUTPUT.PUT_LINE(G_SQLSTMT);
         DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Creation of TMP table ended');
        EXECUTE IMMEDIATE G_SQLSTMT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  ' || CHR(10) ||C_FINAL_TBL_STORAGE||' '|| V_TBLSPACE_NAME || CHR(10) ||' nologging compress ';

        G_SQLSTMT := 'create table FLTR_LKP_ACTVY_PROD_TMP_' || IN_REGN || ' ' ||
                     CHR(10) || C_FINAL_TBL_STORAGE || ' ' ||
                     V_TBLSPACE_NAME || CHR(10) || ' nologging compress ';

        G_SQLSTMT := G_SQLSTMT ||
                     ' as SELECT  FUND_SKID,0 ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,BUS_UNIT_SKID,0 FUND_PROD_SKID , 0 FUND_GRP_SKID, ACCT_PRMTN_SKID ,ACTVY_SKID,FY_DATE_SKID, sum(ETL_MGRT_CODE) ETL_MGRT_CODE';
        G_SQLSTMT := G_SQLSTMT || ' from OPT_ACTVY_PROD_SFCT';
        G_SQLSTMT := G_SQLSTMT || ' /*where REGN_CODE = ''' || IN_REGN ||
                     '''*/';
        G_SQLSTMT := G_SQLSTMT ||
                     ' group by FUND_SKID,PROD_SKID,PRMTN_SKID,BUS_UNIT_SKID,ACCT_PRMTN_SKID,ACTVY_SKID,FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' having sum(ETL_MGRT_CODE) <=2';

        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Creation of TMP table started');
         DBMS_OUTPUT.PUT_LINE(G_SQLSTMT);
         DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Creation of TMP table ended');
        EXECUTE IMMEDIATE G_SQLSTMT;

        G_SQLSTMT := 'delete /*+ PARALLEL(4) */ from OPT_FILTR_LKP A';
        G_SQLSTMT := G_SQLSTMT || ' where exists';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT || ' select ''x'' from ';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select ''OPT_ACTVY_PROD_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID,ACTVY_SKID,FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' from(';
        G_SQLSTMT := G_SQLSTMT ||
                     ' SELECT distinct x.FUND_SKID,0 ACCT_FUND_SKID,y.BRAND_SKID as PROD_SKID,x.PRMTN_SKID,x.BUS_UNIT_SKID,0 FUND_PROD_SKID , 0 FUND_GRP_SKID, x.ACCT_PRMTN_SKID ,x.ACTVY_SKID ,x.FY_DATE_SKID,x.ETL_MGRT_CODE ';
        G_SQLSTMT := G_SQLSTMT || ' from FLTR_LKP_ACTVY_PROD_TMP_' ||
                     IN_REGN || ' x,OPT_PROD_GTIN_BRAND_TMP_4' || IN_REGN || ' y';
        G_SQLSTMT := G_SQLSTMT || ' where x.PROD_SKID = y.PROD_SKID ';
        G_SQLSTMT := G_SQLSTMT || ' and x.ETL_MGRT_CODE = 2 ';
        G_SQLSTMT := G_SQLSTMT || ' minus';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select distinct x.FUND_SKID,0 ACCT_FUND_SKID,z.BRAND_SKID as PROD_SKID,x.PRMTN_SKID,x.BUS_UNIT_SKID,0 FUND_PROD_SKID , 0 FUND_GRP_SKID, x.ACCT_PRMTN_SKID ,x.ACTVY_SKID,x.FY_DATE_SKID,2 ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' from OPT_ACTVY_PROD_FCT x,FLTR_LKP_ACTVY_PROD_TMP_' ||
                     IN_REGN || ' y, OPT_PROD_GTIN_BRAND_TMP_4' || IN_REGN ||
                     ' z ';
        G_SQLSTMT := G_SQLSTMT || ' where x.FUND_SKID = y.FUND_SKID ';
        G_SQLSTMT := G_SQLSTMT || ' and   x.PROD_SKID  = y.PROD_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and   x.PRMTN_SKID = y.PRMTN_SKID';
        --     G_SQLSTMT := G_SQLSTMT || ' and   x.DATE_SKID = y.DATE_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and   x.BUS_UNIT_SKID = y.BUS_UNIT_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and   x.ACCT_PRMTN_SKID = y.ACCT_PRMTN_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and   x.ACTVY_SKID = y.ACTVY_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and   x.PROD_SKID = z.PROD_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and   x.FY_DATE_SKID = y.FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' /*and x.REGN_CODE = ''' || IN_REGN ||
                     '''*/';
        G_SQLSTMT := G_SQLSTMT || ' and y.ETL_MGRT_CODE = 2)';
        G_SQLSTMT := G_SQLSTMT || ' )B ';
        G_SQLSTMT := G_SQLSTMT || ' where A.FUND_SKID = B.FUND_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and A.ACCT_FUND_SKID = B.ACCT_FUND_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.PROD_SKID = B.PROD_SKID ';
        G_SQLSTMT := G_SQLSTMT || ' and A.PRMTN_SKID = B.PRMTN_SKID';
        --      G_SQLSTMT := G_SQLSTMT || ' and A.DATE_SKID = B.DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.BUS_UNIT_SKID = B.BUS_UNIT_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and A.FUND_PROD_SKID = B.FUND_PROD_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.FUND_GRP_SKID = B.FUND_GRP_SKID';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and A.ACCT_PRMTN_SKID = B.ACCT_PRMTN_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.ACTVY_SKID = B.ACTVY_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and A.FY_DATE_SKID = B.FY_DATE_SKID';
        G_SQLSTMT := G_SQLSTMT || ' ) ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' and A.TBL_NAME = ''OPT_PRMTN_PYMT_FCT''';

         DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Deletion of OPT_FILTR_LKP table started');
         DBMS_OUTPUT.PUT_LINE(G_SQLSTMT);
         DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Deletion of OPT_FILTR_LKP table ended');
        EXECUTE IMMEDIATE G_SQLSTMT;
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Deleted Records :'|| SQL%ROWCOUNT||' records');
        COMMIT;
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        G_SQLSTMT := 'insert /*+ APPEND */  into OPT_FILTR_LKP(TBL_NAME, FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID, ACTVY_SKID,FY_DATE_SKID,ETL_RUN_ID)';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select ''OPT_ACTVY_PROD_FCT'', FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID,  ACCT_PRMTN_SKID, ACTVY_SKID,FY_DATE_SKID,' ||
                     V_ETL_RUN_ID;
        G_SQLSTMT := G_SQLSTMT || ' from ';
        G_SQLSTMT := G_SQLSTMT || ' ( ';
        G_SQLSTMT := G_SQLSTMT ||
                     ' SELECT /*+ parallel(x,2) parallel(y,2) */ distinct x.FUND_SKID,0 ACCT_FUND_SKID,y.BRAND_SKID as PROD_SKID,x.PRMTN_SKID,x.FY_DATE_SKID as DATE_SKID,x.BUS_UNIT_SKID,0 FUND_PROD_SKID , 0 FUND_GRP_SKID, x.ACCT_PRMTN_SKID ,x.ACTVY_SKID,x.FY_DATE_SKID ,x.ETL_MGRT_CODE ';
        G_SQLSTMT := G_SQLSTMT || ' from FLTR_LKP_ACTVY_PROD_TMP_' ||
                     IN_REGN || ' x,OPT_PROD_GTIN_BRAND_TMP_4' || IN_REGN ||
                     ' y ';
        G_SQLSTMT := G_SQLSTMT || ' where x.PROD_SKID = y.PROD_SKID';
        G_SQLSTMT := G_SQLSTMT || ' and x.ETL_MGRT_CODE = 1';
        G_SQLSTMT := G_SQLSTMT || ' minus';
        G_SQLSTMT := G_SQLSTMT ||
                     ' select /*+ parallel(a, 2) */ FUND_SKID,ACCT_FUND_SKID,PROD_SKID,PRMTN_SKID,DATE_SKID, BUS_UNIT_SKID, FUND_PROD_SKID, FUND_GRP_SKID, ACCT_PRMTN_SKID, ACTVY_SKID,FY_DATE_SKID,1 from OPT_FILTR_LKP a where TBL_NAME = ''OPT_ACTVY_PROD_FCT''';
        G_SQLSTMT := G_SQLSTMT || ' )A';
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end
     DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Insertion of OPT_FILTR_LKP table started');
         DBMS_OUTPUT.PUT_LINE(G_SQLSTMT);
         DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Insertion of OPT_FILTR_LKP table ended');
    EXECUTE IMMEDIATE G_SQLSTMT;
       DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Inserted Records :'|| SQL%ROWCOUNT||' records');

        COMMIT;

        -- drop table FLTR_LKP_ACTVY_PROD_TMP_APAC purge;
        G_SQLSTMT := 'drop table FLTR_LKP_ACTVY_PROD_TMP_' || IN_REGN ||
                     ' purge';
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Dropping of TMP table started');
         DBMS_OUTPUT.PUT_LINE(G_SQLSTMT);
         DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Dropping of TMP table ended');
        EXECUTE IMMEDIATE G_SQLSTMT;
        -- drop table OPT_PROD_GTIN_BRAND_TMP_APAC purge;
        G_SQLSTMT := 'drop table OPT_PROD_GTIN_BRAND_TMP_4' || IN_REGN ||
                     ' purge';
         DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Dropping of TMP table started');
         DBMS_OUTPUT.PUT_LINE(G_SQLSTMT);
         DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' : Dropping of TMP table ended');
        EXECUTE IMMEDIATE G_SQLSTMT;

        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --sourcing data from OPT_BRAND_BASLN_IFCT
      WHEN IN_FTBL_NAME = 'OPT_BRAND_BASLN_IFCT' THEN

        SQL_MSG := 'Deleting data of OPT_FILTR_LKP sourced from OPT_BRAND_BASLN_IFCT';
        DELETE /*+ PARALLEL(4) */ FROM OPT_FILTR_LKP WHERE TBL_NAME = 'OPT_BRAND_BASLN_IFCT';
        COMMIT;

        SQL_MSG := 'Inserting data of OPT_FILTR_LKP sourced from OPT_BRAND_BASLN_IFCT';
        -- Modified by myra 20090928 in R9, impelement 'parallel with DOP = 2'
        --  /*+ APPEND PARALLEL(2) */
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        INSERT /*+ APPEND */
        INTO OPT_FILTR_LKP
          (TBL_NAME,
           FUND_SKID,
           ACCT_FUND_SKID,
           PROD_SKID,
           PRMTN_SKID,
           DATE_SKID,
           BUS_UNIT_SKID,
           FUND_PROD_SKID,
           FUND_GRP_SKID,
           ACCT_PRMTN_SKID,
           FY_DATE_SKID,
           COMB_PRMTN_DIM_SKID,
           ACTVY_SKID,
           ETL_RUN_ID)
          SELECT 'OPT_BRAND_BASLN_IFCT',
                 FUND_SKID,
                 ACCT_FUND_SKID,
                 PROD_SKID,
                 PRMTN_SKID,
                 DATE_SKID,
                 BUS_UNIT_SKID,
                 FUND_PROD_SKID,
                 FUND_GRP_SKID,
                 ACCT_PRMTN_SKID,
                 FY_DATE_SKID,
                 COMB_PRMTN_DIM_SKID,
                 ACTVY_SKID,
                 V_ETL_RUN_ID
            FROM (SELECT /*+ parallel(a,2) */ DISTINCT BUS_UNIT_SKID,
                                  0 AS FUND_SKID,
                                  0 AS ACCT_FUND_SKID,
                                  PRMTN_ACCT_SKID AS ACCT_PRMTN_SKID,
                                  PROD_SKID,
                                  FY_DATE_SKID,
                                  DUMMY_PRMTN_SKID AS PRMTN_SKID,
                                  FY_DATE_SKID AS DATE_SKID,
                                  0 AS FUND_PROD_SKID,
                                  0 AS FUND_GRP_SKID,
                                  0 AS COMB_PRMTN_DIM_SKID,
                                  0 AS ACTVY_SKID,
                                  0 V_ETL_RUN_ID
                    FROM OPT_BRAND_BASLN_IFCT a) A;
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end

        COMMIT;

        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';

    --Added by Asca on 12/2/2010 for B018 of R11 Begin
      WHEN IN_FTBL_NAME = 'OPT_TSP_LVL_BASLN_IFCT' THEN

        SQL_MSG := 'Deleting data of OPT_FILTR_LKP sourced from OPT_TSP_LVL_BASLN_IFCT';
        DELETE /*+ PARALLEL(4) */ FROM OPT_FILTR_LKP
         WHERE TBL_NAME = 'OPT_TSP_LVL_BASLN_IFCT';
        COMMIT;

        SQL_MSG := 'Inserting data of OPT_FILTR_LKP sourced from OPT_TSP_LVL_BASLN_IFCT';
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - begin
        INSERT /*+ APPEND */
        INTO OPT_FILTR_LKP
          (TBL_NAME,
           FUND_SKID,
           ACCT_FUND_SKID,
           PROD_SKID,
           PRMTN_SKID,
           DATE_SKID,
           BUS_UNIT_SKID,
           FUND_PROD_SKID,
           FUND_GRP_SKID,
           ACCT_PRMTN_SKID,
           FY_DATE_SKID,
           COMB_PRMTN_DIM_SKID,
           ACTVY_SKID,
           ETL_RUN_ID)
          SELECT 'OPT_TSP_LVL_BASLN_IFCT',
                 FUND_SKID,
                 ACCT_FUND_SKID,
                 PROD_SKID,
                 PRMTN_SKID,
                 DATE_SKID,
                 BUS_UNIT_SKID,
                 FUND_PROD_SKID,
                 FUND_GRP_SKID,
                 ACCT_PRMTN_SKID,
                 FY_DATE_SKID,
                 COMB_PRMTN_DIM_SKID,
                 ACTVY_SKID,
                 V_ETL_RUN_ID
            FROM (SELECT /*+ parallel(a, 2) */ DISTINCT BUS_UNIT_SKID,
                                  0 AS FUND_SKID,
                                  0 AS ACCT_FUND_SKID,
                                  PRMTN_ACCT_SKID AS ACCT_PRMTN_SKID,
                                  PROD_SKID,
                                  FY_DATE_SKID,
                                  DUMMY_PRMTN_SKID AS PRMTN_SKID,
                                  FY_DATE_SKID AS DATE_SKID,
                                  0 AS FUND_PROD_SKID,
                                  0 AS FUND_GRP_SKID,
                                  0 AS COMB_PRMTN_DIM_SKID,
                                  0 AS ACTVY_SKID,
                                  0 V_ETL_RUN_ID
                    FROM OPT_TSP_LVL_BASLN_IFCT a) A;
        -- move parallel hint into sub-query by Leo on Jan 26, 2011 - end
        COMMIT;

        R_RTN_VALUE := 0;
        R_RTN_MSG   := 'Success....';
        --Added by Asca on 12/2/2010 for B018 of R11 End

    END CASE;

  END IF;

EXCEPTION
  WHEN DEADLOCK_DETECTED THEN
    R_RTN_VALUE := -1;
    R_RTN_MSG   := 'DeadLock Detected Cannot Continue Retry after Sometime';
    DBMS_OUTPUT.PUT_LINE(R_RTN_MSG);
  WHEN OTHERS THEN
    V_CODE      := SQLCODE;
    V_ERRM      := SUBSTR(SQLERRM, 1, 64);
    R_RTN_MSG   := 'Sql Errors Occured while ' || SQL_MSG ||
                   'The Error code is : ' || V_CODE || ': ' || V_ERRM;
    R_RTN_VALUE := -1;
    DBMS_OUTPUT.PUT_LINE(R_RTN_MSG);
    RETURN;
END;
/

