CREATE OR REPLACE PACKAGE BODY ADWU_OPTIMA_LAEX.Opt_Shpmt_Rpt_Pkg IS
procedure pro_put_line(p_string in varchar2) is
  v_string_length       number;
  v_string_offset       number := 1;
  v_cut_pos             number;
  v_add                 number;
begin
  dbms_output.new_line;
        dbms_output.put_line(to_char(sysdate,'DD-MON-YYYY HH24:MI:SS'));
  v_string_length := length(p_string);

        -- loop thru the string by 255 characters and output each chunk
        while v_string_offset < v_string_length
        loop
    v_cut_pos := nvl(instr(p_string, chr(10), v_string_offset), 0) - v_string_offset;
    if v_cut_pos < 0 or v_cut_pos >= 255 then
      v_cut_pos := 255;
      v_add := 0;
    else
      v_add := 1;
    end if;
          dbms_output.put_line(substr(p_string, v_string_offset, v_cut_pos));
    v_string_offset := v_string_offset + v_cut_pos + v_add;
        end loop;
end;

  PROCEDURE OPT_SHPMT_EXCPT_FCT_LOAD(INREGNCODE VARCHAR2,INTYPE VARCHAR2,
                                     IN_RSTMT_IND IN VARCHAR2 DEFAULT 'N')
  -------------------------------------------------------------------------------------
    -- Procedure    : OPT_SHPMT_EXCPT_FCT_LOAD                                         --
    -- Programmer   : Luke Lu (fei.lu@hp.com)                                          --
    -- Date         : 30-Sep-2007                                                      --
    -- Purpose      : Populate OPT_SHPMT_EXCPT_RPT_FCT data from SFCT table            --
    -------------------------------------------------------------------------------------
    -- Parameters                                                                      --
    -------------------------------------------------------------------------------------
    -- No  Name              Desctiption / Example                      Default        --
    -- --  ----------------- ------------------------------------------ -------------- --
    --  1  inRegnCode          Regn_code   'APAC'
    --  2  inType                   Type   'WLY/MLY'
    -------------------------------------------------------------------------------------
    -- Change History                                                                  --
    -- Date        Programmer        Description                                       --
    -- ----------- ----------------- ------------------------------------------------- --
    -- 30-Sep-2007 Luke Lu           Initial Varsion                                   --
    -- 05-Nov-2008 David Lang     Refresh only few months data for OPT_SHPMT_EXCPT_RPT_FCT
    --                                               based on the newly added paramter --
    -- 11-Aug-2009 BOdhi Wang        Add shipment restatement process, and add column  --
    --                               SHPMT_GEO_CODE.                                   --
    -------------------------------------------------------------------------------------
   IS
    LREGNCODE    VARCHAR2(10);
    LPROCESSNAME VARCHAR2(255);
    LMONTHNUM    VARCHAR2(10);
    LINTYPE      VARCHAR2(10);
    LSTARTDATE DATE;

    v_sql      VARCHAR2(4000) := NULL;
    v_count    number := 0;
    v_i        number := 0;


  BEGIN
    LREGNCODE := TRIM(INREGNCODE);
    LINTYPE := UPPER(TRIM(INTYPE));
    ----------------------------------------------------------------------------
    -- STEP001 Check the input parameter                                      --
    ----------------------------------------------------------------------------
    LPROCESSNAME := 'STEP 001: Check input parameter';
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                         ' STEP 001: Check input parameter (BEGIN) ');
    /*
    IF (LREGNCODE IS NULL) OR
       ((LREGNCODE <> 'APAC') AND (LREGNCODE <> 'EMEA') AND
       (LREGNCODE <> 'NALA')) THEN
      DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           ' STEP 001: Input Parameter RegnCode can not be NULL.');
      RAISE_APPLICATION_ERROR(-20001,
                              'ERROR!!! Input parameter "' || LREGNCODE ||
                              '" is invalid!');
    END IF;
    */
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                         ' STEP 001: Check input parameter (END OK) ');

    IF IN_RSTMT_IND = 'Y' THEN
       -- Shipment restatement
       -- Modified by Bodhi.
       -- Aug 11, 2009
       SELECT count(*) into v_count
         FROM OPT_SHPMT_RSTMT_PLC
        WHERE GEO_CODE<>'HEADER'
          And  RSTMT_IND='Y';

       IF v_count = 0 THEN
          pro_put_line('STEP 002: No GEO_CODE for shipment restatement (END OK)');
          RETURN;
       END IF;

       v_sql :='DELETE FROM OPT_SHPMT_EXCPT_RPT_FCT'||CHR(10)||
               ' WHERE ';

       for SRC in (SELECT GEO_CODE,
                          RSTMT_START_DAY_DATE_SKID,
                          RSTMT_END_DAY_DATE_SKID
                     FROM OPT_SHPMT_RSTMT_PLC
                    WHERE GEO_CODE<>'HEADER'
                      And  RSTMT_IND='Y'
                   )
       loop
           v_i := v_i+1;
           v_sql := v_sql||'SHPMT_GEO_CODE='''||SRC.GEO_CODE||''''||CHR(10)||
                    '   AND DATE_SKID>='||SRC.RSTMT_START_DAY_DATE_SKID||CHR(10)||
                    '   AND DATE_SKID<='||SRC.RSTMT_END_DAY_DATE_SKID||
                    case when v_i <> v_count then
                        CHR(10)||'    OR '
                    else
                        CHR(10)
                    end;
       end loop;

       pro_put_line('Begin to remove old rows...');
       pro_put_line(v_sql);
       execute immediate v_sql;
       pro_put_line('Removed old rows!');
  ELSE -- Normal process.
    ----------------------------------------------------------------------------
    -- STEP002 Truncate partition table                       --
    ----------------------------------------------------------------------------
    /*    LPROCESSNAME := ' STEP 002: Tuncate partitions of OPT_SHPMT_EXCPT_RPT_FCT belogs to region ' ||
                      LREGNCODE;
      DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           ' STEP010 Truncate partition For OPT_SHPMT_EXCPT_RPT_FCT (BEGIN)');
      FOR P_SKID IN (SELECT BUS_UNIT_SKID
                       FROM OPT_BUS_UNIT_FDIM
                     \*                    WHERE REGN_DESC = LREGNCODE*\
                     ) LOOP

        EXECUTE IMMEDIATE 'ALTER TABLE OPT_SHPMT_EXCPT_RPT_FCT TRUNCATE PARTITION P_' ||
                          P_SKID.BUS_UNIT_SKID;

      END LOOP P_SKID;
      DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           ' STEP010 Truncate partition For OPT_SHPMT_EXCPT_RPT_FCT (END OK)');
    */

    --Get the process month num
    SELECT PARM_VAL INTO LMONTHNUM FROM OPT_QRY_EXTRN_PARM_PRC
    WHERE qry_name_code LIKE 'OPT_SHPMT_EXCPT_RPT_SFCT%' ||LINTYPE
         AND PARM_NAME_CODE = 'PRCSS_MTH';


       --If the process month number is greater than 24, which means full refhresh loading (24 months data).
    IF (LMONTHNUM > 24)  THEN
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                         ' STEP002 TRUNCATE table OPT_SHPMT_EXCPT_RPT_FCT (BEGIN)');
        --Truncate the table
        EXECUTE IMMEDIATE ' TRUNCATE TABLE OPT_SHPMT_EXCPT_RPT_FCT reuse storage ';

        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                         ' STEP002 TRUNCATE Table OPT_SHPMT_EXCPT_RPT_FCT (END OK)');
    ELSE
    --If the INTYPE=WLY, refresh 2 month data; if INTYPE=MLY, refresh 4 month data
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                         ' STEP002 Truncate '||LMONTHNUM|| ' months data for table OPT_SHPMT_EXCPT_RPT_FCT (BEGIN)');
    --Get the start date of refresh data
        SELECT CASE(TO_NUMBER(TO_CHAR(LAST_DAY(SYSDATE), 'MM')) -
                                   TO_NUMBER(TO_CHAR(LAST_DAY(SYSDATE - 6), 'MM')))
                        WHEN 0 THEN
                              TRUNC(ADD_MONTHS(TRUNC(SYSDATE, 'MM'),-(LMONTHNUM -1)))
                        ELSE
                               TRUNC(ADD_MONTHS(TRUNC(SYSDATE, 'MM'),-LMONTHNUM)) END
        INTO LSTARTDATE
        FROM DUAL;
    --Delete the data to be refreshed
       DELETE  FROM OPT_SHPMT_EXCPT_RPT_FCT FCT
       WHERE  FCT.DATE_SKID >=
         (SELECT C.CAL_MASTR_SKID FROM OPT_CAL_MASTR_MV01 C
           WHERE C.DAY_DATE=LSTARTDATE);

     DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                         ' STEP002 Truncate '||LMONTHNUM|| ' months data for table OPT_SHPMT_EXCPT_RPT_FCT (END OK)');

     END IF;
   END IF;

   ----------------------------------------------------------------------------
   -- STEP003 Populate FCT table from SFCT table                              --
   ----------------------------------------------------------------------------
   -- Add column SHPMT_GEO_CODE
   -- Bodhi
   -- Aug 11, 2009
   LPROCESSNAME := ' STEP 003: Populate data for OPT_SHPMT_EXCPT_RPT_FCT of region ' ||
                    LREGNCODE;
   DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                         ' STEP 003: Populate data for OPT_SHPMT_EXCPT_RPT_FCT of region ' ||
                         LREGNCODE || ' (BEGIN)');
   INSERT /*+ APPEND */
    INTO OPT_SHPMT_EXCPT_RPT_FCT
      (DATA_TYPE_CODE,
       ISO_CRNCY_CODE,
       BUS_UNIT_SKID,
       DATE_SKID,
       GTIN_CODE,
       SAP_SALES_ORG_CODE,
       SHIPT_CODE,
       VOL_SU_AMT,
       GIV_AMT,
       NIV_AMT,
       REGN_CODE,
       SHPMT_GEO_CODE)
      (SELECT DECODE(SRCE.DATA_TYPE_CODE,
                     'BS',
                     'Direct',
                     'ID',
                     'Indirect',
                     'Other') AS DATA_TYPE_CODE,
              SRCE.ISO_CRNCY_CODE,
              B.BUS_UNIT_SKID,
              NVL(C.CAL_MASTR_SKID, 0) AS CAL_MASTR_SKID,
              SRCE.GTIN_CODE,
              SRCE.SAP_SALES_ORG_CODE,
              SRCE.SHIPT_CODE,
              SUM(SRCE.VOL_SU_AMT) AS VOL_SU_AMT,
              SUM(SRCE.GIV_AMT) AS GIV_AMT,
              SUM(SRCE.NIV_AMT) AS NIV_AMT,
              SRCE.REGN_CODE,
              SRCE.SHPMT_GEO_CODE
         FROM OPT_SHPMT_EXCPT_RPT_SFCT SRCE,
              OPT_BUS_UNIT_FDIM        B,
              OPT_CAL_MASTR_MV01       C
        WHERE B.BUS_UNIT_NAME = SRCE.CNTRY_DESC
          AND SRCE.SHPMT_DATE = C.DAY_DATE
       /* WHERE REGN_CODE = LREGNCODE*/
        GROUP BY SRCE.DATA_TYPE_CODE,
                 SRCE.ISO_CRNCY_CODE,
                 B.BUS_UNIT_SKID,
                 C.CAL_MASTR_SKID,
                 SRCE.GTIN_CODE,
                 SRCE.SAP_SALES_ORG_CODE,
                 SRCE.SHIPT_CODE,
                 SRCE.REGN_CODE,
                 SRCE.SHPMT_GEO_CODE
       HAVING NOT 0 = ALL(SUM(SRCE.VOL_SU_AMT), SUM(SRCE.GIV_AMT), SUM(SRCE.NIV_AMT)));


   COMMIT;
   DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                         ' STEP 003: Populate data for OPT_SHPMT_EXCPT_RPT_FCT of region ' ||
                         LREGNCODE || ' (END OK)');

   --------------------------------------------------------------------------------------------------------------------------
   -- STEP 004: Exeception handling
   --------------------------------------------------------------------------------------------------------------------------

  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           ' Unexpected ORACLE Error');
      DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           ' SQL Exit Code      :' ||
                           SUBSTR(SQLCODE, 1, 244));
      DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           ' SQL Error Message  :' || SQLERRM);
      DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           LPROCESSNAME || '(FAILED)');
      ROLLBACK;

  END OPT_SHPMT_EXCPT_FCT_LOAD;

  PROCEDURE OPT_SHPMT_RPT_AGG(INREGNCODE IN VARCHAR2,
                              INTYPE VARCHAR2,
                              IN_CTRLM_ORD_ID IN VARCHAR2,
                              IN_RSTMT_IND IN VARCHAR2 DEFAULT 'N'
                             ) IS
    --------------------------------------------------------------------------------------------------------------------------
    -- Function     : OPT_SHPMT_RPT_AGG
    -- Programmer   : Hill Pan (zhi-shan.pan@hp.com)
    -- Date         : 30-Sep-2007
    -- Purpose      : This procedure is used to populate data into OPT_SHPMT_RPT_BFCT.
    --------------------------------------------------------------------------------------------------------------------------
    -- Parameters
    --------------------------------------------------------------------------------------------------------------------------
    -- No  Name              Desctiption   /   Example                          Default
    --------------------------------------------------------------------------------------------------------------------------
    -- 1  inRegnCode       Region Code   /   'APAC'
    -- 2  inType                   Type   'WLY/MLY'
    --------------------------------------------------------------------------------------------------------------------------
    -- Change History
    -- Date        Programmer        Description
    --------------------------------------------------------------------------------------------------------------------------
    -- 30-Sep-2007 Hill Pan      Initial Varsion
    -- 18-Feb-2008 Hill Pan      Add FY_DATE_SKID column.
    -- 06-Mar-2008 Hill Pan      Indentical Currency was added to shipment report
    -- 06-Jul-2008 Myra          Using SHPMT_TO_FROM_CODE other than SHPMT_TO_CODE for shipment aggregation
    -- 06-Nov-2008 David Lang    Refresh only few months data for OPT_SHPMT_RPT_BFCT
    --                                               based on the newly added paramter INTYPE
    -- 11-Aug-2009 BOdhi Wang    Add shipment restatement process, and add column
    --                           SHPMT_GEO_CODE.
    -- 29-Aug-2009 Bodhi Wang    Use partition exchange to load OPT_SHPMT_RPT_BFCT--
    -- 31-Aug-2009 Bodhi Wang    Insert aggregated data into target table OPT_SHPMT_RPT_BFCT directly --
    --                           after truncate speceified subpartitions and create missing ones.
    -- 10-Sep-2009 Bodhi Wang    Modify the process to cleanup old partitions.
    -- 28-Sep-2009 Bodhi Wang    Skip loading process if there's no data in OPT_SHPMT_SFCT
    -- 16-Sep-2010 Leo Zeng      Add a join condition when insert data into bfct
    --------------------------------------------------------------------------------------------------------------------------

    --LREGNCODE    VARCHAR2(20);
    --LPROCESSNAME VARCHAR2(255);
    --LMONTHNUM    VARCHAR2(10);
    --LINTYPE             VARCHAR2(10);
    --LSTARTDATE    DATE;

    --v_sql          VARCHAR2(4000) := NULL;
    --v_count        number := 0;
    --v_i            number := 0;
    V_PAST_MTH_NUM NUMBER := 0;
    --V_MTH_SKID     NUMBER := 0;
    V_MTH_QRY_NAME VARCHAR2(50) :='OPT_SHPMT_FCT_'||INTYPE;
    V_DCMPD_PROD_IND VARCHAR2(50);
    V_TRGT_TBL     USER_TABLES.TABLE_NAME%TYPE := 'OPT_SHPMT_RPT_BFCT';
    V_CAL_SKID     NUMBER;

  BEGIN
    --LREGNCODE := TRIM(INREGNCODE);
    --LINTYPE := TRIM(INTYPE);

    -- If there's no data in OPT_SHPMT_SFCT, skip this loading process
    -- Bodhi
    -- Sep 28, 2009
    PRO_PUT_LINE('Check if there is data in opt_shpmt_sfct:>>>>');
    BEGIN
      SELECT CAL_SKID INTO V_CAL_SKID
        FROM OPT_SHPMT_SFCT WHERE ROWNUM<2;
    EXCEPTION
      WHEN no_data_found THEN
      pro_put_line('No data found in opt_shpmt_sfct! This process is skipped!');
      PRO_PUT_LINE('Check if there is data in opt_shpmt_sfct:<<<<');
      return;
    END;
    PRO_PUT_LINE('Check if there is data in opt_shpmt_sfct:<<<<');


    -- Get configured DCMPD_PROD_IND
    PRO_PUT_LINE('Fetch parameter DCMPD_PROD_IND:>>>>');
    SELECT UPPER(TRIM(PARM_VAL)) INTO V_DCMPD_PROD_IND
      FROM OPT_QRY_EXTRN_PARM_PRC
     WHERE QRY_NAME_CODE = 'DCMPD_PROD_IND'
       AND PARM_NAME_CODE='DCMPD_PROD_IND';
    PRO_PUT_LINE('DCMPD_PROD_IND['||V_DCMPD_PROD_IND||']');
    PRO_PUT_LINE('Fetch parameter DCMPD_PROD_IND:<<<<');

    PRO_PUT_LINE('truncate subpartitions of '|| V_TRGT_TBL||':>>>>');
    -- Get configured PRCSS_MTH
    SELECT TO_NUMBER(PARM_VAL) INTO V_PAST_MTH_NUM
      FROM OPT_QRY_EXTRN_PARM_PRC
     WHERE QRY_NAME_CODE = V_MTH_QRY_NAME
       AND PARM_NAME_CODE='PRCSS_MTH';

    -- Create missing partitions and subpartitions for target table.
    PRO_PUT_LINE('Create missing partitions and subpartitions of '||V_TRGT_TBL||':>>>>');
    OPT_SHPMT_LOAD_PKG.PRO_CREAT_MSNG_PRTTN(V_TRGT_TBL,
                                            V_PAST_MTH_NUM
                                           );
    PRO_PUT_LINE('Create missing partitions and subpartitions of '||V_TRGT_TBL||':<<<<');

    PRO_PUT_LINE('PAST_MTH_NUM['||V_PAST_MTH_NUM||']');
    -- Truncate subpartition of target table opt_shpmt_rpt_bfct.
    OPT_SHPMT_LOAD_PKG.PRO_TRNCT_PTTN(V_TRGT_TBL,
                                      IN_RSTMT_IND,
                                      V_PAST_MTH_NUM
                                     );
    PRO_PUT_LINE('truncate subpartitions of '||V_TRGT_TBL||':<<<<');

    -- Insert shipment aggregated data.
    PRO_PUT_LINE('Aggregate data from OPT_SHPMT_SFCT to '||V_TRGT_TBL||':>>>>');

    IF V_DCMPD_PROD_IND <> 'Y' THEN
       INSERT /*+ APPEND */
       INTO OPT_SHPMT_RPT_BFCT
         (DATA_TYPE_CODE,
          BUS_UNIT_SKID,
          PROD_SKID,
          ACCT_SKID,
          SHPMT_DATE_SKID,
          VOL_BUOM_AMT,
          VOL_SU_AMT,
          GIV_AMT,
          NIV_AMT,
          REGN_CODE,
          ISO_CRNCY_CODE,
          FY_DATE_SKID,
          CAL_SKID,
          SHPMT_GEO_CODE,
          CMPLX_DCMPD_DESC)
         SELECT DECODE(FACT.DATA_TYPE_CODE,
                       'BS',
                       'Direct',
                       'ID',
                       'Indirect',
                       'Other') AS DATA_TYPE_CODE,
                FACT.BUS_UNIT_SKID,
                FACT.PROD_SKID PROD_SKID,
                ACCT.ACCT_SKID,
                FACT.SHPMT_DATE_SKID,
                SUM(FACT.VOL_BUOM_AMT) AS VOL_BUOM_AMT,
                SUM(FACT.VOL_SU_AMT) AS VOL_SU_AMT,
                SUM(FACT.GIV_AMT) AS GIV_AMT,
                SUM(FACT.NIV_AMT) AS NIV_AMT,
                INREGNCODE AS REGN_CODE,
                FACT.ISO_CRNCY_CODE,
                CAL.FISC_YR_SKID AS FY_DATE_SKID,
                FACT.CAL_SKID,
                FACT.SHPMT_GEO_CODE,
                'COMPLEX' AS CMPLX_DCMPD_DESC
           FROM OPT_CAL_MASTR_MV01 CAL,
                OPT_SHPMT_SFCT FACT,
                OPT_ACCT_FDIM ACCT
          WHERE FACT.SHPMT_TO_FROM_CODE = ACCT.NAME
            --added by Leo on Sep 16, 2010, start
            AND FACT.BUS_UNIT_SKID = ACCT.BUS_UNIT_SKID
            --added by Leo on Sep 16, 2010, end
            AND FACT.SHPMT_DATE = CAL.DAY_DATE
            AND FACT.BUS_UNIT_SKID <> 0
            AND FACT.PROD_SKID <> 0
            AND FACT.ACCT_SKID <> 0
       GROUP BY FACT.DATA_TYPE_CODE,
                FACT.BUS_UNIT_SKID,
                FACT.PROD_SKID,
                ACCT.ACCT_SKID,
                FACT.ISO_CRNCY_CODE,
                FACT.SHPMT_DATE_SKID,
                CAL.FISC_YR_SKID,
                FACT.CAL_SKID,
                FACT.SHPMT_GEO_CODE
         HAVING NOT 0 = ALL(SUM(FACT.VOL_BUOM_AMT),
                            SUM(FACT.VOL_SU_AMT),
                            SUM(FACT.GIV_AMT),
                            SUM(FACT.NIV_AMT));
    ELSE
       INSERT /*+ APPEND */
       INTO OPT_SHPMT_RPT_BFCT
         (DATA_TYPE_CODE,
          BUS_UNIT_SKID,
          PROD_SKID,
          ACCT_SKID,
          SHPMT_DATE_SKID,
          VOL_BUOM_AMT,
          VOL_SU_AMT,
          GIV_AMT,
          NIV_AMT,
          REGN_CODE,
          ISO_CRNCY_CODE,
          FY_DATE_SKID,
          CAL_SKID,
          SHPMT_GEO_CODE,
          CMPLX_DCMPD_DESC)
         SELECT DECODE(FACT.DATA_TYPE_CODE,
                       'BS',
                       'Direct',
                       'ID',
                       'Indirect',
                       'Other') AS DATA_TYPE_CODE,
                FACT.BUS_UNIT_SKID,
                FACT.PROD_SKID PROD_SKID,
                ACCT.ACCT_SKID,
                FACT.SHPMT_DATE_SKID,
                SUM(FACT.VOL_BUOM_AMT) AS VOL_BUOM_AMT,
                SUM(FACT.VOL_SU_AMT) AS VOL_SU_AMT,
                SUM(FACT.GIV_AMT) AS GIV_AMT,
                SUM(FACT.NIV_AMT) AS NIV_AMT,
                INREGNCODE AS REGN_CODE,
                FACT.ISO_CRNCY_CODE,
                CAL.FISC_YR_SKID AS FY_DATE_SKID,
                FACT.CAL_SKID,
                FACT.SHPMT_GEO_CODE,
                'COMPLEX' AS CMPLX_DCMPD_DESC
           FROM OPT_CAL_MASTR_MV01 CAL,
                OPT_SHPMT_SFCT FACT,
                OPT_ACCT_FDIM ACCT
          WHERE FACT.SHPMT_TO_FROM_CODE = ACCT.NAME
            --added by Leo on Sep 16, 2010, start
            AND FACT.BUS_UNIT_SKID = ACCT.BUS_UNIT_SKID
            --added by Leo on Sep 16, 2010, end
            AND FACT.SHPMT_DATE = CAL.DAY_DATE
            AND FACT.BUS_UNIT_SKID <> 0
            AND FACT.PROD_SKID <> 0
            AND FACT.ACCT_SKID <> 0
       GROUP BY FACT.DATA_TYPE_CODE,
                FACT.BUS_UNIT_SKID,
                FACT.PROD_SKID,
                ACCT.ACCT_SKID,
                FACT.ISO_CRNCY_CODE,
                FACT.SHPMT_DATE_SKID,
                CAL.FISC_YR_SKID,
                FACT.CAL_SKID,
                FACT.SHPMT_GEO_CODE
         HAVING NOT 0 = ALL(SUM(FACT.VOL_BUOM_AMT),
                            SUM(FACT.VOL_SU_AMT),
                            SUM(FACT.GIV_AMT),
                            SUM(FACT.NIV_AMT))
      UNION ALL
         SELECT DECODE(FACT.DATA_TYPE_CODE,
                       'BS',
                       'Direct',
                       'ID',
                       'Indirect',
                       'Other') AS DATA_TYPE_CODE,
                FACT.BUS_UNIT_SKID,
                FACT.COMP_PROD_SKID PROD_SKID,
                ACCT.ACCT_SKID,
                FACT.SHPMT_DATE_SKID,
                SUM(FACT.VOL_BUOM_AMT) AS VOL_BUOM_AMT,
                SUM(FACT.VOL_SU_AMT) AS VOL_SU_AMT,
                SUM(FACT.GIV_AMT) AS GIV_AMT,
                SUM(FACT.NIV_AMT) AS NIV_AMT,
                INREGNCODE AS REGN_CODE,
                FACT.ISO_CRNCY_CODE,
                CAL.FISC_YR_SKID AS FY_DATE_SKID,
                FACT.CAL_SKID,
                FACT.SHPMT_GEO_CODE,
                'DECOMPOSED' AS CMPLX_DCMPD_DESC
           FROM OPT_CAL_MASTR_MV01 CAL,
                OPT_SHPMT_SFCT FACT,
                OPT_ACCT_FDIM ACCT
          WHERE FACT.SHPMT_TO_FROM_CODE = ACCT.NAME
            --added by Leo on Sep 16, 2010, start
            AND FACT.BUS_UNIT_SKID = ACCT.BUS_UNIT_SKID
            --added by Leo on Sep 16, 2010, end
            AND FACT.SHPMT_DATE = CAL.DAY_DATE
            AND FACT.BUS_UNIT_SKID <> 0
            AND FACT.PROD_SKID <> 0
            AND FACT.ACCT_SKID <> 0
            AND FACT.COMP_PROD_SKID <> 0
       GROUP BY FACT.DATA_TYPE_CODE,
                FACT.BUS_UNIT_SKID,
                FACT.COMP_PROD_SKID,
                ACCT.ACCT_SKID,
                FACT.ISO_CRNCY_CODE,
                FACT.SHPMT_DATE_SKID,
                CAL.FISC_YR_SKID,
                FACT.CAL_SKID,
                FACT.SHPMT_GEO_CODE
         HAVING NOT 0 = ALL(SUM(FACT.VOL_BUOM_AMT),
                            SUM(FACT.VOL_SU_AMT),
                            SUM(FACT.GIV_AMT),
                            SUM(FACT.NIV_AMT));
    END IF;

    COMMIT;
    PRO_PUT_LINE('Aggregate data from OPT_SHPMT_SFCT to '||V_TRGT_TBL||':<<<<');

     --Drop old partitions
    PRO_PUT_LINE('Drop old partitions from '||V_TRGT_TBL||':>>>>');
    OPT_PKG_PRTTN_UTIL.pro_cleanup_old_prttn(V_TRGT_TBL,
                                             NULL,  -- use NULL. Modified by Bodhi, 2009.9.10
                                             'M');
    PRO_PUT_LINE('Drop old partitions from '||V_TRGT_TBL||':<<<<');

    PRO_PUT_LINE('Rebuild indexes for '||
                                    V_TRGT_TBL||':>>>>');
    OPT_PKG_PRTTN_UTIL.pro_rebuild_indexes(V_TRGT_TBL,IN_CTRLM_ORD_ID);
    PRO_PUT_LINE('Rebuild indexes for '||V_TRGT_TBL||':<<<<');

  EXCEPTION
    WHEN OTHERS THEN
      PRO_PUT_LINE('Unexpected ORACLE Error');
      PRO_PUT_LINE('SQL Exit Code      :' ||SUBSTR(SQLCODE, 1, 244));
      PRO_PUT_LINE(' SQL Error Message  :' || SQLERRM);
      PRO_PUT_LINE('Load '||V_TRGT_TBL||'(FAILED)');
      ROLLBACK;
  END OPT_SHPMT_RPT_AGG;

  PROCEDURE OPT_ACCT_HIER_BUILD(IN_REGN VARCHAR2,V_I_REGN VARCHAR2 DEFAULT NULL)--SOX IMPLEMENTATION PROJECT
  ------------------------------------------------------------------------------------
    -- Procedure    : OPT_ACCT_HIER_BUILD                                           --
    -- Originator   : Gao Yongwei (yongwei.gao@hp.com)                                --
    -- Author       : Gao Yongwei (yongwei.gao@hp.com)                                --
    -- Date         : 28-Sep-2007                                                     --
    -- Purpose      : loading OPT_ACCT_HIER_FDIM table process                             --
    ------------------------------------------------------------------------------------
    -- Parameters
    ------------------------------------------------------------------------------------
    -- No  Name              Desctiption          /   Example                         --
    ------------------------------------------------------------------------------------
    -- 1  in_Regn              Region Code        /   'APAC'                          --
    ------------------------------------------------------------------------------------
    -- Change History                                                                 --
    -- Date        Programmer        Description                                      --
    -- ----------- ----------------- ---------------------------------------------------
    -- 28-Sep-2007 Gao Yongwei      Initial Version                                   --
    -- 25-Feb-2009 Wang Faqun       Modify extraction sql and add exchange parttion   --
    --                              process
    ------------------------------------------------------------------------------------
   IS

    --Define parameters for exception handling
    DEADLOCK_DETECTED EXCEPTION;
    PRAGMA EXCEPTION_INIT(DEADLOCK_DETECTED, -60);
    V_CODE       NUMBER;
    V_ERRM       VARCHAR2(128);
    SQL_MSG      VARCHAR2(200) := 'Running Procedure OPT_ACCT_HIER_BUILD.';
    V_ETL_RUN_ID NUMBER(15);
    G_SQLSTMT    VARCHAR2(32767);
    V_REGN       VARCHAR2(10) := IN_REGN;
    L_RTN_MSG    VARCHAR2(255);
  BEGIN

    --Initinal variable to achieve ETL_RUN_ID
    SELECT OPT_ETL_RUN_ID_SEQ.NEXTVAL INTO V_ETL_RUN_ID FROM DUAL;
    --Populate ACCT_HIER_FDIM Sql

    G_SQLSTMT := '(
       ACCT_SKID,
       ACCT_NAME,
       ACCT_ID,
       ACCT_TYPE,
       ACCT_TYPE_DESC,
       ACCT_LVL,
       REGN_CODE,
       ETL_RUN_ID,
       BUS_UNIT_SKID,
       ACCT_1_SKID,
       ACCT_2_SKID,
       ACCT_3_SKID,
       ACCT_4_SKID,
       ACCT_5_SKID,
       ACCT_6_SKID,
       ACCT_7_SKID,
       ACCT_8_SKID,
       ACCT_1_NAME,
       ACCT_2_NAME,
       ACCT_3_NAME,
       ACCT_4_NAME,
       ACCT_5_NAME,
       ACCT_6_NAME,
       ACCT_7_NAME,
       ACCT_8_NAME,
       ACCT_1_ID,
       ACCT_2_ID,
       ACCT_3_ID,
       ACCT_4_ID,
       ACCT_5_ID,
       ACCT_6_ID,
       ACCT_7_ID,
       ACCT_8_ID,
       ACCT_1_TYPE,
       ACCT_2_TYPE,
       ACCT_3_TYPE,
       ACCT_4_TYPE,
       ACCT_5_TYPE,
       ACCT_6_TYPE,
       ACCT_7_TYPE,
       ACCT_8_TYPE,
       ACCT_1_TYPE_DESC,
       ACCT_2_TYPE_DESC,
       ACCT_3_TYPE_DESC,
       ACCT_4_TYPE_DESC,
       ACCT_5_TYPE_DESC,
       ACCT_6_TYPE_DESC,
       ACCT_7_TYPE_DESC,
       ACCT_8_TYPE_DESC,
       ACCT_1_LONG_NAME,
       ACCT_2_LONG_NAME,
       ACCT_3_LONG_NAME,
       ACCT_4_LONG_NAME,
       ACCT_5_LONG_NAME,
       ACCT_6_LONG_NAME,
       ACCT_7_LONG_NAME,
       ACCT_8_LONG_NAME,
       ACCT_1_LONG_NAME_TYPE,
       ACCT_2_LONG_NAME_TYPE,
       ACCT_3_LONG_NAME_TYPE,
       ACCT_4_LONG_NAME_TYPE,
       ACCT_5_LONG_NAME_TYPE,
       ACCT_6_LONG_NAME_TYPE,
       ACCT_7_LONG_NAME_TYPE,
       ACCT_8_LONG_NAME_TYPE,
           ACCT_1_GRP_NAME,
       ACCT_2_GRP_NAME,
       ACCT_3_GRP_NAME,
       ACCT_4_GRP_NAME,
       ACCT_5_GRP_NAME,
       ACCT_6_GRP_NAME,
       ACCT_7_GRP_NAME,
       ACCT_8_GRP_NAME,
       SHPMT_START_DATE,
       SHPMT_END_DATE,
     SHPMT_START_DATE_SKID,
       SHPMT_END_DATE_SKID
     ) SELECT
           ACCT_SKID,
       ACCT_NAME,
       ACCT_ID,
       ACCT_TYPE,
       ACCT_TYPE_DESC,
       ACCT_LVL,'||
       ''''||V_REGN||''' AS REGN_CODE,'||
       V_ETL_RUN_ID||' AS ETL_RUN_ID,'||
       'BUS_UNIT_SKID,
       ACCT_1_SKID,
       ACCT_2_SKID,
       ACCT_3_SKID,
       ACCT_4_SKID,
       ACCT_5_SKID,
       ACCT_6_SKID,
       ACCT_7_SKID,
       ACCT_8_SKID,
       ACCT_1_NAME,
       ACCT_2_NAME,
       ACCT_3_NAME,
       ACCT_4_NAME,
       ACCT_5_NAME,
       ACCT_6_NAME,
       ACCT_7_NAME,
       ACCT_8_NAME,
       ACCT_1_ID,
       ACCT_2_ID,
       ACCT_3_ID,
       ACCT_4_ID,
       ACCT_5_ID,
       ACCT_6_ID,
       ACCT_7_ID,
       ACCT_8_ID,
       ACCT_1_TYPE,
       ACCT_2_TYPE,
       ACCT_3_TYPE,
       ACCT_4_TYPE,
       ACCT_5_TYPE,
       ACCT_6_TYPE,
       ACCT_7_TYPE,
       ACCT_8_TYPE,
       ACCT_1_TYPE_DESC,
       ACCT_2_TYPE_DESC,
       ACCT_3_TYPE_DESC,
       ACCT_4_TYPE_DESC,
       ACCT_5_TYPE_DESC,
       ACCT_6_TYPE_DESC,
       ACCT_7_TYPE_DESC,
       ACCT_8_TYPE_DESC,
       ACCT_1_LONG_NAME,
       ACCT_2_LONG_NAME,
       ACCT_3_LONG_NAME,
       ACCT_4_LONG_NAME,
       ACCT_5_LONG_NAME,
       ACCT_6_LONG_NAME,
       ACCT_7_LONG_NAME,
       ACCT_8_LONG_NAME,
       ACCT_1_LONG_NAME_TYPE,
       ACCT_2_LONG_NAME_TYPE,
       ACCT_3_LONG_NAME_TYPE,
       ACCT_4_LONG_NAME_TYPE,
       ACCT_5_LONG_NAME_TYPE,
       ACCT_6_LONG_NAME_TYPE,
       ACCT_7_LONG_NAME_TYPE,
       ACCT_8_LONG_NAME_TYPE,
           ACCT_1_GRP_NAME,
       ACCT_2_GRP_NAME,
       ACCT_3_GRP_NAME,
       ACCT_4_GRP_NAME,
       ACCT_5_GRP_NAME,
       ACCT_6_GRP_NAME,
       ACCT_7_GRP_NAME,
       ACCT_8_GRP_NAME,
       SHPMT_START_DATE,
       SHPMT_END_DATE,
     SHPMT_START_DATE_SKID,
       SHPMT_END_DATE_SKID
       FROM OPT_ACCT_HIER_FDIM_D_VW';
    Opt_Prttn_Load_Pkg.load_by_prttn_exchg_with_sql('OPT_ACCT_HIER_FDIM',G_SQLSTMT,V_I_REGN);--SOX IMPLEMENTATION PROJECT

    --EXECUTE IMMEDIATE G_SQLSTMT;
    --COMMIT;
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                         ' Populate data for OPT_ACCT_HIER_FDIM of region ' ||
                         V_REGN || ' (END OK)');

  EXCEPTION
    WHEN DEADLOCK_DETECTED THEN
      L_RTN_MSG := 'DeadLock Detected Cannot Continue Retry after Sometime';
      DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           ' Populate data for OPT_ACCT_HIER_FDIM ' ||
                           L_RTN_MSG);
    WHEN OTHERS THEN
      V_CODE    := SQLCODE;
      V_ERRM    := SUBSTR(SQLERRM, 1, 128);
      L_RTN_MSG := 'Sql Errors Occured while ' || SQL_MSG ||
                   'The Error code is : ' || V_CODE || ': ' || V_ERRM;
      DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           ' Populate data for OPT_ACCT_HIER_FDIM ' ||
                           L_RTN_MSG);
      RETURN;

  END OPT_ACCT_HIER_BUILD;

 PROCEDURE OPT_REFRESH_MV(IN_MV_NAME VARCHAR2,V_REGN IN VARCHAR2 DEFAULT NULL)--SOX IMPLEMENTATION PROJECT
  ------------------------------------------------------------------------------------
    -- Procedure    : OPT_REFRESH_MV                                                  --
    -- Originator   : Gao Yongwei (yongwei.gao@hp.com)                                --
    -- Author       : Gao Yongwei (yongwei.gao@hp.com)                                --
    -- Date         : 30-Sep-2007                                                     --
    -- Purpose      : Refresh Optima Shipment Report Related Materialized Views       --
    ------------------------------------------------------------------------------------
    -- Parameters
    ------------------------------------------------------------------------------------
    -- No  Name              Desctiption          /   Example                         --
    ------------------------------------------------------------------------------------
    -- 1  in_MV_Name        Materialized View Name /   'OPT_CAL_MASTR_MV01'           --
    ------------------------------------------------------------------------------------
    -- Change History                                                                 --
    -- Date        Programmer        Description                                      --
    -- ----------- ----------------- ---------------------------------------------------
    -- 30-Sep-2007 Gao Yongwei      Initial Version                                   --
    -- 05-Mar-2008 Pan Zhi-Shan     Add construction of OPT_PROD_PRICE_MV             --
    -- 10-Feb-2009 Lin Tong         Add construction of OPT_SHPMT_ACTL_FRCST_MV01     --
    ------------------------------------------------------------------------------------
   IS
    --Define parameters for exception handling
    DEADLOCK_DETECTED EXCEPTION;
    PRAGMA EXCEPTION_INIT(DEADLOCK_DETECTED, -60);
    --V_SQL      varchar2(32767);
    V_CODE     NUMBER;
    V_ERRM     VARCHAR2(128);
    SQL_MSG    VARCHAR2(128) := 'Running Procedure OPT_REFRESH_MV.';
    V_MV_NAME  VARCHAR2(100);
    P_USER     VARCHAR2(30);
    L_RTN_MSG  VARCHAR2(255);
    --G_SQLSTMT  VARCHAR2(1000);
    LMVCOUNT   NUMBER;
    TEMPCOUNT  NUMBER;
    LCURRMONTH VARCHAR2(20);
    PP_USER    VARCHAR2(30);
    register_mv_error EXCEPTION;
  BEGIN

    OPT_AX_TBLSPACE(V_REGN,V_TBLSPACE_NAME,V_INDEX);--SOX IMPLEMENTATION PROJECT

    V_MV_NAME := IN_MV_NAME;
    SELECT USERNAME INTO P_USER FROM USER_USERS WHERE ROWNUM = 1;

    IF V_MV_NAME = 'OPT_PROD_PRICE_MV' THEN
      ---If MV OPT_PROD_PRICE_MV already exist then drop it.
      SELECT COUNT(1)
        INTO LMVCOUNT
        FROM USER_OBJECTS
       WHERE OBJECT_NAME = 'OPT_PROD_PRICE_MV'
         AND OBJECT_TYPE = 'MATERIALIZED VIEW';
      IF LMVCOUNT > 0 THEN
        EXECUTE IMMEDIATE 'DROP MATERIALIZED VIEW OPT_PROD_PRICE_MV';
        DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                             ' OPT_PROD_PRICE_MV was dropped successfully.');
      END IF;
      --Retrieve current month name from cal_mastr_dim
      SELECT MTH_NAME
        INTO LCURRMONTH
        FROM CAL_MASTR_DIM
       WHERE DAY_DATE = TRUNC(SYSDATE)
         AND ROWNUM = 1;
      DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           ' Current Month Name is: ' || LCURRMONTH);

     -- Parse select statement used by MV, to avoid oracle error code : -44201
      SELECT COUNT(*)
      INTO TEMPCOUNT
      FROM
      (SELECT  DISTINCT PROD.BUS_UNIT_SKID,
                        PROD.PROD_SKID,
                        PROD.CREAT_DATE,
                        PROD.PROD_DESC,
                        PROD.PROD_NAME,
                        PROD.PROD_LVL_DESC,
                        PROD.PROD_CODE,
                        PROD.PROD_ACTIV_IND,
                        PROD.ACTIV_IND,
                        PROD.SU_FACTR,
                        PROD.GIV_CONV_FACTR,
                        PRICE.DEFLT_COST_PER_UNIT_NUM,
                        PRICE.NAME_DESC,
                        PROD.BUOM_IND,
                        PROD.BUOM_CU_CONV_FACTR
          FROM OPT_PROD_FDIM PROD,
               OPT_PRICE_FDIM PRICE
         WHERE PRICE.PROD_SKID(+) = PROD.PROD_SKID
           AND PRICE.MTH_NAME(+) = LCURRMONTH
           AND PROD.BUOM_IND = 'Y'
           AND ROWNUM = 1);


      --Create materialized view OPT_PROD_PRICE_MV
      -- Tablespace changed to OPTIMA01 for Exadata
      -- Replaced optima01 by V_TBLSPACE_NAME
      EXECUTE IMMEDIATE '
        CREATE MATERIALIZED VIEW OPT_PROD_PRICE_MV
        '||V_TBLSPACE_NAME||'
        PARALLEL 2
        COMPRESS
        PCTFREE 0
        NOLOGGING
        BUILD IMMEDIATE USING NO INDEX
        REFRESH FORCE ON DEMAND
        ENABLE QUERY REWRITE
        AS
        SELECT DISTINCT PROD.BUS_UNIT_SKID,
                        PROD.PROD_SKID,
                        PROD.CREAT_DATE,
                        PROD.PROD_DESC,
                        PROD.PROD_NAME,
                        PROD.PROD_LVL_DESC,
                        PROD.PROD_CODE,
                        PROD.PROD_ACTIV_IND,
                        PROD.ACTIV_IND,
                        PROD.SU_FACTR,
                        PROD.GIV_CONV_FACTR,
                        PRICE.DEFLT_COST_PER_UNIT_NUM,
                        PRICE.NAME_DESC,
                        PROD.BUOM_IND,
                        PROD.BUOM_CU_CONV_FACTR
          FROM OPT_PROD_FDIM PROD,
               OPT_PRICE_FDIM PRICE
         WHERE PRICE.PROD_SKID(+) = PROD.PROD_SKID
           AND PRICE.MTH_NAME(+) =''' || LCURRMONTH || '''
           AND PROD.BUOM_IND = ''Y''';
      DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           ' OPT_PROD_PRICE_MV was created successfully.');

      --grant proper privilege to roles
      SELECT UPPER(SUBSTR(USERNAME, 1, INSTR(USERNAME, '_') - 1))
        INTO PP_USER
        FROM USER_USERS
       WHERE ROWNUM = 1;
      EXECUTE IMMEDIATE 'grant select, insert, update, delete on OPT_PROD_PRICE_MV to ' ||
                        PP_USER || '_OPTIMA_ETL_UPDATE';
      EXECUTE IMMEDIATE 'grant select on OPT_PROD_PRICE_MV to ' || PP_USER ||
                        '_OPTIMA_SELECT';
      EXECUTE IMMEDIATE 'grant select on OPT_PROD_PRICE_MV to AA12345_OPT';

      DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           ' Privilege to MV OPT_PROD_PRICE_MV was granted to ' ||
                           PP_USER || '_OPTIMA_ETL_UPDATE, ' || PP_USER ||
                           '_OPTIMA_SELECT and AA12345_OPT successfully.');


    --Get MV Statics After refresh
    DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => P_USER,
                                  TABNAME          => V_MV_NAME,
                                  PARTNAME         => NULL,
                                  ESTIMATE_PERCENT => DBMS_STATS.AUTO_SAMPLE_SIZE,
                                  BLOCK_SAMPLE     => FALSE,
                                  METHOD_OPT       => 'FOR ALL COLUMNS SIZE 1',
                                  DEGREE           => NULL,
                                  GRANULARITY      => 'ALL',
                                  CASCADE          => TRUE,
                                  NO_INVALIDATE    => FALSE);
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') || ' ' ||
                         V_MV_NAME || ' was anayzed.');


    ELSE
      -- Modified by Ellen on 2009-03-12 for R8. this is for refresh MV based on prebuild tables.
      --Full Refresh MV

      pro_put_line('start Refresh MV '||IN_MV_NAME);

      OPT_REFRES_MV(V_MV_NAME);

      pro_put_line('end Refresh MV '||IN_MV_NAME|| ' is ok');

    -- Added by Myra May 18th 2009 for R8. add a new MV OPT_SHPMT_ACTL_FRCST_MV02 for report performance.
   /* IF V_MV_NAME = 'OPT_SHPMT_ACTL_FRCST_MV01' THEN

      pro_put_line('start Refresh MV OPT_SHPMT_ACTL_FRCST_MV02');

      OPT_REFRES_MV('OPT_SHPMT_ACTL_FRCST_MV02');

      pro_put_line('end Refresh MV OPT_SHPMT_ACTL_FRCST_MV02 is ok');
    end if;*/

    END IF;

  EXCEPTION
    WHEN DEADLOCK_DETECTED THEN
      L_RTN_MSG := 'DeadLock Detected Cannot Continue Retry after Sometime';
      DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           ' Refresh MV ' || L_RTN_MSG);
     WHEN register_mv_error THEN
     L_RTN_MSG := 'when register MV errors happened';
      DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           ' register MV ' || L_RTN_MSG||V_MV_NAME);

    WHEN OTHERS THEN
      V_CODE    := SQLCODE;
      V_ERRM    := SUBSTR(SQLERRM, 1, 128);
      L_RTN_MSG := 'Sql Errors Occured while ' || SQL_MSG ||
                   'The Error code is : ' || V_CODE || ': ' || V_ERRM;
      DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           ' Refresh MV ' || L_RTN_MSG);
      RETURN;

  END OPT_REFRESH_MV;



  PROCEDURE OPT_REFRES_MV(p_mv_name IN VARCHAR2) IS
  ------------------------------------------------------------------------------------
    -- Procedure    : REGISTER_MV                                                 --
    -- Originator   : Ellen Zhao (yue-hua.zhao@hp.com)                                --
    -- Author       : Gao Yongwei (yongwei.gao@hp.com)                                --
    -- Date         : 24-Mar.-2009                                                    --
    -- Purpose      : Register Related Materialized Views subprocedure of OPT_REFRESH_MV
    ------------------------------------------------------------------------------------
    -- Parameters
    ------------------------------------------------------------------------------------
    -- No  Name              Desctiption          /   Example                         --
    ------------------------------------------------------------------------------------
    -- 1  p_mv_name        Materialized View Name /   'OPT_SHPMT_RPT_PRMPT_MV01'           --
    ------------------------------------------------------------------------------------
    -- Change History                                                                 --
    -- Date        Programmer        Description                                      --
    -- ----------- ----------------- ---------------------------------------------------
    -- 24-Mar.-2009 Ellen Zhao      Initial Version                                   --
    ------------------------------------------------------------------------------------

    v_mv_name VARCHAR2(30) := UPPER(p_mv_name);
    v_num_exists1  NUMBER;
    v_num_exists2  NUMBER;
    v_sql_stmt1  VARCHAR2(32000);
    v_sql_stmt2  VARCHAR2(32000);
    v_msg VARCHAR2(100);
    v_reg_code CLOB;
     view_not_ready EXCEPTION;
     register_mv_error EXCEPTION;
   P_USER varchar2(30);
   V_USER varchar2(30);

     pragma exception_init(view_not_ready, -20001);
   pragma exception_init(register_mv_error, -20002);

  BEGIN

     V_MV_NAME := p_mv_name;
     SELECT USERNAME INTO P_USER FROM USER_USERS WHERE ROWNUM = 1;

     v_sql_stmt1 := 'SELECT COUNT(*) FROM OPT_MV_MTDTA_PRC WHERE MV_NAME = ''' || v_mv_name || '''';
     EXECUTE IMMEDIATE v_sql_stmt1
     INTO v_num_exists1;
     v_sql_stmt2 := 'SELECT COUNT(*) FROM USER_MVIEWS WHERE mview_name = ''' || v_mv_name || '''';
     EXECUTE IMMEDIATE v_sql_stmt2
     INTO v_num_exists2;


    if v_num_exists1<>0 then

    if v_num_exists2=0 then
    pro_put_line('Materialized view '||v_mv_name ||' does not exist');
      --recreate materialized view
       SELECT RGSTR_SQL_TXT INTO v_reg_code FROM OPT_MV_MTDTA_PRC  WHERE MV_NAME =  v_mv_name ;
       v_sql_stmt1:=TO_CHAR(SUBSTR(v_reg_code,1,8000));
       EXECUTE IMMEDIATE v_sql_stmt1;
       v_msg:= 'view registered successfully'||v_mv_name;
       DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           'registered MV' || v_msg);
    end if;

    pro_put_line(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||' Refreshed MV ' || V_MV_NAME );
    -- Added by Myra in R8, unusable indexes before refresh MV
    -- opt_gather_stats.opt_unusable_ind(V_MV_NAME);

    DBMS_APPLICATION_INFO.SET_MODULE(MODULE_NAME => 'Refresh Shipment Report MV',
                                       ACTION_NAME => 'Refreshing MV :' ||
                                                      V_MV_NAME);

     DBMS_MVIEW.REFRESH(V_MV_NAME, 'C', '', TRUE, FALSE, 0, 0, 0, FALSE);
     pro_put_line(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           ' Refreshed MV ' || V_MV_NAME ||
                           ' Successfully....');

    pro_put_line('register materialized view '||v_mv_name);
    pro_put_line('drop materialized view '||v_mv_name);
      v_sql_stmt1:='drop materialized view '||v_mv_name;
      EXECUTE IMMEDIATE v_sql_stmt1;
    pro_put_line('drop materialized view '||v_mv_name ||' success');

    pro_put_line('Alter session set query_rewrite_enabled=FALSE');
    v_sql_stmt1:='Alter session set query_rewrite_enabled=FALSE';
    EXECUTE IMMEDIATE v_sql_stmt1;

  --recreate materialized view
       SELECT RGSTR_SQL_TXT INTO v_reg_code FROM OPT_MV_MTDTA_PRC  WHERE MV_NAME =  v_mv_name ;
       v_sql_stmt1:=TO_CHAR(SUBSTR(v_reg_code,1,8000));
       EXECUTE IMMEDIATE v_sql_stmt1;
       pro_put_line(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           ' successfully registered MV '||v_mv_name);
      pro_put_line('Alter session set query_rewrite_enabled=TRUE');
      v_sql_stmt1:='Alter session set query_rewrite_enabled = TRUE';
      EXECUTE IMMEDIATE v_sql_stmt1;

  else
  if v_num_exists1=0 then

       pro_put_line(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           'view '||v_mv_name ||' does not ready in metadate OPT_MV_MTDTA_PRC' );
       RAISE view_not_ready;
  end if;

 end if;

    --grant proper privilege to roles
      SELECT UPPER(SUBSTR(USERNAME, 1, INSTR(USERNAME, '_') - 1))
        INTO V_USER
        FROM USER_USERS
       WHERE ROWNUM = 1;
      EXECUTE IMMEDIATE 'grant select, insert, update, delete on '||v_mv_name||' to ' ||
                        V_USER || '_OPTIMA_ETL_UPDATE';
      EXECUTE IMMEDIATE 'grant select on '||V_MV_NAME||' to ' || V_USER ||
                        '_OPTIMA_SELECT';
      EXECUTE IMMEDIATE 'grant select on '||V_MV_NAME||' to AA12345_OPT';

      pro_put_line(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           ' Privilege to MV '||V_MV_NAME||' was granted to ' ||
                           V_USER || '_OPTIMA_ETL_UPDATE, ' || V_USER ||
                           '_OPTIMA_SELECT and AA12345_OPT successfully.');


    -- Added by Myra in R8, rebuild unusable indexes after refresh MV
    -- opt_gather_stats.opt_rebuild_ind(V_MV_NAME,null,'Y');
    --Get MV Statics After refresh
    DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => P_USER,
                                  TABNAME          => V_MV_NAME,
                                  PARTNAME         => NULL,
                                  ESTIMATE_PERCENT => DBMS_STATS.AUTO_SAMPLE_SIZE,
                                  BLOCK_SAMPLE     => FALSE,
                                  METHOD_OPT       => 'FOR ALL COLUMNS SIZE 1',
                                  DEGREE           => NULL,
                                  GRANULARITY      => 'ALL',
                                  CASCADE          => TRUE,
                                  NO_INVALIDATE    => FALSE);
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') || ' ' ||
                         V_MV_NAME || ' was anayzed.');


    EXCEPTION
    WHEN OTHERS THEN
      pro_put_line(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                           ' Rebuild MV exception '||v_mv_name);
                           RAISE register_mv_error;

  END OPT_REFRES_MV;


END Opt_Shpmt_Rpt_Pkg;
/

