CREATE OR REPLACE PACKAGE BODY ADWU_OPTIMA_LAEX.OPT_PRE_PRCSS IS

/**********************************************************************/
/* name: pro_put_line                                                 */
/* purpose: print line of text divided into 255-character chunks      */
/* parameters:                                                        */
/*   p_string - string to be printed                                  */
/* author: bazyli blicharski                                          */
/* originator: brian beckman                                          */
/* version: 1.00 - initial version                                    */
/*          1.01 - string cut according to new line marker            */
/**********************************************************************/
procedure pro_put_line(p_string in varchar2) is
  v_string_length       number;
  v_string_offset       number := 1;
  v_cut_pos             number;
  v_add                 number;
BEGIN
  dbms_output.new_line;
  dbms_output.put_line(to_char(sysdate,'DD-MON-YYYY HH24:MI:SS'));
  v_string_length := length(p_string);

  -- loop thru the string by 255 characters and output each chunk
  while v_string_offset < v_string_length
  loop
    v_cut_pos := nvl(instr(p_string, chr(10), v_string_offset), 0) - v_string_offset;
    if v_cut_pos < 0 or v_cut_pos >= 255 then
      v_cut_pos := 255;
      v_add := 0;
    else
      v_add := 1;
    end if;
          dbms_output.put_line(substr(p_string, v_string_offset, v_cut_pos));
    v_string_offset := v_string_offset + v_cut_pos + v_add;
   end loop;
END pro_put_line;


/**********************************************************************/
/* name: pre_extract                                                  */
/* purpose: Create temp table to improve performance after ODS        */
/* parameters: in_tbl_name - Table to be processed                     */
/*                                                                    */
/* author: Anthony                                                    */
/* originator: Anthony                                                */
/* version: 1.00 - initial version                                    */
/*          1.01 -  Updated by David for performance tuning of fund frcst in R8    */
/*          2.00 -  Updated by David for AA performance tuning in R9    */
/**********************************************************************/
PROCEDURE PRE_EXTRACT(in_tbl_name VARCHAR2, in_dop INTEGER :=4,V_REGN IN VARCHAR2 DEFAULT NULL) IS -- SOX Implementation project
  v_tbl_name VARCHAR2(100);
  --V_SQL      VARCHAR2(32767);
  v_cnt  NUMBER;

  --Performance tunning for FUND_FRCST_FCT in R8, Added by David
  --*****************************************************************************
   PROCEDURE create_table_wise(v_tbl_name IN VARCHAR2, v_sql_create IN VARCHAR2)
  IS
    v_cnt   NUMBER;
    --v_row   NUMBER;
    --v_sql_drop   VARCHAR2(100);
    --v_sql_truc   VARCHAR2(100);
    v_sql_insert  VARCHAR2(500);
    v_dop   NUMBER;
  BEGIN
    SELECT COUNT(1) INTO v_cnt from user_tables WHERE table_name = v_tbl_name;
    IF v_cnt > 0 THEN
        pro_put_line('The table ' || v_tbl_name || ' was existed already (END)');
        RETURN;
    END IF;
    pro_put_line('SQL:' || chr(10) || '> ' || REPLACE(v_sql_create, chr(10), chr(10) || '> '));
    EXECUTE IMMEDIATE v_sql_create;
    pro_put_line('Table created successfuly.');

    v_dop := P_DOP;

    IF v_tbl_name='H_TFM_OPT_FUND_FCST_CALC_FULL' THEN

    v_sql_insert:='INSERT /*+ APPEND PARALLEL(' || v_dop || ') */ INTO H_TFM_OPT_FUND_FCST_CALC_FULL'||CHR(10)||
    'SELECT PROD_ID,'||CHR(10)||
       'FUND_ID,'||CHR(10)||
       'START_DT,'||CHR(10)||
       'END_DT,'||CHR(10)||
       'BASE_SHPMT_AMT,'||CHR(10)||
       'ACCRL_FUND_AMT'||CHR(10)||
    'FROM OPT_FUND_FRCST_FCT FCT,'||CHR(10)||
     '(SELECT   '||CHR(10)||
       'MTH_SKID,'||CHR(10)||
      ' MIN(DAY_DATE) AS START_DT,'||CHR(10)||
       'MAX(DAY_DATE) AS END_DT'||CHR(10)||
       'FROM     OPT_CAL_MASTR_DIM'||CHR(10)||
       'GROUP BY MTH_SKID) CAL'||CHR(10)||
    'WHERE FCT.MTH_SKID = CAL.MTH_SKID';

    pro_put_line('Initial Table H_TFM_OPT_FUND_FCST_CALC_FULL successfuly (' || SQL%ROWCOUNT || ' rows processed).');

    END IF;


    IF v_tbl_name='H_TFM_OPT_FUND_MASTER_FULL' THEN
    v_sql_insert:='INSERT  /*+ APPEND PARALLEL(' || v_dop || ') */ INTO H_TFM_OPT_FUND_MASTER_FULL'||CHR(10)||
    'SELECT DISTINCT ACCT_FUND_ID,'||CHR(10)||
    '       BUS_UNIT_ID,'||CHR(10)||
    '       FUND_ID,'||CHR(10)||
    '       NULL,'||CHR(10)||
    '       ''N'','||CHR(10)||
    '       ''N'''                                                 ||CHR(10)||
    'FROM OPT_FUND_FRCST_FCT';
    pro_put_line('Initial Table H_TFM_OPT_FUND_MASTER_FULL successfuly (' || SQL%ROWCOUNT || ' rows processed).');
    END IF;

    EXECUTE IMMEDIATE v_sql_insert;
    COMMIT;
    EXCEPTION
    WHEN OTHERS THEN
        pro_put_line('Error: Creation of temporary table ' || v_tbl_name ||' failed with error - ' || SQLERRM);
        RAISE_APPLICATION_ERROR(-20001, 'Error: Creation of temporary table ' || v_tbl_name ||' failed with error - ' ||SQLERRM);
  END;
  --*****************************************************************************
   -- procedures
  PROCEDURE create_table(v_tbl_name IN VARCHAR2, v_sql_create IN VARCHAR2, V_REGN IN VARCHAR2 DEFAULT NULL) --SOX Implementation project
  IS
    v_cnt   NUMBER;
    v_row   NUMBER;
    v_sql_drop   VARCHAR2(100);
    v_sql_truc   VARCHAR2(100);
    --v_sql_index  VARCHAR2(200);
  BEGIN
    SELECT COUNT(1) INTO v_cnt from user_tables WHERE table_name = v_tbl_name;
    IF v_cnt > 0 THEN
      -- Check data
      IF v_tbl_name <> 'OPT_AA_PRMTN_PROD_TDADS' THEN
        --SELECT COUNT(1) INTO v_row FROM OPT_AA_PRMTN_PROD_TDADS;
        -- modified by Myra 20091101
        OPT_PKG_UTIL.pro_check_tbl_exist('OPT_AA_PRMTN_PROD_TDADS', v_row);
        IF v_row = 0 THEN
          pro_put_line('There is no data in table OPT_AA_PRMTN_PROD_TDADS, so truncate table '|| v_tbl_name);
          v_sql_truc := 'TRUNCATE TABLE '|| v_tbl_name;
          pro_put_line('SQL:' || chr(10) || '> ' || REPLACE(v_sql_truc, chr(10), chr(10) || '> '));
          pro_put_line('Truncate table ' || v_tbl_name || ' (BEGIN)');
          EXECUTE IMMEDIATE v_sql_truc;
          pro_put_line('Truncate table ' || v_tbl_name || ' (END)');
          RETURN;
        END IF;
      END IF;
      v_sql_drop := 'DROP TABLE ' || v_tbl_name || ' PURGE';
      pro_put_line('SQL:' || chr(10) || '> ' || REPLACE(v_sql_drop, chr(10), chr(10) || '> '));
      pro_put_line('Drop table ' || v_tbl_name || ' (BEGIN)');
      EXECUTE IMMEDIATE v_sql_drop;
      pro_put_line('Drop table ' || v_tbl_name || ' (END)');
    END IF;

    pro_put_line('SQL:' || CHR(10) || '> ' || REPLACE(v_sql_create, CHR(10), CHR(10) || '> '));
    pro_put_line('Create table ' || v_tbl_name || ' (BEGIN)');
    EXECUTE IMMEDIATE v_sql_create;
    pro_put_line('Table created successfuly (' || SQL%ROWCOUNT || ' rows processed).');

    /*CASE
      WHEN v_tbl_name = 'OPT_AA_PRMTN_PROD_TDADS' THEN
        -- Index
        v_sql_index := 'CREATE INDEX OPT_AA_PRMTN_PROD_TDADS_IDX1 ON OPT_AA_PRMTN_PROD_TDADS(PROD_ID, FY_DATE_SKID, BUS_UNIT_ID) NOLOGGING';
        EXECUTE IMMEDIATE v_sql_index;

        v_sql_index := 'CREATE INDEX OPT_AA_PRMTN_PROD_TDADS_IDX2 ON OPT_AA_PRMTN_PROD_TDADS(ACCT_ID, BUS_UNIT_ID, PROD_ID, PGM_START_DATE, PGM_END_DATE) NOLOGGING';

      WHEN v_tbl_name = 'OPT_AA_ACTVY_PROD_TDADS' THEN
        -- Index
        v_sql_index := 'CREATE INDEX OPT_AA_ACTVY_PROD_TDADS_IDX1 ON OPT_AA_ACTVY_PROD_TDADS(PROD_ID, FY_DATE_SKID, BUS_UNIT_ID) NOLOGGING';

      WHEN v_tbl_name = 'OPT_AA_PROD_HIER_TDADS' THEN
        -- Index
        v_sql_index := 'CREATE INDEX OPT_AA_PROD_HIER_TDADS_IDX1 ON OPT_AA_PROD_HIER_TDADS(PROD_ID, PROD_ITEM_GTIN) NOLOGGING';

      WHEN v_tbl_name = 'OPT_SVP_BASE_TFADS' THEN
        -- Index
        v_sql_index := 'CREATE INDEX OPT_SVP_BASE_TFADS_IDX1 ON  OPT_SVP_BASE_TFADS(ACCT_ID, PROD_ID, PERD_START_DATE) NOLOGGING';

      WHEN v_tbl_name = 'OPT_AA_ACCT_SHPMT_TFADS' THEN
        -- Index
        v_sql_index := 'SELECT SYSDATE FROM DUAL';

      WHEN v_tbl_name = 'OPT_SVP_INCRM_TFADS' THEN
        -- Index
        v_sql_index := 'CREATE INDEX OPT_SVP_INCRM_TFADS_IDX1 ON OPT_SVP_INCRM_TFADS(ACCT_ID, PROD_ID, PERD_START_DATE) NOLOGGING';

      WHEN v_tbl_name = 'OPT_AA_PRMTN_GTIN_TFADS' THEN
        v_sql_index := 'SELECT SYSDATE FROM DUAL';
    END CASE;*/

    /*pro_put_line('SQL:' || CHR(10) || '> ' || REPLACE(v_sql_index, CHR(10), CHR(10) || '> '));
    pro_put_line('Create Index (BEGIN)');
    EXECUTE IMMEDIATE v_sql_index;
    pro_put_line('Create Index (END)');*/

    -- Statistics
    pro_put_line('Gather statistics for table ' || v_tbl_name || ' (BEGIN)');
    DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                  TABNAME          => v_tbl_name);
    pro_put_line('Gather statistics for table ' || v_tbl_name || ' (END)');

    pro_put_line('Process temp table ' || v_tbl_name ||' (END)');

  EXCEPTION
    WHEN OTHERS THEN
        pro_put_line('Error: Creation of temporary table ' || v_tbl_name ||' failed with error - ' || SQLERRM);
        RAISE_APPLICATION_ERROR(-20001, 'Error: Creation of temporary table ' || v_tbl_name ||' failed with error - ' ||SQLERRM);
  END;

BEGIN

  OPT_AX_TBLSPACE(V_REGN,C_TBLSPACE_NAME,V_INDEX);--SOX Implemenation project

  CASE UPPER(in_tbl_name)
  WHEN 'OPT_ACTVY_GTIN_FCT' THEN
  opt_load_stage_data.load_data('OPT_AA_PRE_PRCSS',V_REGN);--SOX IMPLEMENTATION Project

    /* Commented by David on 2009-11-15
    pro_put_line('Create temp tables for ' ||in_tbl_name|| ' (BEGIN)');
    -- precess OPT_PRMTN_ITEM_GTIN_LKP
    V_SQL := 'MERGE INTO OPT_PRMTN_ITEM_GTIN_LKP D' ||CHR(10)||
              'USING (' ||CHR(10)||
              '  SELECT ' ||CHR(10)||
              '    NVL(PRMTN_ITEM.PRMTN_ID, CURR_PRMTN_ITEM.PRMTN_ID) PRMTN_ID,' ||CHR(10)||
              '    NVL(PRMTN_ITEM.BUS_UNIT_ID, CURR_PRMTN_ITEM.BUS_UNIT_ID) BUS_UNIT_ID,' ||CHR(10)||
              '    NVL(PRMTN_ITEM.PRMTN_CATEG_ID, CURR_PRMTN_ITEM.PRMTN_CATEG_ID) PRMTN_CATEG_ID,' ||CHR(10)||
              '    NVL(PRMTN_ITEM.ITEM_GTIN_ID, CURR_PRMTN_ITEM.ITEM_GTIN_ID) ITEM_GTIN_ID,' ||CHR(10)||
              '    (CASE WHEN PRMTN_ITEM.PRMTN_CATEG_ID IS NULL AND CURR_PRMTN_ITEM.PRMTN_CATEG_ID IS NOT NULL THEN 1 ELSE 0 END) INSERT_FLAG,' ||CHR(10)||
              '    (CASE WHEN PRMTN_ITEM.PRMTN_CATEG_ID IS NOT NULL AND CURR_PRMTN_ITEM.PRMTN_CATEG_ID IS NULL THEN 1 ELSE 0 END) DELETE_FLAG' ||CHR(10)||
              '   FROM' ||CHR(10)||
                  --Purpose: insert the broken down structure of AUTO-GTIN from Category(higher) level to ITEM GTIN(s)
                  --CURR_PRMTN_ITEM: SQL used to insert all the promoted product item gtin
                  --This table is prebuilt in Optima R8 data model
              '  (SELECT ' ||CHR(10)||
              '         DISTINCT ' ||CHR(10)||
              '         AA_PRMTN.ROW_ID              AS PRMTN_ID,' ||CHR(10)||
              '         AA_PRMTN.BU_ID               AS BUS_UNIT_ID,' ||CHR(10)||
              '         CATEG.ROW_ID                 AS PRMTN_CATEG_ID,' ||CHR(10)||
              '--       DCMPD_PROD.ITEM_GTIN_PROD_ID AS ITEM_GTIN_ID' ||CHR(10)||
              '         GTIN_PROD.PROD_ID            AS ITEM_GTIN_ID' ||CHR(10)||
              '    FROM S_SRC                AA_PRMTN,' ||CHR(10)||
              '         S_SRC                CATEG,' ||CHR(10)||
              '         OPT_BUS_UNIT_SKID_VW R,' ||CHR(10)||
              '         S_PERIOD             PERD_ST,' ||CHR(10)||
              '         OPT_CAL_MASTR_DIM    CAL,' ||CHR(10)||
              '         S_CTLG_CAT           CAT,' ||CHR(10)||
              '         S_PROD_INT           PROD,' ||CHR(10)||
              '         OPT_PROD_FDIM        PROD_FDIM,' ||CHR(10)||
              '         OPT_PROD_ASDN_DIM    ASDN_PROD,' ||CHR(10)||
              '--       OPT_DCMPD_PROD_DIM   DCMPD_PROD' ||CHR(10)||
              '         OPT_PROD_FDIM         GTIN_PROD' ||CHR(10)||
              '   WHERE AA_PRMTN.SUB_TYPE = ''PLAN_ACCOUNT_PROMOTION''' ||CHR(10)||
              '     AND CATEG.SUB_TYPE = ''PLAN_ACCT_PROMOTION_CATEGORY''' ||CHR(10)||
              '     AND AA_PRMTN.SRC_CD = ''Annual Agreement''' ||CHR(10)||
              '     AND AA_PRMTN.ROW_ID = CATEG.PAR_SRC_ID' ||CHR(10)||
              '     AND TRIM(BOTH '' '' FROM CATEG.X_AUTO_GTIN_FLG) = ''Y''' ||CHR(10)|| --only set for Annual Agreement
              '     AND AA_PRMTN.BU_ID = R.BUS_UNIT_ID' ||CHR(10)||
              '     AND AA_PRMTN.X_ST_MONTH_ID = PERD_ST.ROW_ID' ||CHR(10)||
              '     AND PERD_ST.START_DT = CAL.DAY_DATE' ||CHR(10)||
              '     AND CATEG.CTLG_CAT_ID = CAT.ROW_ID' ||CHR(10)||
              '     AND CAT.NAME = PROD.NAME' ||CHR(10)||
              '     AND AA_PRMTN.BU_ID = PROD.BU_ID' ||CHR(10)||
              '     AND PROD.ROW_ID = PROD_FDIM.PROD_ID' ||CHR(10)||
              '     AND PROD_FDIM.PROD_SKID = ASDN_PROD.ROOT_PROD_SKID' ||CHR(10)||
              '     AND CAL.FISC_YR_SKID = ASDN_PROD.FY_DATE_SKID' ||CHR(10)||
              '     AND ASDN_PROD.PROD_SKID = GTIN_PROD.PROD_SKID' ||CHR(10)||
              '     AND GTIN_PROD.PROD_CODE = ''EA''' ||CHR(10)||) CURR_PRMTN_ITEM' ||CHR(10)||
              '     FULL OUTER JOIN OPT_PRMTN_ITEM_GTIN_LKP PRMTN_ITEM' ||CHR(10)||
              '     ON (CURR_PRMTN_ITEM.PRMTN_ID = PRMTN_ITEM.PRMTN_ID' ||CHR(10)||
              '     AND CURR_PRMTN_ITEM.BUS_UNIT_ID = PRMTN_ITEM.BUS_UNIT_ID' ||CHR(10)||
              '     AND CURR_PRMTN_ITEM.PRMTN_CATEG_ID = PRMTN_ITEM.PRMTN_CATEG_ID' ||CHR(10)||
              '     AND CURR_PRMTN_ITEM.ITEM_GTIN_ID = PRMTN_ITEM.ITEM_GTIN_ID)) S' ||CHR(10)||
              '  ON (S.PRMTN_ID = D.PRMTN_ID' ||CHR(10)||
              '     AND S.BUS_UNIT_ID = D.BUS_UNIT_ID' ||CHR(10)||
              '     AND S.PRMTN_CATEG_ID = D.PRMTN_CATEG_ID' ||CHR(10)||
              '     AND S.ITEM_GTIN_ID = D.ITEM_GTIN_ID)' ||CHR(10)||
              'WHEN MATCHED THEN UPDATE SET D.PRMTN_PROD_ID = D.PRMTN_PROD_ID' ||CHR(10)||
              '  DELETE WHERE (S.DELETE_FLAG=1)' ||CHR(10)||
              'WHEN NOT MATCHED THEN INSERT (D.PRMTN_ID, D.BUS_UNIT_ID, D.PRMTN_CATEG_ID, D.ITEM_GTIN_ID, D.PRMTN_PROD_ID)' ||CHR(10)||
              '  VALUES (S.PRMTN_ID, S.BUS_UNIT_ID, S.PRMTN_CATEG_ID, S.ITEM_GTIN_ID, ''AX-''||OPT_PRMTN_ITEM_GTIN_ID_SEQ.NEXTVAL)' ||CHR(10)||
              '  WHERE (S.INSERT_FLAG=1)';

    EXECUTE IMMEDIATE V_SQL;
    -- Process OPT_AA_PRMTN_PROD_TDADS
    v_tbl_name := 'OPT_AA_PRMTN_PROD_TDADS';

    V_SQL := 'CREATE TABLE OPT_AA_PRMTN_PROD_TDADS' ||CHR(10)||
              C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
              'parallel (degree '||in_dop||')'||CHR(10)||
              'NOLOGGING compress'||CHR(10)||
              'AS ' ||CHR(10)||
              -- Purpose: Insert the broken-down structure for all Annual Agreement Promoted Product(id)
              --          And also with promotion id, account id, start, end date, etc.
              -- Structure: The codes will be an union of 2 parts ( CASE 1, 2 )
              -- CASE 1: X_AUTO_GTIN_FLG <> 'Y'
              'SELECT DISTINCT' ||CHR(10)||
              '       AA_PRMTN.ROW_ID AS PRMTN_ID,' ||CHR(10)||
              '       AA_PRMTN.PR_ACCNT_ID AS ACCT_ID,' ||CHR(10)||
              '       AA_PRMTN.NAME AS PRMTN_NAME,' ||CHR(10)||
              '       PERD_ST.START_DT AS PGM_START_DATE,' ||CHR(10)||
              '       PERD_END.END_DT AS PGM_END_DATE,' ||CHR(10)||
              '       CAL.FISC_YR_SKID AS FY_DATE_SKID,' ||CHR(10)||
              '       AA_PRMTN.BU_ID AS BUS_UNIT_ID,' ||CHR(10)||
              '       PRMTN_PROD.PROD_ID AS PROD_ID,' ||CHR(10)||
              '       PROD_FDIM.GTIN AS PROD_ITEM_GTIN,' ||CHR(10)||
              '       PRMTN_PROD.ROW_ID AS PRMTN_PROD_ID' ||CHR(10)||
              '  FROM S_SRC             AA_PRMTN,' ||CHR(10)||
              '       S_SRC             CATEG,' ||CHR(10)||
              '       S_SRC             PRMTN_PROD,' ||CHR(10)||
              '       OPT_PROD_FDIM     PROD_FDIM,' ||CHR(10)||
              '       S_PERIOD          PERD_ST,' ||CHR(10)||
              '       S_PERIOD          PERD_END,' ||CHR(10)||
              '       OPT_CAL_MASTR_DIM CAL,' ||CHR(10)||
              '       OPT_BUS_UNIT_SKID_VW R' ||CHR(10)||
              ' WHERE AA_PRMTN.SUB_TYPE = ''PLAN_ACCOUNT_PROMOTION''' ||CHR(10)||
              '   AND CATEG.SUB_TYPE = ''PLAN_ACCT_PROMOTION_CATEGORY''' ||CHR(10)||
              '   AND PRMTN_PROD.SUB_TYPE = ''PLAN_ACCOUNT_PROMOTION_PRODUCT''' ||CHR(10)||
              '   AND AA_PRMTN.SRC_CD = ''Annual Agreement''' ||CHR(10)||
              '   AND AA_PRMTN.ROW_ID = CATEG.PAR_SRC_ID' ||CHR(10)||
              '   AND CATEG.ROW_ID = PRMTN_PROD.PAR_SRC_ID' ||CHR(10)||
              '   AND PRMTN_PROD.PROD_ID = PROD_FDIM.PROD_ID' ||CHR(10)||
              '   AND AA_PRMTN.X_ST_MONTH_ID = PERD_ST.ROW_ID' ||CHR(10)||
              '   AND AA_PRMTN.X_END_MONTH_ID = PERD_END.ROW_ID' ||CHR(10)||
              '   AND PERD_ST.START_DT = CAL.DAY_DATE' ||CHR(10)||
              '   AND AA_PRMTN.BU_ID = R.BUS_UNIT_ID' ||CHR(10)||
              '   AND NOT TRIM(BOTH '' '' FROM CATEG.X_AUTO_GTIN_FLG) = ''Y'' ' ||CHR(10)||
              'UNION ' ||CHR(10)||
              -- CASE 2: X_AUTO_GTIN_FLG = 'Y'
              'SELECT DISTINCT' ||CHR(10)||
              '       AA_PRMTN.ROW_ID AS PRMTN_ID,' ||CHR(10)||
              '       AA_PRMTN.PR_ACCNT_ID AS ACCT_ID,' ||CHR(10)||
              '       AA_PRMTN.NAME AS PRMTN_NAME,' ||CHR(10)||
              '       PERD_ST.START_DT AS PGM_START_DATE,' ||CHR(10)||
              '       PERD_END.END_DT AS PGM_END_DATE,' ||CHR(10)||
              '       CAL.FISC_YR_SKID AS FY_DATE_SKID,' ||CHR(10)||
              '       AA_PRMTN.BU_ID AS BUS_UNIT_ID,' ||CHR(10)||
              '       F.ITEM_GTIN_ID AS PROD_ID,' ||CHR(10)||       --taken from AX prebuilt ITEM_GTIN_LKP table
              '       PROD_FDIM.GTIN AS PROD_ITEM_GTIN,' ||CHR(10)||
              '       F.PRMTN_PROD_ID AS PRMTN_PROD_ID' ||CHR(10)|| --taken from AX prebuilt ITEM_GTIN_LKP table
              '  FROM S_SRC             AA_PRMTN,' ||CHR(10)||
              '       S_SRC             CATEG,' ||CHR(10)||
              '       OPT_PROD_FDIM     PROD_FDIM,' ||CHR(10)||
              '       S_PERIOD          PERD_ST,' ||CHR(10)||
              '       S_PERIOD          PERD_END,' ||CHR(10)||
              '       OPT_CAL_MASTR_DIM CAL,' ||CHR(10)||
              '       OPT_BUS_UNIT_SKID_VW R,' ||CHR(10)||
              '       OPT_PRMTN_ITEM_GTIN_LKP F' ||CHR(10)||
              ' WHERE AA_PRMTN.SUB_TYPE = ''PLAN_ACCOUNT_PROMOTION''' ||CHR(10)||
              '   AND CATEG.SUB_TYPE = ''PLAN_ACCT_PROMOTION_CATEGORY''' ||CHR(10)||
              '   AND AA_PRMTN.SRC_CD = ''Annual Agreement''' ||CHR(10)||
              '   AND AA_PRMTN.ROW_ID = CATEG.PAR_SRC_ID' ||CHR(10)||
              '   AND F.ITEM_GTIN_ID = PROD_FDIM.PROD_ID' ||CHR(10)||
              '   AND AA_PRMTN.X_ST_MONTH_ID = PERD_ST.ROW_ID' ||CHR(10)||
              '   AND AA_PRMTN.X_END_MONTH_ID = PERD_END.ROW_ID' ||CHR(10)||
              '   AND PERD_ST.START_DT = CAL.DAY_DATE' ||CHR(10)||
              '   AND AA_PRMTN.BU_ID = R.BUS_UNIT_ID' ||CHR(10)||
              '   AND TRIM(BOTH '' '' FROM CATEG.X_AUTO_GTIN_FLG) = ''Y''' ||CHR(10)||
              '   AND AA_PRMTN.ROW_ID = F.PRMTN_ID' ||CHR(10)||  -- join on ITEM_GTIN_LKP(PRMTN_ID, CATEG_ID, BUS_UNIT_ID)
              '   AND AA_PRMTN.BU_ID = F.BUS_UNIT_ID' ||CHR(10)||
              '   AND CATEG.ROW_ID = F.PRMTN_CATEG_ID';

    create_table(v_tbl_name, V_SQL);


    -- Process OPT_AA_ACTVY_PROD_TDADS
    v_tbl_name := 'OPT_AA_ACTVY_PROD_TDADS';

    V_SQL := 'CREATE TABLE OPT_AA_ACTVY_PROD_TDADS' ||CHR(10)||
              C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
              'parallel (degree '||in_dop||')'||CHR(10)||
              'NOLOGGING compress'||CHR(10)||
              'AS' ||CHR(10)||
              -- Purpose: Create a table for Annual Agreement's Activity Product(Id)
              --          The data set will also contains Annual Agreement(Id), Activity(Id), etc.
              'SELECT DISTINCT AA_PRMTN.ROW_ID AS PRMTN_ID,' ||CHR(10)||
              '       AA_PRMTN.PR_ACCNT_ID AS ACCT_ID,' ||CHR(10)||
              '       ACTVY.ROW_ID AS ACTVY_ID,' ||CHR(10)||
              '       CAL.FISC_YR_SKID AS FY_DATE_SKID,' ||CHR(10)||
              '       AA_PRMTN.BU_ID AS BUS_UNIT_ID,' ||CHR(10)||
              -- If "Pricing Level" = "GTIN/Promoted Group" use the activity product directly
              -- Else broken down from Category(Higher) level into normal GTIN using TX hierachy
              '       CASE WHEN ACTVY.X_PRICING_LEVEL <> ''GTIN/Promoted Group'' ' ||CHR(10)||
              '       THEN (SELECT PROD.ROW_ID' ||CHR(10)||
              '               FROM S_PROD_INT PROD, S_CTLG_CAT CAT' ||CHR(10)||
              '              WHERE PROD.NAME = CAT.NAME' ||CHR(10)||
              '                AND CAT.ROW_ID = ACTVY_PROD.CTLG_CAT_ID' ||CHR(10)||
              '                AND PROD.BU_ID = AA_PRMTN.BU_ID)' ||CHR(10)||
              '       ELSE ACTVY_PROD.PROD_ID ' ||CHR(10)||
              '       END AS PROD_ID,' ||CHR(10)||
              '       ACTVY_PROD.X_SRC_ACT_DISCOUNT_RATE AS DISC_RATE,' ||CHR(10)||-- "Discount Rate"
              '       ACTVY.FIXED_COST_TYPE_CD AS COST_BASE_DESC,' ||CHR(10)||     -- "Cost Type" - either by Volume/GIV/NIV/BUoM
              '       PERD.ROW_ID AS PERD_ID,' ||CHR(10)||
              '       PERD_ST.START_DT AS PGM_START_DATE,' ||CHR(10)||
              '       PERD_END.END_DT AS PGM_END_DATE' ||CHR(10)||
              '  FROM S_SRC                   AA_PRMTN,' ||CHR(10)||
              '       S_SRC                   ACTVY,' ||CHR(10)||
              '       S_SRC                   ACTVY_PROD,' ||CHR(10)||
              '       S_PERIOD                PERD_ST,' ||CHR(10)||
              '       S_PERIOD                PERD_END,' ||CHR(10)||
              '       S_PERIOD                PERD,' ||CHR(10)||
              '       OPT_CAL_MASTR_DIM       CAL,' ||CHR(10)||
              '       OPT_BUS_UNIT_SKID_VW R' ||CHR(10)||
              ' WHERE AA_PRMTN.SUB_TYPE = ''PLAN_ACCOUNT_PROMOTION''' ||CHR(10)||
              '   AND ACTVY.SUB_TYPE = ''PG_TFMAAI_PROMOTION_ACTIVITY''' ||CHR(10)||
              '   AND ACTVY_PROD.SUB_TYPE = ''PG_TFMAAI_ACTIVITY_PRODUCT''' ||CHR(10)||
              '   AND AA_PRMTN.SRC_CD = ''Annual Agreement''' ||CHR(10)||
              '   AND AA_PRMTN.ROW_ID = ACTVY.PAR_SRC_ID' ||CHR(10)||
              '   AND ACTVY.ROW_ID = ACTVY_PROD.PAR_SRC_ID' ||CHR(10)||
              -- (X_ST_MONTH_ID, X_END_MONTH_ID) are used here, just for AA Period
              '   AND AA_PRMTN.X_ST_MONTH_ID = PERD_ST.ROW_ID' ||CHR(10)||
              '   AND AA_PRMTN.X_END_MONTH_ID = PERD_END.ROW_ID' ||CHR(10)||
              '   AND PERD_ST.START_DT = CAL.DAY_DATE' ||CHR(10)||
              '   AND AA_PRMTN.BU_ID = PERD.BU_ID' ||CHR(10)||
              '   AND PERD.PERIOD_CD = ''Month''' ||CHR(10)||
              '   AND PERD_ST.START_DT <= PERD.START_DT' ||CHR(10)||
              '   AND PERD.END_DT <= PERD_END.END_DT' ||CHR(10)||
              '   AND AA_PRMTN.BU_ID = R.BUS_UNIT_ID';

    create_table(v_tbl_name, V_SQL);


    -- Process OPT_AA_PROD_HIER_TDADS
    v_tbl_name := 'OPT_AA_PROD_HIER_TDADS';

    V_SQL := 'CREATE TABLE OPT_AA_PROD_HIER_TDADS' ||CHR(10)||
             C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
              'parallel (degree '||in_dop||')'||CHR(10)||
              'NOLOGGING compress'||CHR(10)||
              'AS' ||CHR(10)||
              -- Purpose: used the table above to create a hierarchy for AA Promoted product
              'SELECT ' ||CHR(10)||
              '       ASDN_PROD.ROOT_PROD_SKID       AS PROD_SKID,' ||CHR(10)||
              '       ASDN_PROD.FY_DATE_SKID         AS FY_DATE_SKID,' ||CHR(10)||
              '       DCMPD_PROD.BUOM_GTIN_PROD_SKID AS BUOM_GTIN_PROD_SKID,' ||CHR(10)||
              '       DCMPD_PROD.DCMPD_REF_PROD_SKID AS DCMPD_REF_PROD_SKID,' ||CHR(10)||
              '       PROD_DIM.PROD_ID               AS PROD_ID,' ||CHR(10)||
              '       ASDN_PROD.ROOT_PROD_DESC       AS PROD_NAME,' ||CHR(10)||
              '       DCMPD_PROD.BUOM_GTIN_PROD_ID   AS BUOM_GTIN_PROD_ID,' ||CHR(10)||
              '       DCMPD_PROD.BUOM_GTIN_PROD_NAME AS BUOM_GTIN_PROD_NAME,' ||CHR(10)||
              '       DCMPD_PROD.BUOM_FPC_CODE       AS BUOM_FPC_CODE,' ||CHR(10)||
              '       DCMPD_PROD.BUS_UNIT_SKID       AS BUS_UNIT_SKID,' ||CHR(10)||
              '       DCMPD_PROD.BUS_UNIT_ID         AS BUS_UNIT_ID,' ||CHR(10)||
              '       DCMPD_PROD.PROD_ITEM_GTIN      AS PROD_ITEM_GTIN,' ||CHR(10)||
              '       DCMPD_PROD.LGSTC_QTY_FACTR     AS LGSTC_QTY_FACTR,' ||CHR(10)||
              '       DCMPD_PROD.DCMPD_REF_PROD_GTIN AS DCMPD_REF_PROD_GTIN,' ||CHR(10)||
              '       DCMPD_PROD.DCMPD_QTY_FACTR     AS DCMPD_QTY_FACTR,' ||CHR(10)||
              '       DCMPD_PROD.SU_FACTR            AS SU_FACTR,' ||CHR(10)||
              '       DCMPD_PROD.VAL_FACTR           AS VAL_FACTR,' ||CHR(10)||
              '       DCMPD_PROD.PROD_BUOM_GTIN      AS PROD_BUOM_GTIN,' ||CHR(10)||
              '       DCMPD_PROD.DCMPD_REF_PROD_NAME AS DCMPD_REF_PROD_NAME,' ||CHR(10)||
              '       DCMPD_PROD.DCMPD_REF_FPC_CODE  AS DCMPD_REF_FPC_CODE,' ||CHR(10)||
              '       DCMPD_PROD.ITEM_GTIN_PROD_SKID AS ITEM_GTIN_PROD_SKID,' ||CHR(10)||
              '       DCMPD_PROD.ITEM_GTIN_PROD_ID   AS ITEM_GTIN_PROD_ID,' ||CHR(10)||
              '       DCMPD_PROD.ITEM_GTIN_PROD_NAME AS ITEM_GTIN_PROD_NAME,' ||CHR(10)||
              '       DCMPD_PROD.CMPLX_PROD_IND AS CMPLX_PROD_IND' ||CHR(10)||
              '  FROM (SELECT DISTINCT FY_DATE_SKID, BUS_UNIT_ID, PROD_ITEM_GTIN FROM OPT_AA_PRMTN_PROD_TDADS) AA_ITEM_GTIN,' ||CHR(10)||
              '       OPT_DCMPD_PROD_DIM DCMPD_PROD,' ||CHR(10)||
              '       OPT_PROD_ASDN_DIM  ASDN_PROD,' ||CHR(10)||
              '       OPT_PROD_DIM       PROD_DIM' ||CHR(10)||
              ' WHERE AA_ITEM_GTIN.FY_DATE_SKID = ASDN_PROD.FY_DATE_SKID' ||CHR(10)||
              '   AND AA_ITEM_GTIN.BUS_UNIT_ID = DCMPD_PROD.BUS_UNIT_ID' ||CHR(10)||
              '   AND AA_ITEM_GTIN.PROD_ITEM_GTIN = DCMPD_PROD.PROD_ITEM_GTIN' ||CHR(10)||
              '   AND DCMPD_PROD.BUOM_GTIN_PROD_SKID = ASDN_PROD.PROD_SKID' ||CHR(10)||
              '   AND ASDN_PROD.ROOT_PROD_SKID = PROD_DIM.PROD_SKID';

    create_table(v_tbl_name, V_SQL);


    -- Process OPT_SVP_BASE_TFADS
    v_tbl_name := 'OPT_SVP_BASE_TFADS';

    V_SQL := 'CREATE TABLE OPT_SVP_BASE_TFADS' ||CHR(10)||
             C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
              'parallel (degree '||in_dop||')'||CHR(10)||
              'NOLOGGING compress'||CHR(10)||
              -- Purpose: get all SVP baseline related data
              -- The implementation is the same as Product Baseline
              -- please also refer to : OPT_SVP_INCRM_TFADS
              'AS SELECT S_PROD_BASELINE.OU_EXT_ID AS ACCT_ID,' ||CHR(10)||
              '       S_PROD_BASELINE.PRDINT_ID AS PROD_ID,' ||CHR(10)||
              '       S_PROD_BASELINE.PERIOD_ID AS PERD_ID,' ||CHR(10)||
              '       --PERD.ROW_ID               AS PERD_ID,' ||CHR(10)||
              '       P.START_DT                AS PERD_START_DATE,' ||CHR(10)||
              '       P.END_DT                  AS PERD_END_DATE,' ||CHR(10)||
              '       ---------------------------------------------------------------------' ||CHR(10)||
              '       -- Baseline Planning Measures (SU/BUoM/GIV/NIV)' ||CHR(10)||
              '       ---------------------------------------------------------------------' ||CHR(10)||
              '       --Baseline Planning SU' ||CHR(10)||
              '       S_PROD_BASELINE.PLANNED_SALES AS BASLN_SU_AMT,' ||CHR(10)||
              '       --Baseline Planning BUOM = Baseline Planning SU / SU_factor' ||CHR(10)||
              '       ROUND(DECODE(NVL(PROD.X_G_SUFCTOR, 0),' ||CHR(10)||
              '                    0,' ||CHR(10)||
              '                    0,' ||CHR(10)||
              '                    --Baseline Planning SU' ||CHR(10)||
              '                    NVL(S_PROD_BASELINE.PLANNED_SALES, 0) /' ||CHR(10)||
              '                    -- SU_factor' ||CHR(10)||
              '                     PROD.X_G_SUFCTOR),' ||CHR(10)||
              '             7) AS BASLN_BUOM_AMT,' ||CHR(10)||
              '       --Baseline Planning GIV = Baseline Planning BUOM * Price' ||CHR(10)||
              '       ROUND(DECODE(NVL(PROD.X_G_SUFCTOR, 0),' ||CHR(10)||
              '                    0,' ||CHR(10)||
              '                    0,' ||CHR(10)||
              '                    -- Baseline Planning BUOM = Baseline Planning SU / SU factor' ||CHR(10)||
              '                    -- Baseline Planning SU' ||CHR(10)||
              '                    round(NVL(S_PROD_BASELINE.PLANNED_SALES, 0) /' ||CHR(10)||
              '                    -- SU factor' ||CHR(10)||
              '                     PROD.X_G_SUFCTOR, 7)) *' ||CHR(10)||
              '             -- Price' ||CHR(10)||
              '             NVL(ITEM.DFLT_COST_UNIT, 0),' ||CHR(10)||
              '             7) AS BASLN_GIV_AMT,' ||CHR(10)||
              '       --Baseline Planning NIV=Baseline Planning SU * "NIV/SU Converter Factor"' ||CHR(10)||
              '       ROUND(S_PROD_BASELINE.PLANNED_SALES *' ||CHR(10)||
              '             --NIV/SU Converter Factor' ||CHR(10)||
              '             NVL(S_PROD_BASELINE.X_NIVSU_CONV, 0),' ||CHR(10)||
              '             7) AS BASLN_NIV_AMT' ||CHR(10)||
              '  FROM (SELECT S_PROD_BASELINE.*, ORG.BU_ID, ORG.CURR_PRI_LST_ID,' ||CHR(10)||
              '               AA_PRMTN_PROD.ACCT_ID,' ||CHR(10)||
              '               AA_PRMTN_PROD.BUS_UNIT_ID' ||CHR(10)||
              '          FROM S_PROD_BASELINE,' ||CHR(10)||
              '               S_ORG_EXT ORG,' ||CHR(10)||
              '               OPT_BUS_UNIT_SKID_VW R,' ||CHR(10)||
              '               (SELECT DISTINCT' ||CHR(10)||
              '                       ACCT_ID,' ||CHR(10)||
              '                       BUS_UNIT_ID' ||CHR(10)||
              '                  FROM OPT_AA_PRMTN_PROD_TDADS) AA_PRMTN_PROD' ||CHR(10)||
              '         WHERE S_PROD_BASELINE.OU_EXT_ID = ORG.ROW_ID' ||CHR(10)||
              '           AND ORG.BU_ID = R.BUS_UNIT_ID' ||CHR(10)||
              '           AND CG_SKIP_FLG = ''N''' ||CHR(10)||
              '           AND ORG.ROW_ID = AA_PRMTN_PROD.ACCT_ID) S_PROD_BASELINE,' ||CHR(10)||
              '       S_PROD_INT PROD,' ||CHR(10)||
              '       S_PERIOD P,' ||CHR(10)||
              '       --S_PERIOD PERD,' ||CHR(10)||
              '       S_PRI_LST_ITEM ITEM' ||CHR(10)||
              ' WHERE S_PROD_BASELINE.PRDINT_ID = PROD.ROW_ID(+)' ||CHR(10)||
              '   AND S_PROD_BASELINE.PERIOD_ID = P.ROW_ID(+)' ||CHR(10)||
              '   --AND P.START_DT = PERD.START_DT' ||CHR(10)||
              '   --AND S_PROD_BASELINE.BUS_UNIT_ID = PERD.BU_ID' ||CHR(10)||
              '   --AND PERD.PERIOD_CD = ''Month''' ||CHR(10)||
              '   AND S_PROD_BASELINE.PERIOD_ID = ITEM.X_PERIOD_ID(+)' ||CHR(10)||
              '   AND S_PROD_BASELINE.PRDINT_ID = ITEM.PROD_ID(+)' ||CHR(10)||
              '   AND S_PROD_BASELINE.CURR_PRI_LST_ID = ITEM.PRI_LST_ID(+)';

    create_table(v_tbl_name, V_SQL);


    -- Process OPT_AA_ACCT_SHPMT_TFADS
    v_tbl_name := 'OPT_AA_ACCT_SHPMT_TFADS';

    V_SQL := 'CREATE TABLE OPT_AA_ACCT_SHPMT_TFADS ' ||CHR(10)||
              C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
              'parallel (degree '||in_dop||')'||CHR(10)||
              'NOLOGGING compress'||CHR(10)||
              'AS' ||CHR(10)||
               -- Purpose: create a acct, assoc acct Shipment
              'SELECT ACCT_HIER.ASSOC_ACCT_ID,' ||CHR(10)||
              '       SHPMT.PROD_ID,' ||CHR(10)||
              '       SHPMT.COMP_PROD_GTIN_CODE,' ||CHR(10)||
              '       SHPMT.BUS_UNIT_ID,' ||CHR(10)||
              '       SHPMT.SHPMT_DATE,' ||CHR(10)||
              '       SHPMT.VOL_SU_AMT,' ||CHR(10)||
              '       SHPMT.GIV_AMT,' ||CHR(10)||
              '       SHPMT.NIV_AMT,' ||CHR(10)||
              '       SHPMT.VOL_BUOM_AMT' ||CHR(10)||
              '  FROM OPT_SHPMT_FCT SHPMT, OPT_ACCT_ASDN_TYPE2_DIM ACCT_HIER' ||CHR(10)||
              ' WHERE ACCT_HIER.ACCT_SKID = SHPMT.ACCT_SKID' ||CHR(10)||
              '   AND SHPMT.SHPMT_DATE BETWEEN ACCT_HIER.ASDN_EFF_START_DATE AND' ||CHR(10)||
              '       ACCT_HIER.ASDN_EFF_END_DATE' ||CHR(10)||
              '   AND ACCT_HIER.ASSOC_ACCT_ID IN' ||CHR(10)||
              '       (SELECT ACCT_ID FROM OPT_AA_PRMTN_PROD_TDADS)';

    create_table(v_tbl_name, V_SQL);


    -- Process OPT_SVP_INCRM_TFADS
    v_tbl_name := 'OPT_SVP_INCRM_TFADS';

    V_SQL := 'CREATE TABLE OPT_SVP_INCRM_TFADS' ||CHR(10)||
              C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
              'parallel (degree '||in_dop||')'||CHR(10)||
              'NOLOGGING compress'||CHR(10)||
              'AS' ||CHR(10)||
              -- Purpose: get all SVP baseline related data
              -- The implementation is the same as Promoted Product SVP view
              -- please also refer to: OPT_SVP_BASE_TFADS
              '-- SVP Incremental for Short Promotion' ||CHR(10)||
              'SELECT PROMO_PROD.PRMTN_ID,' ||CHR(10)||
              '       PROMO_PROD.NAME AS PRMTN_NAME,' ||CHR(10)||
              '       PROMO_PROD.PR_ACCNT_ID AS ACCT_ID,' ||CHR(10)||
              '       PROMO_PROD.ROW_ID AS PRMTN_PROD_ID,' ||CHR(10)||
              '       PROMO_PROD.PROD_ID AS PROD_ID,' ||CHR(10)||
              '       S_PROD_BASELINE.PERIOD_ID AS PERD_ID,' ||CHR(10)||
              '       --PERD.ROW_ID AS PERD_ID,' ||CHR(10)||
              '       MAX(PERIOD.START_DT) AS PERD_START_DATE,' ||CHR(10)||
              '       MAX(PERIOD.END_DT) AS PERD_END_DATE,' ||CHR(10)||
              '       -- FOLLOWINGS ARE TAKEN FROM OPT_PRMTN_PROD_FCT_SVP_VW' ||CHR(10)||
              '       ROUND(NVL(SUM(DECODE(PROMO_PROD.X_INCREMENTAL_MODEL,' ||CHR(10)||
              '                            ''Percent'',' ||CHR(10)||
              '                            NVL(S_SRC_PRD_BASLN.PRD_BSLN_PCT *' ||CHR(10)||
              '                                S_PROD_BASELINE.PLANNED_SALES / 100 *' ||CHR(10)||
              '                                S_SRC_PRD_BASLN.X_INCREMENTAL_PCT / 100,' ||CHR(10)||
              '                                0),' ||CHR(10)||
              '                            NVL(S_SRC_PRD_BASLN.CG_INCR_SALES, 0))),' ||CHR(10)||
              '                 0),' ||CHR(10)||
              '             7) AS BASLN_INCR_SU_AMT,' ||CHR(10)||
              '       ROUND(NVL(SUM(DECODE(NVL(X_G_SUFCTOR, 0),' ||CHR(10)||
              '                            0,' ||CHR(10)||
              '                            0,' ||CHR(10)||
              '                            DECODE(PROMO_PROD.X_INCREMENTAL_MODEL,' ||CHR(10)||
              '                                   ''Percent'',' ||CHR(10)||
              '                                   NVL(S_SRC_PRD_BASLN.PRD_BSLN_PCT *' ||CHR(10)||
              '                                       S_PROD_BASELINE.PLANNED_SALES / 100 *' ||CHR(10)||
              '                                       S_SRC_PRD_BASLN.X_INCREMENTAL_PCT / 100,' ||CHR(10)||
              '                                       0),' ||CHR(10)||
              '                                   NVL(S_SRC_PRD_BASLN.CG_INCR_SALES, 0)) /' ||CHR(10)||
              '                            X_G_SUFCTOR)),' ||CHR(10)||
              '                 0),' ||CHR(10)||
              '             7) AS BASLN_INCR_BUOM_AMT,' ||CHR(10)||
              '       ROUND(NVL(SUM(DECODE(NVL(X_G_SUFCTOR, 0),' ||CHR(10)||
              '                            0,' ||CHR(10)||
              '                            0,' ||CHR(10)||
              '                            round(DECODE(PROMO_PROD.X_INCREMENTAL_MODEL,' ||CHR(10)|| -- add for B0038
              '                                   ''Percent'',' ||CHR(10)||
              '                                   NVL(S_SRC_PRD_BASLN.PRD_BSLN_PCT *' ||CHR(10)||
              '                                       S_PROD_BASELINE.PLANNED_SALES / 100 *' ||CHR(10)||
              '                                       S_SRC_PRD_BASLN.X_INCREMENTAL_PCT / 100,' ||CHR(10)||
              '                                       0),' ||CHR(10)||
              '                                   NVL(S_SRC_PRD_BASLN.CG_INCR_SALES, 0)) /' ||CHR(10)||
              '                            X_G_SUFCTOR, 7) * ITEM.DFLT_COST_UNIT)),' ||CHR(10)||
              '                 0),' ||CHR(10)||
              '             7) AS BASLN_INCR_GIV_AMT,' ||CHR(10)||
              '       ROUND(NVL(SUM(DECODE(PROMO_PROD.X_INCREMENTAL_MODEL,' ||CHR(10)||
              '                            ''Percent'',' ||CHR(10)||
              '                            NVL(S_SRC_PRD_BASLN.PRD_BSLN_PCT *' ||CHR(10)||
              '                                S_PROD_BASELINE.PLANNED_SALES / 100 *' ||CHR(10)||
              '                                S_SRC_PRD_BASLN.X_INCREMENTAL_PCT / 100,' ||CHR(10)||
              '                                0),' ||CHR(10)||
              '                            NVL(S_SRC_PRD_BASLN.CG_INCR_SALES, 0)) *' ||CHR(10)||
              '                     DECODE(NVL(S_PROD_BASELINE.X_NIVSU_CONV, 0),' ||CHR(10)||
              '                            0,' ||CHR(10)||
              '                            0,' ||CHR(10)||
              '                            S_PROD_BASELINE.X_NIVSU_CONV)),' ||CHR(10)||
              '                 0),' ||CHR(10)||
              '             7) AS BASLN_INCR_NIV_AMT' ||CHR(10)||
              '  FROM (SELECT PROMO.ROW_ID AS PRMTN_ID,' ||CHR(10)||
              '               PROMO.NAME,' ||CHR(10)||
              '               PROMO.PR_ACCNT_ID,' ||CHR(10)||
              '               PROMO_PROD.ROW_ID,' ||CHR(10)||
              '               PROMO_PROD.PROD_ID,' ||CHR(10)||
              '               CATEG.X_INCREMENTAL_MODEL,' ||CHR(10)||
              '               AA_PRMTN_PROD.BUS_UNIT_ID' ||CHR(10)||
              '          FROM S_SRC PROMO, S_SRC CATEG, S_SRC PROMO_PROD, OPT_BUS_UNIT_SKID_VW  R,' ||CHR(10)||
              '               (SELECT DISTINCT ACCT_ID, BUS_UNIT_ID FROM OPT_AA_PRMTN_PROD_TDADS) AA_PRMTN_PROD' ||CHR(10)||
              '         WHERE PROMO_PROD.SUB_TYPE = ''PLAN_ACCOUNT_PROMOTION_PRODUCT''' ||CHR(10)||
              '           AND CATEG.SUB_TYPE = ''PLAN_ACCT_PROMOTION_CATEGORY''' ||CHR(10)||
              '           AND PROMO.SUB_TYPE = ''PLAN_ACCOUNT_PROMOTION''' ||CHR(10)||
              '           AND CATEG.PAR_SRC_ID = PROMO.ROW_ID(+)' ||CHR(10)||
              '           AND PROMO_PROD.PAR_SRC_ID = CATEG.ROW_ID(+)' ||CHR(10)||
              '           AND PROMO.PR_ACCNT_ID = AA_PRMTN_PROD.ACCT_ID' ||CHR(10)||
              '           AND (PROMO.SRC_CD <> ''Annual Agreement'' OR PROMO.SRC_CD IS NULL)' ||CHR(10)||
              '           AND PROMO.STATUS_CD IN (''Planned'', ''Confirmed'', ''Completed'')' ||CHR(10)||
              '           AND NVL(PROMO.X_STOPPED_DT, PROMO.PROG_END_DT) > PROMO.PROG_START_DT' ||CHR(10)||
              '           AND PROMO.BU_ID = R.BUS_UNIT_ID) PROMO_PROD,' ||CHR(10)||
              '       S_SRC_PRD_BASLN,' ||CHR(10)||
              '       S_PROD_BASELINE,' ||CHR(10)||
              '       S_PROD_INT,' ||CHR(10)||
              '       S_PRI_LST_ITEM ITEM,' ||CHR(10)||
              '       --S_PERIOD PERD,' ||CHR(10)||
              '       S_PERIOD PERIOD' ||CHR(10)||
              ' WHERE PROMO_PROD.PROD_ID = S_PROD_INT.ROW_ID' ||CHR(10)||
              '   AND PROMO_PROD.ROW_ID = S_SRC_PRD_BASLN.SRC_ID' ||CHR(10)||
              '   AND S_SRC_PRD_BASLN.PROD_BASELINE_ID = S_PROD_BASELINE.ROW_ID' ||CHR(10)||
              '   AND ITEM.X_PERIOD_ID(+) = S_SRC_PRD_BASLN.X_PERIOD_ID' ||CHR(10)||
              '   AND ITEM.PROD_ID(+) = S_SRC_PRD_BASLN.X_PROD_ID' ||CHR(10)||
              '   AND ITEM.PRI_LST_ID(+) = S_SRC_PRD_BASLN.X_PRI_LST_ID' ||CHR(10)||
--              '   AND ITEM.X_PERIOD_ID = PERIOD.ROW_ID(+)' ||CHR(10)||
              '   AND S_PROD_BASELINE.PERIOD_ID = PERIOD.ROW_ID(+)' ||CHR(10)||
              '   --AND PERIOD.START_DT = PERD.START_DT' ||CHR(10)||
              '   --AND PROMO_PROD.BUS_UNIT_ID = PERD.BU_ID' ||CHR(10)||
              '   --AND PERD.PERIOD_CD = ''Month''' ||CHR(10)||
              ' GROUP BY PROMO_PROD.PRMTN_ID,' ||CHR(10)||
              '          PROMO_PROD.NAME,' ||CHR(10)||
              '          PROMO_PROD.PR_ACCNT_ID,' ||CHR(10)||
              '          PROMO_PROD.ROW_ID,' ||CHR(10)||
              '          PROMO_PROD.PROD_ID,' ||CHR(10)||
              '          S_PROD_BASELINE.PERIOD_ID';

    create_table(v_tbl_name, V_SQL);


    -- Process OPT_AA_PRMTN_GTIN_TFADS
    v_tbl_name := 'OPT_AA_PRMTN_GTIN_TFADS';

    V_SQL := 'CREATE TABLE OPT_AA_PRMTN_GTIN_TFADS' ||CHR(10)||
              C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
              'parallel (degree '||in_dop||')'||CHR(10)||
              'NOLOGGING compress'||CHR(10)||
              'AS -- AA Promotion Item GTIN Month level' ||CHR(10)||
              'WITH' ||CHR(10)||
              'SHPMT AS' ||CHR(10)||
              '(' ||CHR(10)||
              'SELECT V.PRMTN_ID,' ||CHR(10)||
              '       V.ACCT_ID,' ||CHR(10)||
              '       V.PRMTN_NAME,' ||CHR(10)||
              '       V.BUS_UNIT_ID,' ||CHR(10)||
              '       V.PROD_ID,' ||CHR(10)||
              '       V.PRMTN_PROD_ID,' ||CHR(10)||
              '       V.PGM_START_DATE,' ||CHR(10)||
              '       V.FY_DATE_SKID,' ||CHR(10)||
              '       V.BUOM_GTIN,' ||CHR(10)||
              '       V.DCMPD_REF_PROD_SKID,' ||CHR(10)||
              '       V.ITEM_GTIN,' ||CHR(10)||
              '       V.PERD_ID,' ||CHR(10)||
              '       V.PERD_START_DATE,' ||CHR(10)||
              '       V.PERD_END_DATE,' ||CHR(10)||
              '       V.ITEM_GTIN_PROD_SKID,' ||CHR(10)||
              '       V.ITEM_GTIN_PROD_NAME,' ||CHR(10)||
              '       V.ITEM_GTIN_QTY_FACTR,' ||CHR(10)||
              '       V.DCMPD_REF_PROD_GTIN,' ||CHR(10)||
              '       V.DCMPD_REF_PROD_GTIN_NAME,' ||CHR(10)||
              '       V.DCMPD_QTY_FACTR,' ||CHR(10)||
              '       V.BUOM_GTIN_PROD_SKID,' ||CHR(10)||
              '       V.PROD_BUOM_GTIN_NAME,            ' ||CHR(10)||
              '       V.SU_FACTR,' ||CHR(10)||
              '       V.VAL_FACTR,' ||CHR(10)||
              '       V.CMPLX_PROD_IND,' ||CHR(10)||
              '       V.SHORT_PRMTN_ID,' ||CHR(10)||
              '       V.SHORT_PRMTN_NAME,' ||CHR(10)||
              '       V.SHORT_PRMTN_ACCT_ID,' ||CHR(10)||
              '       V.SHORT_PRMTN_ACCT_NAME,' ||CHR(10)||
               -- Shipment Values are decomposed already, so, they are not multiplied by the SU/QTY/Value Factor
              '       SUM(V.VOL_SU_AMT ) AS ACTL_SU_AMT,' ||CHR(10)||
              '       SUM(V.VOL_BUOM_AMT * V.LGSTC_QTY_FACTR) AS ACTL_VOL_QTY,' ||CHR(10)||
              '       SUM(V.GIV_AMT ) AS ACTL_GIV_AMT,' ||CHR(10)||
              '       SUM(V.NIV_AMT ) AS ACTL_NIV_AMT' ||CHR(10)||
              '       FROM' ||CHR(10)||
              '(' ||CHR(10)||
              'SELECT ' ||CHR(10)||
              '       AA_PRMTN_PROD.PRMTN_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.ACCT_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PRMTN_NAME,' ||CHR(10)||
              '       AA_PRMTN_PROD.BUS_UNIT_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PROD_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PRMTN_PROD_ID AS PRMTN_PROD_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PGM_START_DATE,' ||CHR(10)||
              '       AA_PRMTN_PROD.FY_DATE_SKID,' ||CHR(10)||
              '       DCMPD_PROD_SHPMT.PROD_BUOM_GTIN AS BUOM_GTIN,' ||CHR(10)||
              '       DCMPD_PROD_SHPMT.DCMPD_REF_PROD_SKID,' ||CHR(10)||
              '       DCMPD_PROD_SHPMT.PROD_ITEM_GTIN AS ITEM_GTIN,' ||CHR(10)||
              '       (SELECT PERD.ROW_ID ' ||CHR(10)||
              '          FROM S_PERIOD PERD ' ||CHR(10)||
              '         WHERE PERD.BU_ID=AA_PRMTN_PROD.BUS_UNIT_ID' ||CHR(10)||
              '           AND TRUNC(SHPMT.SHPMT_DATE,''MM'') = PERD.START_DT' ||CHR(10)||
              '           AND PERD.PERIOD_CD = ''Month'' ) AS PERD_ID,' ||CHR(10)||
              '       (SELECT PERD.START_DT ' ||CHR(10)||
              '          FROM S_PERIOD PERD ' ||CHR(10)||
              '         WHERE PERD.BU_ID=AA_PRMTN_PROD.BUS_UNIT_ID' ||CHR(10)||
              '           AND TRUNC(SHPMT.SHPMT_DATE,''MM'') = PERD.START_DT' ||CHR(10)||
              '           AND PERD.PERIOD_CD = ''Month'' ) AS PERD_START_DATE,' ||CHR(10)||
              '       (SELECT PERD.END_DT ' ||CHR(10)||
              '          FROM S_PERIOD PERD ' ||CHR(10)||
              '         WHERE PERD.BU_ID=AA_PRMTN_PROD.BUS_UNIT_ID' ||CHR(10)||
              '           AND TRUNC(SHPMT.SHPMT_DATE,''MM'') = PERD.START_DT' ||CHR(10)||
              '           AND PERD.PERIOD_CD = ''Month'' ) AS PERD_END_DATE,' ||CHR(10)||
              '       DCMPD_PROD_SHPMT.ITEM_GTIN_PROD_SKID,' ||CHR(10)||
              '       DCMPD_PROD_SHPMT.ITEM_GTIN_PROD_NAME,' ||CHR(10)||
              '       DCMPD_PROD_SHPMT.LGSTC_QTY_FACTR AS ITEM_GTIN_QTY_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SHPMT.DCMPD_REF_PROD_GTIN,' ||CHR(10)||
              '       DCMPD_PROD_SHPMT.DCMPD_REF_PROD_NAME AS DCMPD_REF_PROD_GTIN_NAME,' ||CHR(10)||
              '       DCMPD_PROD_SHPMT.DCMPD_QTY_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SHPMT.BUOM_GTIN_PROD_SKID,' ||CHR(10)||
              '       DCMPD_PROD_SHPMT.BUOM_GTIN_PROD_NAME AS PROD_BUOM_GTIN_NAME,            ' ||CHR(10)||
              '       DCMPD_PROD_SHPMT.SU_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SHPMT.VAL_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SHPMT.CMPLX_PROD_IND,' ||CHR(10)||
              '       NULL AS SHORT_PRMTN_ID,' ||CHR(10)||
              '       NULL AS SHORT_PRMTN_NAME,' ||CHR(10)||
              '       NULL AS SHORT_PRMTN_ACCT_ID,' ||CHR(10)||
              '       NULL AS SHORT_PRMTN_ACCT_NAME,' ||CHR(10)||
              '       SHPMT.VOL_SU_AMT,' ||CHR(10)||
              '       SHPMT.VOL_BUOM_AMT,' ||CHR(10)||
              '       DCMPD_PROD_SHPMT.LGSTC_QTY_FACTR,' ||CHR(10)||
              '       SHPMT.GIV_AMT,' ||CHR(10)||
              '       SHPMT.NIV_AMT' ||CHR(10)||
              '  FROM OPT_AA_PRMTN_PROD_TDADS AA_PRMTN_PROD,' ||CHR(10)||
              '       --OPT_ACCT_HIER_TDADS ACCT_HIER,' ||CHR(10)||
              '       --OPT_AA_PROD_HIER_TDADS DCMPD_PROD_AA,' ||CHR(10)||
              '       OPT_AA_PROD_HIER_TDADS DCMPD_PROD_SHPMT,' ||CHR(10)||
              '       OPT_AA_ACCT_SHPMT_TFADS SHPMT' ||CHR(10)||
               -- Condition: "Shipment.Accounts Hierarchy"' ||CHR(10)||
              ' WHERE AA_PRMTN_PROD.ACCT_ID = SHPMT.ASSOC_ACCT_ID' ||CHR(10)||
               -- Condition: AA Promoted Product Hierarchy' ||CHR(10)||
               --Condition: Decomposed Shipment Product Hierarchy
               -- If "Shipment"."Component GTIN" is populated, Then join with "Decomposed Shipment Product.decomposed Prod GTIN"
               -- Else simply ignore this condition
              '   AND SHPMT.PROD_ID = DCMPD_PROD_SHPMT.PROD_ID' ||CHR(10)||
              '   AND NVL(SHPMT.COMP_PROD_GTIN_CODE, DCMPD_PROD_SHPMT.DCMPD_REF_PROD_GTIN) = DCMPD_PROD_SHPMT.DCMPD_REF_PROD_GTIN' ||CHR(10)||' ||CHR(10)||
              '   AND AA_PRMTN_PROD.BUS_UNIT_ID = DCMPD_PROD_SHPMT.BUS_UNIT_ID' ||CHR(10)||
              '   AND AA_PRMTN_PROD.FY_DATE_SKID = DCMPD_PROD_SHPMT.FY_DATE_SKID' ||CHR(10)||
               -- Join "Decomposed Shipment Product" with "Decomposed AA Product" on the "Item GTIN" level
              '   AND AA_PRMTN_PROD.PROD_ITEM_GTIN = DCMPD_PROD_SHPMT.PROD_ITEM_GTIN' ||CHR(10)||
               -- Condition: "Shipment.Date/Period"
              '   AND SHPMT.SHPMT_DATE BETWEEN AA_PRMTN_PROD.PGM_START_DATE AND AA_PRMTN_PROD.PGM_END_DATE' ||CHR(10)||' ||CHR(10)||
              '  ) V' ||CHR(10)||
              'GROUP BY V.PRMTN_ID,' ||CHR(10)||
              '       V.ACCT_ID,' ||CHR(10)||
              '       V.PRMTN_NAME,' ||CHR(10)||
              '       V.BUS_UNIT_ID,' ||CHR(10)||
              '       V.PROD_ID,' ||CHR(10)||
              '       V.PRMTN_PROD_ID,' ||CHR(10)||
              '       V.PGM_START_DATE,' ||CHR(10)||
              '       V.FY_DATE_SKID,' ||CHR(10)||
              '       V.BUOM_GTIN,' ||CHR(10)||
              '       V.DCMPD_REF_PROD_SKID,' ||CHR(10)||
              '       V.ITEM_GTIN,' ||CHR(10)||
              '       V.PERD_ID,' ||CHR(10)||
              '       V.PERD_START_DATE,' ||CHR(10)||
              '       V.PERD_END_DATE,' ||CHR(10)||
              '       V.ITEM_GTIN_PROD_SKID,' ||CHR(10)||
              '       V.ITEM_GTIN_PROD_NAME,' ||CHR(10)||
              '       V.ITEM_GTIN_QTY_FACTR,' ||CHR(10)||
              '       V.DCMPD_REF_PROD_GTIN,' ||CHR(10)||
              '       V.DCMPD_REF_PROD_GTIN_NAME,' ||CHR(10)||
              '       V.DCMPD_QTY_FACTR,' ||CHR(10)||
              '       V.BUOM_GTIN_PROD_SKID,' ||CHR(10)||
              '       V.PROD_BUOM_GTIN_NAME,            ' ||CHR(10)||
              '       V.SU_FACTR,' ||CHR(10)||
              '       V.VAL_FACTR,' ||CHR(10)||
              '       V.CMPLX_PROD_IND,' ||CHR(10)||
              '       V.SHORT_PRMTN_ID,' ||CHR(10)||
              '       V.SHORT_PRMTN_NAME,' ||CHR(10)||
              '       V.SHORT_PRMTN_ACCT_ID,' ||CHR(10)||
              '       V.SHORT_PRMTN_ACCT_NAME' ||CHR(10)||
              '),' ||CHR(10)||
              'BASE AS' ||CHR(10)||
              '(' ||CHR(10)||
              'SELECT ' ||CHR(10)||
              '       AA_PRMTN_PROD.PRMTN_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.ACCT_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PRMTN_NAME,' ||CHR(10)||
              '       AA_PRMTN_PROD.BUS_UNIT_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PROD_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PRMTN_PROD_ID AS PRMTN_PROD_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PGM_START_DATE,' ||CHR(10)||
              '       AA_PRMTN_PROD.FY_DATE_SKID,' ||CHR(10)||
              '       DCMPD_PROD_SVP.PROD_BUOM_GTIN AS BUOM_GTIN,' ||CHR(10)||
              '       DCMPD_PROD_SVP.DCMPD_REF_PROD_SKID,' ||CHR(10)||
              '       DCMPD_PROD_SVP.PROD_ITEM_GTIN AS ITEM_GTIN,' ||CHR(10)||
              '       PERD.ROW_ID AS PERD_ID,' ||CHR(10)||
              '       SVP_BASE.PERD_START_DATE,' ||CHR(10)||
              '       SVP_BASE.PERD_END_DATE,' ||CHR(10)||
              '       DCMPD_PROD_SVP.ITEM_GTIN_PROD_SKID,' ||CHR(10)||
              '       DCMPD_PROD_SVP.ITEM_GTIN_PROD_NAME,' ||CHR(10)||
              '       DCMPD_PROD_SVP.LGSTC_QTY_FACTR AS ITEM_GTIN_QTY_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SVP.DCMPD_REF_PROD_GTIN,' ||CHR(10)||
              '       DCMPD_PROD_SVP.DCMPD_REF_PROD_NAME AS DCMPD_REF_PROD_GTIN_NAME,' ||CHR(10)||
              '       DCMPD_PROD_SVP.DCMPD_QTY_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SVP.BUOM_GTIN_PROD_SKID,' ||CHR(10)||
              '       DCMPD_PROD_SVP.BUOM_GTIN_PROD_NAME AS PROD_BUOM_GTIN_NAME,' ||CHR(10)||
              '       DCMPD_PROD_SVP.SU_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SVP.VAL_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SVP.CMPLX_PROD_IND,' ||CHR(10)||
              '       NULL AS SHORT_PRMTN_ID,' ||CHR(10)||
              '       NULL AS SHORT_PRMTN_NAME,' ||CHR(10)||
              '       NULL AS SHORT_PRMTN_ACCT_ID,' ||CHR(10)||
              '       NULL AS SHORT_PRMTN_ACCT_NAME,' ||CHR(10)||
              '       SUM(SVP_BASE.BASLN_SU_AMT * DCMPD_PROD_SVP.SU_FACTR) AS ESTMT_BASE_VOL_SU,' ||CHR(10)||
              '       SUM(SVP_BASE.BASLN_BUOM_AMT * DCMPD_PROD_SVP.DCMPD_QTY_FACTR * DCMPD_PROD_SVP.LGSTC_QTY_FACTR) AS ESTMT_BASE_VOL_QTY,' ||CHR(10)||
              '       SUM(SVP_BASE.BASLN_GIV_AMT * DCMPD_PROD_SVP.VAL_FACTR) AS ESTMT_BASE_GIV_AMT,' ||CHR(10)||
              '       SUM(SVP_BASE.BASLN_NIV_AMT * DCMPD_PROD_SVP.VAL_FACTR) AS ESTMT_BASE_NIV_AMT' ||CHR(10)||
              '  FROM OPT_AA_PRMTN_PROD_TDADS AA_PRMTN_PROD,' ||CHR(10)||
              '       --OPT_ACCT_HIER_TDADS ACCT_HIER,' ||CHR(10)||
              '       --OPT_AA_PROD_HIER_TDADS DCMPD_PROD_AA,' ||CHR(10)||
              '       OPT_AA_PROD_HIER_TDADS DCMPD_PROD_SVP,' ||CHR(10)||
              '       OPT_SVP_BASE_TFADS SVP_BASE,' ||CHR(10)||
              '       S_PERIOD PERD' ||CHR(10)||
              ' --WHERE AA_PRMTN_PROD.ACCT_ID = ACCT_HIER.ASSOC_ACCT_ID' ||CHR(10)||
              ' WHERE AA_PRMTN_PROD.ACCT_ID = SVP_BASE.ACCT_ID' ||CHR(10)||
              '   --AND ACCT_HIER.ACCT_ID = SVP_BASE.ACCT_ID' ||CHR(10)||' ||CHR(10)||
              '   AND SVP_BASE.PROD_ID = DCMPD_PROD_SVP.PROD_ID' ||CHR(10)||' ||CHR(10)||
              '   AND AA_PRMTN_PROD.BUS_UNIT_ID = DCMPD_PROD_SVP.BUS_UNIT_ID' ||CHR(10)||
              '   AND AA_PRMTN_PROD.PROD_ITEM_GTIN = DCMPD_PROD_SVP.PROD_ITEM_GTIN' ||CHR(10)||
              '   AND AA_PRMTN_PROD.FY_DATE_SKID = DCMPD_PROD_SVP.FY_DATE_SKID' ||CHR(10)||
              '   AND SVP_BASE.PERD_START_DATE BETWEEN AA_PRMTN_PROD.PGM_START_DATE AND AA_PRMTN_PROD.PGM_END_DATE' ||CHR(10)||
              '   AND AA_PRMTN_PROD.BUS_UNIT_ID = PERD.BU_ID' ||CHR(10)||
              '   AND SVP_BASE.PERD_START_DATE = PERD.START_DT' ||CHR(10)||
              '   AND PERD.PERIOD_CD = ''Month''' ||CHR(10)||
              'GROUP BY AA_PRMTN_PROD.PRMTN_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.ACCT_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PRMTN_NAME,' ||CHR(10)||
              '       AA_PRMTN_PROD.BUS_UNIT_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PROD_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PRMTN_PROD_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PGM_START_DATE,' ||CHR(10)||
              '       AA_PRMTN_PROD.FY_DATE_SKID,' ||CHR(10)||
              '       DCMPD_PROD_SVP.PROD_BUOM_GTIN,' ||CHR(10)||
              '       DCMPD_PROD_SVP.DCMPD_REF_PROD_SKID,' ||CHR(10)||
              '       DCMPD_PROD_SVP.PROD_ITEM_GTIN,' ||CHR(10)||
              '       PERD.ROW_ID,' ||CHR(10)||
              '       SVP_BASE.PERD_START_DATE,' ||CHR(10)||
              '       SVP_BASE.PERD_END_DATE,' ||CHR(10)||
              '       DCMPD_PROD_SVP.ITEM_GTIN_PROD_SKID,' ||CHR(10)||
              '       DCMPD_PROD_SVP.ITEM_GTIN_PROD_NAME,' ||CHR(10)||
              '       DCMPD_PROD_SVP.LGSTC_QTY_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SVP.DCMPD_REF_PROD_GTIN,' ||CHR(10)||
              '       DCMPD_PROD_SVP.DCMPD_REF_PROD_NAME,' ||CHR(10)||
              '       DCMPD_PROD_SVP.DCMPD_QTY_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SVP.BUOM_GTIN_PROD_SKID,' ||CHR(10)||
              '       DCMPD_PROD_SVP.BUOM_GTIN_PROD_NAME,' ||CHR(10)||
              '       DCMPD_PROD_SVP.SU_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SVP.VAL_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SVP.CMPLX_PROD_IND' ||CHR(10)||
              '),' ||CHR(10)||
              'INCREMENTAL' ||CHR(10)||
              'AS' ||CHR(10)||
              '(' ||CHR(10)||
              'SELECT ' ||CHR(10)||
              '       AA_PRMTN_PROD.PRMTN_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.ACCT_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PRMTN_NAME,' ||CHR(10)||
              '       AA_PRMTN_PROD.BUS_UNIT_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PROD_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PRMTN_PROD_ID AS PRMTN_PROD_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PGM_START_DATE,' ||CHR(10)||
              '       AA_PRMTN_PROD.FY_DATE_SKID,' ||CHR(10)||
              '       DCMPD_PROD_SVP.PROD_BUOM_GTIN AS BUOM_GTIN,' ||CHR(10)||
              '       DCMPD_PROD_SVP.DCMPD_REF_PROD_SKID,' ||CHR(10)||
              '       DCMPD_PROD_SVP.PROD_ITEM_GTIN AS ITEM_GTIN,' ||CHR(10)||
              '       PERD.ROW_ID AS PERD_ID,' ||CHR(10)||
              '       SVP_INCRM.PERD_START_DATE,' ||CHR(10)||
              '       SVP_INCRM.PERD_END_DATE,       ' ||CHR(10)||
              '       DCMPD_PROD_SVP.ITEM_GTIN_PROD_SKID,' ||CHR(10)||
              '       DCMPD_PROD_SVP.ITEM_GTIN_PROD_NAME,' ||CHR(10)||
              '       DCMPD_PROD_SVP.LGSTC_QTY_FACTR AS ITEM_GTIN_QTY_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SVP.DCMPD_REF_PROD_GTIN,' ||CHR(10)||
              '       DCMPD_PROD_SVP.DCMPD_REF_PROD_NAME AS DCMPD_REF_PROD_GTIN_NAME,' ||CHR(10)||
              '       DCMPD_PROD_SVP.DCMPD_QTY_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SVP.BUOM_GTIN_PROD_SKID,' ||CHR(10)||
              '       DCMPD_PROD_SVP.BUOM_GTIN_PROD_NAME AS PROD_BUOM_GTIN_NAME,' ||CHR(10)||
              '       DCMPD_PROD_SVP.SU_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SVP.VAL_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SVP.CMPLX_PROD_IND,' ||CHR(10)||
              '       SVP_INCRM.PRMTN_ID AS SHORT_PRMTN_ID,' ||CHR(10)||
              '       SVP_INCRM.PRMTN_NAME AS SHORT_PRMTN_NAME,' ||CHR(10)||
              '       ACCT_HIER.ACCT_ID AS SHORT_PRMTN_ACCT_ID,' ||CHR(10)||
              '       ACCT_HIER.NAME AS SHORT_PRMTN_ACCT_NAME,' ||CHR(10)||
              '       SUM(SVP_INCRM.BASLN_INCR_SU_AMT * DCMPD_PROD_SVP.SU_FACTR) AS ESTMT_INCRM_SU_AMT,' ||CHR(10)||
              '       SUM(SVP_INCRM.BASLN_INCR_BUOM_AMT * DCMPD_PROD_SVP.DCMPD_QTY_FACTR * DCMPD_PROD_SVP.LGSTC_QTY_FACTR) AS ESTMT_INCRM_VOL_QTY,' ||CHR(10)||
              '       SUM(SVP_INCRM.BASLN_INCR_GIV_AMT * DCMPD_PROD_SVP.VAL_FACTR) AS ESTMT_INCRM_GIV_AMT,' ||CHR(10)||
              '       SUM(SVP_INCRM.BASLN_INCR_NIV_AMT * DCMPD_PROD_SVP.VAL_FACTR) AS ESTMT_INCRM_NIV_AMT' ||CHR(10)||
              '  FROM OPT_AA_PRMTN_PROD_TDADS AA_PRMTN_PROD,' ||CHR(10)||
              '       OPT_ACCT_DIM ACCT_HIER,' ||CHR(10)||
              '       --OPT_AA_PROD_HIER_TDADS DCMPD_PROD_AA,' ||CHR(10)||
              '       OPT_AA_PROD_HIER_TDADS DCMPD_PROD_SVP,' ||CHR(10)||
              '       OPT_SVP_INCRM_TFADS SVP_INCRM,' ||CHR(10)||
              '       S_PERIOD PERD' ||CHR(10)||
              ' --WHERE AA_PRMTN_PROD.ACCT_ID = ACCT_HIER.ASSOC_ACCT_ID' ||CHR(10)||
              '   --AND ACCT_HIER.ACCT_ID = SVP_INCRM.ACCT_ID' ||CHR(10)||
              ' WHERE AA_PRMTN_PROD.ACCT_ID = SVP_INCRM.ACCT_ID' ||CHR(10)||
              '   AND AA_PRMTN_PROD.ACCT_ID = ACCT_HIER.ACCT_ID' ||CHR(10)||' ||CHR(10)||
              '   AND SVP_INCRM.PROD_ID = DCMPD_PROD_SVP.PROD_ID' ||CHR(10)||' ||CHR(10)||
              '   AND AA_PRMTN_PROD.BUS_UNIT_ID = DCMPD_PROD_SVP.BUS_UNIT_ID' ||CHR(10)||
              '   AND AA_PRMTN_PROD.PROD_ITEM_GTIN = DCMPD_PROD_SVP.PROD_ITEM_GTIN' ||CHR(10)||
              '   AND AA_PRMTN_PROD.FY_DATE_SKID = DCMPD_PROD_SVP.FY_DATE_SKID' ||CHR(10)||
              '   AND SVP_INCRM.PERD_START_DATE BETWEEN AA_PRMTN_PROD.PGM_START_DATE AND AA_PRMTN_PROD.PGM_END_DATE' ||CHR(10)||
              '   AND AA_PRMTN_PROD.BUS_UNIT_ID = PERD.BU_ID' ||CHR(10)||
              '   AND SVP_INCRM.PERD_START_DATE = PERD.START_DT' ||CHR(10)||
              '   AND PERD.PERIOD_CD = ''Month''' ||CHR(10)||
              'GROUP BY AA_PRMTN_PROD.PRMTN_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.ACCT_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PRMTN_NAME,' ||CHR(10)||
              '       AA_PRMTN_PROD.BUS_UNIT_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PROD_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PRMTN_PROD_ID,' ||CHR(10)||
              '       AA_PRMTN_PROD.PGM_START_DATE,' ||CHR(10)||
              '       DCMPD_PROD_SVP.PROD_BUOM_GTIN,' ||CHR(10)||
              '       DCMPD_PROD_SVP.DCMPD_REF_PROD_SKID,' ||CHR(10)||
              '       DCMPD_PROD_SVP.PROD_ITEM_GTIN,' ||CHR(10)||
              '       PERD.ROW_ID,' ||CHR(10)||
              '       SVP_INCRM.PERD_START_DATE,' ||CHR(10)||
              '       SVP_INCRM.PERD_END_DATE,' ||CHR(10)||
              '       DCMPD_PROD_SVP.ITEM_GTIN_PROD_SKID,' ||CHR(10)||
              '       DCMPD_PROD_SVP.ITEM_GTIN_PROD_NAME,' ||CHR(10)||
              '       DCMPD_PROD_SVP.LGSTC_QTY_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SVP.DCMPD_REF_PROD_GTIN,' ||CHR(10)||
              '       DCMPD_PROD_SVP.DCMPD_REF_PROD_NAME,' ||CHR(10)||
              '       DCMPD_PROD_SVP.DCMPD_QTY_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SVP.BUOM_GTIN_PROD_SKID,' ||CHR(10)||
              '       DCMPD_PROD_SVP.BUOM_GTIN_PROD_NAME,' ||CHR(10)||
              '       DCMPD_PROD_SVP.SU_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SVP.VAL_FACTR,' ||CHR(10)||
              '       DCMPD_PROD_SVP.CMPLX_PROD_IND,' ||CHR(10)||
              '       SVP_INCRM.PRMTN_ID,' ||CHR(10)||
              '       SVP_INCRM.PRMTN_NAME,' ||CHR(10)||
              '       ACCT_HIER.ACCT_ID,' ||CHR(10)||
              '       ACCT_HIER.NAME,' ||CHR(10)||
              '       AA_PRMTN_PROD.FY_DATE_SKID' ||CHR(10)||
              ')' ||CHR(10)||
              '-- Final SQL' ||CHR(10)||
              'SELECT PRMTN_ID,' ||CHR(10)||
              '       PRMTN_NAME,' ||CHR(10)||
              '       ACCT_ID,' ||CHR(10)||
              '       BUS_UNIT_ID,' ||CHR(10)||
              '       PROD_ID,' ||CHR(10)||
              '       BUOM_GTIN,' ||CHR(10)||
              '       DCMPD_REF_PROD_SKID,' ||CHR(10)||
              '       ITEM_GTIN,' ||CHR(10)||
              '       PRMTN_PROD_ID,' ||CHR(10)||
              '       PERD_ID,' ||CHR(10)||
              '       PERD_START_DATE,' ||CHR(10)||
              '       PERD_END_DATE,' ||CHR(10)||
              '       FY_DATE_SKID,' ||CHR(10)||
              '       ITEM_GTIN_PROD_SKID,' ||CHR(10)||
              '       ITEM_GTIN_PROD_NAME,' ||CHR(10)||
              '       ITEM_GTIN_QTY_FACTR,' ||CHR(10)||
              '       DCMPD_REF_PROD_GTIN,' ||CHR(10)||
              '       DCMPD_REF_PROD_GTIN_NAME,' ||CHR(10)||
              '       DCMPD_QTY_FACTR,' ||CHR(10)||
              '       BUOM_GTIN_PROD_SKID,' ||CHR(10)||
              '       PROD_BUOM_GTIN_NAME,' ||CHR(10)||
              '       SU_FACTR,' ||CHR(10)||
              '       VAL_FACTR,' ||CHR(10)||
              '       CMPLX_PROD_IND,' ||CHR(10)||
              '       SHORT_PRMTN_ID,' ||CHR(10)||
              '       SHORT_PRMTN_NAME,' ||CHR(10)||
              '       SHORT_PRMTN_ACCT_ID,' ||CHR(10)||
              '       SHORT_PRMTN_ACCT_NAME,' ||CHR(10)||
              '       SUM(ESTMT_BASE_VOL_SU) AS ESTMT_BASE_VOL_SU,' ||CHR(10)||
              '       SUM(ESTMT_INCRM_SU_AMT) AS ESTMT_INCRM_SU_AMT,' ||CHR(10)||
              '       SUM(ESTMT_BASE_VOL_QTY) AS ESTMT_BASE_VOL_QTY,' ||CHR(10)||
              '       SUM(ESTMT_INCRM_VOL_QTY) AS ESTMT_INCRM_VOL_QTY,' ||CHR(10)||
              '       SUM(ESTMT_BASE_GIV_AMT) AS ESTMT_BASE_GIV_AMT,' ||CHR(10)||
              '       SUM(ESTMT_INCRM_GIV_AMT) AS ESTMT_INCRM_GIV_AMT,' ||CHR(10)||
              '       SUM(ESTMT_BASE_NIV_AMT) AS ESTMT_BASE_NIV_AMT,' ||CHR(10)||
              '       SUM(ESTMT_INCRM_NIV_AMT) AS ESTMT_INCRM_NIV_AMT,' ||CHR(10)||
              '       SUM(ACTL_SU_AMT) AS ACTL_SU_AMT,' ||CHR(10)||
              '       SUM(ACTL_VOL_QTY) AS ACTL_VOL_QTY,' ||CHR(10)||
              '       SUM(ACTL_GIV_AMT) AS ACTL_GIV_AMT,' ||CHR(10)||
              '       SUM(ACTL_NIV_AMT) AS ACTL_NIV_AMT' ||CHR(10)||
              '  FROM (--Shipment' ||CHR(10)||
              '        SELECT PRMTN_ID,' ||CHR(10)||
              '               PRMTN_NAME,' ||CHR(10)||
              '               ACCT_ID,' ||CHR(10)||
              '               BUS_UNIT_ID,' ||CHR(10)||
              '               PROD_ID,' ||CHR(10)||
              '               BUOM_GTIN,' ||CHR(10)||
              '               DCMPD_REF_PROD_SKID,' ||CHR(10)||
              '               ITEM_GTIN,' ||CHR(10)||
              '               PRMTN_PROD_ID,' ||CHR(10)||
              '               PERD_ID,' ||CHR(10)||
              '               PERD_START_DATE,' ||CHR(10)||
              '               PERD_END_DATE,' ||CHR(10)||
              '               FY_DATE_SKID,' ||CHR(10)||
              '               ITEM_GTIN_PROD_SKID,' ||CHR(10)||
              '               ITEM_GTIN_PROD_NAME,' ||CHR(10)||
              '               ITEM_GTIN_QTY_FACTR,' ||CHR(10)||
              '               DCMPD_REF_PROD_GTIN,' ||CHR(10)||
              '               DCMPD_REF_PROD_GTIN_NAME,' ||CHR(10)||
              '               DCMPD_QTY_FACTR,' ||CHR(10)||
              '               BUOM_GTIN_PROD_SKID,' ||CHR(10)||
              '               PROD_BUOM_GTIN_NAME,' ||CHR(10)||
              '               SU_FACTR,' ||CHR(10)||
              '               VAL_FACTR,' ||CHR(10)||
              '               CMPLX_PROD_IND,' ||CHR(10)||
              '               SHORT_PRMTN_ID,' ||CHR(10)||
              '               SHORT_PRMTN_NAME,' ||CHR(10)||
              '               SHORT_PRMTN_ACCT_ID,' ||CHR(10)||
              '               SHORT_PRMTN_ACCT_NAME,' ||CHR(10)||
              '               0 AS ESTMT_BASE_VOL_SU,' ||CHR(10)||
              '               0 AS ESTMT_INCRM_SU_AMT,' ||CHR(10)||
              '               0 AS ESTMT_BASE_VOL_QTY,' ||CHR(10)||
              '               0 AS ESTMT_INCRM_VOL_QTY,' ||CHR(10)||
              '               0 AS ESTMT_BASE_GIV_AMT,' ||CHR(10)||
              '               0 AS ESTMT_INCRM_GIV_AMT,' ||CHR(10)||
              '               0 AS ESTMT_BASE_NIV_AMT,' ||CHR(10)||
              '               0 AS ESTMT_INCRM_NIV_AMT,' ||CHR(10)||
              '               ACTL_SU_AMT,' ||CHR(10)||
              '               ACTL_VOL_QTY,' ||CHR(10)||
              '               ACTL_GIV_AMT,' ||CHR(10)||
              '               ACTL_NIV_AMT' ||CHR(10)||
              '          FROM SHPMT' ||CHR(10)||
              '        UNION' ||CHR(10)||
              '        --SVP Base' ||CHR(10)||
              '        SELECT PRMTN_ID,' ||CHR(10)||
              '               PRMTN_NAME,' ||CHR(10)||
              '               ACCT_ID,' ||CHR(10)||
              '               BUS_UNIT_ID,' ||CHR(10)||
              '               PROD_ID,' ||CHR(10)||
              '               BUOM_GTIN,' ||CHR(10)||
              '               DCMPD_REF_PROD_SKID,' ||CHR(10)||
              '               ITEM_GTIN,' ||CHR(10)||
              '               PRMTN_PROD_ID,' ||CHR(10)||
              '               PERD_ID,' ||CHR(10)||
              '               PERD_START_DATE,' ||CHR(10)||
              '               PERD_END_DATE,' ||CHR(10)||
              '               FY_DATE_SKID,' ||CHR(10)||
              '               ITEM_GTIN_PROD_SKID,' ||CHR(10)||
              '               ITEM_GTIN_PROD_NAME,' ||CHR(10)||
              '               ITEM_GTIN_QTY_FACTR,' ||CHR(10)||
              '               DCMPD_REF_PROD_GTIN,' ||CHR(10)||
              '               DCMPD_REF_PROD_GTIN_NAME,' ||CHR(10)||
              '               DCMPD_QTY_FACTR,' ||CHR(10)||
              '               BUOM_GTIN_PROD_SKID,' ||CHR(10)||
              '               PROD_BUOM_GTIN_NAME,' ||CHR(10)||
              '               SU_FACTR,' ||CHR(10)||
              '               VAL_FACTR,' ||CHR(10)||
              '               CMPLX_PROD_IND,' ||CHR(10)||
              '               SHORT_PRMTN_ID,' ||CHR(10)||
              '               SHORT_PRMTN_NAME,' ||CHR(10)||
              '               SHORT_PRMTN_ACCT_ID,' ||CHR(10)||
              '               SHORT_PRMTN_ACCT_NAME,' ||CHR(10)||
              '               ESTMT_BASE_VOL_SU,' ||CHR(10)||
              '               0 AS ESTMT_INCRM_SU_AMT,' ||CHR(10)||
              '               ESTMT_BASE_VOL_QTY,' ||CHR(10)||
              '               0 AS ESTMT_INCRM_VOL_QTY,' ||CHR(10)||
              '               ESTMT_BASE_GIV_AMT,' ||CHR(10)||
              '               0 AS ESTMT_INCRM_GIV_AMT,' ||CHR(10)||
              '               ESTMT_BASE_NIV_AMT,' ||CHR(10)||
              '               0 AS ESTMT_INCRM_NIV_AMT,' ||CHR(10)||
              '               0 AS ACTL_SU_AMT,' ||CHR(10)||
              '               0 AS ACTL_VOL_QTY,' ||CHR(10)||
              '               0 AS ACTL_GIV_AMT,' ||CHR(10)||
              '               0 AS ACTL_NIV_AMT' ||CHR(10)||
              '          FROM BASE' ||CHR(10)||
              '        UNION' ||CHR(10)||
              '        --SVP Incremental' ||CHR(10)||
              '        SELECT PRMTN_ID,' ||CHR(10)||
              '               PRMTN_NAME,' ||CHR(10)||
              '               ACCT_ID,' ||CHR(10)||
              '               BUS_UNIT_ID,' ||CHR(10)||
              '               PROD_ID,' ||CHR(10)||
              '               BUOM_GTIN,' ||CHR(10)||
              '               DCMPD_REF_PROD_SKID,' ||CHR(10)||
              '               ITEM_GTIN,' ||CHR(10)||
              '               PRMTN_PROD_ID,' ||CHR(10)||
              '               PERD_ID,' ||CHR(10)||
              '               PERD_START_DATE,' ||CHR(10)||
              '               PERD_END_DATE,' ||CHR(10)||
              '               FY_DATE_SKID,' ||CHR(10)||
              '               ITEM_GTIN_PROD_SKID,' ||CHR(10)||
              '               ITEM_GTIN_PROD_NAME,' ||CHR(10)||
              '               ITEM_GTIN_QTY_FACTR,' ||CHR(10)||
              '               DCMPD_REF_PROD_GTIN,' ||CHR(10)||
              '               DCMPD_REF_PROD_GTIN_NAME,' ||CHR(10)||
              '               DCMPD_QTY_FACTR,' ||CHR(10)||
              '               BUOM_GTIN_PROD_SKID,' ||CHR(10)||
              '               PROD_BUOM_GTIN_NAME,' ||CHR(10)||
              '               SU_FACTR,' ||CHR(10)||
              '               VAL_FACTR,' ||CHR(10)||
              '               CMPLX_PROD_IND,' ||CHR(10)||
              '               SHORT_PRMTN_ID,' ||CHR(10)||
              '               SHORT_PRMTN_NAME,' ||CHR(10)||
              '               SHORT_PRMTN_ACCT_ID,' ||CHR(10)||
              '               SHORT_PRMTN_ACCT_NAME,' ||CHR(10)||
              '               0 AS ESTMT_BASE_VOL_SU,' ||CHR(10)||
              '               ESTMT_INCRM_SU_AMT,' ||CHR(10)||
              '               0 AS ESTMT_BASE_VOL_QTY,' ||CHR(10)||
              '               ESTMT_INCRM_VOL_QTY,' ||CHR(10)||
              '               0 AS ESTMT_BASE_GIV_AMT,' ||CHR(10)||
              '               ESTMT_INCRM_GIV_AMT,' ||CHR(10)||
              '               0 AS ESTMT_BASE_NIV_AMT,' ||CHR(10)||
              '               ESTMT_INCRM_NIV_AMT,' ||CHR(10)||
              '               0 AS ACTL_SU_AMT,' ||CHR(10)||
              '               0 AS ACTL_VOL_QTY,' ||CHR(10)||
              '               0 AS ACTL_GIV_AMT,' ||CHR(10)||
              '               0 AS ACTL_NIV_AMT' ||CHR(10)||
              '          FROM INCREMENTAL)' ||CHR(10)||
              'GROUP BY PRMTN_ID, PRMTN_NAME, ACCT_ID, BUS_UNIT_ID, PROD_ID, BUOM_GTIN, DCMPD_REF_PROD_SKID, ITEM_GTIN, PRMTN_PROD_ID, ' ||CHR(10)||
              '         PERD_ID, PERD_START_DATE, PERD_END_DATE, FY_DATE_SKID,' ||CHR(10)||
              '         ITEM_GTIN_PROD_SKID, ITEM_GTIN_PROD_NAME, ITEM_GTIN_QTY_FACTR,' ||CHR(10)||
              '         DCMPD_REF_PROD_GTIN, DCMPD_REF_PROD_GTIN_NAME, DCMPD_QTY_FACTR,' ||CHR(10)||
              '         BUOM_GTIN_PROD_SKID, PROD_BUOM_GTIN_NAME, SU_FACTR, VAL_FACTR, CMPLX_PROD_IND,' ||CHR(10)||
              '         SHORT_PRMTN_ID, SHORT_PRMTN_NAME, SHORT_PRMTN_ACCT_ID, SHORT_PRMTN_ACCT_NAME';

    create_table(v_tbl_name, V_SQL);


    pro_put_line('Create temp tables for ' ||in_tbl_name|| ' (END)');
    --********************************************************/

  WHEN 'H_TFM_OPT_FUND_FCST_CALC_FULL' THEN
      opt_load_stage_data.load_data('OPT_FUND_FRCST_FCT',V_REGN);--SOX IMPLEMENTATION PROJECT


/* Remarked by bean, for standardized control by metadata. 20091028
   V_SQL:='CREATE TABLE H_TFM_OPT_FUND_FCST_CALC_FULL AS SELECT * FROM H_TFM_OPT_FUND_FCST_CALC WHERE ROWNUM<1';
    v_tbl_name := UPPER(in_tbl_name);
    create_table_wise(v_tbl_name, V_SQL);

    --Performance tunning for FUND_FRCST_FCT in R8, Added by David
    --Update table H_TFM_OPT_FUND_FCST_CALC_FULL
    pro_put_line('Update table ' || v_tbl_name || ' (BEGIN)');

        V_SQL:='DELETE FROM H_TFM_OPT_FUND_FCST_CALC_FULL
     WHERE ACCNT_FUND_ID IN
      (SELECT ACCNT_FUND_ID -- refresh
       FROM H_TFM_OPT_FUND_FCST_CALC
       UNION -- fund is over
       SELECT ACCNT_FUND_ID
       FROM H_TFM_OPT_FUND_FCST_CALC_FULL FUND_FCST
       WHERE EXISTS
        (SELECT '' X ''
              FROM
                 (SELECT \*+ USE_HASH(MDF ACCRL *\
                     MDF.ROW_ID
                    FROM (SELECT ROW_ID,
                                                  NVL(ROUND(X_TOTAL_FORECAST_AMT, 2), 0) AS TOTAL_FRCST_AMT
                                  FROM S_MDF
                                  WHERE FUND_CD = '' ACCOUNT FUND '') MDF,
                                (SELECT MDF_ID,
                                 ROUND(SUM(ACCRL_FUND_AMT), 2) AS ACCRL_AMT
                                 FROM OPT_ACCRL_PROD_IFCT
                                 GROUP BY MDF_ID) ACCRL
                    WHERE MDF.ROW_ID = ACCRL.MDF_ID(+)
                    AND (NVL(MDF.TOTAL_FRCST_AMT, 0) = 0 OR (MDF.TOTAL_FRCST_AMT - NVL(ACCRL.ACCRL_AMT, 0)) = 0)
                    ) V
              WHERE V.ROW_ID = FUND_FCST.ACCNT_FUND_ID)
       UNION -- Fund be deleted
       SELECT ACCNT_FUND_ID
       FROM H_TFM_OPT_FUND_FCST_CALC_FULL FUND_FCST
       WHERE NOT EXISTS (SELECT '' X ''
              FROM S_MDF
              WHERE S_MDF.ROW_ID = FUND_FCST.ACCNT_FUND_ID))';

    EXECUTE IMMEDIATE V_SQL;

    V_SQL:='INSERT\*+APPEND*\ INTO H_TFM_OPT_FUND_FCST_CALC_FULL SELECT * FROM H_TFM_OPT_FUND_FCST_CALC';

    EXECUTE IMMEDIATE V_SQL;
    COMMIT;

     -- Statistics
    pro_put_line('Gather statistics for table ' || v_tbl_name || ' (BEGIN)');
    DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                  TABNAME          => v_tbl_name);
    pro_put_line('Gather statistics for table ' || v_tbl_name || ' (END)');

    pro_put_line('Update table ' || v_tbl_name ||' (END)');

    --Performance tunning for FUND_FRCST_FCT in R8, Added by David
  WHEN 'H_TFM_OPT_FUND_MASTER_FULL' THEN

    V_SQL:='CREATE TABLE H_TFM_OPT_FUND_MASTER_FULL AS SELECT * FROM H_TFM_OPT_FUND_MASTER WHERE ROWNUM<1';
    v_tbl_name := UPPER(in_tbl_name);
    create_table_wise(v_tbl_name, V_SQL);

    --Update table H_TFM_OPT_FUND_MASTER_FULL
    pro_put_line('Update table ' || v_tbl_name || ' (BEGIN)');

    V_SQL:='DELETE FROM H_TFM_OPT_FUND_MASTER_FULL'||CHR(10)||
           'WHERE ACCNT_FUND_ID IN (SELECT DISTINCT ACCNT_FUND_ID FROM H_TFM_OPT_FUND_MASTER)';

    EXECUTE IMMEDIATE V_SQL;

    V_SQL:='INSERT \*+APPEND*\  INTO H_TFM_OPT_FUND_MASTER_FULL SELECT * FROM H_TFM_OPT_FUND_MASTER';

    EXECUTE IMMEDIATE V_SQL;

    COMMIT;

    -- Statistics
    pro_put_line('Gather statistics for table ' || v_tbl_name || ' (BEGIN)');
    DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                  TABNAME          => v_tbl_name);
    pro_put_line('Gather statistics for table ' || v_tbl_name || ' (END)');

    --Added by David for performace tuning
    --*****************************************************************************
    BEGIN
    SELECT COUNT(1) INTO v_cnt from user_tables WHERE table_name = 'PROD_HIER_TMP';
    IF v_cnt > 0 THEN
        pro_put_line('The table PROD_HIER_TMP was existed already (END)');
        V_SQL:='DROP TABLE PROD_HIER_TMP';
        EXECUTE IMMEDIATE V_SQL;
    END IF;
    END;

    pro_put_line('Create temp table PROD_HIER_TMP (BEGIN)');
    V_SQL:='CREATE TABLE PROD_HIER_TMP'||CHR(10)||
                   C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
                   'parallel (degree '||in_dop||')'||CHR(10)||
                   'NOLOGGING compress'||CHR(10)||
                    'AS  SELECT PROD_PARNT.PROD_ID AS PARNT_ID,
                             FUND_PROD.GTIN_ID,
                             D.CTLG_ST_DT,
                             D.CTLG_EN_DT
                        FROM (SELECT DISTINCT FUND_PROD.GTIN_ID
                                FROM H_TFM_OPT_FUND_FCST_CALC_FULL FUND_PROD,
                                     H_TFM_OPT_FUND_MASTER_FULL FUND
                               WHERE FUND_PROD.ACCNT_FUND_ID = FUND.ACCNT_FUND_ID
                                 AND TRIM(NVL(FUND.X_FCST_MANUAL_UPD_FLG,''N''))=''N''
                                 AND FUND.X_FREEZE_FLG=''N'') FUND_PROD,
                             OPT_PROD_DIM PROD_CHILD,
                             OPT_PROD_ASDN_DIM PROD_HIER,
                             OPT_PROD_DIM PROD_PARNT,
                             (SELECT FISC_YR_SKID,
                                     MIN(DAY_DATE) AS CTLG_ST_DT,
                                     MAX(DAY_DATE) CTLG_EN_DT
                                FROM CAL_MASTR_DIM
                               GROUP BY FISC_YR_SKID) D
                       WHERE FUND_PROD.GTIN_ID = PROD_CHILD.PROD_ID
                         AND PROD_CHILD.PROD_SKID = PROD_HIER.PROD_SKID
                         AND PROD_HIER.ROOT_PROD_SKID = PROD_PARNT.PROD_SKID
                         AND PROD_HIER.FY_DATE_SKID = D.FISC_YR_SKID
                         AND D.CTLG_EN_DT >= TRUNC(SYSDATE, ''MM'')';

    EXECUTE IMMEDIATE V_SQL;

    pro_put_line('Gather statistics for table PROD_HIER_TMP (BEGIN)');
    DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                  TABNAME          => 'PROD_HIER_TMP');
    pro_put_line('Gather statistics for table PROD_HIER_TMP (END)');

    pro_put_line('Create temp table PROD_HIER_TMP (END)');

    BEGIN
    SELECT COUNT(1) INTO v_cnt from user_tables WHERE table_name = 'PROD_RELATE_TMP';
    IF v_cnt > 0 THEN
        pro_put_line('The table PROD_RELATE_TMP was existed already (END)');
        V_SQL:='DROP TABLE PROD_RELATE_TMP';
        EXECUTE IMMEDIATE V_SQL;
    END IF;
    END;

    pro_put_line('Create temp table PROD_RELATE_TMP (BEGIN)');
    V_SQL:='CREATE TABLE PROD_RELATE_TMP'|| CHR(10)||
                    C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
                    'parallel (degree '||in_dop||')'||CHR(10)||
                    'NOLOGGING compress'||CHR(10)||
                    'AS SELECT FUND.ROW_ID           AS ACCNT_FUND_ID,
                             FUND.BU_ID            AS BU_ID,
                             FUND_PROD.X_PRDINT_ID AS PROD_ID,
                             FUND_PROD.X_START_DT  AS START_DT,
                             FUND_PROD.X_END_DT    AS END_DT,
                             FUND_PROD.X_FUND_RATE AS FUND_RATE,
                             MAX(FUND_PROD.ROW_ID) AS FUND_PROD_ID
                        FROM S_MDF_XM FUND_PROD,
                             S_MDF FUND,
                             H_TFM_OPT_FUND_MASTER_FULL FUND_MASTER
                       WHERE FUND_PROD.TYPE = ''Accrual''
                         AND FUND.FUND_CD = ''Account Fund''
                         AND FUND.X_TOTAL_FORECAST_AMT <> 0
                         AND FUND_PROD.PAR_ROW_ID = FUND.ROW_ID
                         AND FUND.ROW_ID = FUND_MASTER.ACCNT_FUND_ID
                         AND TRIM(NVL(FUND_MASTER.X_FCST_MANUAL_UPD_FLG,''N''))=''N''
                         AND FUND_MASTER.X_FREEZE_FLG=''N''
                       GROUP BY FUND.ROW_ID, FUND.BU_ID, FUND_PROD.X_PRDINT_ID, FUND_PROD.X_START_DT, FUND_PROD.X_END_DT, FUND_PROD.X_FUND_RATE
                       UNION
                      -- Fund Group Products
                      SELECT FUND.ROW_ID           AS ACCNT_FUND_ID,
                             FUND.BU_ID            AS BU_ID,
                             FUND_PROD.X_PRDINT_ID AS PROD_ID,
                             FUND_PROD.X_START_DT  AS START_DT,
                             FUND_PROD.X_END_DT    AS END_DT,
                             FUND_PROD.X_FUND_RATE AS FUND_RATE,
                             MAX(FUND_PROD.ROW_ID) AS FUND_PROD_ID
                        FROM S_MDF_XM FUND_PROD,
                             S_MDF FUND,
                             H_TFM_OPT_FUND_MASTER_FULL FUND_MASTER
                       WHERE FUND_PROD.TYPE = ''Accrual''
                         AND FUND.FUND_CD = ''Account Fund''
                         AND FUND.X_TOTAL_FORECAST_AMT <> 0
                         AND FUND_PROD.PAR_ROW_ID = FUND.ROOT_MDF_ID
                         -- If there are Account Fund Products, then will not show Fund Group products
                         AND FUND.ROW_ID NOT IN (SELECT FUND.ROW_ID
                                                   FROM S_MDF_XM FUND_PROD, S_MDF FUND
                                                  WHERE FUND_PROD.TYPE = ''Accrual''
                                                    AND FUND.FUND_CD = ''Account Fund''
                                                    AND FUND_PROD.PAR_ROW_ID = FUND.ROW_ID
                                                 )
                         AND FUND.ROW_ID = FUND_MASTER.ACCNT_FUND_ID
                         AND TRIM(NVL(FUND_MASTER.X_FCST_MANUAL_UPD_FLG,''N''))=''N''
                         AND FUND_MASTER.X_FREEZE_FLG=''N''
                       GROUP BY FUND.ROW_ID, FUND.BU_ID, FUND_PROD.X_PRDINT_ID, FUND_PROD.X_START_DT, FUND_PROD.X_END_DT, FUND_PROD.X_FUND_RATE';

    EXECUTE IMMEDIATE V_SQL;

    pro_put_line('Gather statistics for table PROD_RELATE_TMP (BEGIN)');
    DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                  TABNAME          => 'PROD_RELATE_TMP');
    pro_put_line('Gather statistics for table PROD_HIER_TMP (END)');

    pro_put_line('Create temp table PROD_RELATE_TMP (END)');
    --*********************************************************************************/

    pro_put_line('Update table ' || v_tbl_name ||' (END)');

    WHEN '' THEN
      NULL;

    ELSE
      NULL;

    END CASE;

exception
  when others then
    pro_put_line('Error: Unexpected system error - ' || sqlerrm);
END pre_extract;

/**********************************************************************/
/* name: pre_extrt_fund_gen_spnd                                      */
/* purpose: pre-process of extraction fund generation and spending    */
/*          derived from old extraction SQL for performance issue     */
/* parameters:                                                        */
/*                                                                    */
/* author: Bodhi                                                      */
/* originator: Bodhi                                                  */
/* version: 1.00 - initial version                                    */
/*          2.00 - For WE region, keep use old SQL to create temporary*/
/*                 table OPT_FUND_ACCRUAL_VW, for other               */
/*                 regions (AP/CE/LA), use new SQL (provided by Piotr)*/
/*          This procedure has been removed in Optima10
/**********************************************************************/
PROCEDURE pre_extrt_fund_gen_spnd(in_grantee VARCHAR2 := NULL,V_REGN IN VARCHAR2 DEFAULT NULL) IS -- SOX Implementation project
  v_tbl_name VARCHAR2(100);
  v_sql      VARCHAR2(32767);

  TYPE T_VARRAY IS VARRAY(4) OF VARCHAR(50);
  V_VAR T_VARRAY := T_VARRAY('OPT_FUND_BRAND_SPLIT_VW',
                             'OPT_TNSFR_ADJMT_VW',
                             'OPT_FUND_FIXED_VW',
                             'OPT_FUND_ACCRUAL_VW'
                             );

begin

  OPT_AX_TBLSPACE(V_REGN,C_TBLSPACE_NAME,V_INDEX);--SOX Implemenation Project

  for tbl in (select upper(table_name) as table_name
              from user_tables
              where regexp_like(table_name,
                    'OPT_FUND_BRAND_SPLIT_VW$|OPT_FUND_ACCRUAL_VW$|OPT_FUND_FIXED_VW$|OPT_TNSFR_ADJMT_VW$','i')
              )
  loop
    -- Drop table
    v_sql := 'DROP TABLE ' || tbl.table_name;
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
    dbms_output.put_line('Done!');
  end loop;

  v_tbl_name := 'OPT_FUND_BRAND_SPLIT_VW';
  v_sql :='create table '||v_tbl_name||CHR(10)||
          C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
          C_TABLE_DOP ||CHR(10)||
          'NOLOGGING compress'||CHR(10)||
          'as '||CHR(10)||
          'SELECT /*+ NO_EXPAND */
          FUND.FUND_SKID,
       BRAND_HIER.BRAND_SKID,
       BRAND_HIER.FY_DATE_SKID,
       1 / SUM(COUNT(DISTINCT BRAND_HIER.BRAND_SKID)) OVER (PARTITION BY FUND.FUND_SKID) BRAND_SPLIT
  FROM OPT_FUND_FDIM      FUND,
       OPT_FUND_PROD_FDIM FUND_PROD,
       ( --Brand and below
        SELECT PROD.PROD_SKID PROD_SKID,
                CONNECT_BY_ROOT(PROD.PROD_SKID) BRAND_SKID,
                CONNECT_BY_ROOT(PROD.BRAND_SPLIT_QTY) BRAND_SPLIT,
                PROD.FY_DATE_SKID,
                PROD.PROD_LVL_DESC
          FROM OPT_PROD_FY_FDIM PROD
        CONNECT BY PRIOR PROD.PROD_ID = PROD.PARNT_PROD_ID
               AND PRIOR PROD.FY_DATE_SKID = PROD.FY_DATE_SKID
         START WITH PROD.PROD_SKID IN
                    (SELECT PROD_SKID
                       FROM OPT_PROD_DIM
                      WHERE PROD_LVL_DESC = ''Brand'')
        UNION
        --Brand and above
        SELECT PROD.PROD_SKID PROD_SKID,
               CONNECT_BY_ROOT(PROD.PROD_SKID) BRAND_SKID,
               CONNECT_BY_ROOT(PROD.BRAND_SPLIT_QTY) BRAND_SPLIT,
               PROD.FY_DATE_SKID,
               PROD.PROD_LVL_DESC
          FROM OPT_PROD_FY_FDIM PROD
        CONNECT BY PROD.PROD_ID = PRIOR PROD.PARNT_PROD_ID
               AND PROD.FY_DATE_SKID = PRIOR PROD.FY_DATE_SKID
         START WITH PROD.PROD_SKID IN
                    (SELECT PROD_SKID
                       FROM OPT_PROD_DIM
                      WHERE PROD_LVL_DESC = ''Brand'')
        )  BRAND_HIER
 WHERE FUND.FUND_GRP_SKID = FUND_PROD.FUND_GRP_SKID
   AND FUND_PROD.PROD_SKID = BRAND_HIER.PROD_SKID
   AND FUND_PROD.FY_DATE_SKID = BRAND_HIER.FY_DATE_SKID
   AND ((FUND.FUND_TYPE_CODE = ''Accrual'' AND FUND_PROD.FUND_TRGT_FLG = ''F'')
   OR   (FUND.FUND_TYPE_CODE = ''Fixed'' AND FUND_PROD.FUND_TRGT_FLG = ''T''))
   AND FUND.FUND_SKID <> 0
 GROUP BY FUND.FUND_SKID,
          BRAND_HIER.BRAND_SKID,
          BRAND_HIER.FY_DATE_SKID';
  dbms_output.put_line('Create table '||v_tbl_name||'...');
  pro_put_line(v_sql);
  execute immediate v_sql;
  dbms_output.put_line('Done!');
  pro_put_line('Gather statistics for table ' || v_tbl_name || ' (BEGIN)');
  DBMS_STATS.GATHER_TABLE_STATS(OWNNAME     => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                TABNAME     => v_tbl_name);
  pro_put_line('Gather statistics for table ' || v_tbl_name || ' (END)');

  v_tbl_name := 'OPT_TNSFR_ADJMT_VW';
  v_sql :='create table '||v_tbl_name ||CHR(10)||
          C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
          C_TABLE_DOP ||CHR(10)||
          'NOLOGGING compress'||CHR(10)||
          'as '||CHR(10)||
          'SELECT /*+ USE_HASH(TNSFR CAL FUND BRAND_SPLIT) */
       TNSFR.FUND_SKID,
       -- Change the Transfers and Adjustment allocation to Months method for F054
       --(1)Transfers and Adjustments made in the same Fiscal Year as Fund defined in
       -- allocated to the actual Transfer or Adjustment date
       --(2)Transfers and Adjustments made before the Fiscal Year as Fund defined in
       -- allocated to the first Month of the Fiscal Year for which the Fund was defined on
       --(3)Transfers and Adjustments made following the Fiscal Year as Fund defined in
       -- allocated to the Last Month of the Fiscal Year for which the Fund was defined on
       (CASE
         WHEN CAL.FISC_YR_SKID = FUND.FY_DATE_SKID THEN
          CAL.MTH_SKID
         WHEN CAL.FISC_YR_SKID < FUND.FY_DATE_SKID THEN
          FUND.FIRST_MTH_SKID
         ELSE
          FUND.LAST_MTH_SKID
       END) AS MTH_SKID,
       FUND.FY_DATE_SKID,
       TNSFR.ACCT_FUND_SKID,
       TNSFR.BUS_UNIT_SKID,
       BRAND_SPLIT.BRAND_SKID,
       (NVL(TNSFR.ADJMT_AMT, 0) + NVL(TNSFR.IN_TNSFR_AMT, 0) -
           NVL(TNSFR.OUT_TNSFR_AMT, 0)) * BRAND_SPLIT.BRAND_SPLIT AS ADJMT_TNSFR_AMT
  FROM OPT_TNSFR_FCT TNSFR,
       (SELECT FUND.FUND_SKID,
               FUND.FY_DATE_SKID,
               FIRST_MTH.MTH_SKID AS FIRST_MTH_SKID,
               LAST_MTH.MTH_SKID AS LAST_MTH_SKID
          FROM OPT_FUND_FDIM FUND,
               CAL_MASTR_DIM CAL,
               CAL_MASTR_DIM FIRST_MTH,
               CAL_MASTR_DIM LAST_MTH
         WHERE CAL.FISC_YR_SKID = FUND.FY_DATE_SKID
           AND CAL.CAL_MASTR_SKID = CAL.FISC_YR_SKID
           AND FIRST_MTH.DAY_DATE = CAL.FISC_YR_START_DATE
           AND LAST_MTH.DAY_DATE = CAL.FISC_YR_END_DATE) FUND,
       OPT_FUND_BRAND_SPLIT_VW BRAND_SPLIT,
       OPT_CAL_MASTR_MV01 CAL
 WHERE DECODE(TNSFR.TNSFR_TYPE_CODE,
              ''Adjustment'',
              TNSFR.ADJMT_DATE,
              TNSFR.TNSFR_DATE) = CAL.DAY_DATE
   AND TNSFR.FUND_SKID = BRAND_SPLIT.FUND_SKID
   AND TNSFR.FUND_SKID = FUND.FUND_SKID
   AND FUND.FY_DATE_SKID = BRAND_SPLIT.FY_DATE_SKID
   AND TNSFR.FUND_SKID <> 0';
  dbms_output.put_line('Create table '||v_tbl_name||'...');
  pro_put_line(v_sql);
  execute immediate v_sql;
  dbms_output.put_line('Done!');

  v_tbl_name := 'OPT_FUND_FIXED_VW';
  v_sql :='create table '||v_tbl_name||CHR(10)||
          C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
          C_TABLE_DOP ||CHR(10)||
          'NOLOGGING compress'||CHR(10)||
          'as '||CHR(10)||
          'SELECT FUND.FUND_SKID,
       FUND.MTH_SKID,
       FUND.FY_DATE_SKID,
       FUND.ACCT_SKID ACCT_FUND_SKID,
       FUND.BUS_UNIT_SKID BUS_UNIT_SKID,
       BRAND_SPLIT.BRAND_SKID,
       MAX(DECODE(FUND_FCT.SHPMT_TYPE_CODE,
                  ''NIV'',
                  FUND_FCT.BASIC_ACCRL_NIV_AMT,
                  FUND_FCT.TRANX_AMT)) /
       MAX(ROUND(MONTHS_BETWEEN(LAST_DAY(FUND.MDF_END_DATE),
                                TRUNC(FUND.MDF_START_DATE, ''MM'')))) *
       MAX(BRAND_SPLIT.BRAND_SPLIT) INITIAL_AMT,
       MAX(FUND_FCT.CHILD_AMT) /
       MAX(ROUND(MONTHS_BETWEEN(LAST_DAY(FUND.MDF_END_DATE),
                                TRUNC(FUND.MDF_START_DATE, ''MM'')))) *
       MAX(BRAND_SPLIT.BRAND_SPLIT) CHILD_AMT
  FROM (SELECT /*+ NO_MERGE USE_MERGE(FUND CAL) */
         FUND.*,
         CAL.MTH_SKID
          FROM OPT_FUND_FDIM FUND,
               CAL_MASTR_DIM CAL
         WHERE NOT (FUND.MDF_START_DATE > CAL.MTH_END_DATE OR
                FUND.MDF_END_DATE < CAL.MTH_START_DATE)
           AND CAL.DAY_DATE BETWEEN to_date(''20000101'',''YYYYMMDD'') AND ADD_MONTHS(SYSDATE, 36)
           AND FUND.FUND_SKID <> 0
           AND CAL.CAL_MASTR_SKID = CAL.MTH_SKID) FUND,
       OPT_FUND_FCT FUND_FCT,
       OPT_FUND_BRAND_SPLIT_VW BRAND_SPLIT
 WHERE FUND.FUND_SKID = FUND_FCT.FUND_SKID
   AND FUND.FUND_SKID = BRAND_SPLIT.FUND_SKID
   AND FUND.FY_DATE_SKID = BRAND_SPLIT.FY_DATE_SKID
   AND FUND.FUND_TYPE_CODE = ''Fixed''
 GROUP BY FUND.FUND_SKID,
          FUND.MTH_SKID,
          FUND.FY_DATE_SKID,
          FUND.ACCT_SKID,
          FUND.BUS_UNIT_SKID,
          BRAND_SPLIT.BRAND_SKID';
  dbms_output.put_line('Create table '||v_tbl_name||'...');
  pro_put_line(v_sql);
  execute immediate v_sql;
  dbms_output.put_line('Done!');

  --Version 2.0
  --Modified by Bodhi
  v_tbl_name := 'OPT_FUND_ACCRUAL_VW';
  v_sql :='create table '||v_tbl_name||CHR(10)||
          C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
          C_TABLE_DOP ||CHR(10)||
          'NOLOGGING compress'||CHR(10)||
          'as '||CHR(10)||
          CASE WHEN REGEXP_LIKE(USER, '_WE','i') THEN
          'SELECT FUND_SKID,
       MTH_SKID,
       FY_DATE_SKID,
       ACCT_FUND_SKID,
       BUS_UNIT_SKID,
       BRAND_SKID,
       ROUND(SUM(ACCRL_FUND_AMT / BRAND_SPLIT), 7) ACCRL_FUND_AMT,
       ROUND(SUM(ACCRL_FRCST_FUND_AMT / BRAND_SPLIT), 7) ACCRL_FRCST_FUND_AMT,
       ROUND(SUM(BASE_SHPMT_AMT / BRAND_SPLIT), 7) BASE_SHPMT_AMT,
       ROUND(AVG(BDF_FUND_RATE_NUM), 7) BDF_FUND_RATE_NUM
  FROM (SELECT /*+ NO_MERGE USE_HASH(BRAND_SPLIT ACCRL) ORDERED) */
         ACCRL.FUND_SKID,
         ACCRL.MTH_SKID,
         ACCRL.FY_DATE_SKID,
         ACCRL.ACCT_FUND_SKID,
         ACCRL.BUS_UNIT_SKID,
         ACCRL.PROD_SKID,
         BRAND_SPLIT.BRAND_SKID,
         ACCRL.ACCRL_FUND_AMT ACCRL_FUND_AMT,
         ACCRL.ACCRL_FRCST_FUND_AMT ACCRL_FRCST_FUND_AMT,
         ACCRL.BASE_SHPMT_AMT BASE_SHPMT_AMT,
         ACCRL.BDF_FUND_RATE_NUM BDF_FUND_RATE_NUM,
         COUNT(DISTINCT BRAND_SPLIT.BRAND_SKID) OVER(PARTITION BY ACCRL.FUND_SKID, ACCRL.MTH_SKID, ACCRL.PROD_SKID) BRAND_SPLIT
          FROM OPT_BRAND_HIER_TDADS BRAND_SPLIT,
               (SELECT IFCT.FUND_SKID,
                       IFCT.MTH_SKID,
                       IFCT.FY_DATE_SKID,
                       IFCT.ACCT_FUND_SKID,
                       IFCT.BUS_UNIT_SKID,
                       IFCT.PROD_SKID,
                       NVL(ACCRL.ACCRL_FUND_AMT, 0) ACCRL_FUND_AMT, --Accruals to Date
                       IFCT.ACCRL_FUND_AMT ACCRL_FRCST_FUND_AMT, --Fund Forecast
                       IFCT.BASE_SHPMT_AMT BASE_SHPMT_AMT,
                       IFCT.BDF_FUND_RATE_NUM BDF_FUND_RATE_NUM
                  FROM OPT_FUND_ACCRL_GTIN_IFCT IFCT,
                       OPT_INTMD_ACCRL_GTIN_FCT ACCRL
                 WHERE IFCT.FUND_SKID = ACCRL.FUND_SKID(+)
                   AND IFCT.MTH_SKID = ACCRL.MTH_SKID(+)
                   AND IFCT.FY_DATE_SKID = ACCRL.FY_DATE_SKID(+)
                   AND IFCT.PROD_SKID = ACCRL.PROD_SKID(+)
                   AND IFCT.FUND_SKID <> 0) ACCRL
         WHERE ACCRL.FY_DATE_SKID = BRAND_SPLIT.FY_DATE_SKID
           AND ACCRL.PROD_SKID = BRAND_SPLIT.PROD_SKID)
 GROUP BY FUND_SKID,
          MTH_SKID,
          FY_DATE_SKID,
          ACCT_FUND_SKID,
          BUS_UNIT_SKID,
          BRAND_SKID'
      ELSE
  'SELECT ACCRL.FUND_SKID,
         ACCRL.MTH_SKID,
         ACCRL.FY_DATE_SKID,
         ACCRL.ACCT_FUND_SKID,
         ACCRL.BUS_UNIT_SKID,
         BRAND_SPLIT.BRAND_SKID,
         ROUND (SUM (ACCRL.ACCRL_FUND_AMT), 7) ACCRL_FUND_AMT,
         ROUND (SUM (ACCRL.ACCRL_FRCST_FUND_AMT), 7) ACCRL_FRCST_FUND_AMT,
         ROUND (SUM (ACCRL.BASE_SHPMT_AMT), 7) BASE_SHPMT_AMT,
         ROUND (AVG (ACCRL.BDF_FUND_RATE_NUM), 7) BDF_FUND_RATE_NUM
    FROM OPT_BRAND_HIER_TDADS BRAND_SPLIT,
         (SELECT IFCT.FUND_SKID,
                 IFCT.MTH_SKID,
                 IFCT.FY_DATE_SKID,
                 IFCT.ACCT_FUND_SKID,
                 IFCT.BUS_UNIT_SKID,
                 IFCT.PROD_SKID,
                 NVL (ACCRL.ACCRL_FUND_AMT, 0) ACCRL_FUND_AMT,
                 IFCT.ACCRL_FUND_AMT ACCRL_FRCST_FUND_AMT,
                 IFCT.BASE_SHPMT_AMT BASE_SHPMT_AMT,
                 IFCT.BDF_FUND_RATE_NUM BDF_FUND_RATE_NUM
            FROM OPT_FUND_ACCRL_GTIN_IFCT IFCT,
                 OPT_INTMD_ACCRL_GTIN_FCT ACCRL
           WHERE IFCT.FUND_SKID = ACCRL.FUND_SKID(+)
             AND IFCT.MTH_SKID = ACCRL.MTH_SKID(+)
             AND IFCT.FY_DATE_SKID = ACCRL.FY_DATE_SKID(+)
             AND IFCT.PROD_SKID = ACCRL.PROD_SKID(+)
             AND IFCT.FUND_SKID <> 0) ACCRL
   WHERE ACCRL.FY_DATE_SKID = BRAND_SPLIT.FY_DATE_SKID
     AND ACCRL.PROD_SKID = BRAND_SPLIT.PROD_SKID
GROUP BY ACCRL.FUND_SKID,
         ACCRL.MTH_SKID,
         ACCRL.FY_DATE_SKID,
         ACCRL.ACCT_FUND_SKID,
         ACCRL.BUS_UNIT_SKID,
         BRAND_SPLIT.BRAND_SKID'
  END;
  dbms_output.put_line('Create table '||v_tbl_name||'...');
  pro_put_line(v_sql);
  execute immediate v_sql;
  dbms_output.put_line('Done!');

  --Gather statics
  --
  for idx in 1..V_VAR.COUNT
  loop
    IF V_VAR(idx) <> 'OPT_FUND_BRAND_SPLIT_VW' THEN
       pro_put_line('Gather statistics for table ' || V_VAR(idx) || ' (BEGIN)');
       DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                  TABNAME          => V_VAR(idx));
       pro_put_line('Gather statistics for table ' || V_VAR(idx) || ' (END)');
    END IF;
  end loop;

  -- Grant


  for idx in 1..V_VAR.COUNT
  LOOP
    SELECT 'Grant select on '|| V_VAR(idx)||' to '|| DECODE(in_grantee,NULL, user||'_ETL', in_grantee)
      INTO v_sql
      FROM dual;
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
	V_SQL := 'grant select on ' || V_VAR(idx) || ' to ' || USER ||','||C_TBL_ROLES;
      PRO_PUT_LINE(V_SQL);
      EXECUTE IMMEDIATE V_SQL;
    dbms_output.put_line('Done!');
  end loop;

exception
  when others then
    pro_put_line('Error: Unexpected system error - ' || sqlerrm);

END pre_extrt_fund_gen_spnd;

PROCEDURE pre_extrt_actl_frcst(V_REGN IN VARCHAR2 DEFAULT NULL) IS-- SOX implementation project
   v_tbl_name VARCHAR2(100);
   v_sql      VARCHAR2(32767);

BEGIN

  OPT_AX_TBLSPACE(V_REGN,C_TBLSPACE_NAME,V_INDEX);--SOX Implemenation project

  for tbl in (select upper(table_name) as table_name
              from user_tables
              where regexp_like(table_name,
                    'PRMTN_SHPMT_MTH_TDADS|OPT_EFSR_TDADS','i'))
  loop
    -- Drop exist Temp table
    v_sql := 'DROP TABLE ' || tbl.table_name || ' PURGE';
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
    dbms_output.put_line('Drop table ' || tbl.table_name || ' Done!');
  end loop;
  --PRMTN_SHPMT_MTH_TDADS
  v_tbl_name := 'PRMTN_SHPMT_MTH_TDADS';
  v_sql := 'CREATE TABLE PRMTN_SHPMT_MTH_TDADS' || CHR(10) ||
              C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
              C_TABLE_DOP ||CHR(10)||
              'NOLOGGING compress'||CHR(10)||
              ' AS  SELECT SHPMT.ACCT_SKID,
       SHPMT.PROD_SKID,
       SHPMT.BUS_UNIT_SKID,
       CAL.MTH_SKID AS MTH_SKID,
       CAL.FISC_YR_SKID AS FISC_YR_SKID,
       CAL.MTH_START_DATE,
       SHPMT.VOL_SU_AMT,
       SHPMT.GIV_AMT,
       SHPMT.NIV_AMT,
       SHPMT.VOL_BUOM_AMT
  FROM (SELECT ACCT_SKID,
               PROD_SKID,
               BUS_UNIT_SKID,
               MTH_SKID,
               SUM(VOL_SU_AMT) AS VOL_SU_AMT,
               SUM(GIV_AMT) AS GIV_AMT,
               SUM(NIV_AMT) AS NIV_AMT,
               SUM(VOL_BUOM_AMT) AS VOL_BUOM_AMT
          FROM OPT_SHPMT_PRMTN_ACCT_SFCT
         GROUP BY ACCT_SKID, PROD_SKID, BUS_UNIT_SKID, MTH_SKID) SHPMT,
       CAL_MASTR_DIM CAL
 WHERE CAL.CAL_MASTR_SKID = SHPMT.MTH_SKID';
  -- Create the Temp table
  dbms_output.put_line('Create table '||v_tbl_name||'(Begin)');
  pro_put_line(v_sql);
  execute immediate v_sql;
  dbms_output.put_line('Create table '||v_tbl_name||'(Done)');
  -- Gather statistics for temp table
  pro_put_line('Gather statistics for table ' || v_tbl_name || ' (BEGIN)');
  DBMS_STATS.GATHER_TABLE_STATS(OWNNAME     => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                TABNAME     => v_tbl_name);
  pro_put_line('Gather statistics for table ' || v_tbl_name || ' (END)');

    /*     R13 added parallel hint to the EFSR view           */
  --OPT_EFSR_TDADS
  v_tbl_name := 'OPT_EFSR_TDADS';
  v_sql := 'CREATE TABLE OPT_EFSR_TDADS NOLOGGING AS
            SELECT /*+ PARALLEL (EFSR , 8) */ EFSR.*
              FROM OPT_EFSR_VW EFSR,
                   OPT_BUS_UNIT_DIM BU
             WHERE (EFSR.INCRM_SU_AMT <> 0 OR EFSR.INCRM_GIV_AMT <> 0 OR EFSR.INCRM_NIV_AMT <> 0 OR
                    EFSR.INCRM_BUOM_AMT <> 0)
               AND EFSR.BUS_UNIT_SKID=BU.BUS_UNIT_SKID
               AND BU.INCRM_PS_RFRSH_IND=''Y''';
  -- Create the Temp table
  dbms_output.put_line('Create table '||v_tbl_name||'(Begin)');
  pro_put_line(v_sql);
  execute immediate v_sql;
  dbms_output.put_line('Create table '||v_tbl_name||'(Done)');
  -- Gather statistics for temp table
  pro_put_line('Gather statistics for table ' || v_tbl_name || ' (BEGIN)');
  DBMS_STATS.GATHER_TABLE_STATS(OWNNAME     => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                TABNAME     => v_tbl_name);
  pro_put_line('Gather statistics for table ' || v_tbl_name || ' (END)');

  exception
  when others then
    pro_put_line('Error: Unexpected system error - ' || sqlerrm);

END pre_extrt_actl_frcst;

PROCEDURE pre_dd_prmtn (in_grantee VARCHAR2 := NULL,V_REGN IN VARCHAR2 DEFAULT NULL) IS -- SOX Implementation project
  v_tbl_name VARCHAR2(100);
  v_sql      VARCHAR2(32767);

  TYPE T_VARRAY IS VARRAY(4) OF VARCHAR(50);
  V_VAR T_VARRAY := T_VARRAY('OPT_PRMTN_TDADS');

begin

  OPT_AX_TBLSPACE(V_REGN,C_TBLSPACE_NAME,V_INDEX);--SOX Implemenation project

  for tbl in (select upper(table_name) as table_name
              from user_tables
              where upper(table_name)='OPT_PRMTN_TDADS'
              )
  loop
    -- Drop table
    v_sql := 'DROP TABLE ' || tbl.table_name;
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
    dbms_output.put_line('Done!');
  end loop;

  v_tbl_name := 'OPT_PRMTN_TDADS';
  --Modified by Gary for C00507128 to retrofit to FTB W2 on 13-May-2011 begin
  --added by Rajesh J, HP on 2-May-2011 for avoiding long running ... Refer CR: C00507128, Begin
  --EXECUTE IMMEDIATE 'alter session disable parallel query';
  --added by Rajesh J, HP on 2-May-2011 for avoiding long running ... Refer CR: C00507128, End
  --added by Bean, On 15-Aug-2011 for hangup running ..
  --EXECUTE IMMEDIATE 'alter session set "_optimizer_extend_jppd_view_types"=false';
  --Optima-QMR11, CR1264, DD sychronize with report, Promotions DLY, Bean, 2011/08/10
  v_sql :='create table '|| v_tbl_name ||CHR(10)||
          C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
          --commented by Rajesh J, HP on 2-May-2011 for avoiding long running, Begin
          --C_TABLE_DOP ||CHR(10)||
          --commented by Rajesh J, HP on 2-May-2011 for avoiding long running, End
          'NOLOGGING compress'||CHR(10)||
          'as '||CHR(10)||
          'WITH BASLN_QRY AS
(
SELECT /* MATERIALIZE */ SUM(NVL(OPT_BASLN_FCT.ACTL_GIV_AMT , 0)) AS BRAND_ACTL_GIV_AMT,
         SUM(NVL(OPT_BASLN_FCT.ACTL_SU_AMT , 0)) AS BRAND_ACTL_SU_AMT,
         OPT_ACCT_FDIM.ACCT_SKID,
         OPT_PRMTN_FDIM.PRMTN_SKID,
         OPT_BUS_UNIT_FDIM.BUS_UNIT_SKID,
         OPT_CAL_MASTR_DIM.FISC_YR_ABBR_NAME
    from OPT_ACCT_FDIM OPT_ACCT_FDIM,
         OPT_BUS_UNIT_FDIM OPT_BUS_UNIT_FDIM,
         OPT_CAL_MASTR_DIM OPT_CAL_MASTR_DIM,
         OPT_PRMTN_FDIM OPT_PRMTN_FDIM,
         OPT_BASLN_FCT OPT_BASLN_FCT
    where OPT_BUS_UNIT_FDIM.BUS_UNIT_SKID = OPT_BASLN_FCT.BUS_UNIT_SKID
      and OPT_BASLN_FCT.WK_SKID = OPT_CAL_MASTR_DIM.CAL_MASTR_SKID
      and OPT_BASLN_FCT.BUS_UNIT_SKID = OPT_ACCT_FDIM.BUS_UNIT_SKID
      and OPT_BASLN_FCT.BUS_UNIT_SKID = OPT_PRMTN_FDIM.BUS_UNIT_SKID
      and OPT_BASLN_FCT.ACCT_SKID = OPT_ACCT_FDIM.ACCT_SKID
      and OPT_BASLN_FCT.PRMTN_SKID = OPT_PRMTN_FDIM.PRMTN_SKID
      and OPT_ACCT_FDIM.ACCT_LONG_NAME is not null
      --Modified by Adam on 2011-09-09 remove filters for promotions for QMR11.1 retrofit start
      --Modified by Adam on 2011-09-09 remove filters for promotions for QMR11.1 retrofit end
    GROUP BY OPT_ACCT_FDIM.ACCT_SKID,
         OPT_PRMTN_FDIM.PRMTN_SKID,
         OPT_BUS_UNIT_FDIM.BUS_UNIT_SKID,
         OPT_CAL_MASTR_DIM.FISC_YR_ABBR_NAME
),
ACTL_EVENT_ROI_QRY AS
 (SELECT /*+ MATERIALIZE */ PRMTN_FDIM.BUS_UNIT_SKID,
             ACCT.ACCT_SKID,
             PRMTN_FDIM.PRMTN_SKID,
             CHANNEL_AVG.FISC_YR_ABBR_NAME,
             CHANNEL_AVG.ACTL_EVENT_ROI_AVG  --- Trade Channel Event Roi
        FROM  (SELECT OPT_ACCT_FDIM.BUS_UNIT_SKID,  -- Channel, bu split
                       OPT_ACCT_FDIM.CHANL_TYPE_DESC,
                       CAL.FISC_YR_ABBR_NAME,
                       AVG (NVL (OPT_PRMTN_FCT.ACTL_EVENT_ROI, 0)) AS ACTL_EVENT_ROI_AVG
                FROM OPT_PRMTN_FCT, OPT_ACCT_FDIM, OPT_CAL_MASTR_DIM CAL
               WHERE OPT_PRMTN_FCT.BUS_UNIT_SKID   = OPT_ACCT_FDIM.BUS_UNIT_SKID
                 and OPT_PRMTN_FCT.ACCT_PRMTN_SKID = OPT_ACCT_FDIM.ACCT_SKID
                 and OPT_PRMTN_FCT.DATE_SKID       = CAL.CAL_MASTR_SKID
               GROUP BY OPT_ACCT_FDIM.BUS_UNIT_SKID,
                        OPT_ACCT_FDIM.CHANL_TYPE_DESC,
                        CAL.FISC_YR_ABBR_NAME) CHANNEL_AVG,
               OPT_ACCT_FDIM ACCT,
               OPT_PRMTN_FDIM PRMTN_FDIM
       WHERE ACCT.BUS_UNIT_SKID   = PRMTN_FDIM.BUS_UNIT_SKID
         AND ACCT.ACCT_SKID       = PRMTN_FDIM.ACCT_SKID
         AND ACCT.BUS_UNIT_SKID   = CHANNEL_AVG.BUS_UNIT_SKID
         AND ACCT.CHANL_TYPE_DESC = CHANNEL_AVG.CHANL_TYPE_DESC
),
ACTVY_QRY AS
(SELECT /*+ MATERIALIZE */  BUS_UNIT_SKID,
         ACCT_PRMTN_SKID AS ACCT_SKID,
         PRMTN_SKID,
         SUM(NVL(ESTMT_COST_OVRRD_AMT,0)) AS ESTMT_COST_OVRRD_AMT,
         SUM(nvl(MANUL_COST_OVRRD_AMT,0)) as MANUL_COST_OVRRD_AMT,
         SUM(nvl(REVSD_VAR_ESTMT_COST_AMT,0)) AS REVSD_VAR_ESTMT_COST_AMT
    FROM OPT_ACTVY_FCT
   GROUP BY BUS_UNIT_SKID,
         ACCT_PRMTN_SKID,
        PRMTN_SKID
) ,
CHANNEL_AVG_QRY AS
(SELECT /*+ MATERIALIZE */ OPT_ACCT_FDIM.BUS_UNIT_SKID,  -- Channel, bu split
                       OPT_ACCT_FDIM.CHANL_TYPE_DESC,
                       CAL.FISC_YR_ABBR_NAME,
                       AVG (NVL (OPT_PRMTN_FCT.actl_event_roi, 0)) AS ACTL_EVENT_ROI_AVG
                FROM OPT_PRMTN_FCT, OPT_ACCT_FDIM, OPT_CAL_MASTR_DIM CAL
               WHERE OPT_PRMTN_FCT.BUS_UNIT_SKID   = OPT_ACCT_FDIM.BUS_UNIT_SKID
                 and OPT_PRMTN_FCT.ACCT_PRMTN_SKID = OPT_ACCT_FDIM.ACCT_SKID
                 and OPT_PRMTN_FCT.DATE_SKID       = CAL.CAL_MASTR_SKID
               GROUP BY OPT_ACCT_FDIM.BUS_UNIT_SKID,
                        OPT_ACCT_FDIM.CHANL_TYPE_DESC,
                        CAL.FISC_YR_ABBR_NAME
),
PRMTN_SUM_QRY AS
(SELECT /*+ MATERIALIZE */ BUS_UNIT_SKID,  -- Promotion split
                     ACCT_PRMTN_SKID AS ACCT_SKID,
                     BASE_PRMTN_SKID AS PRMTN_SKID,
                     CAL.FISC_YR_ABBR_NAME,
                     SUM (NVL (OPT_PRMTN_FCT.actl_event_roi, 0)) AS ACTL_EVENT_ROI_SUM
                FROM OPT_PRMTN_FCT, OPT_CAL_MASTR_DIM CAL
               WHERE OPT_PRMTN_FCT.DATE_SKID = CAL.CAL_MASTR_SKID
               GROUP BY BUS_UNIT_SKID,
                     ACCT_PRMTN_SKID,
                     BASE_PRMTN_SKID,
                     CAL.FISC_YR_ABBR_NAME
),
CHANNEL_AER_QRY AS
 (SELECT /*+ MATERIALIZE */  PRMTN_SUM.BUS_UNIT_SKID,
             ACCT.ACCT_SKID,
             PRMTN_SUM.PRMTN_SKID,
             CHANNEL_AVG.FISC_YR_ABBR_NAME,
             CASE WHEN ACTL_EVENT_ROI_SUM <> 0 THEN
                       CHANNEL_AVG.ACTL_EVENT_ROI_AVG / ACTL_EVENT_ROI_SUM
                  ELSE 0
              END * 100 as CHANNEL_VAL
        FROM  CHANNEL_AVG_QRY CHANNEL_AVG,
               PRMTN_SUM_QRY PRMTN_SUM,
               OPT_ACCT_FDIM ACCT,
               OPT_PRMTN_FDIM PRMTN_FDIM
       WHERE ACCT.BUS_UNIT_SKID   = PRMTN_SUM.BUS_UNIT_SKID
         AND ACCT.ACCT_SKID       = PRMTN_SUM.ACCT_SKID
         AND ACCT.BUS_UNIT_SKID   = CHANNEL_AVG.BUS_UNIT_SKID
         AND ACCT.CHANL_TYPE_DESC = CHANNEL_AVG.CHANL_TYPE_DESC
         AND PRMTN_SUM.FISC_YR_ABBR_NAME = CHANNEL_AVG.FISC_YR_ABBR_NAME
         AND PRMTN_FDIM.BUS_UNIT_SKID = PRMTN_SUM.BUS_UNIT_SKID
         AND PRMTN_FDIM.PRMTN_SKID    = PRMTN_SUM.PRMTN_SKID
)
SELECT PRMTN_MAIN.FISC_YR_ABBR_NAME,  ---Fiscal year
             PRMTN_MAIN.BUS_UNIT_NAME, -- Orgnization
             PRMTN_MAIN.ACCT_NAME,  -- Account
             PRMTN_MAIN.ACCT_ID,  -- Account ID
             PRMTN_MAIN.PRMTN_NAME,  -- Promotion Name
             PRMTN_MAIN.PRMTN_ID,  -- Promotion ID
             PRMTN_MAIN.ANAPLAN_PRMTN_ID,  -- Anaplan Promotion ID
             PRMTN_MAIN.PRMTN_PLAN_NAME,   -- Promotion Plan name
             PRMTN_MAIN.PRMTN_PLAN_ID,  -- Promotion Plan ID
             PRMTN_MAIN.PRMTN_TYPE_CODE,  -- Promotion type
             PRMTN_MAIN.PRMTN_STTUS_CODE,  --Promotion Status
             PRMTN_MAIN.OWND_BY_NAME,  -- Promotion Owner T#
             PRMTN_MAIN.APPRV_DESC,  -- Promotion Approver Name
             PRMTN_MAIN.APPRV_STTUS_CODE,   -- Promotion Approver Status
             PRMTN_MAIN.AUTO_UPDT_GTIN_IND,     --Auto Update GTIN
             PRMTN_MAIN.CREAT_DATE,             -- Promotion Creation Date
             PRMTN_MAIN.PGM_START_DATE,-- Promotion Start Date
             PRMTN_MAIN.PGM_END_DATE,  -- Promotion End Date
             PRMTN_MAIN.PRMTN_STOP_DATE,
             PRMTN_MAIN.SHPMT_START_DATE,  -- Promotion Shipments Start Date
             PRMTN_MAIN.SHPMT_END_DATE,  -- Promotion Shipments End Date
             PRMTN_MAIN.CNBLN_WK_CNT,           --Cannibalisation  # of Weeks (Shipment)
             PRMTN_MAIN.ACTVY_DETL_POP,         --Promotion - Activity Details and PoP
             PRMTN_MAIN.CMMNT_DESC,  -- Promotion Comments
             PRMTN_MAIN.X_TOT_IN_SU_AMT, --Promo Estimated Total MSU
             PRMTN_MAIN.ACTL_TOT_SU_AMT,        --Promo Actual Total MSU
             PRMTN_MAIN.ACTL_TOT_VOL_VS_EST_TOT_VOL, --Promo Actual Total Vol./ Promo Estimated Total Vol. %
             PRMTN_MAIN.FIXED_COST_ESTMT_AMT,   --Estimated Fixed Cost
             PRMTN_MAIN.VAR_COST_ESTMT_AMT,     --Estimated Variable Cost
           --  nvl(OPT_PRMTN_FCT.REVSD_VAR_ESTMT_COST_AMT,0) as REVSD_VAR_ESTMT_COST_AMT, --Revised Estimated Variable Cost
             PRMTN_MAIN.REVSD_VAR_ESTMT_COST_AMT,  --Revised Estimated Variable Cost
             PRMTN_MAIN.MANUL_COST_OVRRD_AMT,             --Manual Cost Overwrite
           --  ACTVY.REVSD_VAR_ESTMT_COST_AMT AS ESTMT_REVSED_COST,  --- Revised Estimated Variable Cost
             PRMTN_MAIN.EST_COST_VAR_FIX , --Estimated  Cost (Fixed + Variable)
             PRMTN_MAIN.ESTMT_TOT_COST,   -- Estimated Total Cost (Cost Indicator based)
             PRMTN_MAIN.CALC_INDEX_NUM,       --Actual Fixed Cost
             PRMTN_MAIN.ACTL_VAR_COST_NUM, --Actual Variable Cost
             PRMTN_MAIN.ACTL_PROMO_COST,  -- Actual total cost
             PRMTN_MAIN.ACTL_TOT_COST_VS_EST_TOT_COST, --Actual Total Cost/Estimate Total Cost %
             PRMTN_MAIN.ORIG_BASE_VOL_IN_SU_AMT, --(Original Event ROI) Promo Baseline MSU
             PRMTN_MAIN.ORIG_ESTMT_INCRM_NIV_AMT,    --(Original Event ROI) Promo Estimated Incremental NIV
             PRMTN_MAIN.ORIG_INCR_COST_AMT,      --(Original Event ROI ) Total Cost
             PRMTN_MAIN.ORIG_NOS_MULTR_AMT, -- Original Estimated Event ROI
             PRMTN_MAIN.TAG_EVT_ROI,  -- (Original Event ROI ) Target Event ROI
             PRMTN_MAIN.ESTMT_INCRM_CASE_AMT, --Promo Estimated Incremental MSU
             PRMTN_MAIN.ESTMT_INCRM_NIV_AMT, --Promo Estimated Incremental NIV
             PRMTN_MAIN.ESTMT_MDA_LOR_AMT, --Promo Estimated MDA LOR Spending
             PRMTN_MAIN.EST_MDA_SPD,--Promo Estimated MDA Spending
             PRMTN_MAIN.ESTMT_EVENT_ROI, -- Original Estimated Event ROI
             NVL(BRAND_ACTL_SU_AMT, 0)  as BRAND_ACTL_SU_AMT,               --Promo Brands Actual Total MSU
             NVL(BRAND_ACTL_GIV_AMT, 0) as BRAND_ACTL_GIV_AMT,              --Promo Brands Actual Total GIV
             PRMTN_MAIN.ACTL_MDA_LOR_AMT,         --Promo Actual MDA LOR Spending
             PRMTN_MAIN.ACTL_MDA_SPD,      --Promo Actual MDA Spending
             PRMTN_MAIN.ACTL_EVENT_ROI,           --Actual Event ROI
             PRMTN_MAIN.TRADE_CHL_ACTL_EVT_ROI,           --Trade Channel Actual Event ROI
             PRMTN_MAIN.ACTL_VS_EST_COST_IND, --Promo Actual Event ROI vs Trade Channel Actual Event ROI %
             PRMTN_MAIN.PRMTN_AVG_POP --Promo Average Compliance Score %
        from
( SELECT
       CAL_DIM.FISC_YR_ABBR_NAME AS FISC_YR_ABBR_NAME,  ---Fiscal year
       OPT_BUS_UNIT_FDIM.BUS_UNIT_NAME, -- Orgnization
       OPT_ACCT_FDIM.ACCT_SKID,
       OPT_PRMTN_FDIM.PRMTN_SKID,
       OPT_BUS_UNIT_FDIM.BUS_UNIT_SKID,
       OPT_ACCT_FDIM.ACCT_NAME,  -- Account
       OPT_ACCT_FDIM.NAME  AS  ACCT_ID,  -- Account ID
       OPT_PRMTN_FDIM.PRMTN_NAME as PRMTN_NAME,  -- Promotion Name
       OPT_PRMTN_FDIM.PRMTN_ID as PRMTN_ID,  -- Promotion ID
       OPT_PRMTN_FDIM.X_ANAPLAN_ID as ANAPLAN_PRMTN_ID,  -- Anaplann Promotion ID ADDED BY RAVI
       OPT_PRMTN_FDIM.PRMTN_PLAN_NAME as PRMTN_PLAN_NAME,   -- Promotion Plan name
       OPT_PRMTN_FDIM.PRMTN_PLAN_ID as PRMTN_PLAN_ID,  -- Promotion Plan ID
       case
         when case
                when OPT_PRMTN_FDIM.CORP_PRMTN_TYPE_CODE = ''Target Account'' then
                 ''Corporate''
                else
                 OPT_PRMTN_FDIM.CORP_PRMTN_TYPE_CODE
              end is null then
          ''Private''
         else
          case
            when OPT_PRMTN_FDIM.CORP_PRMTN_TYPE_CODE = ''Target Account'' then
             ''Corporate''
            else
             OPT_PRMTN_FDIM.CORP_PRMTN_TYPE_CODE
          end
       end as PRMTN_TYPE_CODE,   --Modified by Zhang Lun @09/4/29, fix defect 2971, -- Promotion type
       OPT_PRMTN_FDIM.PRMTN_STTUS_CODE as PRMTN_STTUS_CODE,  --Promotion Status
       OPT_PRMTN_FDIM.OWND_BY_NAME as OWND_BY_NAME,  -- Promotion Owner T#
       OPT_PRMTN_FDIM.APPRV_DESC as APPRV_DESC,  -- Promotion Approver Name
       OPT_PRMTN_FDIM.APPRV_STTUS_CODE as APPRV_STTUS_CODE,   -- Promotion Approver Status
       OPT_PRMTN_FDIM.AUTO_UPDT_GTIN_IND as AUTO_UPDT_GTIN_IND,     --Auto Update GTIN
       OPT_PRMTN_FDIM.CREAT_DATE as CREAT_DATE,             -- Promotion Creation Date
       TO_CHAR(OPT_PRMTN_FDIM.PGM_START_DATE,''DD-MON-YYYY'')  AS PGM_START_DATE,-- Promotion Start Date
       TO_CHAR(OPT_PRMTN_FDIM.PGM_END_DATE,''DD-MON-YYYY'')  AS PGM_END_DATE,  -- Promotion End Date
       TO_CHAR(OPT_PRMTN_FDIM.PRMTN_STOP_DATE,''DD-MON-YYYY'') AS PRMTN_STOP_DATE,
       TO_CHAR(OPT_PRMTN_FDIM.SHPMT_START_DATE,''DD-MON-YYYY'')  AS SHPMT_START_DATE,  -- Promotion Shipments Start Date
       TO_CHAR(OPT_PRMTN_FDIM.SHPMT_END_DATE,''DD-MON-YYYY'')  AS SHPMT_END_DATE,  -- Promotion Shipments End Date
       OPT_PRMTN_FDIM.CNBLN_WK_CNT as CNBLN_WK_CNT,           --Cannibalisation  # of Weeks (Shipment)
       OPT_PRMTN_FDIM.ACTVY_DETL_POP as ACTVY_DETL_POP,         --Promotion - Activity Details and PoP
       OPT_PRMTN_FDIM.CMMNT_DESC  as CMMNT_DESC,  -- Promotion Comments
       round(PRMTN_ACCUM.X_TOT_IN_SU_AMT,2) as X_TOT_IN_SU_AMT, --Promo Estimated Total MSU
       OPT_PRMTN_FCT.ACTL_TOT_SU_AMT/1000 as ACTL_TOT_SU_AMT,        --Promo Actual Total MSU
       PRMTN_ACCUM.ACTL_TOT_VOL_VS_EST_TOT_VOL, --Promo Actual Total Vol./ Promo Estimated Total Vol. %
       OPT_PRMTN_FCT.FIXED_COST_ESTMT_AMT,   --Estimated Fixed Cost
       OPT_PRMTN_FCT.VAR_COST_ESTMT_AMT,     --Estimated Variable Cost
     --  nvl(OPT_PRMTN_FCT.REVSD_VAR_ESTMT_COST_AMT,0) as REVSD_VAR_ESTMT_COST_AMT, --Revised Estimated Variable Cost
       ACTVY.REVSD_VAR_ESTMT_COST_AMT as REVSD_VAR_ESTMT_COST_AMT,  --Revised Estimated Variable Cost
       ACTVY.MANUL_COST_OVRRD_AMT as MANUL_COST_OVRRD_AMT,             --Manual Cost Overwrite
     --  ACTVY.REVSD_VAR_ESTMT_COST_AMT AS ESTMT_REVSED_COST,  --- Revised Estimated Variable Cost
       OPT_PRMTN_FCT.FIXED_COST_ESTMT_AMT + OPT_PRMTN_FCT.VAR_COST_ESTMT_AMT as EST_COST_VAR_FIX , --Estimated  Cost (Fixed + Variable)
       ACTVY.ESTMT_COST_OVRRD_AMT   AS ESTMT_TOT_COST,   -- Estimated Total Cost (Cost Indicator based)
       OPT_PRMTN_FCT.CALC_INDEX_NUM as CALC_INDEX_NUM,       --Actual Fixed Cost
       OPT_PRMTN_FCT.ACTL_VAR_COST_NUM as ACTL_VAR_COST_NUM, --Actual Variable Cost
       (OPT_PRMTN_FCT.CALC_INDEX_NUM + OPT_PRMTN_FCT.ACTL_VAR_COST_NUM)   AS ACTL_PROMO_COST,  -- Actual total cost
       PRMTN_ACCUM.ACTL_TOT_COST_VS_EST_TOT_COST as ACTL_TOT_COST_VS_EST_TOT_COST, --Actual Total Cost/Estimate Total Cost %
       OPT_PRMTN_FCT.ORIG_BASE_VOL_IN_SU_AMT/1000 as ORIG_BASE_VOL_IN_SU_AMT, --(Original Event ROI) Promo Baseline MSU
       round(PRMTN_ACCUM.ORIG_ESTMT_INCRM_NIV_AMT,2) as ORIG_ESTMT_INCRM_NIV_AMT,    --(Original Event ROI) Promo Estimated Incremental NIV
       OPT_PRMTN_FCT.ORIG_INCR_COST_AMT as ORIG_INCR_COST_AMT,      --(Original Event ROI ) Total Cost
       PRMTN_ACCUM.ORIG_NOS_MULTR_AMT AS ORIG_NOS_MULTR_AMT, -- Original Estimated Event ROI
       case when OPT_PRMTN_FCT.X_NOS_MULTI_TRGT_DESC is null
            then 0
            else cast(OPT_PRMTN_FCT.X_NOS_MULTI_TRGT_DESC as  DOUBLE PRECISION )
        end as  TAG_EVT_ROI,  -- (Original Event ROI ) Target Event ROI
       OPT_PRMTN_FCT.ESTMT_INCRM_CASE_AMT/1000 as ESTMT_INCRM_CASE_AMT, --Promo Estimated Incremental MSU
       OPT_PRMTN_FCT.ESTMT_INCRM_NIV_AMT as ESTMT_INCRM_NIV_AMT, --Promo Estimated Incremental NIV
       OPT_PRMTN_FCT.ESTMT_MDA_LOR_AMT as ESTMT_MDA_LOR_AMT, --Promo Estimated MDA LOR Spending
       NVL(OPT_PRMTN_FCT.FIXED_COST_ESTMT_AMT , 0) + NVL(OPT_PRMTN_FCT.VAR_COST_ESTMT_AMT , 0) +
       NVL(OPT_PRMTN_FCT.BASE_COST_AMT , 0) + NVL(OPT_PRMTN_FCT.ESTMT_SPND_NUM , 0) as EST_MDA_SPD,--Promo Estimated MDA Spending
       NVL(OPT_PRMTN_FCT.ESTMT_EVENT_ROI,0)  AS ESTMT_EVENT_ROI, -- Original Estimated Event ROI
     --  NVL(BASLN.BRAND_ACTL_SU_AMT, 0)  as BRAND_ACTL_SU_AMT,                --Promo Brands Actual Total MSU
     --  NVL(BASLN.BRAND_ACTL_GIV_AMT, 0) as BRAND_ACTL_GIV_AMT,             --Promo Brands Actual Total GIV
       OPT_PRMTN_FCT.ACTL_MDA_LOR_AMT as ACTL_MDA_LOR_AMT,         --Promo Actual MDA LOR Spending
       (nvl(OPT_PRMTN_FCT.CALC_INDEX_NUM , 0) + nvl(OPT_PRMTN_FCT.ACTL_VAR_COST_NUM , 0) +
        nvl(OPT_PRMTN_FCT.ACTL_SPND_NUM , 0)) as ACTL_MDA_SPD,      --Promo Actual MDA Spending
       OPT_PRMTN_FCT.ACTL_EVENT_ROI       as ACTL_EVENT_ROI,           --Actual Event ROI
       ACTL_EVENT_ROI_AVG.ACTL_EVENT_ROI_AVG as TRADE_CHL_ACTL_EVT_ROI,           --Trade Channel Actual Event ROI
       CHANNEL_AER.CHANNEL_VAL as ACTL_VS_EST_COST_IND, --Promo Actual Event ROI vs Trade Channel Actual Event ROI %
       NVL(OPT_PRMTN_FDIM.PRMTN_AVG_POP,0) as PRMTN_AVG_POP --Promo Average Compliance Score %
--Optima11, FTB3, Bean, 2011/07/06, End
FROM (select OPT_PRMTN_FDIM.BUS_UNIT_SKID,
             OPT_ACCT_FDIM.ACCT_SKID,
             OPT_PRMTN_FDIM.PRMTN_SKID,
             sum(nvl(OPT_PRMTN_FCT.X_TOT_IN_SU_AMT , 0) / nullif( 1000 , 0)) as X_TOT_IN_SU_AMT,
             sum(NVL(OPT_PRMTN_FCT.ORIG_NOS_MULTR_AMT , 0)) as ORIG_NOS_MULTR_AMT,
             sum(NVL(OPT_PRMTN_FCT.ORIG_ESTMT_INCRM_NIV_AMT , 0)) as ORIG_ESTMT_INCRM_NIV_AMT,
             case  when sum(NVL(OPT_PRMTN_FCT.X_TOT_IN_SU_AMT , 0) / nullif( 1000 , 0)) <> 0
                    and not sum(NVL(OPT_PRMTN_FCT.X_TOT_IN_SU_AMT , 0) / nullif( 1000 , 0)) is null
                   then NVL(sum(NVL(OPT_PRMTN_FCT.ACTL_TOT_SU_AMT , 0) / nullif( 1000 , 0)) , 0) /
                       nullif( sum(NVL(OPT_PRMTN_FCT.X_TOT_IN_SU_AMT , 0) / nullif( 1000 , 0)) , 0) * 100
                   else 0
             end as Actl_tot_vol_Vs_EST_Tot_Vol,
             case when NVL(sum(NVL(OPT_PRMTN_FCT.FIXED_COST_ESTMT_AMT , 0)) , 0) +
                       NVL(sum(NVL(OPT_PRMTN_FCT.VAR_COST_ESTMT_AMT , 0)) , 0) <> 0 and not
                       NVL(sum(NVL(OPT_PRMTN_FCT.FIXED_COST_ESTMT_AMT , 0)) , 0) + NVL(sum(NVL(OPT_PRMTN_FCT.VAR_COST_ESTMT_AMT , 0)) , 0) is null
                  then NVL(NVL(sum(NVL(OPT_PRMTN_FCT.CALC_INDEX_NUM , 0)) , 0) +
                       NVL(sum(NVL(OPT_PRMTN_FCT.ACTL_VAR_COST_NUM , 0)) , 0) , 0) / nullif( NVL(sum(NVL(OPT_PRMTN_FCT.FIXED_COST_ESTMT_AMT , 0)) , 0) +
                       NVL(sum(NVL(OPT_PRMTN_FCT.VAR_COST_ESTMT_AMT , 0)) , 0) , 0) * 100
                  else 0
             end as Actl_tot_cost_vs_est_tot_cost, --Actual Total Cost/Estimate Total Cost %
             AVG(OPT_PRMTN_FCT.ACTL_EVENT_ROI) AS TRADE_CHL_ACTL_EVT_ROI           --Trade Channel Actual Event ROI
        FROM OPT_PRMTN_FCT, OPT_PRMTN_FDIM, OPT_ACCT_FDIM
       WHERE OPT_PRMTN_FDIM.PRMTN_SKID = OPT_PRMTN_FCT.BASE_PRMTN_SKID
         AND OPT_PRMTN_FDIM.BUS_UNIT_SKID = opt_prmtn_fct.BUS_UNIT_SKID
         AND OPT_PRMTN_FDIM.ACCT_SKID = OPT_ACCT_FDIM.ACCT_SKID
         AND OPT_PRMTN_FDIM.BUS_UNIT_SKID = OPT_ACCT_FDIM.BUS_UNIT_SKID
       GROUP BY OPT_PRMTN_FDIM.BUS_UNIT_SKID,OPT_ACCT_FDIM.ACCT_SKID,
             OPT_PRMTN_FDIM.prmtn_skid)  PRMTN_ACCUM,
ACTVY_QRY ACTVY,
      CHANNEL_AER_QRY  CHANNEL_AER,--------------CHANNEL_AER
    ACTL_EVENT_ROI_QRY ACTL_EVENT_ROI_AVG,--------------ACTL_EVENT_ROI_AVG
     OPT_CAL_MASTR_DIM CAL_DIM,
     OPT_BUS_UNIT_FDIM,
     OPT_ACCT_FDIM,
     OPT_PRMTN_FDIM,
     OPT_PRMTN_FCT

 WHERE OPT_PRMTN_FCT.ACCT_PRMTN_SKID = OPT_ACCT_FDIM.ACCT_SKID
   AND OPT_PRMTN_FCT.BUS_UNIT_SKID   = OPT_ACCT_FDIM.BUS_UNIT_SKID

   AND OPT_PRMTN_FDIM.BUS_UNIT_SKID  = OPT_BUS_UNIT_FDIM.BUS_UNIT_SKID

   AND OPT_PRMTN_FDIM.BUS_UNIT_SKID  = OPT_PRMTN_FCT.BUS_UNIT_SKID
   AND OPT_PRMTN_FDIM.PRMTN_SKID     = OPT_PRMTN_FCT.BASE_PRMTN_SKID

   --Optima11, FTB3, New Accumulation table condition, begin
   AND OPT_PRMTN_FDIM.BUS_UNIT_SKID = PRMTN_ACCUM.BUS_UNIT_SKID(+)
   AND OPT_PRMTN_FDIM.ACCT_SKID     = PRMTN_ACCUM.ACCT_SKID(+)
   AND OPT_PRMTN_FDIM.PRMTN_SKID    = PRMTN_ACCUM.PRMTN_SKID(+)

   AND OPT_PRMTN_FDIM.BUS_UNIT_SKID = CHANNEL_AER.BUS_UNIT_SKID(+)
   and OPT_PRMTN_FDIM.ACCT_SKID     = CHANNEL_AER.ACCT_SKID(+)
   AND OPT_PRMTN_FDIM.PRMTN_SKID    = CHANNEL_AER.PRMTN_SKID(+)

   AND OPT_PRMTN_FDIM.BUS_UNIT_SKID = ACTL_EVENT_ROI_AVG.BUS_UNIT_SKID(+)
   AND OPT_PRMTN_FDIM.ACCT_SKID     = ACTL_EVENT_ROI_AVG.ACCT_SKID(+)
   AND OPT_PRMTN_FDIM.PRMTN_SKID    = ACTL_EVENT_ROI_AVG.PRMTN_SKID(+)
   AND ACTL_EVENT_ROI_AVG.FISC_YR_ABBR_NAME = CAL_DIM.FISC_YR_ABBR_NAME

   AND OPT_PRMTN_FDIM.BUS_UNIT_SKID  = ACTVY.BUS_UNIT_SKID(+)
   AND OPT_PRMTN_FDIM.ACCT_SKID      = ACTVY.ACCT_SKID(+)
   AND OPT_PRMTN_FDIM.PRMTN_SKID     = ACTVY.PRMTN_SKID(+)
   AND CAL_DIM.CAL_MASTR_SKID        = OPT_PRMTN_FCT.DATE_SKID
   AND OPT_ACCT_FDIM.ACCT_LONG_NAME is not null
      --Modified by Adam on 2011-09-09 remove filters for promotions for QMR11.1 retrofit start
      --Modified by Adam on 2011-09-09 remove filters for promotions for QMR11.1 retrofit end
   ) PRMTN_MAIN,
 BASLN_QRY BASLN
  WHERE PRMTN_MAIN.ACCT_SKID         = BASLN.ACCT_SKID(+)
    AND PRMTN_MAIN.PRMTN_SKID        = BASLN.PRMTN_SKID(+)
    AND PRMTN_MAIN.BUS_UNIT_SKID     = BASLN.BUS_UNIT_SKID(+)
    AND PRMTN_MAIN.FISC_YR_ABBR_NAME = BASLN.FISC_YR_ABBR_NAME(+)';
  dbms_output.put_line('Create table '||v_tbl_name||'...');
  pro_put_line(v_sql);
  execute immediate v_sql;
  dbms_output.put_line('Done!');

  --added by Rajesh J, HP on 2-May-2011 for avoiding long running ... Refer CR: C00507128, Begin
  --EXECUTE IMMEDIATE 'alter session enable parallel query';
  --added by Rajesh J, HP on 2-May-2011 for avoiding long running ... Refer CR: C00507128, End
  --Modified by Gary for C00507128 to retrofit to FTB W2 on 13-May-2011 end

  --Gather statics
  --
  for idx in 1..V_VAR.COUNT
  loop
       pro_put_line('Gather statistics for table ' || V_VAR(idx) || ' (BEGIN)');
       DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                  TABNAME          => V_VAR(idx));
       pro_put_line('Gather statistics for table ' || V_VAR(idx) || ' (END)');
  end loop;

  -- Grant

  for idx in 1..V_VAR.COUNT
  LOOP
    SELECT 'Grant select on '|| V_VAR(idx)||' to '|| DECODE(in_grantee,NULL, user||'_ETL', in_grantee)
      INTO v_sql
      FROM dual;
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
	-- SOX IMPLEMENTATION Project STARTED
	V_SQL := 'grant select on ' || V_VAR(idx) || ' to ' || USER ||','||C_TBL_ROLES;
      PRO_PUT_LINE(V_SQL);
      EXECUTE IMMEDIATE V_SQL;
	 --SOX IMPLEMENTATION Project ENDED
    dbms_output.put_line('Done!');
  end loop;

exception
  when others then
    pro_put_line('Error: Unexpected system error - ' || sqlerrm);

END pre_dd_prmtn;

/**********************************************************************/
/* name: pre_dd_prmtn_prod                                            */
/* purpose: pre-process of data dump promoted product                 */
/* parameters:                                                        */
/*                                                                    */
/* author: Bodhi                                                      */
/* originator: Bodhi                                                  */
/* version: 1.00 - initial version                                    */
/*          2.00 - set zero measures as NULL                           */
/*                                                                    */
/*                                                                    */
/**********************************************************************/

PROCEDURE pre_dd_prmtn_prod(in_grantee VARCHAR2 := NULL, in_dop INTEGER :=4,V_REGN IN VARCHAR2 DEFAULT NULL) IS --SOX Implementation project
  v_tbl_name VARCHAR2(100);
  --v_idx_name VARCHAR2(100);
  v_sql      VARCHAR2(32767);
  v_pp       VARCHAR2(32767) := NULL;
  v_count    number := 0;
  v_i        number := 0;

  TYPE T_VARRAY IS VARRAY(4) OF VARCHAR(50);
  V_VAR T_VARRAY := T_VARRAY('OPT_PRMTN_PROD_TDADS');

begin

  OPT_AX_TBLSPACE(V_REGN,C_TBLSPACE_NAME,V_INDEX);--SOX Implemenation project

  for tbl in (select upper(table_name) as table_name
              from user_tables
              where upper(table_name)='OPT_PRMTN_PROD_TDADS'
              )
  loop
    -- Drop table
    v_sql := 'DROP TABLE ' || tbl.table_name;
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
    dbms_output.put_line('Done!');
  end loop;

/*
  FOR idx IN (SELECT upper(INDEX_NAME) AS index_name
                FROM user_indexes
                WHERE upper(INDEX_NAME)='OPT_PRMTN_PROD_TDADS_IDX'
              )
  LOOP
     -- Drop index
    v_sql := 'DROP INDEX ' || idx.index_name;
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
    dbms_output.put_line('Done!');
  END LOOP;
*/

  select count(*) into v_count
    from opt_bus_unit_plc
   where bus_unit_skid<>0;

  for bus in (select bus_unit_skid, bus_unit_name
                from opt_bus_unit_plc
               where bus_unit_skid<>0
               order by bus_unit_skid
             )
  loop
      v_i := v_i+1;
      v_pp := v_pp||'  partition P_'||bus.bus_unit_skid||
                    ' values ('''||bus.bus_unit_name||''')'||
                    case when v_i <> v_count then
                        ','||CHR(10)
                    else
                        CHR(10)||')'||CHR(10)
                    end;
  end loop;

  pro_put_line(v_pp);

  --Version 2.00 - set zero measures as NULL
  --Bodhi
  --OPTIMA11, SIT Defect 608, Martin 2010-11-17, Start: Adjust a code error about BuOM Base Calculation
  v_tbl_name := 'OPT_PRMTN_PROD_TDADS';
  v_sql :='create table '||v_tbl_name ||CHR(10)||
          'partition by list (BUS_UNIT_name)'||CHR(10)||
          '('||CHR(10)||
          v_pp||CHR(10)||
          'parallel (degree '||in_dop||')'||CHR(10)||
          'NOLOGGING'||CHR(10)||
          'as '||CHR(10)||
          'SELECT           BUS_UNIT_NAME,PRMTN_START_DATE_FY,PRMTN_ID,X_ANAPLAN_ID,ESTMT_BASLN_CASE_AMT,ESTMT_BASLN_UNIT_AMT,ESTMT_BASLN_NIV_AMT,ESTMT_INCRM_CASE_AMT,
           BASE_IN_GIV_AMT,INCRM_IN_CASE_AMT,INCRM_IN_GIV_AMT,ESTMT_INCRM_NIV_AMT,TOT_IN_CASE_AMT,TOT_IN_GIV_AMT,X_TOT_IN_SU_AMT,
           ESTMT_TOT_IN_NIV_AMT,BPT_REVSD_SU_AMT,BPT_REVSD_BUOM_AMT,BPT_REVSD_GIV_AMT,BPT_REVSD_NIV_AMT,PRMTD_CATEG_ID,PROD_NAME,
           PRMTD_CATEG_NAME,GTIN,PROD_DESC,SHELF_PRICE,BRAND_BUS_ID,ACTL_BUOM_AMT,ACTL_GIV_AMT,ACTL_NIV_AMT,TRADE_TERM_PCT,STTUS_CODE,
           BRAND_NAME,DISCOUNT_MODEL,ACTVY_DISC_RATE_AMT,INCRM_FROM_BASE_NUM,REDMT_RATE,ACT_PRO_TOT_VOL,ACT_EST_PRO_TOT_VOL
           FROM (
           SELECT FCT.*,PROD_DIM.PROD_NAME FROM (
           SELECT /*+ use_hash(PP) parallel(PP,'||in_dop||')*/
           OPT_BUS_UNIT_FDIM.BUS_UNIT_NAME ,
           OPT_PRMTN_FDIM.PRMTN_START_DATE_FY,
           OPT_PRMTN_FDIM.PRMTN_ID , --"Promotion Id"
           OPT_PRMTN_FDIM.X_ANAPLAN_ID, --Added by Ravi part of anaplan promotion id column addition
           DECODE(PP.ESTMT_BASLN_CASE_AMT,NULL,0,PP.ESTMT_BASLN_CASE_AMT) AS ESTMT_BASLN_CASE_AMT, --"SU Base (Estimated Volume)"
           DECODE(PP.ESTMT_BASLN_UNIT_AMT,NULL,0,PP.ESTMT_BASLN_UNIT_AMT) AS ESTMT_BASLN_UNIT_AMT , --"BuOM Base"
           -- Start Modified by Senthil for PP DD Change Deployment on 24-May-2012
       --DECODE(PP.ESTMT_BASLN_NIV_AMT,NULL,0, PP.ESTMT_BASLN_NIV_AMT) AS ESTMT_BASLN_NIV_AMT, --"NIV Base"
       DECODE(PP.ESTMT_BASLN_NIV_AMT,-99.9999,0,
               DECODE(PP.ESTMT_BASLN_NIV_AMT,NULL,0, PP.ESTMT_BASLN_NIV_AMT)
         ) AS ESTMT_BASLN_NIV_AMT, --"NIV Base"
           -- End Modified by Senthil for PP DD Change Deployment on 24-May-2012
       DECODE(PP.ESTMT_INCRM_CASE_AMT,NULL,0,PP.ESTMT_INCRM_CASE_AMT) AS ESTMT_INCRM_CASE_AMT, --"SU Incremental"
           -- Start Modified by Senthil for PP DD Change Deployment on 24-May-2012
           --DECODE(PP.TOT_IN_GIV_AMT-PP.INCRM_IN_GIV_AMT,NULL,0,PP.TOT_IN_GIV_AMT-PP.INCRM_IN_GIV_AMT) AS BASE_IN_GIV_AMT, --"GIV Base"
           DECODE(PP.ESTMT_BASLN_NIV_AMT,-99.9999,0,
              DECODE(PP.TOT_IN_GIV_AMT-PP.INCRM_IN_GIV_AMT,NULL,0,PP.TOT_IN_GIV_AMT-PP.INCRM_IN_GIV_AMT)
             ) AS BASE_IN_GIV_AMT, --"GIV Base"
           -- End Modified by Senthil for PP DD Change Deployment on 24-May-2012
       DECODE(PP.INCRM_IN_CASE_AMT,NULL,0,PP.INCRM_IN_CASE_AMT)  AS INCRM_IN_CASE_AMT, --"BuOM Incremental"
           DECODE(PP.INCRM_IN_GIV_AMT,NULL,0,PP.INCRM_IN_GIV_AMT) AS INCRM_IN_GIV_AMT, ----"GIV Incremental"
           -- Start Modified by Senthil for PP DD Change Deployment on 24-May-2012
           --DECODE(PP.ESTMT_INCRM_NIV_AMT,NULL,0,PP.ESTMT_INCRM_NIV_AMT) AS ESTMT_INCRM_NIV_AMT, --"NIV Incremental"
           DECODE(PP.ESTMT_BASLN_NIV_AMT,-99.9999,0,
              DECODE(PP.ESTMT_INCRM_NIV_AMT,NULL,0,PP.ESTMT_INCRM_NIV_AMT)
                 ) AS ESTMT_INCRM_NIV_AMT, --"NIV Incremental"
           -- End Modified by Senthil for PP DD Change Deployment on 24-May-2012
       DECODE(PP.TOT_IN_CASE_AMT,NULL,0,PP.TOT_IN_CASE_AMT)  AS TOT_IN_CASE_AMT , --"BuOM Total"
           DECODE(PP.TOT_IN_GIV_AMT,NULL,0,PP.TOT_IN_GIV_AMT)  AS TOT_IN_GIV_AMT , --"GIV Total"
           DECODE(PP.X_TOT_IN_SU_AMT,NULL,0,PP.X_TOT_IN_SU_AMT) AS X_TOT_IN_SU_AMT , --"SU Total"
           -- Start Modified by Senthil for PP DD Change Deployment on 24-May-2012
       --DECODE(PP.ESTMT_BASLN_NIV_AMT+PP.ESTMT_INCRM_NIV_AMT,NULL,0,PP.ESTMT_BASLN_NIV_AMT+PP.ESTMT_INCRM_NIV_AMT) AS ESTMT_TOT_IN_NIV_AMT, --"NIV Total"
       DECODE(PP.ESTMT_BASLN_NIV_AMT,-99.9999,PP.ESTMT_INCRM_NIV_AMT,
              DECODE(PP.ESTMT_BASLN_NIV_AMT+PP.ESTMT_INCRM_NIV_AMT,NULL,0,PP.ESTMT_BASLN_NIV_AMT+PP.ESTMT_INCRM_NIV_AMT)
             ) AS ESTMT_TOT_IN_NIV_AMT, --"NIV Total"
           -- End Modified by Senthil for PP DD Change Deployment on 24-May-2012
       --add by Eric at 2010-07-27 for CR729 -- Start
           PP.BPT_REVSD_SU_AMT,
           PP.BPT_REVSD_BUOM_AMT,
           PP.BPT_REVSD_GIV_AMT,
           PP.BPT_REVSD_NIV_AMT,
           --add by Eric at 2010-07-27 for CR729 -- End
           PP.PRMTD_CATEG_ID,
           PP.PRMTD_CATEG_NAME ,
           PRMTN_CATEG.PROD_SKID, -- Added by Senthil for PP DD Change Deployment on 24-May-2012
       OPT_PROD_FY_FDIM.GTIN , --"Product GTIN Code"
           -- Start Modified by Senthil for PP DD Change Deployment on 24-May-2012
       --OPT_PROD_FY_FDIM.PROD_DESC , --"Product Name"
           DECODE(PP.ESTMT_BASLN_NIV_AMT,-99.9999,NULL,OPT_PROD_FY_FDIM.PROD_DESC) AS PROD_DESC, --"Product Name"
           -- End Modified by Senthil for PP DD Change Deployment on 24-May-2012
           DECODE(PP.SHELF_PRICE,NULL,0,PP.SHELF_PRICE) AS SHELF_PRICE, -- Shelf Price
           OPT_PROD_FY_FDIM.BRAND_BUS_ID , --"Brand ID"
           DECODE(PP.ACTL_BUOM_AMT,NULL,0,PP.ACTL_BUOM_AMT) AS ACTL_BUOM_AMT, --BuOM Actual Shipment
           DECODE(PP.ACTL_GIV_AMT,NULL,0,PP.ACTL_GIV_AMT) AS  ACTL_GIV_AMT, --"GIV Actual Shipment"
           DECODE(PP.ACTL_NIV_AMT,NULL,0,PP.ACTL_NIV_AMT) AS ACTL_NIV_AMT, --"NIV Actual Shipment"
           DECODE(PP.TRADE_TERM_PCT,NULL,0,PP.TRADE_TERM_PCT) AS TRADE_TERM_PCT, --"TT %"
           OPT_PROD_FY_FDIM.STTUS_CODE, --Product Life Cycle
           --add by Daniel.Ma at 2011-07-08 for CR1264 -- Start
           OPT_PROD_FY_FDIM.BRAND_NAME,----Brand Name
           case  when PP.PRICE_FEATR_DISC_TYPE_CODE = ''%'' then ''% Disc'' else ''Case Rate'' end AS Discount_Model,--Discount Model
           PP.ACTVY_DISC_RATE_AMT,--Price Featuring Amount
           PP.INCRM_FROM_BASE_NUM,--Incremental from Base % Value
           PP.REDMT_RATE,--Redemption Rate
           --OPT_PROD_FY_FDIM.SU_FACTR * PP.SUM_ACTL_TOT_QTY_AMT AS Act_Pro_tot_Vol,--Actual Promo Total Volume (SU) --commented by Sujatha on 15-Sep-2011 ... Refer PR#PM00014106
           PP.SUM_ACTL_TOT_QTY_AMT AS Act_Pro_tot_Vol,--Actual Promo Total Volume (SU) --added by Sujatha on 15-Sep-2011 ... Refer PR#PM00014106
           --commented by Sujatha on 15-Sep-2011 ... Refer PR#PM00014106
           --nvl(case  when PP.SUM_X_TOT_IN_SU_AMT = 0 then 0 else OPT_PROD_FY_FDIM.SU_FACTR * PP.SUM_ACTL_TOT_QTY_AMT / nvl(PP.SUM_X_TOT_IN_SU_AMT, 0) * 100 end  , 0) AS Act_Est_Pro_Tot_Vol--Actual Promo Total Volume/ Estimated Promo Total Volume %
           --added by Sujatha on 15-Sep-2011 ... Refer PR#PM00014106
           nvl(case  when PP.SUM_X_TOT_IN_SU_AMT = 0 then 0 else PP.SUM_ACTL_TOT_QTY_AMT / nvl(PP.SUM_X_TOT_IN_SU_AMT, 0) * 100 end  , 0) AS Act_Est_Pro_Tot_Vol--Actual Promo Total Volume/ Estimated Promo Total Volume %
           --add by Daniel.Ma at 2011-07-08 for CR1264 -- End
  FROM OPT_BUS_UNIT_FDIM,
       OPT_ACCT_FDIM,
       OPT_PRMTN_FDIM,
       OPT_PROD_FY_FDIM,
       --OPT_PROD_FY_FDIM BRAND,
       --OPT_PRMTN_PROD_FCT PP,
       (select OPT_PRMTN_PROD_FCT.BUS_UNIT_SKID,
               OPT_PRMTN_PROD_FCT.PROD_SKID,
               OPT_PRMTN_PROD_FCT.PRMTN_SKID,
               OPT_PRMTN_PROD_FCT.ACCT_PRMTN_SKID,
               OPT_PRMTN_PROD_FCT.FY_DATE_SKID,
              OPT_PRMTN_PROD_FCT.ESTMT_BASLN_CASE_AMT,
              OPT_PRMTN_PROD_FCT.ESTMT_BASLN_UNIT_AMT,
              OPT_PRMTN_PROD_FCT.ESTMT_BASLN_NIV_AMT,
              OPT_PRMTN_PROD_FCT.ESTMT_INCRM_CASE_AMT,
              OPT_PRMTN_PROD_FCT.TOT_IN_GIV_AMT,
              OPT_PRMTN_PROD_FCT.INCRM_IN_GIV_AMT,
              OPT_PRMTN_PROD_FCT.INCRM_IN_CASE_AMT,
              OPT_PRMTN_PROD_FCT.ESTMT_INCRM_NIV_AMT,
              OPT_PRMTN_PROD_FCT.TOT_IN_CASE_AMT,
              OPT_PRMTN_PROD_FCT.X_TOT_IN_SU_AMT,
              OPT_PRMTN_PROD_FCT.BPT_REVSD_SU_AMT,
              OPT_PRMTN_PROD_FCT.BPT_REVSD_BUOM_AMT,
              OPT_PRMTN_PROD_FCT.BPT_REVSD_GIV_AMT,
              OPT_PRMTN_PROD_FCT.BPT_REVSD_NIV_AMT,
              OPT_PRMTN_PROD_FCT.PRMTD_CATEG_ID,
              OPT_PRMTN_PROD_FCT.PRMTD_CATEG_NAME,
              OPT_PRMTN_PROD_FCT.SHELF_PRICE,
              OPT_PRMTN_PROD_FCT.ACTL_BUOM_AMT,
              OPT_PRMTN_PROD_FCT.ACTL_GIV_AMT,
              OPT_PRMTN_PROD_FCT.ACTL_NIV_AMT,
              OPT_PRMTN_PROD_FCT.TRADE_TERM_PCT,
              OPT_PRMTN_PROD_FCT.PRICE_FEATR_DISC_TYPE_CODE,
              OPT_PRMTN_PROD_FCT.ACTVY_DISC_RATE_AMT,
              OPT_PRMTN_PROD_FCT.INCRM_FROM_BASE_NUM,
              OPT_PRMTN_PROD_FCT.REDMT_RATE,
              sum(OPT_PRMTN_PROD_FCT.ACTL_TOT_QTY_AMT) AS SUM_ACTL_TOT_QTY_AMT,
              sum(OPT_PRMTN_PROD_FCT.X_TOT_IN_SU_AMT) AS SUM_X_TOT_IN_SU_AMT
        from OPT_PRMTN_PROD_FCT
        group by
              OPT_PRMTN_PROD_FCT.BUS_UNIT_SKID,
              OPT_PRMTN_PROD_FCT.PROD_SKID,
              OPT_PRMTN_PROD_FCT.PRMTN_SKID,
              OPT_PRMTN_PROD_FCT.ACCT_PRMTN_SKID,
              OPT_PRMTN_PROD_FCT.ESTMT_BASLN_CASE_AMT,
              OPT_PRMTN_PROD_FCT.ESTMT_BASLN_UNIT_AMT,
              OPT_PRMTN_PROD_FCT.ESTMT_BASLN_NIV_AMT,
              OPT_PRMTN_PROD_FCT.ESTMT_INCRM_CASE_AMT,
              OPT_PRMTN_PROD_FCT.TOT_IN_GIV_AMT,
              OPT_PRMTN_PROD_FCT.INCRM_IN_GIV_AMT,
              OPT_PRMTN_PROD_FCT.INCRM_IN_CASE_AMT,
              OPT_PRMTN_PROD_FCT.ESTMT_INCRM_NIV_AMT,
              OPT_PRMTN_PROD_FCT.TOT_IN_CASE_AMT,
              OPT_PRMTN_PROD_FCT.X_TOT_IN_SU_AMT,
              OPT_PRMTN_PROD_FCT.BPT_REVSD_SU_AMT,
              OPT_PRMTN_PROD_FCT.BPT_REVSD_BUOM_AMT,
              OPT_PRMTN_PROD_FCT.BPT_REVSD_GIV_AMT,
              OPT_PRMTN_PROD_FCT.BPT_REVSD_NIV_AMT,
              OPT_PRMTN_PROD_FCT.PRMTD_CATEG_ID,
              OPT_PRMTN_PROD_FCT.PRMTD_CATEG_NAME,
              OPT_PRMTN_PROD_FCT.SHELF_PRICE,
              OPT_PRMTN_PROD_FCT.ACTL_BUOM_AMT,
              OPT_PRMTN_PROD_FCT.ACTL_GIV_AMT,
              OPT_PRMTN_PROD_FCT.ACTL_NIV_AMT,
              OPT_PRMTN_PROD_FCT.TRADE_TERM_PCT,
              OPT_PRMTN_PROD_FCT.PRICE_FEATR_DISC_TYPE_CODE,
              OPT_PRMTN_PROD_FCT.ACTVY_DISC_RATE_AMT,
              OPT_PRMTN_PROD_FCT.INCRM_FROM_BASE_NUM,
              OPT_PRMTN_PROD_FCT.REDMT_RATE,
              OPT_PRMTN_PROD_FCT.FY_DATE_SKID

       ) PP,
       (SELECT fisc_yr_abbr_name,CAL_MASTR_SKID
          FROM cal_mastr_dim
         WHERE day_date = trunc(sysdate)';
         -- Commented and modified by Sathyarajan  PM00018530 to_yminterval Functions Failure Start
         --   or day_date= trunc(SYSDATE+to_yminterval(''01-00''))  -- next
         --   or day_date = trunc(SYSDATE-to_yminterval(''01-00'')) -- previous

            v_sql := v_sql || '
            or day_date= trunc( ADD_MONTHS(trunc(to_date(SYSDATE,''DD-MM-RRRR'')),+12))  -- next
            or day_date = trunc(ADD_MONTHS(trunc(to_date(SYSDATE,''DD-MM-RRRR'')),-12)) -- previous
            -- Modified DAY_DATE to SYSDATE by Senthil for PR#PM00028396
     -- Commented and modified by Sathyarajan  PM00018530 to_yminterval Functions Failure End
        ) FY,
    -- Added by Senthil for PP DD Change Deployment on 24-May-2012
        OPT_PRMTN_CATEG_SDIM PRMTN_CATEG
 WHERE PP.ACCT_PRMTN_SKID = OPT_ACCT_FDIM.ACCT_SKID
   AND PP.PRMTN_SKID = OPT_PRMTN_FDIM.PRMTN_SKID
   AND PP.PROD_SKID = OPT_PROD_FY_FDIM.PROD_SKID
   AND PP.BUS_UNIT_SKID = OPT_BUS_UNIT_FDIM.BUS_UNIT_SKID
   --AND OPT_PROD_FY_FDIM.BRAND_ID = BRAND.PROD_ID(+)
   --AND  (PP.ESTMT_BASLN_CASE_AMT <> 0 OR
   --      PP.ESTMT_BASLN_UNIT_AMT<> 0 OR
   --     PP.ESTMT_BASLN_NIV_AMT <> 0 OR
   --      PP.ESTMT_INCRM_CASE_AMT<> 0 OR
   --      PP.INCRM_IN_CASE_AMT   <> 0 OR
   --      PP.INCRM_IN_GIV_AMT    <> 0 OR
   --      PP.ESTMT_INCRM_NIV_AMT <> 0 OR
   --      PP.TOT_IN_CASE_AMT     <> 0 OR
   --      PP.TOT_IN_GIV_AMT      <> 0 OR
   --     PP.X_TOT_IN_SU_AMT     <> 0 OR
   --      PP.ACTL_BUOM_AMT       <> 0 OR
   --      PP.ACTL_GIV_AMT        <> 0 OR
   --      PP.ACTL_NIV_AMT        <> 0 OR
   --      PP.TRADE_TERM_PCT <> 0)
   AND OPT_PRMTN_FDIM.PRMTN_START_DATE_FY=FY.fisc_yr_abbr_name
   AND OPT_PROD_FY_FDIM.FY_DATE_SKID = PP.FY_DATE_SKID   --ADD From CR1264
   AND OPT_PROD_FY_FDIM.BUS_UNIT_SKID = PP.BUS_UNIT_SKID --ADD From CR1264
      -- Start Added by Senthil for PP DD Change Deployment on 24-May-2012
   AND PP.BUS_UNIT_SKID = PRMTN_CATEG.BUS_UNIT_SKID(+)
   AND PP.ACCT_PRMTN_SKID = PRMTN_CATEG.ACCT_SKID(+)
   AND PP.FY_DATE_SKID = PRMTN_CATEG.FY_DATE_SKID(+)
   AND PP.PRMTD_CATEG_ID = PRMTN_CATEG.PRMTN_CATEG_ID(+)
   ) FCT, OPT_PROD_DIM PROD_DIM
   WHERE FCT.PROD_SKID = PROD_DIM.PROD_SKID(+)
   -- End Added by Senthil for PP DD Change Deployment on 24-May-2012
   )';

  --OPTIMA11, SIT Defect 608, Martin 2010-11-17, end
  dbms_output.put_line('Create table '||v_tbl_name||'...');
  pro_put_line(v_sql);


  --execute immediate v_sql;
  execute immediate v_sql;
  dbms_output.put_line('Done!');
  --Modified by Sophy removing gather_stats for BIP-W3(FtRB Wave 3 G) 06-21-2011 Begin
  --Gather statics
  --for idx in 1..V_VAR.COUNT
  --loop
       --pro_put_line('Gather statistics for table ' || V_VAR(idx) || ' (BEGIN)');
       --DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
       --                           TABNAME          => V_VAR(idx));
       --pro_put_line('Gather statistics for table ' || V_VAR(idx) || ' (END)');
  --end loop;
  --Modified by Sophy removing gather_stats for BIP-W3(FtRB Wave 3 G)  06-21-2011 End
/*
  -- Create index
  v_idx_name := 'OPT_PRMTN_PROD_TDADS_IDX';
  v_sql :='create bitmap index '|| v_idx_name ||' ON '||
         v_tbl_name ||'(BUS_UNIT_NAME,PRMTN_START_DATE_FY) local NOLOGGING';
  dbms_output.put_line('Create index '||v_idx_name||'...');
  pro_put_line(v_sql);
  execute immediate v_sql;
  dbms_output.put_line('Done!');
*/

  -- Grant
  for idx in 1..V_VAR.COUNT
  LOOP
    SELECT 'Grant select on '|| V_VAR(idx)||' to '|| DECODE(in_grantee,NULL, user||'_ETL', in_grantee)
      INTO v_sql
      FROM dual;
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
	--SOX IMPLEMENTATION Project STARTED
	V_SQL := 'grant select on ' || V_VAR(idx) || ' to ' || USER ||','||C_TBL_ROLES;
      PRO_PUT_LINE(V_SQL);
      EXECUTE IMMEDIATE V_SQL;
	  --SOX IMPLEMENTATION Project ENDED
    dbms_output.put_line('Done!');
  end loop;

exception
  when others then
    pro_put_line('Error: Unexpected system error - ' || sqlerrm);

END pre_dd_prmtn_prod;


/**********************************************************************/
/* name: pre_dd_base_ship_fund_prod                                   */
/* purpose:                                                            */
/* parameters:                                                        */
/*                                                                    */
/*                                                                    */
/*                                                                    */
/**********************************************************************/
PROCEDURE pre_dd_base_ship_fund_prod(in_grantee VARCHAR2 := NULL, in_dop INTEGER :=4,V_REGN IN VARCHAR2 DEFAULT NULL) IS -- SOX Implementation project
  v_tbl_name VARCHAR2(100);
--  v_idx_name VARCHAR2(100);
  v_sql      VARCHAR2(32767);
  v_pp       VARCHAR2(32767) := NULL;
  v_count    number := 0;
  v_i        number := 0;

  TYPE T_VARRAY IS VARRAY(4) OF VARCHAR(50);
  V_VAR T_VARRAY := T_VARRAY('OPT_BASE_SHIP_FUND_PROD_TDADS');

begin

  OPT_AX_TBLSPACE(V_REGN,C_TBLSPACE_NAME,V_INDEX);--SOX Implemenation project

  for tbl in (select upper(table_name) as table_name
              from user_tables
              where upper(table_name)='OPT_BASE_SHIP_FUND_PROD_TDADS'
              )
  loop
    -- Drop table
    v_sql := 'DROP TABLE ' || tbl.table_name;
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
    dbms_output.put_line('Done!');
  end loop;

  select count(*) into v_count
    from opt_bus_unit_plc
   where bus_unit_skid<>0;

  for bus in (select bus_unit_skid, bus_unit_name
                from opt_bus_unit_plc
               where bus_unit_skid<>0
               order by bus_unit_skid
             )
  loop
      v_i := v_i+1;
      v_pp := v_pp||'  partition P_'||bus.bus_unit_skid||
                    ' values ('''||bus.bus_unit_name||''')'||
                    case when v_i <> v_count then
                        ','||CHR(10)
                    else
                        CHR(10)||')'||CHR(10)
                    end;
  end loop;

  pro_put_line(v_pp);
-- Replace OPT_ACCRL_PROD_IFCT with OPT_FUND_PROD_IFCT
-- Updated by David for Fund re-arch in Optima10 on 2010-02-03
--Modified by Gary for R11 CR1227 on 2011/05/06 start
  v_tbl_name := 'OPT_BASE_SHIP_FUND_PROD_TDADS';
  v_sql :='create table '||v_tbl_name ||CHR(10)||
          'partition by list (BUS_UNIT_name)'||CHR(10)||
          '('||CHR(10)||
          v_pp||CHR(10)||
          'parallel (degree '||in_dop||')'||CHR(10)||
          'NOLOGGING'||CHR(10)||
          'as '||CHR(10)||
          'SELECT BUS_UNIT.BUS_UNIT_NAME,
       FUND.FUND_START_DATE_FY,
       ACCT.ACCT_LONG_NAME,
       ACCT.NAME AS ACCT_ID,
       ACCT.CHANL_TYPE_DESC,--Added by Rajesh.S, HP for PR#PM00017063 on 22-Mar-2012
       FUND.FUND_LONG_NAME,
       FUND.FUND_ID,
       FUND.FUND_CLASS_CODE,--Added by Rajesh.S, HP for PR#PM00017063 on 22-Mar-2012
       FUND_PROD.PROD_DESC,
       FUND_PROD.PROD_LVL_DESC,
       FUND_PROD.PROD_NAME,
       CAL_MASTR.MTH_NAME,
       sum(ACCRL_FRCST.BASE_SHPMT_AMT) AS BASE_SHPMT_AMT,
       sum(ACCRL_FRCST.ACCRL_FUND_AMT) AS ACCRL_FUND_AMT,
       sum(ACCRL_FRCST.VOL_SU_AMT) AS VOL_SU_AMT --Added by Rajesh.S, HP for PR#PM00017063 on 22-Mar-2012
  FROM OPT_FUND_FDIM        FUND,
       OPT_BUS_UNIT_FDIM    BUS_UNIT,
       CAL_MASTR_DIM        CAL_MASTR,
       OPT_FUND_PROD_FDIM   FUND_PROD,
       OPT_ACCT_FDIM        ACCT,
       (SELECT fisc_yr_abbr_name
          FROM cal_mastr_dim
         WHERE day_date = trunc(sysdate)';
         --   Commented and modified by Sathyarajan  PM00018530 to_yminterval Functions Failure Start
         --   or day_date= trunc(SYSDATE+to_yminterval(''01-00''))  -- next
         --   or day_date = trunc(SYSDATE-to_yminterval(''01-00'')) -- previous

            v_sql := v_sql || '
            or day_date= trunc( ADD_MONTHS(trunc(to_date(SYSDATE,''DD-MM-RRRR'')),+12))  -- next
            or day_date = trunc(ADD_MONTHS(trunc(to_date(SYSDATE,''DD-MM-RRRR'')),-12)) -- previous
            -- Modified DAY_DATE to SYSDATE by Senthil for PR#PM00028396
     -- Commented and modified by Sathyarajan  PM00018530 to_yminterval Functions Failure End
        ) FY,
       OPT_FUND_PROD_IFCT  ACCRL_FRCST
 WHERE FUND.FUND_START_DATE_FY = FY.FISC_YR_ABBR_NAME
   AND FUND_PROD.BUS_UNIT_SKID = ACCRL_FRCST.BUS_UNIT_SKID
   AND FUND_PROD.FUND_PROD_SKID = ACCRL_FRCST.FUND_PROD_SKID
   AND FUND_PROD.FY_DATE_SKID = ACCRL_FRCST.FISC_YR_SKID
   AND FUND.FUND_SKID = ACCRL_FRCST.FUND_SKID
   AND FUND.BUS_UNIT_SKID = ACCRL_FRCST.BUS_UNIT_SKID
   AND BUS_UNIT.BUS_UNIT_SKID = ACCRL_FRCST.BUS_UNIT_SKID
   AND CAL_MASTR.CAL_MASTR_SKID = ACCRL_FRCST.MTH_SKID
   AND CAL_MASTR.DAY_DATE < ADD_MONTHS(TRUNC(SYSDATE, ''MM''), 1)
   AND ACCT.ACCT_SKID = ACCRL_FRCST.ACCT_SKID
   AND ACCT.BUS_UNIT_SKID = ACCRL_FRCST.BUS_UNIT_SKID
   AND FUND_PROD.FUND_TRGT_FLG = ''F''
   AND FUND.FUND_TYPE_CODE = ''Accrual''
       --CAL_MASTR.FISC_YR_SKID = (SELECT FISC_YR_SKID FROM CAL_MASTR_DIM WHERE DAY_DATE = TRUNC(SYSDATE))
 group by BUS_UNIT.BUS_UNIT_NAME,
          FUND.FUND_START_DATE_FY,
          ACCT.NAME,
          ACCT.ACCT_LONG_NAME,
          ACCT.CHANL_TYPE_DESC,
          FUND.FUND_ID,
          FUND.FUND_LONG_NAME,
          FUND.FUND_CLASS_CODE,
          FUND_PROD.PROD_NAME,
          FUND_PROD.PROD_DESC,
          FUND_PROD.PROD_LVL_DESC,
          CAL_MASTR.MTH_NAME,
          CAL_MASTR.MTH_NUM
 order by BUS_UNIT.BUS_UNIT_NAME,
          FUND.FUND_START_DATE_FY,
          ACCT.ACCT_LONG_NAME,
          ACCT.NAME,
          FUND.FUND_LONG_NAME,
          FUND.FUND_ID,
          FUND_PROD.PROD_DESC,
          FUND_PROD.PROD_LVL_DESC,
          FUND_PROD.PROD_NAME,
          CAL_MASTR.MTH_NUM';
  dbms_output.put_line('Create table '||v_tbl_name||'...');
  pro_put_line(v_sql);
--Modified by Gary for R11 CR1227 on 2011/05/06 end


  --execute immediate v_sql;
  execute immediate v_sql;
  dbms_output.put_line('Done!');

  --Gather statics
  for idx in 1..V_VAR.COUNT
  loop
       pro_put_line('Gather statistics for table ' || V_VAR(idx) || ' (BEGIN)');
       DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                  TABNAME          => V_VAR(idx));
       pro_put_line('Gather statistics for table ' || V_VAR(idx) || ' (END)');
  end loop;

  -- Grant
  for idx in 1..V_VAR.COUNT
  LOOP
    SELECT 'Grant select on '|| V_VAR(idx)||' to '|| DECODE(in_grantee,NULL, user||'_ETL', in_grantee)
      INTO v_sql
      FROM dual;
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
	--SOX IMPLEMENTATION Project STARTED
	V_SQL := 'grant select on ' || V_VAR(idx) || ' to ' || USER ||','||C_TBL_ROLES;
      PRO_PUT_LINE(V_SQL);
      EXECUTE IMMEDIATE V_SQL;
    dbms_output.put_line('Done!');
	--SOX IMPLEMENTATION PROJECT ENDED
  end loop;

exception
  when others then
    pro_put_line('Error: Unexpected system error - ' || sqlerrm);

END pre_dd_base_ship_fund_prod;




/**********************************************************************/
/* name: pre_dd_base_ship_fund_prod                                   */
/* purpose:                                                            */
/* parameters:                                                        */
/*                                                                    */
/* Optima11,B002,27-Oct-2010,Yves Add a new column VOL_SU_AMT         */
/*                                                                    */
/**********************************************************************/
PROCEDURE pre_dd_base_ship_fund (in_grantee VARCHAR2 := NULL,V_REGN IN VARCHAR2 DEFAULT NULL) IS -- SOX Implementation project
  v_tbl_name VARCHAR2(100);
  v_sql      VARCHAR2(32767);

  TYPE T_VARRAY IS VARRAY(4) OF VARCHAR(50);
  V_VAR T_VARRAY := T_VARRAY('OPT_BASE_SHIP_FUND_IFCT');

begin

  OPT_AX_TBLSPACE(V_REGN,C_TBLSPACE_NAME,V_INDEX);--SOX Implemenation project

  for tbl in (select upper(table_name) as table_name
              from user_tables
              where upper(table_name)='OPT_BASE_SHIP_FUND_IFCT'
              )
  loop
    -- Drop table
    v_sql := 'DROP TABLE ' || tbl.table_name;
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
    dbms_output.put_line('Done!');
  end loop;

  v_tbl_name := 'OPT_BASE_SHIP_FUND_IFCT';
  v_sql :='create table '|| v_tbl_name ||CHR(10)||
          C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
          C_TABLE_DOP ||CHR(10)||
          'NOLOGGING compress'||CHR(10)||
          'as '||CHR(10)||
          'SELECT  DISTINCT
       TRUNK.BUS_UNIT_NAME AS BUS_UNIT_NAME,
       TRUNK.FUND_START_DATE_FY AS FUND_START_DATE_FY,
       TRUNK.ACCT_LONG_NAME AS ACCT_LONG_NAME,
       TRUNK.ACCT_ID AS ACCT_ID,
       TRUNK.CHANL_TYPE_DESC,--Added by Rajesh.S, HP for PR#PM00017063 on 22-Mar-2012
       TRUNK.FUND_LONG_NAME AS FUND_LONG_NAME,
       TRUNK.ACCT_GRP_NAME AS ACCT_GRP_NAME,
       TRUNK.FUND_CLASS_CODE,--Added by Rajesh.S, HP for PR#PM00017063 on 22-Mar-2012
       TRUNK.FUND_ID AS FUND_ID,
       POSTN_LKP.POSTN_NAME AS POSTN_NAME,
       TRUNK.MTH_NAME AS MTH_NAME,
       TRUNK.MTH_NUM as MTH_NUM,
       NVL(SHPMT_ACCRUED.BASE_SHPMT_AMT, 0) AS BASE_SHPMT_AMT,
       -- Optima11,B002,27-Oct-2010,Yves, begin
       NVL(SHPMT_ACCRUED.VOL_SU_AMT, 0) AS VOL_SU_AMT,
       -- Optima11,B002,27-Oct-2010,Yves, end
       NVL(SHPMT_ACCRUED.ACCRL_FUND_AMT, 0) AS ACCRL_FUND_AMT,
       NVL(TOT_BOOKED.TOT_BOOKED_AMT, 0) AS TOT_BOOKED_AMT,
       --Add by Kingham at 20100712 for CR340 --Start
       NVL(TOT_BOOKED.ON_INVC_BOOK_AMT,0) AS ON_INVC_BOOK_AMT,
       NVL(TOT_BOOKED.NON_ON_INVC_BOOK_AMT,0) AS NON_ON_INVC_BOOK_AMT,
       NVL(SHPMT_ACCRUED.ACCRL_FUND_AMT, 0) - NVL(TOT_BOOKED.TOT_BOOKED_AMT, 0) AS ACCRL_BOOKED,
       DECODE(NVL(SHPMT_ACCRUED.ACCRL_FUND_AMT, 0),
              0,
              0,
              NVL(TOT_BOOKED.TOT_BOOKED_AMT, 0) / NVL(SHPMT_ACCRUED.ACCRL_FUND_AMT, 0) * 100) AS BOOKED_VS_ACCRL,
       -- Start Added by Senthil, HP for PR#PM00017063
       NVL(TOT_TRF_ADJ.TNSFR_AMT,0) AS FUND_TNSFR_AMT,
       NVL(TOT_TRF_ADJ.ADJMT_AMT,0) AS FUND_ADJMT_AMT
       -- End Added by Senthil, HP for PR#PM00017063
FROM OPT_FUND_PROD_TRUNK_D_VW TRUNK,
     OPT_FUND_PROD_SHPMT_ACCRL_D_VW SHPMT_ACCRUED,
     OPT_FUND_TOT_BOOKED_D_VW TOT_BOOKED,
     OPT_FUND_TOT_ADJMT_TNSFR_D_VW TOT_TRF_ADJ, -- Added by Senthil, HP for PR#PM00017063
     (SELECT OPT_STRING_AGG(POSTN.NAME_DESC) AS POSTN_NAME, ACCT_POSTN.ORG_SKID AS ACCT_SKID
FROM OPT_POSTN_LKP POSTN, OPT_ACCT_POSTN_LKP ACCT_POSTN
WHERE POSTN.POSTN_ID = ACCT_POSTN.POSTN_ID
  AND ACCT_POSTN.ORG_SKID!=0
GROUP BY ACCT_POSTN.ORG_SKID) POSTN_LKP
WHERE TRUNK.ACCT_SKID = SHPMT_ACCRUED.ACCT_SKID(+)
  AND TRUNK.FUND_SKID = SHPMT_ACCRUED.FUND_SKID(+)
  AND TRUNK.MTH_SKID = SHPMT_ACCRUED.MTH_SKID(+)
  AND TRUNK.ACCT_SKID = TOT_BOOKED.ACCT_SKID(+)
  AND TRUNK.FUND_SKID = TOT_BOOKED.FUND_SKID(+)
  AND TRUNK.MTH_SKID = TOT_BOOKED.MTH_SKID(+)
  AND TRUNK.ACCT_SKID = POSTN_LKP.ACCT_SKID(+)
  -- Start Added by Senthil, HP for PR#PM00017063
  AND TRUNK.ACCT_SKID = TOT_TRF_ADJ.ACCT_SKID(+)
  AND TRUNK.FUND_SKID = TOT_TRF_ADJ.FUND_SKID(+)
  AND TRUNK.MTH_SKID = TOT_TRF_ADJ.MTH_SKID(+)
  -- End Added by Senthil, HP for PR#PM00017063
order by TRUNK.BUS_UNIT_NAME,TRUNK.FUND_START_DATE_FY,
         ACCT_LONG_NAME, ACCT_ID, FUND_LONG_NAME, FUND_ID, TRUNK.MTH_NUM';
      --Add by Kingham at 20100712 for CR340 --End
  dbms_output.put_line('Create table '||v_tbl_name||'...');
  pro_put_line(v_sql);
  execute immediate v_sql;
  dbms_output.put_line('Done!');

  --Gather statics
  --
  for idx in 1..V_VAR.COUNT
  loop
       pro_put_line('Gather statistics for table ' || V_VAR(idx) || ' (BEGIN)');
       DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                  TABNAME          => V_VAR(idx));
       pro_put_line('Gather statistics for table ' || V_VAR(idx) || ' (END)');
  end loop;

  -- Grant

  for idx in 1..V_VAR.COUNT
  LOOP
    SELECT 'Grant select on '|| V_VAR(idx)||' to '|| DECODE(in_grantee,NULL, user||'_ETL', in_grantee)
      INTO v_sql
      FROM dual;
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
	--SOX IMPLEMENTATION PROJECT STARTED
	V_SQL := 'grant select on ' || V_VAR(idx) || ' to ' || USER ||','||C_TBL_ROLES;
      PRO_PUT_LINE(V_SQL);
      EXECUTE IMMEDIATE V_SQL;
	  -- SOX IMPLEMENTATION PROJECT ENDED
    dbms_output.put_line('Done!');
  end loop;

exception
  when others then
    pro_put_line('Error: Unexpected system error - ' || sqlerrm);

END pre_dd_base_ship_fund;

/**********************************************************************/
/* name: pre_dd_ship_buom                                             */
/* purpose: pre-process of data dump shipment buom                    */
/* parameters:                                                        */
/*                                                                    */
/* author: Bodhi                                                      */
/* originator: Bodhi                                                  */
/* version: 1.00 - initial version                                    */
/**********************************************************************/
PROCEDURE pre_dd_ship_buom(in_grantee VARCHAR2 := NULL, in_dop INTEGER :=4) IS
  v_tbl_name VARCHAR2(100);
--  v_idx_name VARCHAR2(100);
  v_sql      VARCHAR2(32767);
  v_pp       VARCHAR2(32767) := NULL;
  v_count    number := 0;
  v_i        number := 0;

  TYPE T_VARRAY IS VARRAY(4) OF VARCHAR(50);
  V_VAR T_VARRAY := T_VARRAY('OPT_SHIP_BUOM_TDADS');

begin
  for tbl in (select upper(table_name) as table_name
              from user_tables
              where upper(table_name)='OPT_SHIP_BUOM_TDADS'
              )
  loop
    -- Drop table
    v_sql := 'DROP TABLE ' || tbl.table_name;
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
    dbms_output.put_line('Done!');
  end loop;

  select count(*) into v_count
    from opt_bus_unit_plc
   where bus_unit_skid<>0;

  for bus in (select bus_unit_skid, bus_unit_name
                from opt_bus_unit_plc
               where bus_unit_skid<>0
               order by bus_unit_skid
             )
  loop
      v_i := v_i+1;
      v_pp := v_pp||'  partition P_'||bus.bus_unit_skid||
                    ' values ('''||bus.bus_unit_name||''')'||
                    case when v_i <> v_count then
                        ','||CHR(10)
                    else
                        CHR(10)||')'||CHR(10)
                    end;
  end loop;

  pro_put_line(v_pp);

  v_tbl_name := 'OPT_SHIP_BUOM_TDADS';
  v_sql :='create table '||v_tbl_name ||CHR(10)||
          'partition by list (BUS_UNIT_name)'||CHR(10)||
          '('||CHR(10)||
          v_pp||CHR(10)||
          'parallel (degree '||in_dop||')'||CHR(10)||
          'NOLOGGING'||CHR(10)||
          'as '||CHR(10)||
          'SELECT BUS_UNIT_NAME,
       MTH_NAME, -- "Month Name"
       ACCT_ID,-- "Account ID"
       ACCT_NAME, -- "Account Name"
       GTIN_BUOM, -- "Product GTIN for BUOM"
       BRAND_ID, -- "Brand ID"
       BRAND_NAME, -- "Brand Name"
       SUM(GIV_AMT) AS GIV_AMT, -- "GIV"
       SUM(NIV_AMT) AS NIV_AMT, -- "NIV"
       SUM(VOL_BUOM_AMT) AS VOL_BUOM_AMT, -- "BUoM quantity"
       SUM(VOL_SU_AMT)  AS VOL_SU_AMT, -- "Volume SU"
       DATA_TYPE_CODE  -- "Flag of Shipment"
  FROM
  (
  SELECT
   BU.BUS_UNIT_NAME,
   initcap(substr(CAL.MTH_ABBR_NAME, 1,3))||'' ''||substr(CAL.MTH_ABBR_NAME,4,4) AS MTH_NAME,
   ACCT.ACCT_NAME AS ACCT_NAME,
   ACCT.NAME AS ACCT_ID,
   PROD.GTIN AS GTIN_BUOM,
   PROD_HIER.PROD_5_NAME AS BRAND_ID,
   PROD_HIER.PROD_5_PROD_DESC AS BRAND_NAME,
   SHPMT.GIV_AMT,
   SHPMT.NIV_AMT,
   SHPMT.VOL_BUOM_AMT,
   SHPMT.VOL_SU_AMT,
   SHPMT.DATA_TYPE_CODE
   /*
   DECODE(SHPMT.DATA_TYPE_CODE,
    ''BS'',''Direct'',
    ''ID'',''Indirect'',
    ''Other'') AS DATA_TYPE_CODE
    */
    FROM OPT_SHPMT_rpt_bFCT    SHPMT,
         --OPT_SHPMT_FCT         SHPMT,
         OPT_PROD_FDIM         PROD,
         OPT_PROD_HIER_FDIM      PROD_HIER,
         CAL_MASTR_DIM         CAL,
         OPT_BUS_UNIT_FDIM       BU,
         OPT_ACCT_FDIM         ACCT,
         OPT_ACCT_HIER_FDIM      ACCT_HIER
   WHERE SHPMT.PROD_SKID = PROD_HIER.PROD_SKID
     AND SHPMT.BUS_UNIT_SKID = PROD_HIER.BUS_UNIT_SKID
     AND CAL.FISC_YR_SKID = PROD_HIER.FY_DATE_SKID
     AND PROD_HIER.PROD_SKID = PROD.PROD_SKID
     AND SHPMT.BUS_UNIT_SKID = BU.BUS_UNIT_SKID
     AND SHPMT.ACCT_SKID = ACCT_HIER.ACCT_SKID
     AND SHPMT.SHPMT_DATE_SKID >= ACCT_HIER.SHPMT_START_DATE_SKID
     AND SHPMT.SHPMT_DATE_SKID <= ACCT_HIER.SHPMT_END_DATE_SKID
     AND ACCT_HIER.ACCT_1_SKID = ACCT.ACCT_SKID
     AND SHPMT.SHPMT_DATE_SKID = CAL.CAL_MASTR_SKID
     AND (CAL.DAY_DATE BETWEEN
         ADD_MONTHS(TRUNC(SYSDATE, ''MM''), -17) AND
         ADD_MONTHS(TRUNC(SYSDATE, ''MM''), 1) - 1)
) A
 GROUP BY
    BUS_UNIT_NAME,
    MTH_NAME,
    ACCT_ID,
    ACCT_NAME,
    GTIN_BUOM,
    BRAND_ID,
    BRAND_NAME,
    DATA_TYPE_CODE';
  dbms_output.put_line('Create table '||v_tbl_name||'...');
  pro_put_line(v_sql);


  --execute immediate v_sql;
  execute immediate v_sql;
  dbms_output.put_line('Done!');

  --Gather statics
  for idx in 1..V_VAR.COUNT
  loop
       pro_put_line('Gather statistics for table ' || V_VAR(idx) || ' (BEGIN)');
       DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                  TABNAME          => V_VAR(idx));
       pro_put_line('Gather statistics for table ' || V_VAR(idx) || ' (END)');
  end loop;

  -- Grant
  for idx in 1..V_VAR.COUNT
  LOOP
    SELECT 'Grant select on '|| V_VAR(idx)||' to '|| DECODE(in_grantee,NULL, user||'_ETL', in_grantee)
      INTO v_sql
      FROM dual;
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
	--SOX IMPLEMENTATION Project
	V_SQL := 'grant select on ' || V_VAR(idx) || ' to ' || USER ||','||C_TBL_ROLES;
      PRO_PUT_LINE(V_SQL);
      EXECUTE IMMEDIATE V_SQL;
	  --SOX IMPLEMENTATION PROJECT ENDED
    dbms_output.put_line('Done!');
  end loop;

exception
  when others then
    pro_put_line('Error: Unexpected system error - ' || sqlerrm);

END pre_dd_ship_buom;

/**********************************************************************/
/* name: pre_dd_ship_cu                                               */
/* purpose: pre-process of data dump shipment cu                      */
/*                                                                    */
/* parameters:                                                        */
/*                                                                    */
/* author: Bodhi                                                      */
/* originator: Bodhi                                                  */
/* version: 1.00 - initial version                                    */
/**********************************************************************/
PROCEDURE pre_dd_ship_cu(in_grantee VARCHAR2 := NULL, in_dop INTEGER :=4,V_REGN IN VARCHAR2 DEFAULT NULL) IS -- SOX Implementation project
  v_tbl_name VARCHAR2(100);
--  v_idx_name VARCHAR2(100);
  v_sql      VARCHAR2(32767);
  v_pp       VARCHAR2(32767) := NULL;
  v_count    number := 0;
  v_i        number := 0;

  TYPE T_VARRAY IS VARRAY(4) OF VARCHAR(50);
  V_VAR T_VARRAY := T_VARRAY('OPT_SHIP_CU_TDADS');

begin
  for tbl in (select upper(table_name) as table_name
              from user_tables
              where upper(table_name)='OPT_SHIP_CU_TDADS'
              )
  loop
    -- Drop table
    v_sql := 'DROP TABLE ' || tbl.table_name;
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
    dbms_output.put_line('Done!');
  end loop;

  select count(*) into v_count
    from opt_bus_unit_plc
   where bus_unit_skid<>0;

  for bus in (select bus_unit_skid, bus_unit_name
                from opt_bus_unit_plc
               where bus_unit_skid<>0
               order by bus_unit_skid
             )
  loop
      v_i := v_i+1;
      v_pp := v_pp||'  partition P_'||bus.bus_unit_skid||
                    ' values ('''||bus.bus_unit_name||''')'||
                    case when v_i <> v_count then
                        ','||CHR(10)
                    else
                        CHR(10)||')'||CHR(10)
                    end;
  end loop;

  pro_put_line(v_pp);

  v_tbl_name := 'OPT_SHIP_CU_TDADS';
  v_sql :='create table '||v_tbl_name ||CHR(10)||
          'partition by list (BUS_UNIT_name)'||CHR(10)||
          '('||CHR(10)||
          v_pp||CHR(10)||
          'parallel (degree '||in_dop||')'||CHR(10)||
          'NOLOGGING'||CHR(10)||
          'as '||CHR(10)||
          'SELECT BUS_UNIT_NAME,
       MTH_NAME, -- "Month Name"
       ACCT_ID,-- "Account ID"
       ACCT_NAME, -- "Account Name"
       GTIN_CU, -- "Product GTIN for CU"
       BRAND_ID, -- "Brand ID"
       BRAND_NAME, -- "Brand Name"
       SUM(GIV_AMT) AS GIV_AMT, -- "GIV"
       SUM(NIV_AMT) AS NIV_AMT, -- "NIV"
       SUM(VOL_BUOM_AMT * BUOM_CU_CONV_FACTR) AS VOL_CU_AMT, -- "BUoM quantity"
       SUM(VOL_SU_AMT)  AS VOL_SU_AMT, -- "Volume SU"
       DATA_TYPE_CODE  -- "Flag of Shipment"
  FROM (SELECT
         BU.BUS_UNIT_NAME,
         initcap(substr(CAL.MTH_ABBR_NAME, 1,3))||'' ''||substr(CAL.MTH_ABBR_NAME,4,4) AS MTH_NAME,
         ACCT.ACCT_NAME AS ACCT_NAME,
         ACCT.NAME AS ACCT_ID,
         PROD.GTIN AS GTIN_BUOM,
         PROD.CU_GTIN AS GTIN_CU,
         PROD.BUOM_CU_CONV_FACTR AS BUOM_CU_CONV_FACTR,
         PROD_HIER.PROD_5_NAME AS BRAND_ID,
         PROD_HIER.PROD_5_PROD_DESC AS BRAND_NAME,
         SHPMT.GIV_AMT,
         SHPMT.NIV_AMT,
         SHPMT.VOL_BUOM_AMT,
         SHPMT.VOL_SU_AMT,
         SHPMT.DATA_TYPE_CODE
   /*
   DECODE(SHPMT.DATA_TYPE_CODE,
    ''BS'',''Direct'',
    ''ID'',''Indirect'',
    ''Other'') AS DATA_TYPE_CODE
    */
          FROM OPT_SHPMT_rpt_bFCT    SHPMT,
               --OPT_SHPMT_FCT         SHPMT,
               OPT_PROD_FDIM            PROD,
               OPT_PROD_HIER_FDIM      PROD_HIER,
               CAL_MASTR_DIM           CAL,
               OPT_BUS_UNIT_FDIM       BU,
               OPT_ACCT_FDIM           ACCT,
         OPT_ACCT_HIER_FDIM      ACCT_HIER
   WHERE SHPMT.PROD_SKID = PROD_HIER.PROD_SKID
     AND SHPMT.BUS_UNIT_SKID = PROD_HIER.BUS_UNIT_SKID
     AND CAL.FISC_YR_SKID = PROD_HIER.FY_DATE_SKID
     AND PROD_HIER.PROD_SKID = PROD.PROD_SKID
     AND SHPMT.BUS_UNIT_SKID = BU.BUS_UNIT_SKID
     AND SHPMT.ACCT_SKID = ACCT_HIER.ACCT_SKID
     AND SHPMT.SHPMT_DATE_SKID >= ACCT_HIER.SHPMT_START_DATE_SKID
     AND SHPMT.SHPMT_DATE_SKID <= ACCT_HIER.SHPMT_END_DATE_SKID
     AND ACCT_HIER.ACCT_1_SKID = ACCT.ACCT_SKID
     AND SHPMT.SHPMT_DATE_SKID = CAL.CAL_MASTR_SKID
     AND (CAL.DAY_DATE BETWEEN
          ADD_MONTHS(TRUNC(SYSDATE, ''MM''), -17) AND
          ADD_MONTHS(TRUNC(SYSDATE, ''MM''), 1) - 1)
) A
 GROUP BY
    BUS_UNIT_NAME,
    MTH_NAME,
    ACCT_ID,
    ACCT_NAME,
    GTIN_CU,
    BRAND_ID,
    BRAND_NAME,
    DATA_TYPE_CODE';
  dbms_output.put_line('Create table '||v_tbl_name||'...');
  pro_put_line(v_sql);


  --execute immediate v_sql;
  execute immediate v_sql;
  dbms_output.put_line('Done!');

  --Gather statics
  for idx in 1..V_VAR.COUNT
  loop
       pro_put_line('Gather statistics for table ' || V_VAR(idx) || ' (BEGIN)');
       DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                  TABNAME          => V_VAR(idx));
       pro_put_line('Gather statistics for table ' || V_VAR(idx) || ' (END)');
  end loop;

  -- Grant
  for idx in 1..V_VAR.COUNT
  LOOP
    SELECT 'Grant select on '|| V_VAR(idx)||' to '|| DECODE(in_grantee,NULL, user||'_ETL', in_grantee)
      INTO v_sql
      FROM dual;
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
	--SOX IMPLEMENTATION PROJECT STARTED
	V_SQL := 'grant select on ' || V_VAR(idx) || ' to ' || USER ||','||C_TBL_ROLES;
      PRO_PUT_LINE(V_SQL);
      EXECUTE IMMEDIATE V_SQL;
	  --SOX IMPLEMENTATION PROJECT ENDED
    dbms_output.put_line('Done!');
  end loop;

exception
  when others then
    pro_put_line('Error: Unexpected system error - ' || sqlerrm);

END pre_dd_ship_cu;

PROCEDURE pre_extrt_prod_basln(in_grantee VARCHAR2 := NULL,in_dop INTEGER:=4,V_REGN IN VARCHAR2 DEFAULT NULL) IS -- SOX implementation project
  v_tbl_name VARCHAR2(100);
--  v_idx_name VARCHAR2(100);
  v_sql      VARCHAR2(32767);

  TYPE T_VARRAY IS VARRAY(4) OF VARCHAR(50);
  V_VAR T_VARRAY := T_VARRAY('OPT_PRMTN_PROD_BASLN_TDADS',
                             'OPT_PROD_BASLN_TDADS');

begin

  OPT_AX_TBLSPACE(V_REGN,C_TBLSPACE_NAME,V_INDEX);--SOX Implemenation project

  for tbl in (select upper(table_name) as table_name
              from user_tables
              where regexp_like(table_name,
                               'OPT_PROD_BASLN_TDADS$|OPT_PRMTN_PROD_BASLN_TDADS$')
              )
  loop
    -- Drop table
    v_sql := 'DROP TABLE ' || tbl.table_name;
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
    dbms_output.put_line('Done!');
  end loop;

  v_tbl_name := 'OPT_PROD_BASLN_TDADS';
  v_sql :='create table '||v_tbl_name ||CHR(10)||
          C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
          'parallel (degree '||in_dop||')'||CHR(10)||
          'NOLOGGING compress'||CHR(10)||
          'as '||CHR(10)||
          'select
PB.ROW_ID AS PROD_BASLN_ID,
PB.OU_EXT_ID AS ACCT_ID,
PB.PRDINT_ID AS PROD_ID,
DECODE(PROD.X_UI_PROD_LEVEL_DESC,
       ''Brand'',
       PROD.ROW_ID,
       PROD.PAR_PROD_INT_ID) AS BRAND_ID,
PB.PERIOD_ID AS PERD_ID,
NVL(PB.PLANNED_SALES,0) AS BASE_SALES_AMT,
PB.BU_ID AS BUS_UNIT_ID,
PB.X_TT_PERCENT AS TRADE_TERM_PCT,
PB.X_NIVSU_CONV AS NIV_SU_CONV_FACTR,
PROD.X_G_SUFCTOR AS X_G_SUFCTOR,
--added for F030 - David Lang
---------------------------------------------------------------------
-- Baseline Measures (SU/BUoM/GIV/NIV)
---------------------------------------------------------------------
--Baseline SU
NVL(PB.BASE_SALES,0) AS BASLN_SU_AMT,
--Baseline BUOM
ROUND(DECODE(NVL(PROD.X_G_SUFCTOR, 0),
             0,
             0,
             --Baseline BUOM = Baseline SU / SU Factor
             PB.BASE_SALES / PROD.X_G_SUFCTOR),
      7) AS BASLN_CASE_AMT,
--Baseline GIV = Baseline BUOM * Price
ROUND(DECODE(NVL(PROD.X_G_SUFCTOR, 0),
             0,
             0,
             --Baseline BUOM
             round(PB.BASE_SALES / PROD.X_G_SUFCTOR)) *  -- add for B038
             --Price (sum of price)
              NVL(ITEM.DFLT_COST_UNIT, 0),
      7) AS BASLN_GIV_AMT,
--Baseline NIV = Baseline SU * "NIV/SU Converter Factor"
ROUND(PB.BASE_SALES * NVL(PB.X_NIVSU_CONV, 0), 7) AS BASLN_NIV_AMT,
---------------------------------------------------------------------
-- Baseline Planning Measures (SU/BUoM/GIV/NIV)
---------------------------------------------------------------------
--Baseline Planning SU
NVL(PB.PLANNED_SALES,0) AS ESTMT_BASLN_SU_AMT,
--Baseline Planning BUOM = Baseline Planning SU / SU_factor
ROUND(DECODE(NVL(PROD.X_G_SUFCTOR, 0),
             0,
             0,
             --Baseline Planning SU
             NVL(PB.PLANNED_SALES,0) /
             -- SU_factor
             PROD.X_G_SUFCTOR),
      7) AS ESTMT_BASLN_CASES_AMT,
--Baseline Planning GIV = Baseline Planning BUOM * Price
ROUND(DECODE(NVL(PROD.X_G_SUFCTOR, 0),
             0,
             0,
             -- Baseline Planning BUOM = Baseline Planning SU / SU factor
             -- Baseline Planning SU
             round(NVL(PB.PLANNED_SALES,0) /       -- add for B038
             -- SU factor
            PROD.X_G_SUFCTOR)) *
      -- Price
      NVL(ITEM.DFLT_COST_UNIT, 0),
      7) AS ESTMT_BASLN_GIV_AMT,
--Baseline Planning NIV=Baseline Planning SU * "NIV/SU Converter Factor"
ROUND(PB.PLANNED_SALES *
      --"NIV/SU Converter Factor"
      NVL(PB.X_NIVSU_CONV, 0),
      7) AS ESTMT_BASLN_NIV_AMT
FROM
(
SELECT A.*,
       A.PRDINT_ID AS PROD_ID,
       ORG.BU_ID ,
       ORG.CURR_PRI_LST_ID
FROM S_PROD_BASELINE A,
     S_ORG_EXT ORG,
     OPT_BUS_UNIT_SKID_VW R
WHERE A.OU_EXT_ID= ORG.ROW_ID
  AND ORG.BU_ID = R.BUS_UNIT_ID
  and A.CG_SKIP_FLG = ''N''
) PB,
S_PRI_LST_ITEM ITEM,
S_PROD_INT     PROD
where PB.CURR_PRI_LST_ID = ITEM.PRI_LST_ID(+)
   AND PB.PRDINT_ID = ITEM.PROD_ID(+)
   AND PB.PERIOD_ID = ITEM.X_PERIOD_ID(+)
   AND PB.PROD_ID = PROD.ROW_ID(+)';
  dbms_output.put_line('Create table '||v_tbl_name||'...');
  pro_put_line(v_sql);

  --execute immediate v_sql;
  execute immediate v_sql;
  dbms_output.put_line('Done!');

  --Gather statics
  pro_put_line('Gather statistics for table ' || v_tbl_name || ' (BEGIN)');
  DBMS_STATS.GATHER_TABLE_STATS(OWNNAME     => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                TABNAME     => v_tbl_name);
  pro_put_line('Gather statistics for table ' || v_tbl_name || ' (END)');
  
  


  --OPT_PRMTN_PROD_BASLN_TDADS
  v_tbl_name := 'OPT_PRMTN_PROD_BASLN_TDADS';
  v_sql :='create table '||v_tbl_name ||CHR(10)||
          C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
          'parallel (degree '||in_dop||')'||CHR(10)||
          'NOLOGGING compress'||CHR(10)||
          'as '||CHR(10)||
          'SELECT PPB.PROD_BASELINE_ID AS PRMTN_PROD_BASLN_ID,
       ---------------------------------------------------------------------
       -- Incremental Measures (SU/BUoM/GIV/NIV)
       ---------------------------------------------------------------------
       --Incremental SU = if (Category Model=''Percent'', "Baseline Planning SU" * Percentage, Stored Incremental SU )
       ROUND(SUM(DECODE(CATEG.X_INCREMENTAL_MODEL,
                        ''Percent'',
                        -- Baseline Planning SU * Percentage
                        NVL(PPB.PRD_BSLN_PCT *
                             PB.BASE_SALES_AMT / 100 *
                            PPB.X_INCREMENTAL_PCT / 100, 0),
                        -- Stored Incremental SU
                        NVL(PPB.CG_INCR_SALES, 0))),
             7) AS INCRM_SU_AMT,
       --Incremental BUOM = Incremental SU / SU factor
       ROUND(-- Incremental SU
    SUM(DECODE(NVL(PB.X_G_SUFCTOR,0),0,0,                                    DECODE(CATEG.X_INCREMENTAL_MODEL,''Percent'',
                               -- Baseline Planning SU * Percentage
                               NVL(PPB.PRD_BSLN_PCT *
                                    PB.BASE_SALES_AMT / 100 *
                                   PPB.X_INCREMENTAL_PCT / 100, 0),
                               -- Stored Incremental SU
                               NVL( PPB.CG_INCR_SALES, 0))/
                               --SU factor
                               PB.X_G_SUFCTOR)),
             7) AS INCRM_IN_CASE_AMT,
       --Incremental GIV = Incremental BUOM * Price
       ROUND(
             --Incremental BUOM
             -- Incremental SU
             SUM(DECODE(NVL(PB.X_G_SUFCTOR,0),0,0,
                 round(DECODE(CATEG.X_INCREMENTAL_MODEL,''Percent'',   --add for b038
                        -- Baseline Planning SU * Percentage
                        NVL( PPB.PRD_BSLN_PCT *
                              PB.BASE_SALES_AMT / 100 *
                             PPB.X_INCREMENTAL_PCT / 100, 0),
                        -- Stored Incremental SU
                        NVL( PPB.CG_INCR_SALES, 0)) /
                        --SU factor
                        PB.X_G_SUFCTOR) *
                        --Price
                        NVL(ITEM.DFLT_COST_UNIT,0))),
             7) AS INCRM_IN_GIV_AMT,
       --Incremental NIV = Incremental SU * "NIV/SU Converter Factor"
       ROUND(
             --Incremental SU
             SUM(DECODE(CATEG.X_INCREMENTAL_MODEL,''Percent'',
                        -- Baseline Planning SU * Percentage
                        NVL(PPB.PRD_BSLN_PCT *
                             PB.BASE_SALES_AMT / 100 *
                            PPB.X_INCREMENTAL_PCT / 100, 0),
                        -- Stored Incremental SU
                        NVL(PPB.CG_INCR_SALES,0))*
                        -- "NIV/SU Converter Factor"
                        NVL(PB.NIV_SU_CONV_FACTR, 0)),
             7) AS INCRM_IN_NIV_AMT
FROM
S_SRC_PRD_BASLN      PPB,
OPT_PROD_BASLN_TDADS PB,
S_PRI_LST_ITEM       ITEM,
S_SRC                PROMO_PROD,
S_SRC                CATEG,
(SELECT ROW_ID, STATUS_CD, SUB_TYPE
   FROM S_SRC
  WHERE (S_SRC.SRC_CD <> ''Annual Agreement'' OR S_SRC.SRC_CD IS NULL) --R8 Defect 1372
    AND REGEXP_LIKE(S_SRC.STATUS_CD,''Planned|Confirmed|Revised|Completed'') --R8 Defect 1372
    AND NVL(S_SRC.X_STOPPED_DT, S_SRC.PROG_END_DT) >= S_SRC.PROG_START_DT --R8 Defect 1372)
 )PROMO
WHERE PPB.PROD_BASELINE_ID = PB.PROD_BASLN_ID
  AND PPB.X_PERIOD_ID = ITEM.X_PERIOD_ID(+)
  AND PPB.X_PROD_ID = ITEM.PROD_ID(+)
  AND PPB.X_PRI_LST_ID = ITEM.PRI_LST_ID(+)
  AND PPB.SRC_ID = PROMO_PROD.ROW_ID
  AND PROMO_PROD.PAR_SRC_ID = CATEG.ROW_ID
  AND CATEG.PAR_SRC_ID = PROMO.ROW_ID
/*
  AND PROMO_PROD.SUB_TYPE(+) = ''PLAN_ACCOUNT_PROMOTION_PRODUCT''  --R8 Defect 1372
  AND CATEG.SUB_TYPE(+) = ''PLAN_ACCT_PROMOTION_CATEGORY''  --R8 Defect 1372
  AND PROMO.SUB_TYPE(+) = ''PLAN_ACCOUNT_PROMOTION''  --R8 Defect 1372
*/
GROUP BY PPB.PROD_BASELINE_ID';
  dbms_output.put_line('Create table '||v_tbl_name||'...');
  pro_put_line(v_sql);

  --execute immediate v_sql;
  execute immediate v_sql;
  dbms_output.put_line('Done!');

  --Gather statics
  pro_put_line('Gather statistics for table ' || v_tbl_name || ' (BEGIN)');
  DBMS_STATS.GATHER_TABLE_STATS(OWNNAME     => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                TABNAME     => v_tbl_name);
  pro_put_line('Gather statistics for table ' || v_tbl_name || ' (END)');


  -- Grant
  for idx in 1..V_VAR.COUNT
  LOOP
    SELECT 'Grant select on '|| V_VAR(idx)||' to '|| DECODE(in_grantee,NULL, user||'_ETL', in_grantee)
      INTO v_sql
      FROM dual;
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
	--SOX IMPLEMENTATION PROJECT STARTED
	V_SQL := 'grant select on ' || V_VAR(idx) || ' to ' || USER ||','||C_TBL_ROLES;
      PRO_PUT_LINE(V_SQL);
      EXECUTE IMMEDIATE V_SQL;
	  --SOX IMPLEMENTATION PROJECT ENDED
    dbms_output.put_line('Done!');
  end loop;

exception
  when others then
    pro_put_line('Error: Unexpected system error - ' || sqlerrm);

END pre_extrt_prod_basln;

PROCEDURE pre_extrt_opt_prmtn_prod_f (V_REGN IN VARCHAR2 DEFAULT NULL) IS -- SOX Implementation project

v_sql varchar2(30000);
v_cnt  number;
v_user varchar2(500);
c_table_name varchar2(500);
c_index_name varchar2(500);
c_column_indexed varchar2(500);
BEGIN

  OPT_AX_TBLSPACE(V_REGN,C_TBLSPACE_NAME,V_INDEX);--SOX Implementation project

  c_table_name := 'OPT_PRMTN_PROD_FCT_SVP';
  c_index_name := 'PRMTN_PROD_FCT_SVP_IDX';
  c_column_indexed := 'ROW_ID';

  -- drop temp table if it exists
  select count(1) into v_cnt from user_tables where table_name = c_table_name;
  if v_cnt > 0 then

    v_sql := 'Drop table '|| c_table_name|| ' cascade constraints';
    pro_put_line('Drop table '|| c_table_name || '(BEGIN)');
    pro_put_line(v_sql);
    execute immediate v_sql;
    pro_put_line('Drop table '|| c_table_name || '(END)');
  end if;


  -- drop index if it exists
  select count(1) into v_cnt from user_indexes where index_name = c_index_name;
  if v_cnt > 0 then
    v_sql := 'drop index '|| c_index_name;
    pro_put_line('Drop index ' || c_index_name|| ' (BEGIN)');
    pro_put_line(v_sql);
    execute immediate v_sql;
    pro_put_line('Drop index ' || c_index_name|| ' (END)');
  end if;

  -- create temp table
  v_sql:= 'create table '||c_table_name ||CHR(10)||
          'NOLOGGING'||CHR(10)||
          ' as select * from OPT_PRMTN_PROD_FCT_SVP_VW';
  pro_put_line('Create table '|| c_table_name || '(BEGIN)');
  pro_put_line(v_sql);
  execute immediate v_sql;
  pro_put_line('Create table '|| c_table_name || '(END)');


  -- create index for temp table
  v_sql:= 'create index '|| c_index_name || ' on '||c_table_name||'('|| c_column_indexed||') nologging';
  pro_put_line('Craete index '|| c_index_name || ' for temp table (BEGIN)');
  pro_put_line(v_sql);
  execute immediate v_sql;
  pro_put_line('Craete index '|| c_index_name || ' for temp table (END)');

  -- get the owner of the temp table
  begin
    select owner into v_user from all_tables where table_name =  c_table_name;
  exception
    when no_data_found then
    pro_put_line('Error: No temp table found');
    raise_application_error(-20000, 'Error: No Temp Table found');
  end;

  -- GRANT PERMISSION TO THE TABLE SOX IMPLEMENTATION PROJECT
  SELECT 'Grant select on '|| c_table_name||' to '|| DECODE(in_grantee,NULL, user||'_ETL', in_grantee)
      INTO v_sql
      FROM dual;
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
	V_SQL := 'grant select on ' || c_table_name || ' to ' || USER ||','||C_TBL_ROLES;
      PRO_PUT_LINE(V_SQL);
      EXECUTE IMMEDIATE V_SQL;
	  --SOX IMPLEMENTATION PROJECT ENDED
    dbms_output.put_line('Done!');
	
	--GRANT PERMISSION TO THE TABLE SOX IMPLEMENTATION PROJECT ENDED

  -- gather statistics for the temp table
  pro_put_line('Gather statistics for table '|| c_table_name || ' (START)');
  DBMS_STATS.GATHER_TABLE_STATS(      ownname          => v_user,
                                      tabname          => c_table_name,
                                      estimate_percent => 10,
                                      method_opt       => 'FOR ALL COLUMNS SIZE AUTO',
                                      granularity      => 'AUTO',
                                      CASCADE          => TRUE);
  pro_put_line('Gather statistics for table '|| c_table_name || ' (END)');

 exception
  when others then
    pro_put_line('Error: Unexpected system error - ' || sqlerrm);
END pre_extrt_opt_prmtn_prod_f;


/**********************************************************************/
/* name: pre_extrt_actvy_spnd                                         */
/* purpose: Create temp table to improve performance for OPT_ACTVY_SPNDG_MTH_FCT */
/* parameters: in_grantee - the user which invoke the procedure       */
/*                                                                    */
/* author: Luke Lu                                                    */
/* version: 1.00 - initial version                                    */
/**********************************************************************/
PROCEDURE pre_extrt_actvy_spnd(in_grantee VARCHAR2 := NULL,V_REGN IN VARCHAR2 DEFAULT NULL) IS -- SOX Implementation project
  v_tbl_name VARCHAR2(100);
  v_sql      VARCHAR2(32767);

  TYPE T_VARRAY IS VARRAY(7) OF VARCHAR(50);
  V_VAR T_VARRAY := T_VARRAY('OPT_BRAND_HIER_TDADS',
                             'OPT_ACTVY_GTIN_TDADS',
                             'OPT_ACTVY_SHPMT_SPLIT_TDADS',
                             'OPT_PRMTN_BRAND_SPLIT_TDADS',
                             'OPT_ACTVY_BRAND_SPLIT_TDADS',
                             'OPT_ACTVY_MTH_TDADS',
                             'OPT_ACTVY_SHPMT_MTH_TDADS'
                             );

begin

  OPT_AX_TBLSPACE(V_REGN,C_TBLSPACE_NAME,V_INDEX);--SOX Implementation project

  for tbl in (select upper(table_name) as table_name
              from user_tables
              where regexp_like(table_name,
                    'OPT_BRAND_HIER_TDADS$|OPT_ACTVY_GTIN_TDADS$|OPT_ACTVY_SHPMT_SPLIT_TDADS$|OPT_PRMTN_BRAND_SPLIT_TDADS$|OPT_ACTVY_BRAND_SPLIT_TDADS$|OPT_ACTVY_MTH_TDADS$|OPT_ACTVY_SHPMT_MTH_TDADS','i')
              )
  loop
    -- Drop table
    v_sql := 'DROP TABLE ' || tbl.table_name || ' PURGE';
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
    dbms_output.put_line('Done!');
  end loop;

  -- 2009-06-29
  -- Add on R8 production by Luke
  -- Add temp tables for performance tuning.
  v_tbl_name := 'OPT_BRAND_HIER_TDADS';
  v_sql :='create table '||v_tbl_name||CHR(10)||
          C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
          C_TABLE_DOP ||CHR(10)||
          'NOLOGGING compress'||CHR(10)||
          'as '||CHR(10)||
          '--Brand and below
SELECT PROD_ESI.PROD_SKID PROD_SKID,
       CONNECT_BY_ROOT(PROD_ESI.PROD_SKID) BRAND_SKID,
       CONNECT_BY_ROOT(PROD_ESI.BRAND_SPLIT_QTY) BRAND_SPLIT,
       PROD_ESI.FY_DATE_SKID,
       PROD_ESI.PROD_LVL_DESC
  FROM OPT_PROD_FY_FDIM PROD_ESI
CONNECT BY PRIOR PROD_ESI.PROD_ID = PROD_ESI.PARNT_PROD_ID
       AND PRIOR PROD_ESI.FY_DATE_SKID = PROD_ESI.FY_DATE_SKID
 START WITH PROD_ESI.PROD_SKID IN
            (SELECT PROD_SKID
               FROM OPT_PROD_DIM
              WHERE PROD_LVL_DESC = ''Brand'')
UNION
--Brand and above
SELECT PROD_ESI.PROD_SKID PROD_SKID,
       CONNECT_BY_ROOT(PROD_ESI.PROD_SKID) BRAND_SKID,
       CONNECT_BY_ROOT(PROD_ESI.BRAND_SPLIT_QTY) BRAND_SPLIT,
       PROD_ESI.FY_DATE_SKID,
       PROD_ESI.PROD_LVL_DESC
  FROM OPT_PROD_FY_FDIM PROD_ESI
CONNECT BY PROD_ESI.PROD_ID = PRIOR PROD_ESI.PARNT_PROD_ID
       AND PROD_ESI.FY_DATE_SKID = PRIOR PROD_ESI.FY_DATE_SKID
 START WITH PROD_ESI.PROD_SKID IN
            (SELECT PROD_SKID
               FROM OPT_PROD_DIM
              WHERE PROD_LVL_DESC = ''Brand'')';
  dbms_output.put_line('Create table '||v_tbl_name||'...');
  pro_put_line(v_sql);
  execute immediate v_sql;
  dbms_output.put_line('Create table '||v_tbl_name||' Done!');
  pro_put_line('Gather statistics for table ' || v_tbl_name || ' (BEGIN)');
  DBMS_STATS.GATHER_TABLE_STATS(OWNNAME     => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                TABNAME     => v_tbl_name);
  pro_put_line('Gather statistics for table ' || v_tbl_name || ' (END)');

  -- 2009-06-29
  -- Add on R8 production by Luke
  -- Add temp tables for performance tuning.
  v_tbl_name := 'OPT_ACTVY_GTIN_TDADS';
  v_sql :='create table '||v_tbl_name ||CHR(10)||
          C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
          C_TABLE_DOP ||CHR(10)||
          'NOLOGGING compress'||CHR(10)||
          'as '||CHR(10)||
          'SELECT ACTVY_PROD.ACTVY_SKID,
       ACTVY_PROD.ACCT_PRMTN_SKID,
       ACTVY_PROD.FY_DATE_SKID,
       ACTVY_PROD.PRICE_FEATR_DISC_TYPE_CODE,
       ACTVY_PROD.ACTVY_PROD_DISC_RATE_AMT,
       PROD_ASDN.PROD_SKID
  FROM OPT_ACTVY_PROD_FCT ACTVY_PROD, OPT_PROD_ASDN_DIM PROD_ASDN
 WHERE ACTVY_PROD.PROD_SKID = PROD_ASDN.ROOT_PROD_SKID
   AND ACTVY_PROD.FY_DATE_SKID = PROD_ASDN.FY_DATE_SKID
   AND PROD_ASDN.PROD_SKID IN
       (SELECT PROD_SKID FROM OPT_PROD_DIM WHERE PROD_LVL_DESC = ''GTIN'')';
  dbms_output.put_line('Create table '||v_tbl_name||'...');
  pro_put_line(v_sql);
  execute immediate v_sql;
  dbms_output.put_line('Create table '||v_tbl_name||' Done!');

  v_tbl_name := 'OPT_ACTVY_SHPMT_SPLIT_TDADS';
  v_sql :='create table '||v_tbl_name||CHR(10)||
          C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
          C_TABLE_DOP||CHR(10)||
          'NOLOGGING compress'||CHR(10)||
          'as '||CHR(10)||
          'SELECT ACTVY_SKID,
       ACCT_PRMTN_SKID,
       BRAND_SKID,
       MTH_SKID,
       FY_DATE_SKID,
       DECODE(PRICE_FEATR_DISC_TYPE_CODE,
              ''%'',
              DECODE(ACTVY_MTH_SHPMT_BRAND_GIV,
                     0,
                     0,
                     ACTVY_MTH_SHPMT_BRAND_GIV / ACTVY_MTH_SHPMT_GIV),
              NULL,
              0,
              DECODE(ACTVY_MTH_SHPMT_BRAND_BUOM,
                     0,
                     0,
                     ACTVY_MTH_SHPMT_BRAND_BUOM / ACTVY_MTH_SHPMT_BUOM)) AS ACTVY_SHPMT_BRAND_SPLT
  FROM ( --Associate the activity products to shipment data
       --Using the following relations
       --Activity Product join to high level products in hierarchy, Shipment product to GTIN LEVEL
       --Shipment accounts should be the children of Promotion account of activity in ACCOUNT HIERARCHY
       --The shipment date should between the activity start and end date
       --Aggregate the data to Brand and Month level
         SELECT /* parallel(SHPMT 4) */
                ACTVY_PROD.ACTVY_SKID,
                ACTVY_PROD.ACCT_PRMTN_SKID,
                BRAND_HIER.BRAND_SKID,
                CAL.MTH_SKID AS MTH_SKID,
                ACTVY_PROD.FY_DATE_SKID,
                ACTVY_PROD.PRICE_FEATR_DISC_TYPE_CODE,
                --Sum of the shipment amount of one activity per month per Brand
                SUM(SHPMT.GIV_AMT * ACTVY_PROD.ACTVY_PROD_DISC_RATE_AMT / 100) AS ACTVY_MTH_SHPMT_BRAND_GIV,
                SUM(SHPMT.VOL_BUOM_AMT * ACTVY_PROD.ACTVY_PROD_DISC_RATE_AMT) AS ACTVY_MTH_SHPMT_BRAND_BUOM,
                --Sum of the shipment amount of one activity per month
                SUM(SUM(SHPMT.GIV_AMT * ACTVY_PROD.ACTVY_PROD_DISC_RATE_AMT / 100)) OVER(PARTITION BY ACTVY_PROD.ACTVY_SKID, ACTVY_PROD.ACCT_PRMTN_SKID, CAL.MTH_SKID) AS ACTVY_MTH_SHPMT_GIV,
                SUM(SUM(SHPMT.VOL_BUOM_AMT *
                        ACTVY_PROD.ACTVY_PROD_DISC_RATE_AMT)) OVER(PARTITION BY ACTVY_PROD.ACTVY_SKID, ACTVY_PROD.ACCT_PRMTN_SKID, CAL.MTH_SKID) AS ACTVY_MTH_SHPMT_BUOM
           FROM OPT_ACTVY_GTIN_TDADS    ACTVY_PROD,
                OPT_SHPMT_RPT_BFCT      SHPMT,
                OPT_ACCT_ASDN_TYPE2_DIM ACCT_ASDN,
                OPT_ACTVY_FDIM          ACTVY,
                OPT_BRAND_HIER_TDADS    BRAND_HIER,
                OPT_CAL_MASTR_DIM       CAL
          WHERE ACTVY_PROD.ACCT_PRMTN_SKID = ACCT_ASDN.ASSOC_ACCT_SKID
            AND SHPMT.ACCT_SKID = ACCT_ASDN.ACCT_SKID
            AND SHPMT.SHPMT_DATE_SKID BETWEEN
                ACCT_ASDN.ASDN_EFF_START_DATE_SKID AND
                ACCT_ASDN.ASDN_EFF_END_DATE_SKID
            AND ACTVY_PROD.PROD_SKID = SHPMT.PROD_SKID
            AND SHPMT.SHPMT_DATE_SKID = CAL.CAL_MASTR_SKID
            AND TRUNC(CAL.DAY_DATE) BETWEEN ACTVY.PGM_START_DATE AND
                ACTVY.PGM_END_DATE
            AND ACTVY_PROD.ACTVY_SKID = ACTVY.ACTVY_SKID
            --Using the Brand hier to aggregate the actvity product to Brand level
            AND ACTVY_PROD.PROD_SKID = BRAND_HIER.PROD_SKID
            AND ACTVY_PROD.FY_DATE_SKID = BRAND_HIER.FY_DATE_SKID
          GROUP BY ACTVY_PROD.ACTVY_SKID,
                   ACTVY_PROD.ACCT_PRMTN_SKID,
                   BRAND_HIER.BRAND_SKID,
                   CAL.MTH_SKID,
                   ACTVY_PROD.FY_DATE_SKID,
                   ACTVY_PROD.PRICE_FEATR_DISC_TYPE_CODE
         --Filter out the data which discount set to 0
         --For those data using Fix type to splite the Activity cost
         HAVING SUM(ACTVY_PROD.ACTVY_PROD_DISC_RATE_AMT) <> 0)';
  dbms_output.put_line('Create table '||v_tbl_name||'...');
  pro_put_line(v_sql);
  execute immediate v_sql;
  dbms_output.put_line('Create table '||v_tbl_name||' Done!');

  v_tbl_name := 'OPT_PRMTN_BRAND_SPLIT_TDADS';
  v_sql :='create table '||v_tbl_name||CHR(10)||
          C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
          C_TABLE_DOP||CHR(10)||
          'NOLOGGING compress'||CHR(10)||
          'as '||CHR(10)||
          'SELECT PRMTN_SKID,
       BRAND_SKID,
       FY_DATE_SKID,
       DECODE(BRAND_SPLIT_IND,
              ''N'',
              1 / BRAND_SPLIT_CNT,
              DECODE(BRAND_SPLIT_SUM,
                     0,
                     1 / BRAND_SPLIT_CNT,
                     BRAND_SPLIT / BRAND_SPLIT_SUM)) BRAND_SPLIT
  FROM (SELECT PRMTN_PROD.PRMTN_SKID,
               BRAND_HIER.BRAND_SKID,
               BRAND_HIER.FY_DATE_SKID,
               MAX(BRAND_HIER.BRAND_SPLIT) BRAND_SPLIT,
               SUM(MAX(BRAND_HIER.BRAND_SPLIT)) OVER(PARTITION BY PRMTN_PROD.PRMTN_SKID) BRAND_SPLIT_SUM,
               SUM(COUNT(DISTINCT BRAND_HIER.BRAND_SKID)) OVER(PARTITION BY PRMTN_PROD.PRMTN_SKID) BRAND_SPLIT_CNT,
               MIN(MIN(DECODE(BRAND_HIER.BRAND_SPLIT, NULL, ''N'', ''Y''))) OVER(PARTITION BY PRMTN_PROD.PRMTN_SKID) BRAND_SPLIT_IND
          FROM OPT_PRMTN_PROD_FCT PRMTN_PROD,
               OPT_BRAND_HIER_TDADS  BRAND_HIER
         WHERE PRMTN_PROD.PROD_SKID = BRAND_HIER.PROD_SKID
           AND PRMTN_PROD.FY_DATE_SKID = BRAND_HIER.FY_DATE_SKID
           --AND BRAND_HIER.PROD_LVL_DESC = ''GTIN''
           AND PRMTN_PROD.PRMTN_SKID <> 0
         GROUP BY PRMTN_PROD.PRMTN_SKID,
                  BRAND_HIER.BRAND_SKID,
                  BRAND_HIER.FY_DATE_SKID)';
  dbms_output.put_line('Create table '||v_tbl_name||'...');
  pro_put_line(v_sql);
  execute immediate v_sql;
  dbms_output.put_line('Create table '||v_tbl_name||' Done!');

  v_tbl_name := 'OPT_ACTVY_BRAND_SPLIT_TDADS';
  v_sql :='create table '||v_tbl_name||CHR(10)||
          C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
          C_TABLE_DOP||CHR(10)||
          'NOLOGGING compress'||CHR(10)||
          'as '||CHR(10)||
          'SELECT ACTVY_SKID,
       BRAND_SKID,
       FY_DATE_SKID,
       DECODE(BRAND_SPLIT_IND,
              ''N'',
              1 / BRAND_SPLIT_CNT,
              DECODE(BRAND_SPLIT_SUM,
                     0,
                     1 / BRAND_SPLIT_CNT,
                     BRAND_SPLIT / BRAND_SPLIT_SUM)) BRAND_SPLIT
  FROM (SELECT ACTVY_PROD.ACTVY_SKID,
               BRAND_HIER.BRAND_SKID,
               BRAND_HIER.FY_DATE_SKID,
               MAX(BRAND_HIER.BRAND_SPLIT) BRAND_SPLIT,
               SUM(MAX(BRAND_HIER.BRAND_SPLIT)) OVER(PARTITION BY ACTVY_PROD.ACTVY_SKID) BRAND_SPLIT_SUM,
               SUM(COUNT(DISTINCT BRAND_HIER.BRAND_SKID)) OVER(PARTITION BY ACTVY_PROD.ACTVY_SKID) BRAND_SPLIT_CNT,
               MIN(MIN(DECODE(BRAND_HIER.BRAND_SPLIT, NULL, ''N'', ''Y''))) OVER(PARTITION BY ACTVY_PROD.ACTVY_SKID) BRAND_SPLIT_IND
          FROM OPT_ACTVY_PROD_FCT   ACTVY_PROD,
               OPT_BRAND_HIER_TDADS BRAND_HIER
         WHERE ACTVY_PROD.PROD_SKID = BRAND_HIER.PROD_SKID
           AND ACTVY_PROD.FY_DATE_SKID = BRAND_HIER.FY_DATE_SKID
           --AND BRAND_HIER.PROD_LVL_DESC = ''GTIN''
           AND ACTVY_PROD.ACTVY_SKID <> 0
         GROUP BY ACTVY_PROD.ACTVY_SKID,
                  BRAND_HIER.BRAND_SKID,
                  BRAND_HIER.FY_DATE_SKID)';
  dbms_output.put_line('Create table '||v_tbl_name||'...');
  pro_put_line(v_sql);
  execute immediate v_sql;
  dbms_output.put_line('Create table '||v_tbl_name||' Done!');

  v_tbl_name := 'OPT_ACTVY_MTH_TDADS';
  v_sql :='create table '||v_tbl_name||CHR(10)||
          C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
          C_TABLE_DOP||CHR(10)||
          'NOLOGGING compress'||CHR(10)||
          'as '||CHR(10)||
          'SELECT ACTVY.ACTVY_SKID,
       CAL.MTH_SKID,
       MAX(ACTVY_FCT.PRDCT_FIXED_COST_AMT) *
       (COUNT(1) / SUM(COUNT(1)) OVER(PARTITION BY ACTVY.ACTVY_SKID)) AS ESTMT_FIX_MTH_COST,
       MAX(ACTVY_FCT.VAR_COST_ESTMT_AMT) *
       (COUNT(1) / SUM(COUNT(1)) OVER(PARTITION BY ACTVY.ACTVY_SKID)) AS ESTMT_VAR_MTH_COST,
       MAX(ACTVY_FCT.PRDCT_FIXED_COST_AMT + ACTVY_FCT.VAR_COST_ESTMT_AMT) *
       (COUNT(1) / SUM(COUNT(1)) OVER(PARTITION BY ACTVY.ACTVY_SKID)) AS ESTMT_TOT_MTH_COST,
       -- Add the Estimated Total Revised Cost calculation for F057
       -- Estimated Total Revised Cost = Estimated Fixed Cost + Estimated Revised Variable Cost
       MAX(ACTVY_FCT.PRDCT_FIXED_COST_AMT + ACTVY_FCT.REVSD_VAR_ESTMT_COST_AMT) *
       (COUNT(1) / SUM(COUNT(1)) OVER(PARTITION BY ACTVY.ACTVY_SKID)) AS ESTMT_TOT_REVSD_MTH_COST,
       MAX(ACTVY_FCT.CALC_INDEX_NUM + ACTVY_FCT.ACTL_VAR_COST_NUM) *
       (COUNT(1) / SUM(COUNT(1)) OVER(PARTITION BY ACTVY.ACTVY_SKID)) AS ACTL_TOT_MTH_COST,
       MAX(ACTVY_FCT.CALC_INDEX_NUM) *
       (COUNT(1) / SUM(COUNT(1)) OVER(PARTITION BY ACTVY.ACTVY_SKID)) AS ACTL_FIX_MTH_COST,
       MAX(ACTVY_FCT.ACTL_VAR_COST_NUM) *
       (COUNT(1) / SUM(COUNT(1)) OVER(PARTITION BY ACTVY.ACTVY_SKID)) AS ACTL_VAR_MTH_COST,
       MAX(ACTVY_FCT.CMMT_AMT + ACTVY_FCT.PAID_PYMT_AMT + ACTVY_FCT.PYMT_BOOK_AMT) *
       (COUNT(1) / SUM(COUNT(1)) OVER(PARTITION BY ACTVY.ACTVY_SKID)) AS PROJ_SPND_AMT
  FROM OPT_ACTVY_DIM ACTVY,
       OPT_ACTVY_FCT ACTVY_FCT,
       CAL_MASTR_DIM CAL
 WHERE CAL.DAY_DATE BETWEEN ACTVY.PGM_START_DATE AND ACTVY.PGM_END_DATE
   AND CAL.DAY_DATE BETWEEN TO_DATE(''20000101'',''YYYYMMDD'') AND ADD_MONTHS(SYSDATE, 36)
   AND ACTVY.ACTVY_SKID = ACTVY_FCT.ACTVY_SKID
   AND ACTVY.ACTVY_SKID <> 0
 GROUP BY ACTVY.ACTVY_SKID,
          CAL.MTH_SKID';
  dbms_output.put_line('Create table '||v_tbl_name||'...');
  pro_put_line(v_sql);
  execute immediate v_sql;
  dbms_output.put_line('Create table '||v_tbl_name||' Done!');

  v_tbl_name := 'OPT_ACTVY_SHPMT_MTH_TDADS';
  v_sql :='create table '||v_tbl_name||CHR(10)||
          C_FINAL_TBL_STORAGE||' '|| C_TBLSPACE_NAME || CHR(10) ||
          C_TABLE_DOP||CHR(10)||
          'NOLOGGING compress'||CHR(10)||
          'as '||CHR(10)||
          'SELECT ACTVY_FCT.ACTVY_SKID,
       CAL.MTH_SKID,
       MAX(ACTVY_FCT.PRDCT_FIXED_COST_AMT) *
       (COUNT(1) / SUM(COUNT(1)) OVER(PARTITION BY ACTVY_FCT.ACTVY_SKID)) AS ESTMT_FIX_MTH_COST,
       MAX(ACTVY_FCT.VAR_COST_ESTMT_AMT) *
       (COUNT(1) / SUM(COUNT(1)) OVER(PARTITION BY ACTVY_FCT.ACTVY_SKID)) AS ESTMT_VAR_MTH_COST,
       MAX(ACTVY_FCT.PRDCT_FIXED_COST_AMT + ACTVY_FCT.VAR_COST_ESTMT_AMT) *
       (COUNT(1) / SUM(COUNT(1)) OVER(PARTITION BY ACTVY_FCT.ACTVY_SKID)) AS ESTMT_TOT_MTH_COST,
       -- Add the Estimated Total Revised Cost calculation for F057
       -- Estimated Total Revised Cost = Estimated Fixed Cost + Estimated Revised Variable Cost
       MAX(ACTVY_FCT.PRDCT_FIXED_COST_AMT + ACTVY_FCT.REVSD_VAR_ESTMT_COST_AMT) *
       (COUNT(1) / SUM(COUNT(1)) OVER(PARTITION BY ACTVY_FCT.ACTVY_SKID)) AS ESTMT_TOT_REVSD_MTH_COST,
       MAX(ACTVY_FCT.CALC_INDEX_NUM + ACTVY_FCT.ACTL_VAR_COST_NUM) *
       (COUNT(1) / SUM(COUNT(1)) OVER(PARTITION BY ACTVY_FCT.ACTVY_SKID)) AS ACTL_TOT_MTH_COST,
       MAX(ACTVY_FCT.CALC_INDEX_NUM) *
       (COUNT(1) / SUM(COUNT(1)) OVER(PARTITION BY ACTVY_FCT.ACTVY_SKID)) AS ACTL_FIX_MTH_COST,
       MAX(ACTVY_FCT.ACTL_VAR_COST_NUM) *
       (COUNT(1) / SUM(COUNT(1)) OVER(PARTITION BY ACTVY_FCT.ACTVY_SKID)) AS ACTL_VAR_MTH_COST,
       MAX(ACTVY_FCT.CMMT_AMT + ACTVY_FCT.PAID_PYMT_AMT + ACTVY_FCT.PYMT_BOOK_AMT) *
       (COUNT(1) / SUM(COUNT(1)) OVER(PARTITION BY ACTVY_FCT.ACTVY_SKID)) AS PROJ_SPND_AMT
  FROM (SELECT ACTVY_SKID, MTH_SKID FROM OPT_ACTVY_SHPMT_SPLIT_TDADS GROUP BY ACTVY_SKID, MTH_SKID) ACTVY_SHPMT_BRAND_SPLIT,
       OPT_ACTVY_FCT ACTVY_FCT,
       CAL_MASTR_DIM CAL
 WHERE CAL.MTH_SKID = ACTVY_SHPMT_BRAND_SPLIT.MTH_SKID
   AND ACTVY_SHPMT_BRAND_SPLIT.ACTVY_SKID = ACTVY_FCT.ACTVY_SKID
   AND CAL.DAY_DATE BETWEEN TO_DATE(''20000101'',''YYYYMMDD'') AND ADD_MONTHS(SYSDATE, 36)
   AND ACTVY_FCT.ACTVY_SKID <> 0
 GROUP BY ACTVY_FCT.ACTVY_SKID,
          CAL.MTH_SKID';
  dbms_output.put_line('Create table '||v_tbl_name||'...');
  pro_put_line(v_sql);
  execute immediate v_sql;
  dbms_output.put_line('Create table '||v_tbl_name||' Done!');

  --Gather statics
  for idx in 1..V_VAR.COUNT
  loop
    IF V_VAR(idx) <> 'OPT_BRAND_HIER_TDADS' THEN
       pro_put_line('Gather statistics for table ' || V_VAR(idx) || ' (BEGIN)');
       DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                  TABNAME          => V_VAR(idx));
       pro_put_line('Gather statistics for table ' || V_VAR(idx) || ' (END)');
    END IF;
  end loop;

  -- Grant
  for idx in 1..V_VAR.COUNT
  LOOP
    SELECT 'Grant select on '|| V_VAR(idx)||' to '|| DECODE(in_grantee,NULL, user||'_ETL', in_grantee)
      INTO v_sql
      FROM dual;
    dbms_output.put_line(v_sql);
    execute immediate v_sql;
	--SOX IMPLEMENTATION PROJECT STARTED
	V_SQL := 'grant select on ' || V_VAR(idx) || ' to ' || USER ||','||C_TBL_ROLES;
      PRO_PUT_LINE(V_SQL);
      EXECUTE IMMEDIATE V_SQL;
	  --SOX IMPLEMENTATION PROJECT ENDED
    dbms_output.put_line('Done!');
  end loop;

exception
  when others then
    pro_put_line('Error: Unexpected system error - ' || sqlerrm);

END pre_extrt_actvy_spnd;

PROCEDURE PRE_DD_FUND_ACCRL_CHILD(V_REGN IN VARCHAR2 DEFAULT NULL) IS -- SOX Implementation project
-------------------------------------------------------------------------------------
-- Procedure   : PRE_DD_FUND_ACCRL_CHILD                                           --
-- Originator  : Kingham (jinhan.zhong@hp.com)                                     --
-- Date        : 24-Sep-2009                                                       --
-- Purpose     : Pre-process of data dump CHILD_FUND_ACCRUALS                      --
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
-- Change History                                                                  --
-- Date           Programmer            Description                                --
-- ------------   -----------------     ---------------------------------------------
-- 24-Sep-2009    Kingham               Initial Version                            --
-- 17-Nov-2009    Kingham               Tuning the process                         --
-- 18-May-2010    Linyan                Tuning the process                         --
-- 19-Aug-2010    Linyan                Tuning the process                         --
-------------------------------------------------------------------------------------
  lSQLStmt                      VARCHAR2(32767);
--Comment by Linyan on May 18, 2010, R10
--  lOwner                        VARCHAR2(200);
  lCondition                    VARCHAR2(1000) DEFAULT '';
  lItr                          NUMBER DEFAULT 0;
  lMSG                          VARCHAR2(1000);
  lCnt                          NUMBER;
  constSHMPT_NC                 VARCHAR2(200) := 'OPT_SHPMT_NC_TFADS';
  constFUND_GTIN_MTH            VARCHAR2(200) := 'OPT_FUND_GTIN_MTH_TDADS';
  constFUND_SHIPT               VARCHAR2(200) := 'OPT_FUND_SHIPT_TDADS';
  constFUND_SHIPT_GTIN          VARCHAR2(200) := 'OPT_FUND_SHIPT_GTIN_TDADS';
  constFUND_CHILD_ACCRL         VARCHAR2(200) := 'OPT_FUND_CHILD_ACCRL_TFADS';
  constACCRL_FUND_MTH           VARCHAR2(200)  := 'OPT_ACCRL_FUND_MTH_TFADS';
BEGIN

  OPT_AX_TBLSPACE(V_REGN,C_TBLSPACE_NAME,V_INDEX);--SOX Implementation project

  lMSG := 'STEP010: Retriving Business Unit from parameter table ...';
  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE(lMSG);
  FOR bu IN (SELECT  BU.BUS_UNIT_SKID AS PARM_VAL
                FROM OPT_QRY_EXTRN_PARM_PRC PARM,
                     OPT_BUS_UNIT_FDIM BU
               WHERE PARM.PARM_VAL = BU.BUS_UNIT_NAME
                 AND QRY_NAME_CODE = 'CHILD_FUND_ACCRUALS_DLY'
                 AND PARM_NAME_CODE = 'BUS_UNIT_NAME')
  LOOP
      IF (lItr <> 0) THEN
        lCondition := lCondition||','||bu.PARM_VAL;
      ELSE
        lCondition := lCondition||bu.PARM_VAL;
      END if;
      lItr := lItr + 1;
  END LOOP;


  -- Create temporary table OPT_SHPMT_NC_TFADS
  lMSG := 'STEP020: Creating table '||constSHMPT_NC || ' ...';
  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE(lMSG);
  BEGIN
    EXECUTE IMMEDIATE ('DROP TABLE '||constSHMPT_NC||' PURGE');
  EXCEPTION WHEN OTHERS THEN
    IF (SQLCODE = -00942) THEN
      NULL;
    ELSE
      RAISE;
    END IF;
  END;

  lSQLStmt :=            'create table '||constSHMPT_NC                                                 ;
  lSQLStmt := lSQLStmt ||' '||C_FINAL_TBL_STORAGE||' '||C_TBLSPACE_NAME||' '||C_TABLE_DOP               ;
  lSQLStmt := lSQLStmt ||'nologging compress                                                           ';
  lSQLStmt := lSQLStmt ||'as                                                                           ';
  lSQLStmt := lSQLStmt ||'select shpmt_nc.opt_shpmt_skid,                                              ';
  lSQLStmt := lSQLStmt ||'       shpmt_nc.shpmt_date,                                                  ';
  lSQLStmt := lSQLStmt ||'       shpmt_nc.cal_skid,                                                    ';
  lSQLStmt := lSQLStmt ||'       shpmt_nc.shpmt_date_skid,                                             ';
  lSQLStmt := lSQLStmt ||'       shpmt_nc.prod_skid,                                                   ';
  lSQLStmt := lSQLStmt ||'       shpmt_nc.bus_unit_skid,                                               ';
  lSQLStmt := lSQLStmt ||'       shpmt_nc.niv_amt,                                                     ';
  lSQLStmt := lSQLStmt ||'       shpmt_nc.giv_amt,                                                     ';
  lSQLStmt := lSQLStmt ||'       shpmt_nc.vol_su_amt,                                                  ';
  lSQLStmt := lSQLStmt ||'       shpmt_nc.vol_buom_amt,                                                ';
  lSQLStmt := lSQLStmt ||'       acct.acct_skid                                                        ';
  lSQLStmt := lSQLStmt ||'  from (select shpmt.opt_shpmt_skid,                                         ';
  lSQLStmt := lSQLStmt ||'               shpmt.shpmt_date,                                             ';
  lSQLStmt := lSQLStmt ||'               shpmt.cal_skid,                                               ';
  lSQLStmt := lSQLStmt ||'               shpmt.shpmt_date_skid,                                        ';
  lSQLStmt := lSQLStmt ||'               shpmt.prod_skid,                                              ';
  lSQLStmt := lSQLStmt ||'               shpmt.bus_unit_skid,                                          ';
  lSQLStmt := lSQLStmt ||'               shpmt.niv_amt,                                                ';
  lSQLStmt := lSQLStmt ||'               shpmt.giv_amt,                                                ';
  lSQLStmt := lSQLStmt ||'               shpmt.vol_su_amt,                                             ';
  lSQLStmt := lSQLStmt ||'               shpmt.vol_buom_amt,                                           ';
----Changed by Linyan for B022 R11 on Aug 19,2010,begin
  lSQLStmt := lSQLStmt ||'               shpmt.shpmt_to_from_code as shpmt_to_code                     ';
  lSQLStmt := lSQLStmt ||'          from opt_shpmt_fct shpmt                                           ';
  lSQLStmt := lSQLStmt ||'           where shpmt.bus_unit_skid in ('||lCondition||')) shpmt_nc,        ';
  lSQLStmt := lSQLStmt ||'        opt_acct_fdim acct                                                   ';
  lSQLStmt := lSQLStmt ||' where shpmt_nc.shpmt_to_code = acct.mastr_name                              ';
  lSQLStmt := lSQLStmt ||'   and shpmt_nc.bus_unit_skid = acct.bus_unit_skid                           ';
/**  lSQLStmt := lSQLStmt ||'               decode(acct_one_up_hier.non_call_ind,                         ';
  lSQLStmt := lSQLStmt ||'                      ''Y'',                                                 ';
  lSQLStmt := lSQLStmt ||'                      shpmt.shpmt_from_code,                                 ';
  lSQLStmt := lSQLStmt ||'                      shpmt.shpmt_to_code) as shpmt_to_code                  ';
  lSQLStmt := lSQLStmt ||'          from opt_shpmt_fct shpmt,                                          ';
  lSQLStmt := lSQLStmt ||'               (select shipto.acct_skid,                                     ';
  lSQLStmt := lSQLStmt ||'                       shipto.MASTR_NAME,                                    ';
  lSQLStmt := lSQLStmt ||'                       oneup.non_call_ind,                                   ';
  lSQLStmt := lSQLStmt ||'                       shipto.bus_unit_skid                                  ';
  lSQLStmt := lSQLStmt ||'                  from opt_acct_fdim shipto, opt_acct_fdim oneup             ';
  lSQLStmt := lSQLStmt ||'                 where shipto.parnt_acct_id = oneup.acct_id) acct_one_up_hier';
  lSQLStmt := lSQLStmt ||'         where shpmt.shpmt_to_code = acct_one_up_hier.MASTR_NAME             ';
  lSQLStmt := lSQLStmt ||'           and shpmt.bus_unit_skid = acct_one_up_hier.bus_unit_skid          ';
  lSQLStmt := lSQLStmt ||'           and shpmt.bus_unit_skid in ('||lCondition||')) shpmt_nc,          ';
  lSQLStmt := lSQLStmt ||'       opt_acct_fdim acct                                                    ';
  lSQLStmt := lSQLStmt ||' where shpmt_nc.shpmt_to_code = acct.mastr_name                              ';
  lSQLStmt := lSQLStmt ||'   and shpmt_nc.bus_unit_skid = acct.bus_unit_skid                           ';**/
--Changed by Linyan for B022 R11 on Aug 19,2010,end

  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE(lSQLStmt);
  EXECUTE IMMEDIATE lSQLStmt;
  lCnt := SQL%ROWCOUNT;
  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE('Creat table '||constSHMPT_NC ||' with '||lCnt||' rows sucessfully');
  DBMS_OUTPUT.NEW_LINE;

  -- Gather statistics for OPT_SHPMT_NC_TFADS
  --Comment by Linyan on May 18, 2010, R10
  --SELECT owner INTO lOwner FROM dba_tables where table_name = constSHMPT_NC and rownum=1;
  BEGIN
    DBMS_STATS.GATHER_TABLE_STATS(ownname          => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                  tabname          => constSHMPT_NC,
                                  estimate_percent => 0.1,
                                  method_opt       => 'FOR ALL COLUMNS SIZE AUTO',
                                  granularity      => 'GLOBAL',
                                  cascade          => TRUE
                                );
  END;

  -- Create temporary table OPT_FUND_GTIN_MTH_TDADS
  lMSG := 'STEP030: Creating table '||constFUND_GTIN_MTH || ' ...';
  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE(lMSG);
  BEGIN
    EXECUTE IMMEDIATE ('DROP TABLE '||constFUND_GTIN_MTH||' PURGE');
  EXCEPTION WHEN OTHERS THEN
    IF (SQLCODE = -00942) THEN
      NULL;
    ELSE
      RAISE;
    END IF;
  END;
  -- modify by Eric for CR3302 R10 4/18/2010. replace opt_accrl_fct with opt_fund_accrl_fct
  -- modified by Linyan, added FISC_YR_ABBR_NAME into table, R10
  lSQLStmt :=            'create table '||constFUND_GTIN_MTH                                         ;
  lSQLStmt := lSQLStmt ||' '||C_FINAL_TBL_STORAGE||' '||C_TBLSPACE_NAME||' '||C_TABLE_DOP            ;
  lSQLStmt := lSQLStmt ||'nologging compress                                                        ';
  lSQLStmt := lSQLStmt ||'as                                                                        ';
  lSQLStmt := lSQLStmt ||'select fund_skid, SHPMT_TYPE_CODE, prod_skid, accrl.mth_skid, cal.mth_name, cal.FISC_YR_ABBR_NAME';
  lSQLStmt := lSQLStmt ||'  from opt_fund_accrl_fct accrl,                                               ';
  lSQLStmt := lSQLStmt ||'       cal_mastr_dim cal                                                  ';
  lSQLStmt := lSQLStmt ||' where accrl.mth_skid = cal.cal_mastr_skid                                ';
  lSQLStmt := lSQLStmt ||'   and accrl.bus_unit_skid in ('||lCondition||')                          ';

  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE(lSQLStmt);
  EXECUTE IMMEDIATE lSQLStmt;
  lCnt := SQL%ROWCOUNT;
  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE('Creat table '||constFUND_GTIN_MTH ||' with '||lCnt||' rows sucessfully');
  DBMS_OUTPUT.NEW_LINE;

  -- Gather statistics for OPT_FUND_GTIN_MTH_TDADS
  --comment by Linyan on Mary 18,2010, R10
  --SELECT owner INTO lOwner FROM dba_tables where table_name = constFUND_GTIN_MTH and rownum=1;
  BEGIN
    DBMS_STATS.GATHER_TABLE_STATS(ownname          => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                  tabname          => constFUND_GTIN_MTH,
                                  estimate_percent => 1,
                                  method_opt       => 'FOR ALL COLUMNS SIZE AUTO',
                                  granularity      => 'GLOBAL',
                                  cascade          => TRUE
                                );
  END;



  -- Create temporary table OPT_FUND_SHIPT_TDADS
  lMSG := 'STEP040: Creating table '||constFUND_SHIPT || ' ...';
  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE(lMSG);
  BEGIN
    EXECUTE IMMEDIATE ('DROP TABLE '||constFUND_SHIPT||' PURGE');
  EXCEPTION WHEN OTHERS THEN
    IF (SQLCODE = -00942) THEN
      NULL;
    ELSE
      RAISE;
    END IF;
  END;

  lSQLStmt :=            'create table '||constFUND_SHIPT                                                         ;
  lSQLStmt := lSQLStmt ||' '||C_FINAL_TBL_STORAGE||' '||C_TBLSPACE_NAME||' '||C_TABLE_DOP                         ;
  lSQLStmt := lSQLStmt ||'nologging compress                                                                      ';
  lSQLStmt := lSQLStmt ||'as                                                                                      ';
  lSQLStmt := lSQLStmt ||'SELECT fund.FUND_ID AS FUND_ID,                                                         ';
  lSQLStmt := lSQLStmt ||'     fund.fund_skid AS fund_skid,                                                       ';
  lSQLStmt := lSQLStmt ||'     fund.FUND_NAME AS FUND_NAME,                                                       ';
  lSQLStmt := lSQLStmt ||'     FA_HIER.FUND_ACCT_ORIG_NAME AS FUND_ACCT_ID,                                       ';
  lSQLStmt := lSQLStmt ||'     FA_HIER.FUND_ACCT_NAME AS FUND_ACCT_NAME,                                          ';
  lSQLStmt := lSQLStmt ||'     FA_HIER.ACCT_ORIG_NAME AS CHILD_ACCT_ID,                                           ';
  lSQLStmt := lSQLStmt ||'     FA_HIER.ACCT_NAME AS CHILD_ACCT_NAME,                                              ';
  lSQLStmt := lSQLStmt ||'     fund.FUND_CLASS_CODE AS FUND_CLASS,                                                ';
  lSQLStmt := lSQLStmt ||'     rate.fund_rate AS FUND_RATE,                                                       ';
  lSQLStmt := lSQLStmt ||'     FA_HIER.SHIPTO,                                                                    ';
  lSQLStmt := lSQLStmt ||'     FA_HIER.ACCT_SKID AS CHILD_SKID,                                                   ';
  lSQLStmt := lSQLStmt ||'     FA_HIER.SHIPT_START_DATE_SKID,                                                     ';
  lSQLStmt := lSQLStmt ||'     FA_HIER.SHIPT_END_DATE_SKID,                                                       ';
  lSQLStmt := lSQLStmt ||'     BU.BUS_UNIT_NAME AS BUS_UNIT_NAME                                                  ';
  lSQLStmt := lSQLStmt ||'FROM OPT_BUS_UNIT_FDIM BU,                                                              ';
  lSQLStmt := lSQLStmt ||'     opt_fund_fdim fund,                                                                ';
  lSQLStmt := lSQLStmt ||'     (SELECT AVG(bdf_fund_rate_num) AS fund_rate,fund_grp_skid from opt_fund_prod_dim fp';
  lSQLStmt := lSQLStmt ||'        WHERE fp.FUND_TRGT_FLG = ''F''                                                  ';
  lSQLStmt := lSQLStmt ||'      GROUP BY fund_grp_skid)rate,                                                      ';
  lSQLStmt := lSQLStmt ||'      (SELECT  FA.FUND_ACCT_ORIG_NAME,                                                  ';
  lSQLStmt := lSQLStmt ||'              FA.FUND_ACCT_NAME,                                                        ';
  lSQLStmt := lSQLStmt ||'              FA.ACCT_ORIG_NAME,                                                        ';
  lSQLStmt := lSQLStmt ||'              FA.ACCT_NAME,                                                             ';
  lSQLStmt := lSQLStmt ||'              FA.FUND_ACCT_SKID,                                                        ';
  lSQLStmt := lSQLStmt ||'              FA.ACCT_SKID,                                                             ';
  lSQLStmt := lSQLStmt ||'              FA.SHIPT_START_DATE_SKID,                                                 ';
  lSQLStmt := lSQLStmt ||'              FA.SHIPT_END_DATE_SKID,                                                   ';
  lSQLStmt := lSQLStmt ||'              AA.ACCT_SKID SHIPTO,                                                      ';
  lSQLStmt := lSQLStmt ||'              AA.BUS_UNIT_SKID AS BUS_UNIT_SKID                                         ';
  lSQLStmt := lSQLStmt ||'         FROM OPT_ACCT_XM_ESI         FA,                                               ';
  lSQLStmt := lSQLStmt ||'              OPT_ACCT_ASDN_TYPE2_DIM AA                                                ';
  lSQLStmt := lSQLStmt ||'        WHERE FA.ACCT_SKID = AA.ASSOC_ACCT_SKID                                         ';
  lSQLStmt := lSQLStmt ||'          AND FA.BUS_UNIT_SKID = AA.BUS_UNIT_SKID                                       ';
  lSQLStmt := lSQLStmt ||'          AND FA.FUND_ACCT <> FA.ACCT_ID                                                ';
  lSQLStmt := lSQLStmt ||'          AND FA.FUND_ACCT IS NOT NULL                                                  ';
  lSQLStmt := lSQLStmt ||'          AND FA.ACCT_END_DATE = TO_DATE(''9999-12-31'', ''YYYY-MM-DD'')                ';
  lSQLStmt := lSQLStmt ||'          AND AA.CURR_IND = ''Y'') FA_HIER                                              ';
  lSQLStmt := lSQLStmt ||'WHERE FA_HIER.FUND_ACCT_SKID = fund.acct_SKID                                           ';
  lSQLStmt := lSQLStmt ||'  AND BU.BUS_UNIT_SKID = FA_HIER.BUS_UNIT_SKID                                          ';
  lSQLStmt := lSQLStmt ||'  AND fund.fund_grp_skid = rate.fund_grp_skid                                           ';
  lSQLStmt := lSQLStmt ||'  AND fund.fund_id <> ''0''                                                             ';
  lSQLStmt := lSQLStmt ||'  AND BU.BUS_UNIT_SKID in ('||lCondition||')                                            ';

  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE(lSQLStmt);
  EXECUTE IMMEDIATE lSQLStmt;
  lCnt := SQL%ROWCOUNT;
  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE('Creat table '||constFUND_SHIPT ||' with '||lCnt||' rows sucessfully');
  DBMS_OUTPUT.NEW_LINE;

  -- Gather statistics for OPT_FUND_SHIPT_TDADS
  -- Comment by Linyan on May 18, 2010, R10
  --SELECT owner INTO lOwner FROM dba_tables where table_name = constFUND_SHIPT and rownum=1;
  BEGIN
    DBMS_STATS.GATHER_TABLE_STATS(ownname          => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                  tabname          => constFUND_SHIPT,
                                  estimate_percent => 0.1,
                                  method_opt       => 'FOR ALL COLUMNS SIZE AUTO',
                                  granularity      => 'GLOBAL',
                                  cascade          => TRUE
                                );
  END;


  -- Create temporary table OPT_FUND_SHIPT_GTIN_TDADS
  lMSG := 'STEP050: Creating table '||constFUND_SHIPT_GTIN || ' ...';
  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE(lMSG);
  BEGIN
    EXECUTE IMMEDIATE ('DROP TABLE '||constFUND_SHIPT_GTIN||' PURGE');
  EXCEPTION WHEN OTHERS THEN
    IF (SQLCODE = -00942) THEN
      NULL;
    ELSE
      RAISE;
    END IF;
  END;
 --modified by Linyan to add FISC_YR_ABBR_NAME to table OPT_FUND_SHIPT_GTIN_TDADS, R10
  lSQLStmt :=            'create table '||constFUND_SHIPT_GTIN                               ;
  lSQLStmt := lSQLStmt ||' '||C_FINAL_TBL_STORAGE||' '||C_TBLSPACE_NAME||' '||C_TABLE_DOP    ;
  lSQLStmt := lSQLStmt ||'nologging compress                                                ';
  lSQLStmt := lSQLStmt ||'as                                                                ';
  lSQLStmt := lSQLStmt ||'select fund.FUND_ID,                                           ';
  lSQLStmt := lSQLStmt ||'       fund.fund_skid,                                         ';
  lSQLStmt := lSQLStmt ||'       fund.FUND_NAME,                                         ';
  lSQLStmt := lSQLStmt ||'       fund.FUND_ACCT_ID,                                      ';
  lSQLStmt := lSQLStmt ||'       fund.FUND_ACCT_NAME,                                    ';
  lSQLStmt := lSQLStmt ||'       fund.CHILD_ACCT_ID,                                     ';
  lSQLStmt := lSQLStmt ||'       fund.CHILD_ACCT_NAME,                                   ';
  lSQLStmt := lSQLStmt ||'       fund.FUND_CLASS,                                        ';
  lSQLStmt := lSQLStmt ||'       fund.fund_rate AS FUND_RATE,                            ';
  lSQLStmt := lSQLStmt ||'       fund.SHIPTO,                                            ';
  lSQLStmt := lSQLStmt ||'       fund.CHILD_SKID,                                        ';
  lSQLStmt := lSQLStmt ||'       fund.SHIPT_START_DATE_SKID,                             ';
  lSQLStmt := lSQLStmt ||'       fund.SHIPT_END_DATE_SKID,                               ';
  lSQLStmt := lSQLStmt ||'       fund.BUS_UNIT_NAME,                                     ';
  lSQLStmt := lSQLStmt ||'       accrl.SHPMT_TYPE_CODE,                                  ';
  lSQLStmt := lSQLStmt ||'       accrl.prod_skid,                                        ';
  lSQLStmt := lSQLStmt ||'       accrl.mth_skid,                                         ';
  lSQLStmt := lSQLStmt ||'       accrl.mth_name,                                         ';
  lSQLStmt := lSQLStmt ||'       accrl.FISC_YR_ABBR_NAME                                 ';
  lSQLStmt := lSQLStmt ||'  from OPT_FUND_SHIPT_TDADS fund, OPT_FUND_GTIN_MTH_TDADS accrl';
  lSQLStmt := lSQLStmt ||' where fund.fund_skid = accrl.fund_skid                        ';

  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE(lSQLStmt);
  EXECUTE IMMEDIATE lSQLStmt;
  lCnt := SQL%ROWCOUNT;
  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE('Creat table '||constFUND_SHIPT_GTIN ||' with '||lCnt||' rows sucessfully');
  DBMS_OUTPUT.NEW_LINE;

  -- Gather statistics for OPT_FUND_SHIPT_GTIN_TDADS
  -- Comment by Linyan on May 18, 2010, R10
  --SELECT owner INTO lOwner FROM dba_tables where table_name = constFUND_SHIPT_GTIN and rownum=1;
  BEGIN
    DBMS_STATS.GATHER_TABLE_STATS(ownname          => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                  tabname          => constFUND_SHIPT_GTIN,
                                  estimate_percent => 0.1,
                                  method_opt       => 'FOR ALL COLUMNS SIZE AUTO',
                                  granularity      => 'GLOBAL',
                                  cascade          => TRUE
                                );
  END;


  -- Create temporary table OPT_FUND_CHILD_ACCRL_TFADS
  lMSG := 'STEP060: Creating table '||constFUND_CHILD_ACCRL || ' ...';
  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE(lMSG);
  BEGIN
    EXECUTE IMMEDIATE ('DROP TABLE '||constFUND_CHILD_ACCRL||' PURGE');
  EXCEPTION WHEN OTHERS THEN
    IF (SQLCODE = -00942) THEN
      NULL;
    ELSE
      RAISE;
    END IF;
  END;
  -- Modified by Linyan to add FISC_YR_ABBR_NAME to table OPT_FUND_CHILD_ACCRL_TFADS, R10
  lSQLStmt :=            'create table '||constFUND_CHILD_ACCRL                                       ;
  lSQLStmt := lSQLStmt ||' '||C_FINAL_TBL_STORAGE||' '||C_TBLSPACE_NAME||' '||C_TABLE_DOP                   ;
  lSQLStmt := lSQLStmt ||'nologging compress                                                               ';
  lSQLStmt := lSQLStmt ||'as                                                                               ';
  lSQLStmt := lSQLStmt ||'SELECT fund.FUND_ID AS FUND_ID,                                                  ';
  lSQLStmt := lSQLStmt ||'       fund.fund_skid AS fund_skid,                                              ';
  lSQLStmt := lSQLStmt ||'       fund.FUND_NAME AS FUND_NAME,                                              ';
  lSQLStmt := lSQLStmt ||'       fund.FUND_ACCT_ID AS FUND_ACCT_ID,                                        ';
  lSQLStmt := lSQLStmt ||'       fund.FUND_ACCT_NAME AS FUND_ACCT_NAME,                                    ';
  lSQLStmt := lSQLStmt ||'       fund.CHILD_ACCT_ID AS CHILD_ACCT_ID,                                      ';
  lSQLStmt := lSQLStmt ||'       fund.CHILD_ACCT_NAME AS CHILD_ACCT_NAME,                                  ';
  lSQLStmt := lSQLStmt ||'       fund.SHPMT_TYPE_CODE AS SHPMT_BASE,                                       ';
  lSQLStmt := lSQLStmt ||'       fund.FUND_CLASS AS FUND_CLASS,                                            ';
  lSQLStmt := lSQLStmt ||'       fund.fund_rate AS FUND_RATE,                                              ';
  lSQLStmt := lSQLStmt ||'       fund.MTH_NAME AS PERIOD,                                                  ';
  lSQLStmt := lSQLStmt ||'       fund.mth_skid AS mth_skid,                                                ';
  lSQLStmt := lSQLStmt ||'       SHPMT.VOL_BUOM_AMT AS BUOM,                                               ';
  lSQLStmt := lSQLStmt ||'       SHPMT.GIV_AMT AS GIV,                                                     ';
  lSQLStmt := lSQLStmt ||'       SHPMT.NIV_AMT AS NIV,                                                     ';
  lSQLStmt := lSQLStmt ||'       fund.BUS_UNIT_NAME AS BUS_UNIT_NAME,                                      ';
  lSQLStmt := lSQLStmt ||'       shpmt.bus_unit_skid AS bus_unit_skid,                                     ';
  lSQLStmt := lSQLStmt ||'       fund.FISC_YR_ABBR_NAME AS FISC_YR_ABBR_NAME,                              ';
  lSQLStmt := lSQLStmt ||'       COUNT(opt_shpmt_skid) over (PARTITION BY fund.fund_skid,                  ';
  lSQLStmt := lSQLStmt ||'                           fund.mth_skid) AS fund_weight,                        ';
  lSQLStmt := lSQLStmt ||'       COUNT(opt_shpmt_skid) over (PARTITION BY fund.fund_skid, fund.CHILD_SKID, ';
  lSQLStmt := lSQLStmt ||'                           fund.mth_skid) AS child_acct_weight                   ';
  lSQLStmt := lSQLStmt ||'  FROM opt_shpmt_nc_tfads SHPMT,                                                 ';
  lSQLStmt := lSQLStmt ||'       OPT_FUND_SHIPT_GTIN_TDADS fund                                            ';
  lSQLStmt := lSQLStmt ||' WHERE fund.shipto = SHPMT.ACCT_SKID                                             ';
  lSQLStmt := lSQLStmt ||'   AND SHPMT.SHPMT_DATE_SKID BETWEEN fund.SHIPT_START_DATE_SKID AND              ';
  lSQLStmt := lSQLStmt ||'       fund.SHIPT_END_DATE_SKID                                                  ';
  lSQLStmt := lSQLStmt ||'   AND fund.prod_skid = shpmt.prod_skid                                          ';
  lSQLStmt := lSQLStmt ||'   AND fund.mth_skid = shpmt.cal_skid                                            ';

  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE(lSQLStmt);
  EXECUTE IMMEDIATE lSQLStmt;
  lCnt := SQL%ROWCOUNT;
  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE('Creat table '||constFUND_CHILD_ACCRL ||' with '||lCnt||' rows sucessfully');
  DBMS_OUTPUT.NEW_LINE;
  -- Gather statistics for OPT_FUND_CHILD_ACCRL_TFADS
  -- Comment by Linyan on May 18, 2010, R10
  --SELECT owner INTO lOwner FROM dba_tables where table_name = constFUND_CHILD_ACCRL and rownum=1;
  BEGIN
    DBMS_STATS.GATHER_TABLE_STATS(ownname          => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                  tabname          => constFUND_CHILD_ACCRL,
                                  estimate_percent => 0.1,
                                  method_opt       => 'FOR ALL COLUMNS SIZE AUTO',
                                  granularity      => 'GLOBAL',
                                  cascade          => TRUE
                                );
  END;


  -- Create temporary table OPT_ACCRL_FUND_MTH_TFADS
  lMSG := 'STEP070: Creating table '||constACCRL_FUND_MTH || ' ...';
  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE(lMSG);
  BEGIN
    EXECUTE IMMEDIATE ('DROP TABLE '||constACCRL_FUND_MTH||' PURGE');
  EXCEPTION WHEN OTHERS THEN
    IF (SQLCODE = -00942) THEN
      NULL;
    ELSE
      RAISE;
    END IF;
  END;
  -- modify by Eric for CR3302 R10 4/18/2010. replace opt_accrl_fct with opt_fund_accrl_fct

  lSQLStmt :=            'create table '||constACCRL_FUND_MTH                                         ;
  lSQLStmt := lSQLStmt ||' '||C_FINAL_TBL_STORAGE||' '||C_TBLSPACE_NAME||' '||C_TABLE_DOP              ;
  lSQLStmt := lSQLStmt ||'nologging compress                                                          ';
  lSQLStmt := lSQLStmt ||'as                                                                          ';
  lSQLStmt := lSQLStmt ||'select fund_skid, mth_skid, sum(accrl_fund_amt) as accrl_amt, bus_unit_skid ';
  lSQLStmt := lSQLStmt ||'          from opt_fund_accrl_fct                                                ';
  lSQLStmt := lSQLStmt ||'         where bus_unit_skid in ('||lCondition||')                          ';
  lSQLStmt := lSQLStmt ||'         group by fund_skid, mth_skid, bus_unit_skid                       ';


  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE(lSQLStmt);
  EXECUTE IMMEDIATE lSQLStmt;
  lCnt := SQL%ROWCOUNT;
  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE('Creat table '||constACCRL_FUND_MTH ||' with '||lCnt||' rows sucessfully');
  DBMS_OUTPUT.NEW_LINE;
  -- Gather statistics for OPT_ACCRL_FUND_MTH_TFADS
  -- Comment by Linyan on May 18, 2010, R10
  -- SELECT owner INTO lOwner FROM dba_tables where table_name = constACCRL_FUND_MTH and rownum=1;
  BEGIN
    DBMS_STATS.GATHER_TABLE_STATS(ownname          => SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA'),
                                  tabname          => constACCRL_FUND_MTH,
                                  estimate_percent => 0.1,
                                  method_opt       => 'FOR ALL COLUMNS SIZE AUTO',
                                  granularity      => 'GLOBAL',
                                  cascade          => TRUE
                                );
  END;


  -- Grant privileges
  lMSG := 'STEP080: Granting privileges ...';
  DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
  DBMS_OUTPUT.PUT_LINE(lMSG);

  FOR role_name IN (SELECT role
                       FROM dba_roles
                      WHERE role LIKE '%OPTIMA%')
  LOOP
    lSQLStmt := 'GRANT SELECT ON ' || constFUND_CHILD_ACCRL || ' TO ' || role_name.role;
    DBMS_OUTPUT.PUT_LINE(lSQLStmt);
    EXECUTE IMMEDIATE lSQLStmt;
    lSQLStmt := 'GRANT SELECT ON ' || constACCRL_FUND_MTH || ' TO ' || role_name.role;
    DBMS_OUTPUT.PUT_LINE(lSQLStmt);
    EXECUTE IMMEDIATE lSQLStmt;
  END LOOP;
  
  


EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      RAISE_APPLICATION_ERROR(-20005,
                              ' Oracle Errors occured while '||lMSG||CHR(10)||
                              ' The Error Cdoe is    : '||SQLCODE||CHR(10)||
                              ' The Error Message is : '||SQLERRM);
END PRE_DD_FUND_ACCRL_CHILD;


END OPT_PRE_PRCSS; 
/

