CREATE OR REPLACE PACKAGE ADWU_OPTIMA_LAEX.Opt_Shpmt_Rpt_Pkg IS
  ------------------------------------------------------------------------------------
  -- Package      : OPT_SHPMT_RPT_PKG                                               --
  -- Originator   : Luke Lu (fei.lu@hp.com)                                         --
  -- Author       : Luke Lu (fei.lu@hp.com)                                         --
  -- Date         : 30-Sep-2007                                                     --
  -- Purpose      : Loading the SHPMT_RPT data                                      --
  ------------------------------------------------------------------------------------
  -- Change History                                                                 --
  -- Date        Programmer        Description                                      --
  -- ----------- ----------------- ------------------------------------------------ --
  -- 30-Sep-2007 Luke Lu           Initial Varsion                                  --
  -- 06-Nov-2008 David Lang        Add a new input parameter INTYPE to              --
  --                               OPT_SHPMT_RPT_AGG and OPT_SHPMT_EXCPT_FCT_LOAD   --
  --                               for performance issue                            --
  -- 11-Aug-2009 Bodhi Wang        Add shipment restatement process, and add column --
  --                               SHPMT_GEO_CODE for OPT_SHPMT_RPT_AGG and         --
  --                               OPT_SHPMT_EXCPT_FCT_LOAD                         --
  -- 29-Aug-2009 Bodhi Wang        Use partition exchange to load OPT_SHPMT_RPT_BFCT--
  -- 10-Sep-2009 Bodhi Wang        Modify the process of OPT_SHPMT_RPT_AGG to       --
  --                               cleanup old partitions.                          --
  -- 28-Sep-2009 Bodhi Wang        Skip loading process if there's no data in OPT_SHPMT_SFCT
  ------------------------------------------------------------------------------------

V_TBLSPACE_NAME     VARCHAR2(50);
V_INDEX             VARCHAR2(50);

  -- procedures, functions
  -- Purpose : Populate data into OPT_SHPMT_RPT_BFCT.
  PROCEDURE OPT_SHPMT_RPT_AGG(INREGNCODE IN VARCHAR2,
                              INTYPE VARCHAR2,
                              IN_CTRLM_ORD_ID IN VARCHAR2,
                              IN_RSTMT_IND IN VARCHAR2 DEFAULT 'N'
                             );

  -- Purpose : loading OPT_ACCT_HIER_FDIM table process
  PROCEDURE OPT_ACCT_HIER_BUILD(IN_REGN VARCHAR2,V_I_REGN IN VARCHAR2 DEFAULT NULL);--SOX IMPLEMENTATION PROJECT

  -- Purpose : Populate OPT_SHPMT_EXCPT_RPT_FCT data from SFCT table
  PROCEDURE OPT_SHPMT_EXCPT_FCT_LOAD(INREGNCODE VARCHAR2,INTYPE VARCHAR2,
                                     IN_RSTMT_IND IN VARCHAR2 DEFAULT 'N');

  -- Purpose : Refresh Optima Shipment Report Related Materialized Views
  PROCEDURE OPT_REFRESH_MV(IN_MV_NAME VARCHAR2,V_REGN IN VARCHAR2 DEFAULT NULL); -- SOX implementation project)
  PROCEDURE OPT_REFRES_MV(p_mv_name IN VARCHAR2);

END Opt_Shpmt_Rpt_Pkg;
/

