CREATE OR REPLACE PACKAGE BODY ADWU_OPTIMA_LAEX.OPT_SHPMT_LOAD_PKG IS

  PROCEDURE PRO_PUT_LINE(P_STRING IN VARCHAR2) IS
    V_STRING_LENGTH NUMBER;
    V_STRING_OFFSET NUMBER := 1;
    V_CUT_POS       NUMBER;
    V_ADD           NUMBER;
  BEGIN
    DBMS_OUTPUT.NEW_LINE;
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
    V_STRING_LENGTH := LENGTH(P_STRING);

    -- loop thru the string by 255 characters and output each chunk
    WHILE V_STRING_OFFSET < V_STRING_LENGTH LOOP
      V_CUT_POS := NVL(INSTR(P_STRING, CHR(10), V_STRING_OFFSET), 0) -
                   V_STRING_OFFSET;
      IF V_CUT_POS < 0 OR V_CUT_POS >= 255 THEN
        V_CUT_POS := 255;
        V_ADD     := 0;
      ELSE
        V_ADD := 1;
      END IF;
      DBMS_OUTPUT.PUT_LINE(SUBSTR(P_STRING, V_STRING_OFFSET, V_CUT_POS));
      V_STRING_OFFSET := V_STRING_OFFSET + V_CUT_POS + V_ADD;
    END LOOP;
  END;

  /*
    *********************************************************************
    * name: FN_FETCH_PAST_MTH_LIMIT
    * purpose  : Fetch past month number limit.
    * Parameter: Table Name
    * Return   : P_PAST_MTH_LIMIT -- past month mumber limit (out)
    * Change History
    * Date         Programmer         Description
    * -------------------------------------------------------------------------
    * 2009-09-10   Bodhi Wang         Initial Version
    ****************************************************************************
    */
  FUNCTION FN_FETCH_PAST_MTH_LIMIT(P_TBL_NAME IN VARCHAR2) return number IS
    V_PAST_MTH_NUM_LIMIT NUMBER;
    V_TBL_NAME           prttn_tbl_detl_prc.TBL_NAME%TYPE := P_TBL_NAME;
  BEGIN
    SELECT CAL_RETN_PERD - 1
      INTO V_PAST_MTH_NUM_LIMIT
      FROM prttn_tbl_detl_prc
     where TBL_NAME = V_TBL_NAME;

    return NVL(V_PAST_MTH_NUM_LIMIT, C_DEFAULT_PAST_MTH_LIMIT); -- Three years if not set.
  END FN_FETCH_PAST_MTH_LIMIT;

  PROCEDURE PRO_TRUNCATE_TABLE(TB_NAME VARCHAR2) IS
    V_COUNT NUMBER(1);
  BEGIN

    SELECT COUNT(1)
      INTO V_COUNT
      FROM USER_TABLES
     WHERE TABLE_NAME = TB_NAME;
    IF (V_COUNT > 0) THEN
      EXECUTE IMMEDIATE 'TRUNCATE TABLE ' || TB_NAME;
    END IF;

  END PRO_TRUNCATE_TABLE;

  /*
    *********************************************************************
    * name: PRO_TRNCT_PTTN
    * purpose: Truncate speceified partitions or subpartitions.
    * parameters: P_TBL_NAME     -- table
    *             P_RSTMT_IND    -- shipment restatement indicator
    *             P_PAST_MTH_NUM -- past month number
    * Change History
    * Date         Programmer         Description
    * -------------------------------------------------------------------------
    * 2009-08-31   Bodhi Wang         Initial Version
    ****************************************************************************
    */
  PROCEDURE PRO_TRNCT_PTTN (P_TBL_NAME VARCHAR2,
                            P_RSTMT_IND IN VARCHAR2 DEFAULT 'N',
                            P_PAST_MTH_NUM IN NUMBER DEFAULT 6) IS

    V_TBL_NAME                USER_TABLES.TABLE_NAME%TYPE := UPPER(P_TBL_NAME);
    V_PAST_MTH_NUM            NUMBER;
    V_PAST_MTH_LIMIT          NUMBER;
    V_SQL                     VARCHAR2(2024);
    V_VC2_TBL_SUB_PRTTN_STR   VARCHAR2(4000) := NULL;
    V_NUM_TBL_SUB_PRTTN_COUNT NUMBER;

  BEGIN

    V_PAST_MTH_LIMIT := FN_FETCH_PAST_MTH_LIMIT(V_TBL_NAME);

    IF ABS(P_PAST_MTH_NUM) > V_PAST_MTH_LIMIT THEN
       V_PAST_MTH_NUM := V_PAST_MTH_LIMIT;
    ELSE
       V_PAST_MTH_NUM := ABS(P_PAST_MTH_NUM);
    END IF;

    PRO_PUT_LINE('Truncate subpartitions of table ' || V_TBL_NAME ||' Begin:>>>');

    -- Check if subpartitioned
    OPT_PKG_PRTTN_UTIL.PRO_CHECK_TBL_SUB_PRTTN(V_TBL_NAME,
                                               V_VC2_TBL_SUB_PRTTN_STR,
                                               V_NUM_TBL_SUB_PRTTN_COUNT);

    IF (V_NUM_TBL_SUB_PRTTN_COUNT <> 0) THEN
       IF P_RSTMT_IND = 'Y' THEN
          -- Truncate speceified subpartitions.
          FOR I IN (
                  SELECT DISTINCT
                         'P'||C.MTH_NUM||'_'|| BU.BUS_UNIT_SKID AS SUBPARTITION_NAME
                    FROM OPT_SHPMT_RSTMT_PLC SR,
                         OPT_BUS_UNIT_FDIM BU,
                         OPT_CAL_MASTR_DIM C
                   WHERE SR.RSTMT_IND='Y'
                     and SR.GEO_CODE<>'HEADER'
                     and SR.GEO_CODE=BU.SHPMT_GEO_CODE
                     and C.CAL_MASTR_SKID >=SR.RSTMT_START_DAY_DATE_SKID
                     and C.CAL_MASTR_SKID <=SR.RSTMT_END_DAY_DATE_SKID
               INTERSECT
                  SELECT SUBPARTITION_NAME
                    FROM USER_TAB_SUBPARTITIONS
                   WHERE TABLE_NAME = V_TBL_NAME
                    )
          LOOP
               V_SQL := 'Alter table ' || V_TBL_NAME ||
                        ' TRUNCATE SUBPARTITION '||I.SUBPARTITION_NAME||
                        ' REUSE STORAGE';
               PRO_PUT_LINE('SQL:' || V_SQL);
               EXECUTE IMMEDIATE V_SQL;
               PRO_PUT_LINE('Subpartition ' || I.SUBPARTITION_NAME ||
                            ' of table '||V_TBL_NAME||' was truncated!');
           END LOOP;
       ELSE
          -- Normal loading, truncate subpartitions.
          FOR I IN (
                  SELECT DISTINCT
                         'P'||C.MTH_NUM||'_'|| BU.BUS_UNIT_SKID AS SUBPARTITION_NAME
                    FROM OPT_BUS_UNIT_FDIM BU,
                         (
                          SELECT DISTINCT MTH_NUM
                            FROM OPT_CAL_MASTR_DIM
                           WHERE day_date >= trunc(
                                 add_months(sysdate,-1*V_PAST_MTH_NUM))
                         ) C
                   WHERE BU.BUS_UNIT_SKID <> 0
               INTERSECT
                  SELECT SUBPARTITION_NAME
                    FROM USER_TAB_SUBPARTITIONS
                   WHERE TABLE_NAME = V_TBL_NAME
                    )
          LOOP
               V_SQL := 'Alter table ' || V_TBL_NAME ||
                        ' TRUNCATE SUBPARTITION '||I.SUBPARTITION_NAME||
                        ' REUSE STORAGE';
               PRO_PUT_LINE('SQL:' || V_SQL);
               EXECUTE IMMEDIATE V_SQL;
               PRO_PUT_LINE('Subpartition ' ||I.SUBPARTITION_NAME||
                            ' of table '||V_TBL_NAME||' was truncated!');
          END LOOP;
       END IF; --Process TYPE
    END IF; --partition TYPE

    PRO_PUT_LINE('Truncate subpartitions of table ' || V_TBL_NAME ||' END:Sucess<<<');
  END PRO_TRNCT_PTTN;


    PROCEDURE PRO_DROP_TABLE(TB_NAME VARCHAR2) IS
    V_COUNT NUMBER(1);
  BEGIN

    SELECT COUNT(1)
      INTO V_COUNT
      FROM USER_TABLES
     WHERE TABLE_NAME = TB_NAME;
    IF (V_COUNT > 0) THEN
      EXECUTE IMMEDIATE 'DROP TABLE ' || TB_NAME;
    END IF;

  END PRO_DROP_TABLE;

  PROCEDURE PRO_DROP_PRTTN(P_TBL_NAME IN USER_TABLES.TABLE_NAME%TYPE,
                           P_DROP_MAX_IND IN VARCHAR2 DEFAULT 'Y') IS
    /**********************************************************************
    * name: PRO_DROP_PRTTN
    * purpose: Drop partitions and subpartitions for target table
    * parameters: P_TBL_NAME -- Target table
    * author: David Lang
    * version: 1.00 - initial version
    **********************************************************************/
    V_SQLSTMT  VARCHAR2(500);
    V_TBL_NAME USER_TABLES.TABLE_NAME%TYPE := UPPER(P_TBL_NAME);
    V_FLAG     NUMBER(1);

    -- Get all the partitions
    CURSOR PRTTN_CUR_TYP IS
      SELECT PARTITION_NAME
        FROM USER_TAB_PARTITIONS
       WHERE TABLE_NAME = V_TBL_NAME
         AND PARTITION_POSITION !=
             (SELECT MAX(PARTITION_POSITION)
                FROM USER_TAB_PARTITIONS
               WHERE TABLE_NAME = V_TBL_NAME);

    -- Get all subpartitions
    CURSOR SUBPRTTN_CUR_TYP IS
      SELECT SUBPARTITION_NAME
        FROM USER_TAB_SUBPARTITIONS
       WHERE TABLE_NAME = V_TBL_NAME
         AND SUBPARTITION_POSITION !=
             (SELECT MAX(SUBPARTITION_POSITION)
                FROM USER_TAB_SUBPARTITIONS
               WHERE TABLE_NAME = V_TBL_NAME);

  BEGIN
    PRO_CHECK_TBL(P_TBL_NAME, V_FLAG);
    -- Table is partitioned
    IF (V_FLAG = 0) THEN
      --First drop all old partitions except PMAX if they exist
      PRO_PUT_LINE('Preparing to drop partitions from ' || V_TBL_NAME);
      FOR PRTTN_REC_TYP IN PRTTN_CUR_TYP LOOP
        V_SQLSTMT := 'ALTER TABLE ' || V_TBL_NAME || ' ' || CHR(10) ||
                     'DROP PARTITION ' || PRTTN_REC_TYP.PARTITION_NAME;

        PRO_PUT_LINE('Executing: ' || V_SQLSTMT);
        EXECUTE IMMEDIATE V_SQLSTMT;
      END LOOP;
      -- Table is subpartitioned
    ELSIF (V_FLAG = 1) THEN
      --First drop all old partitions except PMAX if they exist
      PRO_PUT_LINE('Preparing to drop partitions from ' || V_TBL_NAME);
      FOR PRTTN_REC_TYP IN PRTTN_CUR_TYP LOOP
        V_SQLSTMT := 'ALTER TABLE ' || V_TBL_NAME || ' ' || CHR(10) ||
                     'DROP PARTITION ' || PRTTN_REC_TYP.PARTITION_NAME;

        PRO_PUT_LINE('Executing: ' || V_SQLSTMT);
        EXECUTE IMMEDIATE V_SQLSTMT;
      END LOOP;

      IF P_DROP_MAX_IND = 'Y' THEN
        --Second drop all subpartitions from PMAX
        --except PMAX_DEFAULT if they exist
        PRO_PUT_LINE('Preparing to drop subpartitions from ' || V_TBL_NAME);
        FOR SUBPRTTN_REC_TYP IN SUBPRTTN_CUR_TYP LOOP
          V_SQLSTMT := 'ALTER TABLE ' || V_TBL_NAME || ' ' || CHR(10) ||
                     'DROP SUBPARTITION ' ||
                     SUBPRTTN_REC_TYP.SUBPARTITION_NAME;

          PRO_PUT_LINE('Executing: ' || V_SQLSTMT);
          EXECUTE IMMEDIATE V_SQLSTMT;
        END LOOP;
      END IF;
    END IF;
  END PRO_DROP_PRTTN;

  PROCEDURE PRO_INIT_PRTTN(P_VC2_TBL_NAME IN USER_TABLES.TABLE_NAME%TYPE,
                                 P_RSTMT_IND IN VARCHAR2 DEFAULT 'N',
                                 P_PAST_MTH_NUM IN NUMBER DEFAULT 6,
                                 P_NEXT_MTH_NUM IN NUMBER DEFAULT 1
                                 ) IS
    /*
    *********************************************************************
    * name: PRO_INIT_PRTTN
    * purpose: Initialize and rebuild partitions for target table
    * parameters: P_VC2_TBL_NAME -- Target table
    * Change History
    * Date         Programmer         Description
    * -------------------------------------------------------------------------
    * 2009-08-03   Bodhi Wang         Initial Version
    ****************************************************************************
    */

    V_TBL_NAME                USER_TABLES.TABLE_NAME%TYPE := UPPER(P_VC2_TBL_NAME);
    V_PAST_MTH_NUM            NUMBER;
    V_PAST_MTH_LIMIT          NUMBER;
    V_NEXT_MTH_NUM            NUMBER;
    V_SQL                     VARCHAR2(2024);
    --V_TMP_STR                 VARCHAR2(100):='';
    V_MTH_NUM                 NUMBER:=0;
    V_PTTN_NAME               USER_TAB_PARTITIONS.PARTITION_NAME%TYPE;
    V_SUB_PTTN_NAME           USER_TAB_SUBPARTITIONS.SUBPARTITION_NAME%TYPE;
    V_PTTN_DEFLT_NAME         USER_TAB_PARTITIONS.PARTITION_NAME%TYPE :='PMAX';
    V_SUB_PTTN_DEFLT_NAME     USER_TAB_SUBPARTITIONS.SUBPARTITION_NAME%TYPE;
    V_VC2_TBL_SUB_PRTTN_STR   VARCHAR2(4000) := NULL;
    V_NUM_TBL_SUB_PRTTN_COUNT NUMBER;

    -- Get all the partitions
    CURSOR PRTTN_CUR_TYP IS
      SELECT PARTITION_NAME
        FROM USER_TAB_PARTITIONS
       WHERE TABLE_NAME = V_TBL_NAME;
/*         AND PARTITION_POSITION !=
             (SELECT MAX(PARTITION_POSITION)
                FROM USER_TAB_PARTITIONS
               WHERE TABLE_NAME = V_TBL_NAME);*/

  BEGIN
    V_PAST_MTH_LIMIT := FN_FETCH_PAST_MTH_LIMIT(V_TBL_NAME);
    IF ABS(P_PAST_MTH_NUM) > V_PAST_MTH_LIMIT THEN
       V_PAST_MTH_NUM := V_PAST_MTH_LIMIT;
    ELSE
       V_PAST_MTH_NUM := ABS(P_PAST_MTH_NUM);
    END IF;

    IF ABS(P_NEXT_MTH_NUM) > C_NEXT_MTH_LIMIT THEN
       V_NEXT_MTH_NUM := C_NEXT_MTH_LIMIT;
    ELSE
       V_NEXT_MTH_NUM := ABS(P_NEXT_MTH_NUM);
    END IF;

    -- Cleanup old partitions
    PRO_PUT_LINE('clean up old partitions of table '||V_TBL_NAME||':>>>>');
    OPT_PKG_PRTTN_UTIL.pro_cleanup_old_prttn(V_TBL_NAME, NULL, 'M');
    PRO_PUT_LINE('clean up old partitions of table '||V_TBL_NAME||':<<<<');

    PRO_PUT_LINE('Truncate table ' || V_TBL_NAME ||' Begin:>>>');
    PRO_TRUNCATE_TABLE(V_TBL_NAME);
    PRO_PUT_LINE('Truncate table ' || V_TBL_NAME ||' END:Sucess<<<');

    -- Check if subpartitioned
    OPT_PKG_PRTTN_UTIL.PRO_CHECK_TBL_SUB_PRTTN(V_TBL_NAME,
                                               V_VC2_TBL_SUB_PRTTN_STR,
                                               V_NUM_TBL_SUB_PRTTN_COUNT);

    PRO_PUT_LINE('Create partitions for table ' || V_TBL_NAME ||' Begin:>>>');
    IF (V_NUM_TBL_SUB_PRTTN_COUNT <> 0) THEN
       -- Add missed subpartitions by BUS_UNIT_SKID
       IF P_RSTMT_IND = 'Y' THEN
          -- Initialize table OPT_SHPMT_SFCT.
          PRO_PUT_LINE('Drop partitions for table ' || V_TBL_NAME ||' Begin:>>>');
          PRO_DROP_PRTTN(V_TBL_NAME);
          PRO_PUT_LINE('Drop partitions for table ' || V_TBL_NAME ||' End:Sucess<<<');

          -- created necessary partitions and subpartitions.
          FOR I IN (SELECT DISTINCT
                           c.mth_skid+c.mth_days_in_mth_qty AS PTTN_VALUE,
                           --C.CAL_MASTR_SKID,
                           C.MTH_NUM AS PTTN_MTH_NUM,
                           BU.BUS_UNIT_SKID
                    FROM OPT_SHPMT_RSTMT_PLC SR,
                         OPT_BUS_UNIT_FDIM BU,
                         OPT_CAL_MASTR_DIM C,
                         (
                         SELECT A.MTH_SKID AS MIN_MTH_SKID, B.MTH_SKID AS MAX_MTH_SKID
                           FROM (SELECT MTH_SKID
                                   FROM CAL_MASTR_DIM
                                  WHERE DAY_DATE = TRUNC(ADD_MONTHS(SYSDATE, -24))) A,
                                (SELECT MTH_SKID
                                   FROM CAL_MASTR_DIM
                                  WHERE DAY_DATE = TRUNC(ADD_MONTHS(SYSDATE, 1))) B
                         ) D  -- Added by Bodhi, 2009.9.3
                   where SR.RSTMT_IND='Y'
                     and SR.GEO_CODE<>'HEADER'
                     and SR.GEO_CODE=BU.SHPMT_GEO_CODE
                     and C.CAL_MASTR_SKID >=SR.RSTMT_START_DAY_DATE_SKID
                     and C.CAL_MASTR_SKID <=SR.RSTMT_END_DAY_DATE_SKID
                     AND C.CAL_MASTR_SKID >=D.MIN_MTH_SKID  -- Added by Bodhi, 2009.9.3
                     and C.CAL_MASTR_SKID <=D.MAX_MTH_SKID  -- Limit partitions
                order by C.MTH_NUM
                         ,BU.BUS_UNIT_SKID
                    )
          LOOP
              V_PTTN_NAME := 'P'||I.PTTN_MTH_NUM;
              V_SUB_PTTN_NAME := V_PTTN_NAME||'_'||I.BUS_UNIT_SKID;
              V_SUB_PTTN_DEFLT_NAME := V_PTTN_NAME||'_DEFAULT';

              -- Create partition at first
              IF I.PTTN_MTH_NUM <> V_MTH_NUM THEN  -- Not created
                 V_SQL := 'Alter table ' || V_TBL_NAME ||
                          ' SPLIT PARTITION '||V_PTTN_DEFLT_NAME||
                          ' AT ('||I.PTTN_VALUE ||')'||
                          ' into '||'(partition '||V_PTTN_NAME||
                          ',partition '||V_PTTN_DEFLT_NAME||')'
                          ||CHR(10)||'update indexes'
                          ;
                 PRO_PUT_LINE('SQL:' || V_SQL);
                 EXECUTE IMMEDIATE V_SQL;
                 PRO_PUT_LINE('Partition ' || V_PTTN_NAME ||
                              ' was splitted from '||V_PTTN_DEFLT_NAME||
                              ' partition');

                 V_MTH_NUM := I.PTTN_MTH_NUM;
              END IF;

              -- Create subpartition
              V_SQL := 'Alter table '||V_TBL_NAME||' SPLIT SUBPARTITION '||
                       V_PTTN_NAME||'_DEFAULT VALUES('||I.BUS_UNIT_SKID||
                       ') into (subpartition ' ||V_SUB_PTTN_NAME||
                       ', subpartition ' || V_SUB_PTTN_DEFLT_NAME||')'
                       ||CHR(10)||'update indexes'
                       ;
              PRO_PUT_LINE('SQL:' || V_SQL);
              EXECUTE IMMEDIATE V_SQL;
              PRO_PUT_LINE('Subpartition ' || V_SUB_PTTN_NAME||
                           ' was splitted from '||
                           V_SUB_PTTN_DEFLT_NAME||' subpartition!');
          END LOOP;
       ELSE
          -- Normal loading, rebuild subpartitions.
          FOR I IN PRTTN_CUR_TYP LOOP
              FOR J IN (SELECT BUS_UNIT_SKID
                          FROM OPT_BUS_UNIT_PLC
                         WHERE BUS_UNIT_SKID <> 0
                        MINUS
                        SELECT TO_NUMBER(REPLACE(REGEXP_SUBSTR(SUBPARTITION_NAME,
                                   '_[^_]*', 1, 1),'_',NULL))
                          FROM USER_TAB_SUBPARTITIONS
                         WHERE TABLE_NAME = V_TBL_NAME
                           AND PARTITION_NAME = I.PARTITION_NAME
                           AND SUBPARTITION_NAME <> I.PARTITION_NAME||
                               '_DEFAULT'
                        ) LOOP
                V_SQL := 'Alter table ' || V_TBL_NAME ||
                         ' SPLIT SUBPARTITION ' ||
                         I.PARTITION_NAME || '_DEFAULT VALUES(' ||
                         J.BUS_UNIT_SKID || ') into (subpartition ' ||
                         I.PARTITION_NAME || '_' || J.BUS_UNIT_SKID ||
                         ', subpartition ' || I.PARTITION_NAME ||
                         '_DEFAULT)'
                         ||' update indexes'
                         ;
                PRO_PUT_LINE('SQL:' || V_SQL);
                EXECUTE IMMEDIATE V_SQL;
                PRO_PUT_LINE('Partition '||I.PARTITION_NAME||'_'||
                              J.BUS_UNIT_SKID||
                             ' was splitted from '||
                             I.PARTITION_NAME||'_DEFAULT subpartition');
              END LOOP;
          END LOOP;

          -- Create partitions
          BEGIN
             OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN(V_TBL_NAME,
                                                      V_PAST_MTH_NUM,
                                                      V_NEXT_MTH_NUM,
                                                      'N');
          EXCEPTION
          WHEN OTHERS THEN
            -- If failed to create other months partitions,
            -- Remove partitions at first, then recreate.
            OPT_PKG_UTIL.PRO_PUT_LINE(SQLERRM);
            OPT_PKG_UTIL.PRO_PUT_LINE('Recreate partitions after removing partitions');
            -- Initialize table OPT_SHPMT_SFCT.
            PRO_PUT_LINE('Drop partitions for table ' || V_TBL_NAME ||' Begin:>>>');
            PRO_DROP_PRTTN(V_TBL_NAME,'N');
            PRO_PUT_LINE('Drop partitions for table ' || V_TBL_NAME ||' End:Sucess<<<');

            OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN(V_TBL_NAME,
                                                      V_PAST_MTH_NUM,
                                                      V_NEXT_MTH_NUM,
                                                      'N');
          END;
          PRO_PUT_LINE('Create partitions for table ' ||
                        V_TBL_NAME || ' End:<<<<');
       END IF;
    ELSE
       -- Only partition exists.
       -- Create partitions
       PRO_PUT_LINE('Create partitions for table ' ||
                     V_TBL_NAME || ' Begin:>>>>');
       OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN(V_TBL_NAME,
                                                   V_PAST_MTH_NUM,
                                                   V_NEXT_MTH_NUM,
                                                   'N');
       PRO_PUT_LINE('Create partitions for table ' ||
                     V_TBL_NAME || ' End:<<<<');
    END IF;

  END PRO_INIT_PRTTN;

  PROCEDURE PRO_CREAT_MSNG_PRTTN(P_VC2_TBL_NAME IN USER_TABLES.TABLE_NAME%TYPE,
                               P_PAST_MTH_NUM IN NUMBER DEFAULT 6,
                               P_NEXT_MTH_NUM IN NUMBER DEFAULT 1
                              ) IS
    /*
    *********************************************************************
    * name: PRO_CREAT_MSNG_PRTTN
    * purpose: Initialize and rebuild partitions for target table
    * parameters: P_VC2_TBL_NAME -- Target table
    * Change History
    * Date         Programmer         Description
    * -------------------------------------------------------------------------
    * 2009-08-03   Bodhi Wang         Initial Version
    ****************************************************************************
    */

    V_TBL_NAME                USER_TABLES.TABLE_NAME%TYPE := UPPER(P_VC2_TBL_NAME);
    V_PAST_MTH_NUM            NUMBER;
    V_PAST_MTH_LIMIT          NUMBER;
    V_NEXT_MTH_NUM            NUMBER;
    V_SQL                     VARCHAR2(2024);
    V_MIN_PTTN_NAME           USER_TAB_PARTITIONS.PARTITION_NAME%TYPE;
    V_LIMIT_MTH_NAME          VARCHAR2(6);
    --V_TMP_STR                 VARCHAR2(100);
    --V_PTTN_NAME               USER_TAB_PARTITIONS.PARTITION_NAME%TYPE;
    --V_SUB_PTTN_NAME           USER_TAB_SUBPARTITIONS.SUBPARTITION_NAME%TYPE;
    --V_PTTN_DEFLT_NAME         USER_TAB_PARTITIONS.PARTITION_NAME%TYPE :='PMAX';
    --V_SUB_PTTN_DEFLT_NAME     USER_TAB_SUBPARTITIONS.SUBPARTITION_NAME%TYPE;
    --V_VC2_TBL_SUB_PRTTN_STR   VARCHAR2(100) := NULL;
    V_VC2_TBL_SUB_PRTTN_STR   VARCHAR2(4000) := NULL;
    V_NUM_TBL_SUB_PRTTN_COUNT NUMBER;

    -- Get all the partitions
    CURSOR PRTTN_CUR_TYP IS
      SELECT PARTITION_NAME
        FROM USER_TAB_PARTITIONS
       WHERE TABLE_NAME = V_TBL_NAME;
/*         AND PARTITION_POSITION !=
             (SELECT MAX(PARTITION_POSITION)
                FROM USER_TAB_PARTITIONS
               WHERE TABLE_NAME = V_TBL_NAME);*/

  BEGIN
    V_PAST_MTH_LIMIT := FN_FETCH_PAST_MTH_LIMIT(V_TBL_NAME);
    IF ABS(P_PAST_MTH_NUM) > V_PAST_MTH_LIMIT THEN
       V_PAST_MTH_NUM := V_PAST_MTH_LIMIT;
    ELSE
       V_PAST_MTH_NUM := ABS(P_PAST_MTH_NUM);
    END IF;

    IF ABS(P_NEXT_MTH_NUM) > C_NEXT_MTH_LIMIT THEN
       V_NEXT_MTH_NUM := C_NEXT_MTH_LIMIT;
    ELSE
       V_NEXT_MTH_NUM := ABS(P_NEXT_MTH_NUM);
    END IF;

    -- Check if subpartitioned
    OPT_PKG_PRTTN_UTIL.PRO_CHECK_TBL_SUB_PRTTN(V_TBL_NAME,
                                               V_VC2_TBL_SUB_PRTTN_STR,
                                               V_NUM_TBL_SUB_PRTTN_COUNT);

    IF (V_NUM_TBL_SUB_PRTTN_COUNT <> 0) THEN
      -- Normal loading, create missed subpartitions.
      FOR I IN PRTTN_CUR_TYP LOOP
        FOR J IN (SELECT BUS_UNIT_SKID
                    FROM OPT_BUS_UNIT_PLC
                   WHERE BUS_UNIT_SKID <> 0
                  MINUS
                  SELECT TO_NUMBER(REPLACE(REGEXP_SUBSTR(SUBPARTITION_NAME,
                                   '_[^_]*', 1, 1),'_',NULL))
                    FROM USER_TAB_SUBPARTITIONS
                   WHERE TABLE_NAME = V_TBL_NAME
                     AND PARTITION_NAME = I.PARTITION_NAME
                     AND SUBPARTITION_NAME <> I.PARTITION_NAME||'_DEFAULT') LOOP
          V_SQL := 'Alter table ' || V_TBL_NAME || ' SPLIT SUBPARTITION ' ||
                   I.PARTITION_NAME || '_DEFAULT VALUES(' ||
                   J.BUS_UNIT_SKID || ') into (subpartition ' ||
                   I.PARTITION_NAME || '_' || J.BUS_UNIT_SKID ||
                   ', subpartition ' ||I.PARTITION_NAME||'_DEFAULT)'
                   ||CHR(10)||' update indexes'
                   ;
          PRO_PUT_LINE('Partition '||I.PARTITION_NAME||'_'||J.BUS_UNIT_SKID||
                       ' was splitted from '||
                       I.PARTITION_NAME||'_DEFAULT subpartition:>>>>');
          PRO_PUT_LINE('SQL:' || V_SQL);
          EXECUTE IMMEDIATE V_SQL;
          PRO_PUT_LINE('Partition '||I.PARTITION_NAME||'_'||J.BUS_UNIT_SKID||
                       ' was splitted from '||
                       I.PARTITION_NAME||'_DEFAULT subpartition:<<<<<');
        END LOOP;
      END LOOP;

    END IF;


    -- Find out smallest partition except PMAX.
    -- If not exist, call OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN directly
    -- to create partitions.
    SELECT MIN(PARTITION_NAME)
      INTO V_MIN_PTTN_NAME
      FROM USER_TAB_PARTITIONS
     WHERE TABLE_NAME = V_TBL_NAME
       AND PARTITION_POSITION !=
           (SELECT MAX(PARTITION_POSITION)
              FROM USER_TAB_PARTITIONS
             WHERE TABLE_NAME = V_TBL_NAME);
    IF V_MIN_PTTN_NAME IS NULL THEN
         -- Create missing range partitions
         PRO_PUT_LINE('No Non-PMAX partitions of table ' || V_TBL_NAME || ' exist!');
         PRO_PUT_LINE('Create partitions for table ' || V_TBL_NAME || ' Begin:>>>>');
         OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN(V_TBL_NAME,
                                                V_PAST_MTH_NUM,
                                                V_NEXT_MTH_NUM,
                                                'N');
         PRO_PUT_LINE('Create partitions for table ' || V_TBL_NAME || ' End:<<<<');
         RETURN;
    END IF;

    --MIN < Limit
    --Shrink partitions.
    --shrink data of such partitions which are less than limit month number.
    --shrink them to limit month partition.
    SELECT DISTINCT TO_CHAR(TRUNC(mth_start_date, 'MM'), 'YYYYMM')
      INTO V_LIMIT_MTH_NAME
      FROM cal_mastr_dim
     WHERE mth_start_date =
           TRUNC(ADD_MONTHS(SYSDATE, -1 * V_PAST_MTH_LIMIT), 'MM');

    FOR merge_prttn_rec IN (SELECT PARTITION_NAME
                FROM USER_TAB_PARTITIONS
               WHERE TABLE_NAME = V_TBL_NAME
                 AND PARTITION_NAME < 'P'||V_LIMIT_MTH_NAME
                 AND PARTITION_POSITION !=
                     (SELECT MAX(PARTITION_POSITION)
                        FROM USER_TAB_PARTITIONS
                       WHERE TABLE_NAME = V_TBL_NAME)
            ORDER BY PARTITION_NAME DESC) LOOP
        V_SQL := 'Alter table ' || V_TBL_NAME || ' MERGE PARTITIONS ' ||
                 merge_prttn_rec.PARTITION_NAME ||', P'||V_LIMIT_MTH_NAME||
                 ' INTO PARTITION P'||V_LIMIT_MTH_NAME||' update indexes';
        PRO_PUT_LINE('Merge partition P'||merge_prttn_rec.PARTITION_NAME||
                     ' into P'||V_LIMIT_MTH_NAME||':>>>>');
        PRO_PUT_LINE('SQL:' || V_SQL);
        EXECUTE IMMEDIATE V_SQL;
        PRO_PUT_LINE('Merge partition P'||merge_prttn_rec.PARTITION_NAME||
                    ' into P'||V_LIMIT_MTH_NAME||':<<<<');
      END LOOP;

    --Limit < MIN
    --Expand partitions.
    FOR mth_rec_typ IN (SELECT DISTINCT mth_skid+mth_days_in_mth_qty AS mth_skid,
	                             TO_CHAR(TRUNC(mth_start_date,'MM'),'YYYYMM') AS mth_name
                          FROM cal_mastr_dim
                         WHERE mth_start_date >= TRUNC(ADD_MONTHS(SYSDATE,-1 * V_PAST_MTH_LIMIT),'MM')
                           AND mth_start_date <  TO_DATE(SUBSTR(V_MIN_PTTN_NAME,2,6)||'01','YYYYMMDD')
		 		     AND NOT EXISTS (SELECT 1
		 			                 FROM user_tab_partitions
		 							 WHERE partition_name = 'P'||TO_CHAR(TRUNC(mth_start_date,'MM'),'YYYYMM')
		 							 AND   table_name = V_TBL_NAME
		 							)
                        ORDER BY mth_skid ASC
		 			    )
    LOOP
      V_SQL := 'ALTER TABLE '||V_TBL_NAME||' '||CHR(10)||
		           'SPLIT PARTITION '||V_MIN_PTTN_NAME||' '||
		           'AT ('||TO_CHAR(mth_rec_typ.mth_skid)||') '||CHR(10)||
		 			     'INTO (PARTITION P'||mth_rec_typ.mth_name||', '||CHR(10)||
		 			     '      PARTITION '||V_MIN_PTTN_NAME||CHR(10)||'     )';

      PRO_PUT_LINE('Creating partition P'||mth_rec_typ.mth_name||':>>>>');
      PRO_PUT_LINE(V_SQL);

      EXECUTE IMMEDIATE V_SQL;
      PRO_PUT_LINE('Creating partition P'||mth_rec_typ.mth_name||':<<<<');
    END LOOP;


    -- Create range partitions which are greater than the fetched smallest partition.
    PRO_PUT_LINE('Create partitions for table ' || V_TBL_NAME || ' Begin:>>>>');
    OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN(V_TBL_NAME,
                                                V_PAST_MTH_NUM,
                                                V_NEXT_MTH_NUM,
                                                'N');
    PRO_PUT_LINE('Create partitions for table ' || V_TBL_NAME || ' End:<<<<');
  END PRO_CREAT_MSNG_PRTTN;

  PROCEDURE PRO_EXCHG_PRTTN(P_VC2_SRCE_TBL IN USER_TABLES.TABLE_NAME%TYPE,
                            P_VC2_DEST_TBL IN USER_TABLES.TABLE_NAME%TYPE,
                            P_RSTMT_IND IN VARCHAR2 DEFAULT 'N',
                            P_PAST_MTH_NUM IN NUMBER DEFAULT 6,
							V_REGN VARCHAR2 DEFAULT NULL)--SOX IMPLEMENTATION PROJECT
  /*
    ***************************************************************************
    * Program : PRO_EXCHG_PRTTN
    * Version : 1.0
    * Author  : Bodhi Wang
    * Purpose : Exchanges partitions between 2 partitioned tables for data
    * Parameters :  p_vc2_srce_tbl -- source table
    *               p_vc2_dest_tbl -- destination table
    * Note:  This procedure does not provide error handling --calling code must
    *          handle errors returned from this procedure
    *
    * Change History
    * Date         Programmer         Description
    * -------------------------------------------------------------------------
    * 2009-08-03   Bodhi Wang         Initial Version
    * 2010-04-12   David Lang          To avoid the error caused by TX metadata setup inproperly
    ****************************************************************************
    */
   IS
    V_VC2_TBL_SUB_PRTTN_STR   VARCHAR2(100);
    V_NUM_TBL_SUB_PRTTN_COUNT NUMBER;
    V_VC2_DATE_STR            VARCHAR2(8);
    --V_NUM_SRCE_TBL_COUNT      NUMBER(1);
    --V_NUM_DEST_TBL_COUNT      NUMBER(1);
    V_NUM_PAST_MTH  NUMBER;
    V_PAST_MTH_LIMIT          NUMBER;
    V_VC2_MAX_PRTTN USER_TAB_PARTITIONS.PARTITION_NAME%TYPE;
    v_vc2_temp_tbl         user_tables.table_name%TYPE;
    v_vc2_srce_tbl         user_tables.table_name%TYPE := UPPER(p_vc2_srce_tbl);
    v_vc2_dest_tbl         user_tables.table_name%TYPE := UPPER(p_vc2_dest_tbl);
  BEGIN
    V_PAST_MTH_LIMIT := FN_FETCH_PAST_MTH_LIMIT(P_VC2_DEST_TBL);
    IF ABS(P_PAST_MTH_NUM) > V_PAST_MTH_LIMIT THEN
       V_NUM_PAST_MTH := V_PAST_MTH_LIMIT;
    ELSE
       V_NUM_PAST_MTH := ABS(P_PAST_MTH_NUM);
    END IF;

    -- Check if subpartitioned
    OPT_PKG_PRTTN_UTIL.PRO_CHECK_TBL_SUB_PRTTN(V_VC2_DEST_TBL,
                                               V_VC2_TBL_SUB_PRTTN_STR,
                                               V_NUM_TBL_SUB_PRTTN_COUNT);

    IF P_RSTMT_IND = 'Y' THEN
       SELECT MIN(C.MTH_NUM)
         INTO V_VC2_DATE_STR
         FROM OPT_SHPMT_RSTMT_PLC SR,
              OPT_CAL_MASTR_DIM C
        where SR.RSTMT_IND='Y'
          and SR.GEO_CODE<>'HEADER'
          and C.CAL_MASTR_SKID >=SR.RSTMT_START_DAY_DATE_SKID
          and C.CAL_MASTR_SKID <=SR.RSTMT_END_DAY_DATE_SKID;
    ELSE
       -- Get the date string that composes the partition name
       --for the oldest partition we'll exchange
       -- Updated by David for SD27423868 on 2009-12-03 in R9
       SELECT MTH_NUM
         INTO V_VC2_DATE_STR
         FROM CAL_MASTR_DIM CAL
        WHERE DAY_DATE = --ADD_MONTHS(TRUNC(SYSDATE), -V_NUM_PAST_MTH);
        (CASE(TO_NUMBER(TO_CHAR(LAST_DAY(SYSDATE), 'MM')) -
             TO_NUMBER(TO_CHAR(LAST_DAY(SYSDATE - 6), 'MM'))) WHEN 0 THEN
        TRUNC(ADD_MONTHS(TRUNC(SYSDATE, 'MM'),
                         -(V_NUM_PAST_MTH -1))) ELSE
        TRUNC(ADD_MONTHS(TRUNC(SYSDATE, 'MM'),
                         -V_NUM_PAST_MTH)) END);
    END IF;
    PRO_PUT_LINE('MIN CAL_SKID: ' ||V_VC2_DATE_STR);

    --Updated by David for issue fixing in R10 on 2010-4-12
    IF V_VC2_DATE_STR IS NULL THEN
    PRO_PUT_LINE('The shipment restatement metadata was not setup properly by TX!!!');
    PRO_PUT_LINE('The shipment resteatement will be skipped.');
    ELSE
    -- Get maxvalue partition name as this should never be exchanged
    SELECT PARTITION_NAME
      INTO V_VC2_MAX_PRTTN
      FROM USER_TAB_PARTITIONS
     WHERE TABLE_NAME = V_VC2_SRCE_TBL
       AND PARTITION_POSITION =
           (SELECT MAX(PARTITION_POSITION)
              FROM USER_TAB_PARTITIONS
             WHERE TABLE_NAME = P_VC2_SRCE_TBL);
    PRO_PUT_LINE('MAX Partition: ' ||V_VC2_MAX_PRTTN);

    -- Create temp table
    opt_pkg_prttn_util.pro_create_temp_tbl(v_vc2_dest_tbl, v_vc2_temp_tbl,V_REGN);--SOX IMPLEMENTATION PROJECT

    IF (V_NUM_TBL_SUB_PRTTN_COUNT = 0) THEN
      -- Table is partitioned
      -- Loop thru all partitions in the source table and exchange each one
      FOR REC IN (SELECT PARTITION_NAME
                    FROM USER_TAB_PARTITIONS
                   WHERE TABLE_NAME = V_VC2_SRCE_TBL
                     AND PARTITION_NAME >= 'P' || V_VC2_DATE_STR
                     AND PARTITION_NAME != V_VC2_MAX_PRTTN) LOOP
        OPT_PKG_UTIL.PRO_PUT_LINE('Exchanging partition ' ||
                                  REC.PARTITION_NAME);
        OPT_PKG_PRTTN_UTIL.PRO_EXCHANGE_TBL_WITH_PRTTN(V_VC2_SRCE_TBL,
                                                       REC.PARTITION_NAME,
                                                       V_VC2_TEMP_TBL,
                                                       'N');
        BEGIN
          OPT_PKG_PRTTN_UTIL.PRO_EXCHANGE_TBL_WITH_PRTTN(V_VC2_DEST_TBL,
                                                         REC.PARTITION_NAME,
                                                         V_VC2_TEMP_TBL,
                                                         'N');
        EXCEPTION
          WHEN OTHERS THEN
            -- If exchange between temp table and dest table fails,
            --need to exchange back to source
            OPT_PKG_UTIL.PRO_PUT_LINE(SQLERRM);
            OPT_PKG_UTIL.PRO_PUT_LINE('ERROR!!!  Need to preserve data');
            OPT_PKG_UTIL.PRO_PUT_LINE('Exchanging from ' || V_VC2_TEMP_TBL ||
                                      ' back to ' || V_VC2_SRCE_TBL || '.' ||
                                      REC.PARTITION_NAME);
            OPT_PKG_PRTTN_UTIL.PRO_EXCHANGE_TBL_WITH_PRTTN(V_VC2_SRCE_TBL,
                                                           REC.PARTITION_NAME,
                                                           V_VC2_TEMP_TBL,
                                                           'N');
            RAISE_APPLICATION_ERROR(-20080,
                                    'ERROR!!! Exchanging between ' ||
                                    V_VC2_TEMP_TBL || ' and ' ||
                                    V_VC2_DEST_TBL);
        END;
        -- Need to truncate temp table after exchange
        OPT_PKG_UTIL.PRO_TRUNCATE_TBL(V_VC2_TEMP_TBL);
      END LOOP;
    ELSE
      -- Table is subpartitioned
      -- This means exchange all subpartitions
      FOR REC IN (SELECT SUBPARTITION_NAME
                    FROM USER_TAB_SUBPARTITIONS
                   WHERE TABLE_NAME = V_VC2_SRCE_TBL
                     AND PARTITION_NAME >= 'P' || V_VC2_DATE_STR
                     AND PARTITION_NAME != V_VC2_MAX_PRTTN) LOOP
        OPT_PKG_UTIL.PRO_PUT_LINE('Exchanging subpartition ' ||
                                  REC.SUBPARTITION_NAME);
        OPT_PKG_PRTTN_UTIL.PRO_EXCHANGE_TBL_WITH_PRTTN(V_VC2_SRCE_TBL,
                                                       REC.SUBPARTITION_NAME,
                                                       V_VC2_TEMP_TBL,
                                                       'Y');
        BEGIN
          OPT_PKG_PRTTN_UTIL.PRO_EXCHANGE_TBL_WITH_PRTTN(V_VC2_DEST_TBL,
                                                         REC.SUBPARTITION_NAME,
                                                         V_VC2_TEMP_TBL,
                                                         'Y');
        EXCEPTION
          WHEN OTHERS THEN
            -- If exchange between temp table and dest table fails,
            --need to exchange back to source
            PRO_PUT_LINE(SQLERRM);
            PRO_PUT_LINE('ERROR!!!  Need to preserve data');
            PRO_PUT_LINE('Exchanging from ' || V_VC2_TEMP_TBL ||
                                      ' back to ' || V_VC2_SRCE_TBL);
            OPT_PKG_PRTTN_UTIL.PRO_EXCHANGE_TBL_WITH_PRTTN(
                                         V_VC2_SRCE_TBL,
                                         REC.SUBPARTITION_NAME,
                                         V_VC2_TEMP_TBL,
                                         'Y');
            RAISE_APPLICATION_ERROR(-20080,
                                    'ERROR!!! Exchanging between ' ||
                                    V_VC2_TEMP_TBL || ' and ' ||
                                    V_VC2_DEST_TBL);
        END;
        -- Need to truncate temp table after exchange
        OPT_PKG_UTIL.PRO_TRUNCATE_TBL(V_VC2_TEMP_TBL);
      END LOOP;
    END IF;

    -- Cleanup temp table
    EXECUTE IMMEDIATE 'DROP TABLE '||v_vc2_temp_tbl;
    END IF;
  END PRO_EXCHG_PRTTN;

  PROCEDURE PRO_LOAD(P_VC2_SRCE_TBL IN USER_TABLES.TABLE_NAME%TYPE,
                     P_VC2_DEST_TBL IN USER_TABLES.TABLE_NAME%TYPE,
                     P_VC2_MTH_QRY_NAME IN VARCHAR2,
                     P_CTRLM_ORD_ID IN VARCHAR2,
                     P_RSTMT_IND IN VARCHAR2 DEFAULT 'N'
                     )
  /*
    ***************************************************************************
    * Program : PRO_LOAD
    * Version : 1.0
    * Author  : Bodhi Wang
    * Purpose : Exchanges partitions between 2 partitioned tables for data
    * Parameters :  p_vc2_srce_tbl -- source table
    *               p_vc2_dest_tbl -- destination table
                    P_VC2_MTH_QRY_NAME -- QRY_NAME_CODE
                    P_CTRLM_ORD_ID    -- CTRL-M Order ID
                    P_RSTMT_IND       -- Restatement Indicator, default 'N' for
                                      -- normal shipment load, 'Y' for shipment
                                      -- restatement.
       * Note:  This procedure does not provide error handling --calling code must
    *          handle errors returned from this procedure
    *
    * Change History
    * Date         Programmer         Description
    * -------------------------------------------------------------------------
    * 2009-08-08   Bodhi Wang         Initial Version
    ****************************************************************************
    */
   IS
    V_NUM_PAST_MTH  NUMBER;

  BEGIN
    PRO_PUT_LINE('PRO_LOAD:>>>>');
    PRO_PUT_LINE('Fetch PRCSS_MTH:>>>>');
    SELECT PARM_VAL INTO V_NUM_PAST_MTH
      FROM OPT_QRY_EXTRN_PARM_PRC
     WHERE QRY_NAME_CODE = P_VC2_MTH_QRY_NAME
       AND PARM_NAME_CODE='PRCSS_MTH';
    PRO_PUT_LINE('Fetch PRCSS_MTH['||V_NUM_PAST_MTH||']');
    PRO_PUT_LINE('Fetch PRCSS_MTH:<<<<');

    -- Rebuild partitions
    PRO_PUT_LINE('create missing partition:>>>>');
    OPT_SHPMT_LOAD_PKG.PRO_CREAT_MSNG_PRTTN(
                       P_VC2_DEST_TBL,
                       V_NUM_PAST_MTH
                      );
    PRO_PUT_LINE('create missing partition:<<<<');

    -- Exchange partitions
    PRO_PUT_LINE('exchange partitions:>>>>');
    OPT_SHPMT_LOAD_PKG.PRO_EXCHG_PRTTN(
                  P_VC2_SRCE_TBL,
                  P_VC2_DEST_TBL,
                  P_RSTMT_IND,
                  V_NUM_PAST_MTH
                 );
    PRO_PUT_LINE('exchange partition:<<<<');

    -- Cleanup old partitions
    PRO_PUT_LINE('clean up old partitions of table '||P_VC2_DEST_TBL||':>>>>');
    OPT_PKG_PRTTN_UTIL.pro_cleanup_old_prttn(P_VC2_DEST_TBL, NULL, 'M');
    PRO_PUT_LINE('clean up old partitions of table '||P_VC2_DEST_TBL||':<<<<');
    PRO_PUT_LINE('clean up old partitions of table '||P_VC2_SRCE_TBL||':>>>>');
    OPT_PKG_PRTTN_UTIL.pro_cleanup_old_prttn(P_VC2_SRCE_TBL, NULL, 'M');
    PRO_PUT_LINE('clean up old partitions of table '||P_VC2_SRCE_TBL||':<<<<');

    -- Rebuild indexes
    PRO_PUT_LINE('Rebuild indexes of table '||P_VC2_DEST_TBL||':>>>>');
    OPT_PKG_PRTTN_UTIL.pro_rebuild_indexes(P_VC2_DEST_TBL, P_CTRLM_ORD_ID);
    PRO_PUT_LINE('clean up old partitions of table '||P_VC2_DEST_TBL||':<<<<');
    PRO_PUT_LINE('Rebuild indexes of table '||P_VC2_SRCE_TBL||':>>>>');
    OPT_PKG_PRTTN_UTIL.pro_rebuild_indexes(P_VC2_SRCE_TBL, P_CTRLM_ORD_ID);
    PRO_PUT_LINE('Rebuild indexes of table '||P_VC2_SRCE_TBL||':<<<<');

    PRO_PUT_LINE('PRO_LOAD:<<<<');
  END PRO_LOAD;

  PROCEDURE PRO_FINAL(P_ETL_RUN_DATE IN DATE)
  /*
    ***************************************************************************
    * Program : PRO_FINAL
    * Version : 1.0
    * Author  : Bodhi Wang
    * Purpose : Set RSTMT_IND in table OPT_SHPMT_RSTMT_PLC as 'N' or 'S'.
    * Parameters :  P_ETL_RUN_DATE -- ETL run date
       * Note:  This procedure does not provide error handling --calling code must
    *          handle errors returned from this procedure
    *
    * Change History
    * Date         Programmer         Description
    * -------------------------------------------------------------------------
    * 2009-08-10   Bodhi Wang         Initial Version
    ****************************************************************************
    */
   IS
    V_ACTIV_JOBS        NUMBER;
    V_IS_TOO_LONG       VARCHAR2(1);

  BEGIN
    -- count active jobs
    SELECT COUNT(*) INTO V_ACTIV_JOBS
      FROM OPT_SHPMT_RSTMT_JOB_PRC
     where ACTIV_IND='Y';

    -- judge if restatement period is too long
    SELECT CASE
           WHEN COUNT(*) > 0 THEN 'Y'
           ELSE 'N'
           END INTO V_IS_TOO_LONG
    FROM
    (
      select PARM_NAME_CODE AS SHPMT_GEO_CODE,
             TO_NUMBER(PARM_VAL) AS TIME_RANGE_LIMIT
        from OPT_QRY_EXTRN_PARM_PRC
       where QRY_NAME_CODE='TIME_RANGE_LIMIT'
    ) PAR,
    (
     select geo_code AS shpmt_geo_code,
            months_between(RSTMT_END_DAY_DATE+1,
                           RSTMT_START_DAY_DATE) as TIME_RANGE
      from OPT_SHPMT_RSTMT_PLC
     where GEO_CODE<>'HEADER'
       and ETL_RUN_DATE=P_ETL_RUN_DATE
       and RSTMT_IND='Y'
    ) SR
    WHERE PAR.SHPMT_GEO_CODE=SR.SHPMT_GEO_CODE
      AND PAR.TIME_RANGE_LIMIT<SR.TIME_RANGE;

    -- Check if too long or all jobs are disabled
    -- If not too long and there're active jobs
    -- shipment data must have been restated.
    -- otherwise not yet, just skipped.
    IF V_IS_TOO_LONG = 'N' AND V_ACTIV_JOBS > 0 THEN
       -- 'N'. Restatement is completed.
       update OPT_SHPMT_RSTMT_PLC
          set RSTMT_IND='N',UPDT_DATE=SYSDATE
        where ETL_RUN_DATE=P_ETL_RUN_DATE
          and RSTMT_IND in ('Y', 'S');
    ELSE
       -- 'S', Jobs are skipped because of time range limit or
       -- jobs are disabled.
       -- Need check if all jobs are disabled
       -- and restate manually in weekend.
       update OPT_SHPMT_RSTMT_PLC
          set RSTMT_IND='S',UPDT_DATE=SYSDATE
        where ETL_RUN_DATE=P_ETL_RUN_DATE
          and RSTMT_IND='Y';
    END IF;
  END PRO_FINAL;

  PROCEDURE PRO_CHECK_TBL(P_TBL_NAME IN USER_TABLES.TABLE_NAME%TYPE,
                          P_FLAG     OUT NUMBER) IS
  /*
  **********************************************************************
  * name: opt_check_tbl
  * purpose: checking table, it's must be a partition table and with
  *         default P_DEFAULT partition. and isn't subpartitioned
  * parameters:
  *       P_TBL_NAME : table_name
  *       P_FLAG : 3 -- Table is not partitioned
  *                   2 -- Table is not exist
  *                   1 -- Table is subpartitioned
  *                   0 -- Table is partitioned by srce_sys_id
  * author: Myra.cao
  * version: 1.00 - initial version
  *      1.01 - string cut according to new line marker
  ********************************************************************
  */
  V_TBL_NAME           USER_TABLES.TABLE_NAME%TYPE := UPPER(P_TBL_NAME);
  V_NUM_TBL_COUNT      NUMBER;
  --V_NUM_PRTTN_COUNT    NUMBER;
  V_NUM_SUBPRTTN_COUNT NUMBER;

  BEGIN
    SELECT COUNT(1)
      INTO V_NUM_TBL_COUNT
      FROM USER_TABLES
     WHERE TABLE_NAME = V_TBL_NAME;

    IF (V_NUM_TBL_COUNT = 0) THEN
      RAISE_APPLICATION_ERROR(-20060,
                              'ERROR! ' || V_TBL_NAME || ' does not exist');
      P_FLAG := 2;
    ELSE

      SELECT COUNT(1)
        INTO V_NUM_SUBPRTTN_COUNT
        FROM USER_TAB_SUBPARTITIONS
       WHERE TABLE_NAME = V_TBL_NAME;

      IF (V_NUM_SUBPRTTN_COUNT > 0) THEN
        P_FLAG := 1;
      ELSE
        PRO_PUT_LINE('Table ' || V_TBL_NAME || ' is not subpartitioned');
        P_FLAG := 0;
      END IF;
    END IF;

  END PRO_CHECK_TBL;

PROCEDURE PRO_CHK_SHPMT_ACTL_DATE(P_CURR_DATE IN VARCHAR2 DEFAULT TO_CHAR(SYSDATE))
IS
  /*
  **********************************************************************
  * name: PRO_CHK_SHPMT_ACTL_DATE
  * purpose: For CR1066 in optima R11 to check whether to use actual shipment
  *          for last month
  * parameters:
  *       P_CURR_DATE : Default to be sysdate
  * author: Barbara.Li
  * Date: Nov-24th-2010
  * version: 1.00 - initial version
  * version: 1.01 - Code refining according to Beckman's coments
  ********************************************************************
  */
    v_curr_day VARCHAR2( 15);
    v_last_workday VARCHAR2(15);
    v_dyn_sql VARCHAR2(1000);
    --Updated by Barbara on Mar 28th 2011 to user %TYPE designation according to Beckman's comments
    v_row_cnt NUMBER;
    v_P1M_ACTL_SHPMT_AVAIL_IND OPT_SHPMT_ACTL_FRCST_PLC.P1M_ACTL_SHPMT_AVAIL_IND%TYPE;
    v_parm_val S_LST_OF_VAL_PLC.VAL%TYPE;
    v_week_num NUMBER(2);
    v_weekday_desc VARCHAR2(15);
    v_weekday_num NUMBER(2);
    v_ACTL_SHPMT_EXPCT_DATE OPT_SHPMT_ACTL_FRCST_PLC.ACTL_SHPMT_EXPCT_DATE%TYPE;
BEGIN
   v_curr_day := P_CURR_DATE;
   DBMS_OUTPUT.PUT_LINE(v_curr_day);

   --Check control table whethere contains data, it will be empty for the first run, if it has more than one record, delete them first
   SELECT COUNT(1)
   INTO v_row_cnt
   FROM opt_shpmt_actl_frcst_plc;

   IF v_row_cnt = 0 OR v_row_cnt > 1
   THEN
     IF v_row_cnt > 1 THEN
       DELETE opt_shpmt_actl_frcst_plc;
       COMMIT;
     END IF;

     INSERT INTO opt_shpmt_actl_frcst_plc (ACTL_SHPMT_EXPCT_DATE,P1M_SHPMT_FULL_LOAD_IND,P1M_ACTL_SHPMT_AVAIL_IND)
     SELECT '','N','N'
     FROM dual;

     COMMIT;
   END IF;

   --Query actual shipment expected date current in the table
   SELECT actl_shpmt_expct_date, P1M_ACTL_SHPMT_AVAIL_IND
   INTO v_ACTL_SHPMT_EXPCT_DATE, v_P1M_ACTL_SHPMT_AVAIL_IND
   FROM opt_shpmt_actl_frcst_plc;

   --If it is a new month, update current parameter to 'N'
   IF v_ACTL_SHPMT_EXPCT_DATE IS NULL OR to_char(v_ACTL_SHPMT_EXPCT_DATE,'YYYYMM') <> to_char(to_Date(v_curr_day),'YYYYMM')
   THEN
     DBMS_OUTPUT.PUT_LINE('First Run or A New Month, Update last month actual shipment indicator to N!');
     UPDATE opt_shpmt_actl_frcst_plc
     SET P1M_SHPMT_FULL_LOAD_IND = 'N',
         P1M_ACTL_SHPMT_AVAIL_IND = 'N';
     COMMIT;
     v_P1M_ACTL_SHPMT_AVAIL_IND := 'N';
   END IF;

   -- Check current indicator set in table
   DBMS_OUTPUT.PUT_LINE('Currently, Last Month Actualization indicator is set to '|| v_P1M_ACTL_SHPMT_AVAIL_IND );

   IF v_P1M_ACTL_SHPMT_AVAIL_IND = 'N'
   THEN
   -- Convert ODS parameter to date
     SELECT val
     INTO v_parm_val
     FROM s_lst_of_val_plc
     WHERE TYPE = 'PG_APP_PARAMETERS'
     AND NAME = 'ShipmentActualizationDay';

     DBMS_OUTPUT.PUT_LINE('Last Month Shipment Actualization Day set in Tx is '|| v_parm_val);

     v_week_num      := substr(v_parm_val,1,instr(v_parm_val,'-',1)-1);

     v_weekday_desc  := upper(substr(v_parm_val,instr(v_parm_val,'-',1)+1,3));

     --Updated by Barbara on Mar 28th 2011 according to Beckman's comments to avoid using substr in each case when statement
     CASE
       WHEN v_weekday_desc = 'SUN'
       THEN
         v_weekday_num := 1;
       WHEN v_weekday_desc = 'MON'
       THEN
         v_weekday_num := 2;
       WHEN v_weekday_desc = 'TUE'
       THEN
         v_weekday_num := 3;
       WHEN v_weekday_desc = 'WED'
       THEN
         v_weekday_num := 4;
       WHEN v_weekday_desc = 'THU'
       THEN
         v_weekday_num := 5;
       WHEN v_weekday_desc = 'FRI'
       THEN
         v_weekday_num := 6;
       WHEN v_weekday_desc = 'SAT'
       THEN
         v_weekday_num := 7;
     END CASE;

     DBMS_OUTPUT.PUT_LINE('Converting this parameter to date...');
     --v_dyn_sql := 'SELECT to_char(next_day(trunc(to_date('''||v_curr_day||'''),''MM''),'||v_weekday_num||') + ('|| v_week_num || '-1) * 7) FROM DUAL'; --Commented By Sathyarajan on 27-Jan-2012 ... Refer PR#PM00014537 (Weekend Actualization Plan issue)

     v_dyn_sql := 'SELECT to_char(NEXT_DAY(LAST_DAY(ADD_MONTHS(TO_DATE('''||v_curr_day||'''),-1)),'||v_weekday_num||') + ('|| v_week_num || '-1) * 7) FROM DUAL'; --Added By Sathyarajan on 27-Jan-2012 ... Refer PR#PM00014537 (Weekend Actualization Plan issue)

     DBMS_OUTPUT.PUT_LINE(v_dyn_sql);
     EXECUTE IMMEDIATE v_dyn_sql INTO v_ACTL_SHPMT_EXPCT_DATE;
     DBMS_OUTPUT.PUT_LINE('Last Month Shipment Actualization Date is '||v_ACTL_SHPMT_EXPCT_DATE);

     UPDATE opt_shpmt_actl_frcst_plc SET ACTL_SHPMT_EXPCT_DATE = to_date(v_ACTL_SHPMT_EXPCT_DATE);
     COMMIT;
     DBMS_OUTPUT.PUT_LINE('Updating Last Month Actualization Date in table opt_shpmt_actl_frcst_plc finished.');

     IF to_date(v_curr_day) >= to_date(v_ACTL_SHPMT_EXPCT_DATE) THEN

     /* Remarked by Barbara on Mar 17th 2011 in Optima R11 as confirmed by Piotr no need to do additional check, begin
     --If the date set in Tx has been reached, we need to check whether Ax has fully loaded last month's shipment data

       --Query Last Working Day of Last Month
       DBMS_OUTPUT.PUT_LINE('Querying Last Working Day of Last Month...');
       v_dyn_sql := 'SELECT  trunc(last_day(add_months(''' || v_curr_day || ''',-1)) - decode(to_char(last_day(add_months(''' || v_curr_day || ''',-1)),''D''),1,2,7,1,0)) '
                   || CHR(10) || 'FROM DUAL';
       DBMS_OUTPUT.PUT_LINE(v_dyn_sql);
       EXECUTE IMMEDIATE v_dyn_sql INTO v_last_workday;
       DBMS_OUTPUT.PUT_LINE('Last Working Day of Last Month is '|| v_last_workday );

       --Check Shipment tabla whether it contains records on or later than the last working day
       DBMS_OUTPUT.PUT_LINE('Checking whether Ax has fully loaded shipment for last month...');
       v_dyn_sql :=  'SELECT COUNT(1) FROM opt_shpmt_fct PARTITION ( P' || to_char(to_date(v_last_workday),'YYYYMM') || ')'
                    || CHR(10) || ' Where SHPMT_DATE >= to_date(''' || v_last_workday || ''') and rownum < 2';
       DBMS_OUTPUT.PUT_LINE(v_dyn_sql);
       EXECUTE IMMEDIATE v_dyn_sql INTO v_row_cnt;

       IF v_row_cnt >= 1 THEN
       */
         DBMS_OUTPUT.PUT_LINE('Checking PASSED, updating Last Month Actualization Indicator in opt_shpmt_actl_frcst_plc table..');
         UPDATE opt_shpmt_actl_frcst_plc SET P1M_SHPMT_FULL_LOAD_IND = 'Y',
                                            P1M_ACTL_SHPMT_AVAIL_IND = 'Y';
         COMMIT;
         DBMS_OUTPUT.PUT_LINE('Last Month Actualization Indicator is Y now..');
       /*
       ELSE
         DBMS_OUTPUT.PUT_LINE('Checking FAILED,Last Month Actualization Indicator is set to be N');
       END IF;
       Remarked by Barbara on Mar 17th 2011 in Optima R11 as confirmed by Piotr no need to do additional check, end*/
     ELSE
       DBMS_OUTPUT.PUT_LINE('Last Month Actualization Date has not been reached.');
     END IF;
   ELSE
     DBMS_OUTPUT.PUT_LINE('Last Month Actualization Indicator is set to Y, no need to check again!');
   END IF;

EXCEPTION
    WHEN OTHERS THEN
    raise_application_error(-20003, 'Error:Check Shipment Actualization Date Error With '||SQLERRM);

END PRO_CHK_SHPMT_ACTL_DATE;

END OPT_SHPMT_LOAD_PKG;
/

