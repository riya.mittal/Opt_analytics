CREATE OR REPLACE PACKAGE ADWU_OPTIMA_LAEX.OPT_PRTTN_LOAD_PKG IS
  ------------------------------------------------------------------------------------
  -- Package      : opt_prttn_load_pkg                                               --
  -- Originator   : Hill Pan (zhi-shan.pan@hp.com)                                         --
  -- Author       : Hill Pan (zhi-shan.pan@hp.com)                                         --
  -- Date         : 28-Mar-2008                                                     --
  -- Purpose     : Loading data by partition exchange                       --
  ------------------------------------------------------------------------------------
  -- Change History                                                                 --
  -- Date        Programmer        Description                                      --
  -- ----------- ----------------- ------------------------------------------------ --
  -- 28-Mar-2008 Hill Pan           Initial Varsion                                  --
  --                                                                                --
  --                                                                                --
  ------------------------------------------------------------------------------------
  C_WRONG_PARMS_ERROR       NUMBER := -20000;
  C_STATS_FINAL_EST_PERCENT NUMBER := 5;

  -- constants
  -- Added by Myra 20090928 in R9. for tuning
  C_FINAL_TBL_STORAGE VARCHAR2(100) := 'PCTFREE 0 PCTUSED 0 INITRANS 1 MAXTRANS 255';
  -- Tablespace changed to OPTIMA01 for Exadata
  --V_TBLSPACE_NAME VARCHAR2(20) := 'TABLESPACE OPTIMA01';
  V_TBLSPACE_NAME VARCHAR2(50) := 'DEFAULT';
  V_INDEX         VARCHAR2(50) := 'DEFAULT';

  P_DOP number := 2;
  /*
    c_final_tbl_storage          varchar2(100) := 'PCTFREE 0 PCTUSED 0 INITRANS 1 MAXTRANS 255';
    c_ship_mth_agg_temp_tbl_name varchar2(30) := 'OPT_SHPMT_MTH_FCT_TMP';
    c_oldest_prttn_name          varchar2(30) := 'P000000';

    c_stats_final_est_percent number := 5;
    c_stats_temp_est_percent  number := 5;


    c_tbl_creat_error   number := -20001;
    c_tbl_ins_error     number := -20002;
    c_data_promo_error  number := -20003;
    c_drop_prttn_error  number := -20004;
  */
  PROCEDURE LOAD_BY_PARTITION_EXCHANGE(P_TBL_NAME  IN VARCHAR2,
                                       P_TYPE_CODE IN VARCHAR2,
                                       P_SRC_TBL   IN VARCHAR2 DEFAULT NULL,
									   V_REGN VARCHAR2 DEFAULT NULL);--SOX IMPLEMENTATION PROJECT
  PROCEDURE DUPLICATE_PRTTN_TABLE(P_TMPLT_TABLE IN VARCHAR2,
                                  P_NEW_TBL     IN VARCHAR2,
								  V_REGN VARCHAR2 DEFAULT NULL);--SOX IMPLEMENTATION PROJECT

  PROCEDURE LOAD_BY_PRTTN_EXCHG_WITH_SQL(P_TBL_NAME IN VARCHAR2,
                                         P_SQL      IN VARCHAR2,
										 V_REGN VARCHAR2 DEFAULT NULL);--SOX IMPLEMENTATION PROJECT

END OPT_PRTTN_LOAD_PKG;
/

