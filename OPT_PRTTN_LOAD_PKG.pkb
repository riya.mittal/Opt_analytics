CREATE OR REPLACE package body ADWU_OPTIMA_LAEX.OPT_PRTTN_LOAD_PKG is

  /**********************************************************************/
  /* name: pro_put_line                                                 */
  /* purpose: print line of text divided into 255-character chunks      */
  /* parameters:                                                        */
  /*   p_string - string to be printed                                  */
  /* author: bazyli blicharski                                          */
  /* originator: brian beckman                                          */
  /* version: 1.00 - initial version                                    */
  /*          1.01 - string cut according to new line marker            */
  /**********************************************************************/
  procedure pro_put_line(p_string in varchar2) is
    v_string_length number;
    v_string_offset number := 1;
    v_cut_pos       number;
    v_add           number;
  begin
    dbms_output.new_line;
    dbms_output.put_line(to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
    v_string_length := length(p_string);

    -- loop thru the string by 255 characters and output each chunk
    while v_string_offset < v_string_length loop
      v_cut_pos := nvl(instr(p_string, chr(10), v_string_offset), 0) -
                   v_string_offset;
      if v_cut_pos < 0 or v_cut_pos >= 255 then
        v_cut_pos := 255;
        v_add     := 0;
      else
        v_add := 1;
      end if;
      dbms_output.put_line(substr(p_string, v_string_offset, v_cut_pos));
      v_string_offset := v_string_offset + v_cut_pos + v_add;
    end loop;
  end;

  /************************************************************************************/
  /* name: drop_table (internal procedure                                             */
  /* purpose: drop table passed in the parameter (if exists)                          */
  /* parameters:                                                                      */
  /*   p_tbl_name - table to be removed                                               */
  /* author: bazyli blicharski                                                        */
  /* version: 1.00 - initial version                                                  */
  /************************************************************************************/
  procedure drop_table(p_tbl_name in varchar2) is
    v_cnt number;
    v_sql varchar2(32767);
  begin
    select count(1)
      into v_cnt
      from user_tables
     where table_name = p_tbl_name;
    if v_cnt > 0 then
      v_sql := 'DROP TABLE ' || p_tbl_name || ' purge';
      pro_put_line('SQL:' || chr(10) || '> ' ||
                   replace(v_sql, chr(10), chr(10) || '> '));
      execute immediate v_sql;
      pro_put_line('Table dropped.');
    end if;
  end;

  /************************************************************************************/
  /* name: getclob                                            */
  /* purpose: get contest of clob and fill in varchar2                          */
  /* parameters:                                                                      */
  /*   c - source clob                                               */
  /* Reture value                                                       */
  /*   contest in varchar2 type                                                  */
  /* author: Eric Hu                                                        */
  /* version: 1.00 - initial version                                                  */
  /************************************************************************************/
  function getclob(c clob) return varchar2 is
    buffer   varchar2(2000);
    v_buffer varchar2(32767);
    n_size   number := dbms_lob.getlength(c);
    pos      number := 0;
  begin
    dbms_output.put_line(n_size);
    while (n_size > 0) loop
      buffer   := dbms_lob.substr(c, 2000, 1 + pos * 2000);
      pos      := pos + 1;
      n_size   := n_size - 2000;
      v_buffer := v_buffer || buffer;
    end loop;
    return v_buffer;
  end;

  /************************************************************************************/
  /* name: duplicate_prttn_table                                            */
  /* purpose: duplicate a new table according to template table's definition                          */
  /* parameters:                                                                      */
  /*   p_tmplt_table - template table                                               */
  /*   p_new_tbl - new table                                               */
  /* author: Hill Pan                                                        */
  /* version: 1.00 - initial version                                                  */
  /************************************************************************************/

  procedure duplicate_prttn_table(p_tmplt_table in varchar2,
                                  p_new_tbl     in varchar2,
								  V_REGN VARCHAR2 DEFAULT NULL) is --SOX IMPLEMENTATION PROJECT
    c_tab_def clob := '';
    c_tab_def_new clob := '';
    v_tab_def varchar2(32767) := '';
    p_user    varchar2(50);
  begin
    --drop table with the same name of new table if exist
    drop_table(p_new_tbl);
    select username into p_user from user_users where rownum = 1;
    --Get DDL of template table
    select trim(dbms_metadata.get_ddl('TABLE', p_tmplt_table, p_user))
      into c_tab_def
      from dual;

    --v_tab_def := getclob(c_tab_def);
    --v_tab_def := replace(v_tab_def,
    c_tab_def_new := replace(c_tab_def,
                         '"' || p_tmplt_table || '"',
                         '"' || p_new_tbl || '"');
    dbms_output.put_line(length(c_tab_def_new));
    pro_put_line('Length of SQL statement: ' || length(c_tab_def_new));
    --pro_put_line(v_tab_def);
    --Create table
    --execute immediate v_tab_def;
    execute immediate c_tab_def_new;

    pro_put_line('table name: '||p_tmplt_table||' new table name:'||p_new_tbl);
    pro_put_line('New Table : '||p_new_tbl||' Created');
    opt_pkg_prttn_util.pro_create_temp_index(p_new_tbl, p_tmplt_table,'L',V_REGN);--SOX IMPLEMENTATION PROJECT

  exception
    when others then
      --Raise error
      pro_put_line(sqlerrm);
      raise_application_error(-20081,
                              'ERROR!!! When duplicate a source table for  ' ||
                              p_tmplt_table);
  end duplicate_prttn_table;


 /************************************************************************************/
  /* name: duplicate_prttn_idx                                            */
  /* purpose: duplicate indexes for new table according to template table's definition                          */
  /* parameters:                                                                      */
  /*   p_tmplt_table - template table                                               */
  /*   p_new_tbl - new table                                               */
  /* author: Bodhi Wang                                                        */
  /* version: 1.00 - initial version                                                  */
  /************************************************************************************/

  procedure duplicate_prttn_idx(p_tmplt_table in varchar2,
                                  p_new_tbl     in varchar2) is
  v_tab_def varchar2(32767) := '';
  begin
    for c in (SELECT regexp_replace(DBMS_METADATA.GET_DDL('INDEX',u.index_name),
                                    p_tmplt_table, p_new_tbl) as idx_def
                     FROM USER_INDEXES u
                     WHERE u.TABLE_NAME=p_tmplt_table
              )
    LOOP
         v_tab_def := getclob(c.idx_def);
         pro_put_line(v_tab_def);
         execute immediate v_tab_def;
    end loop;

  exception
    when others then
      --Raise error
      pro_put_line(sqlerrm);
      raise_application_error(-20081,
                              'ERROR!!! When duplicate indexes of ' ||
                              p_tmplt_table);
  end duplicate_prttn_idx;

  /************************************************************************************/
  /* name: opt_promote_data                                            */
  /* purpose: exchange partitions of two tables                           */
  /* parameters:                                                                      */
  /*   p_src_tbl_name - source table                                               */
  /*   p_tgt_tbl_name - destination table                                               */
  /*   p_tmp_tbl_name - temporary table                                               */
  /* author: Hill Pan                                                        */
  /* version: 1.00 - initial version                                                  */
  /************************************************************************************/
  procedure opt_promote_data(p_src_tbl_name in varchar2,
                             p_tgt_tbl_name in varchar2,
                             p_tmp_tbl_name in out varchar2,
							 V_REGN VARCHAR2 DEFAULT NULL) is--SOX IMPLEMENTATION PROJECT
    v_vc2_tbl_sub_prttn_str   varchar2(100);
    v_num_tbl_sub_prttn_count number(3);
    v_num_min_fy_date_skid    number;
    v_vc2_high_value          varchar2(20);
    v_vc2_max_prttn           user_tab_partitions.partition_name%type;
  begin
    --Create temp table
    opt_pkg_prttn_util.pro_create_temp_tbl(p_tgt_tbl_name, p_tmp_tbl_name,V_REGN);--SOX IMPLEMENTATION PROJECT

    -- Alter all indexes on stage table unusable
    -- Added by Myra 20090928 in R9 for compression
    opt_pkg_util.pro_put_line('Disble index on temp table');
    opt_gather_stats.opt_unusable_ind(p_tmp_tbl_name);


    -- Check if subpartitioned
    opt_pkg_prttn_util.pro_check_tbl_sub_prttn(p_tgt_tbl_name,
                                               v_vc2_tbl_sub_prttn_str,
                                               v_num_tbl_sub_prttn_count);

    -- Get maxvalue partition name as this should never be exchanged
    select partition_name
      into v_vc2_max_prttn
      from user_tab_partitions
     where table_name = p_src_tbl_name
       and partition_position =
           (select max(partition_position)
              from user_tab_partitions
             where table_name = p_src_tbl_name);

    if (v_num_tbl_sub_prttn_count = 0) then
      -- Table is partitioned
      -- Loop thru all partitions in the source table and exchange each one
      for rec in (select partition_name
                    from user_tab_partitions
                   where table_name = p_src_tbl_name
                     and partition_name != v_vc2_max_prttn) loop
        opt_pkg_util.pro_put_line('Exchanging partition ' ||
                                  rec.partition_name);
        opt_pkg_prttn_util.pro_exchange_tbl_with_prttn(p_src_tbl_name,
                                                       rec.partition_name,
                                                       p_tmp_tbl_name,
                                                       'N');
        begin
          opt_pkg_prttn_util.pro_exchange_tbl_with_prttn(p_tgt_tbl_name,
                                                         rec.partition_name,
                                                         p_tmp_tbl_name,
                                                         'N');
        exception
          when others then
            -- If exchange between temp table and dest table fails, need to exchange back to source
            opt_pkg_util.pro_put_line(sqlerrm);
            opt_pkg_util.pro_put_line('ERROR!!!  Need to preserve data');
            opt_pkg_util.pro_put_line('Exchanging from ' || p_tmp_tbl_name ||
                                      ' back to ' || p_src_tbl_name || '.' ||
                                      rec.partition_name);
            opt_pkg_prttn_util.pro_exchange_tbl_with_prttn(p_src_tbl_name,
                                                           rec.partition_name,
                                                           p_tmp_tbl_name,
                                                           'N');
            raise_application_error(-20080,
                                    'ERROR!!! Exchanging between ' ||
                                    p_tmp_tbl_name || ' and ' ||
                                    p_tgt_tbl_name);
        end;
        -- Need to truncate temp table after exchange
        opt_pkg_util.pro_truncate_tbl(p_tmp_tbl_name);
      end loop;
    else
      -- Table is subpartitioned
      --Optima11, BIP-wv3-1, 07-July-2011, Daniel.Qin, change: only exchange subpartitions which
      --high_value greater than sys_context('OPTIMA','OPT_MIN_FY_DATE_SKID') or have no sys_context attribute, Begin
      v_num_min_fy_date_skid := TO_NUMBER(SYS_CONTEXT('OPTIMA','OPT_MIN_FY_DATE_SKID'));
      for rec in (select s.subpartition_name,p.high_value
                    from user_tab_subpartitions s,
                         user_tab_partitions p
                   where s.table_name = p_src_tbl_name
                     and p.table_name = p_src_tbl_name
                     and s.partition_name = p.partition_name
                     and p.partition_name != v_vc2_max_prttn) loop
        v_vc2_high_value := rec.high_value;
        if (v_num_min_fy_date_skid IS NULL) or
           (v_num_min_fy_date_skid IS NOT NULL and
            to_number(v_vc2_high_value) > v_num_min_fy_date_skid)
        then
          opt_pkg_util.pro_put_line('Exchanging subpartition ' ||
                                    rec.subpartition_name);
          opt_pkg_prttn_util.pro_exchange_tbl_with_prttn(p_src_tbl_name,
                                                         rec.subpartition_name,
                                                         p_tmp_tbl_name,
                                                         'Y');
          begin
            opt_pkg_prttn_util.pro_exchange_tbl_with_prttn(p_tgt_tbl_name,
                                                           rec.subpartition_name,
                                                           p_tmp_tbl_name,
                                                           'Y');
          exception
          when others then
            -- If exchange between temp table and dest table fails, need to exchange back to source
            opt_pkg_util.pro_put_line(sqlerrm);
            opt_pkg_util.pro_put_line('ERROR!!!  Need to preserve data');
            opt_pkg_util.pro_put_line('Exchanging from ' || p_tmp_tbl_name ||
                                      ' back to ' || p_src_tbl_name);
            opt_pkg_prttn_util.pro_exchange_tbl_with_prttn(p_src_tbl_name,
                                                           rec.subpartition_name,
                                                           p_tmp_tbl_name,
                                                           'Y');
            raise_application_error(-20080,
                                    'ERROR!!! Exchanging between ' ||
                                    p_tmp_tbl_name || ' and ' ||
                                    p_tgt_tbl_name);
          end;
        -- Need to truncate temp table after exchange
        opt_pkg_util.pro_truncate_tbl(p_tmp_tbl_name);
        end if;
      --Optima11, BIP-wv3-1, 07-July-2011, Daniel.Qin, change: only exchange subpartitions which
      --high_value greater than sys_context('OPTIMA','OPT_MIN_FY_DATE_SKID') or have no sys_context attribute, End.
      end loop;

    end if;
    -- drop temporary table
    drop_table(p_tmp_tbl_name);

  exception
    when others then
      opt_pkg_util.pro_put_line(sqlerrm);
      opt_pkg_util.pro_put_line('ERROR!!!  when promote data to ' ||
                                p_tgt_tbl_name);
      RAISE;
  end opt_promote_data;

  /************************************************************************************/
  /* name: load_by_partition_exchange                                                 */
  /* purpose: load data by partition exchange                                         */
  /* parameters:                                                                      */
  /*   p_tbl_name - table to be loaded                                                */
  /* author: Hill Pan                                                                 */
  /* version: 1.00 - initial version                                                  */
  /* OPTIMA11,CR894, 16-Dec-2010,Martin, Use 'MERGE' SQL statement to replace         */
  /*                                     all 'UPDATE/+ BYPASS_UJVC /' SQL statement.  */
  /* OPTIMA11,BIP-Wv3-1 Incremental load Fct table,7-July-2011, verify partition      */
  /*                                     FY window and setup sys_context for job.     */
  /************************************************************************************/
  procedure load_by_partition_exchange(p_tbl_name in varchar2,p_type_code in varchar2, p_src_tbl in varchar2 default null,V_REGN VARCHAR2 DEFAULT NULL) is--SOX IMPLEMENTATION PROJECT

    v_sql          varchar2(32767);
    v_start_date   date := sysdate;
    l_tgt_tbl_name varchar2(30);
    l_src_tbl_name varchar2(30);
    l_tmp_tbl_name varchar2(30);
    l_prcss_name   varchar2(32767);
    l_step_info    varchar2(32767);
    v_cols         varchar2(32667);
    v_vc2_s_ind    varchar2(1);
    v_vc2_t_ind    varchar2(1);
    v_row_count       number;
    v_partition_diff_count number;
    v_partition_name varchar2(30);
  begin
    -- parameter control
    pro_put_line('Checking input parameters:' || chr(10) ||
                 '  p_tbl_name = ' || p_tbl_name || chr(10) ||
                 ' p_type_code ='|| p_type_code);
    if p_tbl_name is null then
      pro_put_line('Error: wrong input parameters!');
      raise_application_error(c_wrong_parms_error,
                              'Error: wrong input parameters!');
    end if;
    l_tgt_tbl_name := upper(p_tbl_name);
    l_prcss_name := l_tgt_tbl_name || '_' || upper(p_type_code);

    if p_src_tbl is not null then
      pro_put_line('Information: Source table is identified as '|| p_src_tbl);
      l_src_tbl_name := upper(p_src_tbl);
      -- Modified by Myra 20091015 rebuild index after promote data and partition exchange.
      opt_gather_stats.opt_unusable_ind(l_src_tbl_name);

      --Optima11, BIP-wv3-1, 07-July-2011, Daniel.Qin, verify fy window and set sys_context Begin
      --check the consistency of srce table and target table load mode
      select max(case when tbl_name = l_src_tbl_name then NVL(FY_INCRM_IND,'F') else '' end),
             max(case when tbl_name = l_tgt_tbl_name then NVL(FY_INCRM_IND,'F') else '' end)
        into v_vc2_s_ind,v_vc2_t_ind
        from opt_prcss_mtdta_prc
       where ((scrpt_desc = 'opt_load_stage_data.ksh' and tbl_name = l_src_tbl_name)
              or
              (scrpt_desc = 'opt_prttn_load.ksh' and tbl_name = l_tgt_tbl_name))
         and exec_type_code = 'DLY';

      if v_vc2_s_ind = 'I' and v_vc2_t_ind <> 'I'
      then
        pro_put_line('Error: Load mode between Srce and Target table are diffrent!');
        raise_application_error(20082,
                              'Error: Load mode between Srce and Target table are diffrent!');
      end if;

      select count(1) into v_row_count
        from user_tab_subpartitions
       where table_name = l_src_tbl_name;

      if v_row_count>0
      then
        --verify fy window
        opt_pkg_prttn_util.pro_verify_fy_window_prttn(l_tgt_tbl_name,l_src_tbl_name);
        --setup for sys_context
        opt_cntxt_sttng_prc(upper(l_tgt_tbl_name),'opt_prttn_load.ksh');
        --Optima11, BIP-wv3-1, 07-July-2011, Daniel.Qin, verify fy window and set sys_context End
      end if;
    else
       pro_put_line('Information: Source table is not identified, will create a temp Source table.');
       l_src_tbl_name := l_tgt_tbl_name || '_TEMP';

    --Added by Bodhi. Mar 8, 2009.
    pro_put_line('Source table name :'|| p_src_tbl);

    --Drop temporary source table
    drop_table(l_src_tbl_name);

    -- Start Added by HP for PR#PM00042908
    IF l_tgt_tbl_name = 'OPT_BRAND_BASLN_IFCT' THEN
       l_step_info := 'Checking Partition List for Target Table...';
       pro_put_line('Checking Partition List for Target Table '|| l_tgt_tbl_name || '(Begin)');
       opt_pkg_prttn_util.pro_verify_fy_window_prttn(l_tgt_tbl_name);
       pro_put_line('Checking Partition List for Target Table '|| l_tgt_tbl_name || '(End)');
    END IF;
    -- End Added by HP for PR#PM00042908
    
    --Create source table
    l_step_info := 'Creating temp source table...';
    duplicate_prttn_table(l_tgt_tbl_name, l_src_tbl_name,V_REGN);--SOX IMPLEMENTATION PROJECT

    -- Modified by Myra 20091015 rebuild index after promote data and partition exchange.
    opt_gather_stats.opt_unusable_ind(l_src_tbl_name);

/*    --Truncate source table
    l_step_info := 'Truncating temp source table...';
    opt_pkg_util.pro_truncate_tbl(l_src_tbl_name);*/

    --Get the column name of source table
    l_step_info := 'Get the column name of source table...';
    v_cols := '';
    for c in (
      select column_name
      from user_tab_columns
      where table_name = l_src_tbl_name
      order by column_id
    )
    loop
      if v_cols is not null then
        v_cols := v_cols || ',';
      end if;
      v_cols := v_cols || c.column_name;
    end loop;

    -- Add by Luke, for run the pre_process to tuning the OPT_SHPMT_ACTL_FRCST_FCT
    l_step_info := 'Run Pre_process of OPT_SHPMT_ACTL_FRCST_FCT...';
    IF l_tgt_tbl_name = 'OPT_SHPMT_ACTL_FRCST_FCT' THEN
       pro_put_line('Pre_process of '|| l_tgt_tbl_name || '(Begin)');
       opt_pre_prcss.pre_extrt_actl_frcst(V_REGN);--SOX IMPLEMENTATION PROJECT
       pro_put_line('Pre_process of '|| l_tgt_tbl_name || '(End)');
    END IF;

    -- manually extend the workarea size for table OPT_BRAND_BASLN_IFCT by leo, on Dec 29 2010
    -- manually extend the workarea size for table OPT_HYPER_PLAN_FCT by leo, on Jan 11, 2011
    --remove current workaround for dpwt issue after oracle patch applied by Leo on Feb 14, 2011 - begin
      /* IF L_TGT_TBL_NAME = 'OPT_BRAND_BASLN_IFCT' OR
         L_TGT_TBL_NAME = 'OPT_HYPER_PLAN_FCT' THEN
        EXECUTE IMMEDIATE 'alter session set workarea_size_policy = manual';
        EXECUTE IMMEDIATE 'alter session set hash_area_size = 2147400000';
        EXECUTE IMMEDIATE 'alter session set sort_area_size = 2147400000';
      END IF; */
    --remove current workaround for dpwt issue after oracle patch applied by Leo on Feb 14, 2011 - end


    --Insert data into source table
    --Update in R8, add the PARALLEL(4) hint when insert temp table, this is for performance tuning.
    l_step_info := 'Inserting data into temp source table...';
    select 'insert /*+ APPEND PARALLEL(pt,4) */ into ' || l_src_tbl_name ||' pt  select
           ' || v_cols || chr(10) ||
           'from (
           ' || sql_stmt_desc || ')'
      into v_sql
      from opt_extrt_ctrl_etl
     where upper(qry_name) = l_prcss_name;

    pro_put_line(v_sql);

    --Optima11, BIT F02, 05-May-2011, Daniel, enable parallel DML for job OPT_HYPER_PLAN_FCT_DLY,Begin
    IF upper(l_prcss_name) = 'OPT_HYPER_PLAN_FCT_DLY' THEN
        EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DML';
    END IF;

    execute immediate v_sql;
    commit;

    IF upper(l_prcss_name) = 'OPT_HYPER_PLAN_FCT_DLY' THEN
        EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DML';
    END IF;
    --Optima11, BIT F02, 05-May-2011, Daniel, enable parallel DML for job OPT_HYPER_PLAN_FCT_DLY, End

    -- Add by Luke, For run the Post process for update some measure in OPT_SHPMT_ACTL_FRCST_FCT
    -- This is for performance tuning
    l_step_info := 'Run Post_process of OPT_SHPMT_ACTL_FRCST_FCT...';
    IF l_tgt_tbl_name = 'OPT_SHPMT_ACTL_FRCST_FCT' THEN
       -- gather statistics for all involved partitions
       pro_put_line('Starting gathering statistics on ' || l_src_tbl_name ||
                    ' table...');
       -- Changed to use Optima Package for statistics gathering
     -- Simon 16:51 2009-9-25
     opt_gather_stats.optima_stats_tbl(l_src_tbl_name);
     pro_put_line('Gathering statistics on ' || l_src_tbl_name || ' table...Done');

       -- Gather the table OPT_CAL_MASTR_DIM_TP
       pro_put_line('Starting gathering statistics on OPT_CAL_MASTR_TP_DIM table...');
       opt_gather_stats.optima_stats_tbl('OPT_CAL_MASTR_TP_DIM', 'Y');
       pro_put_line('Starting gathering statistics on OPT_CAL_MASTR_TP_DIM table gathered');

       pro_put_line('Post_process of '|| l_src_tbl_name || '(Begin)');
       -- Update the EFSR relate measures
            v_sql    :=
                  'MERGE INTO '|| l_src_tbl_name|| ' trgt
                       USING opt_efsr_tdads srce
                          ON (trgt.acct_prmtn_skid = srce.acct_prmtn_skid
                          AND trgt.bus_unit_skid = srce.bus_unit_skid
                          AND trgt.date_skid = srce.date_skid
                          AND trgt.fy_date_skid = srce.fy_date_skid
                          AND trgt.prod_skid = srce.prod_skid
                          AND (srce.incrm_su_amt <> 0
                               OR  srce.incrm_giv_amt <> 0
                               OR  srce.incrm_niv_amt <> 0
                               OR  srce.incrm_buom_amt <> 0))
                  WHEN MATCHED
                  THEN
                     UPDATE SET trgt.incrm_in_efsr_su_amt = srce.INCRM_SU_AMT,
                                trgt.incrm_in_efsr_giv_amt = srce.INCRM_GIV_AMT,
                                trgt.incrm_in_efsr_niv_amt = srce.INCRM_NIV_AMT,
                                trgt.incrm_in_efsr_case_amt = srce.INCRM_BUOM_AMT,
                                trgt.tot_in_efsr_su_amt = trgt.estmt_basln_su_amt + srce.incrm_su_amt,
                                trgt.tot_in_efsr_giv_amt = trgt.estmt_basln_giv_amt + srce.incrm_giv_amt,
                                trgt.tot_in_efsr_niv_amt = trgt.estmt_basln_niv_amt + srce.incrm_niv_amt,
                                trgt.tot_in_efsr_case_amt = trgt.estmt_basln_case_amt + srce.incrm_buom_amt';

/*  Old SQL       v_sql := 'UPDATE  /*+ BYPASS_UJVC /
          (SELECT ACTL_FRCST.ACCT_PRMTN_SKID AS ACCT_PRMTN_SKID,
                  ACTL_FRCST.BUS_UNIT_SKID   AS BUS_UNIT_SKID,
                  ACTL_FRCST.DATE_SKID       AS DATE_SKID,
                  ACTL_FRCST.FY_DATE_SKID    AS FY_DATE_SKID,
                  ACTL_FRCST.PROD_SKID       AS PROD_SKID,
                  ACTL_FRCST.INCRM_IN_EFSR_SU_AMT,
                  ACTL_FRCST.INCRM_IN_EFSR_GIV_AMT,
                  ACTL_FRCST.INCRM_IN_EFSR_NIV_AMT,
                  ACTL_FRCST.INCRM_IN_EFSR_CASE_AMT,
                  ACTL_FRCST.TOT_IN_EFSR_SU_AMT,
                  ACTL_FRCST.TOT_IN_EFSR_GIV_AMT,
                  ACTL_FRCST.TOT_IN_EFSR_NIV_AMT,
                  ACTL_FRCST.TOT_IN_EFSR_CASE_AMT,
                  EFSR.INCRM_SU_AMT AS T_INCRM_IN_EFSR_SU_AMT,
                  EFSR.INCRM_GIV_AMT AS T_INCRM_IN_EFSR_GIV_AMT,
                  EFSR.INCRM_NIV_AMT AS T_INCRM_IN_EFSR_NIV_AMT,
                  EFSR.INCRM_BUOM_AMT AS T_INCRM_IN_EFSR_CASE_AMT,
                  ACTL_FRCST.ESTMT_BASLN_SU_AMT + EFSR.INCRM_SU_AMT AS T_TOT_IN_EFSR_SU_AMT,
                  ACTL_FRCST.ESTMT_BASLN_GIV_AMT + EFSR.INCRM_GIV_AMT AS T_TOT_IN_EFSR_GIV_AMT,
                  ACTL_FRCST.ESTMT_BASLN_NIV_AMT + EFSR.INCRM_NIV_AMT AS T_TOT_IN_EFSR_NIV_AMT,
                  ACTL_FRCST.ESTMT_BASLN_CASE_AMT + EFSR.INCRM_BUOM_AMT AS T_TOT_IN_EFSR_CASE_AMT
             FROM ' || l_src_tbl_name || ' ACTL_FRCST,
                  OPT_EFSR_TDADS EFSR
            WHERE ACTL_FRCST.ACCT_PRMTN_SKID = EFSR.ACCT_PRMTN_SKID
              AND ACTL_FRCST.BUS_UNIT_SKID = EFSR.BUS_UNIT_SKID
              AND ACTL_FRCST.DATE_SKID = EFSR.DATE_SKID
              AND ACTL_FRCST.FY_DATE_SKID = EFSR.FY_DATE_SKID
              AND ACTL_FRCST.PROD_SKID = EFSR.PROD_SKID
              AND (EFSR.INCRM_SU_AMT <> 0 OR EFSR.INCRM_GIV_AMT <> 0
                   OR EFSR.INCRM_NIV_AMT <> 0 OR EFSR.INCRM_BUOM_AMT <> 0)) O
             SET O.INCRM_IN_EFSR_SU_AMT = O.T_INCRM_IN_EFSR_SU_AMT,
                 O.INCRM_IN_EFSR_GIV_AMT = O.T_INCRM_IN_EFSR_GIV_AMT,
                 O.INCRM_IN_EFSR_NIV_AMT = O.T_INCRM_IN_EFSR_NIV_AMT,
                 O.INCRM_IN_EFSR_CASE_AMT = O.T_INCRM_IN_EFSR_CASE_AMT,
                 O.TOT_IN_EFSR_SU_AMT = O.T_TOT_IN_EFSR_SU_AMT,
                 O.TOT_IN_EFSR_GIV_AMT = O.T_TOT_IN_EFSR_GIV_AMT,
                 O.TOT_IN_EFSR_NIV_AMT = O.T_TOT_IN_EFSR_NIV_AMT,
                 O.TOT_IN_EFSR_CASE_AMT = O.T_TOT_IN_EFSR_CASE_AMT';
*/
       pro_put_line(v_sql);
       execute immediate v_sql;
       commit;

       -- Update the YA(Year ago) relate measures
      -- Tuning by Myra 20090929, add filter to Calendar dim for performance improve
            v_sql    :=
               'MERGE INTO '||l_src_tbl_name||' trgt
                       USING (SELECT cal.cal_mastr_skid,
                                     ya_actl.acct_skid,
                                     ya_actl.bus_unit_skid,
                                     ya_actl.prod_skid,
                                     ya_actl.vol_su_amt,
                                     ya_actl.giv_amt,
                                     ya_actl.niv_amt,
                                     ya_actl.vol_buom_amt
                                FROM prmtn_shpmt_mth_tdads ya_actl
                                   , cal_mastr_dim cal
                               WHERE cal.day_date BETWEEN TO_DATE(''20000101'', ''YYYYMMDD'') AND ADD_MONTHS(TRUNC(SYSDATE), 24)
                                 AND ya_actl.mth_start_date = ADD_MONTHS(CAL.MTH_START_DATE, -12)) srce
                          ON (trgt.date_skid = srce.cal_mastr_skid
                          AND trgt.acct_prmtn_skid = srce.acct_skid
                          AND trgt.bus_unit_skid = srce.bus_unit_skid
                          AND trgt.prod_skid = srce.prod_skid)
                  WHEN MATCHED
                  THEN
                     UPDATE SET trgt.actl_su_ya_amt = srce.vol_su_amt
                              , trgt.actl_giv_ya_amt = srce.giv_amt
                              , trgt.actl_niv_ya_amt = srce.niv_amt
                              , trgt.actl_case_ya_amt = srce.vol_buom_amt';

/*  Old SQL   v_sql := 'UPDATE /*+ BYPASS_UJVC /
                 (SELECT ACTL_FRCST.ACCT_PRMTN_SKID AS ACCT_PRMTN_SKID,
                        ACTL_FRCST.BUS_UNIT_SKID AS BUS_UNIT_SKID,
                        ACTL_FRCST.DATE_SKID AS DATE_SKID,
                        ACTL_FRCST.FY_DATE_SKID AS FY_DATE_SKID,
                        ACTL_FRCST.PROD_SKID AS PROD_SKID,
                        ACTL_FRCST.ACTL_SU_YA_AMT,
                        ACTL_FRCST.ACTL_GIV_YA_AMT,
                        ACTL_FRCST.ACTL_NIV_YA_AMT,
                        ACTL_FRCST.ACTL_CASE_YA_AMT,
                        YA_ACTL.VOL_SU_AMT,
                        YA_ACTL.GIV_AMT,
                        YA_ACTL.NIV_AMT,
                        YA_ACTL.VOL_BUOM_AMT
                   FROM ' || l_src_tbl_name || ' ACTL_FRCST,
                        PRMTN_SHPMT_MTH_TDADS YA_ACTL,
                        CAL_MASTR_DIM CAL
                  WHERE ACTL_FRCST.DATE_SKID = CAL.CAL_MASTR_SKID
                    AND ACTL_FRCST.ACCT_PRMTN_SKID = YA_ACTL.ACCT_SKID
                    AND ACTL_FRCST.BUS_UNIT_SKID = YA_ACTL.BUS_UNIT_SKID
                    AND ACTL_FRCST.PROD_SKID = YA_ACTL.PROD_SKID
                    AND CAL.DAY_DATE BETWEEN TO_DATE(''20000101'', ''YYYYMMDD'') AND ADD_MONTHS(TRUNC(SYSDATE), 24)
                    AND YA_ACTL.MTH_START_DATE = ADD_MONTHS(CAL.MTH_START_DATE, -12)) O
                    SET O.ACTL_SU_YA_AMT   = O.VOL_SU_AMT,
                        O.ACTL_GIV_YA_AMT  = O.GIV_AMT,
                        O.ACTL_NIV_YA_AMT  = O.NIV_AMT,
                        O.ACTL_CASE_YA_AMT = O.VOL_BUOM_AMT';
*/
       pro_put_line(v_sql);
       execute immediate v_sql;
       commit;

       -- Post Insert the EFSR data which not exsit in SVP
/*       v_sql := 'insert \*+ APPEND *\ into ' || l_src_tbl_name || ' select
                ' || v_cols || chr(10) ||
                'from (
                SELECT ACCT_PRMTN_SKID,
                 BUS_UNIT_SKID,
                 DATE_SKID,
                 FY_DATE_SKID,
                 PROD_SKID,
                 0 AS BASLN_SU_AMT,
                 0 AS BASLN_GIV_AMT,
                 0 AS BASLN_NIV_AMT,
                 0 AS BASLN_CASE_AMT,
                 0 AS ESTMT_BASLN_SU_AMT,
                 0 AS ESTMT_BASLN_GIV_AMT,
                 0 AS ESTMT_BASLN_NIV_AMT,
                 0 AS ESTMT_BASLN_CASE_AMT,
                 0 AS INCRM_IN_SU_AMT,
                 0 AS INCRM_IN_GIV_AMT,
                 0 AS INCRM_IN_NIV_AMT,
                 0 AS INCRM_IN_CASE_AMT,
                 0 AS TOT_IN_SU_AMT,
                 0 AS TOT_IN_GIV_AMT,
                 0 AS TOT_IN_NIV_AMT,
                 0 AS TOT_IN_CASE_AMT,
                 0 AS ACTL_SU_AMT,
                 0 AS ACTL_GIV_AMT,
                 0 AS ACTL_NIV_AMT,
                 0 AS ACTL_CASE_AMT,
                 0 AS CURR_TOT_SU_AMT,
                 0 AS CURR_TOT_GIV_AMT,
                 0 AS CURR_TOT_NIV_AMT,
                 0 AS CURR_TOT_CASE_AMT,
                 0 AS ACTL_SU_YA_AMT,
                 0 AS ACTL_GIV_YA_AMT,
                 0 AS ACTL_NIV_YA_AMT,
                 0 AS ACTL_CASE_YA_AMT,
                 INCRM_SU_AMT AS INCRM_IN_EFSR_SU_AMT,
                 INCRM_GIV_AMT AS INCRM_IN_EFSR_GIV_AMT,
                 INCRM_NIV_AMT AS INCRM_IN_EFSR_NIV_AMT,
                 INCRM_BUOM_AMT AS INCRM_IN_EFSR_CASE_AMT,
                 INCRM_SU_AMT AS TOT_IN_EFSR_SU_AMT,
                 INCRM_GIV_AMT AS TOT_IN_EFSR_GIV_AMT,
                 INCRM_NIV_AMT AS TOT_IN_EFSR_NIV_AMT,
                 INCRM_BUOM_AMT AS TOT_IN_EFSR_CASE_AMT
            FROM OPT_EFSR_TDADS EFSR
           WHERE (INCRM_SU_AMT <> 0 OR INCRM_GIV_AMT <> 0 OR
                 INCRM_NIV_AMT <> 0 OR INCRM_BUOM_AMT <> 0)
             AND NOT EXISTS
                     (SELECT ''X''
                        FROM ' || l_src_tbl_name || ' ACTL_FRCST
                       WHERE ACTL_FRCST.ACCT_PRMTN_SKID = EFSR.ACCT_PRMTN_SKID
                         AND ACTL_FRCST.DATE_SKID = EFSR.DATE_SKID
                         AND ACTL_FRCST.PROD_SKID = EFSR.PROD_SKID))';
*/
       -- tuning by Myra on 20090926, implement parallel (4), and rewrite sql to use 'not exists'. for performance improve
        -- Commented Insert statement by venkatesh-HPE - PR#PM00049157
       --v_sql := 'insert /*+ APPEND  PARALLEL(4)*/ into ' || l_src_tbl_name || ' select
         /*       ' || v_cols || chr(10) ||
                'from (
                SELECT ACCT_PRMTN_SKID,
                 BUS_UNIT_SKID,
                 DATE_SKID,
                 FY_DATE_SKID,
                 PROD_SKID,
                 0 AS BASLN_SU_AMT,
                 0 AS BASLN_GIV_AMT,
                 0 AS BASLN_NIV_AMT,
                 0 AS BASLN_CASE_AMT,
                 0 AS ESTMT_BASLN_SU_AMT,
                 0 AS ESTMT_BASLN_GIV_AMT,
                 0 AS ESTMT_BASLN_NIV_AMT,
                 0 AS ESTMT_BASLN_CASE_AMT,
                 0 AS INCRM_IN_SU_AMT,
                 0 AS INCRM_IN_GIV_AMT,
                 0 AS INCRM_IN_NIV_AMT,
                 0 AS INCRM_IN_CASE_AMT,
                 0 AS TOT_IN_SU_AMT,
                 0 AS TOT_IN_GIV_AMT,
                 0 AS TOT_IN_NIV_AMT,
                 0 AS TOT_IN_CASE_AMT,
                 0 AS ACTL_SU_AMT,
                 0 AS ACTL_GIV_AMT,
                 0 AS ACTL_NIV_AMT,
                 0 AS ACTL_CASE_AMT,
                 0 AS CURR_TOT_SU_AMT,
                 0 AS CURR_TOT_GIV_AMT,
                 0 AS CURR_TOT_NIV_AMT,
                 0 AS CURR_TOT_CASE_AMT,
                 0 AS ACTL_SU_YA_AMT,
                 0 AS ACTL_GIV_YA_AMT,
                 0 AS ACTL_NIV_YA_AMT,
                 0 AS ACTL_CASE_YA_AMT,
                 INCRM_SU_AMT AS INCRM_IN_EFSR_SU_AMT,
                 INCRM_GIV_AMT AS INCRM_IN_EFSR_GIV_AMT,
                 INCRM_NIV_AMT AS INCRM_IN_EFSR_NIV_AMT,
                 INCRM_BUOM_AMT AS INCRM_IN_EFSR_CASE_AMT,
                 INCRM_SU_AMT AS TOT_IN_EFSR_SU_AMT,
                 INCRM_GIV_AMT AS TOT_IN_EFSR_GIV_AMT,
                 INCRM_NIV_AMT AS TOT_IN_EFSR_NIV_AMT,
                 INCRM_BUOM_AMT AS TOT_IN_EFSR_CASE_AMT
            FROM OPT_EFSR_TDADS
           WHERE (INCRM_SU_AMT <> 0 OR INCRM_GIV_AMT <> 0 OR
                 INCRM_NIV_AMT <> 0 OR INCRM_BUOM_AMT <> 0)
             AND NOT EXISTS  (select ''x'' from ' || l_src_tbl_name ||
                 ' S, OPT_EFSR_TDADS EFSR
                 WHERE S.ACCT_PRMTN_SKID = EFSR.ACCT_PRMTN_SKID
                 AND S.DATE_SKID = EFSR.DATE_SKID
                 AND S.PROD_SKID = EFSR.PROD_SKID))';

       pro_put_line(v_sql);
       execute immediate v_sql; */  -- Commented Insert statement by venkatesh-HPE - PR#PM00049157
       commit;


       pro_put_line('Post_process of '|| l_src_tbl_name || '(End)');
    END IF;
  end if;

  -- To prevent partition exchange when source table is empty
  -- Added by Simon 2009-10-23
  EXECUTE IMMEDIATE 'DELETE FROM ' || l_src_tbl_name || ' WHERE ROWNUM<2';
  v_row_count := SQL%ROWCOUNT;
  ROLLBACK;
  If v_row_count > 0 Then
    --Check source table and target table
    l_step_info := 'Checking temp source table and target table...';
    opt_pkg_prttn_util.pro_check_tables(l_src_tbl_name, l_tgt_tbl_name);

        --Modified by Gary for R11 ticket IM00572780 on 2011/04/22 start (to find partition not match between source and target)
    --to verify whether the target table has initialed partiton list ,if not raise an error to fail the job
        SELECT count(partition_name)
          into v_partition_diff_count
          FROM user_tab_partitions
         WHERE table_name = l_src_tbl_name;
    if v_partition_diff_count = 1 then
            SELECT partition_name
              into v_partition_name
              FROM user_tab_partitions
             WHERE table_name = l_src_tbl_name;

        if v_partition_name = 'P_DEFAULT' then
            pro_put_line('Error: source table do not initial partition list!');
          raise_application_error(-20080,
                                  'Error: partitions has not initialed!');
        end if;
    end if;

        --to verify whether the target table has initialed partiton list ,if not raise an error to fail the job
        SELECT count(partition_name)
          into v_partition_diff_count
          FROM user_tab_partitions
         WHERE table_name = l_src_tbl_name
           AND partition_name NOT IN (SELECT partition_name
                                        FROM user_tab_partitions
                                       WHERE table_name = l_tgt_tbl_name);
    if v_partition_diff_count > 0 then
      pro_put_line('Error: partitions do not match between source and target table!');
      raise_application_error(-20080,
                              'Error: partitions do not match!');
    end if;
        --Modified by Gary for R11 ticket IM00572780 on 2011/04/22 end (to find partition not match between source and target)

    -- Alter all indexes on stage table unusable
    -- Modified by Myra 20090930 unusable index before promote data and partition exchange.
    -- opt_gather_stats.opt_unusable_ind(l_src_tbl_name);
    opt_gather_stats.opt_unusable_ind(l_tgt_tbl_name);
    --Promote data via partition exchange
    l_step_info := 'Prmoting data from temp source table into target table...';
    opt_promote_data(l_src_tbl_name, l_tgt_tbl_name, l_tmp_tbl_name,V_REGN);--SOX IMPLEMENTATION PROJECT
  End if;


  --Drop index and recreate indexes for OPT_PRMTN_PROD_FCT
  --BIP W2, Daniel.Qin,14 Jun 2011 ,recreate index for table OPT_ACTVY_FCT
  --Optima11, BIP-wv3-1, 15-Aug-2011, Daniel.Qin, remove the recreate index for SFCT tble
  IF upper(l_tgt_tbl_name) in ('OPT_PRMTN_PROD_FCT', 'OPT_ACTVY_FCT') THEN
    opt_tool.recreate_table_index(l_tgt_tbl_name,V_REGN);--SOX IMPLEMENTATION PROJECT
    --opt_tool.recreate_table_index(l_src_tbl_name);
  ELSE
      -- Filter out table OPT_PROD_BASLN_FCT for gather statics, performance tuning
      -- By Luke 2011-1-20
      IF UPPER(L_TGT_TBL_NAME) <> 'OPT_PROD_BASLN_FCT' THEN
     -- Rebuild all indexes on stage table
     -- Modified by Myra 20090930 rebuild index after promote data and partition exchange.
     -- opt_gather_stats.opt_rebuild_ind(l_src_tbl_name);
    opt_gather_stats.opt_rebuild_ind(l_tgt_tbl_name);
    -- gather statistics for all involved partitions
    pro_put_line('Starting gathering statistics on ' || l_tgt_tbl_name ||
                 ' table...');
    -- Changed to use Optima Package for statistics gathering
    -- Simon 16:51 2009-9-25
    opt_gather_stats.optima_stats_tbl(l_tgt_tbl_name);
    pro_put_line('Gathering statistics on ' || l_tgt_tbl_name ||' table...Done');
     END IF;
  END IF;


  exception
    when others then
      pro_put_line('Error: Unexpected system error - ' || sqlerrm || ' when ' || l_step_info);
      raise;
  end load_by_partition_exchange;

/************************************************************************************/
  /* name: load_by_prttn_exchg_with_sql                                                                                 */
  /* purpose: load data by partition exchange with sql                                                                      */
  /* parameters:                                                                                                            */
  /*   p_tbl_name - table to be loaded                                                                                 */
  /*   p_sql  - sql string to retrieve data.
  /* author: Bodhi Wang                                                                     */
  /* version: 1.00 - initial version                                                  */
  /************************************************************************************/
  procedure load_by_prttn_exchg_with_sql(
         p_tbl_name in varchar2,
         p_sql in varchar2,
		 V_REGN VARCHAR2 DEFAULT NULL) is --SOX IMPLEMENTATION PROJECT

    v_sql          varchar2(32767);
    v_start_date   date := sysdate;
    l_tgt_tbl_name varchar2(30);
    l_src_tbl_name varchar2(30);
    l_tmp_tbl_name varchar2(30);
    l_step_info varchar2(100);
  begin
    -- parameter control
    pro_put_line('Checking input parameters:' || chr(10) ||
                 '  p_tbl_name = ' || p_tbl_name || chr(10) ||
                 ' p_sql ='|| p_sql || chr(10));
    if p_tbl_name is null or p_sql is null then
      pro_put_line('Error: wrong input parameters!');
      raise_application_error(c_wrong_parms_error,
                              'Error: wrong input parameters!');
    end if;

    l_tgt_tbl_name := upper(p_tbl_name);
    l_src_tbl_name := l_tgt_tbl_name || '_TEMP';

    --Create source table
    l_step_info :='Creating temp source table...';
    pro_put_line(l_step_info);
    duplicate_prttn_table(l_tgt_tbl_name, l_src_tbl_name,V_REGN);--SOX IMPLEMENTATION PROJECT
/*
    l_step_info :='Creating indexes for temp source table...';
    pro_put_line(l_step_info);
    duplicate_prttn_idx(l_tgt_tbl_name, l_src_tbl_name);
*/
    --Check source table and target table
    l_step_info :='Checking temp source table and target table...';
    pro_put_line(l_step_info);
    opt_pkg_prttn_util.pro_check_tables(l_src_tbl_name, l_tgt_tbl_name);

    --Truncate source table
    l_step_info :='Truncating temp source table...';
    pro_put_line(l_step_info);
    opt_pkg_util.pro_truncate_tbl(l_src_tbl_name);

    --Insert data into source table
    l_step_info := 'Inserting data into temp source table...';
    pro_put_line(l_step_info);
     -- CR C01363382 / PR PM00018410 for PErformance added by KB starts
        IF (p_tbl_name='OPT_ACCT_HIER_FDIM')  THEN
            v_sql:='insert /*+ NO_CPU_COSTING */  into ' || l_src_tbl_name || ' '|| p_sql;
        ELSE
            v_sql:='insert /*+ APPEND PARALLEL(' || P_DOP || ') */  into ' || l_src_tbl_name || ' '|| p_sql;
        END IF;
    -- CR C01363382 / PR PM00018410 for PErformance added by KB Ends
    pro_put_line(v_sql);
    execute immediate v_sql;
    commit;

    --Promote data via partition exchange
    l_step_info := 'Prmoting data from temp source table into target table...';
    opt_promote_data(l_src_tbl_name, l_tgt_tbl_name, l_tmp_tbl_name,V_REGN);--SOX IMPLEMENTATION PROJECT

    -- gather statistics for all involved partitions
    pro_put_line('Starting gathering statistics on ' || l_tgt_tbl_name ||
                 ' table...');
    -- Changed to use Optima Package for statistics gathering
  -- Simon 16:51 2009-9-25
  opt_gather_stats.optima_stats_tbl(l_tgt_tbl_name);
  pro_put_line('Gathering statistics on ' || l_tgt_tbl_name ||' table...Done');

    --Drop temporary source table
    drop_table(l_src_tbl_name);
  exception
    when others then
      pro_put_line('Error: Unexpected system error - ' || sqlerrm || ' when ' || l_step_info);
      raise;
  end load_by_prttn_exchg_with_sql;

end OPT_PRTTN_LOAD_PKG;
/

