CREATE OR REPLACE PACKAGE ADWU_OPTIMA_LAEX.OPT_PRE_PRCSS IS
  ------------------------------------------------------------------------------------
  -- Package      : OPT_PRE_PRCSS                                                   --
  -- Originator   : Anthony Wang (dongw@hp.com)                                     --
  -- Author       : Anthony Wang (dongw@hp.com)                                     --
  -- Date         : 20-Jan-2009                                                     --
  -- Purpose      : Implement pre process before general extraction                 --
  ------------------------------------------------------------------------------------
  -- Change History                                                                 --
  -- Date        Programmer        Description                                      --
  -- ----------- ----------------- ---------------------------------------------------
  -- 20-Jan-2009 Anthony Wang      Initial Version                                  --
  -- 08-Jun-2009 Bodhi   Wang      Add pre_dd_ship_buom,pre_dd_ship_cu              --
  --                               Modify pre_dd_prmtn_prod, set zero-measures as   --
  --                               NULL.                                            --
  --                               Modify pre_extrt_fund_gen_spnd, for WE keep use  --
  --                               old SQL, for other regions, user new SQL         --
  --                               provided by Piotr                                --
  -- 28-Oct-2009 Bean, Zhu         introduce opt_load_data_prc package,             --
  --                               for standardized control                     --
  ------------------------------------------------------------------------------------

  -- Added by Myra 20090928 in R9. for tuning
  C_FINAL_TBL_STORAGE VARCHAR2(100) := 'PCTFREE 0 PCTUSED 0 INITRANS 1 MAXTRANS 255';
  C_TBL_ROLES VARCHAR2(100) := 'ADWU_OPTIMA_ETL_UPDATE,ADWU_OPTIMA_SELECT,SSE_ROLE,SSE_RO_ROLE';--SOX IMPLEMENTATION PROJECT
  -- Tablespace changed to OPTIMA01 for Exadata
  --C_TBLSPACE_NAME VARCHAR2(20) := 'TABLESPACE OPTIMA01';

  C_TBLSPACE_NAME VARCHAR2(50);
  V_INDEX         VARCHAR2(50);

  C_TABLE_DOP VARCHAR2(20) := ' PARALLEL 4 ';
  P_DOP number := 2;

  PROCEDURE PRE_EXTRACT(in_tbl_name VARCHAR2, in_dop INTEGER :=4,V_REGN IN VARCHAR2 DEFAULT NULL); -- SOX Implementation project
  PROCEDURE pre_extrt_fund_gen_spnd(in_grantee VARCHAR2 := NULL,V_REGN IN VARCHAR2 DEFAULT NULL); -- SOX implementation project
  PROCEDURE pre_extrt_actl_frcst(V_REGN IN VARCHAR2 DEFAULT NULL);-- SOX implementation project
  PROCEDURE pre_dd_prmtn(in_grantee VARCHAR2 := NULL,V_REGN IN VARCHAR2 DEFAULT NULL); -- SOX implementation project
  PROCEDURE pre_dd_prmtn_prod(in_grantee VARCHAR2 := NULL, in_dop INTEGER :=4,V_REGN IN VARCHAR2 DEFAULT NULL);-- SOX implementation project
  PROCEDURE pre_dd_base_ship_fund_prod(in_grantee VARCHAR2 := NULL, in_dop INTEGER :=4,V_REGN IN VARCHAR2 DEFAULT NULL); -- SOX implementation project
  PROCEDURE pre_dd_base_ship_fund(in_grantee VARCHAR2 := NULL,V_REGN IN VARCHAR2 DEFAULT NULL); -- SOX implementation project
  PROCEDURE pre_dd_ship_buom(in_grantee VARCHAR2 := NULL, in_dop INTEGER :=4);
  PROCEDURE pre_dd_ship_cu(in_grantee VARCHAR2 := NULL, in_dop INTEGER :=4,V_REGN IN VARCHAR2 DEFAULT NULL); -- SOX implementation project
  PROCEDURE pre_extrt_prod_basln(in_grantee VARCHAR2 := NULL, in_dop INTEGER :=4,V_REGN IN VARCHAR2 DEFAULT NULL);--SOX Implementation project
  PROCEDURE pre_extrt_opt_prmtn_prod_f(V_REGN IN VARCHAR2 DEFAULT NULL) -- SOX Implementation project
  PROCEDURE pre_extrt_actvy_spnd(in_grantee VARCHAR2 := NULL,V_REGN IN VARCHAR2 DEFAULT NULL); -- SOX implementation project
  PROCEDURE PRE_DD_FUND_ACCRL_CHILD(V_REGN IN VARCHAR2 DEFAULT NULL); -- SOX Implementation project
END OPT_PRE_PRCSS;
/

