CREATE OR REPLACE PACKAGE ADWU_OPTIMA_LAEX.OPT_IZOOM_REGNL_LOAD_PKG IS

  /**********************************************************************/
  /* name: OPT_IZOOM_REGNL_LOAD_PKG                                              */
  /* purpose:build partitions on opt izoom tables according to srce_sys_id  */
  /*     exchange partitions for iZoom tables           */
  /* author: David Lang                                                   */
  /* version: 1.00 - initial version                                    */
  /**********************************************************************/

  C_FINAL_TBL_STORAGE       VARCHAR2(100) := 'PCTFREE 0 PCTUSED 0 INITRANS 1 MAXTRANS 255';
  C_TMP_MTDTA_TBL_NAME      VARCHAR2(50) := 'OPT_TEMP_IZOOM_MTDTA';
  C_STATS_FINAL_EST_PERCENT NUMBER := 5;
  C_STATS_TEMP_EST_PERCENT  NUMBER := 5;

  PROCEDURE PRO_PUT_LINE(P_STRING IN VARCHAR2);

  PROCEDURE PRO_TRUNCATE_TABLE(TB_NAME VARCHAR2);

  PROCEDURE PRO_INITIALIZE_PRTTN(P_VC2_TBL_NAME IN USER_TABLES.TABLE_NAME%TYPE);

  PROCEDURE PRO_DROP_PRTTN(P_TBL_NAME IN USER_TABLES.TABLE_NAME%TYPE,
                           P_FLG      IN VARCHAR2 DEFAULT 'Y');

  PROCEDURE PRO_REGNL_LOAD(P_VC2_TBL_NAME IN USER_TABLES.TABLE_NAME%TYPE,
                           P_DOP          IN NUMBER DEFAULT 2);

  PROCEDURE PRO_EXCHANGE_PRTTN(P_VC2_SRCE_TBL IN USER_TABLES.TABLE_NAME%TYPE,
                               P_VC2_DEST_TBL IN USER_TABLES.TABLE_NAME%TYPE);

  PROCEDURE PRO_CLEANUP_PRTTN(P_VC2_TBL_NAME        IN USER_TABLES.TABLE_NAME%TYPE,
                              P_VC2_PRTTN_TYPE_CODE IN VARCHAR2 DEFAULT 'L');

  PROCEDURE PRO_INITIALIZE_MTH_PRTTN(P_VC2_TBL_NAME       IN USER_TABLES.TABLE_NAME%TYPE,
                                     P_NUM_PAST_MTH_NUM   IN NUMBER,
                                     P_NUM_FUTURE_MTH_NUM IN NUMBER);

END OPT_IZOOM_REGNL_LOAD_PKG; 
/

