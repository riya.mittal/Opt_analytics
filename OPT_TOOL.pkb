CREATE OR REPLACE PACKAGE BODY ADWU_OPTIMA_LAEX.OPT_TOOL IS

  PROCEDURE OPTIMA_TRUNCATE(TB_NAME VARCHAR2,
                            P_NAME  VARCHAR2 DEFAULT 'ALL') IS
  BEGIN
    IF P_NAME = 'ALL' THEN
      EXECUTE IMMEDIATE 'TRUNCATE TABLE ' || TB_NAME;
    ELSE
      EXECUTE IMMEDIATE 'ALTER TABLE ' || TB_NAME || ' TRUNCATE PARTITION ' ||
                        P_NAME || ' UPDATE INDEXES';
    END IF;
  END OPTIMA_TRUNCATE;

  PROCEDURE OPTIMA_TRUNCATE_SHPMT(TB_NAME VARCHAR2) IS
  BEGIN
    EXECUTE IMMEDIATE 'TRUNCATE TABLE ' || TB_NAME;
  END OPTIMA_TRUNCATE_SHPMT;

  --Added by  "Bodhi" <wang.faqun@hp.com>
  --13 Jul,2007.
  --Added one parameter in_table_name.
  --19 Jul,2007.
  PROCEDURE OPTIMA_TRUNC_SHPMT_SUBPARTS(IN_TABLE_NAME VARCHAR2,
                                        IN_REGION     VARCHAR2) IS
  BEGIN
    --DBMS_OUTPUT.ENABLE(1000000);
    FOR C_SHIP_SFCT_SUBPARTS IN (SELECT 'ALTER TABLE ' || TABLE_NAME ||
                                        ' TRUNCATE SUBPARTITION ' ||
                                        SUBPARTITION_NAME ||
                                        ' DROP STORAGE UPDATE INDEXES' AS TRUN_STMT
                                   FROM USER_TAB_SUBPARTITIONS
                                  WHERE TABLE_NAME = UPPER(IN_TABLE_NAME)
                                    AND SUBPARTITION_NAME LIKE
                                        '%' || IN_REGION || '%') LOOP
      EXECUTE IMMEDIATE C_SHIP_SFCT_SUBPARTS.TRUN_STMT;
      --DBMS_OUTPUT.PUT_LINE(c_ship_sfct_subparts.TRUN_STMT);
    END LOOP C_SHIP_SFCT_SUBPARTS;
  END OPTIMA_TRUNC_SHPMT_SUBPARTS;

  PROCEDURE OPTIMA_DELETE_SFCT(IN_FCT_TABLE_NAME VARCHAR2,
                               IN_FCT_KEY        VARCHAR2) IS
    V_SFCT_TAB_NAME VARCHAR2(50);
    V_EXECSQL       VARCHAR2(2000);
    COUNT_TOTAL     NUMBER(15);

  BEGIN
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                         CHR(10) ||
                         'Delete process before FCT workflow start. (BEGIN)');
    -- Get the Sfct Table Name
    V_SFCT_TAB_NAME := UPPER(SUBSTR(IN_FCT_TABLE_NAME,
                                    1,
                                    LENGTH(IN_FCT_TABLE_NAME) - 4)) ||
                       '_SFCT';
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                         CHR(10) || 'Releate SFCT table is ' ||
                         V_SFCT_TAB_NAME);

    -- Delete the sfct data where ETL_MRGT_CODE = 2 before the FCT workflow start
    V_EXECSQL := 'DELETE /*+PARALLEL(a, 5)*/ FROM ' ||
                 UPPER(IN_FCT_TABLE_NAME) || ' a' || CHR(10) ||
                 '  WHERE  EXISTS ' || CHR(10) ||
                 '  (SELECT /*+PARALLEL(b, 5)*/ ''x'' FROM ' ||
                 V_SFCT_TAB_NAME || ' b' || CHR(10) ||
                 '    WHERE b.ETL_MGRT_CODE = 2 AND b.' || IN_FCT_KEY ||
                 '= a.' || IN_FCT_KEY || ')';
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                         'Execute Sql is: ' || CHR(10) || V_EXECSQL);

    EXECUTE IMMEDIATE V_EXECSQL;

    COUNT_TOTAL := SQL%ROWCOUNT;
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                         CHR(10) || COUNT_TOTAL || ' rows deleted.' ||
                         CHR(10));
    COMMIT;

    --Gether_Stats For FCT table
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                         ' STEP 00: Gather Stats for ' ||
                         IN_FCT_TABLE_NAME || '(BEGIN)');
    BEGIN
      OPT_GATHER_STATS.OPTIMA_STATS_TBL(IN_FCT_TABLE_NAME);
    END;
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                         ' STEP 00: Gather Stats for ' ||
                         IN_FCT_TABLE_NAME || '(END_OK)');

    DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') ||
                         'Delete process before FCT workflow start. (END_OK)');

  END OPTIMA_DELETE_SFCT;

  FUNCTION FN_ENCRYPT(P_CONN IN VARCHAR2, KEY_S IN VARCHAR2) RETURN VARCHAR2 IS
    ENCRYPTED_STR VARCHAR2(2048);
  BEGIN
    DBMS_OBFUSCATION_TOOLKIT.DESENCRYPT(INPUT_STRING     => P_CONN,
                                        KEY_STRING       => KEY_S,
                                        ENCRYPTED_STRING => ENCRYPTED_STR);
    RETURN ENCRYPTED_STR;
  END;

  FUNCTION FN_DECRYPT(P_CONN_CODE IN VARCHAR2, KEY_S IN VARCHAR2)
    RETURN VARCHAR2 IS
    DECRYPTED_STR VARCHAR2(2048);
  BEGIN
    DBMS_OBFUSCATION_TOOLKIT.DESDECRYPT(INPUT_STRING     => P_CONN_CODE,
                                        KEY_STRING       => KEY_S,
                                        DECRYPTED_STRING => DECRYPTED_STR);
    RETURN DECRYPTED_STR;
  END;

  FUNCTION FIND_OWNER RETURN VARCHAR2 IS
    PRJ_OWNER VARCHAR2(60);
  BEGIN
    SELECT USERNAME INTO PRJ_OWNER FROM USER_USERS WHERE ROWNUM = 1;
    RETURN PRJ_OWNER;
  END;

  FUNCTION CALC_ELAPSE_TIME(START_TIME DATE, END_TIME DATE) RETURN VARCHAR2 IS
    V_DURATION VARCHAR2(30);
    T_DURATION NUMBER;
  BEGIN
    SELECT (END_TIME - START_TIME) * 24 * 60 * 60
      INTO T_DURATION
      FROM DUAL;

    IF T_DURATION < 60 THEN
      V_DURATION := '00:00:' || LPAD(TRUNC(T_DURATION, 0), 2, 0);
    ELSE
      IF T_DURATION < 3600 THEN
        V_DURATION := '00:' || LPAD(TRUNC(T_DURATION / 60, 0), 2, 0) || ':' ||
                      LPAD(ROUND(MOD(T_DURATION, 60), 0), 2, 0);
      ELSE
        IF T_DURATION < 86400 THEN
          V_DURATION := LPAD(TRUNC(T_DURATION / 3600, 0), 2, 0) || ':' ||
                        LPAD(TRUNC((TRUNC(MOD(T_DURATION, 3600), 0)) / 60,
                                   0),
                             2,
                             0) || ':' ||
                        LPAD(ROUND(MOD((TRUNC(MOD(T_DURATION, 3600), 0)),
                                       60),
                                   0),
                             2,
                             0);
        ELSE
          IF T_DURATION >= 86400 THEN
            V_DURATION := TRUNC(T_DURATION / 86400, 0) || ' ' ||
                          LPAD(TRUNC(MOD(T_DURATION, 86400) / 3600, 0),
                               2,
                               0) || ':' ||
                          LPAD(ROUND(MOD(TRUNC(MOD(T_DURATION, 86400), 0),
                                         3600),
                                     0),
                               2,
                               0) || ':' || LPAD(TRUNC(MOD(TRUNC(MOD(TRUNC(MOD(T_DURATION,
                                                                               86400),
                                                                           0),
                                                                     3600),
                                                                 0),
                                                           60),
                                                       0),
                                                 2,
                                                 0);
          END IF;
        END IF;
      END IF;
    END IF;

    RETURN V_DURATION;

  END CALC_ELAPSE_TIME;

  FUNCTION PAD_DELIMITER(N NUMBER) RETURN VARCHAR2 AS
    S VARCHAR2(9);
  BEGIN
    SELECT RPAD('-', N, ',') INTO S FROM DUAL;
    RETURN SUBSTR(S, 2, LENGTH(S) - 1);
  END PAD_DELIMITER;

  FUNCTION DELET_HDN_PROD(P_PATH VARCHAR2) RETURN VARCHAR2 AS
    CURR     VARCHAR2(1);
    NEXTCHAR VARCHAR2(1);
    RESLT   VARCHAR2(256) := '';
    TMP_VAL  VARCHAR2(256) := '';
    HDN_FLG  BOOLEAN;
    I        INTEGER;
    F_PATH  VARCHAR2(256) := '';
    PROD_LVL_NUM INTEGER := 0;
  BEGIN
    IF P_PATH IS NULL THEN
      RETURN '';
    END IF;
    HDN_FLG := FALSE;
    I       := 1;
    F_PATH := SUBSTR(P_PATH, INSTR(P_PATH, ',', 1, 1));
    PROD_LVL_NUM := TO_NUMBER(SUBSTR(P_PATH, 1, INSTR(P_PATH, ',', 1, 1)-1));
    WHILE (I < LENGTH(F_PATH)) LOOP
      CURR     := SUBSTR(F_PATH, I, 1);
      NEXTCHAR := SUBSTR(F_PATH, I + 1, 1);
      IF (CURR = 'N') AND (NEXTCHAR = ':') THEN
        HDN_FLG := FALSE;
        IF (INSTR(F_PATH, ',', I, 1) = 0) THEN
          TMP_VAL := SUBSTR(F_PATH, I, LENGTH(F_PATH) - I + 1);
          I       := LENGTH(F_PATH);
        ELSE
          TMP_VAL := SUBSTR(F_PATH, I, INSTR(F_PATH, ',', I, 1) - I);
          I       := INSTR(F_PATH, ',', I, 1);
        END IF;
        RESLT := RESLT || TMP_VAL;
      ELSIF (CURR = 'Y') AND (NEXTCHAR = ':') THEN
        HDN_FLG := TRUE;
        IF (INSTR(F_PATH, ',', I, 1) = 0) THEN
          I := LENGTH(F_PATH);
          PROD_LVL_NUM := PROD_LVL_NUM - 1;
          RESLT := LPAD(RESLT, LENGTH(RESLT)+1, ',');
        ELSE
          I := INSTR(F_PATH, ',', I, 1) + 1;
          PROD_LVL_NUM := PROD_LVL_NUM - 1;
          RESLT := LPAD(RESLT, LENGTH(RESLT)+1, ',');
        END IF;
      ELSE
        HDN_FLG := FALSE;
        RESLT  := RESLT || CURR;
        I       := I + 1;
      END IF;

    END LOOP;

    RESLT := TO_CHAR(PROD_LVL_NUM) || RESLT;
    RESLT := REPLACE(RESLT, 'N:', '');
    IF (SUBSTR(RESLT, LENGTH(RESLT)) = ',') THEN
       RESLT := SUBSTR(RESLT, 1, LENGTH(RESLT) - 1);
    END IF;

    IF (INSTR(RESLT, ',', 1, 10)) > 0 THEN
       RESLT := SUBSTR(RESLT, 1, 1) || SUBSTR(RESLT, 3);
    END IF;

    RETURN RESLT;
  END DELET_HDN_PROD;


FUNCTION CALC_VAL(P_INPUT IN VARCHAR2, FLAG IN VARCHAR2) RETURN VARCHAR2 IS
  V_OUTPUT  VARCHAR2(10);
BEGIN
  IF FLAG = 'MAX' THEN
      EXECUTE IMMEDIATE 'select greatest(' || SUBSTR(P_INPUT, 2) || ') from dual'
      INTO V_OUTPUT;
  ELSE
    IF FLAG = 'MIN' THEN
       EXECUTE IMMEDIATE 'select least(' || SUBSTR(P_INPUT, 2) || ') from dual'
       INTO V_OUTPUT;
    END IF;
  END IF;
  RETURN V_OUTPUT;
END CALC_VAL;

PROCEDURE OPTIMA_BACKUP_TABLE(P_SRC_TBL VARCHAR2, P_TRGT_TBL VARCHAR2, P_OPR_IND VARCHAR2) IS
---------------------------------------------------------------------------------------------
-- OPTIMA11,CR894, 16-Dec-2010,Martin, Use 'MERGE' SQL statement to replace all 'UPDATE/+ BYPASS_UJVC /' SQL statement.
---------------------------------------------------------------------------------------------
  V_SQLSTMT            VARCHAR2(500);
BEGIN
  IF P_OPR_IND = 'B' THEN
  BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE '|| P_TRGT_TBL ||' PURGE';
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
   END;
    EXECUTE IMMEDIATE 'CREATE TABLE ' ||P_TRGT_TBL|| ' COMPRESS AS SELECT * FROM ' || P_SRC_TBL;
  ELSIF P_OPR_IND = 'R' THEN
    EXECUTE IMMEDIATE 'TRUNCATE TABLE '|| P_TRGT_TBL;
    V_SQLSTMT := 'INSERT INTO '||P_TRGT_TBL||' SELECT * FROM '||P_SRC_TBL;
    EXECUTE IMMEDIATE V_SQLSTMT;
    COMMIT;
  END IF;
  --EXCEPTION
    --WHEN OTHERS THEN
     -- DBMS_OUTPUT.PUT_LINE('Error: Unexpected system error - ' || sqlerrm);
     -- RAISE;
END OPTIMA_BACKUP_TABLE;

PROCEDURE OPTIMA_UPD_FUND_ACCT_IND IS
  V_SQLSTMT            VARCHAR2(2000);
BEGIN
  -- New Merge query to replace the BYPASS_UJVC hint
      V_SQLSTMT := 'MERGE INTO opt_acct_hier_type2_dim trgt
                    USING (SELECT DISTINCT fund_acct.acct_id
                                , CASE WHEN fund_acct.acct_skid IS NULL THEN ''N'' ELSE ''Y'' END AS fund_acct_ind
                             FROM opt_acct_hier_type2_dim acct_hier
                                , (   WITH BASE_QRY AS
                                      (
                                      SELECT parnt_id
                                        FROM opt_acct_xm_esi xm
                                       WHERE xm.parnt_id = xm.fund_acct
                                      )
                                      SELECT acct_skid
                                            , acct_id
                                         FROM opt_acct_fdim
                                        WHERE CONNECT_BY_ISLEAF = 1
                                   CONNECT BY PRIOR acct_id = parnt_acct_id
                                   START WITH acct_id IN (SELECT parnt_id
                                                            FROM BASE_QRY)) fund_acct
                            WHERE acct_hier.acct_id(+) = fund_acct.acct_id) srce
                    ON (srce.acct_id = trgt.acct_id)
                    WHEN MATCHED THEN UPDATE SET trgt.fund_acct_ind = srce.fund_acct_ind';

  -- comment by Venkat on Sep 13, 2011 to remove the BYPASS_UJVC hint
  /*v_sqlstmt := 'UPDATE --+ BYPASS_UJVC 
     (SELECT ACCT_HIER.ACCT_ID,
             ACCT_HIER.FUND_ACCT_IND,
             DECODE(FUND_ACCT.ACCT_SKID,
                    NULL,
                    ''N'',
                    ''Y'') AS T_FUND_ACCT_IND
        FROM OPT_ACCT_HIER_TYPE2_DIM ACCT_HIER,
             (SELECT ACCT_SKID, ACCT_ID
                FROM OPT_ACCT_FDIM
               WHERE CONNECT_BY_ISLEAF = 1
              CONNECT BY PRIOR ACCT_ID = PARNT_ACCT_ID
               START WITH ACCT_ID IN
                          (SELECT PARNT_ID
                             FROM OPT_ACCT_XM_ESI XM
                            WHERE XM.PARNT_ID =
                                  XM.FUND_ACCT)) FUND_ACCT
       WHERE ACCT_HIER.ACCT_ID(+) = FUND_ACCT.ACCT_ID) O
   SET O.FUND_ACCT_IND = O.T_FUND_ACCT_IND
 WHERE EXISTS (SELECT 1
          FROM OPT_ACCT_HIER_TYPE2_DIM ACCT_HIER
         WHERE ACCT_HIER.ACCT_ID = O.ACCT_ID)';*/

  EXECUTE IMMEDIATE V_SQLSTMT;
  COMMIT;
END OPTIMA_UPD_FUND_ACCT_IND;


   /**********************************************************************/
  /* name: pro_put_line                                                 */
  /* purpose: print line of text divided into 255-character chunks      */
  /* parameters:                                                        */
  /*   p_string - string to be printed                                  */
  /* author: bazyli blicharski                                          */
  /* originator: brian beckman                                          */
  /* version: 1.00 - initial version                                    */
  /*          1.01 - string cut according to new line marker            */
  /**********************************************************************/
  PROCEDURE PRO_PUT_LINE(P_STRING IN VARCHAR2) IS
    V_STRING_LENGTH NUMBER;
    V_STRING_OFFSET NUMBER := 1;
    V_CUT_POS       NUMBER;
    V_ADD           NUMBER;
  BEGIN
    DBMS_OUTPUT.NEW_LINE;
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
    V_STRING_LENGTH := LENGTH(P_STRING);

    -- loop thru the string by 255 characters and output each chunk
    WHILE V_STRING_OFFSET < V_STRING_LENGTH LOOP
      V_CUT_POS := NVL(INSTR(P_STRING, CHR(10), V_STRING_OFFSET), 0) -
                   V_STRING_OFFSET;
      IF V_CUT_POS < 0 OR V_CUT_POS >= 255 THEN
        V_CUT_POS := 255;
        V_ADD     := 0;
      ELSE
        V_ADD := 1;
      END IF;
      DBMS_OUTPUT.PUT_LINE(SUBSTR(P_STRING, V_STRING_OFFSET, V_CUT_POS));
      V_STRING_OFFSET := V_STRING_OFFSET + V_CUT_POS + V_ADD;
    END LOOP;
  END;

 /************************************************************************************/
  /* name: duplicate_prttn_idx                                            */
  /* purpose: duplicate indexes for new table according to template table'S DEFINITION                          */
  /* parameters:                                                                      */
  /*   p_tmplt_table - template table                                               */
  /*   p_new_tbl - new table                                               */
  /* author: Bodhi Wang                                                        */
  /* version: 1.00 - initial version                                                  */
  /************************************************************************************/

  PROCEDURE DUPLICATE_PRTTN_IDX(V_REGN VARCHAR2 DEFAULT NULL) IS --SOX IMPLEMENTATION PROJECT
  V_TAB_DEF VARCHAR2(32767) ;

BEGIN
OPT_AX_TBLSPACE(V_REGN,V_TBLSPACE_NAME,V_INDEX);--SOX IMPLEMENTATION PROJECT

--Replaced OPTIMA01 by V_TBLSPACE_NAME
V_TAB_DEF := 'create index "OPT_SHPMT_SFCT_NX1" ON
"OPT_SHPMT_SFCT" ("SHPMT_DATE_SKID", "PROD_SKID")
tablespace '||V_INDEX||'
pctfree 10
initrans 2
maxtrans 255
STORAGE(
BUFFER_POOL DEFAULT) LOCAL';
  PRO_PUT_LINE(V_TAB_DEF);
  EXECUTE IMMEDIATE V_TAB_DEF;
  COMMIT;

  EXCEPTION
    WHEN others THEN
      --Raise error
      PRO_PUT_LINE(SQLERRM);
      raise_application_error(-20081,
                              'ERROR!!! When duplicate indexes of OPT_SHPMT_SFCT'
                              );
  END DUPLICATE_PRTTN_IDX;
/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
*note: Drop index
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
PROCEDURE PRO_DROP_INDEX (
   P_IDX_NAME   IN VARCHAR2
)
AS
   V_IDX_CNTR   NUMBER;
BEGIN

   SELECT   COUNT (1)
     INTO   V_IDX_CNTR
     FROM   USER_INDEXES
    WHERE   INDEX_NAME = UPPER (P_IDX_NAME);

   IF V_IDX_CNTR = 1
   THEN
      EXECUTE IMMEDIATE 'DROP INDEX ' || P_IDX_NAME;

      DBMS_OUTPUT.PUT_LINE (
         'SUCCEED: index ' || P_IDX_NAME || ' dropped successfully'
      );
   ELSIF V_IDX_CNTR < 1
   THEN
      DBMS_OUTPUT.PUT_LINE ('ERROR: No such index, nothing to do.');
   ELSE
      DBMS_OUTPUT.PUT_LINE (
         'ERROR : incorrectly index number than expected, nothing to do.'
      );
   END IF;
END PRO_DROP_INDEX;

PROCEDURE PRO_DROP_TAB_ALL_INDEXES (P_TBL_NAME IN VARCHAR2)
/*----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
*note: for seeing output message, switch serveroutput on
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
AS
   V_TBL_CNTR   NUMBER;
BEGIN
   SELECT   COUNT (1)
     INTO   V_TBL_CNTR
     FROM   USER_TABLES
    WHERE   TABLE_NAME = UPPER(P_TBL_NAME);

   IF V_TBL_CNTR > 0
   THEN
      FOR REC IN (SELECT   INDEX_NAME
                           FROM   USER_INDEXES
                          WHERE   TABLE_NAME =UPPER( P_TBL_NAME))
      LOOP
         PRO_DROP_INDEX (REC.INDEX_NAME);
      END LOOP;
   ELSE
      DBMS_OUTPUT.PUT_LINE ('ERROR: No such table, nothing to do.');
   END IF;
END PRO_DROP_TAB_ALL_INDEXES;

 /**********************************************************************/
  /* name: create_table_index                                           */
  /* purpose: recreate indexes of the table                             */
  /* parameters:                                                        */
  /*   p_tbl_name - indexex of which will be created                    */
  /* author: Martin Que                                                 */
  /* version: 1.00 - initial version                                    */
  /**********************************************************************/
PROCEDURE CREATE_TABLE_INDEX(P_TBL_NAME IN VARCHAR2,V_REGN VARCHAR2 DEFAULT NULL) --SOX IMPLEMENTATION PROJECT
AS
 V_SQL VARCHAR2(32767);
BEGIN

OPT_AX_TBLSPACE(V_REGN,V_TBLSPACE_NAME,V_INDEX);--SOX IMPLEMENTATION PROJECT

--Replaced OPTIMA01 by V_TBLSPACE_NAME

CASE
 WHEN UPPER(P_TBL_NAME)='OPT_FUND_FRCST_SFCT' THEN
    V_SQL:='CREATE INDEX OPT_FUND_FRCST_SFCT_NX2 ON OPT_FUND_FRCST_SFCT ("FRCST_SKID")
            PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS
            STORAGE(INITIAL 131072 NEXT 131072 MINEXTENTS 1 MAXEXTENTS 2147483645
            PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
            TABLESPACE '||V_INDEX||'
            PARALLEL 2 ';
    EXECUTE IMMEDIATE V_SQL;
    PRO_PUT_LINE('Indexes of the table '||P_TBL_NAME||' have been created successfully');

-- WHEN upper(p_tbl_name)='OPT_SHPMT_SFCT' THEN
--    v_sql:= 'create index OPT_SHPMT_SFCT_NX1 ON OPT_SHPMT_SFCT ("SHPMT_DATE_SKID", "PROD_SKID")
--            tablespace OPTIMA01
--            pctfree 10
--            initrans 2
--            maxtrans 255
--            STORAGE(BUFFER_POOL DEFAULT) LOCAL';
--    EXECUTE IMMEDIATE v_sql;
--    pro_put_line('Indexes of the table '||p_tbl_name||' have been created successfully');

 WHEN UPPER(P_TBL_NAME)='OPT_PRMTN_PROD_FCT' THEN
    --Optima11, BIT Performance improvement, 22-Mar-2011,Daniel.Qin, delete the unused index, Begin
    V_SQL:= 'CREATE  BITMAP INDEX OPT_PRMTN_PROD_FCT_BX1 ON OPT_PRMTN_PROD_FCT
            (ACCT_PRMTN_SKID  ASC)
            NOLOGGING
            TABLESPACE '||V_INDEX||'
            PARALLEL 2
            LOCAL';
     EXECUTE IMMEDIATE V_SQL;
     V_SQL:= 'CREATE  BITMAP INDEX OPT_PRMTN_PROD_FCT_BX2 ON OPT_PRMTN_PROD_FCT
           (DATE_SKID  ASC)
            NOLOGGING
            TABLESPACE '||V_INDEX||'
            PARALLEL 2
            LOCAL ';
     EXECUTE IMMEDIATE V_SQL;
     V_SQL:= 'CREATE  BITMAP INDEX OPT_PRMTN_PROD_FCT_BX4 ON OPT_PRMTN_PROD_FCT
           (PRMTN_SKID  ASC)
            NOLOGGING
            TABLESPACE '||V_INDEX||'
            PARALLEL 2
            LOCAL ';
      EXECUTE IMMEDIATE V_SQL;
      V_SQL:= 'CREATE  BITMAP INDEX OPT_PRMTN_PROD_FCT_BX5 ON OPT_PRMTN_PROD_FCT
           (PROD_SKID  ASC)
            NOLOGGING
            TABLESPACE '||V_INDEX||'
            PARALLEL 2
            LOCAL ';
       EXECUTE IMMEDIATE V_SQL;
       /*
       v_sql:= 'CREATE  BITMAP INDEX OPT_PRMTN_PROD_FCT_BX6 ON OPT_PRMTN_PROD_FCT
           (PROD_ID  ASC)
            NOLOGGING
            TABLESPACE OPTIMA01
            PARALLEL 2
            LOCAL ';
        EXECUTE IMMEDIATE v_sql;
        v_sql:= 'CREATE  BITMAP INDEX OPT_PRMTN_PROD_FCT_BX7 ON OPT_PRMTN_PROD_FCT
           (PRMTN_ID  ASC)
            NOLOGGING
            TABLESPACE OPTIMA01
            PARALLEL 2
            LOCAL ';
        EXECUTE IMMEDIATE v_sql;
        v_sql:= 'CREATE INDEX OPT_PRMTN_PROD_FCT_NX2 ON OPT_PRMTN_PROD_FCT
           (PRMTN_SKID  ASC,PROD_SKID  ASC,FY_DATE_SKID  ASC)
            PCTFREE 0
            NOLOGGING
            TABLESPACE OPTIMA01
            PARALLEL 2
            LOCAL ';
        EXECUTE IMMEDIATE v_sql;
        v_sql:= 'CREATE INDEX OPT_PRMTN_PROD_FCT_NX1 ON OPT_PRMTN_PROD_FCT
           (PRMTN_FACT_SKID  ASC)
            PARALLEL 2
            LOCAL ';
        EXECUTE IMMEDIATE v_sql;
        */
        --Optima11, BIT Performance improvement, 22-Mar-2011,Daniel.Qin, delete the unused index, End
    PRO_PUT_LINE('Indexes of the table '||P_TBL_NAME||' have been created successfully');

  WHEN UPPER(P_TBL_NAME)='OPT_PRMTN_PROD_SFCT' THEN
    V_SQL:= 'CREATE  BITMAP INDEX OPT_PRMTN_PROD_SFCT_BX1 ON OPT_PRMTN_PROD_SFCT
            (ACCT_PRMTN_SKID  ASC)
               NOLOGGING
               TABLESPACE '||V_INDEX||'
               PARALLEL 2
               LOCAL' ;
     EXECUTE IMMEDIATE V_SQL;
     V_SQL:= 'CREATE  BITMAP INDEX OPT_PRMTN_PROD_SFCT_BX2 ON OPT_PRMTN_PROD_SFCT
         (DATE_SKID  ASC)
             NOLOGGING
             TABLESPACE '||V_INDEX||'
             PARALLEL 2
              LOCAL ';
     EXECUTE IMMEDIATE V_SQL;
     V_SQL:= 'CREATE  BITMAP INDEX OPT_PRMTN_PROD_SFCT_BX4 ON OPT_PRMTN_PROD_SFCT
         (PRMTN_SKID  ASC)
             NOLOGGING
             TABLESPACE '||V_INDEX||'
             PARALLEL 2
              LOCAL ';
      EXECUTE IMMEDIATE V_SQL;
      V_SQL:= 'CREATE  BITMAP INDEX OPT_PRMTN_PROD_SFCT_BX5 ON OPT_PRMTN_PROD_SFCT
         (PROD_SKID  ASC)
             NOLOGGING
             TABLESPACE '||V_INDEX||'
             PARALLEL 2
              LOCAL ';
      EXECUTE IMMEDIATE V_SQL;
      --Optima11, BIT Performance improvement, 25-Mar-2011,Daniel.Qin, delete the unused index, Begin
      /*
      v_sql:= 'CREATE  BITMAP INDEX OPT_PRMTN_PROD_SFCT_BX6 ON OPT_PRMTN_PROD_SFCT
         (PROD_ID  ASC)
             NOLOGGING
             TABLESPACE OPTIMA01
             PARALLEL 2
              LOCAL ';
       EXECUTE IMMEDIATE v_sql;
       v_sql:= 'CREATE  BITMAP INDEX OPT_PRMTN_PROD_SFCT_BX7 ON OPT_PRMTN_PROD_SFCT
         (PRMTN_ID  ASC)
             NOLOGGING
             TABLESPACE OPTIMA01
             PARALLEL 2
              LOCAL ';
       EXECUTE IMMEDIATE v_sql;
       v_sql:= 'CREATE INDEX OPT_PRMTN_PROD_SFCT_NX2 ON OPT_PRMTN_PROD_SFCT
         (PRMTN_SKID  ASC,PROD_SKID  ASC,FY_DATE_SKID  ASC)
             PCTFREE 0
             NOLOGGING
             TABLESPACE OPTIMA01
             PARALLEL 2
              LOCAL ';
       EXECUTE IMMEDIATE v_sql;
       v_sql:= 'CREATE INDEX OPT_PRMTN_PROD_SFCT_NX1 ON OPT_PRMTN_PROD_SFCT
         (PRMTN_FACT_SKID  ASC)
             PARALLEL 2
              LOCAL ';
       EXECUTE IMMEDIATE v_sql;
       */
       --Optima11, BIT Performance improvement, 22-Mar-2011,Daniel.Qin, delete the unused index, End
    PRO_PUT_LINE('Indexes of the table '||P_TBL_NAME||' have been created successfully');
END CASE;
 EXCEPTION
   WHEN OTHERS THEN
    PRO_PUT_LINE('Created indexes '||P_TBL_NAME||' failed with error- '||SQLERRM);
    raise_application_error(C_IDX_CREATE_ERROR,'Error:index create failed, maybe index already exist'||SQLERRM);
END;

PROCEDURE RECREATE_TABLE_INDEX(P_TBL_NAME IN VARCHAR2,V_REGN VARCHAR2 DEFAULT NULL)-- SOX IMPLEMENTATION PROJECT
 /*
  ***************************************************************************
  * Program : RECREATE_TABLE_INDEX
  * Version : 1.0
  * Author  : Daniel Qin
  * Date    : 14-JUN-2011
  * Purpose : Drop/recreate bipmap indexes of the table [BIP-W2]
  * Parameters :  p_tbl_name     --the source table needed to recreate index
  *
  *
  * Change History
  * Date         Programmer         Description
  * -------------------------------------------------------------------------
  * 14-JUN-2011  Daniel Qin         Initial Version
  * 15-JUN-2011  Daniel Qin         Format code and use dynamic property of ta-
  *                                 blespace/log/local options to create index
  ****************************************************************************
  */
  IS
    V_CREATE_SQL              VARCHAR2(1000);
    V_DROP_SQL                VARCHAR2(1000);
    V_ALT_PARL_SQL            VARCHAR2(1000);

    --Optima11, BIP-W2, 15-Jun-2011,Daniel.Qin, get the indexes' attributes for recreate
    CURSOR CUR_INDEX IS
    SELECT UI.INDEX_NAME,
           UI.INDEX_TYPE,
           UI.PARTITIONED,
           --NVL(UPI.DEF_TABLESPACE_NAME,'OPTIMA01') DEF_TABLESPACE_NAME,
           --NVL(UI.TABLESPACE_NAME,'OPTIMA01') TABLESPACE_NAME,
           NVL(UPI.DEF_TABLESPACE_NAME,'DEFAULT') DEF_TABLESPACE_NAME,
           NVL(UI.TABLESPACE_NAME,'DEFAULT') TABLESPACE_NAME,
           DECODE(UI.UNIQUENESS,'NONUNIQUE',NULL) AS UNIQUENESS,
           NVL(UPI.LOCALITY,'') LOCALITY,
           DECODE(UI.LOGGING,'NO',' NOLOGGING','') LOGGING,
           DECODE(UPI.DEF_LOGGING,'NO',' NOLOGGING','') DEF_LOGGING
      FROM USER_INDEXES UI,
           USER_PART_INDEXES UPI
     WHERE UI.INDEX_NAME = UPI.INDEX_NAME(+)
       AND UI.TABLE_NAME = P_TBL_NAME;

  BEGIN

    OPT_AX_TBLSPACE(V_REGN,V_TBLSPACE_NAME,V_INDEX);--SOX IMPLEMENTATION PROJECT

    -- Loop thru all indexes on the table
    FOR REC_INDEX IN CUR_INDEX
    LOOP

      --Optima11, BIP-W2, 15-Jun-2011,Daniel.Qin, Format code
      V_DROP_SQL := 'DROP INDEX '||REC_INDEX.INDEX_NAME;

      ---Generate initial SQL
      IF REC_INDEX.INDEX_TYPE='NORMAL' THEN
        -- Index is either unique or b-tree
        V_CREATE_SQL := 'CREATE '||REC_INDEX.UNIQUENESS||' INDEX '||REC_INDEX.INDEX_NAME||' '||
                           'ON '||P_TBL_NAME||' ('||CHR(10);
      ELSE
        -- Index is a bitmap index
        V_CREATE_SQL := 'CREATE '||REC_INDEX.INDEX_TYPE||' INDEX '||REC_INDEX.INDEX_NAME||' '||
                           'ON '||P_TBL_NAME||' ('||CHR(10);
      END IF;

      ---Add columns to SQL
      FOR REC_INDEX_COL IN (SELECT DECODE(COLUMN_POSITION,1,' ',', ')||COLUMN_NAME||' '||DESCEND COLUMN_NAME
                              FROM USER_IND_COLUMNS
                             WHERE INDEX_NAME = REC_INDEX.INDEX_NAME
                               AND TABLE_NAME = P_TBL_NAME
                             ORDER BY COLUMN_POSITION
                           )
      LOOP
        V_CREATE_SQL := V_CREATE_SQL||REC_INDEX_COL.COLUMN_NAME||CHR(10);
      END LOOP;
      
      --Optima11, BIP-W2, 15-Jun-2011,Daniel.Qin, use original tablespace/log option/local option to create index, Begin
      --Optima11, BIP-Wv3-1, 29-Jul-2011,Daniel.Qin, add PARALLEL 2 when creating indexes
      IF REC_INDEX.PARTITIONED = 'YES' THEN
        --V_CREATE_SQL := V_CREATE_SQL||') '||REC_INDEX.DEF_LOGGING||CHR(10)||'TABLESPACE '||REC_INDEX.DEF_TABLESPACE_NAME
        V_CREATE_SQL := V_CREATE_SQL||') '||REC_INDEX.DEF_LOGGING||CHR(10)||'TABLESPACE '||V_INDEX
                          ||CHR(10)||'PARALLEL 2'||CHR(10)||REC_INDEX.LOCALITY;
      ELSE
        --V_CREATE_SQL := V_CREATE_SQL||') '||REC_INDEX.LOGGING||CHR(10)||'TABLESPACE '||REC_INDEX.TABLESPACE_NAME
        V_CREATE_SQL := V_CREATE_SQL||') '||REC_INDEX.LOGGING||CHR(10)||'TABLESPACE '||V_INDEX
                          ||CHR(10)||'PARALLEL 2'||CHR(10)||REC_INDEX.LOCALITY;
      END IF;
      --Optima11, BIP-W2, 15-Jun-2011,Daniel.Qin, use original tablespace/log option/local option to create index, End
      --Optima11, BIP-Wv3-1, 29-Jul-2011,Daniel.Qin, alter index degree to 1 after created indexes
      V_ALT_PARL_SQL := 'ALTER INDEX '||REC_INDEX.INDEX_NAME||' PARALLEL 1';

      EXECUTE IMMEDIATE V_DROP_SQL;
      PRO_PUT_LINE(V_DROP_SQL||' successfully');

      EXECUTE IMMEDIATE V_CREATE_SQL;
      PRO_PUT_LINE(V_CREATE_SQL||' successfully');

      EXECUTE IMMEDIATE V_ALT_PARL_SQL;
      PRO_PUT_LINE(V_ALT_PARL_SQL||' successfully');

    END LOOP;

  END RECREATE_TABLE_INDEX;

END OPT_TOOL;
/

