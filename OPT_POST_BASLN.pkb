CREATE OR REPLACE PACKAGE BODY ADWU_OPTIMA_LAEX.OPT_POST_BASLN IS

  /**********************************************************************/
  /* name: pro_put_line                                                 */
  /* purpose: print line of text divided into 255-character chunks      */
  /* parameters:                                                        */
  /*   p_string - string to be printed                                  */
  /* author: bazyli blicharski                                          */
  /* originator: brian beckman                                          */
  /* version: 1.00 - initial version                                    */
  /*          1.01 - string cut according to new line marker            */
  /**********************************************************************/
  PROCEDURE PRO_PUT_LINE(P_STRING IN VARCHAR2) IS
    V_STRING_LENGTH NUMBER;
    V_STRING_OFFSET NUMBER := 1;
    V_CUT_POS       NUMBER;
    V_ADD           NUMBER;
  BEGIN
    DBMS_OUTPUT.NEW_LINE;
    DBMS_OUTPUT.PUT_LINE(TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
    V_STRING_LENGTH := LENGTH(P_STRING);

    -- loop thru the string by 255 characters and output each chunk
    WHILE V_STRING_OFFSET < V_STRING_LENGTH LOOP
      V_CUT_POS := NVL(INSTR(P_STRING, CHR(10), V_STRING_OFFSET), 0) -
                   V_STRING_OFFSET;
      IF V_CUT_POS < 0 OR V_CUT_POS >= 255 THEN
        V_CUT_POS := 255;
        V_ADD     := 0;
      ELSE
        V_ADD := 1;
      END IF;
      DBMS_OUTPUT.PUT_LINE(SUBSTR(P_STRING, V_STRING_OFFSET, V_CUT_POS));
      V_STRING_OFFSET := V_STRING_OFFSET + V_CUT_POS + V_ADD;
    END LOOP;
  END;

  /************************************************************************************/
  /* name: drop_table (internal procedure                                             */
  /* purpose: drop table passed in the parameter (if exists)                          */
  /* parameters:                                                                      */
  /*   p_tbl_name - table to be removed                                               */
  /* author: bazyli blicharski                                                        */
  /* version: 1.00 - initial version                                                  */
  /************************************************************************************/
  PROCEDURE DROP_TABLE(P_TBL_NAME IN VARCHAR2) IS
    V_CNT NUMBER;
    V_SQL VARCHAR2(100);
  BEGIN
    SELECT COUNT(1)
      INTO V_CNT
      FROM USER_TABLES
     WHERE TABLE_NAME = P_TBL_NAME;
    IF V_CNT > 0 THEN
      V_SQL := 'DROP TABLE ' || P_TBL_NAME;
      PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                   REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
      EXECUTE IMMEDIATE V_SQL;
      PRO_PUT_LINE('Table dropped.');
    END IF;
  END;

    /************************************************************************************/
    /* name: Truncate_table (internal procedure                                         */
    /* purpose: truncate table passed in the parameter (if exists)                      */
    /* parameters:                                                                      */
    /*   p_tbl_name - table to be removed                                               */
    /* author: David Lang                                                               */
    /* version: 1.00 - initial version                                                  */
    /************************************************************************************/
    PROCEDURE TRUNCATE_TABLE(P_TBL_NAME IN VARCHAR2) IS
        V_CNT NUMBER;
        V_SQL VARCHAR2(100);
    BEGIN
        SELECT COUNT(1)
        INTO V_CNT
        FROM USER_TABLES
        WHERE TABLE_NAME = P_TBL_NAME;
        IF V_CNT > 0 THEN
            V_SQL := 'TRUNCATE TABLE ' || P_TBL_NAME;
            PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                         REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Table truncated.');
        END IF;
    END;
  /************************************************************************************/
  /* name: getclob                                            */
  /* purpose: get contest of clob and fill in varchar2                          */
  /* parameters:                                                                      */
  /*   c - source clob                                               */
  /* Reture value                                                       */
  /*   contest in varchar2 type                                                  */
  /* author: Eric Hu                                                        */
  /* version: 1.00 - initial version                                                  */
  /************************************************************************************/
  FUNCTION GETCLOB(C CLOB) RETURN VARCHAR2 IS
    BUFFER   VARCHAR2(2000);
    V_BUFFER VARCHAR2(32767);
    N_SIZE   NUMBER := DBMS_LOB.GETLENGTH(C);
    POS      NUMBER := 0;
  BEGIN
    DBMS_OUTPUT.PUT_LINE(N_SIZE);
    WHILE (N_SIZE > 0) LOOP
      BUFFER   := DBMS_LOB.SUBSTR(C, 2000, 1 + POS * 2000);
      POS      := POS + 1;
      N_SIZE   := N_SIZE - 2000;
      V_BUFFER := V_BUFFER || BUFFER;
    END LOOP;
    RETURN V_BUFFER;
  END;

  /************************************************************************************/
  /* name: duplicate_prttn_table                                            */
  /* purpose: duplicate a new table according to template table's definition                          */
  /* parameters:                                                                      */
  /*   p_tmplt_table - template table                                               */
  /*   p_new_tbl - new table                                               */
  /* author: Hill Pan                                                        */
  /* version: 1.00 - initial version                                                  */
  /************************************************************************************/

  PROCEDURE DUPLICATE_PRTTN_TABLE(P_TMPLT_TABLE IN VARCHAR2,
                                  P_NEW_TBL     IN VARCHAR2) IS
    C_TAB_DEF CLOB := '';
    V_TAB_DEF VARCHAR2(32767) := '';
    P_USER    VARCHAR2(50);
  BEGIN
    --drop table with the same name of new table if exist
    DROP_TABLE(P_NEW_TBL);
    SELECT USERNAME INTO P_USER FROM USER_USERS WHERE ROWNUM = 1;
    --Get DDL of template table
    SELECT TRIM(DBMS_METADATA.GET_DDL('TABLE', P_TMPLT_TABLE, P_USER))
      INTO C_TAB_DEF
      FROM DUAL;

    V_TAB_DEF := GETCLOB(C_TAB_DEF);
    V_TAB_DEF := REPLACE(V_TAB_DEF,
                         '"' || P_TMPLT_TABLE || '"',
                         '"' || P_NEW_TBL || '"');
    DBMS_OUTPUT.PUT_LINE(LENGTH(V_TAB_DEF));
    PRO_PUT_LINE('Length of SQL statement: ' || LENGTH(V_TAB_DEF));
    PRO_PUT_LINE(V_TAB_DEF);
    --Create table
    EXECUTE IMMEDIATE V_TAB_DEF;

  EXCEPTION
    WHEN OTHERS THEN
      --Raise error
      PRO_PUT_LINE(SQLERRM);
      RAISE_APPLICATION_ERROR(-20081,
                              'ERROR!!! When duplicate a source table for  ' ||
                              P_TMPLT_TABLE);

  END DUPLICATE_PRTTN_TABLE;

  /************************************************************************************/
  /* name: ship_agg_to_wk                                                             */
  /* purpose: aggregates shipment data for the aim of planning baseline calculation   */
  /* parameters:                                                                      */
  /*   p_order_id - control m order id                                                */
  /*   p_mth_cnt - number of months to be aggregated                                  */
  /*   p_dop - degree of parallelism to be used, default is 4                         */
  /* author: bazyli blicharski                                                        */
  /* version: 1.00 - initial version                                                  */
  /* version: 2.00 - Updated by David for F009 in R8                                  */
  /* version: 3.00 - Updated by David for B016 in R10                                 */
  /* version: 3.01 - Updated by David for fixing the issue of incorrect SU at 20100531*/
  /* version: 3.02 - Updated by David at 20100607 for avoid Ora-00600 in R10          */
  /* version: 3.03 - Updated by Eric at 20100920 for avoid Ora-00600 in R11           */
  /************************************************************************************/
  PROCEDURE AGG_TO_WK(P_ORDER_ID  IN VARCHAR2,
                      P_DATA_TYPE IN VARCHAR2,
                      P_MTH_CNT   IN NUMBER,
                      P_DOP       IN NUMBER DEFAULT 4,
					  V_REGN VARCHAR2 DEFAULT NULL) IS -- SOX IMPLEMENTATION PROJECT
    V_MIN_DAY_SKID  CAL_MASTR_DIM.CAL_MASTR_SKID%TYPE;
    V_MAX_DAY_SKID  CAL_MASTR_DIM.CAL_MASTR_SKID%TYPE;
    V_SQL           VARCHAR2(32767);
    V_SQL2           VARCHAR2(32767);
    V_TBLSPACE_NAME VARCHAR2(100) := '';
    --V_START_DATE    DATE := SYSDATE;
    V_DATA_TYPE     VARCHAR2(30) := UPPER(P_DATA_TYPE);
  BEGIN
    -- parameter control
    PRO_PUT_LINE('Checking input parameters:' || CHR(10) ||
                 '  p_order_id = ' || P_ORDER_ID || CHR(10) ||
                 '  p_data_type = ' || P_DATA_TYPE || CHR(10) ||
                 '  p_mth_cnt = ' || P_MTH_CNT || CHR(10) || '  p_dop = ' ||
                 P_DOP);
    IF P_ORDER_ID IS NULL OR
       (V_DATA_TYPE <> 'SHIP' AND V_DATA_TYPE <> 'EPOS') OR
       P_MTH_CNT IS NULL OR P_MTH_CNT NOT BETWEEN 1 AND 24 OR P_DOP IS NULL OR
       P_DOP < 1 THEN
      PRO_PUT_LINE('Error: wrong input parameters!');
      RAISE_APPLICATION_ERROR(C_WRONG_PARMS_ERROR,
                              'Error: wrong input parameters!');
    END IF;

    -- Added by David at 20100607 -- Start
    -- This is the workaround solution which will solve the bug issue ORA-00600
    -- Modified by Eric at 2010-9-20 for ORA-00600 Error --Strat
    --  V_SQL := 'alter session set "_slave_mapping_enabled"=false';
    --
    --  BEGIN
    --      EXECUTE IMMEDIATE V_SQL;
    --      PRO_PUT_LINE('Alter session sucessfully!');
    --  EXCEPTION
    --      WHEN OTHERS THEN
    --          PRO_PUT_LINE('Error: Alter session parameter failed');
    --          RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
    --                                  'Error: Alter session parameter _slave_mapping_enabled  ' ||
    --                                   ' failed with error - ' || SQLERRM);
    --  END;
    -- Modified by Eric at 2010-9-20 for ORA-00600 Error --End
    ---- Added by David at 20100607 -- End

    -- checking in_data_type (shipment or epos)
    IF V_DATA_TYPE = 'SHIP' THEN
      I_BASLN_TEMP_TBL_NAME := C_SHIP_BASLN_TEMP_TBL_NAME;
      I_AGG_TEMP_TBL_NAME   := C_SHIP_AGG_WK_TEMP_TBL_NAME;
      I_FCT_TBL_NAME        := C_SHIP_FCT_TBL_NAME;
      I_AGG_FCT_TBL_NAME    := C_SHIP_AGG_WK_TBL_NAME;
      I_BASLN_FCT_TBL_NAME  := C_SHIP_BASLN_FCT_TBL_NAME;
      I_SHIP_AGG_TEMP_TBL_NAME := C_SHIP_AGG_BFCT_TEMP_TBL_NAME;
      I_SHIP_AGG_TBL_NAME := C_SHIP_AGG_BFCT_TBL_NAME;
    ELSE
      IF V_DATA_TYPE = 'EPOS' THEN
        I_BASLN_TEMP_TBL_NAME := C_EPOS_BASLN_TEMP_TBL_NAME;
        I_AGG_TEMP_TBL_NAME   := C_EPOS_AGG_TEMP_TBL_NAME;
        I_FCT_TBL_NAME        := C_EPOS_FCT_TBL_NAME;
        I_AGG_FCT_TBL_NAME    := C_EPOS_AGG_BFCT_TBL_NAME;
        I_BASLN_FCT_TBL_NAME  := C_EPOS_BASLN_FCT_TBL_NAME;
      ELSE
        PRO_PUT_LINE('Error: wrong input DATA_TYPE parameter! should be "SHIP" or "EPOS". ');
        RAISE_APPLICATION_ERROR(C_WRONG_PARMS_ERROR,
                                'Error: wrong input DATA_TYPE parameter!');
      END IF;
    END IF;

    -- find a tablespace
    BEGIN
      SELECT 'TABLESPACE ' || TABLESPACE_NAME
        INTO V_TBLSPACE_NAME
        FROM USER_TAB_PARTITIONS
       WHERE TABLE_NAME = C_SHIP_FCT_TBL_NAME
         AND ROWNUM = 1;
    EXCEPTION
      WHEN OTHERS THEN
        PRO_PUT_LINE('Warning: Unable to recognize OPT_SHPMT_FCT tablespace, choosing default tablespace!');
    END;

    -- minimum and maximum skids
    SELECT MIN(CAL_MASTR_SKID), MAX(CAL_MASTR_SKID)
      INTO V_MIN_DAY_SKID, V_MAX_DAY_SKID
      FROM CAL_MASTR_DIM
     WHERE DAY_DATE >= ADD_MONTHS(TRUNC(SYSDATE + 6, 'MM'), -P_MTH_CNT + 1)
       AND DAY_DATE < ADD_MONTHS(TRUNC(SYSDATE + 6, 'MM'), 1);

    -- drop temporary table
    DROP_TABLE(I_AGG_TEMP_TBL_NAME);
    DROP_TABLE(I_SHIP_AGG_TEMP_TBL_NAME);

    -- inserting data
    -- Updated by David for F009 in R8
    -- Add three more measures NIV_AMT,BUoM_AMT and SU_AMT
    --Negative shipment need to be considiered in current calculation logic, so we removed function GREATEST()
    IF V_DATA_TYPE = 'SHIP' THEN

        -- prepare temporary table create statement
    		-- Modified by Eric at 2010-9-20 for ORA-00600 Error --Strat
        -- removed the parallel parameter when create partition table to avoid ORA-600 error ' PARALLEL ' || P_DOP || CHR(10) ||
    		-- Modified by Eric at 2010-9-20 for ORA-00600 Error --End
        V_SQL := 'CREATE TABLE ' || I_AGG_TEMP_TBL_NAME || CHR(10) ||
                 C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ' ' ||
                 'PARTITION BY RANGE (WK_SKID)' || CHR(10) ||
                 '(PARTITION "PMAX" VALUES LESS THAN (MAXVALUE) ' ||
                 C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ')' ||
                 CHR(10) || 'AS' || CHR(10) ||
                 '  SELECT WK_SKID, PROD_SKID, BRAND_SKID, GTIN_STTUS_CODE, ACCT_SKID, VOL_SU_AMT, VOL_GIV_AMT,VOL_NIV_AMT,VOL_BUOM_AMT, BUS_UNIT_SKID' ||
                 CHR(10) || '  FROM OPT_SHPMT_WK_FCT' || CHR(10) ||
                 '  WHERE rownum < 1';
        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));

      BEGIN
            EXECUTE IMMEDIATE V_SQL;
            -- initialize partitions
            OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN(P_VC2_TBL_NAME => I_AGG_TEMP_TBL_NAME,
                                                        P_NUM_PAST_MTH_NUM => P_MTH_CNT - 1,  -- change partition num from 24 to 50, by Kingham on 20-Nov-2009 PR 72181 [Kalyan added for Shipment Historical]
                                                        P_NUM_FUTURE_MTH_NUM => 0,
                                                        P_VC2_DROP_PRTTN_FLAG => 'N');

            PRO_PUT_LINE('Table created successfuly');
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table failed with error - ' ||
                             SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table failed with error - ' ||
                                         SQLERRM);
        END;
      -- inserting data to opt_shpmt_wk_fct_tmp
        V_SQL := 'INSERT /*+ APPEND PARALLEL(' || P_DOP || ') */ INTO ' ||
                 I_AGG_TEMP_TBL_NAME ||
                 ' (WK_SKID, PROD_SKID, BRAND_SKID,BUS_UNIT_SKID,GTIN_STTUS_CODE, ACCT_SKID, VOL_SU_AMT,VOL_GIV_AMT,VOL_NIV_AMT,VOL_BUOM_AMT )' ||CHR(10) ||
                 '  SELECT /*+ ORDERED USE_HASH(W C S) */' ||CHR(10) ||
                 '         C.WK_SKID,' || CHR(10) ||
                 '         S.PROD_SKID,' || CHR(10) ||
                 '         S.BRAND_SKID,' ||CHR(10) ||
                 '         S.BUS_UNIT_SKID,' || CHR(10) ||
                 '         S.STTUS_CODE AS GTIN_STTUS_CODE,' || CHR(10) ||
                 '         S.ACCT_SKID,' || CHR(10) ||
                 '         SUM(NVL(S.VOL_SU_AMT, 0)) AS VOL_SU_AMT,' || CHR(10) ||
                 '         SUM(NVL(S.VOL_GIV_AMT, 0)) AS VOL_GIV_AMT,' || CHR(10) ||
                 '         SUM(NVL(S.VOL_NIV_AMT, 0)) AS VOL_NIV_AMT,' || CHR(10) ||
                 '         SUM(NVL(S.VOL_BUOM_AMT, 0)) AS VOL_BUOM_AMT' || CHR(10) ||
                 '  FROM (SELECT WK_SKID' || CHR(10) ||
                 '        FROM CAL_MASTR_DIM' || CHR(10) ||
                 '        WHERE DAY_DATE >= ADD_MONTHS(TRUNC(SYSDATE+6, ''MM''), -' || P_MTH_CNT || ' + 1) - 6' || CHR(10) ||
                 '        AND DAY_DATE < ADD_MONTHS(TRUNC(SYSDATE+6, ''MM''), 1)' || CHR(10) ||
                 '        GROUP BY WK_SKID' || CHR(10) ||
                 '        HAVING MIN(MTH_START_DATE) >= ADD_MONTHS(TRUNC(SYSDATE+6, ''MM''), -' || P_MTH_CNT || ' + 1)) W,' || CHR(10) ||
                 '        CAL_MASTR_DIM C,' || CHR(10) ||
                 '       (SELECT /*+ NO_MERGE ORDERED USE_HASH(A S P1 P2) NO_INDEX(S) */' || CHR(10) ||
                 '              S.SHPMT_DATE_SKID, S.PROD_SKID, P2.PROD_SKID as BRAND_SKID, A.BUS_UNIT_SKID, P1.STTUS_CODE, S.ACCT_SKID, S.VOL_SU_AMT, S.VOL_BUOM_AMT, S.GIV_AMT AS VOL_GIV_AMT, S.NIV_AMT AS VOL_NIV_AMT' ||CHR(10) ||
                 '        FROM OPT_ACCT_DIM A, OPT_SHPMT_FCT S, OPT_PROD_DIM P1, OPT_PROD_DIM P2' ||CHR(10) ||
                 '        WHERE S.SHPMT_DATE_SKID between ' || V_MIN_DAY_SKID || ' and ' || V_MAX_DAY_SKID || CHR(10) ||
                 '        AND S.ACCT_SKID = A.ACCT_SKID' || CHR(10) ||
                 '        AND S.PROD_SKID = P1.PROD_SKID' || CHR(10) ||
                 '        AND P1.BRAND_ID = P2.PROD_ID' || CHR(10) ||
                 '        AND A.ACCT_TYPE_DESC = ''Ship-to location''' ||') S' || CHR(10) ||
                 '  WHERE S.SHPMT_DATE_SKID = C.CAL_MASTR_SKID' || CHR(10) ||
                 '  AND C.WK_SKID = W.WK_SKID' || CHR(10) ||
                 '  GROUP BY C.WK_SKID, S.PROD_SKID, S.BRAND_SKID, S.BUS_UNIT_SKID, S.STTUS_CODE, S.ACCT_SKID';
        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
        BEGIN
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Data created successfuly (' || SQL%ROWCOUNT ||
                         ' rows processed)');
            COMMIT;
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error:  Data creation on ' ||
                             I_AGG_TEMP_TBL_NAME ||
                             ' failed with error - ' || SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_INS_ERROR,
                                        'Error:  Data creation on ' ||
                                         I_AGG_TEMP_TBL_NAME ||
                                         ' failed with error - ' || SQLERRM);
        END;

        -- Updated by David for fixing the issue of incorrect SU at 20100531 -- Start
    		-- Modified by Eric at 2010-9-20 for ORA-00600 Error --Strat
        -- removed the parallel parameter when create partition table to avoid ORA-600 error ' PARALLEL ' || P_DOP || CHR(10) ||
    		-- Modified by Eric at 2010-9-20 for ORA-00600 Error --End
        V_SQL2 := 'CREATE TABLE ' || I_SHIP_AGG_TEMP_TBL_NAME || CHR(10) ||
                 C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME ||  ' ' ||
                 'PARTITION BY RANGE (WK_SKID)' || CHR(10) ||
                 '(PARTITION "PMAX" VALUES LESS THAN (MAXVALUE) ' ||
                 C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME || ')' ||
                 CHR(10) || 'AS' || CHR(10) ||
                 '  SELECT * FROM OPT_SHPMT_AGG_BFCT' || CHR(10) ||
                 '  WHERE rownum < 1';
        PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL2, CHR(10), CHR(10) || '> '));
        -- Updated by David for fixing the issue of incorrect SU at 20100531 -- End

        -- Initialize partitions for opt_shpmt_wk_fct_tmp
        BEGIN
         EXECUTE IMMEDIATE V_SQL2;
            OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN(P_VC2_TBL_NAME => I_SHIP_AGG_TEMP_TBL_NAME,
                                                        P_NUM_PAST_MTH_NUM => P_MTH_CNT - 1,  -- change partition num from 24 to 50, by Kingham on 20-Nov-2009 PR 72181 [Kalyan added for Shipment Historical]
                                                        P_NUM_FUTURE_MTH_NUM => 0,
                                                        P_VC2_DROP_PRTTN_FLAG => 'N');
            PRO_PUT_LINE('Table created successfuly');
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error: Creation of temporary table failed with error - ' ||
                             SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                        'Error: Creation of temporary table failed with error - ' ||
                                         SQLERRM);
        END;
        -- Insert data to opt_shpmt_agg_bfct_tmp
        V_SQL := 'INSERT /*+ APPEND PARALLEL(' || P_DOP || ') */ INTO ' ||
                 I_SHIP_AGG_TEMP_TBL_NAME ||
                  ' (WK_SKID, BUS_UNIT_SKID, PROD_SKID, ACCT_SKID, SU_AMT,GIV_AMT,NIV_AMT,BUOM_AMT)' ||CHR(10) ||
                 '  SELECT' ||CHR(10) ||
                 '         WK_SKID,' || CHR(10) ||
                 '         BUS_UNIT_SKID,' || CHR(10) ||
                 '         BRAND_SKID AS PROD_SKID,' ||CHR(10) ||
                 '         ACCT_SKID,' || CHR(10) ||
                 '         SUM(NVL(VOL_SU_AMT, 0)) AS SU_AMT,' || CHR(10) ||
                 '         SUM(NVL(VOL_GIV_AMT, 0)) AS GIV_AMT,' || CHR(10) ||
                 '         SUM(NVL(VOL_NIV_AMT, 0)) AS NIV_AMT,' || CHR(10) ||
                 '         SUM(NVL(VOL_BUOM_AMT, 0)) AS BUOM_AMT' || CHR(10) ||
                 '  FROM ' || CHR(10) || I_AGG_TEMP_TBL_NAME || CHR(10) ||
                 '        GROUP BY WK_SKID,BUS_UNIT_SKID,BRAND_SKID,ACCT_SKID';

        BEGIN
            PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                     REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
            EXECUTE IMMEDIATE V_SQL;
            PRO_PUT_LINE('Data created successfuly (' || SQL%ROWCOUNT ||
                         ' rows processed)');
            COMMIT;
        EXCEPTION
            WHEN OTHERS THEN
                PRO_PUT_LINE('Error:  Data creation on ' ||
                             I_SHIP_AGG_TEMP_TBL_NAME ||
                             ' failed with error - ' || SQLERRM);
                RAISE_APPLICATION_ERROR(C_TBL_INS_ERROR,
                                        'Error:  Data creation on ' ||
                                         I_SHIP_AGG_TEMP_TBL_NAME ||
                                         ' failed with error - ' || SQLERRM);
        END;

        -- promote data to final OPT_SHPMT_AGG_BFCT table
    BEGIN
      PRO_PUT_LINE('Starting data promotion from ' || I_SHIP_AGG_TEMP_TBL_NAME ||
                   ' to ' || I_SHIP_AGG_TBL_NAME || '...');

      -- Added by David at 20100607 -- Start
      -- truncate table
      TRUNCATE_TABLE(I_SHIP_AGG_TBL_NAME);
      -- Added by David at 20100607 -- End

      -- create missing partitions
      OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN(P_VC2_TBL_NAME        => I_SHIP_AGG_TBL_NAME,
                                                  P_NUM_PAST_MTH_NUM    => P_MTH_CNT - 1,
                                                  P_NUM_FUTURE_MTH_NUM  => 0,
                                                  P_VC2_DROP_PRTTN_FLAG => 'N');
      -- promote data
      OPT_PKG_PRTTN_UTIL.PRO_PROMOTE_DATA(P_VC2_SRCE_TBL        => I_SHIP_AGG_TEMP_TBL_NAME,
                                          P_VC2_DEST_TBL        => I_SHIP_AGG_TBL_NAME,
                                          P_VC2_MIN_CAL_SKID    => NULL,
                                          P_VC2_REGION          => '',
                                          P_VC2_CTRLM_ORDR_ID   => P_ORDER_ID,
                                          P_VC2_PRTTN_TYPE_CODE => 'M',
										  V_REGN);--SOX IMPLEMENTATION PROJECT
      PRO_PUT_LINE('Data promoted succesfully.');
    EXCEPTION
      WHEN OTHERS THEN
        PRO_PUT_LINE('Error: Promotion of data from ' ||
                     I_SHIP_AGG_TEMP_TBL_NAME || ' to ' || I_SHIP_AGG_TBL_NAME ||
                     ' failed with error - ' || SQLERRM);
        RAISE_APPLICATION_ERROR(C_DATA_PROMO_ERROR,
                                'Error: Promotion of data from ' ||
                                I_AGG_TEMP_TBL_NAME || ' to ' ||
                                I_AGG_FCT_TBL_NAME ||
                                ' failed with error - ' || SQLERRM);


    END;
    --Drop temp table OPT_SHPMT_AGG_BFCT_TMP
    --DROP_TABLE(I_SHIP_AGG_TEMP_TBL_NAME);

   -- gather statistics for opt_shpmt_agg_bfct
    PRO_PUT_LINE('Starting gathering statistics on ' || I_SHIP_AGG_TBL_NAME ||
                 ' table...');

    DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV',
                                                                  'CURRENT_SCHEMA'),
                                  TABNAME          => I_SHIP_AGG_TBL_NAME,
                                  GRANULARITY      => 'GLOBAL',
                                  ESTIMATE_PERCENT => C_STATS_FINAL_EST_PERCENT,
                                  BLOCK_SAMPLE     => FALSE,
                                  DEGREE           => 1,
                                  METHOD_OPT       => 'FOR ALL COLUMNS SIZE AUTO');

    PRO_PUT_LINE('Statistics on table ' || I_SHIP_AGG_TBL_NAME ||
                 ' table gathered');
   /* PRO_PUT_LINE('Starting gathering statistics on refreshed partitions of ' ||
                 C_SHIP_AGG_BFCT_TBL_NAME || ' table...');*/
    /*FOR P IN (SELECT OBJECT_NAME, SUBOBJECT_NAME
                FROM USER_OBJECTS
               WHERE OBJECT_NAME = C_SHIP_AGG_BFCT_TBL_NAME
                 AND OBJECT_TYPE = 'TABLE PARTITION'
                 AND LAST_DDL_TIME >= V_START_DATE) LOOP

     PRO_PUT_LINE('Gathering statistics for ' || P.OBJECT_NAME || '.' ||
                   P.SUBOBJECT_NAME);

     DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV',
                                                                    'CURRENT_SCHEMA'),
                                    TABNAME          => P.OBJECT_NAME,
                                    PARTNAME         => P.SUBOBJECT_NAME,
                                    GRANULARITY      => 'PARTITION',
                                    ESTIMATE_PERCENT => C_STATS_FINAL_EST_PERCENT,
                                    BLOCK_SAMPLE     => FALSE,
                                    DEGREE           => 1,
                                    METHOD_OPT       => 'FOR ALL COLUMNS SIZE AUTO');
    END LOOP;*/

    ELSE
      IF V_DATA_TYPE = 'EPOS' THEN

        DUPLICATE_PRTTN_TABLE(I_AGG_FCT_TBL_NAME, I_AGG_TEMP_TBL_NAME);

        V_SQL := 'INSERT /*+ APPEND PARALLEL(' || P_DOP || ') */ INTO ' ||
                 I_AGG_TEMP_TBL_NAME ||
                 ' (WK_SKID, BUS_UNIT_SKID, PROD_SKID, ACCT_SKID, GIV_AMT)' ||CHR(10) ||
                 '  SELECT /*+ ORDERED USE_HASH(W C S) */' || CHR(10) ||
                 '         C.WK_SKID,' || CHR(10) ||
                 '         S.BUS_UNIT_SKID,' || CHR(10) ||
                 '         S.PROD_SKID,' || CHR(10) ||
                 '         S.ACCT_SKID,' || CHR(10) ||
                 '         GREATEST(SUM(NVL(S.GIV_AMT, 0)), 0) AS GIV_AMT' ||CHR(10) ||
                  '  FROM (SELECT WK_SKID' || CHR(10) ||
                 '        FROM CAL_MASTR_DIM' || CHR(10) ||
                 '        WHERE DAY_DATE >= ADD_MONTHS(TRUNC(SYSDATE+6, ''MM''), -' ||
                 P_MTH_CNT || ' + 1) - 6' || CHR(10) ||
                 '        AND DAY_DATE < ADD_MONTHS(TRUNC(SYSDATE+6, ''MM''), 1)' ||CHR(10) ||
                 '        GROUP BY WK_SKID' || CHR(10) ||
                 '        HAVING MIN(MTH_START_DATE) >= ADD_MONTHS(TRUNC(SYSDATE+6, ''MM''), -' ||
                 P_MTH_CNT || ' + 1)) W,' || CHR(10) ||
                 '        CAL_MASTR_DIM C,' || CHR(10) ||
                 '       (SELECT /*+ NO_MERGE ORDERED USE_HASH(A S) NO_INDEX(S) */' ||CHR(10) ||
                 '               S.DATE_SKID, S.BUS_UNIT_SKID, P2.PROD_SKID, S.ACCT_SKID, S.GIV_AMT' ||CHR(10) ||
                 '        FROM OPT_PROD_DIM P1,  OPT_PROD_DIM P2, ' ||
                 I_FCT_TBL_NAME || ' S' || CHR(10) ||
                 '        WHERE S.DATE_SKID between ' || V_MIN_DAY_SKID ||
                 ' and ' || V_MAX_DAY_SKID || CHR(10) ||
                 '        AND S.PROD_SKID = P1.PROD_SKID ' || CHR(10) ||
                 '        AND   P1.BRAND_ID = P2.PROD_ID ' || ') S' ||CHR(10) ||
                 '  WHERE S.DATE_SKID = C.CAL_MASTR_SKID' ||CHR(10) ||
                 '  AND C.WK_SKID = W.WK_SKID' || CHR(10) ||
                 '  GROUP BY C.WK_SKID, S.BUS_UNIT_SKID, S.PROD_SKID, S.ACCT_SKID';

    PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                 REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
    BEGIN
      EXECUTE IMMEDIATE V_SQL;
      PRO_PUT_LINE('Data created successfuly (' || SQL%ROWCOUNT ||
                   ' rows processed)');
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        PRO_PUT_LINE('Error:  Data creation on ' || I_AGG_TEMP_TBL_NAME ||
                     ' failed with error - ' || SQLERRM);
        RAISE_APPLICATION_ERROR(C_TBL_INS_ERROR,
                                'Error:  Data creation on ' ||
                                I_AGG_TEMP_TBL_NAME ||
                                ' failed with error - ' || SQLERRM);
    END;
      END IF;
    END IF;



    -- promote data to final OPT_SHPMT_WK_FCT table
    BEGIN

      -- Added by David at 20100607 -- Start
      -- truncate table
      TRUNCATE_TABLE(I_AGG_FCT_TBL_NAME);
      -- Added by David at 20100607 -- End

      PRO_PUT_LINE('Starting data promotion from ' || I_AGG_TEMP_TBL_NAME ||
                   ' to ' || I_AGG_FCT_TBL_NAME || '...');
      -- create missing partitions
      OPT_PKG_PRTTN_UTIL.PRO_INITIALIZE_MTH_PRTTN(P_VC2_TBL_NAME        => I_AGG_FCT_TBL_NAME,
                                                  P_NUM_PAST_MTH_NUM    => P_MTH_CNT - 1,
                                                  P_NUM_FUTURE_MTH_NUM  => 0,
                                                  P_VC2_DROP_PRTTN_FLAG => 'N');
      -- promote data
      OPT_PKG_PRTTN_UTIL.PRO_PROMOTE_DATA(P_VC2_SRCE_TBL        => I_AGG_TEMP_TBL_NAME,
                                          P_VC2_DEST_TBL        => I_AGG_FCT_TBL_NAME,
                                          P_VC2_MIN_CAL_SKID    => NULL,
                                          P_VC2_REGION          => '',
                                          P_VC2_CTRLM_ORDR_ID   => P_ORDER_ID,
                                          P_VC2_PRTTN_TYPE_CODE => 'M',
										  V_REGN);--SOX IMPLEMENTATION PROJECT
      PRO_PUT_LINE('Data promoted succesfully.');
    EXCEPTION
      WHEN OTHERS THEN
        PRO_PUT_LINE('Error: Promotion of data from ' ||
                     I_AGG_TEMP_TBL_NAME || ' to ' || I_AGG_FCT_TBL_NAME ||
                     ' failed with error - ' || SQLERRM);
        RAISE_APPLICATION_ERROR(C_DATA_PROMO_ERROR,
                                'Error: Promotion of data from ' ||
                                I_AGG_TEMP_TBL_NAME || ' to ' ||
                                I_AGG_FCT_TBL_NAME ||
                                ' failed with error - ' || SQLERRM);
    END;

    -- drop temporary table
    -- DROP_TABLE(I_AGG_TEMP_TBL_NAME);

    -- gather statistics for opt_shpmt_wk_fct
    PRO_PUT_LINE('Starting gathering statistics on ' || I_AGG_FCT_TBL_NAME ||
                 ' table...');

    DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV',
                                                                  'CURRENT_SCHEMA'),
                                  TABNAME          => I_AGG_FCT_TBL_NAME,
                                  GRANULARITY      => 'GLOBAL',
                                  ESTIMATE_PERCENT => C_STATS_FINAL_EST_PERCENT,
                                  BLOCK_SAMPLE     => FALSE,
                                  DEGREE           => 1,
                                  METHOD_OPT       => 'FOR ALL COLUMNS SIZE AUTO');

    PRO_PUT_LINE('Statistics on table ' || I_AGG_FCT_TBL_NAME ||
                 ' table gathered');
    PRO_PUT_LINE('Starting gathering statistics on refreshed partitions of ' ||
                 I_AGG_FCT_TBL_NAME || ' table...');

    /*FOR P IN (SELECT OBJECT_NAME, SUBOBJECT_NAME
                FROM USER_OBJECTS
               WHERE OBJECT_NAME = I_AGG_FCT_TBL_NAME
                 AND OBJECT_TYPE = 'TABLE PARTITION'
                 AND LAST_DDL_TIME >= V_START_DATE) LOOP

     PRO_PUT_LINE('Gathering statistics for ' || P.OBJECT_NAME || '.' ||
                   P.SUBOBJECT_NAME);

     DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV',
                                                                    'CURRENT_SCHEMA'),
                                    TABNAME          => P.OBJECT_NAME,
                                    PARTNAME         => P.SUBOBJECT_NAME,
                                    GRANULARITY      => 'PARTITION',
                                    ESTIMATE_PERCENT => C_STATS_FINAL_EST_PERCENT,
                                    BLOCK_SAMPLE     => FALSE,
                                    DEGREE           => 1,
                                    METHOD_OPT       => 'FOR ALL COLUMNS SIZE AUTO');
    END LOOP;*/

  EXCEPTION
    WHEN OTHERS THEN
      PRO_PUT_LINE('Error: Unexpected system error - ' || SQLERRM);
      RAISE;

  END;

  /************************************************************************************/
  /* name: post_basln_calc                                                            */
  /* purpose: calculates post-event baseline (brand x acct level)                     */
  /* parameters:                                                                      */
  /*   p_order_id - control m order id                                                */
  /*   p_mth_cnt - number of months to be calculated                                  */
  /*   p_dop - degree of parallelism to be used, default is 4                         */
  /* author: bazyli blicharski                                                        */
  /* version: 1.00 - initial version                                                  */
  /*          2.00 - get rid of p_regn_code parameter, product dated hierarchies      */
  /*                 adjustment                                                       */
  /*          2.01 - use pgm_start_date, pgm_end_date to process EPOS baseline        */
  /*                 instead of shpmt_start_date, shpmt_end_date                      */
  /*          2.03 -  Updated by David for F009 in R8                                 */
  /*          3.00 -  Updated by David for B011 in R9 on 2009-07-10                   */
  /*          4.00 -  Updated by Bean for B004 in R9 on 2009-09-04,                   */
  /*                  which is create a dummy table for basln data filling in         */
  /*          5.00 -  Updated by David for performance tunning in R10 on 2010-05-19   */
  /*          5.01 -  Updated by David at 20100526 for fixing prmtn_skid missed in R10*/
  /*          5.02 -  Updated by David at 20100607 for performance tunning in R10     */
  /*          5.03 -  Updated by David at 20100607 for avoid Ora-00600 in R10         */
  /************************************************************************************/
  PROCEDURE POST_BASLN_CALC(P_ORDER_ID  IN VARCHAR2,
                            P_DATA_TYPE IN VARCHAR2,
                            P_MTH_CNT   IN NUMBER,
                            P_DOP       IN NUMBER DEFAULT 4) IS
    V_SQL                  VARCHAR2(32767);
    V_TBLSPACE_NAME        VARCHAR2(100) := '';
    V_MIN_WK_SKID          CAL_MASTR_DIM.WK_SKID%TYPE;
    V_FY_MAP               VARCHAR2(1000) := '';
    V_DATA_TYPE            VARCHAR2(30) := UPPER(P_DATA_TYPE);
    V_PRMTN_START_END_DATE VARCHAR2(1500) := ''; -- prmomotion start date and promotion end date


  BEGIN
    -- parameter control
    PRO_PUT_LINE('Checking input parameters:' || CHR(10) ||
                 '  p_order_id = ' || P_ORDER_ID || CHR(10) ||
                 '  p_data_type = ' || P_DATA_TYPE || CHR(10) ||
                 '  p_mth_cnt = ' || P_MTH_CNT || CHR(10) || '  p_dop = ' ||
                 P_DOP);
    -- Check the parameters
    BEGIN
      IF P_ORDER_ID IS NULL OR
         (V_DATA_TYPE <> 'SHIP' AND V_DATA_TYPE <> 'EPOS') OR
         P_MTH_CNT IS NULL OR P_MTH_CNT NOT BETWEEN 1 AND 24 OR
         P_DOP IS NULL OR P_DOP < 1 THEN
        PRO_PUT_LINE('Error: wrong input parameters!');
        RAISE_APPLICATION_ERROR(C_WRONG_PARMS_ERROR,
                                'Error: wrong input parameters!');
      END IF;
    END;
    BEGIN
      IF V_DATA_TYPE = 'SHIP' THEN
        I_BASLN_TEMP_TBL_NAME  := C_SHIP_BASLN_TEMP_TBL_NAME;
        --I_AGG_TEMP_TBL_NAME    := C_SHIP_AGG_WK_TEMP_TBL_NAME;
        --I_FCT_TBL_NAME         := C_SHIP_FCT_TBL_NAME;
        I_AGG_FCT_TBL_NAME     := C_SHIP_AGG_BFCT_TBL_NAME;
        I_BASLN_FCT_TBL_NAME   := C_SHIP_BASLN_FCT_TBL_NAME;
        V_PRMTN_START_END_DATE := '  DECODE((CASE WHEN prmtn_stop_date IS NOT NULL AND prmtn_stop_date > pgm_start_date' ||CHR(10) ||
                                  '                        AND prmtn_stop_date < pgm_end_date THEN prmtn_stop_date ELSE shpmt_end_date END),' ||CHR(10) ||
                                  '                        NULL,' ||CHR(10) ||
                                  '                        pgm_start_date,' ||CHR(10) ||
                                  '                        shpmt_start_date)' ||CHR(10) ||
                                  '                as start_date,' ||CHR(10) ||
                                  '                DECODE((CASE WHEN prmtn_stop_date IS NOT NULL AND prmtn_stop_date > pgm_start_date' ||CHR(10) ||
                                  '                        AND prmtn_stop_date < pgm_end_date THEN prmtn_stop_date ELSE shpmt_end_date END),' ||CHR(10) ||
                                  '                        NULL,' ||CHR(10) ||
                                  '                        pgm_end_date,' ||CHR(10) ||
                                  '                        (CASE WHEN prmtn_stop_date IS NOT NULL AND prmtn_stop_date > pgm_start_date' ||CHR(10) ||
                                  '                         AND prmtn_stop_date < pgm_end_date THEN prmtn_stop_date ELSE shpmt_end_date END))' ||CHR(10) ||
                                  '                as end_date,';
      ELSIF V_DATA_TYPE = 'EPOS' THEN
        I_BASLN_TEMP_TBL_NAME  := C_EPOS_BASLN_TEMP_TBL_NAME;
        --I_AGG_TEMP_TBL_NAME    := C_EPOS_AGG_TEMP_TBL_NAME;
        --I_FCT_TBL_NAME         := C_EPOS_FCT_TBL_NAME;
        I_AGG_FCT_TBL_NAME     := C_EPOS_AGG_BFCT_TBL_NAME;
        I_BASLN_FCT_TBL_NAME   := C_EPOS_BASLN_FCT_TBL_NAME;
        V_PRMTN_START_END_DATE := '                pgm_start_date ' ||CHR(10) ||
                                  '                as start_date,' ||CHR(10) ||
                                  '                (CASE WHEN prmtn_stop_date IS NOT NULL AND prmtn_stop_date > pgm_start_date' ||CHR(10) ||
                                  '                        AND prmtn_stop_date < pgm_end_date THEN prmtn_stop_date ELSE pgm_end_date END) ' ||CHR(10) ||
                                   '                as end_date,';
      ELSE
        PRO_PUT_LINE('Error: wrong input DATA_TYPE parameter! should be "SHIP" or "EPOS". ');
        RAISE_APPLICATION_ERROR(C_WRONG_PARMS_ERROR,
                                'Error: wrong input DATA_TYPE parameter!');
      END IF;
    END;

    -- find a tablespace
    BEGIN
      SELECT 'TABLESPACE ' || TABLESPACE_NAME
        INTO V_TBLSPACE_NAME
        FROM USER_TAB_PARTITIONS
       WHERE TABLE_NAME = C_SHIP_FCT_TBL_NAME
         AND ROWNUM = 1;
    EXCEPTION
      WHEN OTHERS THEN
        PRO_PUT_LINE('Warning: Unable to recognize OPT_SHPMT_FCT tablespace, choosing default tablespace!');
    END;

    -- Added by David at 20100607 -- Start
    -- This is the workaround solution which will solve the bug issue ORA-00600
    -- Modified by Eric at 2010-9-20 for ORA-00600 Error --Strat
    --  V_SQL := 'alter session set "_slave_mapping_enabled"=false';
    --
    --  BEGIN
    --      EXECUTE IMMEDIATE V_SQL;
    --      PRO_PUT_LINE('Alter session sucessfully!');
    --  EXCEPTION
    --      WHEN OTHERS THEN
    --          PRO_PUT_LINE('Error: Alter session parameter failed');
    --          RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
    --                                  'Error: Alter session parameter _slave_mapping_enabled  ' ||
    --                                   ' failed with error - ' || SQLERRM);
    --  END;
    -- Modified by Eric at 2010-9-20 for ORA-00600 Error --End
    ---- Added by David at 20100607 -- End

      -- Added by David for performance tunning in R10 on 2010-5-19
      -- it is to store the promotion account and ship-to accounts assoc
    		-- Modified by Eric at 2010-9-20 for ORA-00600 Error --Strat
        -- removed the parallel parameter when create partition table to avoid ORA-600 error ' PARALLEL ' || P_DOP || CHR(10) ||
    		-- Modified by Eric at 2010-9-20 for ORA-00600 Error --End
      V_SQL := 'CREATE TABLE OPT_PRMTN_ACCT_ASSOC_SDIM_TEMP'  || CHR(10)||
              C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME ||
              '  nologging compress   AS' || CHR(10) ||
              '  SELECT DISTINCT A.ACCT_SKID, A.ASSOC_ACCT_SKID AS ROOT_ACCT_SKID' || CHR(10) ||
              '      FROM  OPT_ACCT_ASDN_TYPE2_DIM A, OPT_ACCT_DIM B' || CHR(10) ||
              '  WHERE A.ACCT_SKID = B.ACCT_SKID ' || CHR(10) ||
              '    AND B.ACCT_TYPE_DESC = ''Ship-to location'' ' || CHR(10) ||
              '    AND A.ASSOC_ACCT_SKID IN (SELECT ACCT_SKID FROM OPT_PRMTN_DIM)';
      BEGIN
      DROP_TABLE('OPT_PRMTN_ACCT_ASSOC_SDIM_TEMP');
      EXECUTE IMMEDIATE V_SQL;
      PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                 REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
      PRO_PUT_LINE('Table created successfuly (' || SQL%ROWCOUNT ||
                   ' rows processed).');

      -- statistics
      PRO_PUT_LINE('Starting gathering statistics process for ' ||
                 'OPT_PRMTN_ACCT_ASSOC_SDIM_TEMP' || ' table (estimate percent = ' ||
                 C_STATS_TEMP_EST_PERCENT || ')...');
      DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV',
                                                                  'CURRENT_SCHEMA'),
                                  TABNAME          => 'OPT_PRMTN_ACCT_ASSOC_SDIM_TEMP',
                                  ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                  METHOD_OPT       => 'FOR COLUMNS SIZE 1',
                                  CASCADE          => FALSE);
      PRO_PUT_LINE('Statistics gathered.');

      EXCEPTION
      WHEN OTHERS THEN
        PRO_PUT_LINE('Error: Creation of temporary table OPT_PRMTN_ACCT_ASSOC_SDIM_TEMP failed with error - ' ||
                     SQLERRM);
        RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                'Error: Creation of temporary table OPT_PRMTN_ACCT_ASSOC_SDIM_TEMP failed with error - ' || SQLERRM);
      END;

    -- prepare temporary table with promoted products (data selection with filtering and account location)
    -- Modified by Myra on 20090929 in R9, implement compression
    -- Updated by David for performance tuning in R10 on 2010-5-19, which is to filter invalid promotion data to be considered in post baseline calculation
    -- Updated by David at 20100526 for fixing prmtn_skid missed -- Start
    		-- Modified by Eric at 2010-9-20 for ORA-00600 Error --Strat
        -- removed the parallel parameter when create partition table to avoid ORA-600 error ' PARALLEL ' || P_DOP || CHR(10) ||
    		-- Modified by Eric at 2010-9-20 for ORA-00600 Error --End
    V_SQL := 'CREATE TABLE ' || CHR(10) || I_BASLN_TEMP_TBL_NAME || CHR(10) ||
             ' (' || CHR(10) ||
             '  bus_unit_skid not null,' || CHR(10) ||
             '  prmtn_skid not null,' || CHR(10) ||
             '  acct_skid not null,' || CHR(10) ||
             '  root_acct_skid not null,' || CHR(10) ||
             '  prod_skid not null,' || CHR(10) ||
             '  wk_skid not null,' || CHR(10) ||
             '  valid_prmtn_ind,' || CHR(10) ||
             '  post_basln_factr,' || CHR(10) ||
             '  cann_ind' || CHR(10) ||
             ')' || CHR(10) || C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME ||
             ' nologging compress AS' || CHR(10) ||
             'SELECT /*+ USE_HASH(h prom s) */' || CHR(10) ||
             '  prom.bus_unit_skid,' || CHR(10) ||
             '  prom.prmtn_skid,' || CHR(10) ||
             '  h.acct_skid,' || CHR(10) ||
             '  h.root_acct_skid,' || CHR(10) ||
             '  prom.prod_skid,' || CHR(10) ||
             '  c.wk_skid,' || CHR(10) ||
             '  prom.valid_prmtn_ind, ' || CHR(10) ||
             '  prom.post_basln_factr, ' || CHR(10) ||
             '  case when prom.end_date < c.wk_start_date then ''Y'' else ''N'' end as cann_ind' || CHR(10) ||
             ' FROM OPT_PRMTN_ACCT_ASSOC_SDIM_TEMP h,' || CHR(10) ||
             '     (SELECT /*+ NO_MERGE ORDERED USE_HASH(prom prod) */' || CHR(10) ||
             '        prom.bus_unit_skid, prom.acct_skid, prom.prmtn_skid, prod.prod_skid, prom.start_date, prom.end_date, prod.post_basln_factr,' || CHR(10) ||
             '         case when prom.end_date - prom.start_date < prod.max_prmtn_lngth then ''Y'' else ''N'' end as valid_prmtn_ind, ' || CHR(10) ||
             '        (prom.end_date + prom.CNBLN_WK_CNT * 7) as cann_end_date' || CHR(10) ||
             '      FROM' || CHR(10) ||
             '        -- business unit max promotion length defaults' || CHR(10) ||
             '        (SELECT bus_unit_skid, acct_skid, prmtn_skid,' || CHR(10) ||
             '        '||V_PRMTN_START_END_DATE || CHR(10) ||
             '         CNBLN_WK_CNT ' || CHR(10) ||
             '         FROM OPT_PRMTN_DIM' || CHR(10) ||
             '         WHERE prmtn_sttus_code in (''Approved'', ''Confirmed'', ''Completed'')' || CHR(10) ||
             '         AND nvl(prmtn_stop_date, pgm_end_date) >= pgm_start_date' || CHR(10) ||
             '        ) prom,' || CHR(10) ||
             '        (SELECT /*+ NO_MERGE ORDERED USE_HASH(cat prod) */' || CHR(10) ||
             '           prod.prmtn_skid,' || CHR(10) ||
             '           prod.prod_skid,' || CHR(10) ||
             '           cat.post_basln_factr,' || CHR(10) ||
             '           cat.max_prmtn_lngth' || CHR(10) ||
             '         FROM' || CHR(10) ||
             '           -- category level max promotion length defaults' || CHR(10) ||
             '           (SELECT /*+ NO_MERGE ORDERED USE_HASH(bu cat) */' || CHR(10) ||
             '              cat.prod_skid,' || CHR(10) ||
             '              nvl(cat.post_basln_factr, nvl(bu.post_basln_factr,1)) as post_basln_factr, -- Added by David in R9 on 2009-07-10' || CHR(10) ||
             '              nvl(cat.max_prmtn_lngth, bu.max_prmtn_lngth) as max_prmtn_lngth' || CHR(10) ||
             '            FROM' || CHR(10) ||
             '              -- business unit max promotion length defaults' || CHR(10) ||
             '              (SELECT' || CHR(10) ||
             '                 bu.bus_unit_skid,' || CHR(10) ||
             '                 nvl(d.post_basln_factr, g.post_basln_factr) as post_basln_factr, -- Added by David in R9 on 2009-07-10' || CHR(10) ||
             '                 nvl(d.max_prmtn_lngth, g.max_prmtn_lngth) as max_prmtn_lngth' || CHR(10) ||
             '               FROM OPT_BUS_UNIT_DIM bu, OPT_BUS_UNIT_PLC g, OPT_BUS_UNIT_PLC d' || CHR(10) ||
             '               WHERE g.bus_unit_skid = 0' || CHR(10) ||
             '               AND d.deflt_lvl_name is not null' || CHR(10) ||
             '               AND d.bus_unit_skid (+) <> 0' || CHR(10) ||
             '               AND bu.bus_unit_skid = d.bus_unit_skid (+)' || CHR(10) ||
             '              ) bu,' || CHR(10) ||
             '              (SELECT /*+ NO_MERGE */' || CHR(10) ||
             '                 h.prod_skid, h.bus_unit_skid, max(cat.post_basln_factr) post_basln_factr, max(cat.max_prmtn_lngth_qty) keep (dense_rank first order by h.fy_date_skid desc) as max_prmtn_lngth' || CHR(10) ||
             '               FROM OPT_PROD_HIER_FDIM h, OPT_BASLN_CATEG_PARM_PLC cat' || CHR(10) ||
             '               WHERE h.prod_lvl_desc = ''Brand''' || CHR(10) ||
             '               AND h.prod_4_bus_unit_skid = cat.bus_unit_skid (+)' || CHR(10) ||
             '               AND h.prod_4_name = cat.prod_name (+)' || CHR(10) ||
             '               group by h.prod_skid, h.bus_unit_skid' || CHR(10) ||
             '              ) cat' || CHR(10) ||
             '            WHERE cat.bus_unit_skid = bu.bus_unit_skid' || CHR(10) ||
             '           ) cat,' || CHR(10) ||
             '           (SELECT /*+ NO_MERGE ORDERED USE_HASH(p pp) */' || CHR(10) ||
             '              distinct pp.prmtn_skid, p.prod_5_skid as prod_skid' || CHR(10) ||
             '            FROM OPT_PROD_HIER_FDIM p, OPT_PRMTN_PROD_FCT pp' || CHR(10) ||
             '            WHERE pp.prod_skid = p.prod_skid' || CHR(10) ||
             '            AND pp.fy_date_skid = p.fy_date_skid' || CHR(10) ||
             '           ) prod' || CHR(10) ||
             '           WHERE prod.prod_skid = cat.prod_skid' || CHR(10) ||
             '        ) prod' || CHR(10) ||
             '      WHERE prom.prmtn_skid = prod.prmtn_skid' || CHR(10) ||
             '        --AND prom.end_date - prom.start_date < prod.max_prmtn_lngth' || CHR(10) ||
             '     ) prom,' || CHR(10) ||
             '     -- account and product filtering: only take shipped records' || CHR(10) ||
             '     (SELECT /*+ NO_MERGE */ distinct acct_skid, prod_skid FROM ' || I_AGG_FCT_TBL_NAME || ') s,' || CHR(10) ||
             '     -- calendar for weeks derivation' || CHR(10) ||
             '     (SELECT distinct wk_skid, wk_start_date, wk_end_date' || CHR(10) ||
             '      FROM CAL_MASTR_DIM' || CHR(10) ||
             '      WHERE day_date BETWEEN add_months(trunc(sysdate, ''mm''), -' || TO_CHAR(P_MTH_CNT) || ') AND last_day(sysdate)) c' || CHR(10) ||
             ' WHERE h.root_acct_skid = prom.acct_skid' || CHR(10) ||
             ' AND prom.start_date <= c.wk_end_date' || CHR(10) ||
             ' AND prom.cann_end_date >= c.wk_start_date' || CHR(10) ||
             ' AND s.acct_skid = h.acct_skid' || CHR(10) ||
             ' AND s.prod_skid = prom.prod_skid' || CHR(10) ||
             ' --AND prom.end_date > c.wk_start_date';
    -- Updated by David at 20100526 for fixing prmtn_skid missed -- End

    PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                 REPLACE(V_SQL, CHR(10), CHR(10) || '> '));

    -- prepare temporary table with promoted products (data selection with filtering and account location)
    BEGIN
      DROP_TABLE(I_BASLN_TEMP_TBL_NAME);
      EXECUTE IMMEDIATE V_SQL;
      PRO_PUT_LINE('Table created successfuly (' || SQL%ROWCOUNT ||
                   ' rows processed).');
    EXCEPTION
      WHEN OTHERS THEN
        PRO_PUT_LINE('Error: Creation of temporary table ' ||
                     I_BASLN_TEMP_TBL_NAME || ' failed with error - ' ||
                     SQLERRM);
        RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                'Error: Creation of temporary table ' ||
                                I_BASLN_TEMP_TBL_NAME ||
                                ' failed with error - ' || SQLERRM);
    END;

    -- statistics
    PRO_PUT_LINE('Starting gathering statistics process for ' ||
                 I_BASLN_TEMP_TBL_NAME || ' table (estimate percent = ' ||
                 C_STATS_TEMP_EST_PERCENT || ')...');
    DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV',
                                                                  'CURRENT_SCHEMA'),
                                  TABNAME          => I_BASLN_TEMP_TBL_NAME,
                                  ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                  METHOD_OPT       => 'FOR COLUMNS SIZE 1',
                                  CASCADE          => FALSE);
    PRO_PUT_LINE('Statistics gathered.');

    -- Added by David at 20100607 for performance tuning -- Start
    -- It is to fetch the promoted weeks for promotion accounts
    		-- Modified by Eric at 2010-9-20 for ORA-00600 Error --Strat
        -- removed the parallel parameter when create partition table to avoid ORA-600 error ' PARALLEL ' || P_DOP || CHR(10) ||
    		-- Modified by Eric at 2010-9-20 for ORA-00600 Error --End
    V_SQL := ' CREATE TABLE OPT_PRMTN_ACCT_SDIM_TEMP ' || CHR(10) ||
             '  ' || C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME ||
             ' nologging compress AS' || CHR(10) ||
             ' SELECT DISTINCT ' || CHR(10) ||
             '  bus_unit_skid,' || CHR(10) ||
             '  prmtn_skid,' || CHR(10) ||
             '  root_acct_skid,' || CHR(10) ||
             '  prod_skid,' || CHR(10) ||
             '  wk_skid' || CHR(10) ||
             ' FROM '||I_BASLN_TEMP_TBL_NAME;

    BEGIN
      DROP_TABLE('OPT_PRMTN_ACCT_SDIM_TEMP');
      PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                 REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
      EXECUTE IMMEDIATE V_SQL;
      PRO_PUT_LINE('Table created successfuly (' || SQL%ROWCOUNT ||
                  ' rows processed).');
    EXCEPTION
      WHEN OTHERS THEN
        PRO_PUT_LINE('Error: Creation of temporary table ' ||
                     'OPT_PRMTN_ACCT_SDIM_TEMP' || ' failed with error - ' ||
                     SQLERRM);
        RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                'Error: Creation of temporary table ' ||
                                'OPT_PRMTN_ACCT_SDIM_TEMP' ||
                                ' failed with error - ' || SQLERRM);
    END;
    -- Gather statistics
    PRO_PUT_LINE('Starting gathering statistics process for ' ||
                 'OPT_PRMTN_ACCT_SDIM_TEMP' || ' table (estimate percent = ' ||
                 C_STATS_TEMP_EST_PERCENT || ')...');
    DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV',
                                                                  'CURRENT_SCHEMA'),
                                  TABNAME          => 'OPT_PRMTN_ACCT_SDIM_TEMP',
                                  ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                  METHOD_OPT       => 'FOR COLUMNS SIZE 1',
                                  CASCADE          => FALSE);
    PRO_PUT_LINE('Statistics gathered.');
    -- Added by David at 20100607 for performance tuning -- End

    -- truncating old data
    PRO_PUT_LINE('Truncating table ' || I_BASLN_FCT_TBL_NAME || '...');
    V_SQL := 'truncate table ' || I_BASLN_FCT_TBL_NAME || ' reuse storage';
    PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                 REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
    EXECUTE IMMEDIATE V_SQL;
    PRO_PUT_LINE('Table ' || I_BASLN_FCT_TBL_NAME ||
                 ' successfuly truncated.');

    -- calculating post basln process
    IF V_DATA_TYPE = 'SHIP' THEN
      -- find a tablespace
      BEGIN
        SELECT 'TABLESPACE ' || TABLESPACE_NAME
          INTO V_TBLSPACE_NAME
          FROM USER_TAB_PARTITIONS
         WHERE TABLE_NAME = C_SHIP_FCT_TBL_NAME
           AND ROWNUM = 1;
      EXCEPTION
        WHEN OTHERS THEN
          PRO_PUT_LINE('Warning: Unable to recognize ' ||
                       C_SHIP_FCT_TBL_NAME ||
                       ' tablespace, choosing default tablespace!');
      END;

      -- find smallest week skid
      SELECT MIN(WK_SKID)
        INTO V_MIN_WK_SKID
        FROM CAL_MASTR_DIM
       WHERE DAY_DATE BETWEEN
             ADD_MONTHS(TRUNC(SYSDATE, 'mm'), -P_MTH_CNT + 1) AND
             LAST_DAY(SYSDATE);

      -- find week-to-fiscal-year mapping
      FOR C IN (SELECT FISC_YR_SKID,
                       MIN(CAL_MASTR_SKID) AS MIN_DAY_SKID,
                       MAX(CAL_MASTR_SKID) AS MAX_DAY_SKID
                  FROM CAL_MASTR_DIM
                 WHERE DAY_DATE BETWEEN
                       ADD_MONTHS(TRUNC(SYSDATE, 'mm'), -P_MTH_CNT) AND
                       LAST_DAY(SYSDATE)
                 GROUP BY FISC_YR_SKID) LOOP
      V_FY_MAP := V_FY_MAP ||' when b.wk_skid between ' ||
                    TO_CHAR(C.MIN_DAY_SKID) || ' and ' ||
                    TO_CHAR(C.MAX_DAY_SKID) || ' then ' ||
                    TO_CHAR(C.FISC_YR_SKID) || CHR(10);
      END LOOP;

      -- B004, optima90, Bean, begin
      -- step01, create a temporary table for data accumulation.
      -- Modified by Myra 20090929 in R9. implement compression
      -- Updated by David in R10 for performance tuning in R10 on 2010-5-19
    		-- Modified by Eric at 2010-9-20 for ORA-00600 Error --Strat
        -- removed the parallel parameter when create partition table to avoid ORA-600 error ' PARALLEL ' || P_DOP || CHR(10) ||
    		-- Modified by Eric at 2010-9-20 for ORA-00600 Error --End
       V_SQL := 'CREATE TABLE '|| C_SHIP_BASLN_CAL_TBL_NAME  ||CHR(10)||
             -- Added by David in Optima90 on 2009-09-08
             C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME ||
              '  nologging compress   AS ' || CHR(10) ||
              '   SELECT' || CHR(10) ||
              '        ship.bus_unit_skid,' || CHR(10) ||
              '        ship.prod_skid,' || CHR(10) ||
              '        ship.acct_skid,' || CHR(10) ||
              '        promo.root_acct_skid,' || CHR(10) ||
              '        ship.wk_skid,' || CHR(10) ||
              '        --ship.min_wk_skid,'|| CHR(10) ||
              '        --ship.max_wk_skid,'|| CHR(10) ||
              '        ship.post_basln_factr, -- Added by David in R9 on 2009-07-10' || CHR(10) ||
              '        ship.giv_amt,' || CHR(10) ||
              '        ship.basln_giv_amt,' || CHR(10) ||
              '        ship.su_amt,' || CHR(10) ||
              '        ship.basln_su_amt,' || CHR(10) ||
              '        ship.niv_amt,' || CHR(10) ||
              '        ship.basln_niv_amt,' || CHR(10) ||
              '        ship.buom_amt,' || CHR(10) ||
              '        ship.basln_buom_amt' || CHR(10) ||'    FROM' || CHR(10) ||
              '    -- copy backward' || CHR(10) ||
              '    (select' || CHR(10) ||'       cf.prod_skid,' || CHR(10) ||
              '       cf.acct_skid,' || CHR(10) ||
              '       cf.wk_skid,' || CHR(10) ||
              '       --cf.min_wk_skid,'|| CHR(10) ||
              '       --cf.max_wk_skid,'|| CHR(10) ||
              '       cf.post_basln_factr, -- Added by David in R9 on 2009-07-10' || CHR(10) ||
              '       cf.giv_amt,' || CHR(10) ||
              '       case' || CHR(10) ||
              '         when cf.first_wk_skid is not null then' || CHR(10) ||
              '           last_value(case when cf.wk_skid >= cf.first_wk_skid then cf.basln_giv_amt else null end ignore nulls) over (partition by cf.prod_skid, cf.acct_skid order by cf.wk_skid desc)' || CHR(10) ||
              '         when cf.wk_skid >= cf.first_wk_skid then' || CHR(10) ||
              '           cf.basln_giv_amt' || CHR(10) ||
              '         else' || CHR(10) ||
              '           max(cf.ship_giv_avg) over (partition by cf.prod_skid, cf.acct_skid)' || CHR(10) ||
              '         end' || CHR(10) ||
              '       as basln_giv_amt,' || CHR(10) ||
              '       --Added by David in R8' || CHR(10) ||
              '        cf.bus_unit_skid,' || CHR(10) ||
              '        cf.su_amt,' || CHR(10) ||'        case' || CHR(10) ||
              '          -- Copy the first calculated base SU at first_wk_skid to the weeks of shipment before first wk_skid' || CHR(10) ||
              '          when cf.first_wk_skid is not null then' || CHR(10) ||
              '            last_value(case when cf.wk_skid >= cf.first_wk_skid then cf.basln_su_amt else null end ignore nulls) over (partition by cf.prod_skid, cf.acct_skid order by cf.wk_skid desc)' || CHR(10) ||
              '            -- Keep the calculated base SU after first wk_skid' || CHR(10) ||
              '          when cf.wk_skid >= cf.first_wk_skid then' || CHR(10) ||
              '            cf.basln_su_amt' || CHR(10) ||
              '          else' || CHR(10) ||
              '            max(cf.ship_su_avg) over (partition by cf.prod_skid, cf.acct_skid)' || CHR(10) ||
              '        end' || CHR(10) ||
              '        as basln_su_amt,' || CHR(10) ||
              '        cf.niv_amt,' || CHR(10) ||'        case' || CHR(10) ||
              '         -- Copy the first calculated base NIV at first_wk_skid to the weeks of shipment before first wk_skid' || CHR(10) ||
              '          when cf.first_wk_skid is not null then' || CHR(10) ||
              '             last_value(case when cf.wk_skid >= cf.first_wk_skid then cf.basln_niv_amt else null end ignore nulls) over (partition by cf.prod_skid, cf.acct_skid order by cf.wk_skid desc)' || CHR(10) ||
              '          -- Keep the calculated base NIV after first wk_skid' || CHR(10) ||
              '          when cf.wk_skid >= cf.first_wk_skid then' || CHR(10) ||
              '             cf.basln_niv_amt' || CHR(10) ||
              '          else' || CHR(10) ||
              '             max(cf.ship_niv_avg) over (partition by cf.prod_skid, cf.acct_skid)' || CHR(10) ||
              '         end' || CHR(10) ||
              '         as basln_niv_amt,' || CHR(10) ||
              '        cf.buom_amt,' || CHR(10) ||'        case' || CHR(10) ||
              '          -- Copy the first calculated base BUoM at first_wk_skid to the weeks of shipment before first wk_skid' || CHR(10) ||
              '          when cf.first_wk_skid is not null then' || CHR(10) ||
              '             last_value(case when cf.wk_skid >= cf.first_wk_skid then cf.basln_buom_amt else null end ignore nulls) over (partition by cf.prod_skid, cf.acct_skid order by cf.wk_skid desc)' || CHR(10) ||
              '          -- Keep the calculated base BUoM after first wk_skid' || CHR(10) ||
              '          when cf.wk_skid >= cf.first_wk_skid then' || CHR(10) ||
              '             cf.basln_buom_amt' || CHR(10) ||
              '          else' || CHR(10) ||
              '             max(cf.ship_buom_avg) over (partition by cf.prod_skid, cf.acct_skid)' || CHR(10) ||
              '         end' || CHR(10) ||
              '         as basln_buom_amt' || CHR(10) ||
              '     from' || CHR(10) ||
              '       -- copy forward' || CHR(10) ||
              '       (select /*+ NO_MERGE ORDERED */' || CHR(10) ||
              '          prod_skid,' || CHR(10) ||'          acct_skid,' || CHR(10) ||
              '          wk_skid,' || CHR(10) ||
              '          --min_wk_skid,'|| CHR(10) ||
              '          --max_wk_skid,'|| CHR(10) ||
              '          giv_amt,' || CHR(10) ||'          case' || CHR(10) ||
              '            when cnt <= num_of_shpmt_wks then' || CHR(10) ||
              '              null' || CHR(10) ||
              '            else' || CHR(10) ||
              '              last_value(basln_giv_amt ignore nulls) over (partition by prod_skid, acct_skid order by wk_skid asc)' || CHR(10) ||
              '            end' || CHR(10) ||
              '          as basln_giv_amt,' || CHR(10) ||
              '          ship_giv_avg,' || CHR(10) ||
              '          cnt,' || CHR(10) ||
              '          post_basln_factr, -- Added by David in R9 on 2009-07-10' || CHR(10) ||
              '          -- Get the wk_skid for the first calculating base GIV' || CHR(10) ||
              '          min(case when cnt > num_of_shpmt_wks and cnt < 9999 then wk_skid else null end) over (partition by prod_skid, acct_skid) as first_wk_skid,' || CHR(10) ||
              '          --Added by David in R8' || CHR(10) ||
              '          bus_unit_skid,' || CHR(10) ||
              '          su_amt,' || CHR(10) ||
              '           case' || CHR(10) ||
              '             -- Set null base SU before the first wk_skid which should calculate base SU.' || CHR(10) ||
              '             when cnt <= num_of_shpmt_wks then' || CHR(10) ||
              '                 null' || CHR(10) ||
              '             else' || CHR(10) ||
              '             -- Copy the last calculated base SU before any certain promotion start wk_skid after first wk_skid' || CHR(10) ||
              '             last_value(basln_su_amt ignore nulls) over (partition by prod_skid, acct_skid order by wk_skid asc)' || CHR(10) ||
              '           end' || CHR(10) ||
              '           as basln_su_amt,' || CHR(10) ||
              '           ship_su_avg,' || CHR(10) ||
              '           niv_amt,' || CHR(10) ||
              '            case' || CHR(10) ||
              '             -- Set null base NIV before the first wk_skid which should calculate base NIV.' || CHR(10) ||
              '             when cnt <= num_of_shpmt_wks then' || CHR(10) ||
              '                null' || CHR(10) ||
              '             else' || CHR(10) ||
              '             -- Copy the last calculated base NIV before any certain promotion start wk_skid after first wk_skid' || CHR(10) ||
              '               last_value(basln_niv_amt ignore nulls) over (partition by prod_skid, acct_skid order by wk_skid asc)' || CHR(10) ||
              '           end' || CHR(10) ||
              '           as basln_niv_amt,' || CHR(10) ||
              '           ship_niv_avg,' || CHR(10) ||
              '           buom_amt,' || CHR(10) ||
              '            case' || CHR(10) ||
              '             -- Set null base BUoM before the first wk_skid which should calculate base NIV.' || CHR(10) ||
              '             when cnt <= num_of_shpmt_wks then' || CHR(10) ||
              '                null' || CHR(10) ||
              '             else' || CHR(10) ||
              '             -- Copy the last calculated base BUoM before any certain promotion start wk_skid after first wk_skid' || CHR(10) ||
              '               last_value(basln_buom_amt ignore nulls) over (partition by prod_skid, acct_skid order by wk_skid asc)' || CHR(10) ||
              '           end' || CHR(10) ||
              '           as basln_buom_amt,' || CHR(10) ||
              '           ship_buom_avg' || CHR(10) ||
              '       from' || CHR(10) ||
              '         (select /*+ NO_MERGE */' || CHR(10) ||
              '            prod_skid,' || CHR(10) ||
              '            acct_skid,' || CHR(10) ||
              '            wk_skid,' || CHR(10) ||
              '            --min_wk_skid,'|| CHR(10) ||
              '            --max_wk_skid,'|| CHR(10) ||
              '            giv_amt,' || CHR(10) ||
              '            basln_giv_amt,' || CHR(10) ||
              '            ship_giv_avg,' || CHR(10) ||
              '            num_of_shpmt_wks,' || CHR(10) ||
              '            count(1) over (partition by prod_skid, acct_skid order by wk_skid asc) as cnt,' || CHR(10) ||
              '            post_basln_factr, -- Added by David in R9 on 2009-07-10' || CHR(10) ||
              '            --Added by David in R8' || CHR(10) ||
              '            bus_unit_skid,' || CHR(10) ||
              '            su_amt, ' || CHR(10) ||
              '            basln_su_amt, ' || CHR(10) ||
              '            ship_su_avg, ' || CHR(10) ||
              '            niv_amt,' || CHR(10) ||
              '            basln_niv_amt, ' || CHR(10) ||
              '            ship_niv_avg,' || CHR(10) ||
              '            buom_amt,' || CHR(10) ||
              '            basln_buom_amt, ' || CHR(10) ||
              '            ship_buom_avg' || CHR(10) ||
              '          from' || CHR(10) ||
              '            -- post-event baseline calculation' || CHR(10) ||
              '            (select /*+ NO_MERGE ORDERED */' || CHR(10) ||
              '               s.prod_skid,' || CHR(10) ||
              '               s.acct_skid,' || CHR(10) ||
              '               s.wk_skid,' || CHR(10) ||
              '               --s.min_wk_skid,'|| CHR(10) ||
              '               --s.max_wk_skid,'|| CHR(10) ||
              '               s.giv_amt,' || CHR(10) ||
              '               avg(s.giv_amt) over (partition by s.prod_skid, s.acct_skid order by s.wk_skid asc rows between cat.num_of_shpmt_wks preceding and current row) as basln_giv_amt,' || CHR(10) ||
              '               avg(s.giv_amt) over (partition by s.prod_skid, s.acct_skid) as ship_giv_avg,' || CHR(10) ||
              '               cat.num_of_shpmt_wks,' || CHR(10) ||
              '               cat.post_basln_factr, -- Added by David in R9 on 2009-07-10' || CHR(10) ||
              '               s.bus_unit_skid,' || CHR(10) ||
              '               s.su_amt,' || CHR(10) ||
              '               avg(s.su_amt) over (partition by s.prod_skid, s.acct_skid order by s.wk_skid asc rows between cat.num_of_shpmt_wks preceding and current row) as basln_su_amt,' || CHR(10) ||
              '               avg(s.su_amt) over (partition by s.prod_skid, s.acct_skid) as ship_su_avg,' || CHR(10) ||
              '               s.niv_amt,' || CHR(10) ||
              '               avg(s.niv_amt) over (partition by s.prod_skid, s.acct_skid order by s.wk_skid asc rows between cat.num_of_shpmt_wks preceding and current row) as basln_niv_amt,' || CHR(10) ||
              '               avg(s.niv_amt) over (partition by s.prod_skid, s.acct_skid) as ship_niv_avg,' || CHR(10) ||
              '               s.buom_amt,' || CHR(10) ||
              '               avg(s.buom_amt) over (partition by s.prod_skid, s.acct_skid order by s.wk_skid asc rows between cat.num_of_shpmt_wks preceding and current row) as basln_buom_amt,' || CHR(10) ||
              '               avg(s.buom_amt) over (partition by s.prod_skid, s.acct_skid) as ship_buom_avg' || CHR(10) ||
              '             from' || CHR(10) ||
              '                -- Derived the post baseline parameters from BU and Category metadata table' || CHR(10) ||
              '                 (select /*+ NO_MERGE ORDERED USE_HASH(bu cat) */' || CHR(10) ||
              '                    cat.prod_skid,' || CHR(10) ||
              '                    nvl(cat.post_basln_factr, nvl(bu.post_basln_factr,1)) as post_basln_factr, -- Added by David in R9 on 2009-07-10' || CHR(10) ||
              '                    nvl(cat.num_of_shpmt_wks, bu.num_of_shpmt_wks) - 1 as num_of_shpmt_wks' || CHR(10) ||
              '                  from' || CHR(10) ||
              '                    -- business unit max promotion length defaults' || CHR(10) ||
              '                    (select' || CHR(10) ||
              '                       bu.bus_unit_skid,' || CHR(10) ||
              '                       nvl(d.num_of_shpmt_wks, g.num_of_shpmt_wks) as num_of_shpmt_wks,' || CHR(10) ||
              '                       nvl(d.post_basln_factr, g.post_basln_factr) as post_basln_factr -- Added by David in R9 on 2009-07-10' || CHR(10) ||
              '                     from OPT_BUS_UNIT_DIM bu, OPT_BUS_UNIT_PLC g, OPT_BUS_UNIT_PLC d' || CHR(10) ||
              '                     where g.bus_unit_skid = 0' || CHR(10) ||
              '                     and d.deflt_lvl_name is not null' || CHR(10) ||
              '                     and d.bus_unit_skid (+) <> 0' || CHR(10) ||
              '                     and bu.bus_unit_skid = d.bus_unit_skid (+)' || CHR(10) ||
              '                    ) bu,' || CHR(10) ||
              '                    (select /*+ NO_MERGE */' || CHR(10) ||
              '                       h.prod_skid, h.bus_unit_skid, max(cat.post_basln_factr) post_basln_factr,max(cat.num_of_shpmt_wks_qty) keep (dense_rank first order by h.fy_date_skid desc) as num_of_shpmt_wks' || CHR(10) ||
              '                     from OPT_PROD_HIER_FDIM h, OPT_BASLN_CATEG_PARM_PLC cat' || CHR(10) ||
              '                     where h.prod_lvl_desc =''Brand''' || CHR(10) ||
              '                     and h.prod_4_bus_unit_skid = cat.bus_unit_skid (+)' || CHR(10) ||
              '                     and h.prod_4_name = cat.prod_name (+)' || CHR(10) ||
              '                     group by h.prod_skid, h.bus_unit_skid' || CHR(10) ||
              '                    ) cat' || CHR(10) ||
              '                  where cat.bus_unit_skid = bu.bus_unit_skid' || CHR(10) ||
              '                 ) cat,' || CHR(10) ||
              '                 -- zero-shipments derived with promotion weeks' || CHR(10) ||
              '                 (select /*+ NO_MERGE USE_MERGE(s1 s2) */' || CHR(10) ||
              '                    s1.bus_unit_skid,' || CHR(10) ||
              '                    s1.prod_skid,' || CHR(10) ||
              '                    s1.acct_skid,' || CHR(10) ||
              '                    s1.wk_skid,' || CHR(10) ||
              '                    --s1.min_wk_skid,'|| CHR(10) ||
              '                    --s1.max_wk_skid,'|| CHR(10) ||
              '                    nvl(s2.giv_amt, 0) as giv_amt,' || CHR(10) ||
              '                    --Added by David in R8' || CHR(10) ||
              '                    nvl(s2.su_amt, 0) as su_amt,' || CHR(10) ||
              '                    nvl(s2.niv_amt, 0) as niv_amt,' || CHR(10) ||
              '                    nvl(s2.buom_amt, 0) as buom_amt' || CHR(10) ||
              '                  from' || CHR(10) ||
              '                    -- time gaps filling' || CHR(10) ||
              '                    (select /*+ USE_MERGE(s c) */' || CHR(10) ||
              '                       s.bus_unit_skid,' || CHR(10) ||
              '                       s.prod_skid,' || CHR(10) ||
              '                       s.acct_skid,' || CHR(10) ||
              '                       s.min_wk_skid,'|| CHR(10) ||
              '                       s.max_wk_skid,'|| CHR(10) ||
              '                       c.wk_skid' || CHR(10) ||
              '                       -- all the prod/acct pairs' || CHR(10) ||
              '                        from' || CHR(10) ||
              '                           (select' || CHR(10) ||
              '                              distinct wk_skid' || CHR(10) ||
              '                              from CAL_MASTR_DIM' || CHR(10) ||
              '                              where day_date between add_months(trunc(sysdate,''mm''), -' || P_MTH_CNT ||') and last_day(sysdate)' || CHR(10) ||
              '                           ) c,' || CHR(10) ||
              '                          -- statistical data' || CHR(10) ||
              '                          (select' || CHR(10) ||
              '                             bus_unit_skid, prod_skid, acct_skid,' || CHR(10) ||
              '                             --sum(giv_amt) as ship_giv_sum,' || CHR(10) ||
              '                             min(decode(greatest(giv_amt,0), 0, 9999999, wk_skid)) as min_wk_skid,' || CHR(10) ||
              '                             max(decode(greatest(giv_amt,0), 0, 0, wk_skid)) as max_wk_skid' || CHR(10) ||
              '                             --Added by David in R8' || CHR(10) ||
              '                             --sum(niv_amt) as ship_niv_sum,' || CHR(10) ||
              '                             --sum(su_amt)  as ship_su_sum,' || CHR(10) ||
              '                             --sum(buom_amt)  as ship_buom_sum' || CHR(10) ||
              '                           from ' || I_AGG_FCT_TBL_NAME || CHR(10) ||
              '                           where acct_skid in (select acct_skid from OPT_PRMTN_ACCT_ASSOC_SDIM_TEMP)' || CHR(10) ||
              '                           group by bus_unit_skid, prod_skid, acct_skid' || CHR(10) ||
              '                          ) s' || CHR(10) ||
              '                     where c.wk_skid between s.min_wk_skid and s.max_wk_skid ) s1,' || CHR(10) ||
              '                    (select prod_skid, acct_skid, wk_skid, giv_amt,su_amt,niv_amt,buom_amt' || CHR(10) ||
              '                     from ' || I_AGG_FCT_TBL_NAME ||') s2' || CHR(10) ||
              '                  where s1.prod_skid = s2.prod_skid (+)' || CHR(10) ||
              '                  and s1.acct_skid = s2.acct_skid (+)' || CHR(10) ||
              '                  and s1.wk_skid = s2.wk_skid (+)' || CHR(10) ||
              '                  -- substract promo weeks' || CHR(10) ||
              -- Updated by David at 20100526 for fixing prmtn_skid missed -- Start
              '                  and not exists( select ''x'' from ' || I_BASLN_TEMP_TBL_NAME || ' promo' || CHR(10) ||
              '                                   where valid_prmtn_ind = ''Y'' '|| CHR(10) ||
              '                                     and cann_ind = ''N'' '|| CHR(10) ||
              '                                     and s1.prod_skid = promo.prod_skid '|| CHR(10) ||
              '                                     and s1.acct_skid = promo.acct_skid ' || CHR(10) ||
              '                                     and s1.wk_skid = promo.wk_skid ) ' || CHR(10) ||
              -- Updated by David at 20100526 for fixing prmtn_skid missed -- End
              '               ) s' || CHR(10) ||
              '             where s.prod_skid = cat.prod_skid ))' || CHR(10) ||
              '        union all' || CHR(10) ||
              '        -- empty promo weeks' || CHR(10) ||
              '        (select /*+ ORDERED USE_HASH(promo s) */' || CHR(10) ||
              '          promo.prod_skid,' || CHR(10) ||
              '          promo.acct_skid,' || CHR(10) ||
              '          promo.wk_skid,' || CHR(10) ||
                  ---B004, Optima90, Bean, begin
              '          --null as min_wk_skid,'|| CHR(10) ||
              '          --null as max_wk_skid,'|| CHR(10) ||
                  ---B004, Optima90, Bean, end
              '          nvl(s.giv_amt, 0) as giv_amt,' || CHR(10) ||
              '          null as basln_giv_amt,' || CHR(10) ||
              '          null as ship_giv_avg,' || CHR(10) ||
              '          9998 as num_of_shpmt_wks,' || CHR(10) ||
              '          9999 as cnt,' || CHR(10) ||
              '          promo.post_basln_factr as post_basln_factr, -- Added by David in R9 on 2009-07-10' || CHR(10) ||
              '          --Added by David for F009 in R8' || CHR(10) ||
              '          promo.bus_unit_skid as bus_unit_skid,' || CHR(10) ||
              '          nvl(s.su_amt, 0) as su_amt,' || CHR(10) ||
              '          null as basln_su_amt,' || CHR(10) ||
              '          null as ship_su_avg,' || CHR(10) ||
              '          nvl(s.niv_amt, 0) as niv_amt, ' || CHR(10) ||
              '          null as basln_niv_amt,' || CHR(10) ||
              '          null as ship_niv_avg,' || CHR(10) ||
              '          nvl(s.buom_amt, 0) as buom_amt, ' || CHR(10) ||
              '          null as basln_buom_amt,' || CHR(10) ||
              '          null as ship_buom_avg' || CHR(10) ||
              '        from ' || CHR(10) ||
              -- Updated by David at 20100526 for fixing prmtn_skid missed -- Start
              '          (select distinct' || CHR(10) ||
              '             prod_skid,' || CHR(10) ||
              '             acct_skid,' || CHR(10) ||
              '             wk_skid,' || CHR(10) ||
              '             post_basln_factr,' || CHR(10) ||
              '             bus_unit_skid' || CHR(10) ||
              '           from ' || I_BASLN_TEMP_TBL_NAME || CHR(10) ||
              '           where valid_prmtn_ind = ''Y'' ' || CHR(10) ||
              '             and   cann_ind = ''N'' ' || CHR(10) ||
              '          ) promo,' || CHR(10) ||
              -- Updated by David at 20100526 for fixing prmtn_skid missed -- End
              '          (select' || CHR(10) ||
              '             prod_skid, acct_skid, wk_skid, giv_amt,niv_amt,su_amt,bus_unit_skid,buom_amt' || CHR(10) ||
              '           from ' || I_AGG_FCT_TBL_NAME || CHR(10) ||
              '          ) s' || CHR(10) ||
              '        where promo.prod_skid = s.prod_skid (+)' || CHR(10) ||
              '        and promo.acct_skid = s.acct_skid (+)' || CHR(10) ||
              '        and promo.wk_skid = s.wk_skid (+)' || CHR(10) ||
              '       )' || CHR(10) ||
              '    ) cf' || CHR(10) ||
              '   /* this line ignores leading promotions (when there was no shipments) */' || CHR(10) ||
              '   where not (cf.basln_giv_amt is null and cf.cnt = 9999)' || CHR(10) ||
              ' ) ship,' || CHR(10) ||
              ' OPT_PRMTN_ACCT_ASSOC_SDIM_TEMP promo' || CHR(10) ||
              ' WHERE ship.acct_skid = promo.acct_skid';

     PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                 REPLACE(V_SQL, CHR(10), CHR(10) || '> '));

    -- prepare temporary table with promoted products (data selection with filtering and account location)
    BEGIN
      DROP_TABLE(C_SHIP_BASLN_CAL_TBL_NAME);
      EXECUTE IMMEDIATE V_SQL;
      PRO_PUT_LINE('Table created successfuly (' || SQL%ROWCOUNT ||
                   ' rows processed).');
    EXCEPTION
      WHEN OTHERS THEN
        PRO_PUT_LINE('Error: Creation of temporary table ' ||
                     C_SHIP_BASLN_CAL_TBL_NAME || ' failed with error - ' ||
                     SQLERRM);
        RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                'Error: Creation of temporary table ' ||
                                C_SHIP_BASLN_CAL_TBL_NAME ||
                                ' failed with error - ' || SQLERRM);
    END;
    -- statistics
    PRO_PUT_LINE('Starting gathering statistics process for ' ||
                 C_SHIP_BASLN_CAL_TBL_NAME || ' table (estimate percent = ' ||
                 C_STATS_TEMP_EST_PERCENT  || ')...');
    DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV',
                                                                  'CURRENT_SCHEMA'),
                                  TABNAME          => C_SHIP_BASLN_CAL_TBL_NAME,
                                  ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                  METHOD_OPT       => 'FOR COLUMNS SIZE 1',
                                  CASCADE          => FALSE);
    PRO_PUT_LINE('Statistics gathered.');

    -- Purge data from table for new data insertion
    V_SQL := 'TRUNCATE TABLE '||C_OPT_BASLN_DUMMY_TBL_NAME;
    EXECUTE IMMEDIATE V_SQL;

    -- Added by David at 20100607 for performance tuning -- Start
    -- It is to aggregate baseline measures to promotion acct level
    		-- Modified by Eric at 2010-9-20 for ORA-00600 Error --Strat
        -- removed the parallel parameter when create partition table to avoid ORA-600 error ' PARALLEL ' || P_DOP || CHR(10) ||
    		-- Modified by Eric at 2010-9-20 for ORA-00600 Error --End
    V_SQL :=  ' CREATE TABLE OPT_PRMTN_ACCT_BASLN_TEMP' || CHR(10) ||
              '  ' || C_FINAL_TBL_STORAGE || ' ' || V_TBLSPACE_NAME ||
              '  as select' || CHR(10) ||
              '    b.bus_unit_skid,' || CHR(10) ||
              '    b.prod_skid,' || CHR(10) ||
              '    b.root_acct_skid,' || CHR(10) ||
              '    b.wk_skid,' || CHR(10) ||
              '    b.post_basln_factr, '|| CHR(10) ||
              '    sum(b.basln_giv_amt) as basln_num,' || CHR(10) ||
              '    sum(b.giv_amt) as giv_amt,' || CHR(10) ||
              '    sum(b.basln_niv_amt) as basln_niv_num,' || CHR(10) ||
              '    sum(b.niv_amt) as niv_amt,' || CHR(10) ||
              '    sum(b.basln_su_amt) as basln_su_num,' || CHR(10) ||
              '    sum(b.su_amt) as su_amt,' || CHR(10) ||
              '    sum(b.basln_buom_amt) as basln_buom_num,' || CHR(10) ||
              '    sum(b.buom_amt) as buom_amt' || CHR(10) ||
              '  from ' || CHR(10) ||
              '   ' || C_SHIP_BASLN_CAL_TBL_NAME ||' b ' || CHR(10) ||
              'where b.wk_skid >=' || V_MIN_WK_SKID || CHR(10) ||
              'group by b.bus_unit_skid, b.prod_skid, b.root_acct_skid,b.wk_skid,b.post_basln_factr';

    BEGIN
      DROP_TABLE('OPT_PRMTN_ACCT_BASLN_TEMP');
      PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                 REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
      EXECUTE IMMEDIATE V_SQL;
      PRO_PUT_LINE('Table created successfuly (' || SQL%ROWCOUNT ||
                   ' rows processed).');
    EXCEPTION
      WHEN OTHERS THEN
        PRO_PUT_LINE('Error: Creation of temporary table ' ||
                     'OPT_PRMTN_ACCT_BASLN_TEMP' || ' failed with error - ' ||
                     SQLERRM);
        RAISE_APPLICATION_ERROR(C_TBL_CREAT_ERROR,
                                'Error: Creation of temporary table ' ||
                                'OPT_PRMTN_ACCT_BASLN_TEMP' ||
                                ' failed with error - ' || SQLERRM);
    END;

    -- Gather statistics
    PRO_PUT_LINE('Starting gathering statistics process for ' ||
                 'OPT_PRMTN_ACCT_BASLN_TEMP' || ' table (estimate percent = ' ||
                 C_STATS_TEMP_EST_PERCENT || ')...');
    DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV',
                                                                  'CURRENT_SCHEMA'),
                                  TABNAME          => 'OPT_PRMTN_ACCT_BASLN_TEMP',
                                  ESTIMATE_PERCENT => C_STATS_TEMP_EST_PERCENT,
                                  METHOD_OPT       => 'FOR COLUMNS SIZE 1',
                                  CASCADE          => FALSE);
    PRO_PUT_LINE('Statistics gathered.');
    -- Added by David at 20100607 for performance tuning -- End

      -- post-event baseline creation
      -- Updated by David for B011 R9 on 2009-07-10
      -- Add a new parameter for recalculation of post basln
      V_SQL := 'INSERT /*+ APPEND PARALLEL(' || P_DOP ||') */ INTO' || CHR(10) ||
               I_BASLN_FCT_TBL_NAME ||' (BUS_UNIT_SKID,' || CHR(10) ||
              '                 PRMTN_SKID, PROD_SKID, ACCT_SKID, WK_SKID, FY_DATE_SKID, BASLN_GIV_AMT,' || CHR(10) ||
              '                 ACTL_GIV_AMT,BASLN_NIV_AMT,ACTL_NIV_AMT,BASLN_SU_AMT,ACTL_SU_AMT,BASLN_BUOM_AMT,ACTL_BUOM_AMT)' || CHR(10) ||
              'select /*+ ORDERED USE_HASH(p b) */' || CHR(10) ||
              '  bus_unit_skid,' || CHR(10) ||
              '  prmtn_skid,' || CHR(10) ||
              '  prod_skid,' || CHR(10) ||
              '  acct_skid,' || CHR(10) ||
              '  wk_skid,' || CHR(10) ||
              '  fy_date_skid,' || CHR(10) ||
              '  (basln_num*post_basln_factr) as basln_giv_amt,' || CHR(10) ||
              '  giv_amt as actl_giv_amt,' || CHR(10) ||
              '  (basln_niv_num*post_basln_factr) as basln_niv_amt,' || CHR(10) ||
              '  niv_amt as actl_niv_amt,' || CHR(10) ||
              '  (basln_su_num*post_basln_factr) as basln_su_amt,' || CHR(10) ||
              '  su_amt as actl_su_amt,' || CHR(10) ||
              '  (basln_buom_num*post_basln_factr) as basln_buom_amt,' || CHR(10) ||
              '  buom_amt as actl_buom_amt' || CHR(10) ||
              'from (' || CHR(10) ||
      -- Updated by David at 20100607 for performance tuning -- Start
              '  -- account aggregation' || CHR(10) ||
              '  select /*+ ORDERED USE_HASH(p b) */' || CHR(10) ||
              '    b.bus_unit_skid,' || CHR(10) ||
              '    p.prmtn_skid,' || CHR(10) ||
              '    b.prod_skid,' || CHR(10) ||
              '    b.root_acct_skid as acct_skid,' || CHR(10) ||
              '    b.wk_skid,' || CHR(10) ||
              '    b.post_basln_factr, -- Added by David in R9 on 2009-07-10' || CHR(10) ||
              '    case' || V_FY_MAP ||
              '    end as fy_date_skid,' || CHR(10) ||
              '    b.basln_num,' || CHR(10) ||
              '    b.giv_amt,' || CHR(10) ||
              '    b.basln_niv_num,' || CHR(10) ||
              '    b.niv_amt,' || CHR(10) ||
              '    b.basln_su_num,' || CHR(10) ||
              '    b.su_amt,' || CHR(10) ||
              '    b.basln_buom_num,' || CHR(10) ||
              '    b.buom_amt' || CHR(10) ||
              '  from OPT_PRMTN_ACCT_SDIM_TEMP p,' || CHR(10) ||
              '       OPT_PRMTN_ACCT_BASLN_TEMP b ' || CHR(10) ||
              'where b.prod_skid = p.prod_skid (+)' || CHR(10) ||
              'and b.root_acct_skid = p.root_acct_skid (+)' || CHR(10) ||
              'and b.bus_unit_skid = p.bus_unit_skid (+)' || CHR(10) ||
              'and b.wk_skid = p.wk_skid (+) )';
              --'and b.wk_skid >=' || V_MIN_WK_SKID || CHR(10) ||
              --'group by b.bus_unit_skid,p.prmtn_skid, b.prod_skid, b.root_acct_skid,b.wk_skid,b.post_basln_factr )';
      -- Updated by David at 20100607 for performance tuning -- End

      PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                   REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
      BEGIN
        EXECUTE IMMEDIATE V_SQL;
        PRO_PUT_LINE('Data created successfuly (' || SQL%ROWCOUNT ||
                     ' rows processed).');
        COMMIT;
      EXCEPTION
        WHEN OTHERS THEN
          PRO_PUT_LINE('Error: Inserting data into ' ||
                       I_BASLN_FCT_TBL_NAME ||
                       ' table failed with error - ' || SQLERRM);
          RAISE_APPLICATION_ERROR(C_TBL_INS_ERROR,
                                  'Error: Inserting data into ' ||
                                  I_BASLN_FCT_TBL_NAME ||
                                  ' table failed with error - ' || SQLERRM);
      END;

      -- drop temporary tables
      -- Commented by David for tracking data issue
      -- DROP_TABLE(I_BASLN_TEMP_TBL_NAME);

      -- gather statistics for all involved partitions
      PRO_PUT_LINE('Starting gathering statistics on ' ||
                   I_BASLN_FCT_TBL_NAME || ' table...');
      DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV',
                                                                    'CURRENT_SCHEMA'),
                                    TABNAME          => I_BASLN_FCT_TBL_NAME,
                                    GRANULARITY      => 'ALL',
                                    ESTIMATE_PERCENT => C_STATS_FINAL_EST_PERCENT,
                                    BLOCK_SAMPLE     => FALSE,
                                    DEGREE           => 1,
                                    METHOD_OPT       => 'FOR ALL COLUMNS SIZE AUTO');
      PRO_PUT_LINE('Statistics on table ' || I_BASLN_FCT_TBL_NAME ||
                   ' table gathered.');

      -- Process the data from EPOS
    ELSIF V_DATA_TYPE = 'EPOS' THEN

      -- find a tablespace
      BEGIN
        SELECT 'TABLESPACE ' || TABLESPACE_NAME
          INTO V_TBLSPACE_NAME
          FROM USER_TAB_PARTITIONS
         WHERE TABLE_NAME = C_SHIP_FCT_TBL_NAME
           AND ROWNUM = 1;
      EXCEPTION
        WHEN OTHERS THEN
          PRO_PUT_LINE('Warning: Unable to recognize ' ||
                       C_SHIP_FCT_TBL_NAME ||
                       ' tablespace, choosing default tablespace!');
      END;

      -- find smallest week skid
      SELECT MIN(WK_SKID)
        INTO V_MIN_WK_SKID
        FROM CAL_MASTR_DIM
       WHERE DAY_DATE BETWEEN
             ADD_MONTHS(TRUNC(SYSDATE, 'mm'), -P_MTH_CNT + 1) AND
             LAST_DAY(SYSDATE);

      -- find week-to-fiscal-year mapping
      FOR C IN (SELECT FISC_YR_SKID,
                       MIN(CAL_MASTR_SKID) AS MIN_DAY_SKID,
                       MAX(CAL_MASTR_SKID) AS MAX_DAY_SKID
                  FROM CAL_MASTR_DIM
                 WHERE DAY_DATE BETWEEN
                       ADD_MONTHS(TRUNC(SYSDATE, 'mm'), -P_MTH_CNT) AND
                       LAST_DAY(SYSDATE)
                 GROUP BY FISC_YR_SKID) LOOP
        V_FY_MAP := V_FY_MAP || CHR(10) || '      when p.wk_skid between ' ||
                    TO_CHAR(C.MIN_DAY_SKID) || ' and ' ||
                    TO_CHAR(C.MAX_DAY_SKID) || ' then ' ||
                    TO_CHAR(C.FISC_YR_SKID);
      END LOOP;

      -- post-event baseline creation
      V_SQL :=  'INSERT /*+ APPEND PARALLEL(' || P_DOP || ') */ INTO ' || I_BASLN_FCT_TBL_NAME ||
               ' (BUS_UNIT_SKID, PRMTN_SKID, PROD_SKID, ACCT_SKID, WK_SKID, FY_DATE_SKID, BASLN_GIV_AMT, ACTL_GIV_AMT)' ||
               CHR(10) || '  -- account aggregation' ||
               CHR(10) || '  select /*+ ORDERED USE_HASH(p b) */' ||
               CHR(10) || '    p.bus_unit_skid,' ||
               CHR(10) || '    p.prmtn_skid,' ||
               CHR(10) || '    b.prod_skid,' ||
               CHR(10) || '    p.root_acct_skid as acct_skid,' ||
               CHR(10) || '    p.wk_skid,' ||
               CHR(10) || '    case' || CHR(10) || V_FY_MAP ||
               CHR(10) || '    end as fy_date_skid,' ||
               CHR(10) || '    sum(b.basln_giv_amt*b.post_basln_factr) as basln_num,' ||
               CHR(10) || '    sum(b.giv_amt) as giv_amt' ||
               CHR(10) || '  from' ||
               CHR(10) || '    ' || I_BASLN_TEMP_TBL_NAME || ' p,' ||
               CHR(10) || '    -- copy backward'||
               CHR(10) || '    (select'||
               CHR(10) || '       cf.prod_skid,'||
               CHR(10) || '       cf.acct_skid,'||
               CHR(10) || '       cf.wk_skid,'||
               CHR(10) || '       cf.giv_amt,'||
               CHR(10) || '       case'||
               CHR(10) || '         when cf.first_wk_skid is not null then' ||
               CHR(10) || '           last_value(case when cf.wk_skid >= cf.first_wk_skid then cf.basln_giv_amt else null end ignore nulls) over (partition by cf.prod_skid, cf.acct_skid order by cf.wk_skid desc)' ||
               CHR(10) || '         when cf.wk_skid >= cf.first_wk_skid then' ||
               CHR(10) || '           cf.basln_giv_amt' ||
               CHR(10) || '         else'||
               CHR(10) || '           max(cf.ship_giv_avg) over (partition by cf.prod_skid, cf.acct_skid)' ||CHR(10) ||
               CHR(10) || '         end'||
               CHR(10) || '       as basln_giv_amt,'||
               CHR(10) || '       cf.post_basln_factr -- Added by David in R9 on 2009-07-10'||
               CHR(10) || '     from'||
               CHR(10) || '       -- copy forward'||
               CHR(10) || '       (select /*+ NO_MERGE ORDERED */' ||
               CHR(10) || '          prod_skid,' ||
               CHR(10) || '          acct_skid,' ||
               CHR(10) || '          wk_skid,' ||
               CHR(10) || '          giv_amt,' ||
               CHR(10) || '          case' ||
               CHR(10) || '            when cnt <= num_of_shpmt_wks then' ||
               CHR(10) || '              null' ||
               CHR(10) || '            else' ||
               CHR(10) || '              last_value(basln_giv_amt ignore nulls) over (partition by prod_skid, acct_skid order by wk_skid asc)' ||
               CHR(10) || '            end' ||
               CHR(10) || '          as basln_giv_amt,' ||
               CHR(10) || '          ship_giv_avg,' ||
               CHR(10) || '          cnt,' ||
               CHR(10) || '          post_basln_factr, -- Added by David in R9 on 2009-07-10'||
               CHR(10) || '          min(case when cnt > num_of_shpmt_wks and cnt < 9999 then wk_skid else null end) over (partition by prod_skid, acct_skid) as first_wk_skid' ||
               CHR(10) || '       from' ||
               CHR(10) || '         (select /*+ NO_MERGE */' ||
               CHR(10) || '            prod_skid,' ||
               CHR(10) || '            acct_skid,' ||
               CHR(10) || '            wk_skid,' ||
               CHR(10) || '            giv_amt,' ||
               CHR(10) || '            basln_giv_amt,' ||
               CHR(10) || '            ship_giv_avg,' ||
               CHR(10) || '            num_of_shpmt_wks,' ||
               CHR(10) || '            post_basln_factr, -- Added by David in R9 on 2009-07-10'||
               CHR(10) || '            count(1) over (partition by prod_skid, acct_skid order by wk_skid asc) as cnt' ||
               CHR(10) || '          from' ||
               CHR(10) || '             -- post-event baseline calculation' ||
               CHR(10) || '            (select /*+ NO_MERGE ORDERED */' ||
               CHR(10) || '               s.prod_skid,' ||
               CHR(10) || '               s.acct_skid,' ||
               CHR(10) || '               s.wk_skid,' ||
               CHR(10) || '               s.giv_amt,' ||
               CHR(10) || '               avg(s.giv_amt) over (partition by s.prod_skid, s.acct_skid order by s.wk_skid asc rows between cat.num_of_shpmt_wks preceding and current row) as basln_giv_amt,' ||
               CHR(10) || '               s.ship_giv_avg,' ||
               CHR(10) || '               cat.num_of_shpmt_wks,' ||
               CHR(10) || '               cat.post_basln_factr -- Added by David in R9 on 2009-07-10' ||
               CHR(10) || '             from' ||
               CHR(10) || '               -- category level max promotion length defaults ' ||
               CHR(10) || '               (SELECT /*+ NO_MERGE ORDERED USE_HASH(bu cat) */' ||
               CHR(10) || '                  cat.prod_skid,' ||
               CHR(10) || '                  nvl(cat.num_of_shpmt_wks, bu.num_of_shpmt_wks) - 1 as num_of_shpmt_wks,' ||
               CHR(10) || '                  nvl(cat.post_basln_factr, nvl(bu.post_basln_factr,1)) as post_basln_factr -- Added by David in R9 on 2009-07-10' ||
               CHR(10) || '                FROM' ||
               CHR(10) || '                  -- business unit max promotion length defaults' ||
               CHR(10) || '                  (SELECT' ||
               CHR(10) || '                     bu.bus_unit_skid,' ||
               CHR(10) || '                     nvl(d.num_of_shpmt_wks, g.num_of_shpmt_wks) as num_of_shpmt_wks,' ||
               CHR(10) || '                     nvl(d.post_basln_factr, g.post_basln_factr) as post_basln_factr -- Added by David in R9 on 2009-07-10'||
               CHR(10) || '                   FROM OPT_BUS_UNIT_DIM bu, OPT_BUS_UNIT_PLC g, OPT_BUS_UNIT_PLC d' ||
               CHR(10) || '                   WHERE g.bus_unit_skid = 0' ||
               CHR(10) || '                   AND d.deflt_lvl_name is not null' ||
               CHR(10) || '                   AND d.bus_unit_skid (+) <> 0' ||
               CHR(10) || '                   AND bu.bus_unit_skid = d.bus_unit_skid (+)' ||
               CHR(10) || '                  ) bu,' ||
               CHR(10) || '                  (SELECT /*+ NO_MERGE */' ||
               CHR(10) || '                     h.prod_skid, h.bus_unit_skid, max(cat.post_basln_factr) post_basln_factr,max(cat.num_of_shpmt_wks_qty) keep (dense_rank first order by h.fy_date_skid desc) as num_of_shpmt_wks' ||
               CHR(10) || '                   FROM OPT_PROD_HIER_FDIM h, OPT_BASLN_CATEG_PARM_PLC cat' ||
               CHR(10) || '                   WHERE h.prod_lvl_desc = ''Brand''' ||
               CHR(10) || '                   AND h.prod_4_bus_unit_skid = cat.bus_unit_skid (+)' ||
               CHR(10) || '                   AND h.prod_4_name = cat.prod_name (+)' ||
               CHR(10) || '                   group by h.prod_skid, h.bus_unit_skid' ||
               CHR(10) || '                  ) cat' ||
               CHR(10) || '                WHERE cat.bus_unit_skid = bu.bus_unit_skid' ||
               CHR(10) || '               ) cat,' ||
               CHR(10) || '               -- ignore leading and ending zeros' ||
               CHR(10) || '               (select' ||
               CHR(10) || '                  bus_unit_skid,' ||
               CHR(10) || '                  prod_skid,' ||
               CHR(10) || '                  acct_skid,' ||
               CHR(10) || '                  wk_skid,' ||
               CHR(10) || '                  giv_amt,' ||
               CHR(10) || '                  ship_giv_avg,' ||
               CHR(10) || '                  max(giv_amt) over (partition by prod_skid, acct_skid order by wk_skid asc) as leading_zero_mark,' ||
               CHR(10) || '                  max(giv_amt) over (partition by prod_skid, acct_skid order by wk_skid desc) as ending_zero_mark' ||
               CHR(10) || '               from' ||
               CHR(10) || '                 -- zero-shipments derived with promo-weeks substracted' ||
               CHR(10) || '                 (select /*+ NO_MERGE USE_MERGE(s1 s2) */' ||
               CHR(10) || '                    s1.bus_unit_skid,' ||
               CHR(10) || '                    s1.prod_skid,' ||
               CHR(10) || '                    s1.acct_skid,' ||
               CHR(10) || '                    s1.wk_skid,' ||
               CHR(10) || '                    nvl(s2.giv_amt, 0) as giv_amt,' ||
               CHR(10) || '                    s1.ship_giv_avg' ||
               CHR(10) || '                  from' ||
               CHR(10) || '                    -- time gaps filling' ||
               CHR(10) || '                    (select /*+ USE_MERGE(s c) */' ||
               CHR(10) || '                       s.bus_unit_skid,' ||
               CHR(10) || '                       s.prod_skid,' ||
               CHR(10) || '                       s.acct_skid,' ||
               CHR(10) || '                       s.ship_giv_avg,' ||
               CHR(10) || '                       c.wk_skid' ||
               CHR(10) || '                     from' ||
               CHR(10) || '                       (select' ||
               CHR(10) || '                          distinct wk_skid' ||
               CHR(10) || '                        from CAL_MASTR_DIM' ||
               CHR(10) || '                        where day_date between add_months(trunc(sysdate, ''mm''), -' || P_MTH_CNT || ') and last_day(sysdate)' ||
               CHR(10) || '                       ) c,' ||
               CHR(10) || '                       -- all the prod/acct pairs' ||
               CHR(10) || '                       (select' ||
               CHR(10) || '                          bus_unit_skid, prod_skid, acct_skid,' ||
               CHR(10) || '                          ship_giv_sum / ((max_wk_skid - min_wk_skid) / 7 + 1) as ship_giv_avg,' ||
               CHR(10) || '                          min_wk_skid,' ||
               CHR(10) || '                          max_wk_skid' ||
               CHR(10) || '                        from' ||
               CHR(10) || '                          -- statistical data' ||
               CHR(10) || '                          (select' ||
               CHR(10) || '                             bus_unit_skid, prod_skid, acct_skid,' ||
               CHR(10) || '                             sum(giv_amt) as ship_giv_sum,' ||
               CHR(10) || '                             min(wk_skid) as min_wk_skid,' ||
               CHR(10) || '                             max(wk_skid) as max_wk_skid' ||
               CHR(10) || '                           from ' ||
               I_AGG_FCT_TBL_NAME || CHR(10) ||
               CHR(10) || '                           group by bus_unit_skid, prod_skid, acct_skid' ||
               CHR(10) || '                          )' ||
               CHR(10) || '                       ) s' ||
               CHR(10) || '                     where c.wk_skid between s.min_wk_skid and s.max_wk_skid) s1,' ||
               CHR(10) || '                    (select prod_skid, acct_skid, wk_skid, giv_amt from ' ||
               I_AGG_FCT_TBL_NAME || ') s2' ||
               CHR(10) || '                  where s1.prod_skid = s2.prod_skid (+)' ||
               CHR(10) || '                  and s1.acct_skid = s2.acct_skid (+)' ||
               CHR(10) || '                  and s1.wk_skid = s2.wk_skid (+)' ||
               CHR(10) || '                  and (s1.prod_skid, s1.acct_skid, s1.wk_skid) not in (' ||
               CHR(10) || '                    -- substract promo weeks' ||
               CHR(10) || '                    select prod_skid, acct_skid, wk_skid' ||
               CHR(10) || '                    from ' ||
               I_BASLN_TEMP_TBL_NAME ||
               CHR(10) || '                    where valid_prmtn_ind = CHR(10) || ''Y''' ||
               CHR(10) || '           and   cann_ind = ''N'')' ||
               CHR(10) || '                 )' ||
               CHR(10) || '               ) s' ||
               CHR(10) || '             where s.prod_skid = cat.prod_skid' ||
               CHR(10) || '             and s.leading_zero_mark > 0' ||
               CHR(10) || '             and s.ending_zero_mark > 0)' ||
               CHR(10) || '          union all' ||
               CHR(10) || '        -- empty promo weeks' ||
               CHR(10) || '        select /*+ ORDERED USE_HASH(promo s) */' ||
               CHR(10) || '          promo.prod_skid,' ||
               CHR(10) || '          promo.acct_skid,' ||
               CHR(10) || '          promo.wk_skid,' ||
               CHR(10) || '          nvl(s.giv_amt, 0) as giv_amt,' ||
               CHR(10) || '          null as basln_giv_amt,' ||
               CHR(10) || '          null as ship_giv_avg,' ||
               CHR(10) || '          9998 as num_of_shpmt_wks,' ||
               CHR(10) || '          promo.post_basln_factr,-- Added by David in R9 on 2009-07-10'||
               CHR(10) || '          9999 as cnt' ||
               CHR(10) || '        from' ||
               CHR(10) || '          (select distinct' ||
               CHR(10) || '             prod_skid,' ||
               CHR(10) || '             acct_skid,' ||
               CHR(10) || '             wk_skid,' ||
               CHR(10) || '             post_basln_factr -- Added by David in R9 on 2009-07-10'||
               CHR(10) || '           from ' || I_BASLN_TEMP_TBL_NAME ||
               CHR(10) || '           where valid_prmtn_ind = ''Y''' ||
               CHR(10) || '           and   cann_ind = ''N''' ||
               CHR(10) || '          ) promo,' ||
               CHR(10) || '          (select' ||
               CHR(10) || '             prod_skid, acct_skid, wk_skid, giv_amt' ||
               CHR(10) || '           from ' || I_AGG_FCT_TBL_NAME ||
               CHR(10) || '          ) s' ||
               CHR(10) || '        where promo.prod_skid = s.prod_skid (+)' ||
               CHR(10) || '        and promo.acct_skid = s.acct_skid (+)' ||
               CHR(10) || '        and promo.wk_skid = s.wk_skid (+)' ||
               CHR(10) || '       )' ||
               CHR(10) || '    ) cf' ||
               CHR(10) || '   /* this line ignores leading promotions (when there was no shipments) */' ||
               CHR(10) || '   where not (cf.basln_giv_amt is null and cf.cnt = 9999)' ||
               CHR(10) || '  ) b' ||
               CHR(10) || 'where b.prod_skid = p.prod_skid' ||
               CHR(10) || 'and b.acct_skid = p.acct_skid' ||
               CHR(10) || 'and b.wk_skid = p.wk_skid' ||
               CHR(10) || 'and b.wk_skid >= ' || V_MIN_WK_SKID ||
               CHR(10) || 'group by p.bus_unit_skid, p.prmtn_skid, b.prod_skid, p.root_acct_skid, p.wk_skid';

      PRO_PUT_LINE('SQL:' || CHR(10) || '> ' ||
                   REPLACE(V_SQL, CHR(10), CHR(10) || '> '));
      BEGIN
        EXECUTE IMMEDIATE V_SQL;
        PRO_PUT_LINE('Data created successfuly (' || SQL%ROWCOUNT ||
                     ' rows processed).');
        COMMIT;
      EXCEPTION
        WHEN OTHERS THEN
          PRO_PUT_LINE('Error: Inserting data into ' ||
                       I_BASLN_FCT_TBL_NAME ||
                       ' table failed with error - ' || SQLERRM);
          RAISE_APPLICATION_ERROR(C_TBL_INS_ERROR,
                                  'Error: Inserting data into ' ||
                                  I_BASLN_FCT_TBL_NAME ||
                                  ' table failed with error - ' || SQLERRM);
      END;

      -- drop temporary tables
      -- Commented by David for tracking data issue
      --DROP_TABLE(I_BASLN_TEMP_TBL_NAME);

      -- gather statistics for all involved partitions
      PRO_PUT_LINE('Starting gathering statistics on ' ||
                   I_BASLN_FCT_TBL_NAME || ' table...');
      DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => SYS_CONTEXT('USERENV',
                                                                    'CURRENT_SCHEMA'),
                                    TABNAME          => I_BASLN_FCT_TBL_NAME,
                                    GRANULARITY      => 'ALL',
                                    ESTIMATE_PERCENT => C_STATS_FINAL_EST_PERCENT,
                                    BLOCK_SAMPLE     => FALSE,
                                    DEGREE           => 1,
                                    METHOD_OPT       => 'FOR ALL COLUMNS SIZE AUTO');
      PRO_PUT_LINE('Statistics on table ' || I_BASLN_FCT_TBL_NAME ||
                   ' table gathered.');
    ELSE
      PRO_PUT_LINE('Error: wrong input DATA_TYPE parameter! should be "SHIP" or "EPOS". ');
      RAISE_APPLICATION_ERROR(C_WRONG_PARMS_ERROR,
                              'Error: wrong input DATA_TYPE parameter!');
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      PRO_PUT_LINE('Error: Unexpected system error - ' || SQLERRM);
      RAISE;

  END;

END;
/

