CREATE OR REPLACE PACKAGE BODY ADWU_OPTIMA_LAEX.opt_pkg_prttn_util
IS

  -------------------------------------------
  -- Private method declarations
  -------------------------------------------
/*
  PROCEDURE pro_check_tables(p_vc2_srce_tbl    IN  VARCHAR2,
                 p_vc2_dest_tbl    IN  VARCHAR2
  );
*/
  PROCEDURE pro_check_tbl_prttn(p_vc2_tbl_name        IN  user_tables.table_name%TYPE,
                p_vc2_tbl_prttn_str    OUT VARCHAR2,
                                p_num_tbl_prttn_count   OUT NUMBER
  );

/*
  PROCEDURE pro_check_tbl_sub_prttn(p_vc2_tbl_name           IN  user_tables.table_name%TYPE,
                    p_vc2_tbl_sub_prttn_str    OUT VARCHAR2,
                                    p_num_tbl_sub_prttn_count  OUT NUMBER
  );
/*
  PROCEDURE pro_create_temp_tbl(p_vc2_tbl_name         IN  user_tables.table_name%TYPE,
                p_vc2_temp_tbl         OUT user_tables.table_name%TYPE
  );
*/
  PROCEDURE pro_exchange_prttn(p_vc2_srce_tbl           IN  user_tables.table_name%TYPE,
                   p_vc2_dest_tbl           IN  user_tables.table_name%TYPE,
                   p_vc2_temp_tbl           IN  user_tables.table_name%TYPE,
                               p_vc2_min_cal_skid      IN  cal_mastr_dim.cal_mastr_skid%TYPE DEFAULT NULL,
                               p_vc2_region           IN  VARCHAR2 DEFAULT NULL,
                               p_vc2_prttn_type_code   IN  VARCHAR2
  );

  PROCEDURE pro_add_latest_prttn(p_vc2_tbl_name      IN  user_tables.table_name%TYPE,
                 p_vc2_cal_skid      IN  cal_mastr_dim.cal_mastr_skid%TYPE,
                                 p_vc2_prttn_type_code   IN  VARCHAR2
  );




  -------------------------------------------
  -- Begin definitions of public methods
  -------------------------------------------

  PROCEDURE pro_promote_data(p_vc2_srce_tbl        IN    user_tables.table_name%TYPE,
                 p_vc2_dest_tbl        IN    user_tables.table_name%TYPE,
                             p_vc2_min_cal_skid     IN    cal_mastr_dim.cal_mastr_skid%TYPE,
                             p_vc2_region        IN    VARCHAR2,
                 p_vc2_ctrlm_ordr_id    IN    VARCHAR2,
                             p_vc2_prttn_type_code  IN    VARCHAR2,
							 V_REGN IN VARCHAR2 DEFAULT NULL  --SOX Project Implementation
                )
  /*
  ***************************************************************************
  * Program : pro_promote_data
  * Version : 1.0
  * Author  : Brian Beckman
  * Date    : 30-OCT-2005
  * Purpose : Promotes data from a source table to a destination table
  * Parameters :  p_vc2_srce_tbl      -- Source table for promotion
  *          p_vc2_dest_tbl      -- Destination table for promotion
  *          p_vc2_min_cal_skid  -- Minimum cal SKID for promotion -- any
  *                       cal_skids older than this will not be promoted
  *          p_vc2_region          -- Region code for promotion
  *          p_vc2_ctrlm_ordr_id -- CTRLM order id for the calling job
  *          p_vc2_prttn_type_code    -- Indicates if this is monthly or daily partitinoing
  *                        Possible values of 'M', 'D', ''Q', or 'F'
  *
  * Note:  This procedure does not provide error handling --- calling code must
  *         handle errors returned from this procedure
  *
  * Change History
  * Date     Programmer        Description
  * -------------------------------------------------------------------------
  * 30-OCT-2005  Brian Beckman        Initial Version
  * 20-JUN-2006  Jenny Hays        Added valid values of Q and F
  ****************************************************************************
  */
  IS
    v_vc2_temp_tbl       user_tables.table_name%TYPE;
    v_vc2_srce_tbl           user_tables.table_name%TYPE := UPPER(p_vc2_srce_tbl);
    v_vc2_dest_tbl           user_tables.table_name%TYPE := UPPER(p_vc2_dest_tbl);
    v_vc2_region           VARCHAR2(30) := UPPER(p_vc2_region);
    v_vc2_prttn_type_code  VARCHAR2(1) := UPPER(p_vc2_prttn_type_code);
  BEGIN
    opt_pkg_util.pro_put_line('Beginning promotion from '||v_vc2_srce_tbl||' to '||v_vc2_dest_tbl);

    -- Verify promotion type
    IF (v_vc2_prttn_type_code != 'M' AND v_vc2_prttn_type_code != 'D'
        AND v_vc2_prttn_type_code != 'Q' AND v_vc2_prttn_type_code != 'F')
    THEN
      opt_pkg_util.pro_put_line('Promotion type must be either M, D, Q, or F');
      RAISE_APPLICATION_ERROR(-20010,'Promotion type of '||v_vc2_prttn_type_code||' not valid');
    END IF;

    -- Check that the tables are setup properly for promotion
    pro_check_tables(v_vc2_srce_tbl, v_vc2_dest_tbl);

    -- Create temp table
    pro_create_temp_tbl(v_vc2_dest_tbl, v_vc2_temp_tbl,V_REGN);  --SOX Project Implementation

    -- Create missing partitions in destination table
    pro_add_latest_prttn(v_vc2_dest_tbl, NULL, v_vc2_prttn_type_code);

    -- Start process to exchange between source and destination
    pro_exchange_prttn(v_vc2_srce_tbl,v_vc2_dest_tbl,v_vc2_temp_tbl,p_vc2_min_cal_skid,v_vc2_region,v_vc2_prttn_type_code);

    -- Cleanup temp table
    EXECUTE IMMEDIATE 'DROP TABLE '||v_vc2_temp_tbl;

    opt_pkg_util.pro_put_line('Successfully ending promotion from '||v_vc2_srce_tbl||' to '||v_vc2_dest_tbl);

  END pro_promote_data;


  PROCEDURE pro_exchange_tbl_with_prttn(p_vc2_prttn_tbl       IN user_tables.table_name%TYPE,
                    p_vc2_prttn          IN user_tab_partitions.partition_name%TYPE,
                    p_vc2_temp_tbl          IN user_tables.table_name%TYPE,
                                        p_vc2_subprttn_flag   IN VARCHAR2
                       )
  /*
  ***************************************************************************
  * Program : pro_exchange_tbl_with_prttn
  * Version : 1.0
  * Author  : Brian Beckman
  * Date    : 30-OCT-2005
  * Purpose : Exchanges a temp table with a partition of a partitioned table
  * Parameters :  p_vc2_prttn_tbl -- partitioned table
  *          p_vc2_prttn -- partition or subpartition to be exchanged
  *          p_vc2_temp_tbl -- temp table
  *          p_vc2_subprttn_flag -- flag to indicate if a partition or
  *              subpartition will be exchanged ---- Y or N
  *
  * Note:  This procedure does not provide error handling --- calling code must
  *         handle errors returned from this procedure
  *
  * Change History
  * Date     Programmer        Description
  * -------------------------------------------------------------------------
  * 30-OCT-2005  Brian Beckman        Initial Version
  ****************************************************************************
  */
  IS
    v_vc2_sql_stmt     VARCHAR2(500);
    v_vc2_prttn_exchg    VARCHAR2(25);
    v_vc2_prttn_tbl      user_tables.table_name%TYPE := UPPER(p_vc2_prttn_tbl);
    v_vc2_prttn         user_tab_partitions.partition_name%TYPE := UPPER(p_vc2_prttn);
    v_vc2_temp_tbl         user_tables.table_name%TYPE := UPPER(p_vc2_temp_tbl);
    v_vc2_subprttn_flag  VARCHAR2(1) := UPPER(p_vc2_subprttn_flag);
  BEGIN

    IF (v_vc2_subprttn_flag = 'N')
    THEN
      v_vc2_prttn_exchg:='EXCHANGE PARTITION ';
    ELSIF (v_vc2_subprttn_flag = 'Y')
    THEN
      v_vc2_prttn_exchg:='EXCHANGE SUBPARTITION ';
    ELSE
      RAISE_APPLICATION_ERROR(-20020,'ERROR!!!  Subpartition flag of '||v_vc2_subprttn_flag||' not valid');
    END IF;

    v_vc2_sql_stmt := 'ALTER TABLE '||v_vc2_prttn_tbl||' '||
              v_vc2_prttn_exchg||v_vc2_prttn||' '||
                      'WITH TABLE '||v_vc2_temp_tbl||' '||
                      'INCLUDING INDEXES WITHOUT VALIDATION';

    opt_pkg_util.pro_put_line(v_vc2_sql_stmt);

    EXECUTE IMMEDIATE v_vc2_sql_stmt;

    opt_pkg_util.pro_put_line('Exchange successful');

  END pro_exchange_tbl_with_prttn;


  PROCEDURE pro_rebuild_indexes(p_vc2_tbl_name          IN  user_tables.table_name%TYPE,
                p_vc2_ctrlm_ordr_id   IN  VARCHAR2
                   )
  /*
  ***************************************************************************
  * Program : pro_rebuild_indexes
  * Version : 1.0
  * Author  : Brian Beckman
  * Date    : 30-OCT-2005
  * Purpose : Rebuilds indexes on a table
  * Parameters :  p_vc2_tbl_name  -- Table to rebuild indexes on
  *          p_vc2_ctrlm_ordr_id -- CTRLM order id
  *
  * Metadata table:  PRTTN_TBL_DETL_PRC - used to get the maximum number of seconds to
  *                    wait while performing index rebuilds
  *
  * Logging table:  PRTTN_INDX_RBLD_STTUS_PLC - index rebuilds are logged here
  *
  * Note:  This procedure does not provide error handling --- calling code must
  *         handle errors returned from this procedure
  *
  * Change History
  * Date     Programmer        Description
  * -------------------------------------------------------------------------
  * 30-OCT-2005  Brian Beckman        Initial Version
  * 08-Aug-2009  Bodhi Wang        change type of v_num_tbl_sub_prttn_count as number.
  ****************************************************************************
  */
  IS
    v_vc2_tbl_sub_prttn_str    VARCHAR2(100);
    v_num_tbl_sub_prttn_count  NUMBER;
    v_vc2_subprttn_flag        VARCHAR2(1);
    v_vc2_sqlstmt           VARCHAR2(1000);
    v_num_alert_sttus       NUMBER(1);
    v_vc2_alert_mesg       VARCHAR2(2000);
    v_num_jobnum           NUMBER;
    v_vc2_rbld_start_datetm    VARCHAR2(30);
    v_num_rbld_error_count       NUMBER;
    v_num_rbld_sec           PRTTN_TBL_DETL_PRC.index_rbld_wait_scnd_qty%TYPE;
    v_vc2_output_str       VARCHAR2(4000);
    v_num_index_count       NUMBER(2);
    v_vc2_tbl_name           user_tables.table_name%TYPE := UPPER(p_vc2_tbl_name);

    CURSOR cur_index IS
    SELECT index_name
    FROM user_indexes
    WHERE table_name=v_vc2_tbl_name;

  BEGIN
    opt_pkg_util.pro_put_line('Starting procedure adw_prttn_util.rebuild_indexes');
    -- Verify that indexes exist on the table
    SELECT COUNT(1)
    INTO v_num_index_count
    FROM user_indexes
    WHERE table_name=v_vc2_tbl_name;

    IF (v_num_index_count = 0)
    THEN
      opt_pkg_util.pro_put_line('No indexes exist on '||v_vc2_tbl_name);
    ELSE
      -- Check if subpartitioned
      pro_check_tbl_sub_prttn(v_vc2_tbl_name, v_vc2_tbl_sub_prttn_str,v_num_tbl_sub_prttn_count);

      IF (v_num_tbl_sub_prttn_count = 0)
      THEN
    v_vc2_subprttn_flag:='N';
      ELSE
        v_vc2_subprttn_flag:='Y';
      END IF;

      SELECT TO_CHAR(SYSDATE,'DD-MON-YYYY HH24:MI:SS')
      INTO v_vc2_rbld_start_datetm
      FROM dual;

      -- Loop thru all indexes on the source table and rebuild each one
      FOR rec_index IN cur_index
      LOOP
        opt_pkg_util.pro_put_line('Rebuilding index '||rec_index.index_name);
        -- Remove the alert if it already exists
    DBMS_ALERT.REMOVE(rec_index.index_name);
    -- Register the alert
        DBMS_ALERT.REGISTER(rec_index.index_name);
        -- Kick off the execution
    v_vc2_sqlstmt := 'opt_pkg_prttn_util.pro_rebuild_index_prttn('||
                 ''''||v_vc2_tbl_name||''','||
                          ''''||rec_index.index_name||''','||
                         ''''||v_vc2_subprttn_flag||''','||
                         ''''||p_vc2_ctrlm_ordr_id||''','||
                         ''''||v_vc2_rbld_start_datetm||''');';
        opt_pkg_util.pro_put_line(v_vc2_sqlstmt);
        DBMS_JOB.SUBMIT(v_num_jobnum,v_vc2_sqlstmt);
        COMMIT;
      END LOOP;

      -- Get the max amount of time to wait for index rebuilds to finish
      -- Default to 1000 seconds if none specified in metadata
      SELECT COUNT(1)
      INTO v_num_index_count
      FROM PRTTN_TBL_DETL_PRC
      WHERE UPPER(tbl_name)=v_vc2_tbl_name;

      IF (v_num_index_count = 1)
      THEN
        SELECT NVL(index_rbld_wait_scnd_qty,1000)
        INTO v_num_rbld_sec
        FROM PRTTN_TBL_DETL_PRC
        WHERE UPPER(tbl_name)=v_vc2_tbl_name;
      ELSE
        v_num_rbld_sec := 1000;
      END IF;

      FOR rec_index IN cur_index
      LOOP
        -- Wait for each index rebuild to finish
        DBMS_ALERT.WAITONE(rec_index.index_name,v_vc2_alert_mesg,v_num_alert_sttus,v_num_rbld_sec);
        IF (v_num_alert_sttus != 0)
        THEN
          -- Job timed out
          RAISE_APPLICATION_ERROR(-20030,'ERROR!!! Rebuild of index '||rec_index.index_name||' timed out');
        ELSE
          --Job was successful
          opt_pkg_util.pro_put_line('Index '||rec_index.index_name||' rebuilt successfully');
    END IF;
      END LOOP;

      FOR rec_index IN cur_index
      LOOP
        -- Deregister the alert
        DBMS_ALERT.REMOVE(rec_index.index_name);
      END LOOP;

      -- Check log table to make sure entries were made into it
      SELECT COUNT(1)
      INTO v_num_rbld_error_count
      FROM PRTTN_INDEX_RBLD_STTUS_PLC
      WHERE ctrlm_ordr_id = p_vc2_ctrlm_ordr_id
      AND    rbld_start_datetm = TO_DATE(v_vc2_rbld_start_datetm,'DD-MON-YYYY HH24:MI:SS')
      AND    tbl_name = p_vc2_tbl_name;

      IF (v_num_rbld_error_count = 0)
      THEN
        RAISE_APPLICATION_ERROR(-20032,'ERROR!!!  No entries made to PRTTN_INDEX_RBLD_STTUS_PLC');
      END IF;

      -- Check log table to see if index rebuilds were successful
      SELECT COUNT(1)
      INTO v_num_rbld_error_count
      FROM PRTTN_INDEX_RBLD_STTUS_PLC
      WHERE ctrlm_ordr_id = p_vc2_ctrlm_ordr_id
      AND    rbld_start_datetm = TO_DATE(v_vc2_rbld_start_datetm,'DD-MON-YYYY HH24:MI:SS')
      AND    tbl_name = p_vc2_tbl_name
      AND    rbld_err_msg_desc IS NOT NULL;

      IF (v_num_rbld_error_count != 0)
      THEN
        -- Loop thru the logging table and output the error messages
    FOR rec_index IN (SELECT index_name,rbld_err_msg_desc
                  FROM PRTTN_INDEX_RBLD_STTUS_PLC
                           WHERE ctrlm_ordr_id = p_vc2_ctrlm_ordr_id
                          AND    rbld_start_datetm = TO_DATE(v_vc2_rbld_start_datetm,'DD-MON-YYYY HH24:MI:SS')
                          AND    tbl_name = p_vc2_tbl_name
                          AND    rbld_err_msg_desc IS NOT NULL
                         )
        LOOP
          opt_pkg_util.pro_put_line('Index '||rec_index.index_name||' rebuild failed with error: '||rec_index.rbld_err_msg_desc);
    END LOOP;
        RAISE_APPLICATION_ERROR(-20031,'ERROR!!!  Index rebuilds did not complete successfully');
      END IF;

      opt_pkg_util.pro_put_line('Index rebuilds successful');
    END IF;

    opt_pkg_util.pro_put_line('Ending procedure adw_prttn_util.rebuild_indexes');

  END pro_rebuild_indexes;



  PROCEDURE pro_rebuild_index_prttn(p_vc2_tbl_name        IN    user_tables.table_name%TYPE,
                    p_vc2_index_name        IN    user_indexes.index_name%TYPE,
                    p_vc2_subprttn_flag     IN    VARCHAR2,
                                    p_vc2_ctrlm_ordr_id     IN    PRTTN_INDEX_RBLD_STTUS_PLC.ctrlm_ordr_id%TYPE,
                                    p_vc2_rbld_start_datetm IN  VARCHAR2
                   )
  /*
  ***************************************************************************
  * Program : pro_rebuild_index_prttn
  * Version : 1.0
  * Author  : Brian Beckman
  * Date    : 30-OCT-2005
  * Purpose : Rebuilds index partitions on a table
  * Parameters :  p_vc2_index_name -- Index name
  *          p_vc2_subprttn_flag -- Indicates if index is subpartitioned
  *            Possible values are Y or N
  *
  * THIS PROCEDURE SHOULD NEVER BE CALLED DIRECTLY AND SHOULD ONLY BE CALLED VIA
  *   THE rebuild_all_indexes PROCEDURE
  *
  * Logging table:  PRTTN_INDX_RBLD_STTUS_PLC - used to log the index rebuild
  *                         status information
  *
  * Note:  This procedure does not provide error handling --- calling code must
  *         handle errors returned from this procedure
  *
  * Change History
  * Date     Programmer        Description
  * -------------------------------------------------------------------------
  * 30-OCT-2005  Brian Beckman        Initial Version
  ****************************************************************************
  */
  IS
    v_vc2_sql_stmt    VARCHAR2(100);
    v_num_job_id      NUMBER;
    v_vc2_sqlerrm      VARCHAR2(512);
  BEGIN
    --Must use bg_job_id, not fg_job_id when this code is called from DBMS_JOB
    SELECT SYS_CONTEXT ('userenv','bg_job_id')
    INTO v_num_job_id
    FROM DUAL;

    -- Log the start of the index rebuilds
    INSERT INTO PRTTN_INDEX_RBLD_STTUS_PLC(tbl_name,index_name,ctrlm_ordr_id,dbms_job_num,rbld_start_datetm,RBLD_END_DATETM,RBLD_ERR_MSG_DESC)
    VALUES (p_vc2_tbl_name,
        p_vc2_index_name,
            p_vc2_ctrlm_ordr_id,
            v_num_job_id,
            TO_DATE(p_vc2_rbld_start_datetm,'DD-MON-YYYY HH24:MI:SS'),
            NULL,
            NULL
           );

    COMMIT;

    IF (p_vc2_subprttn_flag) = 'N'
    THEN
      -- Index is partitioned
      -- Loop thru all partitions and rebuild them
      FOR rec IN (SELECT partition_name
              FROM user_ind_partitions
                  WHERE index_name=p_vc2_index_name
                  AND    status='UNUSABLE'
                 )
      LOOP
        v_vc2_sql_stmt := 'ALTER INDEX '||p_vc2_index_name||' REBUILD PARTITION '||rec.partition_name||' NOLOGGING';
        EXECUTE IMMEDIATE v_vc2_sql_stmt;
      END LOOP;
    ELSE
      -- Index is subpartitioned
      -- Loop thru all subpartitions and rebuild them
      FOR rec IN (SELECT subpartition_name
              FROM user_ind_subpartitions
                  WHERE index_name=p_vc2_index_name
                  AND    status='UNUSABLE'
                 )
      LOOP
        v_vc2_sql_stmt := 'ALTER INDEX '||p_vc2_index_name||' REBUILD SUBPARTITION '||rec.subpartition_name;
        EXECUTE IMMEDIATE v_vc2_sql_stmt;
      END LOOP;
    END IF;

    -- Log the end of the rebuilds
    UPDATE PRTTN_INDEX_RBLD_STTUS_PLC
    SET rbld_end_datetm = SYSDATE
    WHERE ctrlm_ordr_id = p_vc2_ctrlm_ordr_id
    AND   tbl_name = p_vc2_tbl_name
    AND   index_name = p_vc2_index_name
    AND   rbld_start_datetm = TO_DATE(p_vc2_rbld_start_datetm,'DD-MON-YYYY HH24:MI:SS');

    COMMIT;

    -- Signal success
    DBMS_ALERT.SIGNAL(p_vc2_index_name,p_vc2_index_name||' - COMPLETE');
  COMMIT; --Added in R8, Added commit statement to avoid overtime error.

  EXCEPTION
  WHEN OTHERS
    THEN
      v_vc2_sqlerrm := SQLERRM;

      -- Log the error
      UPDATE PRTTN_INDEX_RBLD_STTUS_PLC
      SET rbld_end_datetm = SYSDATE,
      rbld_err_msg_desc = v_vc2_sqlerrm
      WHERE ctrlm_ordr_id = p_vc2_ctrlm_ordr_id
      AND    tbl_name = p_vc2_tbl_name
      AND    index_name = p_vc2_index_name
      AND    rbld_start_datetm = TO_DATE(p_vc2_rbld_start_datetm,'DD-MON-YYYY HH24:MI:SS');

      COMMIT;

      -- Signal failure
      DBMS_ALERT.SIGNAL(p_vc2_index_name,p_vc2_index_name||' - FAILURE');
    COMMIT; --Added commit statement to avoid overtime error.

  END pro_rebuild_index_prttn;


  PROCEDURE pro_initialize_mth_prttn (p_vc2_tbl_name         IN  user_tables.table_name%TYPE,
                      p_num_past_mth_num     IN  NUMBER,
                      p_num_future_mth_num     IN  NUMBER,
                                      p_vc2_drop_prttn_flag  IN  VARCHAR2
                     )
   /*
   ***************************************************************************
   * Program : pro_initialize_mth_prttn
   * Version : 1.0
   * Author  : Brian Beckman
   * Date    : 02-AUG-2005
   * Purpose : Fully rebuilds the monthly partitioning of a table
   *           Should only be done in preparation for a full load of data
   *
   * Note:  This procedure does not provide error handling --- calling code must
   *          handle errors returned from this procedure
   *
   * Change History
   * Date      Programmer         Description
   * -------------------------------------------------------------------------
   * 02-AUG-2005  Brian Beckman      Initial Version
   * 21-JUN-2006  Brian Beckman      Converted to use CAL_MASTR_DIM
   ****************************************************************************
   */
   IS
      v_vc2_max_prttn           user_tab_partitions.partition_name%TYPE;
      v_vc2_sqlstmt           VARCHAR2(500);
      v_vc2_sqlerrm           VARCHAR2(512);
      v_num_sqlcode           NUMBER;
      v_num_tbl_count       NUMBER;
      v_vc2_tbl_name       user_tables.table_name%TYPE := UPPER(p_vc2_tbl_name);
      v_vc2_drop_prttn_flag    VARCHAR2(1) := UPPER(p_vc2_drop_prttn_flag);

      CURSOR prttn_cur_typ IS
      SELECT partition_name
      FROM user_tab_partitions
      WHERE table_name = v_vc2_tbl_name
      AND    partition_position != (SELECT MAX(partition_position)
                   FROM user_tab_partitions
                   WHERE table_name = v_vc2_tbl_name
                                  );

   BEGIN

     opt_pkg_util.pro_check_tbl_exist(v_vc2_tbl_name, v_num_tbl_count);
     IF (v_num_tbl_count = 0)
     THEN
       RAISE_APPLICATION_ERROR(-20060,'ERROR!!! '||v_vc2_tbl_name||' does not exist');
     ELSE
       opt_pkg_util.pro_put_line('Table '||v_vc2_tbl_name||' exists');
     END IF;

     IF (v_vc2_drop_prttn_flag='Y')
     THEN
       --First drop all old partitions if they exist
       opt_pkg_util.pro_put_line('Preparing to drop partitions from '||v_vc2_tbl_name);
       FOR prttn_rec_typ IN prttn_cur_typ
       LOOP
     v_vc2_sqlstmt:='ALTER TABLE '||v_vc2_tbl_name||' '||CHR(10)||
                'DROP PARTITION '||prttn_rec_typ.partition_name;

     opt_pkg_util.pro_put_line('Executing: '||v_vc2_sqlstmt);
     EXECUTE IMMEDIATE v_vc2_sqlstmt;
       END LOOP;
     ELSE
       opt_pkg_util.pro_put_line('Not dropping existing partitions from '||v_vc2_tbl_name);
     END IF;

     --Get maxvalue partition name
     SELECT partition_name
     INTO v_vc2_max_prttn
     FROM user_tab_partitions
     WHERE table_name = v_vc2_tbl_name
     AND   partition_position = (SELECT MAX(partition_position)
                 FROM user_tab_partitions
                 WHERE table_name = v_vc2_tbl_name
                                );

     --Now loop thru all monthly SKIDs and create the partitions
     FOR mth_rec_typ IN (SELECT DISTINCT mth_skid+mth_days_in_mth_qty AS mth_skid,
                    TO_CHAR(TRUNC(mth_start_date,'MM'),'YYYYMM') AS mth_name
             FROM cal_mastr_dim
             WHERE mth_start_date BETWEEN TRUNC(ADD_MONTHS(SYSDATE,-1 * p_num_past_mth_num),'MM')
                          AND     TRUNC(ADD_MONTHS(SYSDATE,p_num_future_mth_num),'MM')
                         AND NOT EXISTS (SELECT 1
                                 FROM user_tab_partitions
                                         WHERE partition_name = 'P'||TO_CHAR(TRUNC(mth_start_date,'MM'),'YYYYMM')
                                         AND   table_name = v_vc2_tbl_name
                                        )
             ORDER BY mth_skid ASC
                         )
     LOOP
       v_vc2_sqlstmt := 'ALTER TABLE '||v_vc2_tbl_name||' '||CHR(10)||
                'SPLIT PARTITION '||v_vc2_max_prttn||' '||
                'AT ('||TO_CHAR(mth_rec_typ.mth_skid)||') '||CHR(10)||
                        'INTO (PARTITION P'||mth_rec_typ.mth_name||', '||CHR(10)||
                        '      PARTITION '||v_vc2_max_prttn||CHR(10)||'     )';

       opt_pkg_util.pro_put_line('Creating partition P'||mth_rec_typ.mth_name);
       opt_pkg_util.pro_put_line(v_vc2_sqlstmt);

       EXECUTE IMMEDIATE v_vc2_sqlstmt;

     END LOOP;

   END pro_initialize_mth_prttn;


   PROCEDURE pro_initialize_day_prttn (p_vc2_tbl_name          IN  user_tables.table_name%TYPE,
                       p_num_past_day_num      IN  NUMBER,
                       p_num_future_day_num   IN  NUMBER,
                                       p_vc2_drop_prttn_flag  IN  VARCHAR2
                     )
   /*
   ***************************************************************************
   * Program : pro_initialize_day_prttn
   * Version : 1.0
   * Author  : Brian Beckman
   * Date    : 02-AUG-2005
   * Purpose : Fully rebuilds the daily partitioning of a table
   *           Should only be done in preparation for a full load of data
   *
   * Note:  This procedure does not provide error handling --- calling code must
   *          handle errors returned from this procedure
   *
   * Change History
   * Date      Programmer         Description
   * -------------------------------------------------------------------------
   * 02-AUG-2005  Brian Beckman      Initial Version
   * 21-JUN-2006  Brian Beckman      Corrected bug in daily partition creation loop
   *                     to TRUNC the sysdate comparison ranges to prevent time
   *                     from becoming part of the comparison and converted to use
   *                     CAL_MASTR_DIM.
   ****************************************************************************
   */
   IS
      v_vc2_max_prttn           user_tab_partitions.partition_name%TYPE;
      v_vc2_sqlstmt           VARCHAR2(500);
      v_vc2_sqlerrm           VARCHAR2(512);
      v_num_sqlcode           NUMBER;
      v_num_tbl_count       NUMBER;
      v_vc2_tbl_name       user_tables.table_name%TYPE := UPPER(p_vc2_tbl_name);
      v_vc2_drop_prttn_flag    VARCHAR2(1) := UPPER(p_vc2_drop_prttn_flag);

      CURSOR prttn_cur_typ IS
      SELECT partition_name
      FROM user_tab_partitions
      WHERE table_name = v_vc2_tbl_name
      AND    partition_position != (SELECT MAX(partition_position)
                   FROM user_tab_partitions
                   WHERE table_name = v_vc2_tbl_name
                                  );

   BEGIN

     opt_pkg_util.pro_check_tbl_exist(v_vc2_tbl_name, v_num_tbl_count);
     IF (v_num_tbl_count = 0)
     THEN
       RAISE_APPLICATION_ERROR(-20070,'ERROR!!! '||v_vc2_tbl_name||' does not exist');
     ELSE
       opt_pkg_util.pro_put_line('Table '||v_vc2_tbl_name||' exists');
     END IF;

     IF (v_vc2_drop_prttn_flag='Y')
     THEN
       --First drop all old partitions if they exist
       opt_pkg_util.pro_put_line('Preparing to drop partitions from '||v_vc2_tbl_name);
       FOR prttn_rec_typ IN prttn_cur_typ
       LOOP
     v_vc2_sqlstmt:='ALTER TABLE '||v_vc2_tbl_name||' '||CHR(10)||
                'DROP PARTITION '||prttn_rec_typ.partition_name;

     opt_pkg_util.pro_put_line('Executing: '||v_vc2_sqlstmt);
     EXECUTE IMMEDIATE v_vc2_sqlstmt;
       END LOOP;
     ELSE
       opt_pkg_util.pro_put_line('Not dropping existing partitions from '||v_vc2_tbl_name);
     END IF;

     --Get maxvalue partition name
     SELECT partition_name
     INTO v_vc2_max_prttn
     FROM user_tab_partitions
     WHERE table_name = v_vc2_tbl_name
     AND   partition_position = (SELECT MAX(partition_position)
                 FROM user_tab_partitions
                 WHERE table_name = v_vc2_tbl_name
                                );

     --Now loop thru all daily SKIDs and create the partitions
     FOR day_rec_typ IN (SELECT DISTINCT a.cal_mastr_skid+1 AS day_skid,
                    TO_CHAR(a.day_date,'YYYYMMDD') AS day_name
             FROM cal_mastr_dim a
             WHERE a.day_date BETWEEN TRUNC(SYSDATE + -1 * p_num_past_day_num,'DD')
                      AND      TRUNC(SYSDATE + p_num_future_day_num,'DD')
             AND NOT EXISTS (SELECT 1
                                 FROM user_tab_partitions
                                         WHERE partition_name = 'P'||TO_CHAR(a.day_date,'YYYYMMDD')
                                         AND   table_name = v_vc2_tbl_name
                                        )
             ORDER BY day_skid ASC
                         )
     LOOP
       v_vc2_sqlstmt := 'ALTER TABLE '||v_vc2_tbl_name||' '||CHR(10)||
                'SPLIT PARTITION '||v_vc2_max_prttn||' '||
                'AT ('||TO_CHAR(day_rec_typ.day_skid)||') '||CHR(10)||
                        'INTO (PARTITION P'||day_rec_typ.day_name||', '||CHR(10)||
                        '      PARTITION '||v_vc2_max_prttn||CHR(10)||'     )';

       opt_pkg_util.pro_put_line('Creating partition P'||day_rec_typ.day_name);
       opt_pkg_util.pro_put_line(v_vc2_sqlstmt);

       EXECUTE IMMEDIATE v_vc2_sqlstmt;

     END LOOP;

   END pro_initialize_day_prttn;



  -------------------------------------------
  --Begin definitions of private methods
  -------------------------------------------

  PROCEDURE pro_check_tables(p_vc2_srce_tbl    IN  VARCHAR2,
                 p_vc2_dest_tbl    IN  VARCHAR2
                            )
  /*
  ***************************************************************************
  * Program : pro_check_tables
  * Version : 1.0
  * Author  : Brian Beckman
  * Date    : 30-OCT-2005
  * Purpose : Checks that tables are aligned for promotion
  * Parameters :  p_vc2_srce_tbl  -- Source table
  *          p_vc2_dest_tbl  --  Destination table
  *
  * Note:  This procedure does not provide error handling --- calling code must
  *         handle errors returned from this procedure
  *
  * Change History
  * Date     Programmer        Description
  * -------------------------------------------------------------------------
  * 30-OCT-2005  Brian Beckman        Initial Version
  ****************************************************************************
  */
  IS
    v_num_srce_tbl_count           NUMBER(1);
    v_num_dest_tbl_count           NUMBER(1);
    v_vc2_srce_tbl_prttn_str       VARCHAR2(100);
    v_vc2_dest_tbl_prttn_str       VARCHAR2(100);
    v_num_srce_tbl_prttn_count       NUMBER(4);
    v_num_dest_tbl_prttn_count           NUMBER(4);
    v_vc2_srce_tbl_sub_prttn_str       VARCHAR2(100);
    v_vc2_dest_tbl_sub_prttn_str       VARCHAR2(100);
    v_num_srce_tbl_sub_prttn_count       NUMBER(4);
    v_num_dest_tbl_sub_prttn_count       NUMBER(4);
  BEGIN

    -------------------------------------------
    -- Check that the tables are not the same
    -------------------------------------------
    IF (p_vc2_srce_tbl = p_vc2_dest_tbl)
    THEN
      RAISE_APPLICATION_ERROR(-20001,'ERROR!!! '||p_vc2_srce_tbl||' is the same as '||p_vc2_dest_tbl);
    END IF;

    -------------------------------------------
    -- Check that the tables exist
    -------------------------------------------
    opt_pkg_util.pro_check_tbl_exist(p_vc2_srce_tbl, v_num_srce_tbl_count);
    opt_pkg_util.pro_check_tbl_exist(p_vc2_dest_tbl, v_num_dest_tbl_count);
    IF (v_num_srce_tbl_count = 0)
    THEN
      RAISE_APPLICATION_ERROR(-20002,'ERROR!!! '||p_vc2_srce_tbl||' does not exist');
    ELSE
      opt_pkg_util.pro_put_line('Table '||p_vc2_srce_tbl||' exists');
    END IF;

    IF (v_num_dest_tbl_count = 0)
    THEN
      RAISE_APPLICATION_ERROR(-20003,'ERROR!!! '||p_vc2_dest_tbl||' does not exist');
    ELSE
      opt_pkg_util.pro_put_line('Table '||p_vc2_dest_tbl||' exists');
    END IF;

    -------------------------------------------
    -- Check if both tables are partitioned
    -------------------------------------------
    pro_check_tbl_prttn(p_vc2_srce_tbl, v_vc2_srce_tbl_prttn_str, v_num_srce_tbl_prttn_count);
    pro_check_tbl_prttn(p_vc2_dest_tbl, v_vc2_dest_tbl_prttn_str, v_num_dest_tbl_prttn_count);

    IF (v_num_srce_tbl_prttn_count = 0 OR v_num_dest_tbl_prttn_count = 0)
    THEN
      --This means that at least one table is not partitioned
      RAISE_APPLICATION_ERROR(-20004,'ERROR!!! Either '||p_vc2_srce_tbl||' or '||p_vc2_dest_tbl||' is not partitioned');
    END IF;

    IF (v_vc2_srce_tbl_prttn_str != v_vc2_dest_tbl_prttn_str)
    THEN
      RAISE_APPLICATION_ERROR(-20005,'ERROR!!! '||p_vc2_srce_tbl||' is not partitioned the same as '||p_vc2_dest_tbl);
    END IF;

    -------------------------------------------
    -- Check the subpartitioning of both tables
    -------------------------------------------
    pro_check_tbl_sub_prttn(p_vc2_srce_tbl, v_vc2_srce_tbl_sub_prttn_str, v_num_srce_tbl_sub_prttn_count);
    pro_check_tbl_sub_prttn(p_vc2_dest_tbl, v_vc2_dest_tbl_sub_prttn_str, v_num_dest_tbl_sub_prttn_count);

    IF (v_vc2_srce_tbl_sub_prttn_str != v_vc2_dest_tbl_sub_prttn_str )
    THEN
      RAISE_APPLICATION_ERROR(-20006,'ERROR!!! '||p_vc2_srce_tbl||' is not subpartitioned the same as '||p_vc2_dest_tbl);
    END IF;

  END pro_check_tables;



  PROCEDURE pro_check_tbl_prttn(p_vc2_tbl_name        IN  user_tables.table_name%TYPE,
                p_vc2_tbl_prttn_str    OUT VARCHAR2,
                                p_num_tbl_prttn_count   OUT NUMBER
                   )
  /*
  ***************************************************************************
  * Program : pro_check_tbl_prttn
  * Version : 1.0
  * Author  : Brian Beckman
  * Date    : 30-OCT-2005
  * Purpose : Checks if a table is partitioned
  * Parameters :  p_vc2_tbl_name  -- Table to be checked for partitioning
  *          p_vc2_tbl_prttn_str -- Partitioning key column string in format
  *                         -1-ColA-2-ColB-3-ColC...etc
  *          p_num_tbl_prttn_count -- Number of partitions in the table
  *
  * Note:  This procedure does not provide error handling --- calling code must
  *         handle errors returned from this procedure
  *
  * Change History
  * Date     Programmer        Description
  * -------------------------------------------------------------------------
  * 30-OCT-2005  Brian Beckman        Initial Version
  ****************************************************************************
  */
  IS

    CURSOR cur_prttn IS
    SELECT column_name,column_position
    FROM user_part_key_columns
    WHERE name=p_vc2_tbl_name
    ORDER BY column_position;

  BEGIN

    -- Get the number of partitions in the table
    SELECT COUNT(1)
    INTO p_num_tbl_prttn_count
    FROM user_tab_partitions
    WHERE table_name=p_vc2_tbl_name;

    -- Construct the table partitioning key string
    FOR rec_prttn_key IN cur_prttn
    LOOP
      p_vc2_tbl_prttn_str := p_vc2_tbl_prttn_str||'-'||rec_prttn_key.column_position||'-'||rec_prttn_key.column_name;
    END LOOP;

    opt_pkg_util.pro_put_line(p_vc2_tbl_name||' has '||p_num_tbl_prttn_count||' partitions');
    opt_pkg_util.pro_put_line(p_vc2_tbl_name||' is partitioned like '||p_vc2_tbl_prttn_str);

  END pro_check_tbl_prttn;


  PROCEDURE pro_check_tbl_sub_prttn(p_vc2_tbl_name        IN  user_tables.table_name%TYPE,
                    p_vc2_tbl_sub_prttn_str    OUT VARCHAR2,
                                    p_num_tbl_sub_prttn_count      OUT NUMBER
                   )
  /*
  ***************************************************************************
  * Program : pro_check_tbl_sub_prttn
  * Version : 1.0
  * Author  : Brian Beckman
  * Date    : 30-OCT-2005
  * Purpose : Checks if a table is subpartitioned
  * Parameters :  p_vc2_tbl_name  -- Table to be checked for partitioning
  *          p_vc2_tbl_sub_prttn_str -- Subpartitioning key column string
  *
  * Note:  This procedure does not provide error handling --- calling code must
  *         handle errors returned from this procedure
  *
  * Change History
  * Date     Programmer        Description
  * -------------------------------------------------------------------------
  * 30-OCT-2005  Brian Beckman        Initial Version
  ****************************************************************************
  */
  IS

    CURSOR cur_prttn IS
    SELECT column_name,column_position
    FROM user_subpart_key_columns
    WHERE name=p_vc2_tbl_name
    ORDER BY column_position;

  BEGIN

    -- Get the count of the number of subpartitions in the table
    SELECT COUNT(1)
    INTO p_num_tbl_sub_prttn_count
    FROM user_tab_subpartitions
    WHERE table_name=p_vc2_tbl_name;

    IF (p_num_tbl_sub_prttn_count != 0)
    THEN
      -- Construct the subpartitioning key string
      FOR rec_prttn_key IN cur_prttn
      LOOP
    p_vc2_tbl_sub_prttn_str := p_vc2_tbl_sub_prttn_str||'-'||rec_prttn_key.column_position||'-'||rec_prttn_key.column_name;
      END LOOP;
    ELSE
      p_vc2_tbl_sub_prttn_str := NULL;
    END IF;

    opt_pkg_util.pro_put_line(p_vc2_tbl_name||' has '||p_num_tbl_sub_prttn_count||' subpartitions');
    opt_pkg_util.pro_put_line(p_vc2_tbl_name||' is subpartitioned like '||p_vc2_tbl_sub_prttn_str);

  END pro_check_tbl_sub_prttn;


  PROCEDURE pro_create_temp_tbl(p_vc2_tbl_name     IN  user_tables.table_name%TYPE,
                p_vc2_temp_tbl     OUT user_tables.table_name%TYPE,
				V_REGN IN VARCHAR2 DEFAULT NULL) --SOX Implementation project
                   
  /*
  ***************************************************************************
  * Program : pro_create_temp_tbl
  * Version : 1.0
  * Author  : Brian Beckman
  * Date    : 30-OCT-2005
  * Purpose : Creates a temp tbl with generic name
  * Parameters :  p_vc2_tbl_name  -- Table to be the basis for the temp table
  *          p_vc2_temp_tbl  -- Temp table created
  *
  * Note:  This procedure does not provide error handling --- calling code must
  *         handle errors returned from this procedure
  *
  * Change History
  * Date     Programmer        Description
  * -------------------------------------------------------------------------
  * 30-OCT-2005  Brian Beckman        Initial Version
  ****************************************************************************
  */
  IS
    v_num_temp_seq_num        NUMBER;
    v_num_tbl_count       NUMBER(1);
    v_num_compress_count      NUMBER(3);
    v_vc2_compress        VARCHAR2(10);
    v_vc2_sql_stmt        VARCHAR2(1000);

  BEGIN

        OPT_AX_TBLSPACE(V_REGN,V_TBLSPACE_NAME,V_INDEX); --SOX Project Implementation

    -- Get next sequence number
    SELECT OPT_PRTTN_UTIL_SEQ.NEXTVAL
    INTO v_num_temp_seq_num
    FROM dual;

    -- Build temp table name
    p_vc2_temp_tbl := 'ADW_PRTTN_TEMP_'||v_num_temp_seq_num;

    -- Check if table already exists
    opt_pkg_util.pro_check_tbl_exist(p_vc2_temp_tbl,v_num_tbl_count);
    IF (v_num_tbl_count != 0)
    THEN
      -- Table already exists and must be dropped
      EXECUTE IMMEDIATE 'DROP TABLE '||p_vc2_temp_tbl;
    END IF;

    -- Check if table is compressed
    opt_pkg_util.pro_check_tbl_compression(p_vc2_tbl_name,v_num_compress_count);
    IF (v_num_compress_count > 0)
    THEN
      v_vc2_compress := ' COMPRESS ';
    ELSE
      v_vc2_compress := ' ';
    END IF;

    -- Build CTAS statement
    v_vc2_sql_stmt := 'CREATE TABLE '||p_vc2_temp_tbl||' '||
              'NOLOGGING PCTFREE 0 '||v_vc2_compress||' '||
--                      'TABLESPACE OPTIMA01 '||
                      V_TBLSPACE_NAME||' '||
                      'AS '||
                      'SELECT * '||
                      'FROM '||p_vc2_tbl_name||' '||
                      'WHERE ROWNUM < 1';

    opt_pkg_util.pro_put_line('Creating temp table '||p_vc2_temp_tbl);
    opt_pkg_util.pro_put_line(v_vc2_sql_stmt);
	
	opt_pkg_util.pro_put_line('CREATE TEMP_TABLE V_REGN:'||V_REGN);

    EXECUTE IMMEDIATE v_vc2_sql_stmt;

    pro_create_temp_index(p_vc2_temp_tbl, p_vc2_tbl_name,NULL,V_REGN); --SOX Project Implementation

  END pro_create_temp_tbl;


  PROCEDURE pro_create_temp_index(p_vc2_temp_tbl    IN    user_tables.table_name%TYPE,
                  p_vc2_tbl_name    IN    user_tables.table_name%TYPE,
                  p_local_idx         varchar2 default NULL, -- added by Myra on 2009-03-12 for R8, for creating local indexes on table for enabling partition exchange.
                  V_REGN IN VARCHAR2 DEFAULT NULL) -- SOX Implementation Project
  /*
  ***************************************************************************
  * Program : pro_create_temp_index
  * Version : 1.0
  * Author  : Brian Beckman
  * Date    : 30-OCT-2005
  * Purpose : Creates indexes on a temp table
  * Parameters :  p_vc2_temp_tbl -- temp table
  *          p_vc2_tbl_name -- Source table
  *
  * Note:  This procedure does not provide error handling --- calling code must
  *         handle errors returned from this procedure
  *
  * Change History
  * Date     Programmer        Description
  * -------------------------------------------------------------------------
  * 30-OCT-2005  Brian Beckman        Initial Version
  ****************************************************************************
  */
  IS

    v_vc2_sql_stmt     VARCHAR2(4000);
    v_vc2_index_type     user_indexes.index_type%TYPE;
    v_nbr_seqval     NUMBER;
    v_vc2_unique     user_indexes.uniqueness%TYPE;
    v_local         varchar2(20);
    v_index_name     VARCHAR2(30);

    CURSOR cur_index IS
    SELECT index_name,
           index_type,
     partitioned,
           DECODE(uniqueness,'NONUNIQUE',NULL) AS uniqueness
    FROM user_indexes
    WHERE table_name=p_vc2_tbl_name;

  BEGIN
    
    OPT_AX_TBLSPACE(V_REGN,V_TBLSPACE_NAME,V_INDEX); --SOX Project Implementation
	
	opt_pkg_util.pro_put_line('VARIABLE_REGN'||V_REGN);
	opt_pkg_util.pro_put_line('VARIABLE INDEX'||V_INDEX);

    -- Loop thru all indexes on the table
    FOR rec_index IN cur_index
    LOOP

      -- Get a unique number for the index name
      SELECT OPT_PRTTN_UTIL_SEQ.NEXTVAL
      INTO v_nbr_seqval
      FROM dual;

    -- check index type, is local one?
     if p_local_idx = 'L' AND rec_index.partitioned='YES' then
    v_local := 'LOCAL';
     else v_local := null;
     end if;

     --Generate the Index Name
     v_index_name := SUBSTR(p_vc2_temp_tbl, 1, 23) ||'_'|| v_nbr_seqval;

      ---Generate initial SQL
      IF rec_index.index_type='NORMAL'
      THEN
        -- Index is either unique or b-tree
    v_vc2_sql_stmt := 'CREATE '||rec_index.uniqueness||' INDEX '||v_index_name||' '||
              'ON '||p_vc2_temp_tbl||' ('||CHR(10);
      ELSE
    -- Index is a bitmap index
    v_vc2_sql_stmt := 'CREATE '||rec_index.index_type||' INDEX '||v_index_name||' '||
              'ON '||p_vc2_temp_tbl||' ('||CHR(10);
      END IF;

      ---Add columns to SQL
      FOR rec_index_col IN (SELECT DECODE(column_position,1,' ',', ')||column_name column_name
                FROM user_ind_columns
                WHERE index_name = rec_index.index_name
                AND   table_name = p_vc2_tbl_name
                ORDER BY column_position
                           )
      LOOP
    v_vc2_sql_stmt := v_vc2_sql_stmt||rec_index_col.column_name||CHR(10);
      END LOOP;

      --- Modified by Myra on 2009-03-12 for R8, Add closing parenthesis and index info
      --v_vc2_sql_stmt := v_vc2_sql_stmt||') NOLOGGING TABLESPACE OPTIMA01 '||v_local;
      v_vc2_sql_stmt := v_vc2_sql_stmt||') NOLOGGING TABLESPACE '||V_INDEX||' '||v_local;

      opt_pkg_util.pro_put_line(v_vc2_sql_stmt);

      EXECUTE IMMEDIATE v_vc2_sql_stmt;

    END LOOP;

  END pro_create_temp_index;


  PROCEDURE pro_exchange_prttn(p_vc2_srce_tbl        IN  user_tables.table_name%TYPE,
                   p_vc2_dest_tbl        IN  user_tables.table_name%TYPE,
                   p_vc2_temp_tbl        IN  user_tables.table_name%TYPE,
                               p_vc2_min_cal_skid    IN  cal_mastr_dim.cal_mastr_skid%TYPE DEFAULT NULL,
                               p_vc2_region        IN  VARCHAR2 DEFAULT NULL,
                               p_vc2_prttn_type_code    IN  VARCHAR2
                  )
  /*
  ***************************************************************************
  * Program : pro_exchange_prttn
  * Version : 1.0
  * Author  : Brian Beckman
  * Date    : 30-OCT-2005
  * Purpose : Exchanges partitions between 2 partitioned tables
  * Parameters :  p_vc2_srce_tbl -- source table
  *          p_vc2_dest_tbl -- destination table
  *          p_vc2_temp_tbl -- temp table
  *          p_vc2_min_cal_skid -- the minimum cal_skid that will be exchanged
  *                       into the dest table
  *          p_vc2_region -- The region that will be subpartition exchangd
  *          p_vc2_prttn_type_code -- Promotion type possible values of M for monthly
  *                    and D for daily, Q for quarter, and F for fiscal year
  *
  * Note:  This procedure does not provide error handling --- calling code must
  *         handle errors returned from this procedure
  *
  * Change History
  * Date     Programmer        Description
  * -------------------------------------------------------------------------
  * 30-OCT-2005  Brian Beckman        Initial Version
  * 24-JAN-2005  Brian Beckman        Added truncate of temp table in each processing loop
  * 20-JUN-2006  Jenny Hays        Added acceptable prttn types of Q(quarter) and F(fiscal year)
  *                    and changed the use of the calendar table to CAL_MASTR_DIM
  * 21-JUN-2006  Brian Beckman        Modified day and month calc for v_vc2_date_str variable to not
  *                    subtract one day from the calculation and to use day_num and
  *                    mth_num columns.
  ****************************************************************************
  */
  IS
    v_vc2_tbl_sub_prttn_str    VARCHAR2(100);
    v_num_tbl_sub_prttn_count  NUMBER(3);
    v_vc2_date_str           VARCHAR2(8);
    v_num_srce_tbl_count       NUMBER(1);
    v_num_dest_tbl_count       NUMBER(1);
    v_vc2_max_prttn        user_tab_partitions.partition_name%TYPE;
  BEGIN
    -- Check if subpartitioned
    pro_check_tbl_sub_prttn(p_vc2_dest_tbl, v_vc2_tbl_sub_prttn_str,v_num_tbl_sub_prttn_count);

    IF (p_vc2_min_cal_skid IS NULL)
    THEN

      -- Use Jan 01 1999 as default
      IF (p_vc2_prttn_type_code = 'M' OR p_vc2_prttn_type_code = 'Q')
      THEN
    v_vc2_date_str:='199901';
      ELSIF (p_vc2_prttn_type_code = 'F')
      THEN
      v_vc2_date_str := '1999';
      ELSE  -- day partitioning
      v_vc2_date_str:='19990101';
      END IF;

    ELSE

      -- Get the date string that composes the partition name for the oldest partition we'll exchange
      SELECT DECODE(p_vc2_prttn_type_code,'D',day_num,
                          'M',mth_num,
                                          'Q',qtr_num,
                                          'F',fisc_yr_num
                   )
      INTO v_vc2_date_str
      FROM cal_mastr_dim
      WHERE cal_mastr_skid=p_vc2_min_cal_skid;

    END IF;

    -- Get maxvalue partition name as this should never be exchanged
    SELECT partition_name
    INTO v_vc2_max_prttn
    FROM user_tab_partitions
    WHERE table_name = p_vc2_srce_tbl
    AND   partition_position = (SELECT MAX(partition_position)
                FROM user_tab_partitions
                WHERE table_name = p_vc2_srce_tbl
                               );

    IF (v_num_tbl_sub_prttn_count = 0)
    THEN
      -- Table is partitioned
      -- Loop thru all partitions in the source table and exchange each one
      FOR rec IN (SELECT partition_name
          FROM user_tab_partitions
          WHERE table_name = p_vc2_srce_tbl
                  AND    partition_name >= 'P'||v_vc2_date_str
                  AND    partition_name != v_vc2_max_prttn
         )
      LOOP
        opt_pkg_util.pro_put_line('Exchanging partition '||rec.partition_name);
        pro_exchange_tbl_with_prttn(p_vc2_srce_tbl, rec.partition_name, p_vc2_temp_tbl,'N');
        BEGIN
          pro_exchange_tbl_with_prttn(p_vc2_dest_tbl, rec.partition_name, p_vc2_temp_tbl,'N');
        EXCEPTION
        WHEN OTHERS
        THEN
      -- If exchange between temp table and dest table fails, need to exchange back to source
          opt_pkg_util.pro_put_line(SQLERRM);
          opt_pkg_util.pro_put_line('ERROR!!!  Need to preserve data');
          opt_pkg_util.pro_put_line('Exchanging from '||p_vc2_temp_tbl||' back to '||p_vc2_srce_tbl||'.'||rec.partition_name);
          pro_exchange_tbl_with_prttn(p_vc2_srce_tbl, rec.partition_name, p_vc2_temp_tbl,'N');
          RAISE_APPLICATION_ERROR(-20080,'ERROR!!! Exchanging between '||p_vc2_temp_tbl||' and '||p_vc2_dest_tbl);
        END;
        -- Need to truncate temp table after exchange
        opt_pkg_util.pro_truncate_tbl(p_vc2_temp_tbl);
      END LOOP;
    ELSE
      -- Table is subpartitioned
      IF (p_vc2_region IS NOT NULL)
      THEN
    FOR rec IN (SELECT subpartition_name
            FROM user_tab_subpartitions
            WHERE table_name = p_vc2_srce_tbl
                AND   partition_name >= 'P'||v_vc2_date_str
                    AND   subpartition_name LIKE '%'||p_vc2_region
                    AND   partition_name != v_vc2_max_prttn
           )
    LOOP
          opt_pkg_util.pro_put_line('Exchanging subpartition '||rec.subpartition_name);
          pro_exchange_tbl_with_prttn(p_vc2_srce_tbl, rec.subpartition_name, p_vc2_temp_tbl,'Y');
          BEGIN
        pro_exchange_tbl_with_prttn(p_vc2_dest_tbl, rec.subpartition_name, p_vc2_temp_tbl,'Y');
      EXCEPTION
          WHEN OTHERS
          THEN
        -- If exchange between temp table and dest table fails, need to exchange back to source
            opt_pkg_util.pro_put_line(SQLERRM);
            opt_pkg_util.pro_put_line('ERROR!!!  Need to preserve data');
            opt_pkg_util.pro_put_line('Exchanging from '||p_vc2_temp_tbl||' back to '||p_vc2_srce_tbl);
            pro_exchange_tbl_with_prttn(p_vc2_srce_tbl, rec.subpartition_name, p_vc2_temp_tbl,'Y');
            RAISE_APPLICATION_ERROR(-20080,'ERROR!!! Exchanging between '||p_vc2_temp_tbl||' and '||p_vc2_dest_tbl);
          END;
          -- Need to truncate temp table after exchange
      opt_pkg_util.pro_truncate_tbl(p_vc2_temp_tbl);
    END LOOP;
      ELSE
        -- This means exchange all subpartitions regardless of region
        FOR rec IN (SELECT subpartition_name
            FROM user_tab_subpartitions
            WHERE table_name = p_vc2_srce_tbl
                AND   partition_name >= 'P'||v_vc2_date_str
                    AND   partition_name != v_vc2_max_prttn
           )
    LOOP
          opt_pkg_util.pro_put_line('Exchanging subpartition '||rec.subpartition_name);
          pro_exchange_tbl_with_prttn(p_vc2_srce_tbl, rec.subpartition_name, p_vc2_temp_tbl,'Y');
          BEGIN
        pro_exchange_tbl_with_prttn(p_vc2_dest_tbl, rec.subpartition_name, p_vc2_temp_tbl,'Y');
          EXCEPTION
          WHEN OTHERS
          THEN
        -- If exchange between temp table and dest table fails, need to exchange back to source
            opt_pkg_util.pro_put_line(SQLERRM);
            opt_pkg_util.pro_put_line('ERROR!!!  Need to preserve data');
            opt_pkg_util.pro_put_line('Exchanging from '||p_vc2_temp_tbl||' back to '||p_vc2_srce_tbl);
            pro_exchange_tbl_with_prttn(p_vc2_srce_tbl, rec.subpartition_name, p_vc2_temp_tbl,'Y');
            RAISE_APPLICATION_ERROR(-20080,'ERROR!!! Exchanging between '||p_vc2_temp_tbl||' and '||p_vc2_dest_tbl);
          END;
          -- Need to truncate temp table after exchange
      opt_pkg_util.pro_truncate_tbl(p_vc2_temp_tbl);
    END LOOP;
      END IF;
    END IF;

  END pro_exchange_prttn;



  PROCEDURE pro_add_latest_prttn(p_vc2_tbl_name      IN  user_tables.table_name%TYPE,
                 p_vc2_cal_skid      IN  cal_mastr_dim.cal_mastr_skid%TYPE,
                                 p_vc2_prttn_type_code   IN  VARCHAR2
                            )
  /*
  ***************************************************************************
  * Program : pro_add_latest_prttn
  * Version : 1.0
  * Author  : Brian Beckman
  * Date    : 30-OCT-2005
  * Purpose : Adds the latest partition to a table
  * Parameters :  p_vc2_tbl_name -- Table to have a partitioned added to it
  *          p_vc2_cal_skid -- CAL_SKID for the new partition to be created
  *          p_vc2_prttn_type_code -- Code representing the partitioning
  *                       grouping:
  *                        M for monthly
  *                        D for daily
  *                        Q for quarterly
  *                        F for fiscal year
  *
  * Note:  This procedure does not provide error handling --- calling code must
  *         handle errors returned from this procedure
  *
  * Change History
  * Date     Programmer        Description
  * -------------------------------------------------------------------------
  * 30-OCT-2005  Brian Beckman        Initial Version
  * 20-JUN-2006  Jenny Hays        Added handling for quarters and fiscal years
  *                    Changed calendar to CAL_MASTR_DIM
  * 21-JUN-2006  Brian Beckman        Corrected bug when determing v_vc2_cal_skid value
  *                    when p_vc2_cal_skid is NULL -- TRUNC by DD, not DAY
  *                    and also changed the mth, qtr and FY v_vc2_prttn_str and
  *                    v_vc2_cal_skid to be for the mth/QTR/FY of sysdate
  *                    and not sysdate - 1
  ****************************************************************************
  */
  IS
    v_vc2_prttn_str    VARCHAR2(10);
    v_vc2_cal_skid        cal_mastr_dim.cal_mastr_skid%TYPE;
    v_vc2_sql_stmt        VARCHAR2(500);
    v_vc2_max_prttn     user_tab_partitions.partition_name%TYPE;
    v_num_prttn_count   NUMBER(4);

  BEGIN

    IF (p_vc2_cal_skid IS NULL)
    THEN
      -- Use today's CAL_SKID for the default
      SELECT cal_mastr_skid
      INTO v_vc2_cal_skid
      FROM cal_mastr_dim
      WHERE day_date=TRUNC(SYSDATE,'DD');
    ELSE
      v_vc2_cal_skid:=p_vc2_cal_skid;
    END IF;

    IF (p_vc2_prttn_type_code='M')
    THEN
      -- Monthly partitioning buckets
      SELECT mth_skid+mth_days_in_mth_qty,TO_CHAR(mth_num)
      INTO v_vc2_cal_skid, v_vc2_prttn_str
      FROM cal_mastr_dim
      WHERE cal_mastr_skid=v_vc2_cal_skid;
    ELSIF (p_vc2_prttn_type_code='D')
    THEN
      -- Daily partitioning buckets
      SELECT cal_mastr_skid+1,TO_CHAR(day_num)
      INTO v_vc2_cal_skid, v_vc2_prttn_str
      FROM cal_mastr_dim
      WHERE cal_mastr_skid=v_vc2_cal_skid;
    ELSIF (p_vc2_prttn_type_code='Q')
    THEN
      -- Quarterly partitioning buckets
      SELECT qtr_skid+qtr_days_in_qtr_qty,TO_CHAR(qtr_num)
      INTO v_vc2_cal_skid, v_vc2_prttn_str
      FROM cal_mastr_dim
      WHERE cal_mastr_skid = v_vc2_cal_skid;
    ELSIF (p_vc2_prttn_type_code='F')
    THEN
      -- Fiscal year partitioning buckets
      SELECT fisc_yr_skid+fisc_yr_days_in_fisc_yr_qty,TO_CHAR(fisc_yr_num)
      INTO v_vc2_cal_skid, v_vc2_prttn_str
      FROM cal_mastr_dim
      WHERE cal_mastr_skid = v_vc2_cal_skid;
    ELSE
      RAISE_APPLICATION_ERROR(-20040,'ERROR!!! p_vc2_prttn_type_code value of '||p_vc2_prttn_type_code||' not valid');
    END IF;

    -- Check if the partition already exists
    SELECT COUNT(1)
    INTO v_num_prttn_count
    FROM user_tab_partitions
    WHERE table_name = p_vc2_tbl_name
    AND   partition_name = 'P'||v_vc2_prttn_str;

    IF (v_num_prttn_count = 1)
    THEN
      opt_pkg_util.pro_put_line('Partition P'||v_vc2_prttn_str||' already exists -- no new partitions added to '||p_vc2_tbl_name);
    ELSE
      -- Get the name of the maxvalue partition
      SELECT partition_name
      INTO v_vc2_max_prttn
      FROM user_tab_partitions
      WHERE table_name = p_vc2_tbl_name
      AND    partition_position = (SELECT MAX(partition_position)
                  FROM user_tab_partitions
                  WHERE table_name = p_vc2_tbl_name
                                   );

      v_vc2_sql_stmt := 'ALTER TABLE '||p_vc2_tbl_name||' '||
                'SPLIT PARTITION '||v_vc2_max_prttn||' '||
                          'AT ('||v_vc2_cal_skid||') INTO '||
                        '(PARTITION P'||v_vc2_prttn_str||', '||
                        ' PARTITION '||v_vc2_max_prttn||')';

      opt_pkg_util.pro_put_line(v_vc2_sql_stmt);

      EXECUTE IMMEDIATE v_vc2_sql_stmt;

    END IF;

  END pro_add_latest_prttn;


  PROCEDURE pro_cleanup_old_prttn(p_vc2_tbl_name      IN  user_tables.table_name%TYPE,
                  p_vc2_cal_skid      IN  cal_mastr_dim.cal_mastr_skid%TYPE DEFAULT NULL,
                                  p_vc2_prttn_type_code   IN  VARCHAR2
                 )
  /*
  ***************************************************************************
  * Program : pro_cleanup_old_prttn
  * Version : 1.0
  * Author  : Brian Beckman
  * Date    : 30-OCT-2005
  * Purpose : Drops old partitions from a table
  * Parameters :  p_vc2_tbl_name -- Table to have a partitioned added to it
  *          p_vc2_cal_skid -- oldest CAL_SKID to keep - if NULL, metadata will be used
  *                      to determine what to purge
  *          p_vc2_prttn_type_code -- Partitioning type code to indicate
  *                       partitioning buckets
  *                       M for monthly
  *                       D for daily
  *
  * Metadata table: PRTTN_TBL_DETL_PRC -- used to determine the number of retention months
  *
  * Note:  This procedure does not provide error handling --- calling code must
  *         handle errors returned from this procedure
  *
  * Change History
  * Date     Programmer        Description
  * -------------------------------------------------------------------------
  * 30-OCT-2005  Brian Beckman        Initial Version
  * 20-JUN-2006  Jenny Hays        Changed CAL_DIM to CAL_MASTR_DIM
  *                    Added handling for quarters and fiscal years
  ****************************************************************************
  */
  IS
    v_num_retn_perd         NUMBER(4);
    v_vc2_cal_skid         cal_mastr_dim.cal_mastr_skid%TYPE;
    v_vc2_prttn_str      VARCHAR2(10);
    v_vc2_sql_stmt         VARCHAR2(4000);
  BEGIN
    IF (p_vc2_cal_skid IS NULL)
    THEN
      -- Get the number of days to retain data for
      SELECT COUNT(1)
      INTO v_num_retn_perd
      FROM PRTTN_TBL_DETL_PRC
      WHERE tbl_name = p_vc2_tbl_name;

      IF (v_num_retn_perd IS NULL)
      THEN
    opt_pkg_util.pro_put_line('Unable to determine retention period for '||p_vc2_tbl_name);
        RAISE_APPLICATION_ERROR(-20050,'PRTTN_TBL_DETL_PRC is not setup for '||p_vc2_tbl_name);
      END IF;

      SELECT cal_retn_perd
      INTO v_num_retn_perd
      FROM PRTTN_TBL_DETL_PRC
      WHERE tbl_name = p_vc2_tbl_name;

      opt_pkg_util.pro_put_line('Retaining only '||v_num_retn_perd||' periods.');

      -- Use today's CAL_SKID for the default
      IF (p_vc2_prttn_type_code = 'M')
      THEN
        -- Perform a monthly conversion
    SELECT mth_skid
        INTO v_vc2_cal_skid
        FROM cal_mastr_dim
        WHERE day_date=TO_CHAR(ADD_MONTHS(SYSDATE,-1 * v_num_retn_perd),'DD-MON-YYYY');
      ELSIF (p_vc2_prttn_type_code = 'D')
      THEN
        -- Perform a daily conversion
        SELECT cal_mastr_skid
        INTO v_vc2_cal_skid
        FROM cal_mastr_dim
        WHERE day_date=TO_CHAR(SYSDATE - v_num_retn_perd,'DD-MON-YYYY');
      ELSIF (p_vc2_prttn_type_code = 'Q')
      THEN
        -- Perform a quarterly conversion
        SELECT qtr_skid
        INTO v_vc2_cal_skid
        FROM cal_mastr_dim
        WHERE day_date=TO_CHAR(ADD_MONTHS(LAST_DAY(SYSDATE),-3 * v_num_retn_perd ),'DD-MON-YYYY');
      ELSIF (p_vc2_prttn_type_code = 'F')
      THEN
        -- Perform a fiscal year conversion
        SELECT fisc_yr_skid
        INTO v_vc2_cal_skid
        FROM cal_mastr_dim
        WHERE day_date=TO_CHAR(ADD_MONTHS(LAST_DAY(SYSDATE),-12 * v_num_retn_perd ),'DD-MON-YYYY');
      ELSE
        RAISE_APPLICATION_ERROR(-20051,'ERROR!!! p_vc2_prttn_type_code value of '||p_vc2_prttn_type_code||' not valid');
      END IF;
    ELSE
      v_vc2_cal_skid:=p_vc2_cal_skid;
    END IF;

    -- Convert the minimum retention SKID to a string
    IF (p_vc2_prttn_type_code='M')
    THEN
      -- Monthly partitioning buckets
      -- Need DISTINCT as multiple rows in CAL_DIM exists for this MTH_SKID
      SELECT DISTINCT 'P'||TO_CHAR(mth_end_date,'YYYYMM')
      INTO v_vc2_prttn_str
      FROM cal_mastr_dim
      WHERE mth_skid=v_vc2_cal_skid;
    ELSIF (p_vc2_prttn_type_code='D')
    THEN
      -- Daily partitioning buckets
      SELECT 'P'||TO_CHAR(day_date,'YYYYMMDD')
      INTO v_vc2_prttn_str
      FROM cal_mastr_dim
      WHERE cal_mastr_skid=v_vc2_cal_skid;
    ELSIF (p_vc2_prttn_type_code='Q')
    THEN
      -- Quarterly partitioning buckets
      SELECT DISTINCT 'P'||qtr_num
      INTO v_vc2_prttn_str
      FROM cal_mastr_dim
      WHERE qtr_skid=v_vc2_cal_skid;
    ELSIF (p_vc2_prttn_type_code='F')
    THEN
      -- Fiscal year partitioning buckets
      SELECT DISTINCT 'P'||fisc_yr_num
      INTO v_vc2_prttn_str
      FROM cal_mastr_dim
      WHERE fisc_yr_skid=v_vc2_cal_skid;
    ELSE
      RAISE_APPLICATION_ERROR(-20052,'ERROR!!! p_vc2_prttn_type_code value of '||p_vc2_prttn_type_code||' not valid');
    END IF;

    opt_pkg_util.pro_put_line (v_vc2_prttn_str);

    -- Loop thru all the partitions and drop those which are too old
    FOR rec_prttn IN (SELECT partition_name
              FROM user_tab_partitions
                      WHERE table_name = p_vc2_tbl_name
                      AND    partition_name <= v_vc2_prttn_str
                     )
    LOOP
      v_vc2_sql_stmt := 'ALTER TABLE '||p_vc2_tbl_name||' '||
                'DROP PARTITION '||rec_prttn.partition_name;

      opt_pkg_util.pro_put_line(v_vc2_sql_stmt);

      EXECUTE IMMEDIATE v_vc2_sql_stmt;

    END LOOP;

  END pro_cleanup_old_prttn;

  PROCEDURE pro_initialize_qtr_prttn (p_vc2_tbl_name         IN  user_tables.table_name%TYPE,
                      p_num_past_qtr_num     IN  NUMBER,
                      p_num_future_qtr_num     IN  NUMBER,
                                      p_vc2_drop_prttn_flag  IN  VARCHAR2
                     )
   /*
   ***************************************************************************
   * Program : pro_initialize_qtr_prttn
   * Version : 1.0
   * Author  : Jenny Hays
   * Date    : 20-June-2006
   * Purpose : Fully rebuilds the partitioning of a table based on a quarter
   *
   *
   *
   * Note:  This procedure does not provide error handling --- calling code must
   *          handle errors returned from this procedure
   *
   *        This procedure also uses CAL_MASTR_DIM instead of CAL_DIM
   *
   * Change History
   * Date      Programmer         Description
   * -------------------------------------------------------------------------
   * 20-JUN-2006  Jenny Hays     Initial Version
   ****************************************************************************
   */
   IS
      v_vc2_max_prttn           user_tab_partitions.partition_name%TYPE;
      v_vc2_sqlstmt           VARCHAR2(500);
      v_vc2_sqlerrm           VARCHAR2(512);
      v_num_sqlcode           NUMBER;
      v_num_tbl_count       NUMBER;
      v_vc2_tbl_name       user_tables.table_name%TYPE := UPPER(p_vc2_tbl_name);
      v_vc2_drop_prttn_flag    VARCHAR2(1) := UPPER(p_vc2_drop_prttn_flag);

      CURSOR prttn_cur_typ IS
      SELECT partition_name
      FROM user_tab_partitions
      WHERE table_name = v_vc2_tbl_name
      AND    partition_position != (SELECT MAX(partition_position)
                   FROM user_tab_partitions
                   WHERE table_name = v_vc2_tbl_name
                                  );

   BEGIN

     opt_pkg_util.pro_check_tbl_exist(v_vc2_tbl_name, v_num_tbl_count);
     IF (v_num_tbl_count = 0)
     THEN
       RAISE_APPLICATION_ERROR(-20060,'ERROR!!! '||v_vc2_tbl_name||' does not exist');
     ELSE
       opt_pkg_util.pro_put_line('Table '||v_vc2_tbl_name||' exists');
     END IF;

     IF (v_vc2_drop_prttn_flag='Y')
     THEN
       --First drop all old partitions if they exist
       opt_pkg_util.pro_put_line('Preparing to drop partitions from '||v_vc2_tbl_name);
       FOR prttn_rec_typ IN prttn_cur_typ
       LOOP
     v_vc2_sqlstmt:='ALTER TABLE '||v_vc2_tbl_name||' '||CHR(10)||
                'DROP PARTITION '||prttn_rec_typ.partition_name;

     opt_pkg_util.pro_put_line('Executing: '||v_vc2_sqlstmt);
     EXECUTE IMMEDIATE v_vc2_sqlstmt;
       END LOOP;
     ELSE
       opt_pkg_util.pro_put_line('Not dropping existing partitions from '||v_vc2_tbl_name);
     END IF;

     --Get maxvalue partition name
     SELECT partition_name
     INTO v_vc2_max_prttn
     FROM user_tab_partitions
     WHERE table_name = v_vc2_tbl_name
     AND   partition_position = (SELECT MAX(partition_position)
                 FROM user_tab_partitions
                 WHERE table_name = v_vc2_tbl_name
                                );

     --Now loop thru all monthly SKIDs and create the partitions
     FOR qtr_rec_typ IN (SELECT DISTINCT qtr_skid+qtr_days_in_qtr_qty AS qtr_skid,
                    qtr_num AS qtr_name
             FROM cal_mastr_dim
             WHERE day_date BETWEEN ADD_MONTHS(TRUNC(SYSDATE,'MONTH'),(-3*p_num_past_qtr_num))
                    AND    ADD_MONTHS(TRUNC(SYSDATE,'MONTH'),(p_num_future_qtr_num*3))
                         AND NOT EXISTS (SELECT 1
                                 FROM user_tab_partitions
                                         WHERE partition_name = 'P'||qtr_name
                                         AND   table_name = v_vc2_tbl_name
                                        )
             ORDER BY qtr_skid ASC
                         )
     LOOP
       v_vc2_sqlstmt := 'ALTER TABLE '||v_vc2_tbl_name||' '||CHR(10)||
                'SPLIT PARTITION '||v_vc2_max_prttn||' '||
                'AT ('||TO_CHAR(qtr_rec_typ.qtr_skid)||') '||CHR(10)||
                        'INTO (PARTITION P'||qtr_rec_typ.qtr_name||', '||CHR(10)||
                        '      PARTITION '||v_vc2_max_prttn||CHR(10)||'     )';

       opt_pkg_util.pro_put_line('Creating partition P'||qtr_rec_typ.qtr_name);
       opt_pkg_util.pro_put_line(v_vc2_sqlstmt);

       EXECUTE IMMEDIATE v_vc2_sqlstmt;

     END LOOP;

   END pro_initialize_qtr_prttn;

   PROCEDURE pro_initialize_fy_prttn (p_vc2_tbl_name         IN  user_tables.table_name%TYPE,
                      p_num_past_fy_num    IN  NUMBER,
                      p_num_future_fy_num    IN  NUMBER,
                                      p_vc2_drop_prttn_flag  IN  VARCHAR2
                     )
   /*
   ***************************************************************************
   * Program : pro_initialize_fy_prttn
   * Version : 1.0
   * Author  : Jenny Hays
   * Date    : 20-June-2006
   * Purpose : Fully rebuilds the partitioning of a table based on a fiscal year
   *
   *
   *
   * Note:  This procedure does not provide error handling --- calling code must
   *          handle errors returned from this procedure
   *
   *        This procedure also uses CAL_MASTR_DIM instead of CAL_DIM
   *
   * Change History
   * Date      Programmer         Description
   * -------------------------------------------------------------------------
   * 20-JUN-2006  Jenny Hays     Initial Version
   ****************************************************************************
   */
   IS
      v_vc2_max_prttn           user_tab_partitions.partition_name%TYPE;
      v_vc2_sqlstmt           VARCHAR2(500);
      v_vc2_sqlerrm           VARCHAR2(512);
      v_num_sqlcode           NUMBER;
      v_num_tbl_count       NUMBER;
      v_vc2_tbl_name       user_tables.table_name%TYPE := UPPER(p_vc2_tbl_name);
      v_vc2_drop_prttn_flag    VARCHAR2(1) := UPPER(p_vc2_drop_prttn_flag);

      CURSOR prttn_cur_typ IS
      SELECT partition_name
      FROM user_tab_partitions
      WHERE table_name = v_vc2_tbl_name
      AND    partition_position != (SELECT MAX(partition_position)
                   FROM user_tab_partitions
                   WHERE table_name = v_vc2_tbl_name
                                  );

   BEGIN

     opt_pkg_util.pro_check_tbl_exist(v_vc2_tbl_name, v_num_tbl_count);
     IF (v_num_tbl_count = 0)
     THEN
       RAISE_APPLICATION_ERROR(-20060,'ERROR!!! '||v_vc2_tbl_name||' does not exist');
     ELSE
       opt_pkg_util.pro_put_line('Table '||v_vc2_tbl_name||' exists');
     END IF;

     IF (v_vc2_drop_prttn_flag='Y')
     THEN
       --First drop all old partitions if they exist
       opt_pkg_util.pro_put_line('Preparing to drop partitions from '||v_vc2_tbl_name);
       FOR prttn_rec_typ IN prttn_cur_typ
       LOOP
     v_vc2_sqlstmt:='ALTER TABLE '||v_vc2_tbl_name||' '||CHR(10)||
                'DROP PARTITION '||prttn_rec_typ.partition_name;

     opt_pkg_util.pro_put_line('Executing: '||v_vc2_sqlstmt);
     EXECUTE IMMEDIATE v_vc2_sqlstmt;
       END LOOP;
     ELSE
       opt_pkg_util.pro_put_line('Not dropping existing partitions from '||v_vc2_tbl_name);
     END IF;

     --Get maxvalue partition name
     SELECT partition_name
     INTO v_vc2_max_prttn
     FROM user_tab_partitions
     WHERE table_name = v_vc2_tbl_name
     AND   partition_position = (SELECT MAX(partition_position)
                 FROM user_tab_partitions
                 WHERE table_name = v_vc2_tbl_name
                                );

     --Now loop thru all monthly SKIDs and create the partitions
     FOR fy_rec_typ IN (SELECT DISTINCT fisc_yr_skid+fisc_yr_days_in_fisc_yr_qty AS fisc_yr_skid,
                    fisc_yr_num AS fisc_yr_name
             FROM cal_mastr_dim
             WHERE day_date BETWEEN ADD_MONTHS(TRUNC(SYSDATE, 'MONTH'),-12 * p_num_past_fy_num)
                    AND ADD_MONTHS(TRUNC(SYSDATE,'MONTH'),p_num_future_fy_num*12)
                         AND NOT EXISTS (SELECT 1
                                 FROM user_tab_partitions
                                         WHERE partition_name = 'P'||fisc_yr_num
                                         AND   table_name = v_vc2_tbl_name
                                        )
             ORDER BY fisc_yr_skid ASC
                         )
     LOOP
       v_vc2_sqlstmt := 'ALTER TABLE '||v_vc2_tbl_name||' '||CHR(10)||
                'SPLIT PARTITION '||v_vc2_max_prttn||' '||
                'AT ('||TO_CHAR(fy_rec_typ.fisc_yr_skid)||') '||CHR(10)||
                        'INTO (PARTITION P'||fy_rec_typ.fisc_yr_name||', '||CHR(10)||
                        '      PARTITION '||v_vc2_max_prttn||CHR(10)||'     )';

       opt_pkg_util.pro_put_line('Creating partition P'||fy_rec_typ.fisc_yr_name);
       opt_pkg_util.pro_put_line(v_vc2_sqlstmt);

       EXECUTE IMMEDIATE v_vc2_sqlstmt;

     END LOOP;

   END pro_initialize_fy_prttn;

  PROCEDURE pro_verify_fy_window_prttn (p_vc2_dest_tbl_name IN    user_tables.table_name%TYPE,
                    p_vc2_srce_tbl_name IN    user_tables.table_name%TYPE default null
  )
   /*
   ***************************************************************************
   * Program : pro_verify_fy_window_prttn
   * Version : 1.0
   * Author  : Daniel.Qin
   * Date    : 4-July-2011
   * Purpose : Check dest table and srce table partition lists
   *           whether match with metadata table OPT_FY_PRTTN_PRC.
   *           Partitions past due will be truncate and merge into the oldest partition
   *           permitted. And partitions will be split if new partitions needed.
   * Note:  This procedure does not provide error handling --- calling code must
   *          handle errors returned from this procedure
   *
   * Change History
   * Date      Programmer         Description
   * -------------------------------------------------------------------------
   * 4-JULY-2011  Daniel.Qin         Initial Version
   ****************************************************************************
   */
   IS
      v_vc2_max_prttn           user_tab_partitions.partition_name%TYPE;
      v_vc2_min_prttn           user_tab_partitions.partition_name%TYPE;
      v_vc2_sqlstmt           VARCHAR2(500);
      --v_vc2_sqlerrm         VARCHAR2(512);
      v_vc2_merge_to_prn_1     VARCHAR2(20) ;

      v_num_past_fy_cnt        NUMBER;
      v_num_futr_fy_cnt        NUMBER;
      v_num_cnt            NUMBER;
      v_num_subprttn_cnt       NUMBER;
      v_num_prttn_cnt           NUMBER;
      v_num_sqlcode           NUMBER;
      v_num_curr_fy_num        NUMBER;
      s_cnt               NUMBER;

      v_vc2_step_info           VARCHAR2(100);
      v_vc2_dest_tbl_name      user_tables.table_name%TYPE := UPPER(p_vc2_dest_tbl_name);
      v_vc2_srce_tbl_name      user_tables.table_name%TYPE := UPPER(p_vc2_srce_tbl_name);

   BEGIN

     v_vc2_step_info := 'PROC:PRO_VERIFY_FY_WINDOW_PRTTN BEGIN.';
     opt_pkg_util.pro_put_line(v_vc2_step_info);
     v_vc2_step_info := 'STEP 10: initial variables.';
     opt_pkg_util.pro_put_line(v_vc2_step_info);

     IF v_vc2_dest_tbl_name IS NULL
     THEN
       RAISE_APPLICATION_ERROR(-20061,'Parameter ERROR!!! p_vc2_dest_tbl_name is null !');
     END IF;

     --STEP10 Retrieve v_past_fy_cnt,v_futr_fy_cnt from metadata table
     SELECT MAX(PAST_FY_CNT),MAX(FUTR_FY_CNT),COUNT(1)
       INTO v_num_past_fy_cnt,v_num_futr_fy_cnt,v_num_cnt
       FROM OPT_FY_PRTTN_PRC
      WHERE TBL_NAME  = v_vc2_dest_tbl_name;

     --STEP10 check if table exists subpartitions
     SELECT COUNT(1) INTO v_num_subprttn_cnt
       FROM USER_TAB_SUBPARTITIONS
      WHERE TABLE_NAME=v_vc2_dest_tbl_name;

     IF (v_num_cnt = 0)
     THEN
       RAISE_APPLICATION_ERROR(-20061,'ERROR!!! '||v_vc2_dest_tbl_name
                   ||' does not defined in metadata table OPT_FY_PRTTN_PRC!');
     ELSIF (v_num_subprttn_cnt = 0 )
     THEN
       RAISE_APPLICATION_ERROR(-20061,'ERROR!!! '||v_vc2_dest_tbl_name
                   ||' is not a table with subpartitions!');
     ELSE
       --STEP 10: get current fy num
       SELECT FISC_YR_NUM
     INTO v_num_curr_fy_num
     FROM CAL_MASTR_DIM
    WHERE DAY_DATE = TRUNC(SYSDATE,'DD');

       SELECT PARTITION_NAME
     INTO v_vc2_max_prttn
     FROM USER_TAB_PARTITIONS
    WHERE TABLE_NAME = v_vc2_dest_tbl_name
      AND PARTITION_POSITION = (SELECT MAX(PARTITION_POSITION)
                      FROM USER_TAB_PARTITIONS
                     WHERE TABLE_NAME = v_vc2_dest_tbl_name
                    );

       SELECT PARTITION_NAME
     INTO v_vc2_min_prttn
     FROM USER_TAB_PARTITIONS
    WHERE TABLE_NAME = v_vc2_dest_tbl_name
      AND PARTITION_POSITION = (SELECT MIN(PARTITION_POSITION)
                      FROM USER_TAB_PARTITIONS
                     WHERE TABLE_NAME = v_vc2_dest_tbl_name
                    );

       --STEP 10: check srce table and tgt table if have the same partitions
       v_vc2_step_info := 'STEP 10: check srce table and target table partitions.';
       opt_pkg_util.pro_put_line(v_vc2_step_info);
       SELECT COUNT(PARTITION_NAME)
     INTO v_num_prttn_cnt
     FROM USER_TAB_PARTITIONS
    WHERE TABLE_NAME = v_vc2_srce_tbl_name
      AND PARTITION_NAME NOT IN (SELECT PARTITION_NAME
                       FROM USER_TAB_PARTITIONS
                      WHERE TABLE_NAME = v_vc2_dest_tbl_name);
       IF v_num_prttn_cnt > 0
       THEN
     ---Modified By Rajesh S for PM0002204 Start

     opt_pkg_util.pro_put_line('Error: partitions do not match between source and target table!');

     opt_pkg_util.pro_put_line('Trying to Recreate the Missing Partition in Target Table');

     OPT_PKG_PRTTN_UTIL.pro_initialize_fy_prttn(v_vc2_dest_tbl_name,3,2,'N');

     SELECT COUNT(PARTITION_NAME)
     INTO v_num_prttn_cnt
     FROM USER_TAB_PARTITIONS
    WHERE TABLE_NAME = v_vc2_srce_tbl_name
      AND PARTITION_NAME NOT IN (SELECT PARTITION_NAME
                       FROM USER_TAB_PARTITIONS
                      WHERE TABLE_NAME = v_vc2_dest_tbl_name);
       IF v_num_prttn_cnt > 0 THEN
          opt_pkg_util.pro_put_line('Error: partitions do not match between source and target table!');
          raise_application_error(-20061,'Error: partitions do not match!');
       ELSE
          opt_pkg_util.pro_put_line('Target Table Partition Created Sucessfully!');
       END IF;
        ---Modified By Rajesh S for PM0002204 End
       END IF;

       --STEP 20: check if there are some partitions need to split
       v_vc2_step_info := 'STEP 20: check partitions need to split begin.';
       opt_pkg_util.pro_put_line(v_vc2_step_info);
       FOR cur_add_prttn IN (SELECT DISTINCT FISC_YR_SKID+FISC_YR_DAYS_IN_FISC_YR_QTY AS FISC_YR_SKID,
                    FISC_YR_NUM AS FISC_YR_NAME
                   FROM CAL_MASTR_DIM
                  WHERE DAY_DATE BETWEEN ADD_MONTHS(TRUNC(SYSDATE, 'MONTH'),-12 * v_num_past_fy_cnt)
                AND ADD_MONTHS(TRUNC(SYSDATE,'MONTH'),v_num_futr_fy_cnt*12)
                AND NOT EXISTS (SELECT 1
                          FROM USER_TAB_PARTITIONS
                         WHERE PARTITION_NAME = 'P'||FISC_YR_NUM
                           AND TABLE_NAME = v_vc2_dest_tbl_name)
                  ORDER BY ABS(FISC_YR_NUM-v_num_curr_fy_num) ASC
                 )
       LOOP
     --create previous fy partition
     IF (cur_add_prttn.FISC_YR_NAME < v_num_curr_fy_num)
     THEN
       v_vc2_sqlstmt := 'ALTER TABLE '||v_vc2_dest_tbl_name||' '||CHR(10)||
                'SPLIT PARTITION '||v_vc2_min_prttn||' '||
                'AT ('||TO_CHAR(cur_add_prttn.FISC_YR_SKID)||') '||CHR(10)||
                   'INTO (PARTITION P'||cur_add_prttn.FISC_YR_NAME||', '||CHR(10)||
                   'PARTITION '||v_vc2_min_prttn||CHR(10)||')';

       v_vc2_step_info := 'STEP 20: Creating partition P'||cur_add_prttn.FISC_YR_NAME||
                  ' For table '||v_vc2_dest_tbl_name;
       opt_pkg_util.pro_put_line(v_vc2_step_info);
       opt_pkg_util.pro_put_line(v_vc2_sqlstmt);
       EXECUTE IMMEDIATE v_vc2_sqlstmt;

       IF v_vc2_srce_tbl_name IS NOT NULL
       THEN
         v_vc2_sqlstmt := 'ALTER TABLE '||v_vc2_srce_tbl_name||' '||CHR(10)||
                  'SPLIT PARTITION '||v_vc2_min_prttn||' '||
                  'AT ('||TO_CHAR(cur_add_prttn.FISC_YR_SKID)||') '||CHR(10)||
                 'INTO (PARTITION P'||cur_add_prttn.FISC_YR_NAME||', '||CHR(10)||
                 'PARTITION '||v_vc2_min_prttn||CHR(10)||')';

         v_vc2_step_info := 'STEP 20: Creating partition P'||cur_add_prttn.FISC_YR_NAME||
                ' For table '||v_vc2_srce_tbl_name;
         opt_pkg_util.pro_put_line(v_vc2_step_info);
         opt_pkg_util.pro_put_line(v_vc2_sqlstmt);
         EXECUTE IMMEDIATE v_vc2_sqlstmt;
       END IF;
       v_vc2_min_prttn := 'P'||cur_add_prttn.fisc_yr_name;

     ELSE
       v_vc2_sqlstmt := 'ALTER TABLE '||v_vc2_dest_tbl_name||' '||CHR(10)||
                'SPLIT PARTITION '||v_vc2_max_prttn||' '||
                'AT ('||TO_CHAR(cur_add_prttn.FISC_YR_SKID)||') '||CHR(10)||
                'INTO (PARTITION P'||cur_add_prttn.FISC_YR_NAME||', '||CHR(10)||
                'PARTITION '||v_vc2_max_prttn||CHR(10)||')';

       v_vc2_step_info := 'STEP 20: Creating partition P'||cur_add_prttn.FISC_YR_NAME||
                  ' For table '||v_vc2_dest_tbl_name;
       opt_pkg_util.pro_put_line(v_vc2_step_info);
       opt_pkg_util.pro_put_line(v_vc2_sqlstmt);
       EXECUTE IMMEDIATE v_vc2_sqlstmt;

       IF v_vc2_srce_tbl_name IS NOT NULL
       THEN
         v_vc2_sqlstmt := 'ALTER TABLE '||v_vc2_srce_tbl_name||' '||CHR(10)||
                  'SPLIT PARTITION '||v_vc2_max_prttn||' '||
                  'AT ('||TO_CHAR(cur_add_prttn.fisc_yr_skid)||') '||CHR(10)||
                  'INTO (PARTITION P'||cur_add_prttn.fisc_yr_name||', '||CHR(10)||
                  'PARTITION '||v_vc2_max_prttn||CHR(10)||')';

         v_vc2_step_info := 'STEP 20: Creating partition P'||cur_add_prttn.FISC_YR_NAME||
                ' For table '||v_vc2_srce_tbl_name;
         opt_pkg_util.pro_put_line(v_vc2_step_info);
         opt_pkg_util.pro_put_line(v_vc2_sqlstmt);
         EXECUTE IMMEDIATE v_vc2_sqlstmt;
       END IF;
     END IF;
       END LOOP;
       v_vc2_step_info := 'STEP 20: check partitions need to split end ok.';
       opt_pkg_util.pro_put_line(v_vc2_step_info);

       --STEP 30: check if there are some partitions need to merge
       opt_pkg_util.pro_put_line('STEP 30: check partitions need to merge begin.');
       FOR cur_drop_prttn IN (SELECT PARTITION_NAME,PARTITION_POSITION
                FROM USER_TAB_PARTITIONS
                   WHERE TABLE_NAME = v_vc2_dest_tbl_name
                 AND NOT EXISTS (SELECT 1
                           FROM CAL_MASTR_DIM
                          WHERE DAY_DATE BETWEEN ADD_MONTHS(TRUNC(SYSDATE, 'MONTH'),-12 * v_num_past_fy_cnt)
                                     AND ADD_MONTHS(TRUNC(SYSDATE,'MONTH'),v_num_futr_fy_cnt*12)
                            AND 'P'||FISC_YR_NUM = PARTITION_NAME
                         )
                 AND PARTITION_POSITION != (SELECT MAX(PARTITION_POSITION)
                                  FROM USER_TAB_PARTITIONS
                                 WHERE TABLE_NAME = v_vc2_dest_tbl_name
                                )
                   ORDER BY ABS(TO_NUMBER(SUBSTR(PARTITION_NAME,2))- v_num_curr_fy_num) DESC
                 )
       LOOP
     IF TO_NUMBER(SUBSTR(cur_drop_prttn.PARTITION_NAME,2)) < v_num_curr_fy_num
     THEN

       --truncate partition which out the range of the fy window
       v_vc2_sqlstmt := 'ALTER TABLE '||v_vc2_dest_tbl_name||' TRUNCATE PARTITION '||
                cur_drop_prttn.PARTITION_NAME||' UPDATE INDEXES';
       EXECUTE IMMEDIATE v_vc2_sqlstmt;
       opt_pkg_util.pro_put_line('truncate partition <<'||v_vc2_sqlstmt||'>> ok!');

       SELECT PARTITION_NAME
         INTO v_vc2_merge_to_prn_1
         FROM USER_TAB_PARTITIONS
        WHERE TABLE_NAME = v_vc2_dest_tbl_name
          AND PARTITION_POSITION = (SELECT PARTITION_POSITION+1
                      FROM USER_TAB_PARTITIONS
                     WHERE PARTITION_NAME = cur_drop_prttn.PARTITION_NAME
                       AND TABLE_NAME = v_vc2_dest_tbl_name);

       v_vc2_sqlstmt := 'ALTER TABLE '||v_vc2_dest_tbl_name||' MERGE PARTITIONS '
              ||cur_drop_prttn.PARTITION_NAME||','||v_vc2_merge_to_prn_1
              ||' INTO PARTITION '|| v_vc2_merge_to_prn_1 ||' UPDATE INDEXES';
       EXECUTE IMMEDIATE v_vc2_sqlstmt;
       opt_pkg_util.pro_put_line('execute partition merge sql <<'||v_vc2_sqlstmt||'>> ok!');

       IF v_vc2_srce_tbl_name IS NOT NULL
       THEN
         --check srce table exists partition cur_drop_prttn.PARTITION_NAME
         SELECT COUNT(1) INTO S_CNT
           FROM USER_TAB_PARTITIONS
          WHERE PARTITION_NAME IN (cur_drop_prttn.PARTITION_NAME)
            AND TABLE_NAME = v_vc2_srce_tbl_name;

         --BIP-W3-1,August-24-2011,Daniel.Qin,add a segment of IF clause, Begin
         IF (S_CNT > 0)
         THEN
           v_vc2_sqlstmt := 'ALTER TABLE '||v_vc2_srce_tbl_name||' TRUNCATE PARTITION '||
                  cur_drop_prttn.PARTITION_NAME||' UPDATE INDEXES';
           EXECUTE IMMEDIATE v_vc2_sqlstmt;
           opt_pkg_util.pro_put_line('truncate partition <<'||v_vc2_sqlstmt||'>> ok!');

           v_vc2_sqlstmt := 'ALTER TABLE '||v_vc2_srce_tbl_name||' MERGE PARTITIONS '
                ||cur_drop_prttn.PARTITION_NAME||','||v_vc2_merge_to_prn_1
                ||' INTO PARTITION '|| v_vc2_merge_to_prn_1 ||' UPDATE INDEXES';
           EXECUTE IMMEDIATE v_vc2_sqlstmt;
           opt_pkg_util.pro_put_line('execute partition merge sql <<'||v_vc2_sqlstmt||'>> ok!');
         END IF;
          --BIP-W3-1,August-24-2011,Daniel.Qin,add a segment of IF clause, End
       END IF;
     ELSE
       v_vc2_sqlstmt := 'ALTER TABLE '||v_vc2_dest_tbl_name||' MERGE PARTITIONS '
             ||cur_drop_prttn.PARTITION_NAME||','||v_vc2_max_prttn
             ||' INTO PARTITION '|| v_vc2_max_prttn ||' UPDATE INDEXES';
       EXECUTE IMMEDIATE v_vc2_sqlstmt;
       opt_pkg_util.pro_put_line('execute partition merge sql <<'||v_vc2_sqlstmt||'>> ok!');

       IF v_vc2_srce_tbl_name IS NOT NULL
       THEN

         --check srce table exists partition cur_drop_prttn.PARTITION_NAME
         SELECT COUNT(1) INTO S_CNT
           FROM USER_TAB_PARTITIONS
          WHERE PARTITION_NAME IN (cur_drop_prttn.PARTITION_NAME)
            AND TABLE_NAME = v_vc2_srce_tbl_name;
         IF S_CNT>0 THEN
           v_vc2_sqlstmt := 'ALTER TABLE '||v_vc2_srce_tbl_name||' MERGE PARTITIONS '
                ||cur_drop_prttn.PARTITION_NAME||','||v_vc2_max_prttn
                ||' INTO PARTITION '|| v_vc2_max_prttn ||' UPDATE INDEXES';
           EXECUTE IMMEDIATE v_vc2_sqlstmt;
           opt_pkg_util.pro_put_line('execute partition merge sql <<'||v_vc2_sqlstmt||'>> ok!');
         END IF;
       END IF;
     END IF;
       END LOOP;
       opt_pkg_util.pro_put_line('STEP 30: check partitions need to merge end ok!');
       opt_pkg_util.pro_put_line('STEP 40: Verification of fy window partition is ok!');
     END IF;

   EXCEPTION
     WHEN others THEN
       opt_pkg_util.pro_put_line('Error: Unexpected system error - ' || to_char(sqlerrm) || ' when ' || v_vc2_step_info);
       raise;
   END pro_verify_fy_window_prttn;

END opt_pkg_prttn_util;
/

