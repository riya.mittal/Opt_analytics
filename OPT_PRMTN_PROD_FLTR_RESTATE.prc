CREATE OR REPLACE PROCEDURE ADWU_OPTIMA_LAEX.OPT_PRMTN_PROD_FLTR_RESTATE(IN_REGN_CODE VARCHAR2,
                                                        R_RTN_VALUE  OUT NUMBER,
                                                        R_RTN_MSG    OUT VARCHAR2,V_REGN IN VARCHAR2 DEFAULT NULL)--SOX IMPLEMENTATION PROJECT
------------------------------------------------------------------------------------
  -- Procedure  : OPT_PRMTN_PROD_FLTR_RESTATE           --
  -- Originator : Gao Yongwei (yongwei.gao@hp.com)          --
  -- Author : Gao Yongwei (yongwei.gao@hp.com)          --
  -- Date   : 24-Oct-2007               --
  -- Purpose  : Refresh filter table OPT_ACTVY_PROD_FLTR_LKP        --
  ------------------------------------------------------------------------------------
  -- Parameters
  ------------------------------------------------------------------------------------
  -- No  Name    Desctiption        /   Example       --
  ------------------------------------------------------------------------------------
  -- 1  in_Regn_code  process region /   'APAC','EMEA','NALA'       --
  -- 2  r_Rtn_value return code        /   '0'          --
  -- 3  r_Rtn_msg   return msg         /   'Success....'      --
  ------------------------------------------------------------------------------------
  -- Change History                 --
  -- Date        Programmer  Description            --
  -- ----------- ----------------- ---------------------------------------------------
  -- 24-Oct-2007 Gao Yongwei  Initial Version           --
  -- 03-Feb-2008 Hill Pan     Add FY_DATE_SKID for Dated prod hier.
  -- 02-Sep-2009 Wang XiaoTao Add the joins on BUS_UNIT_SKID to improve performance.
  -- 02-Apr-2010 Gary         Modified insert sql,using util pkg perform exchange
  --                          partition to improve performance.
  -- 29-Dec-2010 Gary         Modified insert sql,added TSP AA promotion product(brand level)
  -- 20-Apr-2011 Daniel.Qin   Use left join between prmtn_fdim and prmtn_prod table
  ------------------------------------------------------------------------------------
 AS

  --Define parameters for exception handling
  DEADLOCK_DETECTED EXCEPTION;
  PRAGMA EXCEPTION_INIT(DEADLOCK_DETECTED, -60);
  V_CODE             NUMBER := SQLCODE;
  V_ERRM             VARCHAR2(64) := SUBSTR(SQLERRM, 1, 64);
  SQL_MSG            VARCHAR2(600);
  G_SQLSTMT          VARCHAR2(1000);
  V_NUMBER_TBL_COUNT NUMBER;
  V_ETL_RUN_ID       NUMBER;
  COUNT_TOTAL        NUMBER;
BEGIN
  --Initinal variable to achieve ETL_RUN_ID
  SELECT OPT_ETL_RUN_ID_SEQ.NEXTVAL INTO V_ETL_RUN_ID FROM DUAL;

  --G_SQLSTMT := ' alter table OPT_PRMTN_PROD_FLTR_SLKP truncate partition P_'||in_Regn_code;
  DBMS_OUTPUT.PUT_LINE(to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||'-> truncate table begin');
  G_SQLSTMT := ' truncate table OPT_PRMTN_PROD_FLTR_SLKP';
  EXECUTE IMMEDIATE G_SQLSTMT;
  DBMS_OUTPUT.PUT_LINE(to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||'->'||G_SQLSTMT);

  --before insert data,unusable index of OPT_PRMTN_PROD_FLTR_SLKP.
  BEGIN
          opt_gather_stats.opt_unusable_ind('OPT_PRMTN_PROD_FLTR_SLKP');
          DBMS_OUTPUT.PUT_LINE(to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||'->unusable index of OPT_PRMTN_PROD_FLTR_SLKP OK');
  END;
  --delete from  OPT_PRMTN_PROD_FLTR_SLKP where regn_code = in_regn_code;
  --COMMIT;


  --Modified by Gary for R11 defect2962 on 2011/2/22 start
  --modified by Gary for performance tuning
  --FTB-W2,CR1147,2011/4/20,Daniel.Qin, Use left join between prmtn_fdim and prmtn_prod table
  --R11,BITW2,2011/5/13,Daniel.Qin, performance tuning-add pdml and hint on insertion and selection
  EXECUTE IMMEDIATE 'ALTER SESSION ENABLE PARALLEL DML';

  INSERT /*+ APPEND PARALLEL(slkp,4)*/ INTO OPT_PRMTN_PROD_FLTR_SLKP slkp
  SELECT /*+ parallel(PP,2) parallel(PM,2)*/
           DISTINCT PM.BUS_UNIT_SKID,
                    PM.ACCT_SKID AS ACCT_PRMTN_SKID,
                    PM.PRMTN_SKID,
                    A.ACTVY_TYPE_CODE,
                    NVL(PP.PROD_SKID, 0),
                    P.STTUS_CODE,
                    PM.DATE_SKID,
                    PM.REGN_CODE,
                    0 AS ETL_RUN_ID,
                    NVL(PP.FY_DATE_SKID, PM.DATE_SKID)
      FROM OPT_PRMTN_PROD_FCT PP,
           OPT_PROD_FDIM      P,
           OPT_PRMTN_FDIM     PM,
           (select PRMTN_ID,ACTVY_TYPE_CODE from OPT_ACTVY_FDIM group by PRMTN_ID,ACTVY_TYPE_CODE) A
     WHERE PM.PRMTN_SKID = PP.PRMTN_SKID(+)
       AND PP.PROD_SKID = P.PROD_SKID(+)
       AND PM.BUS_UNIT_SKID = PP.BUS_UNIT_SKID(+)
       AND PP.BUS_UNIT_SKID = P.BUS_UNIT_SKID(+)
       AND PM.PRMTN_ID = A.PRMTN_ID(+)
       AND NOT (PM.FRCST_MODEL_NAME LIKE 'TSP%' AND PM.CORP_PRMTN_TYPE_CODE = 'Annual Agreement')
  --Modified by Gary for R11 defect2962 on 2011/2/22 end
  --Modified by Gary for R11 defect2575 on 2011/2/16 start
  --Modified by Gary for R11 defect1623 on 2010/12/29 start
  UNION ALL
  SELECT DISTINCT PM.BUS_UNIT_SKID,
                  NVL (A.ACCT_PRMTN_SKID, PM.ACCT_SKID) ACCT_PRMTN_SKID,
                  PM.PRMTN_SKID,
                  A.ACTVY_TYPE_CODE,
                  NVL (A.PROD_SKID, 0) PROD_SKID,
                  A.STTUS_CODE,
                  PM.DATE_SKID,
                  PM.REGN_CODE,
                  0 AS ETL_RUN_ID,
                  NVL (A.FY_DATE_SKID, PM.DATE_SKID) FY_DATE_SKID
      FROM (SELECT DISTINCT PP.ACCT_SKID AS ACCT_PRMTN_SKID,
                            A.ACTVY_TYPE_CODE,
                            NVL (PP.BRAND_SKID, 0) PROD_SKID,
                            P.STTUS_CODE,
                            0 AS ETL_RUN_ID,
                            NVL (PP.FY_DATE_SKID, 0) FY_DATE_SKID,
                            A.PRMTN_ID,
                            PP.BUS_UNIT_SKID
            FROM   OPT_TSP_AA_BRAND_SFCT PP,
                   OPT_PROD_FDIM P,
                   OPT_ACTVY_FDIM A
           WHERE       PP.ACTVY_SKID = A.ACTVY_SKID
                   AND PP.BRAND_SKID = P.PROD_SKID
                   AND PP.BUS_UNIT_SKID = P.BUS_UNIT_SKID) A,
         OPT_PRMTN_FDIM PM
      WHERE       A.PRMTN_ID(+) = PM.PRMTN_ID
         AND A.BUS_UNIT_SKID(+) = PM.BUS_UNIT_SKID
         AND PM.FRCST_MODEL_NAME LIKE 'TSP%'
         AND PM.CORP_PRMTN_TYPE_CODE = 'Annual Agreement';
  --Modified by Gary for R11 defect1623 on 2010/12/29 end
  --Modified by Gary for R11 defect2575 on 2011/2/16 end
  COUNT_TOTAL:=SQL%ROWCOUNT;
  COMMIT;

  EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DML';
  DBMS_OUTPUT.PUT_LINE(to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||'->'||count_total||' rows INSERT INTO OPT_PRMTN_PROD_FLTR_SLKP OK ');

  --exchange partition ,using OPT_PRTTN_LOAD_PKG.load_by_partition_exchange    modified by Gary
  BEGIN
             OPT_PRTTN_LOAD_PKG.load_by_partition_exchange('OPT_PRMTN_PROD_FLTR_LKP', 'DLY', 'OPT_PRMTN_PROD_FLTR_SLKP',V_REGN);--SOX IMPLEMENTATION PROJECT
             DBMS_OUTPUT.PUT_LINE(to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')||'->exchange partition ,using OPT_PRTTN_LOAD_PKG.load_by_partition_exchange');
  END;


  /*FOR C_BUS_UNIT_SKID IN (SELECT DISTINCT BUS_UNIT_SKID
                            FROM OPT_PRMTN_PROD_FLTR_SLKP \*where regn_code = in_regn_code*\
                          ) LOOP
    --drop table OPT_ACTVY_PROD_FLTR_LKP_TMP
    SELECT COUNT(1)
      INTO V_NUMBER_TBL_COUNT
      FROM USER_TABLES
     WHERE TABLE_NAME =
           'OPT_PRMTN_PROD_FLTR_LKP_' || C_BUS_UNIT_SKID.BUS_UNIT_SKID;
    IF V_NUMBER_TBL_COUNT = 1 THEN
      G_SQLSTMT := 'drop table OPT_PRMTN_PROD_FLTR_LKP_' ||
                   C_BUS_UNIT_SKID.BUS_UNIT_SKID || ' purge';
      EXECUTE IMMEDIATE G_SQLSTMT;
      DBMS_OUTPUT.PUT_LINE(G_SQLSTMT);
    END IF;
    --create tmp table OPT_ACTVY_PROD_FLTR_LKP_TMP
    G_SQLSTMT := ' create table OPT_PRMTN_PROD_FLTR_LKP_' ||
                 C_BUS_UNIT_SKID.BUS_UNIT_SKID ||
                 ' as select * from OPT_PRMTN_PROD_FLTR_SLKP
         WHERE BUS_UNIT_SKID = ' ||
                 C_BUS_UNIT_SKID.BUS_UNIT_SKID;

    EXECUTE IMMEDIATE G_SQLSTMT;
    DBMS_OUTPUT.PUT_LINE(G_SQLSTMT);

    --perform partition exchange
    G_SQLSTMT := ' ALTER TABLE  OPT_PRMTN_PROD_FLTR_LKP
       EXCHANGE PARTITION P_' ||
                 C_BUS_UNIT_SKID.BUS_UNIT_SKID ||
                 ' WITH TABLE OPT_PRMTN_PROD_FLTR_LKP_' ||
                 C_BUS_UNIT_SKID.BUS_UNIT_SKID;

    EXECUTE IMMEDIATE G_SQLSTMT;
    DBMS_OUTPUT.PUT_LINE(G_SQLSTMT);

    --purge tmp table OPT_ACTVY_PROD_FLTR_LKP_TMP
    G_SQLSTMT := 'drop table OPT_PRMTN_PROD_FLTR_LKP_' ||
                 C_BUS_UNIT_SKID.BUS_UNIT_SKID || ' purge';
    EXECUTE IMMEDIATE G_SQLSTMT;
    DBMS_OUTPUT.PUT_LINE(G_SQLSTMT);
  END LOOP;

  --re-build local index on OPT_PRMTN_PROD_FLTR_LKP those are unusable
  FOR P_VC2_INDEX_NAME IN (SELECT INDEX_NAME
                             FROM USER_INDEXES
                            WHERE TABLE_NAME = 'OPT_PRMTN_PROD_FLTR_LKP') LOOP
    FOR REC IN (SELECT PARTITION_NAME
                  FROM USER_IND_PARTITIONS
                 WHERE INDEX_NAME IN
                       (SELECT INDEX_NAME
                          FROM USER_INDEXES
                         WHERE TABLE_NAME = 'OPT_PRMTN_PROD_FLTR_LKP')
                   AND STATUS = 'UNUSABLE') LOOP
      G_SQLSTMT := 'ALTER INDEX ' || P_VC2_INDEX_NAME.INDEX_NAME ||
                   ' REBUILD PARTITION ' || REC.PARTITION_NAME ||
                   ' NOLOGGING';
      EXECUTE IMMEDIATE G_SQLSTMT;
      DBMS_OUTPUT.PUT_LINE(G_SQLSTMT);
    END LOOP;

  END LOOP;*/

  R_RTN_VALUE := 0;
  R_RTN_MSG   := 'Success....';

EXCEPTION
  WHEN DEADLOCK_DETECTED THEN
    R_RTN_VALUE := -1;
    R_RTN_MSG   := 'DeadLock Detected Cannot Continue Retry after Sometime';
    DBMS_OUTPUT.PUT_LINE(R_RTN_MSG);
  WHEN OTHERS THEN
    V_CODE      := SQLCODE;
    V_ERRM      := SUBSTR(SQLERRM, 1, 64);
    R_RTN_MSG   := 'Sql Errors Occured while ' || SQL_MSG ||
                   'The Error code is : ' || V_CODE || ': ' || V_ERRM;
    R_RTN_VALUE := -1;
    DBMS_OUTPUT.PUT_LINE(R_RTN_MSG);
    RETURN;

END; 
/

