CREATE OR REPLACE PACKAGE ADWU_OPTIMA_LAEX.opt_pkg_prttn_util

/*********************************************************
* File Name: opt_pkg_prttn_util.sql
* Package Name: opt_pkg_prttn_util
* Date:  30-OCT-2005
* Author:  Brian Beckman
* Code Owner:  Brian Beckman
* Overview:  Generic package to hold utility functions for
*               ADW partitioning
*
*
* Change History:
* Date      Programmer         Reason
* --------- ----------------   -------------------------
* 30-OCT-05 B. Beckman         Initial Version
* 08-Aug-2009  Bodhi Wang      change type of v_num_tbl_sub_prttn_count as number.
* 07-July-2011 Daniel.Qin      add procedure pro_verify_fy_window_prttn for verify
                               partition window of fct tables which need to
                               load by increamental mode.
***********************************************************/
IS

  V_TBLSPACE_NAME VARCHAR2(50);
  V_INDEX         VARCHAR2(50);

  --Public Methods

  PROCEDURE pro_promote_data(p_vc2_srce_tbl             IN  user_tables.table_name%TYPE,
                             p_vc2_dest_tbl             IN  user_tables.table_name%TYPE,
                             p_vc2_min_cal_skid         IN  cal_mastr_dim.cal_mastr_skid%TYPE,
                             p_vc2_region               IN  VARCHAR2,
                             p_vc2_ctrlm_ordr_id        IN  VARCHAR2,
                             p_vc2_prttn_type_code      IN  VARCHAR2,
							 V_REGN IN VARCHAR2 DEFAULT NULL); -- SOX implementation project
  
  PROCEDURE pro_exchange_tbl_with_prttn(p_vc2_prttn_tbl       IN  user_tables.table_name%TYPE,
                                        p_vc2_prttn           IN  user_tab_partitions.partition_name%TYPE,
                                        p_vc2_temp_tbl        IN  user_tables.table_name%TYPE,
                                        p_vc2_subprttn_flag   IN  VARCHAR2
  );

  PROCEDURE pro_rebuild_indexes(p_vc2_tbl_name        IN  user_tables.table_name%TYPE,
                                p_vc2_ctrlm_ordr_id   IN  VARCHAR2
  );

  -- Note that this procedure is only public so that it can be called via DBMS_JOB
  -- This should never be called directly
  PROCEDURE pro_rebuild_index_prttn(p_vc2_tbl_name           IN  user_tables.table_name%TYPE,
                                    p_vc2_index_name         IN  user_indexes.index_name%TYPE,
                                    p_vc2_subprttn_flag      IN  VARCHAR2,
                                    p_vc2_ctrlm_ordr_id      IN  prttn_index_rbld_sttus_plc.ctrlm_ordr_id%TYPE,
                                    p_vc2_rbld_start_datetm  IN  VARCHAR2
  );

  PROCEDURE pro_initialize_mth_prttn (p_vc2_tbl_name         IN  user_tables.table_name%TYPE,
                                      p_num_past_mth_num     IN  NUMBER,
                                      p_num_future_mth_num   IN  NUMBER,
                                      p_vc2_drop_prttn_flag  IN  VARCHAR2
  );

  PROCEDURE pro_initialize_day_prttn (p_vc2_tbl_name         IN  user_tables.table_name%TYPE,
                                      p_num_past_day_num     IN  NUMBER,
                                      p_num_future_day_num   IN  NUMBER,
                                      p_vc2_drop_prttn_flag  IN  VARCHAR2
  );

  PROCEDURE pro_initialize_qtr_prttn (p_vc2_tbl_name         IN  user_tables.table_name%TYPE,
                                      p_num_past_qtr_num     IN  NUMBER,
                                      p_num_future_qtr_num   IN  NUMBER,
                                      p_vc2_drop_prttn_flag  IN  VARCHAR2
  );

  PROCEDURE pro_initialize_fy_prttn (p_vc2_tbl_name         IN  user_tables.table_name%TYPE,
                                      p_num_past_fy_num     IN  NUMBER,
                                      p_num_future_fy_num   IN  NUMBER,
                                      p_vc2_drop_prttn_flag  IN  VARCHAR2
  );

  PROCEDURE pro_cleanup_old_prttn(p_vc2_tbl_name          IN user_tables.table_name%TYPE,
                                  p_vc2_cal_skid          IN  cal_mastr_dim.cal_mastr_skid%TYPE DEFAULT NULL,
                                  p_vc2_prttn_type_code   IN  VARCHAR2
  );
  PROCEDURE pro_check_tables(p_vc2_srce_tbl    IN  VARCHAR2,
                             p_vc2_dest_tbl    IN  VARCHAR2
  );
  PROCEDURE pro_create_temp_tbl(p_vc2_tbl_name       IN  user_tables.table_name%TYPE,
                                p_vc2_temp_tbl       OUT user_tables.table_name%TYPE,
								V_REGN IN VARCHAR2 DEFAULT NULL); -- SOX implementation project
								
  PROCEDURE pro_check_tbl_sub_prttn(p_vc2_tbl_name             IN  user_tables.table_name%TYPE,
                                    p_vc2_tbl_sub_prttn_str    OUT VARCHAR2,
                                    p_num_tbl_sub_prttn_count  OUT NUMBER
  );
    PROCEDURE pro_create_temp_index(p_vc2_temp_tbl    IN  user_tables.table_name%TYPE,
                                  p_vc2_tbl_name    IN  user_tables.table_name%TYPE,
                                  p_local_idx        varchar2 default NULL,
								  V_REGN IN VARCHAR2 DEFAULT NULL); -- SOX implementation project
   PROCEDURE pro_verify_fy_window_prttn (p_vc2_dest_tbl_name         IN  user_tables.table_name%TYPE,
                                        p_vc2_srce_tbl_name         IN  user_tables.table_name%TYPE default NULL
  );

END opt_pkg_prttn_util;
/

