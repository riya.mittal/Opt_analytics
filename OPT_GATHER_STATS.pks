CREATE OR REPLACE package ADWU_OPTIMA_LAEX.opt_gather_stats is
  ------------------------------------------------------------------------------------
  -- Package      : opt_gather_stats                                             --
  -- Originator   : Myra cao (huifang.cao@hp.com)                                   --
  -- Author       : Myra cao (huifang.cao@hp.com)                                   --
  -- Date         : 06-Sep-2006                                                     --
  -- Purpose      : optima tool function                                            --
  ------------------------------------------------------------------------------------
  -- Change History                                                                 --
  -- Date        Programmer        Description                                      --
  -- ----------- ----------------- ------------------------------------------------ --
  -- 13-June-2007 Myra Cao         Initial Version                                  --
  --                               optima_statistics                                --
  --                               tab_statistics                                   --
  ------------------------------------------------------------------------------------

  -- constants
  c_no_jobname_error       number := -20010;
  c_build_ind_error        number := -20011;
  c_upd_cmprs_time_error   number := -20012;
  c_gather_tbl_stats_error number := -20013;
--  c_tablespace             varchar2(10) := 'OPTIMA01';
  c_tablespace     VARCHAR2(50);
  V_INDEX          VARCHAR2(50);

  unusable_index exception;
  pragma exception_init(unusable_index, -20000);

  -- Purpose : This procedure will map table_name by job_name from metadata table:
  procedure optima_stats(p_job_name varchar2);

  -- Purpose : This procedure will collect optima tables's statitics when the table:
  -- premise : To use dynamic statistics, should have the parameter statistics_level=TYPICAL and 'GATHER_STATS_JOB' is enabled.
  -- 1) if table never has been analyzed before, p_stats_flag=1, or has refresh flag Refresh_Ind= 'Y', which means a full update,
  --  then enforce analyzing. else
  -- 2)if statistics flag stats_Ind='Y',  call dyn_tab_stats to check whether this table has been
  -- truncated or has stale data, if it's truncated or partition truncated, or has stale data changed records > 20% total records
  -- then analyzed it.

  procedure optima_stats_tbl(p_Table_Name  varchar2, p_Refresh_Ind varchar2 default 'Y');

  --Purpose : gather stats for BIP Incremental table, it only gather these partitions which have been truncated.
  procedure optima_stats_ptbl(p_Table_Name  varchar2, prttn_name varchar2, degree_cnt number default null);

  -- Purpose:  This procedure will gather table statitics base on dynamic view all_tab_modifications
  -- Check all_tab_modifications view, to whether this table has been truncated or has stale data, if it has.
  -- call DBMS_STATS.GATHER_TABLE_STATS to analyzed it.
  procedure dyn_tab_stats(p_user             varchar2,
                          p_table_name       varchar2,
                          p_estimate_percent number,
                          p_method_opt       varchar2,
                          p_change_percent   number);

  /*
  ***************************************************************************
  * Program : opt_rebuild_ind
  * Version : 1.0
  * Author  : Myra (huifang.cao@hp.com)
  * Date    : 13-Jun-2007
  * Purpose : if is_unusable = 'Y', has unusable indexes, rebuild them on table/partition/subpartition
  *           if is_unusable = 'N', default value, always rebuld indexes on table/partition/sub/partition
  */
  procedure opt_rebuild_ind(p_Table_Name varchar2,
                            p_Index_Name varchar2 default null,
                            exclude_bitmap_ind varchar2 default 'N',
                            is_unusable varchar2 default 'N',
                            cmprs_ind varchar2 default 'N',
                            ind_tablespace varchar2 default null
                            );

  /*
  ***************************************************************************
  * Program : opt_unuable_ind
  * Version : 1.0
  * Author  : Myra (huifang.cao@hp.com)
  * Date    : 20-May-2009
  * Purpose : unusable indexes
  ***************************************************************************
  -- 28-Sep-2009    Simon Hui        Modify                                 --
  --    Add skip_unique_flag to indicate whether to skip unique index or not
  ***************************************************************************
 */
  procedure opt_unusable_ind(p_Table_Name varchar2, skip_unique_flag varchar2 default 'N');

 procedure trck_sessn (p_obj_name in varchar2, p_obj_type in varchar2, p_step_ind in number, p_step_desc varchar2, p_ctrl_id in varchar2);

   /*
  ***************************************************************************
  * Program : cmprs_tbl
  * Version : 1.0
  * Author  : Myra (huifang.cao@hp.com)
  * Date    : 13-March-2008
  * Purpose : if p_table_nam is null, will compress all tables wich cmprs_ind = 'Y' in table opt_tbl_metda_prc.
  *           else if p_table_name is not null, will compress this specific table
  */
 procedure cmprs_tbl (p_table_name varchar2 default null, p_cmprs_flg varchar2 default 'Y',V_REGN VARCHAR2 DEFAULT NULL);--SOX IMPLEMENTATION PROJECT

end opt_gather_stats;
/

